# Igui

The `Igui` twiki can be found [here](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltIGUI).

The Igui settings can now be configured via a configuration file. The file must be formatted as a property file.
The Igui settings can still be configured using command line (i.e., system) properties and environment variables
as described [here](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltIGUI#Settings); at the same time
priorities for configuration items have been defined:
-   Items defined in the configuration file have the highest priority;
-   Items not found in the configuration file are then searched in system properties;
-   If no properties are defined for a configuration item, then environment variables are used.

The configuration file can be defined via the `igui.property.file` property. If that property is not defined, then
a configuration file is searched in `<USER-HOME>/.igui/igui_<TDAQ-RELEASE>.properties`.

Here is an example of a configuration file containing all the available options:

```
# See https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltIGUI#Settings for more details

# Environment variables can be used with the following format:
# ${ENV_VAR_NAME:-defaultValue}

# The default ERS subscription
# That can also be defined via the TDAQ_IGUI_ERS_SUBSCRIPTION environment variable
igui.ers.subscription=sev = FATAL

# The location of the OKS file holding the description of the default ERS filter
# That can also be defined via the TDAQ_IGUI_ERS_FILTER_FILE environment variable
igui.ers.filter.file=/atlas/oks/${TDAQ_RELEASE:-tdaq-10-00-00}/combined/sw/Igui-ers.data.xml

# The log-book URL
# That can also be defined via the TDAQ_ELOG_SERVER_URL environment variable
igui.elog.url=http://pc-atlas-www.cern.ch/elisa/api/

# The OKS GIT web view URL
# That can also be defined via the TDAQ_IGUI_DB_GIT_BASE environment variable
igui.git.url=http://pc-tdq-git.cern.ch/gitea/oks/${TDAQ_RELEASE:-tdaq-10-00-00}/commit/

# The browser to be used to open web links
# That can also be defined via the TDAQ_IGUI_USE_BROWSER environment variable
igui.use.browser=firefox

# Whether to show the warning at CONNET or not
# That can also be defined via the TDAQ_IGUI_NO_WARNING_AT_CONNECT environment variable
igui.hide.connect.warning=true

# Whether to ask for the control resource when the Igui is started
# That can also be defined via the TDAQ_IGUI_USE_RM environment variable
igui.useRM=true

# The timeout (in minutes) after which an Igui in status display mode will show a dialog informing 
# the operator that it is going to be terminated
# That can also be defined via the TDAQ_IGUI_FORCEDSTOP_TIMEOUT environment variable
tdaq.igui.forcedstop.timeout=60

# Message to be shown by a confirmation dialog before the STOP command is sent
# That can also be defined via the TDAQ_IGUI_STOP_WARNING environment variable
igui.stop.warning=Do not stop the partition@${TDAQ_PARTITION:-ATLAS}

# Number of rows to be shown by default by the ERS panel
# That can also be defined via the TDAQ_IGUI_ERS_ROWS environment variable
igui.ers.rows=1000

# Panels to forcibly load when the Igui is started
# That can also be defined via the TDAQ_IGUI_PANEL_FORCE_LOAD environment variable
# Panels are identified by their class names; multiple panels can be separated by ":"
igui.panel.force.load=PmgGui.PmgISPanel:IguiPanels.DFPanel.DFPanel

# Name of the main RDB server
# WARNING: do not define this property unless you really know what you are doing
igui.rdb.name=RDB

# Name of the read/write RDB server
# WARNING: do not define this property unless you really know what you are doing
igui.rdb_rw.name=RDB_RW
```

## tdaq-10-00-00

A new revised version of the `Main Command` panel is introduced and selected by default. A description of the new panel can be found [here](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltIGUI#The_new_Main_Command_panel).

The old legacy version is still available and will be removed in later `Igui` versions.

## tdaq-09-03-00

**Changes exposed to users**:

-   Developers of Igui panels should add a dependency from `Jers/ers.jar` in order to compile the code;
-   A new script (`Igui_start_initial`) is available to start an IGUI in the initial partition ([ADTCC-259](https://its.cern.ch/jira/browse/ADTCC-259)). The script does not require any command line parameter;
-   The Run Control advanced panel now reports information about the actual host an application is running on ([ADTCC-223](https://its.cern.ch/jira/browse/ADTCC-223)).
    

**Internal changes**:   

-   Elog dialog: removed `FTK` from the list of systems affected
-   Handling new exceptions from the `config` layer;
-   Lazy loading of the `PMG` client interface;
-   Better reporting of errors occurring during a `GIT` commit ([ADTCC-262](https://its.cern.ch/jira/browse/ADTCC-262));
-   Proper message in case of merge conflicts ([ADHI-4822](https://its.cern.ch/jira/browse/ADHI-4822));
-   Improved error message in case of external changes to the `OKS` configuration.


## tdaq-09-02-01

-   `Commit & Reload` functionality adapted to the new `OKS GIT` back-end ([ADTCC-227](https://its.cern.ch/jira/browse/ADTCC-227)):

    A new reload panel has been developed in order to allow proper configuration reload when the new `OKS GIT` back-end is used. In this new version of the panel, the user
    is asked to select the configuration version to reload on the basis of the last commits available in the `GIT` repository.
    The selection of the configuration to be reloaded is no more file-based, but the user is asked to select a `GIT` commit that, in principle, may include changes to several
    separate files. In such a way, there is not any risk no more to select a set of incompatible files. The list of modified files per commit is available un-hiding a specific
    column in the table (that can be done changing the table properties using the little button located on the right upper corner of the table).
    Of course, the selection of a specific commit implies to import all the changes applied in the previous commits.

