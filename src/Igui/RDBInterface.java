package Igui;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import Igui.IguiException.RDBException;
import config.ConfigException;


/**
 * Class implementing communication to the RDB system using the IDL interface.
 * <p>
 * It is used only to retrieve the modified database files and send the command to reload them.
 */
class RDBInterface {

    /**
     * Lock that client classes may use to synchronize with the reloading operations
     */
    private final Lock lk;

    /**
     * Single executor used to parallelize the calls to the RDB and the RDB_RW
     */
    private final ThreadPoolExecutor rdbReloaderExecutor = ExecutorFactory.createSingleExecutor("RDB-Reloader");

    /**
     * Constructor.
     */
    RDBInterface() {
        this.lk = new ReentrantLock();
    }

    /**
     * It asks the RDB system to reload the specified the database files.
     * <p>
     * This operation is guarded by the {@link Lock} returned by the {@link #getSynchronizer()} method.
     * 
     * @param files The list of files to reload
     * @throws IguiException.RDBException Thrown if some error occurs during the communication with the RDB system
     */
    void reloadDatabaseFiles(final List<? extends String> files) throws IguiException.RDBException {
        if(files.size() > 0) {
            final String[] f = files.toArray(new String[files.size()]);
            Future<?> task = null;

            // Here we lock and release it only after both the RDB and the RDB_RW servers
            // have finished reloading the database (see Igui.ReloadDbTask.doInBackground).
            this.lk.lock();
            try {
                task = this.rdbReloaderExecutor.submit(new Callable<Void>() {
                    @Override
                    public Void call() throws IguiException.RDBException {
                        final String serverName = IguiConfiguration.instance().getRWDbServerName();
                        RDBInterface.this.reloadRDB(serverName, f);
                        IguiLogger.debug(serverName + " has finished reloading the database");
                        return null;
                    }
                });
                
                final String serverName = IguiConfiguration.instance().getDbServerName();
                this.reloadRDB(serverName, f);
                IguiLogger.debug(serverName + " has finished reloading the database");
            }
            catch(final IguiException.RDBException ex) {
                throw ex;
            }
            catch(final Exception ex) {
                throw new IguiException.RDBException("Unexpected error while sending the reload database command: " + ex, ex);
            }
            finally {
                try {
                    if(task != null) {
                        task.get();
                    }
                }
                catch(final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    throw new IguiException.RDBException("The database reloading has been interrupted: " + ex, ex);
                }
                catch(final ExecutionException ex) {
                    final Throwable cause = ex.getCause();
                    if(RDBException.class.isInstance(cause) == true) {
                        throw RDBException.class.cast(cause);
                    }
                    
                    throw new IguiException.RDBException("Cannot reload the database: " + ex.getCause(), ex);
                }
                finally {
                    this.lk.unlock();
                }
            }
        }
    }

    /**
     * This method sets the configuration version to use and triggers the DB reload.
     * 
     * @param configVersion The version (hash) of the configuration version to use
     * @throws RDBException Error in the reload procedure
     */
    void setConfigurationAndReload(final String configVersion) throws RDBException {
        this.lk.lock();
        try {
            Igui.instance().getDalPartition().set_config_version(configVersion, true);
        }
        catch(final ConfigException ex) {
            final StringBuilder msg = new StringBuilder();   
            final boolean schemaChanged = dal.Algorithms.is_schema_changed(ex).booleanValue();
            if(schemaChanged == true) {
                msg.append("\nOKS schema file(s) have been modified. The partition must be restarted before continuing.\n\n");
                msg.append("1. Terminate the partition closing the IGUI and selecting the \"" + IguiFrame.closeAndExitLabel() + "\" option from the \"File\" menu.\n");                
                msg.append("2. Start the partition from scratch using the standard tools.\n");
            } else {
                msg.append("Failed to set and reload the new configuration version: " + ex);
            }
            
            throw new IguiException.RDBException(msg.toString(), schemaChanged, ex);
        }
        finally {
            this.lk.unlock();
        }
    }
    
    /**
     * It returns the lock used to protect the reload command.
     * 
     * @return The lock used to protect the reload command
     */
    Lock getSynchronizer() {
        return this.lk;
    }

    /**
     * It sends a command to an RDB server to reload some modified files.
     * 
     * @param rdbName The name of the RDB server as it is published in IPC
     * @param files The Array of files to be reloaded
     * @throws IguiException.RDBException The files cannot be reloaded
     */
    private void reloadRDB(final String rdbName, final String[] files) throws IguiException.RDBException {
        try {
            // Get the server reference from IPC and ask to reload
            try {
                final rdb.cursor rdbServer = Igui.instance().getPartition().lookup(rdb.cursor.class, rdbName);
                rdbServer.reload_database(rdb.CreateProcessInfo.get("IGUI"), files);
            }
            catch(final ipc.InvalidObjectException ex) {
                try {
                    final rdb.writer rdbWriter = Igui.instance().getPartition().lookup(rdb.writer.class, rdbName);
                    rdbWriter.reload_database(rdb.CreateProcessInfo.get("IGUI"), files);
                }
                catch(final ipc.InvalidObjectException exx) {
                    throw new IguiException.RDBException("Database reloading failed because the \"" + rdbName
                                                         + "\" server could not be found in IPC: " + exx, exx);
                }
            }
        }
        catch(final IguiException.RDBException ex) {
            throw ex;
        }
        catch(final ipc.InvalidPartitionException ex) {
            throw new IguiException.RDBException(rdbName + " database reloading failed because the IPC partition cannot be reached: " + ex,
                                                 ex);
        }
        catch(final rdb.CannotProceed ex) {
            final StringBuilder msg = new StringBuilder();            
            
            final rdb.CannotProceedException cause = new rdb.CannotProceedException(ex);
            final boolean schemaChanged = dal.Algorithms.is_schema_changed(cause).booleanValue();
            if(schemaChanged == true) {
                msg.append("\nOKS schema file(s) have been modified. The partition must be restarted before continuing.\n\n");
                msg.append("1. Terminate the partition closing the IGUI and selecting the \"" + IguiFrame.closeAndExitLabel() + "\" option from the \"File\" menu.\n");
                msg.append("2. Start the partition from scratch using the standard tools.\n");
            } else {
                msg.append(rdbName + " database reloading failed");
            }
            
            throw new IguiException.RDBException(msg.toString(), schemaChanged, cause);
        }
        catch (final daq.tokens.AcquireTokenException ex) {
            throw new IguiException.RDBException("cannot acquire token to reload " + rdbName, ex);
        }
        catch(final Exception ex) {
            throw new IguiException.RDBException(rdbName + " database reloading failed: " + ex.getMessage(), ex);
        }
    }
}
