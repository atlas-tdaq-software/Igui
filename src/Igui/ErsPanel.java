package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.JToolTip;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import Igui.IguiConstants.MessageSeverity;
import Igui.IguiException.ConfigException;
import Igui.RunControlFSM.State;
import Igui.Common.JTreeTable;
import Igui.Common.MultiLineTable;
import Igui.Common.MultiLineToolTip;
import ers.BadStreamConfiguration;
import ers.Issue;
import ers.ReceiverNotFound;
import ers.StreamManager;


/**
 * Panel implementing the interface to the ERS system. The core part of the panel is a table showing the messages coming from the ERS (only
 * messages matching the subscription criteria are received). The panel offers the user an easy way to change the subscription criteria
 * using simple radio buttons (more complex formula may be written in any case) and to switch between the long and short message format.
 * Even the number of visible messages in the table can be changes (for performance reasons the number of visible messages should be around
 * 1000). The RMB can be used to copy messages to the system clip-board.
 * <p>
 * The GUI also offers the possibility to enable/disable or reload the default ERS filter. The reload is needed every time the database file
 * containing the filter description is modified. The filter and the user criteria are combined in a logical AND.
 * <p>
 * The message table is a customized instance of {@link JTreeTable}.
 */
class ErsPanel extends IguiPanel {
    private static final long serialVersionUID = -6469880502284255860L;

    /**
     * Timeout (in seconds) waiting for the subscription to be removed when the panel is terminated
     * 
     * @see #panelTerminated()
     */
    private final static int TIMEOUT = 10;

    /**
     * Single executor to perform and remove ERS subscriptions
     */
    private final ThreadPoolExecutor subscriptionExecutor = ExecutorFactory.createSingleExecutor("ERS-Subscriber");

    /**
     * List containing the check boxes used to define the subscription criteria
     * <p>
     * At the moment the list if filled in the constructor ({@link #initGUI()}) so there is no need of synchronization (it is accessed only
     * from the EDT).
     */
    private final List<JCheckBox> checkBoxesList = new ArrayList<JCheckBox>();

    /**
     * This panel name
     */
    private final static String panelName = "ERS";

    /**
     * Default number of visible messages
     */
    private final Integer defaultMaxRows = Integer.valueOf(IguiConfiguration.instance().getErsPanelRows());

    /**
     * Button used to select the short message format
     */
    private final JToggleButton shortMessageButton = new JToggleButton();

    /**
     * Button used to select the long message format
     */
    private final JToggleButton longMessageButton = new JToggleButton();

    /**
     * Radio button used to enable the user entering a custom subscription criteria
     */
    private final JCheckBox customCheckBox = new JCheckBox("Expression");

    /**
     * Toggle button to enable/disable the default ERS subscription filter
     */
    private final JToggleButton defaultFilterButton = new JToggleButton() {
        private static final long serialVersionUID = -6149495867420166225L;

        @Override
        public JToolTip createToolTip() {
            final MultiLineToolTip tt = new MultiLineToolTip(false);
            tt.setComponent(this);
            return tt;
        }       
    };

    /**
     * Button used to reload the default ERS filters
     */
    private final JButton defaultFilterReloadButton = new JButton() {
        private static final long serialVersionUID = 857617409320457104L;

        @Override
        public JToolTip createToolTip() {
            final MultiLineToolTip tt = new MultiLineToolTip(false);
            tt.setComponent(this);
            return tt;
        }       
    };

    /**
     * Text field to enter the custom criteria string
     */
    private final JTextField customSubscription = new JTextField();

    /**
     * Button used to subscribe to the ERS using the specified criteria
     */
    private final JButton subscriptionButton = new JButton("Subscribe");

    /**
     * Text field used to show the current subscription criteria (at the beginning there is no subscription)
     */
    private final JTextField currentSubscriptionText = new JTextField("NONE");

    /**
     * Spinner used to change the number of messages in the table
     */
    private final SpinnerNumberModel rowSpinnerModel = new SpinnerNumberModel(this.defaultMaxRows,
                                                                              Integer.valueOf(100),
                                                                              Integer.valueOf(10000),
                                                                              Integer.valueOf(1));
    private final JSpinner rowSpinner = new JSpinner(this.rowSpinnerModel);

    /**
     * Name of the icon file for the {@link #clearERSButton} button
     */
    private final static String clearERSIconName = "clearERS.png";

    /**
     * Button used to clear the message table
     */
    private final JButton clearERSButton = new JButton("Clear");

    /**
     * Object holding methods associated to buttons
     */
    private final ButtonActions bActions = new ButtonActions();

    /**
     * The root node of the message table
     */
    private final MessageTableTreeNode rootMessageNode;

    /**
     * The message table model
     */
    private final MessageTableTreeModel tableTreeModel;

    /**
     * The message table
     */
    private final MessageTable messageTable;

    /**
     * Receiver used to process call-backs.
     */
    private final IssueReceiver issueReceiver;

    /**
     * The name of the stream that the panel has currently subscribed
     */
    private final String stream = "mts";

    /**
     * Default subscription string (the default filter may be added to it if available)
     */
    private final static String defaultSimpleSubscription = "sev=ERROR or sev=WARNING or sev=FATAL";

    /**
     * Object used to load the default ERS filter
     */
    private final ErsFilterLoader filterLoader = new ErsFilterLoader(IguiConfiguration.instance().getErsFilterFile());

    /**
     * The partition name: useful because this class is used also for the ERS monitor (where the partition context may be changed by the
     * user via the UI)
     */
    // The volatile grants visibility amongst threads (the string is created in
    // the EDT but than read in different threads). After the GUI
    // is visible, for the sake of consistency (i.e., do not change the
    // partition while subscribing) the partition is always changed in the
    // subscriber executor (which is a single executor). If the strategy is
    // changed then, maybe, some additional synchronization may be
    // required.
    private volatile String partitionName;

    /**
     * String buffer holding the user subscription criteria (defined by the user via the UI)
     * <p>
     * The final subscription is a logical combination of this criteria and the filter
     * <p>
     * The StringBuffer gives enough synchronization to not let the string be in an inconsistent state. After the panel is build and
     * initialized, this string buffer is read and written in the {@link #subscriptionExecutor} executor. In case of more parallelism, more
     * synchronization may be needed (i.e., the string is always formally correct and not corrupted, but of course the logic of other
     * invariants is not guaranteed).
     */
    private final StringBuffer ersUserCriteria = new StringBuffer();

    /**
     * Object to format the date in the message table
     */
    private static final ThreadLocal<DateFormat> timeFormat = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.UK);
        }
    };

    /**
     * String buffer holding the full subscription string: filter plus user criteria
     * <p>
     * For synchronization comments, look at {@link #ersUserCriteria}
     */
    private final StringBuffer ersFullSubscription = new StringBuffer();

    /**
     * Icon file name for the {@link #shortMessageButton} button
     */
    private final static String shortMessageIconName = "shortMessage.png";

    /**
     * Icon file name for the {@link #longMessageButton} button
     */
    private final static String longMessageIconName = "longMessage.png";

    /**
     * Icon file name for the {@link #defaultFilterButton} button
     */
    private final static String defaultFilterIconName = "ersFilter.png";

    /**
     * Icon file name for the {@link #defaultFilterReloadButton} button
     */
    private final static String defaultFilterReloadIconName = "ersFilterReload.png";

    /**
     * Application name used for internal messages (i.e., messages shown in the table but produced by the Igui itself)
     */
    private final static String internalMessageAppName = "IGUI";

    /**
     * Message name used for internal messages (i.e., messages shown in the table but produced by the Igui itself)
     */
    private final static String internalMessageName = "INTERNAL";

    /**
     * The table used to show the ers.Issues
     */
    private static class MessageTable extends JTreeTable implements Cloneable {
        private static final long serialVersionUID = -5999934276288961362L;

        /**
         * The size of map caching the height of the row containing the message text (it is the only one with multiple rows)
         */
        private final static int cacheSize = 5000;

        /**
         * Map caching the height of the message row taking into account the string that the row will hold (String -> (column width -> row
         * height)).
         * <p>
         * The map size is calculated in such a way that no re-hashes will ever happen.
         */
        private final Map<String, Map<Integer, Integer>> widthMap = new HashMap<String, Map<Integer, Integer>>(Math.round(MessageTable.cacheSize / 0.75f) + 1);

        /**
         * Constructor.
         * 
         * @param treeTableModel The model to use for the table
         * @see #cutomize()
         */
        MessageTable(final MessageTableTreeModel treeTableModel) {
            super(treeTableModel);
            this.cutomize();
        }

        /**
         * Overridden for performance reasons: the idea it to cache height values instead of calculating them every time.
         * <p>
         * A lot of CPU time is saved when the table is just resized and new messages come or there are some common messages repeated in
         * time.
         * 
         * @see Igui.Common.MultiLineTable#getHeight(java.lang.String, int)
         */
        @Override
        public int getHeight(final String text, final int width) {
            int h;

            try {
                final Integer w = this.widthMap.get(text).get(Integer.valueOf(width));
                h = w.intValue();
            }
            catch(final NullPointerException ex) {
                // The value is not in the cache, calculate the height in the
                // 'normal' way
                h = super.getHeight(text, width);

                // The cache has reached its maximum size, clear it
                if(this.widthMap.size() > MessageTable.cacheSize) {
                    this.widthMap.clear();
                }

                // Some value for the 'text' already exists?
                final boolean isPresent = this.widthMap.containsKey(text);
                if(isPresent == true) {
                    // Yes, it exists -> just put a new entry
                    this.widthMap.get(text).put(Integer.valueOf(width), Integer.valueOf(h));
                } else {
                    // No, it does not -> create the new entry in the map
                    final Map<Integer, Integer> m = new HashMap<Integer, Integer>(100);
                    m.put(Integer.valueOf(width), Integer.valueOf(h));
                    this.widthMap.put(text, m);
                }
            }

            return h;
        }

        /**
         * It creates a clone of the message table (used in {@link ErsMonitor}).
         */
        @Override
        public MessageTable clone() {
            // Clone the root node
            final MessageTableTreeNode rootNode = (MessageTableTreeNode) this.getTree().getModel().getRoot();
            final MessageTableTreeNode newRootNode = (MessageTableTreeNode) rootNode.clone();

            // Copy each root node child and assign the copied node to the new
            // root
            @SuppressWarnings("rawtypes")
            final Enumeration ch = rootNode.children();
            while(ch.hasMoreElements() == true) {
                newRootNode.add(this.copyNode((MessageTableTreeNode) ch.nextElement()));
            }

            // Create a new model and use it in a new table
            final MessageTableTreeModel newModel = new MessageTableTreeModel(newRootNode, rootNode.getChildCount());
            return new MessageTable(newModel);
        }

        /**
         * It clones the <code>node</code> node and all its children (it calls itself recursively).
         * <p>
         * Each node cannot belong to more than one tree.
         * 
         * @param node The node to be cloned
         * @return A copy of <code>node</code>
         */
        private MessageTableTreeNode copyNode(final MessageTableTreeNode node) {
            final MessageTableTreeNode newNode = (MessageTableTreeNode) node.clone();

            @SuppressWarnings("rawtypes")
            final Enumeration ch = node.children();
            while(ch.hasMoreElements() == true) {
                newNode.add(this.copyNode((MessageTableTreeNode) ch.nextElement()));
            }

            return newNode;
        }

        /**
         * It customizes the table
         */
        private void cutomize() {
            // Set the font to use
            final Font f = new Font("Dialog", Font.BOLD, 12);
            this.getTree().setFont(f);
            this.setFont(f);

            // General options
            this.setIntercellSpacing(new Dimension(0, 1));
            this.setShowGrid(false);
            this.setShowHorizontalLines(true);
            this.getTableHeader().setReorderingAllowed(false);
            this.setColumnControlVisible(true);
            this.setSortable(false); // Do not enable sorting! Great performance
                                     // penalty with the usual message rate!
            this.setVisibleRowCount(30);

            // Tune column width
            this.getColumnModel().getColumn(0).setMinWidth(95);
            this.getColumnModel().getColumn(0).setPreferredWidth(100);
            this.getColumnModel().getColumn(0).setMaxWidth(110);

            this.getColumnModel().getColumn(1).setMinWidth(120);
            this.getColumnModel().getColumn(1).setPreferredWidth(120);

            this.getColumnModel().getColumn(2).setMinWidth(120);
            this.getColumnModel().getColumn(2).setPreferredWidth(120);

            this.getColumnModel().getColumn(3).setMinWidth(120);
            this.getColumnModel().getColumn(3).setPreferredWidth(120);

            this.getColumnModel().getColumn(4).setMinWidth(250);
            this.getColumnModel().getColumn(4).setPreferredWidth(700);

            // Set column cell renderers.
            // No renderer should be installed for column '0' (the tree
            // column)!!!
            this.setDefaultRenderer(MessageSeverity.class, new SeverityColumnRenderer());
            this.setDefaultRenderer(String.class, new StringColumnRenderer());
            this.getColumnModel().getColumn(4).setCellRenderer(new MessageColumnRenderer());

            this.getTree().putClientProperty("JTree.lineStyle", "HORIZONTAL");

            // Add mouse listener
            this.addMouseListener(new MouseListenerAdapter(this));
        }
    }

    /**
     * Class extending {@link DefaultMutableTreeNode} and describing nodes used in the message table. Each node represents an ers.Issue.
     */
    private final static class MessageTableTreeNode extends DefaultMutableTreeNode {
        private static final long serialVersionUID = 6315305585039944964L;

        /**
         * List containing the message parameters 0 - time (String) 1 - severity ({@link MessageSeverity}) 2 - application name (String) 3 -
         * message name (String) 4 - message text (String) 5 - long message (String)
         */
        private final List<Object> args = new ArrayList<Object>();

        /**
         * The message severity
         */
        private final MessageSeverity severity;

        /**
         * Constructor
         * 
         * @param time Message time
         * @param severity Message severity
         * @param appName Application name
         * @param msgName Message name
         * @param msgText Message text
         * @param msgDetails Additional message details
         */
        MessageTableTreeNode(final String time,
                             final MessageSeverity severity,
                             final String appName,
                             final String msgName,
                             final String msgText,
                             final String msgDetails)
        {
            super();
            this.args.add(0, time);
            this.args.add(1, severity);
            this.args.add(2, appName);
            this.args.add(3, msgName);
            this.args.add(4, msgText);

            final StringBuilder longMessage = new StringBuilder();
            longMessage.append(severity.name() + " - ");
            longMessage.append(msgText + " - ");
            longMessage.append("APPLICATION: " + appName + " - ");
            longMessage.append(msgDetails);

            this.args.add(5, longMessage.toString());
            this.severity = severity;
        }

        public String getTime() {
            return (String) this.args.get(0);
        }

        @SuppressWarnings("unused")
        public MessageSeverity getSeverity() {
            return (MessageSeverity) this.args.get(1);
        }

        @SuppressWarnings("unused")
        public String getApplicationName() {
            return (String) this.args.get(2);
        }

        @SuppressWarnings("unused")
        public String getMessageName() {
            return (String) this.args.get(3);
        }

        @SuppressWarnings("unused")
        public String getMessageText() {
            return (String) this.args.get(4);
        }

        @SuppressWarnings("unused")
        public String getLongMessage() {
            return (String) this.args.get(5);
        }

        @SuppressWarnings("unused")
        public Color getBackgroundColor() {
            return this.severity.getBgColor();
        }

        @SuppressWarnings("unused")
        public Color getForegroundColor() {
            return this.severity.getFgColor();
        }

        @SuppressWarnings("unused")
        public Color getTextColor() {
            return this.severity.getTxtColor();
        }

        @SuppressWarnings("unused")
        public Color getTextBackgorundColor() {
            return this.severity.getTxtBgColor();
        }

        /**
         * Convenience method to be used with the table model (each column in the table will show a message parameter).
         * 
         * @param col The table column
         * @return The object to be shown at column <code>col</code>
         * @throws IndexOutOfBoundsException The <code>col</code> value should not exceed the {@link #args} bounds.
         * @see MessageTableTreeModel#getValueAt(TreeNode, int)
         */
        public Object getValueAt(final int col) throws IndexOutOfBoundsException {
            return this.args.get(col);
        }

        /**
         * Needed to show the time as text in the tree nodes.
         * <p>
         * The tree used as renderer of the table first column will show the message time.
         * 
         * @see javax.swing.tree.DefaultMutableTreeNode#toString()
         */
        @Override
        public String toString() {
            return this.getTime();
        }
    }

    /**
     * The table model: it is based on the usage of {@link MessageTableTreeNode} nodes.
     * <p>
     * This table model defines 5 columns for the table:
     * <ul>
     * <li>TIME (Object);
     * <li>SEVERITY ({@link IguiConstants.MessageSeverity});
     * <li>APPLICATION (String);
     * <li>NAME (String);
     * <li>MESSAGE (Object) (not String to easily install a different renderer)
     * </ul>
     * 
     * @see JTreeTable.AbstractTreeTableModel
     */
    private static class MessageTableTreeModel extends JTreeTable.AbstractTreeTableModel {
        private static final long serialVersionUID = -2692038462578123476L;

        /**
         * The root node
         */
        private final MessageTableTreeNode rootNode;

        /**
         * The maximum number of rows (i.e., messages) to be shown
         */
        private final AtomicInteger maxRowNum;

        /**
         * Whether to show the short or long message format
         */
        private final AtomicBoolean showLongMessage = new AtomicBoolean(false);

        /**
         * Names of the columns.
         */
        private final String[] cNames = {"TIME", "SEVERITY", "APPLICATION", "NAME", "MESSAGE"};

        /**
         * Types of the columns.
         */
        private final Class<?>[] cTypes = {Object.class, MessageSeverity.class, String.class, String.class, Object.class};

        /**
         * Constructor.
         * 
         * @param root The root node
         * @param maxRows The maximum number of rows to show
         */
        MessageTableTreeModel(final MessageTableTreeNode root, final int maxRows) {
            super(root);
            this.rootNode = root;
            this.maxRowNum = new AtomicInteger(maxRows);
        }

        /**
         * @see Igui.Common.JTreeTable.AbstractTreeTableModel#getColumnCount()
         */
        @Override
        public int getColumnCount() {
            return this.cNames.length;
        }

        /**
         * @see Igui.Common.JTreeTable.AbstractTreeTableModel#getColumnName(int)
         */
        @Override
        public String getColumnName(final int column) {
            return this.cNames[column];
        }

        /**
         * @see Igui.Common.JTreeTable.AbstractTreeTableModel#getValueAt(javax.swing.tree.TreeNode, int)
         */
        @Override
        public Object getValueAt(final TreeNode node, final int column) {
            // The model uses MessageTableTreeNode nodes
            final MessageTableTreeNode msgNode = (MessageTableTreeNode) node;
            // The last column may show the short or long message text
            if((column == 4) && (this.showLongMessage.get() == true)) {
                return msgNode.getValueAt(5);
            }

            return msgNode.getValueAt(column);
        }

        /**
         * @see Igui.Common.JTreeTable.AbstractTreeTableModel#getColumnClass(int)
         */
        @Override
        public Class<?> getColumnClass(final int column) {
            return this.cTypes[column];
        }

        /**
         * It returns the maximum number of visible rows.
         * 
         * @return The maximum number of visible rows
         */
        @SuppressWarnings("unused")
        public int getMaxRowNumber() {
            return this.maxRowNum.intValue();
        }

        /**
         * It adds a message (i.e., a node) to the tree table at the position specified by <code>childIndex</code>.
         * 
         * @param child The message (node) to add to the table
         * @param childIndex The position the message (node) should be inserted at
         */
        public void addMessage(final MessageTableTreeNode child, final int childIndex) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // The message will be added to the root node (not visible)
            this.rootNode.insert(child, childIndex);

            // Always fire the right event!!!
            this.fireTreeNodesInserted(new TreeModelEvent(this,
                                                          new MessageTableTreeNode[] {this.rootNode},
                                                          new int[] {childIndex},
                                                          new MessageTableTreeNode[] {child}));

            // Check the table size
            this.checkTableSize();

            // TODO: this event should not be needed but without it messages are not properly
            // shown after the table has been cleared
            this.fireTreeNodesChanged(new TreeModelEvent(this, new MessageTableTreeNode[] {this.rootNode}));
        }

        /**
         * It adds the message at the top of the tree.
         * <p>
         * It just calls {@link #addMessage(MessageTableTreeNode, int)} with a <code>childIndex</code> value equal to zero.
         * 
         * @param child The message (node) to add to the table
         */
        public void addMessage(final MessageTableTreeNode child) {
            this.addMessage(child, 0);
        }

        /**
         * It adds multiple messages (nodes) to the table.
         * <p>
         * This method should be preferred with respect to adding multiple times single messages because a single event is fired (and not an
         * event for each node insertion).
         * 
         * @param children Array containing the messages (nodes) to add to the table
         */
        public void addMessages(final MessageTableTreeNode[] children) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            final int nodeToAdd = Array.getLength(children);
            // Children indexes have to be calculated to fire the right event
            final int[] childIndexes = new int[nodeToAdd];

            // Add the children to the root node and build the index array
            int counter = 0;
            for(final MessageTableTreeNode node : children) {
                this.rootNode.insert(node, counter);
                childIndexes[counter] = counter;
                ++counter;
            }

            // Fire the right event
            this.fireTreeNodesInserted(new TreeModelEvent(this, new MessageTableTreeNode[] {this.rootNode}, childIndexes, children));

            // Check the table size
            this.checkTableSize();

            // TODO: this event should not be needed but without it messages are not properly
            // shown after the table has been cleared
            this.fireTreeNodesChanged(new TreeModelEvent(this, new MessageTableTreeNode[] {this.rootNode}));
        }

        /**
         * It removes the <code>child</code> message (node) from the table.
         * <p>
         * If several removals are needed at the same time, then do not use this method.
         * 
         * @param child The message (node) to remove
         */
        @SuppressWarnings("unused")
        public void removeMessage(final MessageTableTreeNode child) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // Remove the node
            final TreeNode parent = child.getParent();
            if(parent != null) {
                final MessageTableTreeNode parentMessage = (MessageTableTreeNode) parent;
                final int childIndex = parentMessage.getIndex(child);

                // Remove the child
                parentMessage.remove(child);

                // Fire the right event
                this.fireTreeNodesRemoved(new TreeModelEvent(this,
                                                             parentMessage.getPath(),
                                                             new int[] {childIndex},
                                                             new MessageTableTreeNode[] {child}));
            } else {
                IguiLogger.debug("Cannot remove message " + child.toString() + " because it has no parent!");
            }
        }

        /**
         * It removes the <code>msg</code> messages from the table.
         * <p>
         * Use this method when the messages to remove do not belong to the same parent, otherwise use one of the other methods requiring
         * the parent node.
         * 
         * @param msg Array containing the messages to remove from the table
         */
        public void removeMessages(final MessageTableTreeNode[] msg) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            for(final MessageTableTreeNode node : msg) {
                node.removeFromParent();
            }

            this.fireTreeStructureChanged(new TreeModelEvent(this, new MessageTableTreeNode[] {this.rootNode}));
        }

        /**
         * Remove the message (node) at index specified by <code>childIndex</code>
         * <p>
         * If several removals are needed at the same time, then do not use this method.
         * 
         * @param parent The parent of the nodes to remove
         * @param childIndex The index of the message (node) to remove
         * @throws ArrayIndexOutOfBoundsException If <code>childIndex</code> is out of bounds
         */
        @SuppressWarnings("unused")
        public void removeMessage(final MessageTableTreeNode parent, final int childIndex) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // Look for the node
            final MessageTableTreeNode childToRemove = (MessageTableTreeNode) parent.getChildAt(childIndex);

            // Remove and fire the event
            parent.remove(childIndex);

            this.fireTreeNodesRemoved(new TreeModelEvent(this,
                                                         parent.getPath(),
                                                         new int[] {childIndex},
                                                         new MessageTableTreeNode[] {childToRemove}));

        }

        /**
         * It removes from <code>parent</code> all the messages (nodes) with indexes defined in <code>index</code>
         * 
         * @param parent The parent node
         * @param index The children indexes (they must be in the ascending order, from lowest to highest).
         * @throws ArrayIndexOutOfBoundsException If one of the passed indexes is out of bounds
         */
        @SuppressWarnings("unused")
        public void removeMessages(final MessageTableTreeNode parent, final int[] index) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            try {
                final TreeNode[] nodes = new TreeNode[index.length];
                for(int i = 0; i < index.length; ++i) {
                    final int ind = index[i];
                    final TreeNode n = parent.getChildAt(ind);
                    nodes[i] = n;
                    parent.remove(ind);
                }

                this.fireTreeNodesRemoved(new TreeModelEvent(this, parent.getPath(), index, nodes));
            }
            catch(final ArrayIndexOutOfBoundsException ex) {
                // One of the index is out of bounds. Fire a tree structure
                // changed event to keep the table in a consistent state
                this.fireTreeStructureChanged(new TreeModelEvent(this, new MessageTableTreeNode[] {this.rootNode}));
                throw ex;
            }
        }

        /**
         * It removes the <code>child</code> messages (nodes) from the parent <code>parent</code> node.
         * 
         * @param parent The parent node
         * @param child The children nodes to remove
         */
        @SuppressWarnings("unused")
        public void removeMessages(final MessageTableTreeNode parent, final MessageTableTreeNode[] child) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // The indexes of the removed nodes must be ordered from lowest to
            // highest
            final Map<Integer, MessageTableTreeNode> m = new TreeMap<Integer, MessageTableTreeNode>();
            for(final MessageTableTreeNode n : child) {
                final int index = parent.getIndex(n);
                if(index != -1) {
                    m.put(Integer.valueOf(index), n);
                } else {
                    IguiLogger.debug("Cannot remove the message " + n.toString() + " becasue it is not a child of " + parent.toString());
                }
            }

            // Remove the nodes
            final int num = m.size();
            final int[] index = new int[num];
            final MessageTableTreeNode[] nodes = new MessageTableTreeNode[num];

            int counter = 0;
            for(final Map.Entry<Integer, MessageTableTreeNode> e : m.entrySet()) {
                final Integer i = e.getKey();
                index[counter] = i.intValue();

                final MessageTableTreeNode n = e.getValue();
                nodes[counter++] = n;
                n.removeFromParent();
            }

            // Notify listeners
            this.fireTreeNodesRemoved(new TreeModelEvent(this, parent.getPath(), index, nodes));
        }

        /**
         * It removes messages (nodes) from index <code>startIndex</code> to index <code>stopIndex</code>.
         * 
         * @param parent The parent node
         * @param startIndex The start index (excluded)
         * @param stopIndex The stop index (included)
         * @throws IllegalArgumentException If <code>startIndex</code> is greater than <code>stopIndex</code>
         */
        public void removeMessages(final MessageTableTreeNode parent, final int startIndex, final int stopIndex)
            throws IllegalArgumentException
        {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // Check the arguments
            if(startIndex > stopIndex) {
                throw new IllegalArgumentException("Stop index has to be greater than start index");
            }

            // Calculate the number of nodes to remove
            final int num = stopIndex - startIndex;

            // Create the index and node vectors needed to fire the right event
            final int[] index = new int[num];
            final MessageTableTreeNode[] nodesToRemove = new MessageTableTreeNode[num];

            // Start removing (the node at startIndex is not removed, so start
            // at startIndex + 1)
            int incrementingIndex = startIndex;
            for(int i = 0; i < num; ++i) {
                index[i] = ++incrementingIndex;
                // Since in every cycle of the loop we remove a node, the node
                // to remove is always at position "startIndex + 1"
                nodesToRemove[i] = (MessageTableTreeNode) parent.getChildAt(startIndex + 1);
                if(nodesToRemove[i] != null) {
                    nodesToRemove[i].removeFromParent();
                } else {
                    IguiLogger.debug("Failed removing message at index " + (startIndex + 1) + ": message not found");
                }
            }

            // Fire the event
            this.fireTreeNodesRemoved(new TreeModelEvent(this, parent.getPath(), index, nodesToRemove));
        }

        /**
         * It clears the table.
         */
        public void clearTable() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            this.rootNode.removeAllChildren();
            this.fireTreeStructureChanged(new TreeModelEvent(this, new MessageTableTreeNode[] {this.rootNode}));
        }

        /**
         * It returns <code>true</code> if the long message format is used.
         * 
         * @return <code>true</code> if the long message format is used
         */
        public boolean longMessages() {
            return this.showLongMessage.get();
        }

        /**
         * It checks the number of messages in the table and, eventually, eldest messages are removed if their number is greater than
         * {@link #maxRowNum}.
         * <p>
         * This method is called any time a new message (node) is inserted into the table.
         */
        private void checkTableSize() {
            // Get the total number of messages
            final int rootChildNum = this.rootNode.getChildCount();
            // Calculate the number of messages to remove
            final int toRemove = rootChildNum - this.maxRowNum.intValue();
            if(toRemove > 0) {
                // Remove messages in excess
                final int startIndex = this.maxRowNum.intValue() - 1;
                final int stopIndex = startIndex + toRemove;
                this.removeMessages(this.rootNode, startIndex, stopIndex);
            }
        }

        /**
         * Call this method to set the short or long message format to be shown.
         * 
         * @param longMsg <code>true</code> to use the long message format
         */
        void setShowLongMsg(final boolean longMsg) {
            this.showLongMessage.set(longMsg);
            this.fireTreeNodesChanged(new TreeModelEvent(this, new MessageTableTreeNode[] {this.rootNode}));
        }

        /**
         * Call this method to set the maximum number of messages the table will contain.
         * 
         * @param newValue The maximum number of messages the table will contain
         */
        void setMaxRowNumber(final int newValue) {
            this.maxRowNum.set(newValue);
            this.checkTableSize();
        }
    }

    /**
     * Renderer used for the column showing the message severity.
     */
    private final static class SeverityColumnRenderer extends JLabel implements TableCellRenderer {
        private static final long serialVersionUID = -915375964733738521L;

        SeverityColumnRenderer() {
            super();
            this.setOpaque(true);
            this.setHorizontalAlignment(SwingConstants.CENTER);
            this.setVerticalAlignment(SwingConstants.CENTER);
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            final MessageSeverity msgSev = (MessageSeverity) value;

            this.setForeground(msgSev.getFgColor());
            this.setBackground(msgSev.getBgColor());
            this.setText(msgSev.name());

            return this;
        }
    }

    /**
     * Renderer used for columns whose class is {@link String} (APPLICATION and NAME columns).
     */
    private final static class StringColumnRenderer extends JLabel implements TableCellRenderer {
        private static final long serialVersionUID = 4758936112077482838L;

        StringColumnRenderer() {
            super();
            this.setOpaque(true);
            this.setHorizontalAlignment(SwingConstants.CENTER);
            this.setVerticalAlignment(SwingConstants.CENTER);
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            this.setText(value.toString());

            if(!isSelected) {
                // Get the message severity to set back and foreground colors
                final Object obj = table.getModel().getValueAt(row, 1);
                if(MessageSeverity.class.isInstance(obj)) {
                    final MessageSeverity sev = (MessageSeverity) obj;
                    this.setForeground(sev.getTxtColor());
                    this.setBackground(sev.getTxtBgColor());
                }
            } else {
                this.setForeground(table.getSelectionForeground());
                this.setBackground(table.getSelectionBackground());
            }

            return this;
        }
    }

    /**
     * Renderer used for column showing the message text. This is the only column supposed to be rendered using a multi-line renderer.
     */
    private final static class MessageColumnRenderer extends MultiLineTable.MultiLineCellRenderer {
        private static final long serialVersionUID = -6918186130321696372L;

        MessageColumnRenderer() {
            super();
            this.setFont(new Font("Dialog", Font.PLAIN, 12));
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            this.setText(value.toString());
            if(!isSelected) {
                // Get the message severity to set back and foreground colors
                final Object obj = table.getModel().getValueAt(row, 1);
                if(MessageSeverity.class.isInstance(obj)) {
                    final MessageSeverity msgSev = (MessageSeverity) obj;
                    this.setForeground(msgSev.getTxtColor());
                    this.setBackground(msgSev.getTxtBgColor());
                }
            } else {
                this.setForeground(table.getSelectionForeground());
                this.setBackground(table.getSelectionBackground());
            }

            return this;
        }
    }

    /**
     * {@link IssueReceiver} used to receive and process call-back coming from ERS (i.e., messages matching the subscription criteria).
     * <p>
     * As soon as the messages are received they are put in queue and then processed by a separate thread to be formatted properly and
     * inserted into the message table.
     * <p>
     * To start the message processing the {@link IssueReceiver#startMessageProcessing()} method had to be called and remember to call
     * {@link IssueReceiver#stopMessageProcessing()} at the end.
     */
    private final class IssueReceiver implements ers.IssueReceiver {
        /**
         * The queue used to store the received messages
         */
        private final BlockingQueue<ers.Issue> msgQueue = new LinkedBlockingQueue<ers.Issue>();

        /**
         * Single executor to process ERS Issues periodically
         */
        private final ScheduledThreadPoolExecutor msgProcesserExecutor = ExecutorFactory.createSingleScheduledThreadPoolExecutor("ERS-Message-Processing");

        /**
         * The thread processing the messages in the queue
         */
        private final ListenerTask listenerTask = new ListenerTask();

        /**
         * Thread accessing the queue where messages are stored, formatting and inserting them into the message table.
         */
        private final class ListenerTask implements Runnable {
            /**
             * Temporary buffer holding the messages drained from the {@link IssueReceiver#msgQueue}
             */
            private final List<ers.Issue> msgList = new ArrayList<ers.Issue>();

            /**
             * Constructor
             */
            ListenerTask() {
            }

            /**
             * It drains the messages from the queue to the {@link #msgList} and processes them.
             * 
             * @see {@link IssueReceiver#processMessages(List)}
             */
            @Override
            public void run() {
                try {
                    this.msgList.add(IssueReceiver.this.msgQueue.take());// Here it blocks if the queue is empty
                    IssueReceiver.this.msgQueue.drainTo(this.msgList);
                    IssueReceiver.this.processMessages(this.msgList);
                }
                catch(final InterruptedException ex) {
                    IguiLogger.warning("The ERS message processing task has been interruped");
                    Thread.currentThread().interrupt();
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected error while processing ERS messages: " + ex, ex);
                }
                finally {
                    // Clear the temporary buffer
                    this.msgList.clear();
                }
            }
        }

        /**
         * The received {@link Issue} is put into the message queue.
         * 
         * @see ers.IssueReceiverr#receive(ers.Issue)
         */
        @Override
        public void receive(final ers.Issue issue) {
            try {
                this.msgQueue.put(issue);
            }
            catch(final InterruptedException ex) {
                IguiLogger.warning(ex.toString(), ex);
                Thread.currentThread().interrupt();
            }
            catch(final NullPointerException ex) {
                final String errMsg = "Tried to insert a null element into the ERS message queue";
                IguiLogger.error(errMsg, ex);
            }
        }

        /**
         * The working thread is started
         */
        void startMessageProcessing() {
            this.msgProcesserExecutor.scheduleWithFixedDelay(this.listenerTask, 0, 300, TimeUnit.MILLISECONDS);
        }

        /**
         * The working thread is stopped
         */
        void stopMessageProcessing() {
            this.msgProcesserExecutor.shutdownNow();
        }

        /**
         * Messages are properly processed to be inserted into the message table.
         * 
         * @param messageList The list containing all the messages to process.
         * @see #createMessageNode(MessageEvent)
         */
        private void processMessages(final List<ers.Issue> messageList) {
            // Create an array which will contain all the messages to be added
            // into the table
            final int msgNum = messageList.size();
            final MessageTableTreeNode[] msgArray = new MessageTableTreeNode[msgNum];

            // Create all the nodes
            for(int i = 0; i < msgNum; ++i) {
                final MessageTableTreeNode msgNode = this.createMessageNode(messageList.get(i));
                msgArray[i] = msgNode;
            }

            // Use the table model to add nodes (messages) into the table
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    // The selection removing is a needed hack: it looks like
                    // the new inserted messages are selected if the first
                    // message in the table is selected (bug in JTable)
                    if(ErsPanel.this.messageTable.getRowCount() > 0) {
                        ErsPanel.this.messageTable.removeRowSelectionInterval(0, 0);
                    }
                    ErsPanel.this.tableTreeModel.addMessages(msgArray);
                }
            });
        }

        /**
         * It properly creates nodes to be added into the message table starting from an {@link Issue}.
         * 
         * @param msg The message to use to create a table node
         * @return The table node keeping the message parameters
         */
        private MessageTableTreeNode createMessageNode(final ers.Issue msg) {
            MessageTableTreeNode ersMsg = null;
            final DateFormat df = ErsPanel.timeFormat.get();

            try {
                // Create the node
                ersMsg = new MessageTableTreeNode(df.format(new Date(msg.ltime())),
                                                  MessageSeverity.severityFromErsSeverity(msg.severity()),
                                                  msg.context().application_name(),
                                                  msg.getId(),
                                                  msg.message(),
                                                  this.getMessageDetails(msg));

                // If we have another Issue as cause, then recurse
                ers.Issue cause = msg.cause();
                int i = 0;
                while(cause != null) {
                    final MessageTableTreeNode chainMsg = new MessageTableTreeNode(df.format(new Date(cause.ltime())),
                                                                                   MessageSeverity.severityFromErsSeverity(cause.severity()),
                                                                                   cause.context().application_name(),
                                                                                   cause.getId(),
                                                                                   cause.message(),
                                                                                   this.getMessageDetails(cause));

                    ersMsg.insert(chainMsg, i++);
                    cause = cause.cause();
                }
            }
            catch(final Exception ex) {
                final StringBuilder errStr = new StringBuilder("Error processing ERS message");
                errStr.append(": " + msg.message());
                errStr.append(". Got exception: " + ex.toString());

                IguiLogger.error(errStr.toString(), ex);

                // Create a custom message to show the error
                ersMsg = new MessageTableTreeNode(df.format(new Date()),
                                                  IguiConstants.MessageSeverity.ERROR,
                                                  ErsPanel.internalMessageAppName,
                                                  ErsPanel.internalMessageName,
                                                  errStr.toString(),
                                                  "");
            }
            return ersMsg;
        }

        /**
         * It returns a string with additional details about the message.
         * 
         * @param msg The {@link Issue} coming from the stream
         * @param pos The position of the message in the {@link Issue} (in case of chained messages)
         * @return A string with additional details about the message
         */
        private final String getMessageDetails(final ers.Issue msg) {
            final StringBuilder s = new StringBuilder();

            s.append("DATE: ");
            s.append(msg.time());

            s.append(" - PID: ");
            s.append(msg.context().process_id());

            s.append(" - TID: ");
            s.append(msg.context().thread_id());

            s.append(" - HOST: ");
            s.append(msg.context().host_name());

            s.append(" - USERID: ");
            s.append(msg.context().user_id());

            s.append(" - USERNAME: ");
            s.append(msg.context().user_name());

            s.append(" - CWD: ");
            s.append(msg.context().cwd());

            s.append(" - FILENAME: ");
            s.append(msg.context().file_name());

            s.append(" - LINE_NUMBER: ");
            s.append(msg.context().line_number());

            s.append(" - PACKAGE_NAME: ");
            s.append(msg.context().package_name());

            s.append(" - FUNCTION_NAME: ");
            s.append(msg.context().function_name());

            s.append(" - QUALIFIERS: ");
            final int count = msg.qualifiers().size();
            for(int i = 0; i < count; i++) {
                s.append(msg.qualifiers().get(i));
                s.append(" ");
            }

            if(!msg.parameters().isEmpty()) {
                s.append(" - PARAMETERS: ");
                for(final Map.Entry<String, String> entry : msg.parameters().entrySet()) {
                    s.append(entry.getKey() + "=" + entry.getValue() + " ");
                }
            }

            return s.toString();
        }
    }

    /**
     * Mouse listener for the message table: the right click mouse event will show a contextual menu allowing to copy to the clipboard the
     * content of the selected message.
     */
    private static class MouseListenerAdapter extends MouseAdapter {
        /**
         * Reference to the message table
         */
        private final WeakReference<MessageTable> tableRef;

        /**
         * The pop-up menu
         */
        private final JPopupMenu popUpMenu = new JPopupMenu();

        /**
         * The pop-up menu item to copy the message content to the clipboard
         */
        private final JMenuItem copyMenuItem = new JMenuItem("Copy to clipboard");

        /**
         * The pop-up menu item to remove the selected messages from the table
         */
        private final JMenuItem removeMenuItem = new JMenuItem("Remove selected message(s)");

        /**
         * Buffer holding the message to copy to the clipboard
         */
        private final StringBuilder messageToCopy = new StringBuilder();

        /**
         * The clipboard
         */
        private final Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();

        /**
         * Constructor.
         * 
         * @param table The table this listener should be added
         */
        MouseListenerAdapter(final MessageTable table) {
            super();
            this.tableRef = new WeakReference<MessageTable>(table);
            this.popUpMenu.add(this.copyMenuItem);
            this.copyMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    try {
                        // The 'messageToCopy' is filled when one of the mouse button is clicked
                        MouseListenerAdapter.this.copyToClipboard(MouseListenerAdapter.this.messageToCopy.toString());
                    }
                    catch(final Exception ex) {
                        final String errMsg = "Failed to copy the selected content to the clipboard: " + ex.getMessage();
                        IguiLogger.error(errMsg, ex);
                        // To show the error dialog do not use the ErrorFrame class because
                        // this is shared with the ErsMonitor (where there is no Igui...).
                        // Yes, it sucks! I know...
                        final ErrorInfo errorInfo = new ErrorInfo("ERS Panel Error", errMsg, null, null, ex, null, null);
                        JXErrorPane.showDialog(MouseListenerAdapter.this.popUpMenu, errorInfo);
                    }
                }
            });

            this.popUpMenu.add(this.removeMenuItem);
            this.removeMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final MessageTable t = MouseListenerAdapter.this.tableRef.get();
                    if(t != null) {
                        final JTree tree = t.getTree();
                        final TreePath tp[] = tree.getSelectionPaths();
                        if(tp != null) {
                            final MessageTableTreeNode[] nodesToRemove = new MessageTableTreeNode[tp.length];
                            for(int i = 0; i < tp.length; ++i) {
                                final MessageTableTreeNode child = (MessageTableTreeNode) tp[i].getLastPathComponent();
                                nodesToRemove[i] = child;
                            }

                            final MessageTableTreeModel tm = (MessageTableTreeModel) tree.getModel();
                            tm.removeMessages(nodesToRemove);
                        }
                    }
                }
            });
        }

        /**
         * The pop-up menu is shown and the {@link #messageToCopy} buffer is properly filled using the selected message information.
         */
        @Override
        public void mouseClicked(final MouseEvent e) {
            // When a mouse button is clicked, the buffer to be copied to the
            // clipboard
            // is always filled: this avoids any issue with incoming messages
            // potentially changing
            // the row selection. Moreover we want the buffer to be copied to
            // the clipboard
            // when the left mouse button is clicked. In case the right mouse
            // button is clicked, the
            // contextual dialog is shown.
            final MessageTable t = this.tableRef.get();
            if(t != null) {
                this.messageToCopy.delete(0, this.messageToCopy.length());
                int[] selectedRows = null;

                {
                    // Get the selected rows
                    final int[] rows = t.getSelectedRows();
                    if(rows.length > 0) {
                        selectedRows = rows;
                    } else {
                        // If no rows are selected try to get the row near the
                        // mouse pointer
                        final int r = t.rowAtPoint(new Point(e.getX(), e.getY()));
                        if(r != -1) {
                            t.getSelectionModel().setSelectionInterval(r, r);
                            selectedRows = new int[] {r};
                        }
                    }
                }

                if(selectedRows != null) {
                    // Get the contents of all the columns for all the selected
                    // rows and fill the 'messageToCopy' buffer
                    final TableModel tm = t.getModel();
                    final int colNum = tm.getColumnCount();
                    for(final int i : selectedRows) {
                        for(int j = 0; j < colNum; ++j) {
                            final Object obj = tm.getValueAt(i, j);
                            if(obj != null) {
                                this.messageToCopy.append(obj.toString());
                                this.messageToCopy.append(" ");
                            }
                        }
                        this.messageToCopy.append("\n");
                    }

                    if(javax.swing.SwingUtilities.isRightMouseButton(e) == true) {
                        // RMB: Now show the pop-up menu
                        this.popUpMenu.show(e.getComponent(), e.getX(), e.getY());
                    } else {
                        // Fill the buffer
                        try {
                            this.copyToClipboard(this.messageToCopy.toString());
                        }
                        catch(final IllegalStateException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        }

        /**
         * It copies the <code>content</code> string to the system clipboard.
         * 
         * @param content The string to copy to clipboard
         */
        private void copyToClipboard(final String content) {
            final StringSelection str = new StringSelection(content);
            this.clipBoard.setContents(str, str);
        }
    }

    /**
     * Simple class with methods associated to button actions.
     */
    private class ButtonActions {
        /**
         * Constructor.
         */
        ButtonActions() {
        }

        /**
         * Method called when the {@link ErsPanel#clearERSButton} button is clicked: it clears the message table.
         * 
         * @see MessageTableTreeModel#clearTable()
         */
        void clearTable() {
            ErsPanel.this.tableTreeModel.clearTable();
        }

        /**
         * Method called when one of the {@link ErsPanel#shortMessageButton} or {@link ErsPanel#longMessageButton} toggle button is
         * selected: it sets the message format to show in the table.
         * 
         * @param table The table to set the message format (needed because used by the {@link ErsMonitor} as weel)
         * @param showLong <code>true</code> if the long message format will be shown
         * @see MessageTableTreeModel#setShowLongMsg(boolean)
         */
        void setLongMessage(final MessageTable table, final boolean showLong) {
            final MessageTableTreeModel tModel;
            final MessageTable tb;
            final int[] selRow;

            if(table == null) {
                // Called from this panel
                tModel = ErsPanel.this.tableTreeModel;
                selRow = ErsPanel.this.messageTable.getSelectedRows();
                tb = ErsPanel.this.messageTable;
            } else {
                // Maybe called from the ErsMonitor
                tModel = (MessageTableTreeModel) table.getTree().getModel();
                selRow = table.getSelectedRows();
                tb = table;
            }

            tModel.setShowLongMsg(showLong);
            tb.repaint();

            // Try to keep the row selection after the change to the message
            // format
            if((selRow != null) && (selRow.length > 0)) {
                try {
                    // Here invokeLater is needed for the selection to work;
                    // this is due to the way events are handled by the
                    // JTreeTable.
                    // (tModel.setShowLongMsg(..) is generating such an event).
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                tb.setRowSelectionInterval(selRow[0], selRow[selRow.length - 1]);
                                tb.scrollRowToVisible(selRow[0]);
                            }
                            catch(final IllegalArgumentException ex) {
                                // This may happen because 'selRow'
                                // may not be in the valid range anymore
                                // (i.e., lines are removed because of the
                                // fixed buffer - number of visible messages)
                                IguiLogger.debug(ex.getMessage(), ex);
                            }
                        }
                    });
                }
                catch(final IllegalArgumentException ex) {
                    // Ignore
                }
            }
        }

        /**
         * It is called when an event from {@link ErsPanel#rowSpinner} is generated: it sets the maximum number of messages the table will
         * contain.
         * 
         * @param value The maximum number of messages in the table
         * @see MessageTableTreeModel#setMaxRowNumber(int)
         */
        void setMaxRowNumber(final int value) {
            ErsPanel.this.tableTreeModel.setMaxRowNumber(value);
        }

        /**
         * Method called when an event is originated by one of the check boxes dealing with subscription criteria.
         * 
         * @param source The source of the event
         */
        void checkBoxSelected(final JCheckBox source) {
            // Try to understand who is the source of the event
            if(ErsPanel.this.checkBoxesList.contains(source)) {
                // The source is one of the default criteria radio buttons
                // Deselect the radio button used for custom criteria
                ErsPanel.this.customCheckBox.setSelected(false);
            } else {
                // This means that the event source is the custom subscription
                // radio button
                // Deselect all the others
                for(final JCheckBox cb : ErsPanel.this.checkBoxesList) {
                    cb.setSelected(false);
                }
            }

            // Make the subscription text filed editable or not
            if(ErsPanel.this.customCheckBox.isSelected()) {
                ErsPanel.this.customSubscription.setEditable(true);
            } else {
                ErsPanel.this.customSubscription.setEditable(false);
            }
        }

        /**
         * Method called when the filter is enabled/disabled in the GUI. It always calls {@link #changeSubscription()}.
         * 
         * @param isButtonSelected
         */
        void toggleFilter(final boolean isButtonSelected) {
            ErsPanel.this.selectFilterButton(isButtonSelected, "");
            this.changeSubscription();
        }

        /**
         * Method called when the {@link ErsPanel#defaultFilterReloadButton} button is pressed.
         * <p>
         * It just invalidates the {@link ErsPanel#filterLoader}: it means that the filter will be calculated again next time the
         * {@link ErsPanel#filterLoader} is interrogated.
         * <p>
         * If the filter is enabled in the GUI, then the subscription is re-done so that the filter is re-calculated (see
         * {@link #changeSubscription()}).
         */
        void reloadFilter() {
            ErsPanel.this.filterLoader.invalidateFilter();
            if(ErsPanel.this.defaultFilterButton.isSelected() == true) {
                this.changeSubscription();
            }
        }

        /**
         * Method every time the subscription has to be modified. That can be when: the user pushes the {@link ErsPanel#subscriptionButton}
         * or the default filter is enabled or disabled.
         * <p>
         * The method calculates the subscription string taking into account the criteria specified by the user and the default filter.
         */
        void changeSubscription() {
            // The new subscription criteria (the ones specified by the user)
            final StringBuilder newUserCriteria = new StringBuilder();

            if(ErsPanel.this.customCheckBox.isSelected()) {
                // Use the custom subscription
                newUserCriteria.append(ErsPanel.this.customSubscription.getText());
            } else {
                // Look for which check boxes are selected and build the
                // subscription criteria string
                for(final JCheckBox cb : ErsPanel.this.checkBoxesList) {
                    if(cb.isSelected()) {
                        if(newUserCriteria.length() > 0) {
                            newUserCriteria.append(" or ");
                        }
                        newUserCriteria.append("sev=");
                        newUserCriteria.append(cb.getText());
                    }
                }
            }

            // Check the status of the toggle button to understand whether the
            // filter is enabled or not
            final boolean isFilterEnabled = ErsPanel.this.defaultFilterButton.isSelected();

            // Change the subscription using the executor
            ErsPanel.this.subscriptionExecutor.execute(new SwingWorker<Void, String>() {
                private final static String noSubscription = "NONE";

                @Override
                protected Void doInBackground() throws BadStreamConfiguration {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ErsPanel.this.subscriptionButton.setEnabled(false);
                        }
                    });

                    // Get the ERS filter
                    final String filterString;
                    if(isFilterEnabled == true) {
                        filterString = ErsPanel.this.getFilterString();
                    } else {
                        filterString = "";
                    }

                    // Remove the current subscription
                    final String currentSub = ErsPanel.this.ersFullSubscription.toString();
                    try {
                        StreamManager.instance().remove_receiver(ErsPanel.this.issueReceiver);
                        // Here we have no subscription
                        @SuppressWarnings("static-access")
                        final String noSub = this.noSubscription;
                        this.publish(noSub);
                        IguiLogger.info("Unsubscribed from stream " + ErsPanel.this.issueReceiver + " with expression \"" + currentSub
                                        + "\"");
                    }
                    catch(final ReceiverNotFound ex) {
                        IguiLogger.warning("Failed to unsubscribe from " + ErsPanel.this.issueReceiver + " stream with expression \""
                                           + currentSub + "\": " + ex, ex);
                    }

                    // Perform a new subscription: be aware that buildFullSubString may throw IllegalArgumentException
                    final String newUserCriteriaString = newUserCriteria.toString();
                    final String newSub = ErsPanel.this.buildFullSubString(filterString, newUserCriteriaString);

                    StreamManager.instance().add_receiver(ErsPanel.this.issueReceiver,
                                                          ErsPanel.this.stream,
                                                          ErsPanel.this.partitionName,
                                                          newSub); // The new subscription is done

                    // In the panel show only the user criteria and not the filer
                    this.publish(newUserCriteriaString);
                    IguiLogger.info("Subscribed to " + ErsPanel.this.stream + " with expression \"" + newSub + "\"");

                    // At this point we can cache the subscription criteria
                    ErsPanel.this.ersUserCriteria.replace(0, Integer.MAX_VALUE, newUserCriteria.toString());
                    ErsPanel.this.ersFullSubscription.replace(0, Integer.MAX_VALUE, newSub);

                    return null;
                }

                /**
                 * It writes the current subscription to the text field at the bottom right corner of the GUI.
                 */
                @Override
                protected void process(final List<String> chunks) {
                    for(final String str : chunks) {
                        ErsPanel.this.currentSubscriptionText.setText(str);
                        @SuppressWarnings("static-access")
                        final String noSub = this.noSubscription;
                        if(str.equals(noSub)) {
                            ErsPanel.this.currentSubscriptionText.setBackground(Color.RED);
                        } else {
                            ErsPanel.this.currentSubscriptionText.setBackground(null);
                        }
                    }
                }

                /**
                 * Just error checking
                 */
                @Override
                protected void done() {
                    try {
                        this.get();
                        ErsPanel.this.addInternalMessage(IguiConstants.MessageSeverity.INFORMATION, "ERS subscription done");
                    }
                    catch(final InterruptedException ex) {
                        IguiLogger.warning(ex.toString(), ex);
                        Thread.currentThread().interrupt();
                    }
                    catch(final ExecutionException ex) {
                        final String msg;
                        final Throwable cause = ex.getCause();
                        if(cause instanceof IllegalArgumentException) {
                            msg = "Failed to change the " + ErsPanel.this.stream + " subscription: " + cause.toString();
                        } else {
                            msg = "Failed to change the " + ErsPanel.this.stream + " subscription: " + ex.toString()
                                  + ". Modify your subscription criteria or try to anable/disable the default filter (if available).";
                        }
                        IguiLogger.error(msg, ex);
                        ErsPanel.this.showErrorDialog("IGUI - ERS Panel Error", msg, ex);
                    }
                    finally {
                        // Enable the button again
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                ErsPanel.this.subscriptionButton.setEnabled(true);
                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * Constructor called by the Igui: this is not a tabbed panel.
     * 
     * @param tabbed <code>false</code> if the panel is not a tabbed one
     */
    public ErsPanel(final boolean tabbed) {
        this(tabbed, Igui.instance().getPartition().getName());
    }

    /**
     * Constructor used for the {@link ErsMonitor}.
     * 
     * @param partitionName The name of the partition
     */
    ErsPanel(final String partitionName) {
        this(false, partitionName);
    }

    /**
     * Constructor.
     * 
     * @param tabbed <code>false</code> if the panel is not a tabbed one
     * @param partitionName The name of the partition
     */
    private ErsPanel(final boolean tabbed, final String partitionName) {
        super(tabbed);

        this.partitionName = partitionName;

        // Determine a default criteria for ERS
        final String defCrit = IguiConfiguration.instance().getDefaultErsSubscription();
        if((defCrit != null) && (defCrit.isEmpty() == false)) {
            this.ersUserCriteria.append(defCrit);
        } else {
            this.ersUserCriteria.append(ErsPanel.defaultSimpleSubscription);
        }

        // Add a message to the root node
        this.rootMessageNode = new MessageTableTreeNode("", MessageSeverity.LOG, "", "", "", "");
        this.rootMessageNode.add(new MessageTableTreeNode(ErsPanel.timeFormat.get().format(new Date()),
                                                          MessageSeverity.INFORMATION,
                                                          ErsPanel.internalMessageAppName,
                                                          ErsPanel.internalMessageName,
                                                          "ERS message panel created",
                                                          ""));

        this.tableTreeModel = new MessageTableTreeModel(this.rootMessageNode, this.defaultMaxRows.intValue());

        this.messageTable = new MessageTable(this.tableTreeModel);

        this.issueReceiver = new IssueReceiver();
        IguiLogger.debug("Created IssueReceiver");

        this.initGUI();
    }

    /**
     * @see Igui.IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return ErsPanel.panelName;
    }

    /**
     * @see Igui.IguiPanel#getTabName()
     */
    @Override
    public String getTabName() {
        return ErsPanel.panelName;
    }

    /**
     * Here the panel is initialized:
     * <ul>
     * <li>The worker processing the received messages is started (@see {@link ERSListener#startMessageProcessing()});
     * <li>The subscription to the ERS server is performed (since the ERS server may not be up yet when the Igui is started, the
     * subscription is done in a separate thread looping as soon as the ERS server is up and running).
     * </ul>
     * 
     * @param standAlone <code>true</code> if called by the {@link ErsMonitor}
     */
    void panelInit(final boolean standAlone) {
        // Start the thread processing the received messages
        this.issueReceiver.startMessageProcessing();

        // Get the default filter
        final String ersDefaultFilter;
        if(this.filterLoader.getFile().isEmpty() == false) {
            ersDefaultFilter = this.getFilterString();
        } else {
            ersDefaultFilter = "";

            // The file for the default filter is not specified: disable the
            // filter button
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ErsPanel.this.defaultFilterButton.setEnabled(false);
                    ErsPanel.this.defaultFilterReloadButton.setEnabled(false);
                    ErsPanel.this.defaultFilterButton.setToolTipText("No default " + ErsPanel.this.stream + " filters available");
                }
            });
        }

        // The real subscription string is a logical combination of the
        // subscription and filter criteria
        // Store the full subscription string in the class variable
        // NOTE: "buildFullSubString" will throw an exception if the ERS criteria is empty; that should
        // never be true at this stage. If it happens then it is a BUG!!!
        this.ersFullSubscription.append(this.buildFullSubString(ersDefaultFilter, this.ersUserCriteria.toString()));

        // Start a thread trying to subscribe to ERS: the MTS server may not yet be up when the panel is created
        // Use the subscription executor for consistency
        // (you do not want to (un)subscribe in the while, and the GUI is already shown!)

        this.subscriptionExecutor.execute(new Runnable() {
            @Override
            public void run() {
                final String subStr = ErsPanel.this.ersFullSubscription.toString();

                boolean stop = false;
                int counter = 0;
                while(stop == false) {
                    try {
                        // Try to subscribe
                        StreamManager.instance().add_receiver(ErsPanel.this.issueReceiver,
                                                              ErsPanel.this.stream,
                                                              ErsPanel.this.partitionName,
                                                              subStr);

                        {
                            final String msg = "Subscribed to the " + ErsPanel.this.stream + " server";
                            IguiLogger.info(msg + ". Subscription string is: " + subStr);
                            ErsPanel.this.addInternalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                        }

                        // Show the current subscription
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                ErsPanel.this.currentSubscriptionText.setText(ErsPanel.this.ersUserCriteria.toString());
                                ErsPanel.this.currentSubscriptionText.setBackground(null);
                            }
                        });

                        // Stop the loop
                        stop = true;
                    }
                    catch(final ers.BadStreamConfiguration ex) {
                        final String errMsg = "Failed to add receiver for stream: " + ErsPanel.this.stream + " subCriteria: " + subStr
                                              + "\n msg: " + ex.getMessage();
                        ErsPanel.this.addInternalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);

                        final Throwable c = ex.getCause();
                        if(mts.BadExpression.class.isInstance(c) == true) {
                            IguiLogger.error(errMsg, ex);
                            stop = true;

                            // Show the current subscription
                            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    ErsPanel.this.currentSubscriptionText.setText("NONE");
                                    ErsPanel.this.currentSubscriptionText.setBackground(Color.RED);
                                }
                            });

                            {
                                final String diagMsg = "Bad ERS subscription: "
                                                       + "\""
                                                       + subStr
                                                       + "\""
                                                       + ".\nModify it (or disable any default filter if present) and try to subscribe again";
                                ErsPanel.this.showErrorDialog("IGUI - ERS Panel Error", diagMsg, ex);
                            }
                        }
                    }
                    finally {
                        // Show a message sometimes
                        final int times = ++counter;
                        if((times % 50) == 0) {
                            ErsPanel.this.addInternalMessage(IguiConstants.MessageSeverity.ERROR,
                                                             "Still trying to subscribe to the " + ErsPanel.this.stream
                                                                 + " server... Look at the log file for details.");
                        }
                        try {
                            Thread.sleep(200);
                        }
                        catch(final InterruptedException ex) {
                            // Thread interrupted, stop the loop
                            final String errMsg = "The " + ErsPanel.this.stream + " subscriber thread has been interrupted";
                            IguiLogger.warning(errMsg, ex);
                            stop = true;
                            Thread.currentThread().interrupt();
                            continue;
                        }
                    }
                }
            }
        });
    }

    /**
     * It calls {@link #panelInit(boolean)} with <code>false</code> as argument.
     * 
     * @see Igui.IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void panelInit(final State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException {
        try {
            this.panelInit(false);
        }
        catch(final Exception ex) {
            throw new IguiException.IguiPanelError(ErsPanel.panelName, ex);
        }
    }

    /**
     * It removes the subscription from the current stream
     * 
     * @see Igui.IguiPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        // Unsubscribe from stream
        try {
            this.subscriptionExecutor.submit(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    StreamManager.instance().remove_receiver(ErsPanel.this.issueReceiver);
                    IguiLogger.info("Unsubscribed from " + ErsPanel.this.stream + " stream");
                    return null;
                }
            }).get(ErsPanel.TIMEOUT, TimeUnit.SECONDS);
        }
        catch(final InterruptedException ex) {
            IguiLogger.warning("Thread interrupted");
            Thread.currentThread().interrupt();
        }
        catch(final ExecutionException ex) {
            IguiLogger.warning("Failed removing " + ErsPanel.this.stream + " subscription: " + ex.getMessage(), ex.getCause());
        }
        catch(final TimeoutException ex) {
            IguiLogger.warning("Timeout elapsed unsubscribing from the server", ex);
        } // Do not wait forever...

        // Stop message processing
        this.issueReceiver.stopMessageProcessing();

        IguiLogger.debug("Panel \"" + this.getPanelName() + "\" has been terminated");
    }

    /**
     * This panel is not RC state aware.
     * 
     * @see Igui.IguiPanel#rcStateAware()
     */
    @Override
    public boolean rcStateAware() {
        return false;
    }

    /**
     * It shows an error dialog.
     * <p>
     * Needed so that the {@link ErsMonitor} can overload it and show error dialogs without using the {@link ErrorFrame} class.
     * 
     * @param msg The error message
     */
    void showErrorDialog(final String msg) {
        ErrorFrame.showError(msg);
    }

    /**
     * It shows an error dialog.
     * <p>
     * Needed so that the {@link ErsMonitor} can overload it and show error dialogs without using the {@link ErrorFrame} class.
     * 
     * @param title The dialog title
     * @param msg The error message
     * @param ex The exception associated to the error message
     */
    void showErrorDialog(final String title, final String msg, final Throwable ex) {
        ErrorFrame.showError(title, msg, ex);
    }

    /**
     * Convenience method used to add to the table messages produced by the Igui itself.
     * <p>
     * The message application name will be {@link #internalMessageAppName} and the message name will be {@link #internalMessageName}.
     * 
     * @param severity The message severity
     * @param msgText The message text
     * @see Igui#internalMessage(MessageSeverity, String)
     */
    void addInternalMessage(final MessageSeverity severity, final String msgText) {
        final MessageTableTreeNode msgNode = new MessageTableTreeNode(ErsPanel.timeFormat.get().format(new Date()),
                                                                      severity,
                                                                      ErsPanel.internalMessageAppName,
                                                                      ErsPanel.internalMessageName,
                                                                      msgText,
                                                                      "");
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // The selection removing is a needed hack: it looks like
                // the new inserted messages are selected if the first
                // message in the table is selected (bug in JTable)
                if(ErsPanel.this.messageTable.getRowCount() > 0) {
                    ErsPanel.this.messageTable.removeRowSelectionInterval(0, 0);
                }
                ErsPanel.this.tableTreeModel.addMessage(msgNode);
            }
        });
    }

    /**
     * It changes the ERS subscription from a partition to another one (used by the {@link ErsMonitor}).
     * <p>
     * The computation is performed in the {@link #subscriptionExecutor} executor. The {@link Future} returned by this method can be used to
     * get the result.
     * 
     * @param partName The partition where the ERS is running
     * @param unsubscribe <code>true</code> if the previous subscription has to be removed
     * @return A {@link Future} to retrieve the result of the computation
     */
    Future<?> changePartitionSubscription(final String partName, final boolean unsubscribe) {
        return this.subscriptionExecutor.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.ERSException {
                ErsPanel.this.partitionName = partName;

                try {
                    // Remove the subscription
                    if(unsubscribe == true) {
                        final String currentSub = ErsPanel.this.ersFullSubscription.toString();
                        try {
                            StreamManager.instance().remove_receiver(ErsPanel.this.issueReceiver);
                            IguiLogger.info("Unsubscribed from stream " + ErsPanel.this.issueReceiver + "  with expression \"" + currentSub
                                            + "\"");
                        }
                        catch(final ReceiverNotFound ex) {
                            IguiLogger.warning("Failed to unsubscribe from " + ErsPanel.this.issueReceiver + " stream with expression \""
                                               + currentSub + "\": " + ex, ex);
                        }
                    }

                    // Add an internal message (this is also a trick to not have messages from the previous partition
                    // when the table is cleared - remember that the first message is not removed)
                    ErsPanel.this.addInternalMessage(IguiConstants.MessageSeverity.INFORMATION, "Changing subscription to partition \""
                                                                                                + partName + "\"");

                    // Clear the table
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ErsPanel.this.tableTreeModel.clearTable();
                        }
                    });

                    // We need to check again the default filter because the partition has changed
                    final String filter = ErsPanel.this.getFilterString();

                    // Build the full subscription string
                    final String newSub = ErsPanel.this.buildFullSubString(filter, ErsPanel.this.ersUserCriteria.toString());

                    // Make a new subscription
                    try {
                        StreamManager.instance().add_receiver(ErsPanel.this.issueReceiver, ErsPanel.this.stream, partName, newSub);
                    }
                    catch(final BadStreamConfiguration ex) {
                        final String errMsg = "Error  when subscribing to stream \"" + ErsPanel.this.stream + "\" with criteria \""
                                              + newSub + "\"";

                        ErsPanel.this.addInternalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                        IguiLogger.error(errMsg + ": " + ex, ex);

                        throw new IguiException.ERSException(ex);
                    }

                    ErsPanel.this.ersFullSubscription.replace(0, Integer.MAX_VALUE, newSub);
                }
                catch(final Exception ex) {
                    throw new IguiException.ERSException(ex);
                }

                return null;
            }
        });
    }

    /**
     * It clones the message table showing it in a new frame.
     * <p>
     * To be called in the EDT.
     * 
     * @param title The title of the frame which will contain the "clone" table
     * @return The frame containing the "clone" tabled
     * @see MessageTable#clone()
     */
    JFrame cloneTable(final String title) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        final MessageTable cloneTable = this.messageTable.clone();
        final boolean showingLong = this.tableTreeModel.longMessages();

        final JFrame fr = new JFrame(title);
        fr.setLayout(new BorderLayout());
        fr.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        fr.addWindowListener(new WindowAdapter() {
            // When the window is closed remove everything from the frame and
            // clear the table:
            // we do not wat references hanging somewhere
            @Override
            public void windowClosing(final WindowEvent e) {
                fr.removeAll();
                ((MessageTableTreeModel) cloneTable.getTree().getModel()).clearTable();
            }
        });

        fr.add(new JScrollPane(cloneTable), BorderLayout.CENTER);

        // Add buttons for short/long message format selection
        final JToggleButton l = new JToggleButton("Long");
        l.setIcon(Igui.createIcon(ErsPanel.longMessageIconName));
        l.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.setLongMessage(cloneTable, true);
            }
        });

        final JToggleButton s = new JToggleButton("Short");
        s.setIcon(Igui.createIcon(ErsPanel.shortMessageIconName));
        s.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.setLongMessage(cloneTable, false);
            }
        });

        if(showingLong == true) {
            l.setSelected(true);
        } else {
            s.setSelected(true);
        }

        final ButtonGroup bg = new ButtonGroup();
        bg.add(s);
        bg.add(l);

        final JPanel p = new JPanel(new FlowLayout());
        p.add(s);
        p.add(l);

        fr.add(p, BorderLayout.SOUTH);

        fr.setSize(new Dimension(900, 600));
        fr.pack();
        fr.setVisible(true);

        return fr;
    }

    /**
     * It builds the subscription string taking into account the default filter and the user criteria (they are combined in a logical AND).
     * 
     * @param filter The default filter
     * @param criteria The user criteria
     * @return The full subscription string
     * @throws IllegalArgumentException Thrown when <code>criteria</code> is empty
     */
    private String buildFullSubString(final String filter, final String criteria) throws IllegalArgumentException {
        if(criteria.isEmpty() == true) {
            throw new IllegalArgumentException("criteria for stream " + ErsPanel.this.stream + " cannot be empty");
        }

        final String sub;
        if(filter.isEmpty() == true) {
            sub = criteria;
        } else {
            final StringBuilder str = new StringBuilder();
            str.append("(");
            str.append(filter);
            str.append(") and (");
            str.append(criteria);
            str.append(")");

            sub = str.toString();
        }

        return sub;
    }

    /**
     * It enables/disabled the {@link #defaultFilterButton} and {@link #defaultFilterReloadButton} buttons.
     * <p>
     * The aim is to have the reload button enabled only if the filter is active.
     * <p>
     * To be always executed in the EDT.
     * 
     * @param select <code>TRUE</code> if the filter is enabled (i.e., the toggle button is selected).
     * @param toolTip The text to show in the tool-tip
     */
    private void selectFilterButton(final boolean select, final String toolTip) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        this.defaultFilterButton.setSelected(select);
        this.defaultFilterReloadButton.setEnabled(select);

        if(select == true) {
            this.defaultFilterButton.setToolTipText("The ERS filter is enabled. Click to disable it. " + toolTip);
            this.defaultFilterButton.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.red, 3),
                                                                                  BorderFactory.createEmptyBorder(2, 2, 2, 2)));
        } else {
            this.defaultFilterButton.setToolTipText("The ERS filter is NOT enabled. Click to enable it. " + toolTip);
            this.defaultFilterButton.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        }
    }

    /**
     * It returns a string representing the default ERS filter to be applied.
     * 
     * @return A string representing the default ERS filter to be applied
     */
    private String getFilterString() {
        final StringBuilder filterString = new StringBuilder();
        try {
            filterString.append(ErsPanel.this.filterLoader.getSubscriptionString(ErsPanel.this.partitionName));
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ErsPanel.this.selectFilterButton(true, "The current filter is:\n" + filterString.toString());
                }
            });
        }
        catch(final ConfigException ex) {
            final String errMsg = "Cannot build the ERS default filter, only user criteria will be used: " + ex;
            IguiLogger.error(errMsg, ex);
            ErsPanel.this.addInternalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ErsPanel.this.selectFilterButton(false, "");
                }
            });
        }

        return filterString.toString();
    }

    /**
     * It build this panel GUI.
     */
    private void initGUI() {
        this.setLayout(new BorderLayout());

        // Radio buttons
        final JCheckBox warningCheckBox = new JCheckBox(IguiConstants.MessageSeverity.WARNING.name());
        final JCheckBox errorCheckBox = new JCheckBox(IguiConstants.MessageSeverity.ERROR.name());
        final JCheckBox fatalCheckBox = new JCheckBox(IguiConstants.MessageSeverity.FATAL.name());
        final JCheckBox infoCheckBox = new JCheckBox(IguiConstants.MessageSeverity.INFORMATION.name(), false);
        Collections.addAll(this.checkBoxesList, warningCheckBox, errorCheckBox, fatalCheckBox, infoCheckBox);

        if(this.ersUserCriteria.toString().equals(ErsPanel.defaultSimpleSubscription) == true) {
            warningCheckBox.setSelected(true);
            errorCheckBox.setSelected(true);
            fatalCheckBox.setSelected(true);
            this.customCheckBox.setSelected(false);
        } else {
            warningCheckBox.setSelected(false);
            errorCheckBox.setSelected(false);
            fatalCheckBox.setSelected(false);
            this.customCheckBox.setSelected(true);
            this.customSubscription.setText(this.ersUserCriteria.toString());
        }

        // Register spinner listener
        this.rowSpinner.setPreferredSize(new Dimension(80, 10));
        this.rowSpinnerModel.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(final ChangeEvent e) {
                final int value = ErsPanel.this.rowSpinnerModel.getNumber().intValue();
                ErsPanel.this.bActions.setMaxRowNumber(value);
            }
        });

        // Buttons
        for(final JCheckBox cb : this.checkBoxesList) {
            cb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final JCheckBox source = (JCheckBox) e.getSource();
                    ErsPanel.this.bActions.checkBoxSelected(source);
                }
            });
        }

        this.customCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final JCheckBox source = (JCheckBox) e.getSource();
                ErsPanel.this.bActions.checkBoxSelected(source);
            }
        });

        this.subscriptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.changeSubscription();
            }
        });

        final ButtonGroup bGroup = new ButtonGroup();
        bGroup.add(this.shortMessageButton);
        bGroup.add(this.longMessageButton);

        this.shortMessageButton.setSelected(true);
        this.shortMessageButton.setIcon(Igui.createIcon(ErsPanel.shortMessageIconName));
        this.shortMessageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.setLongMessage(null, false);
            }
        });

        this.longMessageButton.setIcon(Igui.createIcon(ErsPanel.longMessageIconName));
        this.longMessageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.setLongMessage(null, true);
            }
        });

        this.clearERSButton.setIcon(Igui.createIcon(ErsPanel.clearERSIconName));
        this.clearERSButton.setVerticalTextPosition(SwingConstants.CENTER);
        this.clearERSButton.setHorizontalTextPosition(SwingConstants.LEFT);
        this.clearERSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.clearTable();
            }
        });

        this.defaultFilterButton.setIcon(Igui.createIcon(ErsPanel.defaultFilterIconName));
        this.defaultFilterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.toggleFilter(ErsPanel.this.defaultFilterButton.isSelected());
            }
        });

        this.defaultFilterReloadButton.setIcon(Igui.createIcon(ErsPanel.defaultFilterReloadIconName));
        this.defaultFilterReloadButton.setToolTipText("Reload the default " + ErsPanel.this.stream + " filter");
        this.defaultFilterReloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ErsPanel.this.bActions.reloadFilter();
            }
        });

        // Top toolbar
        final JToolBar topToolBar = new JToolBar();
        topToolBar.add(this.defaultFilterReloadButton);
        topToolBar.add(this.defaultFilterButton);
        topToolBar.addSeparator();
        topToolBar.add(new JLabel("Subscription criteria"));
        topToolBar.addSeparator();
        for(final JCheckBox cb : this.checkBoxesList) {
            topToolBar.add(cb);
        }

        topToolBar.add(this.customCheckBox);
        topToolBar.add(this.customSubscription);
        topToolBar.addSeparator();
        topToolBar.add(this.subscriptionButton);
        topToolBar.setFloatable(false);

        // Bottom toolbar
        final JToolBar bottomToolBar = new JToolBar();
        bottomToolBar.add(this.clearERSButton);
        bottomToolBar.addSeparator();
        bottomToolBar.add(new JLabel("Message format"));
        bottomToolBar.addSeparator();
        bottomToolBar.add(this.shortMessageButton);
        bottomToolBar.add(this.longMessageButton);
        bottomToolBar.addSeparator();
        bottomToolBar.add(new JLabel("Visible rows"));
        bottomToolBar.addSeparator();
        bottomToolBar.add(this.rowSpinner);
        bottomToolBar.addSeparator();
        bottomToolBar.add(new JLabel("Current ERS subscription"));
        bottomToolBar.addSeparator();
        this.currentSubscriptionText.setEditable(false);
        this.currentSubscriptionText.setHorizontalAlignment(SwingConstants.CENTER);
        this.currentSubscriptionText.setFont(new Font("Dialog", Font.BOLD, 12));
        bottomToolBar.add(this.currentSubscriptionText);
        bottomToolBar.setFloatable(false);

        final JScrollPane scrPanel = new JScrollPane(this.messageTable);
        scrPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        // Disable the custom subscription text field if needed
        if(!this.customCheckBox.isSelected()) {
            this.customSubscription.setEditable(false);
        }

        this.customSubscription.setToolTipText("<html>Examples of valid selection criteria:<br>" + 
                                               "(sev=ERROR or sev=FATAL) or (app=Tile* and not qual=debug)<br>" + 
                                               "msg=HLTMPP* or sev=fatal or param(err_code)=123 or (qual!=DEBUG and context(package)!=TestManager)</html>");
        
        this.add(topToolBar, BorderLayout.NORTH);
        this.add(scrPanel, BorderLayout.CENTER);
        this.add(bottomToolBar, BorderLayout.SOUTH);
    }
}
