package Igui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import Igui.IguiException.UnknownState;
import Igui.IguiException.UnknownStatus;
import Igui.RunControlFSM.ApplicationStatus;
import Igui.RunControlFSM.State;
import config.ConfigException;


/**
 * Class storing information of all the applications showed in the Run Control tree: it contains a "static" part with information retrieved
 * from the configuration database and a dynamic "one" which can be regularly updated via IS.
 * <p>
 * All the methods defined in this class are thread safe. In particular a lock-less thread safe algorithm has been developed to update the
 * node information.
 * 
 * @see #updateInfo(rc.DAQApplicationInfo, boolean)
 * @see #updateInfo(rc.RCStateInfo, boolean)
 */
final class RunControlApplicationData {
    // Static information: taken from configuration database
    // All the fields are final (immutable) strings and can be shared by various threads without any external synchronization

    /**
     * The application name
     */
    private final String name;

    /**
     * The name of the segment the application belongs to
     */
    private final String segmentName;

    /**
     * The name of the (primary) host the application is configured to be running on
     */
    private final String host;
    
    /**
     * Parameters the application has been started with
     */
    private final AtomicReference<String> parameters = new AtomicReference<String>(null);

    /**
     * Application restart parameters
     */
    private final AtomicReference<String> restartParameters = new AtomicReference<String>(null);

    /**
     * Action taken if the application exits unexpectedly
     */
    private final String ifExitUnexpected;

    /**
     * Action taken if the application fails
     */
    private final String ifFailedToStart;

    /**
     * Action taken if the application is in error
     */
    private final String ifError;

    /**
     * When the application should be started
     */
    private final String startAt;

    /**
     * When the application should be terminated
     */
    private final String stopAt;

    /**
     * If the application can exit spontaneously
     */
    private final boolean canExit;

    /**
     * The directory the application should be started in
     */
    private final String startIn;

    /**
     * Application initialization timeout
     */
    private final int initTimeout;

    /**
     * The application action timeout
     */
    private final int actionTimeout;

    /**
     * <code>true</code> if the application is a segment controller
     */
    private final boolean isController;

    /**
     * <code>true</code> if the application is the controller of a TTC partition
     */
    private final boolean isTTCPartitionController;

    /**
     * <code>true</code> if the application can be restarted in the RUNNING state
     */
    private final boolean restartableInRunning;

    /**
     * <code>true</code> if the application implements the RunControlApplication interface
     */
    private final boolean isRunControlApplication;

    /**
     * The application class name from the OKS database
     */
    private final String appClassName;

    /**
     * List of infrastructure applications belonging to a given a segment
     */
    private final List<String> infrApps = new ArrayList<>();

    // Dynamic information (i.e., it can be updated by IS call-backs)
    // The IS application status is considered an invariant (it defines the current application status).
    // The 'appInfo' object keeps that information and the application status is changed via atomic updates of it.

    /**
     * Atomic reference to class holding node description coming from IS
     */
    private final AtomicReference<ApplicationISStatus> appInfo =
                                                               new AtomicReference<ApplicationISStatus>(new ApplicationISStatus(new ApplicationISStatus.RCInfo(),
                                                                                                                                new ApplicationISStatus.SupervisorInfo()));

    /**
     * Default integer value used for non RC applications
     */
    private final static int defaultInt = -999;

    /**
     * The {@link dal.BaseApplication} object holding application's description
     */
    private final dal.BaseApplication appConfig;

    /**
     * An instance of this class holds a "snapshot" of the current status of the application defined in the IS server.
     * <p>
     * This class is immutable and any instance of it (if properly constructed) can be published and shared amongst various threads without
     * additional locking.
     * <p>
     * The class contains two references to other two immutable classes holding two different kinds of IS information: RCStateInfo and
     * SupervisorInfo.
     */
    static final class ApplicationISStatus {
        private final RCInfo rcInfo_;
        private final SupervisorInfo supInfo_;

        /**
         * Info coming from RunCtrl.<name> IS server
         */
        private final static class RCInfo {
            /**
             * Application FSM state
             */
            private final RunControlFSM.State state;

            /**
             * Application busy state
             */
            private final boolean busy;

            /**
             * Application transitioning state
             */
            private final boolean transitioning;

            /**
             * Current transition command being executed by this application
             */
            private final String currentTransitionCmd;

            /**
             * Application fault state
             */
            private final boolean fault;

            /**
             * Application error description
             */
            private final String[] error;

            /**
             * Bad children
             */
            private final Set<String> badChildren = new HashSet<>();

            /**
             * Time stamp of the last information update from IS
             */
            private final long infoTime;

            /**
             * The last transition performed by the application
             */
            private final String lastTransitionCmd;
            
            /**
             * Constructor.
             */
            private RCInfo() {
                this(0);
            }

            /**
             * Constructor.
             * <p>
             * Sets default values for all the fields:
             * <ul>
             * <li>State = ABSENT;
             * <li>Busy = FREE;
             * <li>Fault = NONE;
             * <li>Current transition command = empty string;
             * <li>Last transition command = empty string;
             * <li>Error = empty string array.
             * </ul>
             * 
             * @param time Time stamp associated to the IS information
             */

            private RCInfo(final long time) {
                this.state = RunControlFSM.State.ABSENT;
                this.busy = false;
                this.transitioning = false;
                this.fault = false;
                this.currentTransitionCmd = "";
                this.error = new String[] {};
                this.infoTime = time;
                this.lastTransitionCmd = "";
            }

            /**
             * Constructor.
             * <p>
             * Class fields are initialized using IS information.
             * 
             * @param info The IS information
             * @throws UnknownState The application state cannot be recognized
             * @see RunControlFSM#getStateFromString(String)
             */
            private RCInfo(final rc.RCStateInfo info) throws UnknownState {
                this.state = RunControlFSM.getStateFromString(info.state);
                this.busy = info.busy;
                this.transitioning = info.transitioning;
                this.fault = info.fault;
                this.currentTransitionCmd = info.currentTransitionName;
                this.error = info.errorReasons;
                Collections.addAll(this.badChildren, info.badChildren);
                this.infoTime = info.getTime().getTimeMicro();
                this.lastTransitionCmd = info.lastTransitionName;
            }
        }

        /**
         * Info coming from RunCtrl.Supervisor.<name> IS server
         */
        private final static class SupervisorInfo {
            /**
             * The application status
             */
            private final RunControlFSM.ApplicationStatus status;

            /**
             * The application membership
             */
            private final boolean membership;

            /**
             * The host the application is actually running on (may be different than the primary host defined in configuration)
             */
            private final String runningHost;

            /**
             * Time when information was updated the last time
             */
            private final long infoTime;
            
            /**
             * Constructor.
             */
            private SupervisorInfo() {
                this(0);
            }

            /**
             * Constructor.
             * <p>
             * It sets all the fields to their default values:
             * <ul>
             * <li>Status = ABSENT;
             * <li>Membership = IN;
             * </ul>
             */
            private SupervisorInfo(final long time) {
                this.status = RunControlFSM.ApplicationStatus.ABSENT;
                this.membership = true;
                this.runningHost = "";
                this.infoTime = time;
            }

            /**
             * Constructor.
             * <p>
             * It initializes the class members using the IS information.
             * 
             * @param info The IS information
             * @throws UnknownStatus The application status cannot be recognized
             * @throws IllegalArgumentException The application membership cannot be recognized
             * @see RunControlFSM#getStatusFromString(String)
             * @see Enum#valueOf(Class, String)
             */
            private SupervisorInfo(final rc.DAQApplicationInfo info) throws UnknownStatus, IllegalArgumentException {
                this.status = RunControlFSM.getStatusFromString(info.status);
                this.membership = info.membership;
                this.runningHost = info.host;
                this.infoTime = info.getTime().getTimeMicro();
            }
        }

        /**
         * Constructor.
         * 
         * @param rc Application RCState information
         * @param sup Application supervisor information
         */
        private ApplicationISStatus(final RCInfo rc, final SupervisorInfo sup) {
            this.rcInfo_ = rc;
            this.supInfo_ = sup;
        }

        // Followings are all delegate 'getters'

        /**
         * Gets the name of the host the application is actually running on
         * 
         * @return The name of the host the application is actually running on
         */
        public String getRunningHost() {
            return this.supInfo_.runningHost;
        }
        
        /**
         * Gets the application state.
         * 
         * @return The application state
         */
        public State getState() {
            return this.rcInfo_.state;
        }

        /**
         * Gets the application status.
         * 
         * @return The application status
         */
        public ApplicationStatus getStatus() {
            return this.supInfo_.status;
        }

        /**
         * Gets the application error description.
         * 
         * @return The application error description
         */
        public String getError() {
            final StringBuilder sb = new StringBuilder();

            if(this.rcInfo_.fault == true) {
                for(final String s : this.rcInfo_.error) {
                    sb.append(s + "\n");
                }
            }

            return sb.toString();
        }

        /**
         * Gets the list of children causing this application to be in error.
         * 
         * @return The list of children causing this application to be in error
         */
        public Set<String> getBadChildren() {
            return Collections.unmodifiableSet(this.rcInfo_.badChildren);
        }

        /**
         * Gets the transition command this application is executing.
         * 
         * @return The transition command this applications is executing
         */
        public String getCurrentTransition() {
            return this.rcInfo_.currentTransitionCmd;
        }
 
        /**
         * Gets the last transition command this application executed.
         * 
         * @return The last transition command this application executed
         */
        public String getLastTransition() {
            return this.rcInfo_.lastTransitionCmd;
        }

        /**
         * Gets the application busy state
         * 
         * @return <code>true</code> if the application is busy.
         */
        public boolean isBusy() {
            return this.rcInfo_.busy;
        }

        /**
         * Returns <em>true</em> if the application is performing an FSM transition
         * 
         * @return <em>true</em> if the application is performing an FSM transition
         */
        public boolean isTransitioning() {
            return this.rcInfo_.transitioning;
        }

        /**
         * Returns <code>true</code> if the application membership is IN.
         * 
         * @return <code>true</code> if the application membership is IN.
         */
        public boolean isEnabled() {
            return this.supInfo_.membership;
        }

        /**
         * Returns <code>true</code> if the application is in error.
         * 
         * @return <code>true</code> if the application is in error
         */
        public boolean isError() {
            return this.rcInfo_.fault;
        }

        /**
         * Returns <code>true</code> if the application is running.
         * 
         * @return <code>true</code> if the application is running
         */
        public boolean isUp() {
            final boolean status;
            if(this.getStatus().equals(ApplicationStatus.UP)) {
                status = true;
            } else {
                status = false;
            }
            return status;
        }
    }

    /**
     * Constructor for non run control applications.
     * <p>
     * <code>ifError</code> is set to an empty string.
     * 
     * @param name Application name
     * @param segmentName Application segment name
     * @param host Application host name (primary host from configuration)
     * @param ifExitUnexpected Action taken when the application exits unexpectedly
     * @param ifFailedToStart Action taken when the application fails to start
     * @param startAt When the application is started
     * @param stopAt When the application is stopped
     * @param startIn The directory the application should be started in
     * @param initTimeout The application initialization timeout
     * @param restartableInRunning <code>true</code> if the application can be restarted in the RUNNING state
     * @param className The application class name from the database
     * @param canExit <true> if the application is allowed to exit at any moment
     * @param appConfig The {@link dal.BaseApplication} object holding the application's configuration
     */
    private RunControlApplicationData(final String name,
                                      final String segmentName,
                                      final String host,
                                      final String ifExitUnexpected,
                                      final String ifFailedToStart,
                                      final String startAt,
                                      final String stopAt,
                                      final String startIn,
                                      final int initTimeout,
                                      final boolean restartableInRunning,
                                      final String className,
                                      final boolean canExit,
                                      final dal.BaseApplication appConfig)
    {
        super();
        this.name = name;
        this.segmentName = segmentName;
        this.host = host;
        this.ifExitUnexpected = ifExitUnexpected;
        this.ifFailedToStart = ifFailedToStart;
        this.ifError = "";
        this.startAt = startAt;
        this.stopAt = stopAt;
        this.startIn = startIn;
        this.initTimeout = initTimeout;
        this.actionTimeout = RunControlApplicationData.defaultInt;
        this.isController = false;
        this.isTTCPartitionController = false;
        this.restartableInRunning = restartableInRunning;
        this.isRunControlApplication = false;
        this.appClassName = className;
        this.canExit = canExit;
        this.appConfig = appConfig;
    }

    /**
     * Constructor for run control application.
     * 
     * @param name Application name
     * @param segmentName Application segment name
     * @param host Application host name (primary host from configuration)
     * @param ifExitUnexpected Action taken if the application exits unexpectedly
     * @param ifFailedToStart Action taken if the application fails to start
     * @param ifError Action taken if the application is in error
     * @param startIn The directory the application should be started in
     * @param initTimeout The application initialization timeout
     * @param actionTimeout The application action timeout
     * @param segment <code>null</code> for all the run control applications but controllers (in that case it refers to the segment the
     *            controller supervises)
     * @param isTTCPartitionController <code>true<code> if the controller controls a TTC partition
     * @param restartableInRunning <code>true</code> if the application can be restarted in the RUNNING state
     * @param className The application class name from the database
     * @param appConfig The {@link dal.BaseApplication} object holding the application's configuration
     * @throws IguiException.ConfigException Some error occurred getting application information from the database
     */
    private RunControlApplicationData(final String name,
                                      final String segmentName,
                                      final String host,
                                      final String ifExitUnexpected,
                                      final String ifFailedToStart,
                                      final String ifError,
                                      final String startIn,
                                      final int initTimeout,
                                      final int actionTimeout,
                                      final dal.Segment segment,
                                      final boolean isTTCPartitionController,
                                      final boolean restartableInRunning,
                                      final String className,
                                      final dal.BaseApplication appConfig) throws IguiException.ConfigException
    {
        super();
        this.name = name;
        this.segmentName = segmentName;
        this.host = host;
        this.ifExitUnexpected = ifExitUnexpected;
        this.ifFailedToStart = ifFailedToStart;
        this.ifError = ifError;
        this.startAt = "BOOT";
        this.stopAt = "SHUTDOWN";
        this.startIn = startIn;
        this.initTimeout = initTimeout;
        this.actionTimeout = actionTimeout;
        this.isController = (segment != null);
        this.isTTCPartitionController = isTTCPartitionController;
        this.restartableInRunning = restartableInRunning;
        this.isRunControlApplication = true;
        this.appClassName = className;
        this.canExit = false;
        this.appConfig = appConfig;

        try {
            if((this.isController == true) && (segment != null)) {
                final dal.BaseApplication[] infr = segment.get_infrastructure();
                if(infr != null) {
                    for(final dal.BaseApplication app : infr) {
                        this.infrApps.add(app.UID());
                    }
                }
            }
        }
        catch(final ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting info from database for application " + name, ex);
        }
    }

    /**
     * It builds a {@link RunControlApplicationData} from a {@link dal.BaseApplication} object. Use this for all applications but controllers and
     * infrastructure applications.
     * <p>
     * Use {@link #createInfrastructureApplicationData(dal.BaseApplication, String)} for Infrastructure applications. Use
     * {@link #createControllerApplicationData(dal.BaseApplication)} for controller applications.
     * 
     * @param app The {@link dal.BaseApplication} describing the application
     * @return The {@link RunControlApplicationData} containing the application information stored in the configuration database
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     */

    static RunControlApplicationData createApplicationData(final dal.BaseApplication app) throws IguiException.ConfigException {
        try {
            return RunControlApplicationData.createApplicationData(app, app.get_segment().UID(), null);
        }
        catch(final ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting info from database for application " + app.UID(), ex);
        }
    }

    /**
     * Use this method to build a {@link RunControlApplicationData} object for a controller application.
     * <p>
     * Use {@link #createInfrastructureApplicationData(dal.BaseApplication, String)} for Infrastructure applications and
     * {@link #createApplicationData(dal.BaseApplication)} for all the other applications.
     * 
     * @param seg The segment the controller is supervising
     * @return The {@link RunControlApplicationData} containing the application information stored in the configuration database
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     */
    static RunControlApplicationData createControllerApplicationData(final dal.Segment seg) throws IguiException.ConfigException {
        try {
            final dal.BaseApplication app = seg.get_controller();
            return RunControlApplicationData.createApplicationData(app, app.get_segment().UID(), seg);
        }
        catch(final ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting info from database for the controller of segment " + seg.UID(), ex);
        }
    }

    /**
     * Use this method in order to build a {@link RunControlApplicationData} object for an Infrastructure application.
     * <p>
     * Infrastructure applications are different because they (logically) belong to the parent segment.
     * <p>
     * Use {@link #createApplicationData(dal.BaseApplication, boolean)} for non-Infrastructure applications. Use
     * {@link #createControllerApplicationData(dal.BaseApplication)} for controller applications.
     * 
     * @param app The {@link dal.BaseApplication} describing the application
     * @param segName The logic (parent) segment the application belongs to
     * @return The {@link RunControlApplicationData} containing the application information stored in the configuration database
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     */
    static RunControlApplicationData createInfrastructureApplicationData(final dal.BaseApplication app, final String segName)
        throws IguiException.ConfigException
    {
        return RunControlApplicationData.createApplicationData(app, segName, null);
    }

    /**
     * @param app The {@link dal.BaseApplication} describing the application
     * @param segName The name of the segment the application belongs to (for Infrastructure applications use the name of the parent
     *            segment)
     * @param seg For controllers, it represents the segment the controller supervises: otherwise <code>null</code>
     * @return The {@link RunControlApplicationData} containing the application information stored in the configuration database
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     */
    static private RunControlApplicationData createApplicationData(final dal.BaseApplication app, final String segName, final dal.Segment seg)
        throws IguiException.ConfigException
    {
        final String appName = app.UID();
        final String segmentName = segName;

        RunControlApplicationData appData;

        try {
            final String host = app.get_host().UID();
            final String ifExitUnex = app.get_IfExitsUnexpectedly();
            final String ifFailedToStart = app.get_IfFailsToStart();
            final String startIn = app.get_StartIn();
            final int initTimeout = app.get_InitTimeout();
            final String className = app.class_name();
            final boolean restartableInRunning = app.get_RestartableDuringRun();
            
            final dal.RunControlApplicationBase rcBaseApp = dal.RunControlApplicationBase_Helper.cast(app);
            if(rcBaseApp != null) {
                // This is a run control application
                final String ifError = rcBaseApp.get_IfError();
                final int actionTimeout = rcBaseApp.get_ActionTimeout();
                final boolean controlsTTCPartition = rcBaseApp.get_ControlsTTCPartitions();

                appData = new RunControlApplicationData(appName,
                                                        segmentName,
                                                        host,
                                                        ifExitUnex,
                                                        ifFailedToStart,
                                                        ifError,
                                                        startIn,
                                                        initTimeout,
                                                        actionTimeout,
                                                        seg,
                                                        controlsTTCPartition,
                                                        restartableInRunning,
                                                        className,
                                                        app);
            } else {
                // This is not a run control application
                boolean canExit = false;

                String startAt = "BOOT";
                String stopAt = "SHUTDOWN";
                final dal.CustomLifetimeApplicationBase cltApp = dal.CustomLifetimeApplicationBase_Helper.cast(app);
                if(cltApp != null) {
                    canExit = cltApp.get_AllowSpontaneousExit();

                    final String lifeTime = cltApp.get_Lifetime();
                    final String[] ss = lifeTime.split("_");
                    if(ss.length == 2) {
                        startAt = ss[0];
                        stopAt = ss[1];
                    }
                }

                appData =
                        new RunControlApplicationData(appName,
                                                      segmentName,
                                                      host,
                                                      ifExitUnex,
                                                      ifFailedToStart,
                                                      startAt,
                                                      stopAt,
                                                      startIn,
                                                      initTimeout,
                                                      restartableInRunning,
                                                      className,
                                                      canExit,
                                                      app);
            }
        }
        catch(final ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting info from database for application " + appName, ex);
        }

        return appData;
    }

    /**
     * Gets the application action timeout.
     * 
     * @return The application action timeout or an empty string for a non run control application
     */
    public String getActionTimeout() {
        if(this.actionTimeout != RunControlApplicationData.defaultInt) {
            return Integer.toString(this.actionTimeout);
        }

        return "";
    }

    /**
     * Gets the (primary) host the application is configured to run on.
     * 
     * @return The (primary) host the application is configured to run on
     */
    public String getHost() {
        return this.host;
    }

    /**
     * Gets the action taken when the application exits unexpectedly
     * 
     * @return The action taken when the application exits unexpectedly
     */
    public String getIfExitUnexpected() {
        return this.ifExitUnexpected;
    }

    /**
     * Gets the action taken when the application is in error
     * 
     * @return The action taken when the application is in error
     */
    public String getIfError() {
        return this.ifError;
    }

    /**
     * Gets the action taken when the application fails to start
     * 
     * @return The action taken when the application fails to start
     */
    public String getIfFailedToStart() {
        return this.ifFailedToStart;
    }

    /**
     * Gets the application initialization timeout.
     * 
     * @return The application initialization timeout
     */
    public int getInitTimeout() {
        return this.initTimeout;
    }

    /**
     * Gets the application name.
     * 
     * @return The application name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the application start parameters.
     * 
     * @return The application start parameters
     */
    public String getParameters() {
        if(this.parameters.get() == null) {
            this.setParameters();
        }

        return this.parameters.get();
    }

    /**
     * Gets the application restart parameters.
     * 
     * @return The application restart parameters
     */
    public String getRestartParameters() {
        if(this.restartParameters.get() == null) {
            this.setParameters();
        }

        return this.restartParameters.get();
    }

    /**
     * It sets the application's start and restart parameters.
     */
    private void setParameters() {
        final StringBuilder startArgs = new StringBuilder();
        final StringBuilder restartArgs = new StringBuilder();

        try {
            this.appConfig.get_info(new HashMap<>(),
                                    new ArrayList<>(),
                                    startArgs,
                                    restartArgs);
        }
        catch(final ConfigException ex) {
            IguiLogger.error("Error occurred getting start/restart arguments for application " + "\"" + this.name + "\": "
                             + ex.getMessage(), ex);

            startArgs.append("ERROR: start arguments unknown");
            restartArgs.append("ERROR: restart arguments unknown");
        }

        this.parameters.compareAndSet(null, startArgs.toString());
        this.restartParameters.compareAndSet(null, restartArgs.toString());
    }

    /**
     * Gets the application segment name.
     * 
     * @return The application segment name
     */
    public String getSegmentName() {
        return this.segmentName;
    }

    /**
     * Gets when the application is started.
     * 
     * @return When the application is started
     */
    public String getStartAt() {
        return this.startAt;
    }

    /**
     * It returns <em>true</em> if the application is allowed to exit spontaneously
     * 
     * @return <em>true</em> if the application is allowed to exit spontaneously
     */
    public boolean canExitSpontaneously() {
        return this.canExit;
    }

    /**
     * Gets the directory where the application is started.
     * 
     * @return The directory where the application is started
     */
    public String getStartIn() {
        return this.startIn;
    }

    /**
     * Gets when the application is terminated.
     * 
     * @return When the application is terminated
     */
    public String getStopAt() {
        return this.stopAt;
    }

    /**
     * Returns <code>true</code> if the application is a controller.
     * 
     * @return <code>true</code> if the application is a controller
     */
    public boolean isController() {
        return this.isController;
    }

    /**
     * It returns the OKS application's class name
     * 
     * @return The OKS application's class name
     */
    public String getClassName() {
        return this.appClassName;
    }

    /**
     * Returns <code>true</code> if the application is the controller of a TTC partition
     * 
     * @return <code>true</code> if the application is the controller of a TTC partition
     */
    public boolean isTTPartitionController() {
        return this.isTTCPartitionController;
    }

    /**
     * Returns <code>true</code> if the application is the application can be restarted in the RUNNING state
     * 
     * @return <code>true</code> if the application is the application can be restarted in the RUNNING state
     */
    public boolean isRestartableInRunning() {
        return this.restartableInRunning;
    }

    /**
     * Returns <code>true</code> if the application implements the RunControlApplication interface.
     * 
     * @return <code>true</code> if the application implements the RunControlApplication interface
     */
    public boolean isRunControlApplication() {
        return this.isRunControlApplication;
    }

    /**
     * It returns the list of infrastructure applications belonging to a controller.
     * <p>
     * Note that the list is the one reported in the configuration database (segment configuration), but actually the infrastructure
     * applications logically belong to (and are started by) the parent controller.
     * 
     * @return Empty list for all the applications but segment controllers (in this case the returned list contains the infrastructure
     *         applications of the segment supervised by the controller as described in the database - those applications are actually
     *         managed by the parent controller).
     */
    public List<String> infrastructureApplications() {
        return Collections.unmodifiableList(this.infrApps);
    }

    /**
     * It resets the application RC information to its default values.
     * 
     * @see #resetRCInfo(long, boolean)
     */
    void resetRCInfo() {
        this.resetRCInfo(0, true);
    }

    /**
     * It resets the application RC information to its default values but updates the time stamp.
     * <p>
     * This is used when the information is removed from IS: the values have to be reset but not the time stamp.
     * 
     * @param time Time stamp
     * @param forceUpdate If <code>true</code> the information will be reset without taking into account the time stamp.
     */
    void resetRCInfo(final long time, final boolean forceUpdate) {
        final ApplicationISStatus.RCInfo newRCInfo = new ApplicationISStatus.RCInfo(time);

        boolean stop = false;
        do {
            final ApplicationISStatus ip = this.appInfo.get();
            // Use the atomic reference CAS to avoid any locking
            // We cannot use here just 'set' on the atomic reference because we are changing only 'half' of the information
            final long timeDiff = time - ip.rcInfo_.infoTime;
            // When the information is removed from IS the time stamp is at most equal to the last (fresh) one
            if((timeDiff >= 0) || (forceUpdate == true)) {
                stop = this.appInfo.compareAndSet(ip, new ApplicationISStatus(newRCInfo, ip.supInfo_));
            } else {
                stop = true;
            }
        }
        while(stop == false);
    }

    /**
     * It resets the application Supervisor information to its default values.
     * 
     * @see #resetSupervisorInfo(long, boolean)
     */
    void resetSupervisorInfo() {
        this.resetSupervisorInfo(0, true);
    }

    /**
     * It resets the application Supervisor information to its default values but updates the time stamp.
     * <p>
     * This is used when the information is removed from IS: the values have to be reset but not the time stamp.
     * 
     * @param time Time stamp
     * @param forceUpdate If <code>true</code> the information will be reset without taking into account the time stamp.
     */
    void resetSupervisorInfo(final long time, final boolean forceUpdate) {
        final ApplicationISStatus.SupervisorInfo newSupInfo = new ApplicationISStatus.SupervisorInfo(time);

        boolean stop = false;
        do {
            final ApplicationISStatus ip = this.appInfo.get();
            // Use the atomic reference CAS to avoid any locking
            // We cannot use here just 'set' on the atomic reference because we are changing only 'half' of the information
            final long timeDiff = time - ip.supInfo_.infoTime;
            // When the information is removed from IS the time stamp is at most equal to the previous one
            if((timeDiff >= 0) || (forceUpdate == true)) {
                stop = this.appInfo.compareAndSet(ip, new ApplicationISStatus(ip.rcInfo_, newSupInfo));
            } else {
                stop = true;
            }
        }
        while(stop == false);
    }

    /**
     * Updates application information from supervisor.
     * 
     * @param info IS structure containing application supervisor information
     * @param forceUpdate If <code>true</code> the information will be updated even if the current and the new information have the same
     *            time stamp
     * @return The difference between the current and the updated information time stamp
     * @throws IllegalArgumentException The application membership status cannot be determined
     * @throws UnknownStatus The application status cannot be determined
     * @see Igui.RunControlApplicationData.ApplicationISStatus.SupervisorInfo
     */
    long updateInfo(final rc.DAQApplicationInfo info, final boolean forceUpdate) throws UnknownStatus, IllegalArgumentException {
        // The new supervisor information structure
        final ApplicationISStatus.SupervisorInfo newSupInfo = new ApplicationISStatus.SupervisorInfo(info);
        // The time stamp difference between the current and the new information
        long timeDiff = 0;
        // The new information time stamp
        final long newTime = info.getTime().getTimeMicro();

        boolean stop = false;
        // In the loop we try to update atomically and in a consistent way the 'appInfo' reference
        do {
            // Get the current application IS information
            final ApplicationISStatus ip = this.appInfo.get();

            // Compute the time difference between the current and the new information
            final long oldTime = ip.supInfo_.infoTime;
            timeDiff = newTime - oldTime;

            // If the new information is "fresh" or the updated is forced then try to update the reference
            if((timeDiff > 0) || ((timeDiff >= 0) && (forceUpdate == true))) {
                // Here use CAS: update atomically the reference only if somebody else has not done yet
                // If the reference is updated then 'stop' is set to 'true' and the loop is over
                // Please note how the ApplicationISStatus is created: the reference to the IS information of
                // the 'RCStatus' type is not modified at all!
                // The point is that we do not want to update the reference if some other thread has already done it: only
                // the 'compareAndSet' method is atomic and it would be enough if we should not
                // check for the IS call-backs order (via information time stamps).
                stop = this.appInfo.compareAndSet(ip, new ApplicationISStatus(ip.rcInfo_, newSupInfo));
            } else {
                // The information we are trying to update is old (maybe somebody has already updated it), stop trying
                stop = true;
            }
            // The loop stops when trying to update the reference makes no sense
        }
        while(stop == false);

        return timeDiff;
    }

    /**
     * Updates application information from run control.
     * 
     * @param info IS structure containing application run control information
     * @param forceUpdate If <code>true</code> the information will be updated even if the current and the new information have the same
     *            time stamp
     * @return The difference between the current and the updated information time stamp
     * @throws UnknownState The application state cannot be determined
     * @see Igui.RunControlApplicationData.ApplicationISStatus.RCInfo
     */
    long updateInfo(final rc.RCStateInfo info, final boolean forceUpdate) throws UnknownState {
        // The algorithms works exactly as the previous method: it uses CAS on the atomic reference
        final ApplicationISStatus.RCInfo newRCInfo = new ApplicationISStatus.RCInfo(info);
        long timeDiff = 0;
        final long newTime = info.getTime().getTimeMicro();

        boolean stop = false;
        do {
            final ApplicationISStatus ip = this.appInfo.get();

            final long oldTime = ip.rcInfo_.infoTime;
            timeDiff = newTime - oldTime;

            if((timeDiff > 0) || ((timeDiff >= 0) && (forceUpdate == true))) {
                stop = this.appInfo.compareAndSet(ip, new ApplicationISStatus(newRCInfo, ip.supInfo_));
            } else {
                stop = true;
            }
        }
        while(stop == false);

        return timeDiff;
    }

    /**
     * It returns a reference to the object containing the application IS status.
     * <p>
     * The returned object is immutable and can be safely shared amongst various threads.
     * 
     * @return A reference to the object containing the application IS status
     */
    public ApplicationISStatus getISStatus() {
        return this.appInfo.get();
    }

    /**
     * It returns the application name.
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Two applications are equal if the have the same name.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        final boolean b;

        if(RunControlApplicationData.class.isInstance(obj)) {
            b = this.toString().equals(obj.toString());
        } else {
            b = false;
        }

        return b;
    }

    /**
     * The has code is generated from the string describing the application name.
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }
}
