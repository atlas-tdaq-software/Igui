package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolTip;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.jdesktop.swingx.JXLabel;

import Igui.IguiException.ConfigException;
import Igui.RunControlFSM.State;
import Igui.Common.MultiLineToolTip;
import dal.ResourceSet_Helper;


/**
 * This panel contains the "Segments and Resources" tree, used to enable/disable segments and resources.
 * <p>
 * The tree is built taking information from the configuration database. Since the number of tree nodes may be huge the tree is built in a
 * lazy way: children are added to a node only when that node is expanded. Segments and resources may be enabled/disabled using the pop-up
 * menu appearing when the mouse right button is clocked an a tree node is selected. Changes may be committed to the database using the
 * "Commit &amp; Reload" button in the Igui main frame tool-bar. If the Igui is in STATUS DISPLAY then it will not be possible to modify
 * resources or segments.
 */
final class SegmentsResourcesPanel extends IguiPanel {

    private static final long serialVersionUID = 9145570943122742037L;

    /**
     * The panel name
     */
    private final static String panelName = "Segments & Resources";

    /**
     * Label put on the toolbar to show simple messages
     */
    private final JLabel messageLabel = new JLabel();

    /**
     * Name of the default icon file for leaf nodes
     */
    private final static String defaultLeafIconName = "retry.png";

    /**
     * The tree model
     */
    private final DefaultTreeModel treeModel = new DefaultTreeModel(null);

    /**
     * The segments and resources tree
     */
    private final JTree segmentTree = new JTree(this.treeModel) {
        private static final long serialVersionUID = 3639522943424030227L;

        @Override
        public JToolTip createToolTip() {
            final MultiLineToolTip tt = new MultiLineToolTip(true);
            tt.setComponent(this);
            return tt;
        }
    };

    /**
     * Executor where all the task modifying the database (i.e., enabling/disabling components, aborting changes) will be executed.
     */
    private final ThreadPoolExecutor dbUpdaterExecutor = ExecutorFactory.createSingleExecutor("SegAndRes-DB-Updater");

    /**
     * Changed items counter.
     * <p>
     * If this counter is grater than 0 and the panel is de-selected, then the user is informed that the tree was modified but changes were
     * not committed to the database. Any time the database is reloaded this counter is reset.
     */
    private final AtomicInteger changedItems = new AtomicInteger(0);

    /**
     * Database changes counter.
     * <p>
     * This counter keeps trace of the changes made to the database.
     */
    private final AtomicInteger databaseChanges = new AtomicInteger(0);

    /**
     * The global collection of disabled components
     */
    private final ConcurrentSkipListSet<dal.Component> disabledComponents = new ConcurrentSkipListSet<dal.Component>(new DalComponentComparator());

    /**
     * Segment icon file name
     */
    private final static String segmentIconName = "segment.png";

    /**
     * Resource icon file name
     */
    private final static String resourceIconName = "resource.png";

    /**
     * File name for icon showing a difference between the node state shown in the tree and the one in the database
     */
    private final static String differenceToDatabase = "differenceToDatabase.png";

    /**
     * Segment icon
     */
    private final static ImageIcon segmentIcon = Igui.createIcon(SegmentsResourcesPanel.segmentIconName);

    /**
     * Resource icon
     */
    private final static ImageIcon resourceIcon = Igui.createIcon(SegmentsResourcesPanel.resourceIconName);

    /**
     * Icon showing a difference between the node state shown in the tree and the one in the database
     */
    private final static ImageIcon diffToDbIcon = Igui.createIcon(SegmentsResourcesPanel.differenceToDatabase);

    /**
     * Class implementing the <code>Comparator</code> interface for the <code>ConcurrentSkipListSet</code> containing {@link dal.Component}
     * objects.
     * <p>
     * It uses the <code>UID</code> to compare two {@link dal.Component} objects.
     */
    private final static class DalComponentComparator implements Comparator<dal.Component> {

        /**
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(final dal.Component o1, final dal.Component o2) {
            return o1.UID().compareTo(o2.UID());
        }

    }

    /**
     * Enumeration listing the node (i.e., resource) enabled state.
     * <p>
     * To make the tree nodes visualization easier, to each state a background and a foreground colors are associated.
     */
    enum EnabledState {
        /**
         * The resource is enabled.
         * <p>
         * Foreground is black, background is green.
         */
        ENABLED(Color.black, Color.green) {
            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#state()
             */
            @Override
            public String state() {
                return "enabled";
            }

            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#enabled()
             */
            @Override
            public boolean enabled() {
                return true;
            }
        },
        /**
         * The resource is enabled but some of its children are not.
         * <p>
         * Foreground is red, background is green.
         */
        ENABLED_WITH_DISABLED_CHILDREN(Color.red, Color.green) {
            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#state()
             */
            @Override
            public String state() {
                return "enabled";
            }

            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#enabled()
             */
            @Override
            public boolean enabled() {
                return true;
            }
        },
        /**
         * The resource is disabled.
         * <p>
         * The foreground is black, the background is red.
         */
        EXPLICITLY_DISABLED(Color.black, Color.red) {
            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#state()
             */
            @Override
            public String state() {
                return "disabled";
            }

            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#enabled()
             */
            @Override
            public boolean enabled() {
                return false;
            }
        },
        /**
         * The resource is disabled because one of its parents is disabled.
         * <p>
         * Foreground is black, background is light gray.
         */
        INDIRECTLY_DISABLED(Color.black, Color.lightGray) {
            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#state()
             */
            @Override
            public String state() {
                return "disabled";
            }

            /**
             * @see Igui.SegmentsResourcesPanel.EnabledState#enabled()
             */
            @Override
            public boolean enabled() {
                return false;
            }
        };

        /**
         * Foreground color associated with the state
         */
        private final Color frg;

        /**
         * Background color associated to the state
         */
        private final Color bkg;

        /**
         * Constructor.
         * 
         * @param frg Foreground color
         * @param bkg Background color
         */
        EnabledState(final Color frg, final Color bkg) {
            this.frg = frg;
            this.bkg = bkg;
        }

        /**
         * It returns the background color associated to the state.
         * 
         * @return The background color
         */
        public Color bkgColor() {
            return this.bkg;
        }

        /**
         * It returns the foreground color associated to the state.
         * 
         * @return The foreground color
         */
        public Color frgColor() {
            return this.frg;
        }

        /**
         * It returns a simple string describing the state (i.e., "enabled" or "disabled").
         * 
         * @return A simple string describing the state.
         */
        abstract public String state();

        /**
         * It returns the enabled state of the resource.
         * 
         * @return <code>true</code> if the segment or resource is enabled.
         */
        abstract boolean enabled();
    }

    /**
     * Wrapper class used to describe the dal objects to be used in the tree.
     */
    private abstract class TreeDalObject {
        /**
         * <code>TRUE</code> if this object has some child
         */
        private volatile Boolean hasChildren;

        /**
         * This object enabled state
         */
        private volatile EnabledState enabledState;

        /**
         * Constructor.
         * 
         * @param obj The underlining dal object
         */
        TreeDalObject(final dal.Component obj) {
        }

        /**
         * It gets the array of resources in relationship with the dal object.
         * 
         * @return The array of resources in relationship with the dal object
         * @throws IguiException.ConfigException Some error occurred while interacting with the database
         */
        abstract public dal.Component[] getResources() throws IguiException.ConfigException;

        /**
         * It gets the array of segments in relationship with the dal object.
         * 
         * @return The array of segments in relationship with the dal object
         * @throws IguiException.ConfigException Some error occurred while interacting with the database
         */
        abstract public dal.Segment[] getSegments() throws IguiException.ConfigException;

        /**
         * It gets the underlining dal object.
         * 
         * @return The underlining dal object
         */
        abstract public dal.Component getDalComponent();

        /**
         * It gets the icon to be used for this object representation in the tree.
         * 
         * @return The icon to be used for this object representation in the tree
         */
        abstract public ImageIcon getIcon();

        /**
         * It gets the type name of this object (i.e., segment or resource).
         * 
         * @return The type name of this object (i.e., segment or resource)
         */
        abstract public String getTypeName();

        /**
         * It gets a string explaining the reason why the dal component is disabled.
         * <p>
         * In case of error the string gives a description of the error occurred.
         * <p>
         * For efficiency call this method after having determined the component disabled status.
         * 
         * @return A string explaining the reason why the dal component is disabled
         */
        public String getWhyDisabled() {
            String reason;

            try {
                if(!this.getEnabledState().enabled()) {
                    reason = this.getDalComponent().why_disabled(Igui.instance().getDalPartition(), "   ", true);
                } else {
                    reason = "The component \"" + this.getName() + "\" is enabled";
                }
            }
            catch(final Exception ex) {
                reason = "The disabled status for component \"" + this.getName() + "\" cannot be determined: " + ex;
                IguiLogger.error(reason, ex);
            }

            return reason;
        }

        /**
         * It gets this object name to be used in the tree representation.
         * 
         * @return The name to be used in the tree representation
         */
        public String getName() {
            return this.getDalComponent().UID();
        }

        /**
         * It tells if the underlining dal object has some child.
         * <p>
         * To optimize performances this method should be called after {@link #getEnabledState()}.
         * 
         * @return <code>TRUE</code> if the dal object has some child
         * @throws IguiException.ConfigException Some error occurred while interacting with the database
         */
        public boolean hasChildren() throws IguiException.ConfigException {
            if(this.hasChildren == null) {
                final dal.Segment[] segs = this.getSegments();
                if((segs != null) && (segs.length > 0)) {
                    this.hasChildren = Boolean.TRUE;
                } else {
                    final dal.Component[] res = this.getResources();
                    if((res != null) && (res.length > 0)) {
                        this.hasChildren = Boolean.TRUE;
                    } else {
                        this.hasChildren = Boolean.FALSE;
                    }
                }
            }

            return this.hasChildren.booleanValue();
        }

        /**
         * It gets the dal object enabled state.
         * 
         * @return The dal object enabled state
         * @throws IguiException.ConfigException Some error occurred while interacting with the database
         */
        public EnabledState getEnabledState() throws IguiException.ConfigException {
            if(this.enabledState == null) {
                try {
                    final dal.Partition p = Igui.instance().getDalPartition();
                    final boolean enabled = !this.getDalComponent().disabled(p);
                    if(enabled == true) {
                        // The node may be ENABLED or ENABLED_WITH_DISABLED_CHILDREN
                        this.enabledState = EnabledState.ENABLED;

                        // Check if some child is disabled
                        // First fill the list of node children
                        final Set<dal.Component> childrenList = this.getAllChildren();

                        // Scan the global list of disabled components to see
                        // if it contains a child of this node
                        for(final dal.Component c : childrenList) {
                            if(c.disabled(p) == true) {
                                this.enabledState = EnabledState.ENABLED_WITH_DISABLED_CHILDREN;
                                break;
                            }
                        }
                    } else {
                        // The node may be INDIRECTLY_DISABLED or EXPLICITLY_DISABLED

                        // Check if explicitly disabled
                        // Look for this node in the global list of disabled components
                        if(SegmentsResourcesPanel.this.disabledComponents.contains(this.getDalComponent())) {
                            this.enabledState = EnabledState.EXPLICITLY_DISABLED;
                        } else {
                            this.enabledState = EnabledState.INDIRECTLY_DISABLED;
                        }
                    }
                }
                catch(final IguiException.ConfigException ex) {
                    throw ex;
                }
                catch(final Exception ex) {
                    throw new IguiException.ConfigException("Failed getting enabled state for " + this.getName() + ": " + ex.getMessage(),
                                                            ex);
                }
            }

            return this.enabledState;
        }

        /**
         * It gets all the dal object children (recursively).
         * 
         * @return A set containing all the dal object children
         * @throws IguiException.ConfigException Some error occurred while interacting with the database
         */
        private Set<dal.Component> getAllChildren() throws IguiException.ConfigException {
            final Set<dal.Component> children = new LinkedHashSet<dal.Component>();

            final dal.Segment[] segs = this.getSegments();
            if((segs != null) && (segs.length > 0)) {
                this.hasChildren = Boolean.TRUE;
                for(final dal.Segment s : segs) {
                    children.add(s);
                    children.addAll(this.getAllSegmentChildren(s));
                }
            }

            final dal.Component[] ress = this.getResources();
            if((ress != null) && (ress.length > 0)) {
                this.hasChildren = Boolean.TRUE;
                for(final dal.Component r : ress) {
                    children.add(r);
                    children.addAll(this.getAllResourceChildren(r));
                }
            }

            return children;
        }

        /**
         * It gets all the children (both segments and resources) of the specified segment.
         * 
         * @param dalSegment The segment whose children we are looking for
         * @return A set containing the children
         * @throws IguiException.ConfigException Some error occurred while interacting with the database
         */
        private Set<dal.Component> getAllSegmentChildren(final dal.Segment dalSegment) throws IguiException.ConfigException {
            try {
                final Set<dal.Component> childrenList = new LinkedHashSet<dal.Component>();

                final dal.Segment[] segments = dalSegment.get_Segments();
                if(segments != null) {
                    for(final dal.Segment seg : segments) {
                        childrenList.add(seg);
                        childrenList.addAll(this.getAllSegmentChildren(seg));
                    }
                }

                final dal.ResourceBase[] resources = dalSegment.get_Resources();
                if(resources != null) {
                    for(final dal.ResourceBase res : resources) {
                        childrenList.add(res);
                        childrenList.addAll(this.getAllResourceChildren(res));
                    }
                }

                final dal.TemplateSegment ts = dal.TemplateSegment_Helper.cast(dalSegment);
                if(ts != null) {
                    final dal.Rack[] racks = ts.get_Racks();
                    if(racks != null) {
                        for(final dal.Rack r : racks) {
                            childrenList.add(r);
                        }
                    }
                }

                return childrenList;
            }
            catch(final IguiException.ConfigException ex) {
                throw ex;
            }
            catch(final Exception ex) {
                throw new IguiException.ConfigException("Failed filling the children list for segment " + dalSegment.UID(), ex);
            }
        }

        /**
         * It gets all the children of the specified resource.
         * 
         * @param dalResource The resource whose children we are looking for
         * @return A set containing the children
         * @throws IguiException.ConfigException Some error occurred while interacting with the database
         */
        private Set<dal.Component> getAllResourceChildren(final dal.Component dalResource) throws IguiException.ConfigException {
            try {
                final Set<dal.Component> childrenList = new LinkedHashSet<dal.Component>();

                final dal.ResourceSet resourceSet = ResourceSet_Helper.cast(dalResource);
                if(resourceSet != null) {
                    final dal.ResourceBase[] resources = resourceSet.get_Contains();
                    if(resources != null) {
                        for(final dal.ResourceBase res : resources) {
                            childrenList.add(res);
                            childrenList.addAll(this.getAllResourceChildren(res));
                        }
                    }
                }

                return childrenList;
            }
            catch(final IguiException.ConfigException ex) {
                throw ex;
            }
            catch(final Exception ex) {
                throw new IguiException.ConfigException("Failed filling the children list for resource " + dalResource.UID(), ex);
            }
        }
    }

    /**
     * Wrapper class describing a {@link dal.Segment} object to be used in the tree.
     */
    private final class DalSegment extends TreeDalObject {

        private final dal.Segment dalSegment;
        private volatile dal.Component[] childResources;
        private volatile dal.Segment[] childSegments;

        /**
         * Constructor.
         * 
         * @param seg The dal segment.
         */
        DalSegment(final dal.Segment seg) {
            super(seg);
            this.dalSegment = seg;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getResources()
         */
        @Override
        public dal.Component[] getResources() throws IguiException.ConfigException {
            if(this.childResources == null) {
                try {
                    final List<dal.Component> allResources = new ArrayList<dal.Component>();

                    final dal.ResourceBase[] res = this.dalSegment.get_Resources();
                    for(final dal.ResourceBase r : res) {
                        allResources.add(r);
                    }

                    final dal.TemplateSegment ts = dal.TemplateSegment_Helper.cast(this.dalSegment);
                    if(ts != null) {
                        final dal.Rack[] racks = ts.get_Racks();
                        if(racks != null) {
                            for(final dal.Rack r : racks) {
                                allResources.add(r);
                            }
                        }
                    }

                    this.childResources = allResources.toArray(new dal.Component[allResources.size()]);
                }
                catch(final Exception ex) {
                    throw new IguiException.ConfigException("Failed getting children resources for segment \"" + this.getName() + "\": "
                                                            + ex.getMessage(), ex);
                }
            }

            return this.childResources;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getSegments()
         */
        @Override
        public dal.Segment[] getSegments() throws IguiException.ConfigException {
            if(this.childSegments == null) {
                try {
                    this.childSegments = this.dalSegment.get_Segments();
                }
                catch(final Exception ex) {
                    throw new IguiException.ConfigException("Failed getting children segments for segment \"" + this.getName() + "\": "
                                                            + ex.getMessage(), ex);
                }
            }

            return this.childSegments;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getDalComponent()
         */
        @Override
        public dal.Segment getDalComponent() {
            return this.dalSegment;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getIcon()
         */
        @Override
        public ImageIcon getIcon() {
            return SegmentsResourcesPanel.segmentIcon;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getTypeName()
         */
        @Override
        public String getTypeName() {
            return "Segment";
        }
    }

    /**
     * Wrapper class describing a {@link dal.Resource} object to be used in the tree.
     */
    private final class DalResource extends TreeDalObject {

        private final dal.Component dalResource;
        private final TreeDalObject parent;
        private final AtomicBoolean modifiedState = new AtomicBoolean(false);
        private volatile dal.ResourceBase[] childResources;

        /**
         * Constructor.
         * 
         * @param res The dal resource
         * @param parent The parent segment/resource
         */
        DalResource(final dal.Component res, final TreeDalObject parent) {
            super(res);
            this.dalResource = res;
            this.parent = parent;
        }
        
        /**
         * Overridden to deal with some components that are not actually resources (e.g., Racks)
         * Indeed, unless explicitly disabled, those objects will always be enabled for DAL
         */
        @Override
        public EnabledState getEnabledState() throws IguiException.ConfigException {
            EnabledState state = super.getEnabledState();
            if((state.enabled() == true) && (this.parent.getEnabledState().enabled() == false))
            {
                state = EnabledState.INDIRECTLY_DISABLED;
                this.modifiedState.set(true);
            }
            
            return state;
        }
        
        /**
         * Overridden to deal with some components that are not actually resources (e.g., Racks)
         * Indeed, unless explicitly disabled, those objects will always be enabled for DAL
         */
        @Override
        public String getWhyDisabled() {
            if(this.modifiedState.get() == true) {
                return "it's parent " + this.parent.getName() + "@" + 
                       this.parent.getDalComponent().class_name() + 
                       " is disabled because:\n\t" + this.parent.getWhyDisabled();
            } 
            
            return super.getWhyDisabled();
        }
        
        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getResources()
         */
        @Override
        public dal.ResourceBase[] getResources() throws IguiException.ConfigException {
            if(this.childResources == null) {
                try {
                    final dal.ResourceSet resourceSet = ResourceSet_Helper.cast(this.dalResource);
                    if(resourceSet != null) {
                        this.childResources = resourceSet.get_Contains();
                    }
                }
                catch(final Exception ex) {
                    throw new IguiException.ConfigException("Failed getting children resources for resource \"" + this.getName() + "\": "
                                                            + ex.getMessage(), ex);
                }
            }

            return this.childResources;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getSegments()
         */
        @Override
        public dal.Segment[] getSegments() {
            return null;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getDalComponent()
         */
        @Override
        public dal.Component getDalComponent() {
            return this.dalResource;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getIcon()
         */
        @Override
        public ImageIcon getIcon() {
            return SegmentsResourcesPanel.resourceIcon;
        }

        /**
         * @see Igui.SegmentsResourcesPanel.TreeDalObject#getTypeName()
         */
        @Override
        public String getTypeName() {
            return "Resource";
        }

    }

    /**
     * Class extending {@link DefaultMutableTreeNode} and used to represent the tree nodes.
     */
    private final class SRTreeNode extends DefaultMutableTreeNode {

        private static final long serialVersionUID = 6910015623116659643L;

        /**
         * String representing the reason why a the node is disabled
         */
        private final String disabledReason;

        /**
         * The underlining {@link TreeDalObject} object (used as user object)
         */
        private final TreeDalObject treeObject;

        /**
         * <code>true</code> if this node has children (needed for lazy loading)
         */
        private final Boolean hasChildren;

        /**
         * <code>true</code> if this node can be disabled
         */
        private final boolean canBeDisabled;

        /**
         * Flag to know if the subtree having this node as parent has already been loaded from the database (needed for lazy loading).
         */
        private volatile boolean subtreeLoadedFromDB = false;

        /**
         * The enabled state of the tree node
         */
        private volatile EnabledState enabledState;

        /**
         * The enabled state of the tree node as described in the database
         */
        private final EnabledState enabledState_DB;

        /**
         * Name of data element
         */
        private final String name;

        /**
         * Maximum length for the "why disabled" message (that is to protect the tool-tip and the
         * dialogue window).
         */
        private final static int MAX_DISABLED_MSG_LENGTH = 2000;
        
        /**
         * Constructor.
         * <p>
         * It create a node which can be disabled. The {@link TreeDalObject} is set as the user object of this
         * {@link DefaultMutableTreeNode}.
         * 
         * @param obj The {@link DefaultMutableTreeNode} object describing the node
         * @throws IguiException.ConfigException Thrown if some error occurs retrieving information from the database
         */
        SRTreeNode(final TreeDalObject obj) throws IguiException.ConfigException {
            this(obj, true);
        }

        /**
         * Constructor.
         * <p>
         * It create the node. The {@link TreeDalObject} is set as the user object of this {@link DefaultMutableTreeNode}.
         * 
         * @param obj The {@link DefaultMutableTreeNode} object describing the node
         * @param canBeDisabled The {@link DefaultMutableTreeNode} object describing the node
         * @throws IguiException.ConfigException Thrown if some error occurs retrieving information from the database
         */
        SRTreeNode(final TreeDalObject obj, final boolean canBeDisabled) throws IguiException.ConfigException {
            super(obj);
            this.treeObject = obj;
            this.name = obj.getName();
            this.enabledState_DB = obj.getEnabledState();
            this.enabledState = this.enabledState_DB;
            this.hasChildren = Boolean.valueOf(obj.hasChildren()); // For opt call this after getEnabledState()
            
            final String whyDisabledFull = obj.getWhyDisabled(); // For opt call this after getEnabledState()
            if(whyDisabledFull.length() > SRTreeNode.MAX_DISABLED_MSG_LENGTH) {
                this.disabledReason = whyDisabledFull.substring(0, SRTreeNode.MAX_DISABLED_MSG_LENGTH) + "\n... TRUCATED, TEXT TOO LONG!";
            } else {
                this.disabledReason = whyDisabledFull;
            }
                
            this.setAllowsChildren(this.hasChildren.booleanValue());
            this.canBeDisabled = canBeDisabled;
        }

        /**
         * It gets a string representing the reason why the node is disabled.
         * 
         * @see TreeDalObject#getWhyDisabled()
         * @return A string representing the reason why the node is disabled
         */
        public final String getDisabledReason() {
            return this.disabledReason;
        }

        /**
         * It returns <code>true</code> if the node can be disabled.
         * <p>
         * An example of node that cannot be disabled is the "online" segment node.
         * 
         * @return <code>true</code> if the node can be disabled
         */
        public final boolean canBeDisabled() {
            return this.canBeDisabled;
        }

        /**
         * @see TreeDalObject#hasChildren().
         */
        @SuppressWarnings("unused")
        public final boolean hasChildren() {
            return this.hasChildren.booleanValue();
        }

        /**
         * It returns the enabled state in the database of the component represented by the tree node.
         * 
         * @return The enabled state in the database of the component represented by the tree node
         */
        public final EnabledState getEnabledState_DB() {
            return this.enabledState_DB;
        }

        /**
         * It sets the node enabled state.
         * 
         * @param enabledState The node enabled state
         */
        protected final void setEnabledState(final EnabledState enabledState) {
            this.enabledState = enabledState;
        }

        /**
         * It returns the current enabled state of the component represented by the tree node.
         * 
         * @return The current enabled state of the component represented by the tree node
         */
        public final EnabledState getEnabledState() {
            return this.enabledState;
        }

        /**
         * It sets whether the subtree whose node is this node has been loaded from the database (needed for lazy loading).
         * 
         * @param subtreeLoadedFromDB <code>true</code> if the subtree has already been loaded
         */
        protected final void setSubtreeLoadedFromDB(final boolean subtreeLoadedFromDB) {
            this.subtreeLoadedFromDB = subtreeLoadedFromDB;
        }

        /**
         * It returns <code>true</code> if the subtree whose this node is parent has been already loaded from the database.
         * <p>
         * This is needed for the tree lazy loading.
         * 
         * @return <code>true</code> if the subtree whose this node is parent has been already loaded from the database
         */
        public final boolean isSubtreeLoadedFromDB() {
            return this.subtreeLoadedFromDB;
        }

        /**
         * Overloaded for the expand handle to be shown in the tree when lazy loading is implemented.
         * <p>
         * It uses the super class implementation for the root node, while it returns <code>true</code> if the node has children.
         * 
         * @return <code>true</code> if the node has children
         * @see javax.swing.tree.DefaultMutableTreeNode#isLeaf()
         */
        @Override
        public final boolean isLeaf() {
            if(this.isRoot()) {
                return this.isLeaf();
            }

            return !(this.hasChildren.booleanValue());
        }

        /**
         * It returns the name of the tree node as it is displayed.
         * 
         * @return The name of the tree node as it is displayed
         * @see javax.swing.tree.DefaultMutableTreeNode#toString()
         */
        @Override
        public final String toString() {
            return this.name;
        }

        /**
         * @see javax.swing.tree.DefaultMutableTreeNode#getUserObject()
         */
        @Override
        public final TreeDalObject getUserObject() {
            return this.treeObject;
        }

        /**
         * It gets the icon to be used in the tree to represent this node.
         * 
         * @return The icon to be used in the tree to represent this node
         * @see TreeDalObject#getIcon()
         */
        public ImageIcon getIcon() {
            return this.treeObject.getIcon();
        }

        /**
         * @see TreeDalObject#getTypeName()
         */
        public String getTypeName() {
            return this.treeObject.getTypeName();
        }
    }

    /**
     * Class extending {@link DefaultTreeCellRenderer} and used as renderer for the tree.
     * <p>
     * The renderer return a <code>JPanel</code> containing:
     * <ul>
     * <li>A <code>JLabel</code> containing only an icon to differentiate resources from segments;
     * <li>A <code>JPanel</code> containing a <code>JLabel</code> with the node name;
     * <li>A <code>JPanel</code> containing a <code>JLabel</code> with the node enabled state.
     * </ul>
     */
    private final static class TreeCellRenderer extends DefaultTreeCellRenderer {

        private static final long serialVersionUID = -5642688931315778240L;

        private final JPanel panel;
        private final JLabel type;
        private final JLabel name;
        private final JPanel namePanel;
        private final JLabel status;
        private final JPanel statusPanel;

        private final Color SelectedBackgroundColor = Color.yellow;

        TreeCellRenderer() {
            super();

            // Set layout
            this.panel = new JPanel(true);

            this.panel.setLayout(new BorderLayout());
            this.panel.setOpaque(true);
            this.panel.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));

            this.type = new JLabel();
            this.name = new JLabel();
            this.name.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
            this.name.setForeground(Color.black);
            this.namePanel = new JPanel(true);
            this.namePanel.setOpaque(true);
            this.namePanel.setLayout(new BorderLayout());
            this.namePanel.add(this.type, BorderLayout.WEST);
            this.namePanel.add(this.name, BorderLayout.CENTER);

            this.status = new JLabel();
            this.status.setBorder(BorderFactory.createEtchedBorder());
            this.status.setOpaque(true);
            this.statusPanel = new JPanel(true);
            this.statusPanel.setLayout(new BorderLayout());
            this.statusPanel.setOpaque(true);
            this.statusPanel.add(this.status, BorderLayout.CENTER);

            this.panel.add(this.namePanel, BorderLayout.WEST);
            this.panel.add(this.statusPanel, BorderLayout.CENTER);

        }

        @Override
        public Component getTreeCellRendererComponent(final JTree tree,
                                                      final Object value,
                                                      final boolean sel,
                                                      final boolean expanded,
                                                      final boolean leaf,
                                                      final int row,
                                                      final boolean focus)
        {
            Component componentRenderer;

            if(SRTreeNode.class.isInstance(value)) {
                // Node data
                final SRTreeNode data = (SRTreeNode) (value);
                final SegmentsResourcesPanel.EnabledState nodeEnabledStatus = data.getEnabledState();
                final SegmentsResourcesPanel.EnabledState nodeEnabledStatusDB = data.getEnabledState_DB();
              
                // Set name and default foreground colors
                this.name.setText(data.toString());

                // Set icons
                this.type.setIcon(data.getIcon());

                if(!nodeEnabledStatus.equals(nodeEnabledStatusDB)) {
                    this.name.setIcon(SegmentsResourcesPanel.diffToDbIcon);
                } else {
                    this.name.setIcon(null);
                }

                // Check the status
                this.status.setForeground(nodeEnabledStatus.frgColor());
                this.status.setBackground(nodeEnabledStatus.bkgColor());
                this.status.setText(nodeEnabledStatus.state());
                this.type.setEnabled(nodeEnabledStatus.enabled());

                // Item selected
                if(sel == true) {
                    this.panel.setBackground(this.SelectedBackgroundColor);
                    this.namePanel.setBackground(this.SelectedBackgroundColor);
                    this.statusPanel.setBackground(this.SelectedBackgroundColor);
                } else {
                    this.panel.setBackground(Color.white);
                    this.namePanel.setBackground(Color.white);
                    this.statusPanel.setBackground(Color.white);
                }

                // this.panel.setToolTipText("Press right mouse button to enable/disable segment");
                if(!nodeEnabledStatusDB.enabled()) {
                    final String boldMsg = "The component \"" + data.toString() + "\" is disabled because:\n";
                    final String plainMsg = data.getDisabledReason();

                    final StringBuilder str = new StringBuilder();
                    str.append("<html>");
                    str.append("<b>");
                    str.append(boldMsg.replaceAll("\\n", Matcher.quoteReplacement("<br />")));
                    str.append("</b>");
                    str.append(plainMsg.replaceAll("\\n", Matcher.quoteReplacement("<br />")).replaceAll("  ",
                                                                                                         Matcher.quoteReplacement("&nbsp;")));
                    str.append("</html>");

                    this.panel.setToolTipText(str.toString());
                } else {
                    final StringBuilder msg = new StringBuilder();
                    msg.append("<html>");
                    msg.append("The component ");
                    msg.append("<b>");
                    msg.append(data.toString());
                    msg.append("</b>");
                    msg.append(" is enabled");
                    msg.append("</html>");
                    this.panel.setToolTipText(msg.toString());
                }

                componentRenderer = this.panel;
            } else {
                componentRenderer = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, focus);
            }

            return componentRenderer;
        }
    }

    /**
     * SwingWorker used to load node information from database when a node is expanded (lazy loading).
     * <p>
     * As soon as the node is expanded a "fake" node is added to the subtree whose name informs the user that the action may take a while
     * because the information is retrieved from the database. This node is removed when all the subtree items are loaded and the right tree
     * content is shown.
     */
    private class NodeInfoWorker extends SwingWorker<Void, Void> {

        /**
         * The expanding node
         */
        private final SRTreeNode treeNode;

        /**
         * The 'waiting' node
         */
        private final DefaultMutableTreeNode waitingNode = new DefaultMutableTreeNode("Loading children form DAL...");

        /**
         * Constructor.
         * 
         * @param treeNode The expanded node.
         */
        NodeInfoWorker(final SRTreeNode treeNode) {
            this.treeNode = treeNode;
        }

        /**
         * It adds children to the expanding node.
         * 
         * @see javax.swing.SwingWorker#doInBackground()
         */
        @Override
        protected Void doInBackground() throws IguiException.ConfigException {
            synchronized(this.treeNode) {
                // Add 'waiting' node
                this.treeNode.add(this.waitingNode);
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        SegmentsResourcesPanel.this.treeModel.nodesWereInserted(NodeInfoWorker.this.treeNode, new int[] {0});
                    }
                });

                this.addChildrenToNode();
            }

            return null;
        }

        /**
         * It sets the flag taking into account that the subtree information has been loaded from the database and signals the tree model
         * that the node structure has changed.
         * 
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                this.get();
                this.treeNode.setSubtreeLoadedFromDB(true);
            }
            catch(final InterruptedException ex) {
                final String errMsg = "Task interrupted while waiting for the node \"" + this.treeNode.toString()
                                      + "\" info to be retrieved from the databse";
                IguiLogger.warning(errMsg, ex);
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                final String errorMsg = "Cannot load tree form DAL: " + ex.toString();
                IguiLogger.error(errorMsg, ex);
                ErrorFrame.showError("Segments & Resources Panel Error", errorMsg, ex);
            }
            finally {
                this.waitingNode.removeFromParent();
                SegmentsResourcesPanel.this.treeModel.nodeStructureChanged(this.treeNode);
            }
        }

        /**
         * It adds all the children nodes to the node is going to be expanded.
         * 
         * @throws IguiException.ConfigException Error getting information from database
         */
        private void addChildrenToNode() throws IguiException.ConfigException {
            final dal.Component[] ress = this.treeNode.getUserObject().getResources();
            if(ress != null) {
                for(final dal.Component r : ress) {
                    this.treeNode.add(new SRTreeNode(new DalResource(r, this.treeNode.getUserObject())));
                }
            }

            final dal.Segment[] segs = this.treeNode.getUserObject().getSegments();
            if(segs != null) {
                for(final dal.Segment s : segs) {
                    this.treeNode.add(new SRTreeNode(new DalSegment(s)));
                }
            }
        }
    }

    /**
     * Class implementing the {@link TreeWillExpandListener} interface.
     * <p>
     * It is used for the tree lazy loading.
     * 
     * @see NodeInfoWorker
     */
    private class TreeWillExpandAdapter implements TreeWillExpandListener {

        /**
         * @see javax.swing.event.TreeWillExpandListener#treeWillCollapse(javax.swing.event.TreeExpansionEvent)
         */
        @Override
        public void treeWillCollapse(final TreeExpansionEvent event) throws ExpandVetoException {
        }

        /**
         * @see javax.swing.event.TreeWillExpandListener#treeWillExpand(javax.swing.event.TreeExpansionEvent)
         */
        @Override
        public void treeWillExpand(final TreeExpansionEvent event) throws ExpandVetoException {
            // Get the expanding node
            final Object expandingNode = event.getPath().getLastPathComponent();

            if(SRTreeNode.class.isInstance(expandingNode)) {
                final SRTreeNode lastNode = (SRTreeNode) expandingNode;

                // Start a SwingWorker loading the subtree.
                if(lastNode.isSubtreeLoadedFromDB() == false) {
                    new NodeInfoWorker(lastNode).execute();
                }
            }
        }
    }

    /**
     * Class extending {@link MouseAdapter} and implementing {@link ActionListener} used to create a pop-up menu appearing when the right
     * mouse button is clicked and to show a window giving the reason why a component is disabled.
     * <p>
     * The pop-up menu allows the user to enable or disable the selected segment or resource. The menu is not available if the Igui is in
     * STATUS DISPLAY.
     */
    private class MouseListenerAdapter extends MouseAdapter implements ActionListener {

        private final static String enabledItemIconName = "treeComponentEnabled.png";
        private final static String disabledItemIconName = "treeComponentDisabled.png";

        private final ImageIcon disabledIcon = Igui.createIcon(MouseListenerAdapter.disabledItemIconName);
        private final ImageIcon enabledIcon = Igui.createIcon(MouseListenerAdapter.enabledItemIconName);

        private final JPopupMenu popUpMenu = new JPopupMenu();
        private final JMenuItem popUpMenuItem = new JMenuItem();

        private final List<SRTreeNode> selectedNodes = new ArrayList<SRTreeNode>(); // Access it only from the EDT

        /**
         * Runnable used to update the database list of disabled components.
         */
        private final class ComponentStateModifier implements Runnable {
            /**
             * Map containing the nodes whose disabled state is going to be modified. Each node is mapped to its new disabled state.
             * <p>
             * It has to be a thread safe map because it may be accessed both in the working thread and in the EDT.
             */
            private final ConcurrentMap<SRTreeNode, EnabledState> nodesToChange = new ConcurrentHashMap<SRTreeNode, EnabledState>();

            /**
             * List of nodes to be added to the disabled list.
             * <p>
             * Needed to revert the changes in case of failures writing them to the DB
             */
            private final List<SRTreeNode> addedToDisabledList = new LinkedList<SRTreeNode>();

            /**
             * List of nodes to be removed from the disabled list.
             * <p>
             * Needed to revert the changes in case of failures writing them to the DB
             */
            private final List<SRTreeNode> removedFromDisabledList = new LinkedList<SRTreeNode>();

            /**
             * List of nodes selected by the user but whose state cannot be changed
             */
            private final List<SRTreeNode> cannotDisableList = new LinkedList<SRTreeNode>();

            /**
             * Constructor.
             * 
             * @param node The nodes whose state is going to be changed
             */
            public ComponentStateModifier(final List<? extends SRTreeNode> nodes) {
                for(final SRTreeNode tn : nodes) {
                    this.nodesToChange.put(tn, EnabledState.ENABLED);
                }
            }

            /**
             * Here is the list of actions taken in this method:
             * <ul>
             * <li>The new state to set for the node is decided: if the node is enabled or indirectly disabled then its new status will be
             * EXPLICITELY_DISABLED, otherwise it will be ENABLED;
             * <li>The local list of disabled components is updated;
             * <li>The DB list of disabled component is updated using the local list;
             * <li>If the DB is correctly updated then the new node state is set.
             * </ul>
             * 
             * @see SegmentsResourcesPanel#updateLocalDisabledComponentList(dal.Component, boolean)
             * @see SegmentsResourcesPanel#updateDbDisabledComponentList()
             */
            @Override
            public void run() {
                SegmentsResourcesPanel.this.setBusy(true);
                SegmentsResourcesPanel.this.setBusyMessage("Updating the disabled state of components...");

                try {
                    // The new node state is set only after that the DB list of disabled components
                    // has been successfully updated.

                    for(final Entry<SRTreeNode, EnabledState> mapEntry : this.nodesToChange.entrySet()) {
                        final SRTreeNode tn = mapEntry.getKey();

                        if(tn.canBeDisabled() == false) {
                            this.cannotDisableList.add(tn);
                            mapEntry.setValue(tn.getEnabledState());
                        } else {
                            final EnabledState newState;

                            final EnabledState currentState = tn.getEnabledState();
                            final EnabledState dbState = tn.getEnabledState_DB();

                            if(!currentState.equals(dbState)) {
                                // This means that we are reverting the changes
                                newState = dbState;
                                SegmentsResourcesPanel.this.changedItems.decrementAndGet();
                            } else {
                                // Set the new enabled status
                                if(currentState.enabled() || currentState.equals(EnabledState.INDIRECTLY_DISABLED)) {
                                    newState = EnabledState.EXPLICITLY_DISABLED;
                                } else {
                                    newState = EnabledState.ENABLED;
                                }
                                SegmentsResourcesPanel.this.changedItems.incrementAndGet();
                            }

                            mapEntry.setValue(newState);

                            // Update the disabled items set
                            if(newState.enabled() || newState.equals(EnabledState.INDIRECTLY_DISABLED)) {
                                this.removedFromDisabledList.add(tn);
                                final boolean result = SegmentsResourcesPanel.this.updateLocalDisabledComponentList(tn.getUserObject().getDalComponent(),
                                                                                                                    false);
                                assert (result == true) : "ERROR removing " + tn.toString() + " from the disabled list";
                            } else {
                                this.addedToDisabledList.add(tn);
                                final boolean result = SegmentsResourcesPanel.this.updateLocalDisabledComponentList(tn.getUserObject().getDalComponent(),
                                                                                                                    true);
                                assert (result == true) : "ERROR adding " + tn.toString() + " to the disabled list";
                            }
                        }
                    }

                    if((this.removedFromDisabledList.isEmpty() == false) || (this.addedToDisabledList.isEmpty() == false)) {
                        // Update the DB list of disabled components
                        // This method may throw an IguiException.ConfigException exception
                        SegmentsResourcesPanel.this.updateDbDisabledComponentList();

                        // If we are here it means that the DB list of disabled
                        // components has been successfully updated

                        // Update the status of tree nodes
                        for(final Entry<SRTreeNode, EnabledState> mapEntry : this.nodesToChange.entrySet()) {
                            mapEntry.getKey().setEnabledState(mapEntry.getValue());
                        }

                        // Increment the database changes counter
                        SegmentsResourcesPanel.this.databaseChanges.getAndIncrement();

                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                // Fire the tree model to update tree rendering
                                for(final SRTreeNode tn : ComponentStateModifier.this.nodesToChange.keySet()) {
                                    SegmentsResourcesPanel.this.treeModel.nodeChanged(tn);
                                }

                                if(SegmentsResourcesPanel.this.changedItems.get() > 0) {
                                    SegmentsResourcesPanel.this.writeToolbarMessage("The database has been modified but changes have not been committed yet!");
                                } else {
                                    SegmentsResourcesPanel.this.writeToolbarMessage("");
                                }
                            }
                        });
                    }
                }
                catch(final ConfigException ex) {
                    // The DB component list update failed: revert changes in the local list
                    for(final SRTreeNode tn : this.addedToDisabledList) {
                        final boolean result = SegmentsResourcesPanel.this.updateLocalDisabledComponentList(tn.getUserObject().getDalComponent(),
                                                                                                            false);
                        assert (result == true) : "ERROR removing " + tn.toString() + " from the disabled list";
                    }

                    for(final SRTreeNode tn : this.removedFromDisabledList) {
                        final boolean result = SegmentsResourcesPanel.this.updateLocalDisabledComponentList(tn.getUserObject().getDalComponent(),
                                                                                                            true);
                        assert (result == true) : "ERROR adding " + tn.toString() + " to the disabled list";
                    }

                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            // Show error to the user
                            final StringBuilder nodes = new StringBuilder();
                            for(final SRTreeNode tn : ComponentStateModifier.this.nodesToChange.keySet()) {
                                nodes.append(tn.toString());
                                nodes.append(" ");
                            }

                            final String errMsg = "Failed to enable/disable components \"" + nodes.toString() + "\": " + ex.toString();
                            IguiLogger.error(errMsg, ex);
                            ErrorFrame.showError("Segments & Resources Panel Error", errMsg, ex);
                        }
                    });
                }
                finally {
                    SegmentsResourcesPanel.this.setBusy(false);

                    // Inform about nodes that cannot be disabled
                    if(this.cannotDisableList.isEmpty() == false) {
                        final StringBuilder nl = new StringBuilder();
                        for(final SRTreeNode n : this.cannotDisableList) {
                            nl.append(n.toString());
                            nl.append(" ");
                        }
                        nl.deleteCharAt(nl.length() - 1);

                        ErrorFrame.showInfo("The state of \"" + nl.toString() + "\" cannot be modified.");
                    }
                }
            }
        }

        MouseListenerAdapter() {
            this.popUpMenuItem.setBackground(Color.white);
            this.popUpMenuItem.setOpaque(true);
            this.popUpMenuItem.addActionListener(this);

            this.popUpMenu.add(this.popUpMenuItem);
            this.popUpMenu.addSeparator();
            this.popUpMenu.pack();
        }

        /**
         * Show the pop-up menu if the right or middle mouse button is clicked.
         * 
         * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseClicked(final MouseEvent e) {
            // React only if the right or middle mouse button is clicked
            if(javax.swing.SwingUtilities.isRightMouseButton(e) == true) {
                // Do not show the menu if in STATUS DISPLAY or the RC is in a state not allowing the DB modification
                final IguiConstants.AccessControlStatus al = Igui.instance().getAccessLevel();
                if((al.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY) == true)
                   && (Igui.instance().isDBChangeAllowed() == true))
                {
                    // Modify/clear the selection taking into account the mouse position
                    {
                        // Cannot use this afterwards, the selection may be changed
                        final TreePath[] selPaths = SegmentsResourcesPanel.this.segmentTree.getSelectionPaths();
                        final TreePath mouseSelectedPath = SegmentsResourcesPanel.this.segmentTree.getPathForLocation(e.getX(), e.getY());

                        boolean mouseOverSelection = false;

                        // The mouse is over the tree
                        if(selPaths != null) {
                            for(final TreePath tp : selPaths) {
                                if(tp.equals(mouseSelectedPath) == true) {
                                    mouseOverSelection = true;
                                    break;
                                }
                            }
                        }

                        if(mouseSelectedPath != null) {
                            if((selPaths == null) || (mouseOverSelection == false)) {
                                SegmentsResourcesPanel.this.segmentTree.setSelectionPath(mouseSelectedPath);
                            }
                        } else {
                            SegmentsResourcesPanel.this.segmentTree.clearSelection();
                        }

                    }

                    // Get the selected tree node
                    boolean showMenu = false;

                    final TreePath[] selPaths = SegmentsResourcesPanel.this.segmentTree.getSelectionPaths();
                    if(selPaths != null) {
                        if(selPaths.length > 1) {
                            // Multiple nodes are already selected
                            this.selectedNodes.clear();

                            for(final TreePath tp : selPaths) {
                                final Object lastNode = tp.getLastPathComponent();
                                if(SRTreeNode.class.isInstance(lastNode)) {
                                    this.selectedNodes.add((SRTreeNode) lastNode);
                                }
                            }

                            // In principle here the list may still be empty if none of the selected nodes is not of the right class
                            if(this.selectedNodes.isEmpty() == false) {
                                this.popUpMenuItem.setText("Switch the enabled/disabled status");
                                this.popUpMenuItem.setIcon(null);
                                showMenu = true;
                            }
                        } else {
                            // At most one node is selected
                            final TreePath currentSelectedPath = selPaths[0];
                            final Object selectedLastPathComponent = currentSelectedPath.getLastPathComponent();

                            // Check the node type: the root node (if visible) may be of a different type
                            if(SRTreeNode.class.isInstance(selectedLastPathComponent)) {
                                this.selectedNodes.clear();

                                final SRTreeNode sn = (SRTreeNode) selectedLastPathComponent;
                                this.selectedNodes.add(sn);

                                final EnabledState selectedNodeState = sn.getEnabledState();
                                if(selectedNodeState.enabled() || selectedNodeState.equals(EnabledState.INDIRECTLY_DISABLED)) {
                                    // The node can be disabled if it is enabled or indirectly disabled
                                    this.popUpMenuItem.setText("Disable " + sn.getTypeName().toLowerCase() + " " + sn.toString());
                                    this.popUpMenuItem.setIcon(this.disabledIcon);
                                } else {
                                    // The node can be enabled if it is nor disabled and indirectly disabled
                                    this.popUpMenuItem.setText("Enable " + sn.getTypeName().toLowerCase() + " " + sn.toString());
                                    this.popUpMenuItem.setIcon(this.enabledIcon);
                                }

                                showMenu = true;
                            }
                        }
                    }

                    if(showMenu == true) {
                        this.popUpMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            } else if(javax.swing.SwingUtilities.isMiddleMouseButton(e)) {
                final TreePath selectedPath = SegmentsResourcesPanel.this.segmentTree.getPathForLocation(e.getX(), e.getY());
                if(selectedPath != null) {
                    SegmentsResourcesPanel.this.segmentTree.setSelectionPath(selectedPath);
                    final Object selectedLastPathComponent = selectedPath.getLastPathComponent();
                    if(SRTreeNode.class.isInstance(selectedLastPathComponent)) {
                        final SRTreeNode selNode = (SRTreeNode) selectedLastPathComponent;
                        if(selNode.getEnabledState_DB().enabled()) {
                            ErrorFrame.showInfo(selNode.getDisabledReason());
                        } else {
                            ErrorFrame.showInfo("The component \"" + selNode.toString() + "\" is disabled because:\n"
                                                + selNode.getDisabledReason());
                        }
                    }
                }
            } else if(javax.swing.SwingUtilities.isLeftMouseButton(e)) {
                final TreePath selectedPath = SegmentsResourcesPanel.this.segmentTree.getPathForLocation(e.getX(), e.getY());
                if(selectedPath == null) {
                    SegmentsResourcesPanel.this.segmentTree.clearSelection();
                }
            }
        }

        /**
         * React to the user action: enable or disable segments and resources.
         * <p>
         * Since a communication with the RDB is involved, the action is executed by a SwingWorker.
         * 
         * @see ComponentStateModifier#run()
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            SegmentsResourcesPanel.this.dbUpdaterExecutor.execute(new ComponentStateModifier(this.selectedNodes));
        }
    }

    {
        this.helpFilName = IguiConstants.HELP_FILE_PREFIX + IguiConstants.HELP_PATH + "SegmentResourcePanel.htm";
        this.initGUI();
    }

    /**
     * Constructor: it builds the panel GUI.
     * 
     * @param mainIgui
     */
    public SegmentsResourcesPanel(final Igui mainIgui) {
        super(mainIgui);
    }

    /**
     * If the panel is de-selected but database changes are not committed then a dialog appears alerting the user.
     * 
     * @see Igui.IguiPanel#panelDeselected()
     */
    @Override
    public void panelDeselected() {
        if(this.databaseChanges.get() > 0) {
            this.showDiscardDbDialog("Some resources have been enabled/disabled but changes were\nnot committed to the database.");
        }
    }

    /**
     * @see Igui.IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return SegmentsResourcesPanel.panelName;
    }

    /**
     * @see Igui.IguiPanel#getTabName()
     */
    @Override
    public String getTabName() {
        return SegmentsResourcesPanel.panelName;
    }

    /**
     * It builds the root node and the child segments.
     * <p>
     * The root node is not showed but it is forced to exapnd to trigger the lazy loading mechanism on its children. The list of all the
     * disabled item is retrieved from database as well.
     * 
     * @see Igui.IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     * @throws IguiException Some error occurred getting information from database
     */
    @Override
    public void panelInit(final State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException {
        // Clear the local list of disabled components and reset counters
        this.changedItems.set(0);
        this.databaseChanges.set(0);
        this.disabledComponents.clear();

        try {
            // Load OnlineSegment
            final dal.OnlineSegment onlSeg = Igui.instance().getDalPartition().get_OnlineInfrastructure();
            if(onlSeg == null) {
                throw new IguiException.ConfigException("Unable to find the Online segment");
            }

            // Get all the disabled objects from db
            final dal.Component[] disabledComp = Igui.instance().getDalPartition().get_Disabled();
            if(disabledComp != null) {
                Collections.addAll(this.disabledComponents, disabledComp);
            }

            // Set the root node
            final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
            rootNode.add(new SRTreeNode(new DalSegment(onlSeg), false));

            // Load Partition Segments
            final dal.Segment[] segArray = Igui.instance().getDalPartition().get_Segments();
            if(segArray != null) {
                for(final dal.Segment seg : segArray) {
                    rootNode.add(new SRTreeNode(new DalSegment(seg)));
                }
            }

            // Expand root node
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    SegmentsResourcesPanel.this.writeToolbarMessage("");
                    SegmentsResourcesPanel.this.treeModel.setRoot(rootNode);
                    SegmentsResourcesPanel.this.treeModel.nodeStructureChanged(rootNode);
                    final TreeNode[] rootFirstChildPath = ((DefaultMutableTreeNode) rootNode.getFirstChild()).getPath();
                    SegmentsResourcesPanel.this.segmentTree.makeVisible(new TreePath(rootFirstChildPath));
                }
            });
        }
        catch(final IguiException ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new IguiException.IguiPanelError("Got exception while building top nodes", ex);
        }
    }

    /**
     * @see Igui.IguiPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        this.dbUpdaterExecutor.shutdown();
    }

    /**
     * This panel is not RC state aware.
     * 
     * @see Igui.IguiPanel#rcStateAware()
     */
    @Override
    public boolean rcStateAware() {
        return false;
    }

    /**
     * When the database changes are discarded the panel is re-initialized.
     */
    @Override
    public void dbDiscarded() {
        try {
            // Re-initialize the panel
            this.panelInit(Igui.instance().getRCState(), Igui.instance().getAccessLevel());
        }
        catch(final IguiException ex) {
            final String errMsg = "Failed rebuilding the \"Segments and Resources\" tree: " + ex.getMessage()
                                  + "\n. Tree components may not be shown correctly. ";
            IguiLogger.error(errMsg, ex);
            ErrorFrame.showError("Segments & Resources Panel Error", errMsg, ex);
        }
    }

    /**
     * After a database reload the tree is reset and initialized again.
     * 
     * @see Igui.IguiPanel#dbReloaded()
     * @see #panelInit(RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void dbReloaded() {
        // Reset the tree: collapse it and set a new root node
        // It is necessary to wait for this action (executed in the EDT) to finish
        // for updating the tree
        try {
            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    SegmentsResourcesPanel.this.treeModel.setRoot(null);
                    SegmentsResourcesPanel.this.treeModel.setRoot(new DefaultMutableTreeNode());
                }
            });

            // Reinitialize the tree
            this.panelInit(Igui.instance().getRCState(), Igui.instance().getAccessLevel());
        }
        catch(final Exception ex) {
            IguiLogger.error("Panel \"" + this.getPanelName() + "\" failed the DB reloading: " + ex.getMessage(), ex);
            ErrorFrame.showError("Segments & Resources Panel Error", "Database reload operation failed: " + ex.getMessage(), ex);
            if(InterruptedException.class.isInstance(ex)) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * It updates the local list of disabled items.
     * <p>
     * The list is initially retrieved from the database when the panel is initialized.
     * 
     * @see #panelInit(RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     * @param component The component that has been enabled or disabled
     * @param add If <code>true</code> the component is added to the list of disabled components (i.e., the component has been disabled).
     * @return <code>true</code> if the list has been successfully updated
     */
    private boolean updateLocalDisabledComponentList(final dal.Component component, final boolean add) {
        boolean result;

        if(add == true) {
            result = this.disabledComponents.add(component);
        } else {
            result = this.disabledComponents.remove(component);
        }

        return result;
    }

    /**
     * The database list of disabled components is updated.
     * <p>
     * This method implies a remote call to the RW Configuration.
     * 
     * @throws ConfigException Thrown if some error occurs updating the database information
     */
    private void updateDbDisabledComponentList() throws ConfigException {
        try {
            final config.Configuration rwConf = this.getRwDb();
            if(rwConf != null) {
                final dal.Partition p_rw = dal.Partition_Helper.get(rwConf, Igui.instance().getPartition().getName());
                if(p_rw != null) {
                    p_rw.set_Disabled(this.disabledComponents.toArray(new dal.Component[this.disabledComponents.size()]));
                } else {
                    final String errorMsg = "Cannot modify the selected object: the RW DAL Partition object cannot be found";
                    throw new IguiException.ConfigException(errorMsg);
                }
            } else {
                final String errorMsg = "Cannot modify the selected object: got null RW DAL Configuration."
                                        + " Most likely running in status display mode";
                throw new IguiException.ConfigException(errorMsg);
            }
        }
        catch(final IguiException.ConfigException ex) {           
            throw ex;
        }
        catch(final Exception ex) {
            final String errorMsg = "Cannot modify the selected object: " + ex.getMessage();
            throw new IguiException.ConfigException(errorMsg, ex);
        }
    }

    private void writeToolbarMessage(final String message) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        if((message != null) && (message.isEmpty() == false)) {
            this.messageLabel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.black, 3),
                                                                           BorderFactory.createEmptyBorder(3, 5, 3, 5)));
            this.messageLabel.setBackground(Color.WHITE);
            this.messageLabel.setForeground(Color.RED);
            this.messageLabel.setText(message);
        } else {
            this.messageLabel.setBorder(null);
            this.messageLabel.setBackground(null);
            this.messageLabel.setForeground(null);
            this.messageLabel.setText("");
        }
    }

    /**
     * It initializes the panel GUI.
     */
    private final void initGUI() {
        ToolTipManager.sharedInstance().registerComponent(this.segmentTree);

        // This cannot be done at the moment because the tree is lazy loaded...
        // final TreeSearchable ts = SearchableUtils.installSearchable(this.segmentTree);
        // ts.setCaseSensitive(false);
        // ts.setRecursive(true);
        // ts.setRepeats(true);
        // ts.setWildcardEnabled(true);
        // ts.setSearchingDelay(500);
        // ts.setSearchLabel("Search:");

        this.segmentTree.setRootVisible(false);
        this.segmentTree.setShowsRootHandles(true);
        this.segmentTree.setAutoscrolls(true);
        this.segmentTree.setLargeModel(true);
        this.segmentTree.setDoubleBuffered(true);
        this.segmentTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        final TreeCellRenderer treeRenderer = new TreeCellRenderer();
        treeRenderer.setLeafIcon(Igui.createIcon(SegmentsResourcesPanel.defaultLeafIconName));
        this.segmentTree.setCellRenderer(treeRenderer);
        this.segmentTree.addTreeWillExpandListener(new TreeWillExpandAdapter());
        this.segmentTree.addMouseListener(new MouseListenerAdapter());
        this.segmentTree.putClientProperty("JTree.lineStyle", "Angled");

        final JScrollPane segmentsTreeScrollPanel = new JScrollPane(this.segmentTree);
        segmentsTreeScrollPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        final JPanel leftEmptyPanel = new JPanel(true);
        leftEmptyPanel.setLayout(new BorderLayout());
        leftEmptyPanel.setBackground(Color.white);
        final JXLabel titleLabel = new JXLabel("Segments & Resources");
        titleLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 25));
        titleLabel.setTextRotation((3.0 * Math.PI) / 2.0);
        leftEmptyPanel.add(titleLabel, BorderLayout.NORTH);
        leftEmptyPanel.setOpaque(true);

        final JPanel northEmptyPanel = new JPanel(true);
        northEmptyPanel.setLayout(new BorderLayout());
        northEmptyPanel.setBackground(Color.white);
        northEmptyPanel.setOpaque(true);
        northEmptyPanel.setPreferredSize(new Dimension(300, 20));

        this.messageLabel.setOpaque(true);
        this.toolBar.add(Box.createHorizontalGlue());
        this.toolBar.add(this.messageLabel);
        this.toolBar.add(Box.createHorizontalStrut(1));
        this.toolBar.add(Box.createHorizontalGlue());

        this.setLayout(new BorderLayout());
        this.add(this.toolBar, BorderLayout.SOUTH);
        this.add(northEmptyPanel, BorderLayout.NORTH);
        this.add(leftEmptyPanel, BorderLayout.WEST);
        this.add(segmentsTreeScrollPanel, BorderLayout.CENTER);
    }
}
