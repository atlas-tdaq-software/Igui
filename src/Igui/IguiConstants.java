package Igui;

import java.awt.Color;


/**
 * Various constants used in the Igui package.
 * <p>
 * This class also contains enumerations for the access control level and the message severity used in the ERS panel.
 */
public final class IguiConstants {

    /**
     * The TDAQ software installation path
     */
    final static String INST_PATH = System.getenv("TDAQ_INST_PATH");

    /**
     * Path (relative to the TDAQ installation path) containing the online help
     */
    final static String HELP_RELATIVE_PATH = "/share/data/Igui/OnlineHelp/";
    
    /**
     * Property specifying the UID of the RDB_RW server
     */
    final static String RDBRW_NAME_PROPERTY = "igui.rdb_rw.name";
    
    /**
     * Default RDB implementation
     */
    final static String DB_DEFAULT_IMPLEMENTATION = "rdbconfig";

    /**
     * Property specifying the UID of the main RDB server
     */
    final static String RDB_NAME_PROPERTY = "igui.rdb.name";
    
    /**
     * Name of the run control IS server
     */
    final static String RC_IS_SERVER_NAME = "RunCtrl";

    /**
     * Name of the setup IS server
     */
    final static String SETUP_IS_SERVER_NAME = "Setup";

    /**
     * Name of the TriggerState IS server
     */
    final static String TRIGGERINFO_IS_INFO_NAME = IguiConstants.RC_IS_SERVER_NAME + ".TriggerState";

    /**
     * Name of the GlobalBusy IS server
     */
    final static String GLOBALBUSY_IS_INFO_NAME = "RunParams.GlobalBusy";

    /**
     * Name of IS server with information about the dead-time
     */
    final static String DEAD_TIME_IS_INFO_NAME = "L1CT.CTP.Instantaneous.BusyFractions";
    
    /**
     * Dead-time threshold
     */
    final static short DEAD_TIME_THRESHOLD = 5;
    
    /**
     * Name of the Ready4Physics IS server
     */
    final static String READY4PHYSICS_IS_INFO_NAME = "RunParams.Ready4Physics";

    /**
     * Name of the RunParams IS server
     */
    final static String RUNPARAMS_IS_INFO_NAME = "RunParams.RunParams";

    /**
     * Name of the RunInfo IS server
     */
    final static String RUNINFO_IS_INFO_NAME = "RunParams.RunInfo";

    /**
     * Name of the LuminosityInfo IS server
     */
    final static String LUMINFO_IS_INFO_NAME = "RunParams.LumiBlock";

    /**
     * Name of the IS server used to store the stop of run reason
     */
    final static String STOP_WHILE_BEAM_IS_INFO_NAME = "RunParams.StopWhileBeam";

    /**
     * Name of the IS server used to record that the trigger has been put on hold by the IGUI
     */
    final static String HOLD_TRIGGER_IS_SERVER = "RunParams";

    /**
     * Name of the IS information used to record that the trigger has been put on hold by the IGUI
     */    
    final static String HOLD_TRIGGER_IS_INFO_NAME = "HoldTrigger";
    
    /**
     * Name of the IS server used to retrieve/set the status of the Auto-pilot
     */
    final static String AUTOPILOT_IS_INFO_NAME = "RunCtrlStatistics.Autopilot";
    
    /**
     * Name of the beam status flag IS server
     */
    final static String BEAM_STATUS_IS_SERVER = "LHC.StableBeamsFlag";

    /**
     * Name of the LHC clock type info IS server
     */
    final static String CLOCK_TYPE_IS_SERVER = "LHC.RF2TTCApp-DCS";

    /**
     * Name of the run control supervision IS information
     */
    final static String SUPERVISION_IS_INFO_NAME = IguiConstants.RC_IS_SERVER_NAME + ".Supervision";

    /**
     * Property defining whether a warning dialog should be shown before the STOP transition command is sent to the RootController.
     * <p>
     * The (string) content of the property is the message to be shown to the operator.
     */
    final static String IGUI_STOP_MSG_PROPERTY = "igui.stop.warning";

    /**
     * Property defining whether a warning dialog should be shown before the STOP transition command is sent to the RootController.
     * <p>
     * The (string) content of the property is the message to be shown to the operator.
     */
    final static String IGUI_STOP_MSG_ENV_VAR = "TDAQ_IGUI_STOP_WARNING";
    
    /**
     * Property defining the partition the Igui is started in
     */
    final static String PARTITION_PROPERTY = "tdaq.ipc.partition.name";

    /**
     * Property defining the version of the TDAQ software
     */
    final static String TDAQ_VERSION_PROPERTY = "igui.tdaq.version";

    /**
     * Property defining the location of the Log4j configuration file
     */
    final static String TDAQ_LOG4J_CONF_PROPERTY = "igui.log4j.conf.file";

    /**
     * Environment variable defining the location of the Log4j configuration file
     */
    final static String TDAQ_LOG4J_CONF_ENV_VAR = "TDAQ_IGUI_LOG4J_CONF_FILE";

    /**
     * Property defining the location of the Igui configuration file
     */
    final static String IGUI_PROPERTY_FILE = "igui.property.file";

    /**
     * Name of the initial partition
     */
    final static String TDAQ_INITIAL_PARTITION = "initial";

    /**
     * Property defining the class name of panels to be created
     */
    final static String IGUI_PANELS_PROPERTY = "igui.panel";

    /**
     * Property defining the class name of user panels to be loaded when the Igui starts
     */
    final static String IGUI_PANELS_FORCE_LOAD_PROPERTY = "igui.panel.force.load";

    /**
     * Environment variabl defining the class name of user panels to be loaded when the Igui starts
     */    
    final static String IGUI_PANELS_FORCE_LOAD_ENV_VAR = "TDAQ_IGUI_PANEL_FORCE_LOAD";
    
    /**
     * Property specifying whether at the beginning the RM should be asked for CONTROL resource
     */
    final static String IGUI_USE_RM_PROPERTY = "igui.useRM";

    /**
     * Environment variable specifying whether at the beginning the RM should be asked for CONTROL resource
     */
    final static String IGUI_USE_RM_ENV_VAR = "TDAQ_IGUI_USE_RM";
    
    /**
     * Property specifying the maximum number of visible rows in the ERS panel
     */
    final static String ERS_PANEL_ROWS_PROPERTY = "igui.ers.rows";
    
    /**
     * Environment variable specifying the maximum number of visible rows in the ERS panel
     */
    final static String ERS_PANEL_ROWS_ENV_VAR = "TDAQ_IGUI_ERS_ROWS";
    
    /**
     * Property specifying the user profile (used at P1 to load default ERS filters)
     */
    final static String IGUI_USER_PROFILE = "igui.user.profile";

    /**
     * Property specifying the file (full path) where the ERS panel should look to find a default ERS filter
     */
    final static String IGUI_ERS_FILTER_FILE_PROPERTY = "igui.ers.filter.file";

    /**
     * Environment variable specifying the file (full path) where the ERS panel should look to find a default ERS filter
     */
    final static String IGUI_ERS_FILTER_FILE_ENV_VAR = "TDAQ_IGUI_ERS_FILTER_FILE";

    /**
     * Name of the script to use to start the Igui
     */
    final static String IGUI_START_SCRIPT = "Igui_start";

    /**
     * Property to start the Igui with a custom ERS subscription
     */
    final static String ERS_SUB_PROPERTY = "igui.ers.subscription";

    /**
     * Environment variable to start the Igui with a custom ERS subscription
     */
    final static String ERS_SUB_ENV_VAR = "TDAQ_IGUI_ERS_SUBSCRIPTION";
    
    /**
     * Path (in jar) to icon files
     */
    final static String IGUI_ICONS_JAR_REF = "/data/Images/";

    /**
     * Prefix to show help files
     * 
     * @see HelpFrame
     */
    final static String HELP_FILE_PREFIX = "file:";

    /**
     * Path to the location containing the online help files
     */
    final static String HELP_PATH = IguiConstants.getHelpPath();

    /**
     * Property defining the Igui debug level: if set to <code>true</code> then debug messages are shown.
     */
    final static String IGUI_DEBUG_PROPERTY = "tdaq.igui.debug";

    /**
     * Default IS info sources for event counters
     */
    final static String DEFAULT_EVENT_COUNTER_INFO_SERVER = "DF";
    final static String DEFAULT_EVENT_COUNTER_INFO_NAME = "HLTSV";
    final static String DEFAULT_EVENT_COUNTER_INFO_ATTRIBUTE = "ProcessedEvents";

    /**
     * Default IS info sources for event rates
     */
    final static String DEFAULT_EVENT_RATE_INFO_SERVER = "DF";
    final static String DEFAULT_EVENT_RATE_INFO_NAME = "HLTSV";
    final static String DEFAULT_EVENT_RATE_INFO_ATTRIBUTE = "Rate";

    /**
     * Timeout (in seconds) for the initial subscription to the SETUP IS server
     */
    final static int SETUP_TIMEOUT = 10;

    /**
     * Timeout (in seconds) for the Root Controller to reach the NONE state
     */
    final static int RC_NONE_STATE_TIMEOUT = 30;

    /**
     * Timeout (in seconds) for the partition to exit
     */
    final static int EXIT_TIMEOUT = 60;

    /**
     * Timeout (in seconds) for a panel to complete its action
     */
    final static int PANEL_ACTION_TIMEOUT = 15;

    /**
     * Max number of characters in option panes
     */
    final static int OPTION_PANE_MAX_CHAR_PER_LINE = 75;

    /**
     * Elog configuration property
     */
    final static String ELOG_SERVER_URL_PROPERTY = "igui.elog.url";

    /**
     * Elog configuration env variables
     */
    final static String ELOG_SERVER_URL_VAR = "TDAQ_ELOG_SERVER_URL";

    /**
     * Name of binaries for applications to be started from the Igui tool-bar
     */
    final static String ERS_EXEC = "start_ers_monitor";
    final static String IS_EXEC = "is_monitor";
    final static String DVS_EXEC = "dvs_start_gui";
    final static String EV_EXEC = "event_viewer.py";
    final static String DBE_EXEC = "dbe";
    final static String LM_EXEC = "log_manager";
    final static String OH_EXEC = "oh_display";
    final static String WORK_DIR = "/tmp/";

    /**
     * Env variable to detect if we are in the P1 environment
     */
    final static String P1_ENV_VAR = "TDAQ_SETUP_POINT1";

    // Environment variables and properties used to open the GIT web pages showing the database file history
    /**
     * The base web address of the GIT web interface (i.e., http://pc-tbed-git.cern.ch:3000/atlas-gitea/oks-tdaq-99-00-07/commit)
     * as an environment variable
     */
    final static String DB_GIT_BASE_ENV_VAR = "TDAQ_IGUI_DB_GIT_BASE";

    /**
     * The base web address of the GIT web interface (i.e., http://pc-tbed-git.cern.ch:3000/atlas-gitea/oks-tdaq-99-00-07/commit)
     * as a property
     */
    final static String DB_GIT_BASE_PROPERTY = "igui.git.url";
    
    /**
     * The name of the environment variable set when the OKS server is used
     */
    final static String TDAQ_DB_REPOSITORY_VAR = "TDAQ_DB_REPOSITORY";
    
    /**
     * The repository used for the OKS server (<em>null</em> if the OKS server is not used)
     */
    final static String TDAQ_DB_REPOSITORY = System.getenv(IguiConstants.TDAQ_DB_REPOSITORY_VAR);

    // NOTE: the Java API offers a Desktop connection to find the system browser, but it works
    // at the moment only if GNOME libraries are installed.
    /**
     * The browser the IGUI uses to open web pages (i.e., firefox, mozilla, etc).
     * Environment variable.
     */
    final static String USE_BROWSER_ENV_VAR = "TDAQ_IGUI_USE_BROWSER";

    /**
     * The browser the IGUI uses to open web pages (i.e., firefox, mozilla, etc).
     * Property.
     */
    final static String USE_BROWSER_PROPERTY = "igui.use.browser";

    /**
     * Environment variable used to decide whether to show or not the warning window at CONNECT
     */
    final static String HIDE_WARNING_AT_CONNECT_ENV_VAR = "TDAQ_IGUI_NO_WARNING_AT_CONNECT";

    /**
     * Property used to decide whether to show or not the warning window at CONNECT
     */
    final static String HIDE_WARNING_AT_CONNECT_PROPERTY = "igui.hide.connect.warning";

    /**
     * Property used to set the timeout (in minutes) to start the IGUI forced shutdown (when in status display)
     */
    final static String FORCED_STOP_TIMEOUT_MINUTES_PROPERTY = "tdaq.igui.forcedstop.timeout";

    /**
     * Environment variables used to set the timeout (in minutes) to start the IGUI forced shutdown (when in status display)
     */
    final static String FORCED_STOP_TIMEOUT_MINUTES_ENV_VAR = "TDAQ_IGUI_FORCEDSTOP_TIMEOUT";

    /**
     * Useful java property names
     */
    final static String classPathProperty = "java.class.path";
    final static String javaVersionProperty = "java.runtime.version";
    final static String javaHomeProperty = "java.home";
    final static String javaVMProperty = "java.vm.name";
    final static String userNameProperty = "user.name";

    /**
     * Enumeration for the access control states.
     */
    public enum AccessControlStatus {
        /**
         * It represents the status display access level: the user has not the Igui control.
         */
        DISPLAY(0, "StatusDisplay"),

        /**
         * It represents the access status giving the user the full Igui control.
         */
        CONTROL(1, "UserControl");

        /**
         * Integer used to make comparisons between different access level states
         */
        private final int state;

        /**
         * Name of the associated resource: it must be equal to resources defined in the configuration database.
         */
        private final String resourceName;

        /**
         * The enumeration object constructor.
         * 
         * @param state Simple integer to identify the enumeration element
         * @param resourceName Name of the associated resource
         */
        private AccessControlStatus(final int state, final String resourceName) {
            this.state = state;
            this.resourceName = resourceName;
        }

        /**
         * It compares this access level to <code>controlState</code>
         * 
         * @param controlState An access level state to compare to this
         * @return <code>true</code> if this access level is higher (i.e., gives more privileges to the user) than <code>controlState</code>
         */
        public boolean hasMorePrivilegesThan(final AccessControlStatus controlState) {
            return(this.state > controlState.state);
        }

        /**
         * It gives the name of the associated resource
         * 
         * @return The associated resource name
         */
        String resourceName() {
            return this.resourceName;
        }
    }

    /**
     * Enumeration for ERS messages severity.
     */
    public enum MessageSeverity {
        DEBUG(new Color(4783938), Color.black, Color.black, Color.white),
        LOG(new Color(13421823), Color.black, Color.black, Color.white),
        INFORMATION(new Color(7370745), Color.white, Color.black, Color.white),
        WARNING(new Color(16579594), Color.black, Color.black, Color.white),
        ERROR(Color.red, Color.white, Color.red, Color.white),
        FATAL(new Color(10027008), Color.white, new Color(10682368), Color.white);

        private final Color bgColor;
        private final Color fgColor;
        private final Color txtColor;
        private final Color txtBgColor;

        /**
         * Constructor.
         * 
         * @param bg Background color associated to the {@link MessageSeverity}
         * @param fg Foreground color associated to the {@link MessageSeverity}
         * @param txt Text color associated to the {@link MessageSeverity}
         * @param txtBg Text background color associated to the {@link MessageSeverity}
         */
        private MessageSeverity(final Color bg, final Color fg, final Color txt, final Color txtBg) {
            this.bgColor = bg;
            this.fgColor = fg;
            this.txtColor = txt;
            this.txtBgColor = txtBg;
        }

        /**
         * It gets the message severity from a string.
         * <p>
         * If for some reason the <code>severity</code> string does not correspond to any {@link MessageSeverity} enumeration, then a
         * warning message is logged and a default {@link MessageSeverity#INFORMATION} is returned.
         * 
         * @param severity String representing the message severity.
         * @return The {@link MessageSeverity} represented by the <code>severity</code> string.
         */
        public static final MessageSeverity severityFromString(final String severity) {
            MessageSeverity msgSeverity;

            try {
                msgSeverity = MessageSeverity.valueOf(severity);
            }
            catch(final Exception ex) {
                IguiLogger.warning("Unknown message severity \"" + severity + "\": " + ex.getMessage(), ex);
                msgSeverity = MessageSeverity.INFORMATION;
            }

            return msgSeverity;
        }

        /**
         * It gets message severity from a string.
         * <p>
         * Since the input is ers.Severity it should always be valid
         * </p>
         * 
         * @param severity ers.Severity of the message
         * @return The {@link MessageSeverity} represented by the <code>severity</code> string.
         */
        public static final MessageSeverity severityFromErsSeverity(final ers.Severity severity) {

            switch(severity.level()) {
                case Debug:
                    return MessageSeverity.DEBUG;
                case Log:
                    return MessageSeverity.LOG;
                case Information:
                    return MessageSeverity.INFORMATION;
                case Warning:
                    return MessageSeverity.WARNING;
                case Error:
                    return MessageSeverity.ERROR;
                case Fatal:
                    return MessageSeverity.FATAL;

                default:
                    IguiLogger.warning("Should never see this! Unknowk message severity \"" + severity.toString()
                                       + "\". Making it INFORMATION.");
                    return MessageSeverity.INFORMATION;
            }
        }

        /**
         * It returns the background color associated to the {@link MessageSeverity}
         * 
         * @return The background color associated to the {@link MessageSeverity}
         */
        public final Color getBgColor() {
            return this.bgColor;
        }

        /**
         * It returns the foreground color associated to the {@link MessageSeverity}
         * 
         * @return The foreground color associated to the {@link MessageSeverity}
         */
        public final Color getFgColor() {
            return this.fgColor;
        }

        /**
         * It returns the text color associated to the {@link MessageSeverity}
         * 
         * @return The text color associated to the {@link MessageSeverity}
         */
        public final Color getTxtColor() {
            return this.txtColor;
        }

        /**
         * It returns the text background color associated to the {@link MessageSeverity}
         * 
         * @return The text background color associated to the {@link MessageSeverity}
         */
        public final Color getTxtBgColor() {
            return this.txtBgColor;
        }
    }

    /**
     * Simple empty constructor
     */
    private IguiConstants() {
    }

    /**
     * It gives the full path to the online help files location
     * 
     * @return The path to the online help location
     */
    private final static String getHelpPath() {
        final StringBuilder str = new StringBuilder();

        if(IguiConstants.INST_PATH != null) {
            str.append(IguiConstants.INST_PATH + IguiConstants.HELP_RELATIVE_PATH);
        }

        return str.toString();
    }
}
