package Igui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;

import Igui.IguiConstants.MessageSeverity;
import Igui.IguiException.IguiPanelError;
import Igui.Common.BusyUI;
import Igui.Common.LockedUI;
import pmgClient.PmgClient;


/**
 * This is the parent class for all the Igui panels.
 * <p>
 * All the Igui panels have to extend this class and implement all the needed methods.
 * <ul>
 * <li><b>Layout: </b> This class does not force to use a defined layout. The user class can implement any of it.
 * <li><b>Threading: </b> Every IguiPanel panel will have its own thread where all the methods will be executed. In case of multiple
 * operations the tasks are queued and executed in a serial way. So, from the panel point of view, everything works in a single threaded
 * fashion. Only constructors are executed in the Swing EDT as required by the Swing policy. Please, remember to access any Swing component
 * in the EDT (see {@link javax.swing.SwingUtilities#invokeAndWait(Runnable)}, {@link javax.swing.SwingUtilities#invokeAndWait(Runnable)})
 * and avoid to execute there time consuming tasks (this may completely freeze the gui and, at least, make it unresponsive). In general any
 * operation requiring a remote call should be considered potentially time consuming (the remote service may be unavailable or just slow).
 * An easy way to cope with this situation is to use the {@link javax.swing.SwingWorker} class. If during some tasks the user should not be
 * able to access the panel, the panel itself can be disabled or put in a busy state using the {@link #disable(boolean)} and
 * {@link #setBusy(boolean)} methods.
 * <li><b>Access to the database configuration: </b> Two methods are provided to access the read-only ({@link #getDb()}) and read-write (
 * {@link #getRwDb()}) database configuration. Please, always use them to read or modify database items because all the database commit and
 * reload operations will be executed by the Igui using only those references. In this way an IguiPanel has not to implement its own way to
 * commit any database change and reload any database file. Database aborting operations should be avoided and performed only using the
 * interface provided by the {@link #showDiscardDbDialog(String)} method. The database reload is allowed only in very specific FSM states;
 * user panel should not allow any database modifications in other states and should use the {@link Igui#isDBChangeAllowed()} method to
 * check.
 * <li><b>The tool bar: </b> A {@link javax.swing.JToolBar} object ({@link #toolBar}) is provided for easily placing buttons or other
 * components.
 * <li><b>PMG interface: </b> If the panel needs to interact with the PMG, then use {@link #getPmgClient()} to get an entry point to the 
 * PMG client interface.
 * <li><b>Logging: </b> To produce log messages please use the {@link IguiLogger} class.
 * <li><b>Interface to the RC: </b> Two methods are provided giving an interface to the RC FSM:
 * {@link #rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)} and
 * {@link #rcTransitionCommandSending(Igui.RunControlFSM.Transition)}. Those methods will be called only if the {@link #rcStateAware()} will
 * return <code>true</code>.
 * <li><b>Interface to the e-log: </b> Two methods are provided giving an interface to the e-log: {@link #elogString()} and
 * {@link #showElogDialog(String, String, String)}.
 * <li><b>How to include your panel: </b> To include your panel in the Igui you can set the <code>igui.panel</code> property when the Igui
 * is started (i.e., execute the <code>Igui_start</code> script passing the <code>-Digui.panel="package_name.class_name"</code> option -
 * multiple panel definitions may be separated by a ":") or define it in the configuration database (<code>IGUIProperties</code>
 * relationship of the <code>SW_Repository</code> class).
 * </ul>
 */
abstract public class IguiPanel extends JPanel implements RCTransitionActions, IguiInterface {

    private static final long serialVersionUID = 131677118401706339L;

    /**
     * Icon file name for the help button
     */
    private final static String helpIconName = "help.png";

    /**
     * The help file name
     */
    protected volatile String helpFilName = null;

    /**
     * Is this a 'tabbed' panel (i.e., the main panel is an IguiPanel but it is not tabbed)?
     */
    private final boolean tabbed;

    /**
     * Tool bar useful to add user buttons (it will include the help button by default)
     */
    protected final JToolBar toolBar = new JToolBar(javax.swing.SwingConstants.HORIZONTAL);

    /**
     * The help button
     */
    private final JButton helpButton = new JButton(Igui.createIcon(IguiPanel.helpIconName));

    /**
     * The JXLayer object used to 'disable' this panel or put it in a 'busy' state
     */
    private final JXLayer<JComponent> panelLayer = new JXLayer<JComponent>(this);

    /**
     * The UI to make the 'busy' effect
     */
    private final LockableUI busyUI = new BusyUI();

    /**
     * The UI to make the 'disabled' effect
     */
    private final LockableUI lockedUI = new LockedUI();

    /**
     * Single executor where panel methods are executed
     */
    private final ThreadPoolExecutor executorThread = ExecutorFactory.createSingleExecutor(this.getClass().getName());

    // Init block
    {
        this.helpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiPanel.this.showHelp();
            }
        });
        this.helpButton.setOpaque(false);
        this.helpButton.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        this.toolBar.add(this.helpButton);
        this.toolBar.setFloatable(false);
        this.toolBar.setBorder(BorderFactory.createLineBorder(Color.black));

        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3),
                                                          BorderFactory.createBevelBorder(BevelBorder.LOWERED)));
    }

    /**
     * Constructor.
     * <p>
     * This constructor should be used only when the panel is executed as stand-alone and not in the Igui frame. It will <b>never</b> be
     * called by the Igui.
     */
    public IguiPanel() {
        super(true);
        this.tabbed = false;
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";
    }

    /**
     * Constructor.
     * <p>
     * This is the constructor called by the Igui to build the panel. It will always be executed in the EDT and it should only take care of
     * creating and initializing graphic components. Time consuming tasks needed to be executed before the panel is made visible should be
     * executed in the {@link #panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)} method.
     * 
     * @param mainIgui Reference to the {@link Igui} reference.
     */
    public IguiPanel(final Igui mainIgui) {
        super(true);
        this.tabbed = true;
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";
    }

    /**
     * Constructor.
     * <p>
     * This constructor is used internally to create panels extending IguiPanel but which should not be put in the Igui frame tab pane
     * (i.e., the main command and the ERS log panels).
     * 
     * @param tabbed If <code>true</false> the panel will be added to the tab pane.
     */
    IguiPanel(final boolean tabbed) {
        super(true);
        this.tabbed = tabbed;
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";
    }

    /**
     * Method to put the panel in a "busy" state.
     * <p>
     * A translucent layer (a glass pane, with a gamma filter applied) will be placed over the panel. This additional layer will intercept
     * all the mouse and keyboard events so that the panel will not be accessible to the user. But the panel itself will always be visible.
     * Some messages about why the panel is busy can be reported using the {@link #setBusyMessage(String)} method.
     * <p>
     * The method may be called from any thread.
     * 
     * @param busy If <code>true</code> the panel is put in a "busy" state, otherwise the panel is put in its "normal" state.
     */
    public void setBusy(final boolean busy) {
        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            this.setBusyImpl(busy);
        } else {
            try {
                javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        IguiPanel.this.setBusyImpl(busy);
                    }
                });
            }
            catch(final InterruptedException ex) {
                IguiLogger.warning(ex.toString(), ex);
                Thread.currentThread().interrupt();
            }
            catch(final InvocationTargetException ex) {
                final String errMsg = "Cannot put the panel \"" + this.getPanelName() + "\" in the busy state: " + ex;
                IguiLogger.error(errMsg, ex);
            }
        }
    }

    /**
     * Use this method to report messages when the panel is "busy".
     * <p>
     * For this method to be effective it should be called <b>after</b> the panel is put in a "busy" state. Multiple calls to this method
     * may be used to update the shown message.
     * 
     * @param message The message to be shown
     * @see #setBusy(boolean)
     */
    public void setBusyMessage(final String message) {
        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            ((BusyUI) this.busyUI).setBusyMessage(message);
        } else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ((BusyUI) IguiPanel.this.busyUI).setBusyMessage(message);
                }
            });
        }
    }

    /**
     * It "disables" the panel.
     * <p>
     * An additional layer (a glass pane, with an emboss filter applied) will be placed over the panel. This additional layer will intercept
     * all the mouse and keyboard events so that the panel will not be accessible to the user. The panel will look as "disable".
     * 
     * @param disable If <code>true</code> the panel is "disabled", otherwise is "enabled"
     */
    public void disable(final boolean disable) {
        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            this.disableImpl(disable);
        } else {
            try {
                javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        IguiPanel.this.disableImpl(disable);
                    }
                });
            }
            catch(final InterruptedException ex) {
                IguiLogger.warning(ex.toString(), ex);
                Thread.currentThread().interrupt();
            }
            catch(final InvocationTargetException ex) {
                final String errStr = "Cannot put the panel \"" + this.getPanelName() + "\" in the disabled state: " + ex;
                IguiLogger.error(errStr, ex);
            }
        }
    }

    /**
     * It gets a reference to the read-only database configuration object.
     * <p>
     * Subclasses are discouraged to create their own configuration object because database commit and reload procedures may not work as
     * expected.
     * 
     * @return A reference to the read-only database configuration object
     */
    public final config.Configuration getDb() {
        return Igui.instance().getDb();
    }

    /**
     * It gets a reference to the read-write database configuration object.
     * <p>
     * Subclasses are discouraged to create their own configuration object because database commit and reload procedures may not work as
     * expected.
     * 
     * @return A reference to the read-write database configuration object or <code>null</code> if the current access level is
     *         "status display"
     */
    public final config.Configuration getRwDb() {
        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            return Igui.instance().getRwDb();
        }

        return null;
    }

    /**
     * It returns an instance of the entry-point class to the PMG client library.
     * 
     * @return An instance of the entry-point class to the PMG client library.
     */
    public final PmgClient getPmgClient() {
        return Igui.instance().getPmgClient();
    }
    
    /**
     * Return this panel name.
     * <p>
     * Since this method may be used very early in the class constructor, it should do no more than just returning a simple string and not
     * rely on other class objects construction.
     */
    @Override
    abstract public String getPanelName();

    /**
     * Return the name to show for this panel in the Igui tab pane.
     * <p>
     * Since this method may be used very early in the class constructor, it should do no more than just returning a simple string and not
     * rely on other class objects construction.
     */
    @Override
    abstract public String getTabName();

    /**
     * Method implementing the panel initialization.
     * <p>
     * Subclasses should implement here all the actions to be executed before the panel is made visible. Typically this include some
     * interaction with the partition infrastructure. The Igui will call this method after the constructor has terminated its execution and
     * will make the panel visible only after this method has returned.
     * <p>
     * This method will always be executed in the panel own thread.
     * 
     * @param rcState The current Root Controller state: this information may be used to take different actions with respect to the FSM
     *            state.
     * @param controlStatus The current access level state: this information may be used to enable or disable buttons taking into account
     *            the current access level.
     * @throws IguiException Such an exception should be thrown if the panel is not able to successfully terminate its initialization. If
     *             the panel fails its initialization then it will not be added to the tab pane and will be terminated (i.e.,
     *             {@link #panelTerminated()}).
     * @see RunControlFSM.State
     * @see IguiConstants.AccessControlStatus
     */
    @Override
    abstract public void panelInit(final RunControlFSM.State rcState, final IguiConstants.AccessControlStatus controlStatus)
        throws IguiException;

    /**
     * Method called when the panel is removed from the tab pane (i.e., the associated repository is no more used) or the Igui is going to
     * exit.
     * <p>
     * Subclasses should use this method to implement all the clean-up procedures.
     * <p>
     * This method will always be executed in the panel own thread.
     */
    @Override
    abstract public void panelTerminated();

    /**
     * Subclasses should override this method to inform the Igui whether or not they want to be notified about changes of the Root
     * Controller state or when a transition command is going to be send to the RootController.
     * <p>
     * This method will always be executed in the panel own thread.
     * 
     * @return <code>true</code> if the panel wants to be notified about changes of the Root Controller state.
     * @see #rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)
     * @see #rcTransitionCommandSending(Igui.RunControlFSM.Transition)
     */
    @Override
    abstract public boolean rcStateAware();

    /**
     * Method called any time the panel is selected.
     * <p>
     * A typical usage of this method is to make subscriptions to services needed only when the user looks at the panel. Such subscriptions
     * may be removed when the panel is unselected ({@link #panelDeselected()}).
     * <p>
     * This method will always be executed in the panel own thread.
     * 
     * @see #panelDeselected()
     */
    @Override
    public void panelSelected() {
    }

    /**
     * Method called any time the panel is unselected.
     * <p>
     * A typical usage of this method is to remove subscriptions to services not needed only when the panel is not visible. Such
     * subscriptions may be renewed when the panel is selected again ({@link #panelSelected()}).
     * <p>
     * This method will always be executed in the panel own thread.
     * 
     * @see #panelSelected()
     */
    @Override
    public void panelDeselected() {
    }

    /**
     * Method called after the configuration database is reloaded.
     * <p>
     * Subclasses should override this method to update all the information extracted from the database after a database reload (following
     * some changes).
     * <p>
     * This method will always be executed in the panel own thread.
     */
    @Override
    public void dbReloaded() {
    }

    /**
     * Method called after some database changes have been discarded.
     * <p>
     * Subclasses should override this method to synch with the database after some database changes have been discarded.
     * <p>
     * This method will always be executed in the panel own thread.
     */
    @Override
    public void dbDiscarded() {
    }

    /**
     * Method called any time the access level changes.
     * <p>
     * Subclasses should override this method to implements actions to be taken when the access level changes (i.e., some buttons may be
     * enabled in CONTROL state but not in STATUS DISPLAY).
     * <p>
     * This method will always be executed in the panel own thread.
     */
    @Override
    public void accessControlChanged(final IguiConstants.AccessControlStatus newAccessStatus) {
    }

    /**
     * Method called when the "Re-subscribe to IS servers" item is selected in the "Command" menu in the main frame.
     * <p>
     * This method is useful if the panel relies on some IS information subscription but for some reason the subscription is lost.
     * <p>
     * This method will always be executed in the panel own thread.
     */
    @Override
    public void refreshISSubscription() {
    }

    /**
     * This method is part of the panel interface to the run control FSM.
     * <p>
     * It is called any time a state transition occurs but only if {@link #rcStateAware()} return <code>true</code>.
     * <p>
     * This method will always be executed in the panel own thread and the Igui will not wait for it to be executed before giving the user
     * the possibility to send a new state transition command to the Root Controller.
     * 
     * @param oldState The transition source state
     * @param newState The transition destination state
     * @see RunControlFSM
     */
    @Override
    public void rcStateChanged(final RunControlFSM.State oldState, final RunControlFSM.State newState) {
    }

    /**
     * This method is part of the panel interface to the run control FSM.
     * <p>
     * It is called any time a state transition command is going to be sent to the RootController but only if {@link #rcStateAware()} return
     * <code>true</code>.
     * <p>
     * This method will always be executed in the panel own thread.
     * 
     * @param transition The transition corresponding to the command the RootController is going to receive.
     * @deprecated
     */
    @Deprecated
    @Override
    public void rcTransitionCommandSending(final RunControlFSM.Transition transition) {
    }

    /**
     * Method to return a string to be added to any automatically created e-Log entry (basically at start and end of run).
     * <p>
     * This method will always be executed in the panel own thread.
     * 
     * @return The string to be added to the e-Log entry.
     */
    @Override
    public String elogString() {
        return "";
    }

    /**
     * It asks the IGUI to show a dialog the operator can use to fill an elog entry.
     * <p>
     * This method should not be called in the EDT. When it is called in the EDT it will be executed in the panel own thread, always return
     * <code>true</code> and never throw any exception in case of errors, but will simply show an error dialog reporting the cause of the
     * issue.
     * 
     * @param dialogTitle The title of the elog window dialog
     * @param entrySubject The subject of the elog entry to be submitted (this cannot be null or empty)
     * @param messageText The text to be appended to the elog entry (it will appear in the dialog text area)
     * @return <code>false</code> if the elog dialog cannot be shown because it is disabled (it can be enabled in the IGUI "Settings" menu)
     * @throws IguiPanelError Some error occurred trying to build and display the elog dialog
     */
    protected boolean showElogDialog(final String dialogTitle, final String entrySubject, final String messageText) throws IguiPanelError {
        // Check if EDT; if yes than submit to executor
        boolean toReturn = true;

        if(javax.swing.SwingUtilities.isEventDispatchThread() == true) {
            this.executorThread.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        final boolean enabled = Igui.instance().showElogDialog(IguiPanel.this.getPanelName(),
                                                                               dialogTitle,
                                                                               entrySubject,
                                                                               messageText);
                        if(enabled == false) {
                            final String errMsg = "Cannot start the ELOG dialog interface (requested by panel \""
                                                  + IguiPanel.this.getPanelName()
                                                  + "\"): the e-log interface is disabled (it can be enabled in the \"Setting\" menu)";
                            IguiLogger.warning(errMsg);
                            Igui.instance().internalMessage(MessageSeverity.WARNING, errMsg);
                        }
                    }
                    catch(final Exception ex) {
                        final String errMsg = "Cannot start the ELOG dialog interface (requested by panel \""
                                              + IguiPanel.this.getPanelName() + "\"): " + ex.getMessage()
                                              + ". The ELOG interface can be disabled in the \"Settings\" menu.";
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("IGUI - ELOG Dialog Error", errMsg, ex);
                    }
                }
            });
        } else {
            try {
                toReturn = Igui.instance().showElogDialog(this.getPanelName(), dialogTitle, entrySubject, messageText);
            }
            catch(final Exception ex) {
                throw new IguiPanelError(this.getPanelName(), "Cannot start the ELOG dialog interface: " + ex.getMessage(), ex);
            }
        }

        return toReturn;
    }

    /**
     * Method called to show the panel online help.
     * <p>
     * It is executed when the {@link #helpButton} JButton (on the tool bar) is pushed. Subclasses have to define the {@link #helpFilName}
     * variable pointing to the help file location.
     * <p>
     * It is safe to not execute this method in the EDT.
     */
    protected void showHelp() {
        if(this.helpFilName != null) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        final HelpFrame helpWindow = new HelpFrame(IguiPanel.this.helpFilName);
                        helpWindow.makeVisible();
                    }
                    catch(final IOException ex) {
                        final String errMsg = "This panel help cannot be shown:\nCannot open \"" + IguiPanel.this.helpFilName + "\": " + ex;
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("Online Help Error", errMsg, ex);
                    }
                }
            });
        } else {
            ErrorFrame.showWarning("This panel help cannot be showed:\nno help file has been defined.");
        }
    }

    /**
     * Use this method to ask the Igui to show a modal dialog asking the user whether it prefers to discard, postpone or commit the database
     * changes.
     * <p>
     * Typical usage is to show this message whenever the panel is de-selected and the database changes are not yet committed.
     * <p>
     * If changes will be postponed then the Igui will flag the panel tab with a "warning" style icon, remembering the user that there is
     * something to take care for that particular panel. That icon will be removed when the database changes are committed or discarded.
     * <p>
     * This method will have no effect if the Igui is in status display mode.
     * 
     * @param message A message which will be displayed in the shown modal dialog (i.e., which kind of changes have been applied to the
     *            database but not committed).
     */
    protected final void showDiscardDbDialog(final String message) {
        Igui.instance().showDbReloadOrDiscardDialog(message == null ? "" : message);
    }

    /**
     * It tells if the panel is a "tab" one or not.
     * 
     * @return <code>true</code> if the panel is a "tab" panel.
     */
    final boolean isTabbed() {
        return this.tabbed;
    }

    /**
     * It gets the JXLayer used to implement the "busy" and "disabled" UIs.
     * 
     * @return The JXLayer component.
     */
    final JXLayer<JComponent> getPanelLayer() {
        return this.panelLayer;
    }

    /**
     * Internal method to execute {@link #elogString()} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<String> execElogString() {
        return this.executorThread.submit(new Callable<String>() {
            @Override
            public String call() throws IguiException.IguiPanelError {
                try {
                    return IguiPanel.this.elogString();
                }
                catch(final Exception ex) {
                    IguiLogger.error("Exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #panelTerminated()} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execPanelTerminated() {
        final Future<?> task = this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.panelTerminated();
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
        this.executorThread.shutdown();

        return task;
    }

    /**
     * Internal method to execute {@link #panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)} in the panel own
     * thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execPanelInit(final RunControlFSM.State rcState, final IguiConstants.AccessControlStatus accessStatus) {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.panelInit(rcState, accessStatus);
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });

    }

    /**
     * Internal method to execute {@link #panelSelected()} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execPanelSelected() {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.panelSelected();
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #panelDeselected()} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execPanelDeselected() {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.panelDeselected();
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #dbReloaded()} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execDbReloaded() {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.dbReloaded();
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #dbDiscarded()} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execDbDiscarded() {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.dbDiscarded();
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #accessControlChanged(Igui.IguiConstants.AccessControlStatus)} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execAccessControlChanged(final IguiConstants.AccessControlStatus newAccessStatus) {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.accessControlChanged(newAccessStatus);
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #refreshISSubscription()} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execRefreshISSubscription() {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.refreshISSubscription();
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execRcStateChanged(final RunControlFSM.State oldState, final RunControlFSM.State newState) {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.rcStateChanged(oldState, newState);
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method to execute {@link #rcTransitionCommandSending(Igui.RunControlFSM.Transition)} in the panel own thread.
     * 
     * @return A Future representing the executed task
     */
    final Future<?> execRcTransitionCommandSending(final RunControlFSM.Transition trn) {
        return this.executorThread.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError {
                try {
                    IguiPanel.this.rcTransitionCommandSending(trn);
                    return null;
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception in panel \"" + IguiPanel.this.getPanelName() + "\": " + ex.getMessage(), ex);
                    // Make the exception visible to the Future
                    throw new IguiException.IguiPanelError(IguiPanel.this.getPanelName(), ex);
                }
            }
        });
    }

    /**
     * Internal method setting the "busy" UI.
     * 
     * @param busy If <code>true</code> the "busy" UI is installed
     * @see #setBusy(boolean)
     * @see #busyUI
     */
    private final void setBusyImpl(final boolean busy) {
        if((busy == true) && !(this.panelLayer.getUI() instanceof BusyUI)) {
            this.panelLayer.setUI(this.busyUI);
        }
        this.busyUI.setLocked(busy);
    }

    /**
     * Internal method setting the "disabled" UI.
     * 
     * @param disable If <code>true</code> the "disabled" UI is installed
     * @see #disable(boolean)
     * @see #lockedUI
     */
    private final void disableImpl(final boolean disable) {
        if((disable == true) && !(this.panelLayer.getUI() instanceof LockedUI)) {
            this.panelLayer.setUI(this.lockedUI);
        }
        this.lockedUI.setLocked(disable);
    }

}
