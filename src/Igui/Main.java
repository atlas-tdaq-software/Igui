package Igui;

import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;

import com.jidesoft.plaf.LookAndFeelFactory;


public class Main {

    /**
     * This class contains the <code>main</code> to start the Igui.
     * 
     * @param args Arguments
     */
    public static void main(final String[] args) {
        try {
            final String classPath = System.getProperty("java.class.path", "Unknown Class Path").replace(":", "\n");
            IguiLogger.info("Using Java class path:\n" + classPath);

            // Install the L&F
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        // *** When Nimbus will be available ***
                        // UIManager.put("nimbusBase", Color.BLUE.brighter());
                        // UIManager.put("nimbusSelectionBackground", Color.BLUE.brighter());
                        // UIManager.put("nimbusBlueGray", Color.BLUE);
                        // UIManager.put("control", Color.BLUE.brighter());
                        //
                        // for(LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                        // if("Nimbus".equals(info.getName())) {
                        // UIManager.setLookAndFeel(info.getClassName());
                        // break;
                        // }
                        // }
                        //
                        // new LookAndFeelFactory.NimbusInitializer().initialize(UIManager.getDefaults());

                        MetalLookAndFeel.setCurrentTheme(new OceanTheme());
                        javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                    }
                    catch(final Exception ex) {
                        IguiLogger.error("Cannot set the UI \"Look And Feel\"" + ex.getMessage(), ex);
                    }

                    // Needed to use Jide extensions
                    LookAndFeelFactory.installJideExtension();
                }
            });

            Igui.instance().init(true);
            Igui.instance().show();
        }
        catch(final Exception ex) {
            IguiLogger.error("The IGUI cannot be started: " + ex.getMessage(), ex);
            ErrorFrame.showError("IGUI Unrecoverable Error", "The IGUI cannot be started: " + ex.getMessage(), ex);
            Igui.instance().iguiExit(false, false);
        }
    }

}
