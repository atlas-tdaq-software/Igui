package Igui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import Igui.Common.MultiLineTable;


class AboutWindow {
    private final JFrame frame = new JFrame("Igui: About");
    private final MultiLineTable table;

    private static class InstanceHolder {
        private static AboutWindow instance = InstanceHolder.initInstance();

        private InstanceHolder() {
        }

        static AboutWindow getInstance() {
            return InstanceHolder.instance;
        }

        private static AboutWindow initInstance() {
            return new AboutWindow();
        }
    }

    private final class ValueColumnRenderer extends MultiLineTable.MultiLineCellRenderer {
        private static final long serialVersionUID = -8638791675028410441L;

        ValueColumnRenderer() {
            super();
            this.setFont(new Font("Dialog", Font.PLAIN, 12));
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table_,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            this.setText(value.toString());
            return this;
        }
    }

    private final static class KeyColumnRenderer extends JLabel implements TableCellRenderer {
        private static final long serialVersionUID = 7178571320157728386L;

        KeyColumnRenderer() {
            super();
            this.setOpaque(true);
            this.setHorizontalAlignment(SwingConstants.CENTER);
            this.setVerticalAlignment(SwingConstants.CENTER);
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table_,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            this.setText(value.toString());
            return this;
        }
    }

    private AboutWindow() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

        final DefaultTableModel tm = new DefaultTableModel();
        this.table = new MultiLineTable(tm);

        tm.addColumn("", this.getKeyColumnValues());
        tm.addColumn("", this.getValueColumnValues());

        this.table.setIntercellSpacing(new Dimension(0, 10));
        this.table.setShowGrid(false);
        this.table.setShowHorizontalLines(false);
        this.table.setRowHeight(30);
        this.table.getTableHeader().setReorderingAllowed(false);
        this.table.setColumnControlVisible(false);
        this.table.setSortable(false);
        this.table.setVisibleRowCount(30);
        this.table.setRolloverEnabled(false);
        this.table.setEditable(false);
        this.table.getTableHeader().setVisible(false);
        this.table.getColumnModel().getColumn(0).setCellRenderer(new KeyColumnRenderer());
        this.table.getColumnModel().getColumn(1).setCellRenderer(new ValueColumnRenderer());

        this.frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.frame.setLayout(new BorderLayout());

        final JScrollPane scrp = new JScrollPane(this.table);
        final JPanel p = new JPanel(new BorderLayout());
        p.add(scrp, BorderLayout.CENTER);
        p.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.frame.add(p, BorderLayout.CENTER);
    }

    static void show() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

        try {
            final AboutWindow w = InstanceHolder.getInstance();
            w.frame.setPreferredSize(new Dimension(800, 600));
            w.table.packAll();
            w.frame.pack();
            w.table.getColumnModel().getColumn(0).setPreferredWidth(200);
            w.table.getColumnModel().getColumn(1).setPreferredWidth(600);
            w.frame.setLocationRelativeTo(Igui.instance().getMainFrame());
            w.frame.setVisible(true);
        }
        catch(final Exception ex) {
            final String errMsg = "Failed creating the \"About\" window: " + ex;
            IguiLogger.error(errMsg, ex);
            ErrorFrame.showError("IGUI Error", errMsg, ex);
        }
    }

    private String[] getKeyColumnValues() {
        return new String[] {"TDAQ Software Version", "TDAQ Installation Path", "JAVA VM", "JAVA Home", "JAVA Classpath",
                             "JAVA VM Properties", "User Name"};
    }

    private String[] getValueColumnValues() {
        final StringBuilder props = new StringBuilder();

        {
            final Map<Object, Object> sortedMap = new TreeMap<Object, Object>(System.getProperties());
            final Set<Entry<Object, Object>> propEntries = sortedMap.entrySet();

            for(final Entry<Object, Object> e : propEntries) {
                if(e.getKey().toString().equals(IguiConstants.classPathProperty) == false) {
                    props.append("\"" + e.getKey().toString() + "\"");
                    props.append("=");
                    props.append(e.getValue().toString());
                    props.append("\n");
                }
            }
        }

        final String classPath = System.getProperty(IguiConstants.classPathProperty, "Unknown Class Path").replace(":", "\n");

        return new String[] {
                             System.getProperty(IguiConstants.TDAQ_VERSION_PROPERTY, "Unknown Version"),
                             IguiConstants.INST_PATH != null ? IguiConstants.INST_PATH : "Unknown",
                             System.getProperty(IguiConstants.javaVMProperty, "Unknown VM") + " runtime version "
                                 + System.getProperty(IguiConstants.javaVersionProperty, "unknown"),
                             System.getProperty(IguiConstants.javaHomeProperty, "Unknown"), classPath, props.toString(),
                             System.getProperty(IguiConstants.userNameProperty, "Unknown")};
    }

    // public static void main(final String[] args) {
    // javax.swing.SwingUtilities.invokeLater(new Runnable() {
    // @Override
    // public void run() {
    // AboutWindow.show();
    // }
    // });
    // }
}
