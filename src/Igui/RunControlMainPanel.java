package Igui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Igui.IguiConstants.AccessControlStatus;
import Igui.RunControlFSM.State;
import Igui.RunControlTreePanel.RCTreeNode;


/**
 * This panel contains and behaves as a communication hub between the Run Control Tree panel and all other Run Control sub-panels.
 * <p>
 * The main Igui has a reference only to this panel and does not know all the other sub-panels. This means that this panel is the only Run
 * Control panel to extend {@link IguiPanel} and it takes care of calling relevant methods on the other sub-panels.
 * <p>
 * It gives the opportunity to subscribe to changes of the tree selection and/or in the of the tree nodes status (see
 * {@link RunControlMainPanel#addTreeChangeSubscriber(AbstractRunControlPanel)} and
 * {@link RunControlMainPanel#addTreeSelectionSubscriber(AbstractRunControlPanel)}).
 * <p>
 * All the Run Control sub-panels will extends {@link AbstractRunControlPanel}.
 * <p>
 * It implements the {@link ChangeListener} interface so that it is able to notify the tab-panels when they are selected or de-selected.
 * <p>
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI Builder, which is free for non-commercial use. If Jigloo is
 * being used commercially (ie, by a corporation, company or business for any purpose whatever) then you should purchase a license for each
 * developer using Jigloo. Please visit www.cloudgarden.com for details. Use of Jigloo implies acceptance of these licensing terms. A
 * COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR ANY CORPORATE OR COMMERCIAL
 * PURPOSE.
 */

final class RunControlMainPanel extends IguiPanel implements ChangeListener {

    private static final long serialVersionUID = 5189160083481183803L;

    /**
     * This panel name
     */
    private final static String panelName = "Run Control";

    /**
     * The tab panel containing all the sub-panels
     */
    private final JTabbedPane rcTabbedPane;

    /**
     * The tree panel
     */
    private final RunControlTreePanel treePanel;

    /**
     * List containing all the sub-panels
     */
    private final List<AbstractRunControlPanel> subPanelList = new ArrayList<AbstractRunControlPanel>();

    /**
     * The currently selected tab panel
     */
    private final AtomicReference<AbstractRunControlPanel> selectedTabPanel = new AtomicReference<AbstractRunControlPanel>();

    /**
     * Default alignment
     */
    private final static float toolBarAlignment = Component.BOTTOM_ALIGNMENT;

    {
        this.helpFilName = IguiConstants.HELP_FILE_PREFIX + IguiConstants.HELP_PATH + "RunControlPanel.htm";
        this.rcTabbedPane = new JTabbedPane();

        // The RC tree panel should be created before all the other sub-panels (they could subscribe to tree selection/updates)
        this.treePanel = new RunControlTreePanel(this);
        Collections.addAll(this.subPanelList, new RunControlTestsPanel(this), new RunControlAdvancedPanel(this));
        this.initGUI();
    }

    /**
     * Constructor: it just builds the panel GUI.
     */
    public RunControlMainPanel(final Igui mainIgui) {
        super(mainIgui);
    }

    /**
     * @see IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return RunControlMainPanel.panelName;
    }

    /**
     * @see IguiPanel#getTabName()
     */
    @Override
    public String getTabName() {
        return RunControlMainPanel.panelName;
    }

    /**
     * It calls <code>refreshISSubscription()</code> on all the sub-panels.
     * 
     * @see IguiPanel#refreshISSubscription()
     * @see AbstractRunControlPanel#refreshISSubscription()
     * @see RunControlTreePanel#refreshISSubscription()
     * @see RunControlTestsPanel#refreshISSubscription()
     * @see RunControlAdvancedPanel#refreshISSubscription()
     */
    @Override
    public void refreshISSubscription() {
        this.treePanel.refreshISSubscription();

        for(final AbstractRunControlPanel p : this.subPanelList) {
            p.refreshISSubscription();
        }
    }

    /**
     * It calls <code>dbReloaded()</code> on all the tab-panels.
     * 
     * @see IguiPanel#dbReloaded()
     * @see AbstractRunControlPanel#dbReloaded()
     * @see RunControlTreePanel#dbReloaded()
     * @see RunControlTestsPanel#dbReloaded()
     * @see RunControlAdvancedPanel#dbReloaded()
     */
    @Override
    public void dbReloaded() {
        this.treePanel.dbReloaded();

        for(final AbstractRunControlPanel p : this.subPanelList) {
            p.dbReloaded();
        }
    }

    /**
     * The panel is initialized.
     * <p>
     * It calls <code>panelInit(State, AccessControlStatus)</code> on all the sub-panels.
     * 
     * @throws IguiException Thrown if the run control tree or one of the other sub-panels cannot be created
     * @see IguiPanel#panelInit(State, AccessControlStatus)
     * @see AbstractRunControlPanel#panelInit(State, AccessControlStatus)
     * @see RunControlTreePanel#panelInit(State, AccessControlStatus)
     * @see RunControlTestsPanel#panelInit(State, AccessControlStatus)
     * @see RunControlAdvancedPanel#panelInit(State, AccessControlStatus)
     */
    @Override
    public void panelInit(final State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException {
        this.treePanel.panelInit(rcState, accessStatus);

        for(final AbstractRunControlPanel p : this.subPanelList) {
            p.panelInit(rcState, accessStatus);
        }
    }

    /**
     * It calls <code>panelTerminated()</code> on all the sub-panels. It also removes the ChangeListener listener.
     * 
     * @see IguiPanel#panelTerminated()
     * @see AbstractRunControlPanel#panelTerminated()
     * @see RunControlTreePanel#panelTerminated()
     * @see RunControlTestsPanel#panelTerminated()
     * @see RunControlAdvancedPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        // Remove the change listener (panelDeselected may be called when removing the panel)
        this.rcTabbedPane.removeChangeListener(this);

        // Call panelTerminated() for all the child panels
        this.treePanel.panelTerminated();

        for(final AbstractRunControlPanel p : this.subPanelList) {
            p.panelTerminated();
        }

        IguiLogger.debug("Panel \"" + this.getPanelName() + "\" has been terminated");
    }

    /**
     * This panel is not RC state aware.
     * 
     * @see IguiPanel#rcStateAware()
     */
    @Override
    public boolean rcStateAware() {
        return false;
    }

    /**
     * It is called every time this panel is selected and it calls <code>panelSelected()</code> on the selected tab-panel.
     * <p>
     * This method action is scheduled to the EDT, so sub-panels should implement properly the <code>panelSelected</code> method.
     * 
     * @see IguiPanel#panelSelected()
     * @see AbstractRunControlPanel#panelSelected()
     * @see RunControlTreePanel#panelSelected()
     * @see RunControlTestsPanel#panelSelected()
     * @see RunControlAdvancedPanel#panelSelected()
     */
    @Override
    public void panelSelected() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final Component selPanel = RunControlMainPanel.this.rcTabbedPane.getSelectedComponent();
                if(selPanel != null) {
                    ((AbstractRunControlPanel) selPanel).panelSelected();
                }
            }
        });
    }

    /**
     * It is called every time this panel is de-selected and it calls <code>panelDeselected()</code> on the de-selected tab-panel.
     * <p>
     * This method action is scheduled to the EDT, so sub-panels should implement properly the <code>panelDeselected</code> method.
     * 
     * @see IguiPanel#panelDeselected()
     * @see AbstractRunControlPanel#panelDeselected()
     * @see RunControlTreePanel#panelDeselected()
     * @see RunControlTestsPanel#panelDeselected()
     * @see RunControlAdvancedPanel#panelDeselected()
     */
    @Override
    public void panelDeselected() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final Component selPanel = RunControlMainPanel.this.rcTabbedPane.getSelectedComponent();
                if(selPanel != null) {
                    ((AbstractRunControlPanel) selPanel).panelDeselected();
                }
            }
        });
    }

    /**
     * It is called every time a different tab panel is selected (or de-selected) and it calls <code>panelSelected</code> or
     * <code>panelDeselected</code> on sub-panels.
     * <p>
     * Those methods are executed in the EDT, so sub-panels should implement properly the <code>panelDeselected</code> and
     * <code>panelSelected</code> methods.
     * 
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     * @see AbstractRunControlPanel#panelDeselected()
     * @see RunControlTreePanel#panelDeselected()
     * @see RunControlTestsPanel#panelDeselected()
     * @see RunControlAdvancedPanel#panelDeselected()
     */
    @Override
    public void stateChanged(final ChangeEvent e) {
        // Get the new and the old selected panels
        final AbstractRunControlPanel newSelectedPanel = (AbstractRunControlPanel) this.rcTabbedPane.getSelectedComponent();
        final AbstractRunControlPanel oldSelectedPanel = this.selectedTabPanel.get();

        if(newSelectedPanel != oldSelectedPanel) {
            IguiLogger.debug("The new selected run control sub-panel is \"" + newSelectedPanel.getPanelName() + "\"");

            // Call panel methods
            newSelectedPanel.panelSelected();
            if(oldSelectedPanel != null) {
                oldSelectedPanel.panelDeselected();
            }
        }

        // Cache a reference to the new selected panel
        this.selectedTabPanel.compareAndSet(oldSelectedPanel, newSelectedPanel);
    }

    /**
     * It calls <code>accessControlChanged()</code> on all the sub-panels.
     * <p>
     * This method is NOT executed in the EDT.
     * 
     * @see IguiPanel#accessControlChanged(AccessControlStatus)
     * @see AbstractRunControlPanel#accessControlChanged(AccessControlStatus)
     * @see RunControlTreePanel#accessControlChanged(AccessControlStatus)
     * @see RunControlTestsPanel#accessControlChanged(AccessControlStatus)
     * @see RunControlAdvancedPanel#accessControlChanged(AccessControlStatus)
     */
    @Override
    public void accessControlChanged(final AccessControlStatus newAccessStatus) {
        this.treePanel.accessControlChanged(newAccessStatus);

        for(final AbstractRunControlPanel p : this.subPanelList) {
            p.accessControlChanged(newAccessStatus);
        }
    }

    /**
     * It gives a reference to the currently selected sub-panel.
     * <p>
     * It may return null if no panel has been selected yet.
     */
    AbstractRunControlPanel getSelectedSubPanel() {
        return this.selectedTabPanel.get();
    }

    /**
     * It returns the currently selected node in the Run Control tree.
     * <p>
     * Do not keep strong references to the returned object: in case of RC tree reloading, a reference to the node described by the returned
     * {@link RCTreeNode} object would be kept and this may imply keeping a reference to all the nodes in the old tree.
     * 
     * @see RunControlTreePanel#getSelectedNode()
     */
    RCTreeNode getSelectedTreeNode() {
        return this.treePanel.getSelectedNode();
    }

    /**
     * It returns the node in the Run Control tree corresponding to the application whose name is <em>appName</em>.
     * <p>
     * Do not keep strong references to the returned object: in case of RC tree reloading, a reference to the node described by the returned
     * {@link RCTreeNode} object would be kept and this may imply keeping a reference to all the nodes in the old tree.
     * 
     * @see RunControlTreePanel#getNode(String)
     */
    RCTreeNode getTreeNode(final String appName) {
        return this.treePanel.getNode(appName);
    }

    /**
     * It adds a subscriber to be notified any time the Run Control selected node changes.
     * 
     * @param panel The subscriber panel
     * @see AbstractRunControlPanel#treeNodeSelected(RCTreeNode)
     * @see RunControlTreePanel#treeNodeSelected(RCTreeNode)
     * @see RunControlTestsPanel#treeNodeSelected(RCTreeNode)
     * @see RunControlAdvancedPanel#treeNodeSelected(RCTreeNode)
     */
    void addTreeSelectionSubscriber(final AbstractRunControlPanel panel) {
        this.treePanel.addSelectionListener(panel.getTreeSelectionListener());
    }

    /**
     * It adds a subscriber to be notified every time a node in the Run Control tree changes its status.
     * 
     * @param panel The subscriber panel
     * @see AbstractRunControlPanel#treeNodesChanged(RCTreeNode[])
     * @see RunControlTreePanel#treeNodesChanged(RCTreeNode[])
     * @see RunControlTestsPanel#treeNodesChanged(RCTreeNode[])
     * @see RunControlAdvancedPanel#treeNodesChanged(RCTreeNode[])
     */
    void addTreeChangeSubscriber(final AbstractRunControlPanel panel) {
        this.treePanel.addModelListener(panel.getTreeModelListener());
    }

    /**
     * It adds a component to the tool-bar.
     * <p>
     * To be executed in the EDT.
     * 
     * @param c The component to add to the tool-bar
     */
    void addToToolbar(final JComponent c) {
        c.setAlignmentY(RunControlMainPanel.toolBarAlignment);
        this.toolBar.add(Box.createHorizontalGlue());
        this.toolBar.add(c);
    }

    /**
     * It setups the tool-bar: make sure that all the items in the tool-bar have the right alignment
     */
    private void setupToolbar() {
        for(int i = 0;; ++i) {
            final Component c = this.toolBar.getComponentAtIndex(i);
            if(c != null) {
                if(JComponent.class.isInstance(c)) {
                    ((JComponent) c).setAlignmentY(RunControlMainPanel.toolBarAlignment);
                }
            } else {
                break;
            }
        }
    }

    /**
     * Here the panel GUI is initialized.
     * <p>
     * Split panel -> RC tree panel + tab panel Tab panel -> infrastructure panel + advanced panel
     */
    private void initGUI() {
        this.setupToolbar();

        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(700, 450));

        this.treePanel.setPreferredSize(new Dimension(450, 450));

        this.rcTabbedPane.setTabPlacement(SwingConstants.BOTTOM);
        this.rcTabbedPane.addChangeListener(this);

        for(final AbstractRunControlPanel p : this.subPanelList) {
            this.rcTabbedPane.add(p.getPanelName(), p);
        }
        this.rcTabbedPane.setPreferredSize(new Dimension(250, 450));

        final JSplitPane splitPane = new JSplitPane();
        splitPane.setOneTouchExpandable(true);
        splitPane.add(this.rcTabbedPane, JSplitPane.RIGHT);
        splitPane.add(this.treePanel, JSplitPane.LEFT);
        splitPane.setDividerLocation(400);
        splitPane.setResizeWeight(0.85);

        this.add(splitPane, BorderLayout.CENTER);
        this.add(this.toolBar, BorderLayout.SOUTH);
    }

}
