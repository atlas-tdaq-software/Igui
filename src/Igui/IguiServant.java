package Igui;

import ipc.InvalidPartitionException;
import ipc.Partition;


/**
 * Class allowing to create a CORBA servant able to receive remote request.
 * <p>
 * The actual implementation of the actions to be executed is in {@link IguiServantImpl}.
 */
class IguiServant {
    private final Partition partition;
    private IguiServantImpl servantImpl;

    /**
     * Constructor.
     * <p>
     * It creates the underlining CORBA object.
     * 
     * @param p The partition the object belongs to
     */
    IguiServant(final Partition p) {
        this.partition = p;
        this.servantImpl = new IguiServantImpl(p);
    }

    /**
     * It publishes the CORBA object to IPC.
     * 
     * @throws InvalidPartitionException The partition is not valid
     */
    synchronized public void publish() throws InvalidPartitionException {
        try {
            this.servantImpl.publish();
        }
        catch(final IllegalStateException ex) {
            // The current CORBA object has been released: create a new one
            this.servantImpl = new IguiServantImpl(this.partition);
            this.servantImpl.publish();
        }
    }

    /**
     * It removes the IPC publication and releases the underlining CORBA object
     * 
     * @throws InvalidPartitionException The partition is not valid
     */
    synchronized public void withdraw() throws InvalidPartitionException {
        // Calling "release()" here is really mandatory.
        // Indeed this is the only way to deactivate the underlining CORBA object
        // If the object is not deactivated, then it will stay active and, potentially,
        // clients will keep the reference in their cache as valid (and will not ask
        // the naming server for a new reference).        
        try {
            this.servantImpl.withdraw();
            this.servantImpl.release();
        }
        catch(final IllegalStateException ex) {
            IguiLogger.debug("CORBA object already released", ex);
        }
    }
}
