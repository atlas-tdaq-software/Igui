package Igui;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import Igui.IguiException.ElogException;
import daq.eformat.DetectorMask.DETECTOR;
import elisa_client.model.InputMessage;
import elisa_client.model.OptionMetadata;
import elisa_client.service.ElisaClient;


class ElogInterface {
    private final String messageType;
    private final String centralDAQSystemAffected;
    private final Map<String, String[]> optionMap = new ConcurrentHashMap<String, String[]>();
    private final ElisaClient elisaClient;
    private final List<String> systemAffected;
    private final Map<String, DETECTOR> detMap = new ConcurrentHashMap<String, DETECTOR>();
    
    static {
        System.setProperty("elisaRest.URL", IguiConfiguration.instance().getElogURL());
    }

    ElogInterface() throws ElogException {
        final ApplicationContext appContext = new ClassPathXmlApplicationContext("java/src/main/resources/applicationContext.xml");
        
        this.elisaClient = appContext.getBean("elisaClient", ElisaClient.class);
        if(this.elisaClient == null) {
            throw new IguiException.ElogException("Got a null handler to connect to the e-log");
        }

        try {
            this.messageType = this.elisaClient.getMTforIGUI(null, null, null);
            
            this.systemAffected = this.elisaClient.getSystemsAffected(null, null, null, null).getList();

            final List<OptionMetadata> omdList = this.elisaClient.getMessageTypeOptions(null, null, null, this.messageType).getList();
            for(final OptionMetadata omd : omdList) {
                final String optionName = omd.getName();
                final String vals = omd.getPossibleValues();
                if(vals != null) {
                    this.optionMap.put(optionName, vals.split(","));
                } else {
                    this.optionMap.put(optionName, new String[] {});
                }
            }
            
            this.centralDAQSystemAffected = this.elisaClient.getSAforDetector(null, null, null, DETECTOR.DAQ);
            
            this.detMap.put(this.centralDAQSystemAffected, DETECTOR.DAQ);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.PIXEL), DETECTOR.PIXEL);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.SCT), DETECTOR.SCT);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.TRT), DETECTOR.TRT);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.LAR), DETECTOR.LAR);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.TILE), DETECTOR.TILE);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.MDT), DETECTOR.MDT);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.RPC), DETECTOR.RPC);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.TGC), DETECTOR.TGC);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.LVL1), DETECTOR.LVL1);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.BCM), DETECTOR.BCM);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.LUCID), DETECTOR.LUCID);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.ZDC), DETECTOR.ZDC);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.ALFA), DETECTOR.ALFA);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.HLT), DETECTOR.HLT);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.MICROMEGA), DETECTOR.MICROMEGA);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.AFP), DETECTOR.AFP);
            this.detMap.put(this.elisaClient.getSAforDetector(null, null, null, DETECTOR.STGC), DETECTOR.STGC);            
        }
        catch(final Exception ex) {
            throw new IguiException.ElogException("Failed to retrieve the e-log configuration: " + ex.getMessage(), ex);
        }
    }

    public DETECTOR getDetector(final String sa) {
        return this.detMap.get(sa);
    }    
    
    public String getCentralDAQSystemAffected() {
        return this.centralDAQSystemAffected;
    }
    
    public List<String> getSystemAffected() {
        return Collections.unmodifiableList(this.systemAffected);
    }

    public String getMessageType() {
        return this.messageType;
    }

    public String[] getAttributeValues(final String optionName) {
        return this.optionMap.get(optionName);
    }

    public boolean isAttributeValid(final String attributeName) {
        return this.optionMap.containsKey(attributeName);
    }

    public Set<String> getValidAttributes() {
        return Collections.unmodifiableSet(this.optionMap.keySet());
    }

    public void sendMessage(final InputMessage message, final String user, final String pwd) throws ElogException {
        try {
            this.elisaClient.insertMessage(user, pwd, null, message);
        }
        catch(final Exception ex) {
            throw new IguiException.ElogException("Failed to post the e-log entry:\n" + this.parseErrorMessage(ex.getMessage()), ex);
        }
    }

    public static boolean isElogEnvReady() {
        return !IguiConfiguration.instance().getElogURL().isEmpty();
    }

    private String parseErrorMessage(final String msg) {
        String returnMsg;

        try {
            final Document xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(msg.trim())));
            final XPath xpath = XPathFactory.newInstance().newXPath();
            final String status = xpath.evaluate("error_report/error/entry[@key='status']" + "/text()", xmlDoc);
            final String statusCode = xpath.evaluate("error_report/error/entry[@key='code']" + "/text()", xmlDoc);
            final String message = xpath.evaluate("error_report/error/entry[@key='message']" + "/text()", xmlDoc);
            returnMsg = message + " (status = " + status + ", code = " + statusCode + ")";
        }
        catch(final SAXException | IOException | ParserConfigurationException | XPathExpressionException ex) {
            // If the parsing fails, just return the initial string
            IguiLogger.warning("Failed to parse Elog XML error message: " + ex , ex);
            returnMsg = msg;
        }
            
        return returnMsg;
    }
    
//    public static void main(final String[] args) {
//        try {
//            final ElogInterface elogInterface = new ElogInterface();
//            final List<String> sysEff = elogInterface.getSystemAffected();
//            for(final String s : sysEff) {
//                System.err.println(s);
//            }
//        }
//        catch(final Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
