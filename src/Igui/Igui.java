package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.SplashScreen;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.jidesoft.swing.DefaultOverlayable;
import com.jidesoft.swing.MultilineLabel;
import com.jidesoft.swing.StyledLabelBuilder;

import Igui.IguiConstants.MessageSeverity;
import Igui.IguiException.ConfigException;
import Igui.IguiException.ISException;
import Igui.IguiException.PanelInitializationFailed;
import Igui.IguiException.PanelRegistrationFailed;
import Igui.IguiFrame.InitialInfrastructureFrame;
import Igui.RunControlApplicationData.ApplicationISStatus;
import Igui.RunControlTestsPanel.InitialInfrastructureTestsPanel;
import Igui.Common.BusyUI;
import Igui.Common.ProcessForker;
import config.Change;
import config.SystemException;
import config.Version;
import daq.rc.CommandSender;
import daq.rc.RCException;
import is.InfoEvent;
import oks.CommitError;
import oks.CommitError.type_t;
import pmgClient.ConfigurationException;
import pmgClient.CorbaException;
import pmgClient.Handle;
import pmgClient.LookupException;
import pmgClient.PmgClient;


/**
 * This is the Igui main class.
 * <p>
 * It manages all the Igui panels (creation, initialization and termination), the interaction with the partition infrastructure (i.e., IS
 * information subscriptions, database reload and committing, interface to the Resource Manager) and the Igui startup and shutdown
 * procedure. It does not deal with graphics (this is delegated to the {@link IguiFrame} class) but can message the main frame about updates
 * of the Root Controller state. It implements the singleton pattern so that one and only one reference to it is created. It keeps trace of
 * all the Igui panels and this is the only place where strong references to panels have to be stored (this is needed for panel dynamic
 * loading/unloading). It's this class duty to detect when a Root Controller state transition happens and to notify the subscribed panels.
 * <p>
 * The Igui can be started using the following code:
 * <ul>
 * <li>{@code Igui.instance().init(true);}
 * <li>{@code Igui.instance().show()}
 * </ul>
 */
public final class Igui {

    /**
     * The splash screen
     */
    private volatile SplashScreen splash;

    /**
     * The Graphics2D object used by the splash screen
     */
    private volatile Graphics2D splashGraphic;

    /**
     * A bold font used to write messages on the splash screen
     */
    private final Font bold = new Font(Font.SANS_SERIF, Font.BOLD, 12);

    /**
     * The Root Controller information data structure
     */
    private volatile RunControlApplicationData rootControllerData = null;

    /**
     * Executor used to perform periodic actions
     */
    private final ScheduledThreadPoolExecutor periodicExecutor =
                                                               ExecutorFactory.createSingleScheduledThreadPoolExecutor("Igui-Periodic-Checker");

    /**
     * Executor used to process IS call-backs from Root Controller
     */
    private final ThreadPoolExecutor rcStateChangeExecutor = ExecutorFactory.createSingleExecutor("Igui-RC-Subscriber");

    /**
     * Executor used to process database update call-backs
     */
    private final ThreadPoolExecutor dbReloadedExecutor = ExecutorFactory.createSingleExecutor("Igui-DB-Reloading");

    /**
     * Executor used to start processes (using tool-bar buttons)
     */
    private final ThreadPoolExecutor processExecutor = ExecutorFactory.createSingleExecutor("Igui-Process-Forker");

    /**
     * Executor used to re-subscribe panels to IS servers
     */
    private final ThreadPoolExecutor isRefresherExecutor = ExecutorFactory.createSingleExecutor("Igui-IS-Refresher");

    /**
     * Executor used to create/destroy user panels on demand
     */
    // Keep this executor single threaded. That means that panels can be loaded/removed using the frame menu
    // only serially. But having multiple threads doing that would make extremely difficult to queue loading and
    // removal for the same panel (the user could select and deselect the same panel from the list very quickly
    // and the threads may interfere each other).
    private final ThreadPoolExecutor onDemandPanelLoader = ExecutorFactory.createSingleExecutor("Igui-panel-creator");

    /**
     * Map containing references to all IguiPanels: this is the only place where IguiPanel strong references have to be stored.
     */
    private final ConcurrentMap<Class<? extends IguiPanel>, IguiPanel> panelMap =
                                                                                new ConcurrentHashMap<Class<? extends IguiPanel>, IguiPanel>();

    /**
     * ArrayList containing the class name of the panels to build: it will contain only the panels which will never be removed from the tab
     * pane.
     * <p>
     * Access to this set is guarded by {@link #panelListLock}
     */
    private final Set<String> privateTabPanelList = new LinkedHashSet<String>();

    /**
     * ArrayList containing the class name of the panels which will be dynamically created and removed.
     * <p>
     * Access to this set is guarded by {@link #panelListLock}
     */
    private final Set<String> userTabPanelList = new LinkedHashSet<String>();

    /**
     * Lock associated to {@link #privateTabPanelList} and {@link #userTabPanelList}
     */
    private final ReentrantReadWriteLock panelListLock = new ReentrantReadWriteLock();

    /**
     * Write lock associated to {@link #privateTabPanelList} and {@link #userTabPanelList}
     */
    private final ReentrantReadWriteLock.WriteLock panelListLockW = this.panelListLock.writeLock();

    /**
     * Read lock associated to {@link #privateTabPanelList} and {@link #userTabPanelList}
     */
    private final ReentrantReadWriteLock.ReadLock panelListLockR = this.panelListLock.readLock();

    /**
     * The current IPC Partition
     */
    private volatile ipc.Partition partition;

    /**
     * Object dealing with remote calls and IPC publication
     */
    private volatile IguiServant servant;

    /**
     * Object used to process RootController IS call-backs
     */
    private volatile ISInfoReceiver rootCtrlReceiver;

    /**
     * The read-only dal.Configuration object
     */
    private volatile config.Configuration db;

    /**
     * The read-write dal.Configuration object
     */
    private volatile config.Configuration dbRW;

    /**
     * The DAL partition object (RO RDB)
     */
    private volatile dal.Partition dalPartition;

    /**
     * The Online Segment
     */
    private volatile dal.Segment onlineSegment;

    /**
     * The DAL converter used for variable substitution (RO RDB)
     */
    private volatile dal.SubstituteVariables dalConverter;

    /**
     * Object implementing the interface to the Resource Manager
     */
    private final RMInterface rmInterface = new RMInterface();

    /**
     * Object implementing the interface to the rdb.Cursor IPC object: used only to reload the database.
     */
    private final RDBInterface rdbInterface = new RDBInterface();

    /**
     * The access status level: the default is status display.
     */
    private volatile IguiConstants.AccessControlStatus rmAccessLevel = IguiConstants.AccessControlStatus.DISPLAY;

    /**
     * The Igui main frame
     */
    private final IguiFrame mainFrame;

    /**
     * Object implementing database modifications subscription
     */
    private volatile config.Subscription dbSubscription = null;

    /**
     * The "Run Control" panel class name
     */
    private final static String rcMainPanelClass = "Igui.RunControlMainPanel";

    /**
     * The "Segment and Resources" panel class name
     */
    private final static String segPanelClass = "Igui.SegmentsResourcesPanel";

    /**
     * The "Dataset Tag" panel class name
     */
    private final static String dsPanelClass = "Igui.DSPanel";

    /**
     * The Commander object used to send commands to controllers
     */
    private volatile CommandSender rcCommander;

    /**
     * Lock used to detect when the initial infrastructure is ready and the full Igui can be shown
     */
    private final ReentrantLock initialInfrReadyLock = new ReentrantLock();

    /**
     * Condition variable associated to {@link #initialInfrReadyLock}
     */
    private final Condition initialInfrReadyCond = this.initialInfrReadyLock.newCondition();

    /**
     * The frame showing the initial infrastructure test gui.
     */
    private volatile InitialInfrastructureFrame initialInfrFrame;

    /**
     * Boolean used to keep trace of pending DB changes.
     * <p>
     * This boolean is reset to <code>false</code> any time the DB is successfully reloaded or discarded.
     * 
     * @see #setDbChangesPending(boolean)
     * @see #dbChangesPending()
     */
    private final AtomicBoolean pendingDbChanges = new AtomicBoolean(false);

    /**
     * Latch used to start the Root Controller IS call-backs processing only after all the panels have been built.
     * <p>
     * Without doing that it may happen that a call-back is received after the panel has been initialized but before the panel is registered
     * for state transition notifications.
     */
    private final CountDownLatch isCallBackLatch = new CountDownLatch(1);

    /**
     * R/W lock used to protect IS call-back executions and panel creations after the initial setup procedure.
     */
    private final ReentrantReadWriteLock isCallBackLock = new ReentrantReadWriteLock();

    /**
     * <code>true</code> if the config layer uses the GIT back-end.
     */
    private final AtomicBoolean configUsesGitBackend = new AtomicBoolean(false);
        
    /**
     * That is the task actually checking whether the database has been externally modified
     */
    private final DbChangesChecker dbChagesCheckerTask = new DbChangesChecker();
    
    /**
     * The shutdown hook
     */
    private final Thread shutdownHook = new ShutdownHook();

    /**
     * Singleton to lazy-load the PMG client interface
     */
    private static class PmgClientBridge {
        private static class Holder {
            private final static PmgClient INSTANCE = Holder.init();

            private static PmgClient init() {
                try {
                    return new PmgClient();
                }
                catch(final ConfigurationException ex) {
                    // Translate that in an un-checked exception
                    // There is really nothing to do if that happens
                    throw new RuntimeException(ex);
                }
            }
        }

        private PmgClientBridge() {
        }

        static PmgClient instance() {
            return Holder.INSTANCE;
        }
    }
    
    /**
     * Shutdown hook to make a minimal clean-up when the Igui is closed in an unclean way.
     * <p>
     * The clean-up includes the removal of IS and DB subscriptions, of the IPC reference and the release of the RM token. The hook is
     * installed just after the Igui initialization and is removed before exiting normally (in this way it is not called multiple times).
     * <p>
     * This includes some code duplication from the {@link ExitThread}, but here only a few actions are performed because the shutdown hook
     * should be kept as fast as possible.
     */
    private final class ShutdownHook extends Thread {
        ShutdownHook() {
        }

        @Override
        public void run() {
            IguiLogger.info("The shutdown hook is starting its clean-up...");

            // Remove IPC publication
            try {
                if(Igui.this.servant != null) {
                    Igui.this.servant.withdraw();
                    IguiLogger.info("IPC publication has been removed (if previously done)");
                }
            }
            catch(final ipc.InvalidPartitionException ex) {
                // This is not actually a problem
                IguiLogger.error("The IPC publication has not been removed because the IPC partition cannot be reached: " + ex, ex);
            }
            catch(final Exception ex) {
                IguiLogger.error("Unexpected exception removing the IPC reference", ex);
            }

            // Un-subscribe from IS
            try {
                if(Igui.this.rootCtrlReceiver != null) {
                    final String msg = "Unsubscribing from RunCtrl IS server";
                    IguiLogger.info(msg);

                    Igui.this.rootCtrlReceiver.unsubscribe();
                }
            }
            catch(final Exception ex) {
                IguiLogger.error("Failed to unsubscribe from RunCtrl IS server: " + ex.getMessage(), ex);
            }
            finally {
                Igui.this.rcStateChangeExecutor.shutdown();
            }

            // Unsubscribe from db
            try {
                if((Igui.this.db != null) && (Igui.this.dbSubscription != null)) {
                    final String msg = "Unsubscribing from database";
                    IguiLogger.info(msg);

                    Igui.this.db.unsubscribe(Igui.this.dbSubscription);
                }
            }
            catch(final Exception ex) {
                IguiLogger.error("Database unsubscription failed: " + ex.getMessage(), ex);
            }
            finally {
                Igui.this.dbReloadedExecutor.shutdown();
            }

            // Close RDB RW session
            try {
                if(Igui.this.dbRW != null) {
                    final String msg = "Closing the RDB RW session";
                    IguiLogger.info(msg);

                    Igui.this.dbRW.unload();
                }
            }
            catch(final Exception ex) {
                IguiLogger.error("Failed to close the RDB RW session: " + ex, ex);
            }

            // Free RM resources
            try {
                Igui.this.rmInterface.freeCurrentResource();

                final String msg = "RM resources (if any) have been freed";
                IguiLogger.info(msg);
            }
            catch(final Exception ex) {
                IguiLogger.error("Failed freeing RM resources: " + ex.getMessage(), ex);
            }
        }
    }

    /**
     * {@link Runnable} implementing the actions needed to verify whether the database has been 
     * externally modified and the corresponding changes have not been reloaded yet.
     * The check is not done if the IGUI is in display mode.
     * <p>
     * Submitted to the {@link Igui#periodicExecutor} executor.
     * <p>
     * The notification to the {@link IguiFrame} of eventual changes can be paused and resumed using
     * {@link DbChangesChecker#pause()} and {@link DbChangesChecker#resume()}.
     * <p>
     * @see IguiFrame#notifyExternalDbChanges(boolean)
     */
    private final class DbChangesChecker implements Runnable {
        private boolean pause = false;
        
        DbChangesChecker() {            
        }
        
        @Override
        public void run() {
            if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY) == true) {
                try {
                    // This is out of the synchronization block, it may take a long time to execute
                    final Version[] updates = Igui.this.db.get_changes();
    
                    synchronized(this) {
                        if(this.pause == false) {                        
                            if(updates.length != 0) {
                                Igui.this.mainFrame.notifyExternalDbChanges(true);
                            } else {
                                Igui.this.mainFrame.notifyExternalDbChanges(false);
                            }
                        }                                               
                    }
                }
                catch(final SystemException | NullPointerException ex) {
                    IguiLogger.warning("Could not check external updates to the database: " + ex, ex);
                }
            } else {
                Igui.this.mainFrame.notifyExternalDbChanges(false);
            }
        }        
        
        /**
         * It is guaranteed that, after calling this method, the {@link IguiFrame} is not notified anymore.
         */
        void pause() {
            synchronized(this) {
                this.pause = true;                
            }
        }
        
        /**
         * It enables again the notification to the {@link IguiFrame}.
         */
        void resume() {
            synchronized(this) {
                this.pause = false;                
            }
        }
    }
    
    /**
     * Runnable submitted to the {@link Igui#periodicExecutor} scheduled executor.
     * <p>
     * This runnable checks whether the partition is running or not. If the partition is not running the Igui is put in busy state and the
     * user is informed. When the partition is up again a new Igui instance is started and this Igui is stopped.
     * <p>
     * The check is done only when the Igui runs in status display mode. There is no need to do it in control mode.
     */
    private final class PartitionChecker implements Runnable {
        private final AtomicBoolean hasBeenStopped = new AtomicBoolean(false);

        PartitionChecker() {
        }

        @Override
        public void run() {
            // Check whether the partition is up but only if the Igui is in status display mode
            if(Igui.this.rmAccessLevel.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY) == false) {
                final boolean validPart = Igui.this.partition.isValid();
                if(validPart == false) {
                    // The partition has been stopped, keep trace of it and inform the user
                    this.hasBeenStopped.set(true);
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            final String msg =
                                             "The partition has been stopped: the Igui will be automatically restarted as soon as the partition is up again";
                            IguiLogger.info(msg);
                            Igui.this.mainFrame.setBusy(true);
                            Igui.this.mainFrame.setBusyMessage(msg);
                        }
                    });
                } else {
                    // The partition is up, check whether it was down previously: the Igui has to be restarted only in that case
                    if(this.hasBeenStopped.get() == true) {
                        // Keep trace that the partition is up again
                        this.hasBeenStopped.set(false);

                        IguiLogger.info("The partition is up again: a new Igui instance will be started");

                        // Start a new Igui
                        try {
                            Runtime.getRuntime().exec(IguiConstants.IGUI_START_SCRIPT + " -p " + Igui.this.partition.getName());
                        }
                        catch(final IOException ex) {
                            final String errMsg = "A new Igui instance cannot be started: " + ex;
                            IguiLogger.error(errMsg, ex);
                            ErrorFrame.showError("IGUI Error", errMsg, ex);
                        }

                        // Exit this Igui
                        Igui.this.iguiExit(false, true);
                    }
                }
            }
        }
    }

    /**
     * SwingWorker used to ask all the IguiPanels to refresh their subscription(s) to IS server(s).
     * <p>
     * In the background thread it will execute the IS refresh for all the panels and wait for all the panel actions to terminate. During
     * this procedure the main frame is put in a busy state and messages are reported about which panel is doing what.
     * <p>
     * This SwingWorker is always executed in the {@link Igui#isRefresherExecutor} executor.
     * 
     * @see IguiPanel#refreshISSubscription()
     * @see Igui#refreshISSubscriptions()
     */
    private final class RefreshISTask extends SwingWorker<Void, String> {
        public RefreshISTask() {
        }

        /**
         * The IS subscription refreshing actions are executed for all the panels. This method will return as soon as all the panel tasks
         * are terminated. The main frame is put in a busy state.
         * 
         * @see javax.swing.SwingWorker#doInBackground()
         * @throws InterruptedException
         */
        @Override
        protected Void doInBackground() throws InterruptedException {
            // Set the main frame busy
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Igui.this.mainFrame.setBusy(true);
                }
            });

            // Map to contain the panel tasks
            final Map<IguiPanel, Future<?>> taskMap = new HashMap<IguiPanel, Future<?>>();

            // Execute action on all the IguiPanels
            for(final IguiPanel p : Igui.this.panelMap.values()) {
                final String pName = p.getPanelName();
                {
                    final String msg = "Sending command to panel \"" + pName + "\" to refresh IS subscription(s)...";
                    IguiLogger.info(msg);
                    this.publish(msg);
                }
                final Future<?> task = p.execRefreshISSubscription();
                taskMap.put(p, task);
            }

            // Wait for all the panels to finish their job
            final Set<Map.Entry<IguiPanel, Future<?>>> es = taskMap.entrySet();
            for(final Map.Entry<IguiPanel, Future<?>> me : es) {
                final String panelName = me.getKey().getPanelName();
                try {
                    {
                        final String msg = "Waiting for panel \"" + panelName + "\" to re-subscribe to IS server(s)...";
                        IguiLogger.info(msg);
                        this.publish(msg);
                    }
                    me.getValue().get(IguiConstants.PANEL_ACTION_TIMEOUT, TimeUnit.SECONDS);
                    {
                        final String msg = "Panel \"" + panelName + "\" finished to re-subscribe to IS server(s)";
                        IguiLogger.info(msg);
                        this.publish(msg);
                    }
                }
                catch(final InterruptedException ex) {
                    throw new InterruptedException("IS re-subscriber thread has been interrupted: " + ex);
                }
                catch(final ExecutionException ex) {
                    final Throwable cause = ex.getCause();
                    final String errMsg = "Panel \"" + panelName + "\" failed to re-subscribe to IS server(s): " + cause;
                    IguiLogger.error(errMsg, ex);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                }
                catch(final TimeoutException ex) {
                    final String errMsg = "Timeout elapsed refreshing IS subscriptions for panel \"" + panelName + "\"";
                    IguiLogger.error(errMsg, ex);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                }
            }

            taskMap.clear();

            {
                // Refresh the RootController subscription
                final String msgBeg = "Refreshing main subscription to the Root controller state...";
                IguiLogger.info(msgBeg);
                this.publish(msgBeg);
                
                try {
                    Igui.this.rootCtrlReceiver.unsubscribe();
                }
                catch(final ISException ex) {
                    IguiLogger.warning("Failed to remove IS Root controller subscription: " + ex, ex);
                }
                
                try {
                    Igui.this.rootCtrlReceiver.subscribe();
                
                    final String msgEnd = "The main IS subscription to the Root controller state has been re-done...";
                    IguiLogger.info(msgEnd);
                    this.publish(msgEnd);
                }
                catch(final ISException ex) {
                    final String errMsg = "Failed to subscribe to the Root controller state, the GUI will not be properly updated anymore.\n" + 
                                          "Try again refreshing the IS subscription or restart the application.";
                    IguiLogger.error(errMsg + " Error is: " + ex, ex);
                    ErrorFrame.showError("IGUI Error", errMsg, ex);
                }                
            }
            
            return null;
        }

        /**
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                this.get();
            }
            catch(final InterruptedException ex) {
                IguiLogger.warning("EDT interrupted!!!");
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final Throwable cause = ex.getCause();
                final String errMsg = "Unexpected error during panel IS re-subscription: " + cause;
                IguiLogger.error(errMsg, ex);
                ErrorFrame.showError("IGUI Panel(s) Error", errMsg, ex);
            }
            finally {
                // Remove the main frame busy status
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Igui.this.mainFrame.setBusy(false);
                    }
                });
            }
        }

        /**
         * It logs all the messages and writes to the glass pane the last one.
         * 
         * @see javax.swing.SwingWorker#process(java.util.List)
         */
        @Override
        protected void process(final List<String> chunks) {
            // Show on the glass panel only the last message
            Igui.this.mainFrame.setBusyMessage(chunks.get(chunks.size() - 1));
        }
    }

    /**
     * SwingWorker implementing the database reload action.
     * <p>
     * In the background thread it will execute the database reload for all the panels and wait for all the panel actions to terminate.
     * During this procedure the main frame is put in a busy state and messages are reported about which panel is doing what.
     * <p>
     * Moreover the DB converter is reset, the root controller and online segment names are updated and the list of user panel is updated as
     * well.
     */
    private final class ReloadDbTask extends SwingWorker<Void, String> {
        ReloadDbTask() {
        }

        /**
         * The database reload actions are executed here. This method will return as soon as all the actions are terminated. The main frame
         * is put in a busy state.
         * 
         * @see javax.swing.SwingWorker#doInBackground()
         * @see IguiPanel#dbReloaded()
         * @see IguiFrame#setBusy(boolean)
         * @see IguiFrame#setBusyMessage(String)
         * @see Igui#updateUserPanels()
         * @throws InterruptedException
         * @throws ConfigException
         */
        @Override
        protected Void doInBackground() throws InterruptedException, ConfigException {
            // Why do we need to lock here?
            // This lock is exported by the RDBInterface object. The current schema implies the sending of
            // the reload command both to the RDB and the RDB_RW servers. But the IGUI is subscribed to the
            // RDB server only. We want to avoid processing the call-back before the RDB_RW has finished
            // reloading the database as well (see RDBInterface.reloadDatabaseFiles).
            final Lock lk = Igui.this.rdbInterface.getSynchronizer();
            lk.lock();

            try {
                IguiLogger.info("The IGUI is starting reloading the database");

                // Set the main frame busy
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Igui.this.mainFrame.setBusy(true);
                        Igui.this.mainFrame.enablePanelListButton(false);
                    }
                });

                // Invalidate the RW RDB cache, re-calculate the Online Segment and reset the converter
                {
                    {
                        final String msg = "Invalidating the cache of the RW RDB...";
                        IguiLogger.info(msg);
                        this.publish(msg);
                    }

                    Igui.this.dbRW.unread_all_objects(true);

                    {
                        final String msg = "Resetting database converters...";
                        IguiLogger.info(msg);
                        this.publish(msg);
                    }

                    try {
                        Igui.this.dalConverter.reset(Igui.this.dalPartition);
                    }
                    catch(final config.ConfigException ex) {
                        final String errStr = "Failed resetting the DAL converter: " + ex.getMessage();
                        IguiLogger.error(errStr, ex);
                        Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errStr);
                    }
                        
                    {
                        final String msg = "Refreshing the Online Segment...";
                        IguiLogger.info(msg);
                        this.publish(msg);
                    }

                    Igui.this.setOnlineSegment(Igui.this.dalPartition);
                }

                // Update Root Controller data from the database: this will reset any IS info
                // NOTE: call this after "setOnlineSegment()"
                Igui.this.setRootControllerData();

                // Now update RootController IS info
                final String rootControllerName = Igui.this.getRootControllerName();
                try {
                    final rc.RCStateInfo info = ISInfoReceiver.getInfo(IguiConstants.RC_IS_SERVER_NAME + "." + rootControllerName,
                                                                       rc.RCStateInfo.class);
                    Igui.this.getRCInfo().updateInfo(info, true);
                }
                catch(final Exception ex) {
                    IguiLogger.error("Failed updating RootController info from IS: " + ex, ex);
                }
                finally {
                    try {
                        final rc.DAQApplicationInfo info = ISInfoReceiver.getInfo(IguiConstants.SUPERVISION_IS_INFO_NAME + "."
                                                                                  + rootControllerName, rc.DAQApplicationInfo.class);
                        Igui.this.getRCInfo().updateInfo(info, true);
                    }
                    catch(final Exception ex) {
                        IguiLogger.error("Failed updating RootController info from IS: " + ex, ex);
                    }
                }

                {
                    final String msg = "Database reloading for all the Igui panels...";
                    IguiLogger.info(msg);
                    this.publish(msg);
                }

                // Map containing the task each panel will execute
                // Panel -> task Future
                final Map<IguiPanel, Future<?>> taskList = new HashMap<IguiPanel, Future<?>>();

                // Execute panel actions
                final Collection<IguiPanel> panelList = Igui.this.panelMap.values();
                for(final IguiPanel p : panelList) {
                    final String msg = "Reloading database for panel \"" + p.getPanelName() + "\"";
                    IguiLogger.info(msg);
                    this.publish(msg);

                    // Panel db reloading is executed in the panel own thread
                    final Future<?> panelTask = p.execDbReloaded();
                    taskList.put(p, panelTask);
                }

                // Wait for all the panels to terminate their actions
                final Set<Map.Entry<IguiPanel, Future<?>>> mapEntrySet = taskList.entrySet();
                for(final Map.Entry<IguiPanel, Future<?>> mapEntry : mapEntrySet) {
                    final IguiPanel p = mapEntry.getKey();
                    final String panelName = p.getPanelName();
                    final Future<?> panelTask = mapEntry.getValue();

                    // Get each panel task result
                    try {
                        this.publish("Waiting for panel \"" + panelName + "\" to finish reloading the database...");
                        panelTask.get(IguiConstants.PANEL_ACTION_TIMEOUT, TimeUnit.SECONDS);
                        final String msg = "Panel \"" + panelName + "\" has finished reloading the database";
                        IguiLogger.info(msg);
                        this.publish(msg);

                        Igui.this.mainFrame.setTabWarning(p, false);
                    }
                    catch(final InterruptedException ex) {
                        throw new InterruptedException("The database reloading task has been interrupted while waiting for the panel \""
                                                       + panelName + "\" to complete its work!");
                    }
                    catch(final TimeoutException ex) {
                        final String errMsg = "Timeout elapsed for panel \"" + panelName + "\" while reloading the database: " + ex;
                        Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                        IguiLogger.error(errMsg, ex);
                    }
                    catch(final ExecutionException ex) {
                        final String errMsg = "Error for panel \"" + panelName + "\" while reloading the database: " + ex.getCause();
                        Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                        IguiLogger.error(errMsg, ex);
                    }
                }

                // Update the list of user panels
                {
                    final String msg = "Updating the list of user panels...";
                    IguiLogger.info(msg);
                    this.publish(msg);
                }
                try {
                    Igui.this.updateUserPanels();
                }
                catch(final IguiException ex) {
                    final String errStr = "Failed updating the list of user panels after database reloading: " + ex.getMessage();
                    IguiLogger.error(errStr, ex);
                    Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errStr);
                }

                return null;
            }
            finally {
                lk.unlock();
            }
        }

        /**
         * It gets the result of the background task and reports any unexpected error. At the end the main frame is put in a non-busy state.
         * 
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                this.get();
                final String msg = "All panels have finished reloading the database";
                Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                IguiLogger.info(msg);
            }
            catch(final InterruptedException ex) {
                final String errStr = "Cannot get the result of the database reloading task: " + ex;
                IguiLogger.error(errStr, ex);
                Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errStr);
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final String errMsg = "An unexpected error occurred reloading the db: " + ex.getCause();
                IguiLogger.error(errMsg, ex);
                Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                ErrorFrame.showError("DB Reloading Error", errMsg, ex);
            }
            finally {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Igui.this.mainFrame.setBusy(false);
                        Igui.this.mainFrame.enablePanelListButton(true);
                        final IguiPanel rcPanel = Igui.this.panelMap.get(RunControlMainPanel.class);
                        if(rcPanel != null) {
                            Igui.this.mainFrame.switchToPanel(rcPanel);
                        }
                    }
                });
            }
        }

        /**
         * It updates the status message for the main frame {@link BusyUI}.
         * 
         * @see BusyUI#setBusyMessage(String)
         * @see IguiFrame#setBusyMessage(String)
         * @see javax.swing.SwingWorker#process(java.util.List)
         */
        @Override
        protected void process(final List<String> chunks) {
            // Show on the glass pane only the last message
            Igui.this.mainFrame.setBusyMessage(chunks.get(chunks.size() - 1));
        }
    }

    /**
     * {@link SwingWorker} used to ask all the IguiPanels to abort any database change.
     * <p>
     * In the background thread it will execute the database aborting for all the panels and wait for all the panel actions to terminate.
     * During this procedure the main frame is put in a busy state and messages are reported about which panel is doing what.
     * <p>
     * This SwingWorked is always executed in the {@link Igui#dbReloadedExecutor} executor, and in this way there is no risk to mix DB
     * aborting with DB reloading.
     * 
     * @see IguiPanel#dbDiscarded()
     * @see Igui#discardDb()
     */
    private final class AbortDbChangesTask extends SwingWorker<Void, String> {
        AbortDbChangesTask() {
        }

        /**
         * The database aborting actions are executed for all the panels. This method will return as soon as all the panel tasks are
         * terminated. The main frame is put in a busy state.
         * <p>
         * Before sending commands to all the IguiPanels the "abort()" method is called on the RW RDB server.
         * 
         * @see javax.swing.SwingWorker#doInBackground()
         * @see IguiPanel#dbDiscarded()
         * @throws InterruptedException
         * @throws IguiException.ConfigException Some error occurred discarding the DB changes ("abort()" failed on the RW RDB server)
         */
        @Override
        protected Void doInBackground() throws InterruptedException, IguiException.ConfigException {
            // Set the main frame busy
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Igui.this.mainFrame.setBusy(true);
                }
            });

            // Calling abort on the RW configuration
            try {
                final String msg = "Aborting database changes...";
                IguiLogger.info(msg);
                this.publish(msg);

                final boolean success = Igui.this.dbRW.abort();
                if(success == false) {
                    throw new IguiException.ConfigException("Failed calling \"abort\" to discard database changing!");
                }
                Igui.this.pendingDbChanges.set(false);
            }
            catch(final Exception ex) {
                throw new IguiException.ConfigException(ex.toString(), ex);
            }

            // Execute dbDiscarded on all the panels
            final Map<IguiPanel, Future<?>> taskMap = new HashMap<IguiPanel, Future<?>>();

            final Collection<IguiPanel> allPanels = Igui.this.panelMap.values();
            for(final IguiPanel p : allPanels) {
                final String pName = p.getPanelName();

                final String msg = "Discarding database changes for panel \"" + pName + "\"";
                IguiLogger.info(msg);
                this.publish(msg);

                final Future<?> task = p.execDbDiscarded();
                taskMap.put(p, task);
            }

            // Get the action result from all the panels
            final Set<Map.Entry<IguiPanel, Future<?>>> allTasks = taskMap.entrySet();
            for(final Map.Entry<IguiPanel, Future<?>> me : allTasks) {
                final IguiPanel p = me.getKey();
                final String pName = p.getPanelName();
                final Future<?> task = me.getValue();
                try {
                    {
                        final String msg = "Waiting for panel \"" + pName + "\" to finish discarding database changes...";
                        IguiLogger.info(msg);
                        this.publish(msg);
                    }
                    task.get(IguiConstants.PANEL_ACTION_TIMEOUT, TimeUnit.SECONDS);
                    {
                        final String msg = "Panel \"" + pName + "\" has finished discarding database changes";
                        IguiLogger.info(msg);
                        this.publish(msg);
                    }
                    Igui.this.mainFrame.setTabWarning(p, false);
                }
                catch(final InterruptedException ex) {
                    throw new InterruptedException("The database discarding task has been interrupted while waiting for the panel \""
                                                   + pName + "\" to complete its work!");
                }
                catch(final ExecutionException ex) {
                    final Throwable cause = ex.getCause();
                    final String errMsg = "Error for panel \"" + pName + "\" while discarding database changes: " + cause;
                    Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                    IguiLogger.error(errMsg, ex);
                }
                catch(final TimeoutException ex) {
                    final String errMsg = "Timeout elapsed for panel \"" + pName + "\" discarding database changes: " + ex;
                    Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                    IguiLogger.error(errMsg, ex);
                }
            }

            return null;
        }

        /**
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                this.get();
                final String msg = "All panels have finished discarding database changes";
                Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                IguiLogger.info(msg);
            }
            catch(final InterruptedException ex) {
                final String errStr = "Cannot get the result of the database discarding task: " + ex;
                IguiLogger.error(errStr, ex);
                Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errStr);
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final Throwable cause = ex.getCause();
                final String errMsg = "An error occurred while discarding database changes: " + cause;
                IguiLogger.error(errMsg, ex);
                Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                ErrorFrame.showError("Error While Discarding Database Changes", errMsg, ex);
            }
            finally {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Igui.this.mainFrame.setBusy(false);
                    }
                });
            }
        }

        /**
         * It logs all the messages and writes to the glass pane only the last one.
         * 
         * @see javax.swing.SwingWorker#process(java.util.List)
         */
        @Override
        protected void process(final List<String> chunks) {
            // Show on the glass pane only the last message
            Igui.this.mainFrame.setBusyMessage(chunks.get(chunks.size() - 1));
        }
    }

    /**
     * Class implementing database call-backs (coming when the database content changes).
     * <p>
     * Actions following a database reload are implemented using a SwingWorker ({@link ReloadDbTask}) executed in the
     * {@link Igui#dbReloadedExecutor} executor.
     */
    private final class ConfigCallback implements config.Callback {
        ConfigCallback() {
        }

        /**
         * It starts the {@link ReloadDbTask} SwingWorker in the {@link ReloadDbTask} executor.
         * 
         * @see config.Callback#process_changes(config.Change[], java.lang.Object)
         */
        @Override
        public void process_changes(final Change[] arg0, final Object arg1) {
            // Start a thread waiting for all the panels to execute the dbReloaded() method
            Igui.this.dbReloadedExecutor.execute(new ReloadDbTask());
        }
    }

    /**
     * ISInfoReceiver for processing RootController call-backs.
     * 
     * @see Igui#rcStateChanged(is.InfoEvent)
     */
    private final class RootCtrlReceiver extends ISInfoReceiver {
        /**
         * Constructor.
         */
        RootCtrlReceiver(final is.Criteria subCriteria) {
            super(IguiConstants.RC_IS_SERVER_NAME, subCriteria);
        }

        /**
         * @see is.InfoWatcher#infoCreated(InfoEvent)
         */
        @Override
        protected void infoCreated(final is.InfoEvent info) {
            Igui.this.rcStateChanged(info);
        }

        /**
         * @see is.InfoWatcher#infoUpdated(InfoEvent)
         */
        @Override
        protected void infoUpdated(final is.InfoEvent info) {
            Igui.this.rcStateChanged(info);
        }

        /**
         * @see is.InfoWatcher#infoSubscribed(InfoEvent)
         */
        @Override
        protected void infoSubscribed(final is.InfoEvent info) {
            IguiLogger.info("Received IS Root controller call-back after subscription");
            Igui.this.rcStateChanged(info);
        }
    }

    /**
     * Thread implementing the exit procedure.
     * <p>
     * This thread is executed every time the Igui is asked to exit. It executes the following tasks:
     * <ul>
     * <li>If there are still pending database changes then the user is asked about what to do (i.e., discard or commit changes);
     * <li>It un-subscribes from the RunCtrl IS server;
     * <li>It removes the IPC publication (if any);
     * <li>It terminates all the panels;
     * <li>It un-subscribes from database change notifications;
     * <li>It frees the acquired Resource Manager resources;
     * <li>It removes the ChangeListener from the IguiFrame tabbed pane;
     * <li>It asks (if requested) the Root Controller to stop the running partition.
     * </ul>
     * 
     * @see Igui#iguiExit(boolean)
     * @see Igui#dbChangesPending()
     * @see Igui#unregisterPanel(IguiPanel)
     * @see AbortDbChangesTask
     * @see RMInterface#freeCurrentResource()
     * @see IguiFrame#removeTabListener()
     */
    private final class ExitThread extends Thread {
        /**
         * Whether or not to ask the RootController to stop the running partition
         */
        private final boolean stopPartition;

        /**
         * Whether or not to bypass all the exit procedure
         */
        private final boolean forceExit;

        /**
         * Constructor.
         * 
         * @param stopPartition Boolean indicating if the partition should be stopped.
         * @param forceExit If <code>true</code> all the ordered shutdown procedure will be skipped.
         */
        ExitThread(final boolean stopPartition, final boolean forceExit) {
            super();
            this.stopPartition = stopPartition;
            this.forceExit = forceExit;
        }

        /**
         * It checks whether the partition has been closed: both the IPC server and the Root Controller are no more up.
         * 
         * @return <em>true</em> if the partition does not exist any more
         */
        private boolean partitionClosed() {
            final boolean ipcServerAlive = Igui.this.getPartition().isValid();

            // If the status of the Root Controller cannot be determined, then
            // consider it as not up. That is because the vast majority of times
            // the check on the IPC server is enough
            boolean rootControllerAlive = false;
            try {
                final Handle h = Igui.this.getPmgClient().lookup(Igui.this.getRootControllerName(),
                                                                 Igui.this.getPartition().getName(),
                                                                 Igui.this.getRCInfo().getHost());
                rootControllerAlive = (h != null) ? true : false;
            }
            catch(final LookupException | CorbaException ex) {
                IguiLogger.warning("Failed to determine if the Root Controller is alive: " + ex, ex);
            }

            return((ipcServerAlive == false) && (rootControllerAlive == false));
        }

        /**
         * @see java.lang.Thread#run()
         */
        @Override
        public void run() {
            if(this.forceExit == false) {
                // Consider the case in which there are pending DB changes.
                // If the partition has to be stopped don't bother about that

                boolean updatedDbs = false;
                if(Igui.this.dbRW != null) {
                    try {
                        updatedDbs = (Igui.this.dbRW.get_updated_dbs().length > 0);
                    }
                    catch(final Exception ex) {
                        IguiLogger.warning("Failed getting the updated files from the DB: " + ex, ex);
                    }
                }

                if((this.stopPartition == false) && ((Igui.this.dbChangesPending() == true) || (updatedDbs == true))) {
                    // Create a FutureTask to be sent to the EDT
                    final FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {
                        @Override
                        public Integer call() {
                            final Object[] options = new Object[] {"Discard", "Commit"};

                            final int result = JOptionPane.showOptionDialog(Igui.this.mainFrame,
                                                                            "There are un-committed changes to the database. Please, consider to commit or discard\nchanges before exiting, otherwise the database may remain locked.",
                                                                            "The IGUI Cannot Be Stopped",
                                                                            JOptionPane.DEFAULT_OPTION,
                                                                            JOptionPane.QUESTION_MESSAGE,
                                                                            null,
                                                                            options,
                                                                            options[0]);

                            return Integer.valueOf(result);
                        }
                    });

                    // Access the JOptionPane in the EDT!
                    javax.swing.SwingUtilities.invokeLater(task);

                    // Get the user selection
                    try {
                        final Integer res = task.get();
                        switch(res.intValue()) {
                            case 0: // User decided to abort DB changes
                                // Abort changes and wait for the procedure to finish
                                Igui.this.dbReloadedExecutor.submit(new AbortDbChangesTask()).get();
                                // Try again the exit procedure; now there should be no more
                                // pending database changes.
                                Igui.this.iguiExit(this.stopPartition, false);
                                break;
                            case 1: // User decided to commit DB changes
                                // Commit
                                Igui.this.commitAndReload();
                                // Try the exit procedure again; now there should be no more
                                // pending database changes.
                                Igui.this.iguiExit(this.stopPartition, false);
                                break;
                            default: // Do nothing
                                break;
                        }
                    }
                    catch(final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                        final String errMsg = "Exit thread interrupted: " + ex;
                        IguiLogger.warning(errMsg);
                        ErrorFrame.showError("Error Exiting the Igui", errMsg, ex);
                    }
                    catch(final ExecutionException ex) {
                        final String errMsg = "Got an unexpected exception: " + ex.getCause();
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("Error Exiting the Igui", errMsg, ex);
                    }
                    catch(final IguiException.ConfigException ex) {
                        final String errMsg = "Database committing and reloading failed: " + ex;
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("Database Error", errMsg, ex);
                    }
                } else {
                    // No pending DB changes
                    try {
                        // Set the frame busy
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                Igui.this.mainFrame.setBusy(true);
                            }
                        });

                        {
                            final String msg = "Igui is starting the exit procedure...";
                            IguiLogger.info(msg);
                            Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                            this.writeFrameMessage(msg);
                        }

                        // This will be executed when the igui is closed during the
                        // infrastructure test phase
                        if(Igui.this.initialInfrFrame != null) {
                            try {
                                Igui.this.initialInfrFrame.terminate();
                            }
                            catch(final Exception ex) {
                                IguiLogger.warning("Failed to terminate the initial infrastructure panel: " + ex, ex);
                            }
                        }

                        // Stop the periodic scheduler
                        Igui.this.periodicExecutor.shutdown();

                        // Un-subscribe from IS
                        try {
                            if(Igui.this.rootCtrlReceiver != null) {
                                final String msg = "Unsubscribing from RunCtrl IS server";
                                IguiLogger.info(msg);
                                Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                                this.writeFrameMessage(msg);

                                Igui.this.rootCtrlReceiver.unsubscribe();
                            }
                        }
                        catch(final Exception ex) {
                            IguiLogger.error("Failed to unsubscribe from RunCtrl IS server: " + ex.getMessage(), ex);
                        }
                        finally {
                            Igui.this.rcStateChangeExecutor.shutdown();
                        }

                        // Unsubscribe from db
                        try {
                            if((Igui.this.db != null) && (Igui.this.dbSubscription != null)) {
                                final String msg = "Usubscribing from database";
                                IguiLogger.info(msg);
                                Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                                this.writeFrameMessage(msg);

                                Igui.this.db.unsubscribe(Igui.this.dbSubscription);
                            }
                        }
                        catch(final Exception ex) {
                            IguiLogger.error("Database unsubscription failed: " + ex.getMessage(), ex);
                        }
                        finally {
                            Igui.this.dbReloadedExecutor.shutdown();
                        }

                        // This is important to avoid problems when all the panels are removed
                        Igui.this.mainFrame.removeTabListener();

                        // Map containing the panel termination tasks
                        // Panel -> task Future
                        final Map<IguiPanel, Future<?>> taskMap = new HashMap<IguiPanel, Future<?>>();
                        {
                            // Terminate all the panels
                            final Set<Map.Entry<Class<? extends IguiPanel>, IguiPanel>> mapEntrySet = Igui.this.panelMap.entrySet();
                            for(final Map.Entry<Class<? extends IguiPanel>, IguiPanel> mapEntry : mapEntrySet) {
                                final IguiPanel p = mapEntry.getValue();
                                taskMap.put(p, Igui.this.unregisterPanel(p));
                            }
                        }

                        // Wait for panels to be terminated
                        final Set<Map.Entry<IguiPanel, Future<?>>> taskMapEntrySet = taskMap.entrySet();
                        for(final Map.Entry<IguiPanel, Future<?>> task : taskMapEntrySet) {
                            try {
                                final String msg = "Waiting for panel \"" + task.getKey().getPanelName() + "\" to be terminated...";
                                IguiLogger.info(msg);
                                this.writeFrameMessage(msg);

                                task.getValue().get(IguiConstants.PANEL_ACTION_TIMEOUT, TimeUnit.SECONDS);
                            }
                            catch(final InterruptedException ex) {
                                throw ex;
                            }
                            catch(final Exception ex) {
                                final String msg = "An error occurred while terminating panel \"" + task.getKey().getPanelName() + "\": "
                                                   + ex;
                                IguiLogger.error(msg, ex);
                                this.writeFrameMessage(msg);
                            }
                        }

                        {
                            final String msg = "All panels have been terminated";
                            IguiLogger.info(msg);
                            this.writeFrameMessage(msg);
                        }

                        // Unload the RW db: this is needed to close the session
                        try {
                            if(Igui.this.dbRW != null) {
                                final String msg = "Closing the RDB RW session";
                                IguiLogger.info(msg);
                                Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                                this.writeFrameMessage(msg);

                                Igui.this.dbRW.unload();
                            }
                        }
                        catch(final Exception ex) {
                            IguiLogger.error("Failed to close the RDB RW session: " + ex, ex);
                        }

                        // Some clean-up in the main frame
                        Igui.this.getMainFrame().onExit();

                        // Check if the partition has to be terminated
                        if(this.stopPartition == true) {
                            {
                                final String msg = "Asking the Root Controller to terminate the partition...";
                                IguiLogger.info(msg);
                                this.writeFrameMessage(msg);
                            }

                            try {
                                Igui.this.rcCommander.exit(Igui.this.getRootControllerName());

                                // The command was successfully sent to the Root Controller
                                {
                                    final String msg = "Waiting for the partition to exit...";
                                    IguiLogger.info(msg);
                                    this.writeFrameMessage(msg);
                                }

                                // Loop waiting for the partition to exit
                                final long timeToStop = new Date().getTime() + (IguiConstants.EXIT_TIMEOUT * 1000);
                                while((this.partitionClosed() == false) && (new Date().getTime() < timeToStop)) {
                                    Thread.sleep(333);
                                }

                                // At this point the partition should not exists anymore;
                                // if it is still alive alert the user
                                if(this.partitionClosed() == false) {
                                    final String msg = "Partition still alive after a timeout of " + IguiConstants.EXIT_TIMEOUT
                                                       + " seconds, manual clean-up may be needed. "
                                                       + "Consider using the PMG tools (e.g., the PMG control panel) to check any left-over process or infrastructure problems.";
                                    IguiLogger.info(msg);
                                    this.writeFrameMessage(msg);
                                    try {
                                        javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                                            @Override
                                            public void run() {
                                                ErrorFrame.showError(msg);
                                            }
                                        });
                                    }
                                    catch(final InvocationTargetException ex) {
                                        IguiLogger.warning(ex.toString(), ex);
                                    }
                                }
                            }
                            catch(final daq.rc.RCException ex) {
                                final String msg =
                                                 "Failed sending the EXIT command to the Root Controller, manual cleanup is needed to shutdown the partition. "
                                                 + "Consider using the PMG tools (e.g., the PMG control panel) to check any left-over process or infrastructure problems.";
                                IguiLogger.error(msg, ex);
                                ErrorFrame.showError("IGUI - Error at Exit", msg, ex);
                            }
                        }
                    }
                    catch(final InterruptedException ex) {
                        final String errMsg =
                                            "The IGUI exit task has been interrupted,\nthe exit procedure has not been correctly terminated! "
                                            + "Consider using the PMG tools (e.g., the PMG control panel) to check any left-over process or infrastructure problems.";
                        IguiLogger.error(errMsg, ex);
                        try {
                            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                                @Override
                                public void run() {
                                    ErrorFrame.showError(errMsg);
                                }
                            });
                        }
                        catch(final Exception e) {
                            IguiLogger.warning(e.toString(), e);
                        }
                    }
                    catch(final Exception ex) {
                        final String errMsg =
                                            "An unexpected error occured while the Igui was exiting,\nthe exit procedure has not been correctly terminated! "
                                            + "Consider using the PMG tools (e.g., the PMG control panel) to check any left-over process or infrastructure problems.";
                        IguiLogger.error(errMsg, ex);
                        try {
                            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                                @Override
                                public void run() {
                                    ErrorFrame.showError(errMsg);
                                }
                            });
                        }
                        catch(final Exception e) {
                            IguiLogger.warning(e.toString(), e);
                        }
                    }
                }
            }

            // Free RM resources
            try {
                Igui.this.rmInterface.freeCurrentResource();

                final String msg = "RM resources (if any) have been freed";
                IguiLogger.info(msg);
                this.writeFrameMessage(msg);
            }
            catch(final Exception ex) {
                IguiLogger.error("Failed freeing RM resources: " + ex.getMessage(), ex);
            }

            // Remove IPC publication
            if(Igui.this.servant != null) {
                try {
                    Igui.this.servant.withdraw();
                }
                catch(final ipc.InvalidPartitionException ex) {
                    IguiLogger.error("The IPC reference has not been removed because the IPC partition cannot be reached: " + ex, ex);
                }
                catch(final Exception ex) {
                    IguiLogger.error("Unexpected exception removing the IPC reference: " + ex, ex);
                }
            }

            IguiLogger.info("Igui is exiting...");

            // Remove the shutdown hook
            try {
                Runtime.getRuntime().removeShutdownHook(Igui.this.shutdownHook);
                IguiLogger.debug("The shutdown hook has been removed");
            }
            catch(final Exception ex) {
                IguiLogger.warning("The shutdown hook could not be removed: " + ex, ex);
            }

            System.exit(0);
        }

        /**
         * It writes a message on the Igui frame layer
         * 
         * @param message The message
         */
        private void writeFrameMessage(final String message) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Igui.this.mainFrame.setBusyMessage(message);
                }
            });
        }
    }

    /**
     * Class holder for the Igui instance.
     * <p>
     * Thread safe lock-less idiom to implement the singleton pattern: the java VM garantees that the static reference will be created only
     * once and in a thread safe context.
     * 
     * @see Igui#instance()
     */
    private static class IguiInstanceHolder {
        /**
         * The only <code>Igui</code> reference
         */
        private final static Igui iguiInstance = IguiInstanceHolder.createInstance();

        /**
         * Private constructor.
         */
        private IguiInstanceHolder() {
        }

        /**
         * Public method to get the <code>Igui</code> instance.
         * 
         * @return The <code>Igui</code> instance.
         */
        public static Igui getInstance() {
            return IguiInstanceHolder.iguiInstance;
        }

        /**
         * It creates the Igui instance.
         * <p>
         * The Igui constructor is called in the EDT.
         * 
         * @return The Igui instance
         * @throws RuntimeException Thrown if the Igui instance cannot be created. This is an unrecoverable situation.
         */
        private final static Igui createInstance() throws RuntimeException {
            try {
                final FutureTask<Igui> iguiBuilder = new FutureTask<Igui>(new Callable<Igui>() {
                    @Override
                    public Igui call() throws Exception {
                        return new Igui();
                    }
                });

                javax.swing.SwingUtilities.invokeLater(iguiBuilder);

                return iguiBuilder.get();
            }
            catch(final Exception e) {
                IguiLogger.error("The IGUI cannot be started because of an unrecoverable error: " + e.getMessage(), e);
                if(InterruptedException.class.isInstance(e)) {
                    Thread.currentThread().interrupt();
                }

                System.exit(-1);

                throw new RuntimeException(e);
            }
        }
    }

    /**
     * The constructor.
     * <p>
     * It is executed in the EDT and it does:
     * <ul>
     * <li>Create the splash screen (if specified in the command line options);
     * <li>Create the main frame.
     * </ul>
     */
    private Igui() {
        // Get the splash screen
        try {
            this.splash = SplashScreen.getSplashScreen();
            this.splashGraphic = this.splash.createGraphics();
            IguiLogger.debug("Splash screen created");
        }
        catch(final Exception ex) {
            IguiLogger.debug("Error creating the splash screen: " + ex.getMessage(), ex);
        }

        // Create the main JFrame
        final String msg = "Creating the main frame...";
        IguiLogger.debug(msg);
        this.writeSplash(msg);
        this.mainFrame = new IguiFrame();
    }

    /**
     * It gives the current state of the Root Controller.
     * 
     * @return The Root Controller state
     * @see RunControlFSM#getStateFromString(String)
     */
    public RunControlFSM.State getRCState() {
        return this.rootControllerData.getISStatus().getState();
    }

    /**
     * It returns <code>true</code> if it is allowed to modify the DB in the current RC state
     * <p>
     * Currently it is allowed to modify the DB only in the {@link RunControlFSM.State#NONE} state
     * 
     * @return <code>true</code> if it is allowed to modify the DB in the current RC state
     */
    public boolean isDBChangeAllowed() {
        final ApplicationISStatus rcISStatus = this.rootControllerData.getISStatus();
        return (rcISStatus.getState().follows(RunControlFSM.State.NONE) == false) && (rcISStatus.isTransitioning() == false);
    }

    /**
     * It gives the current access level state (i.e., control or status display)
     * 
     * @return The current access level
     */
    public IguiConstants.AccessControlStatus getAccessLevel() {
        return this.rmAccessLevel;
    }

    /**
     * It gives a reference to the current IPC partition in which the Igui is started.
     * 
     * @return The IPC partition
     */
    public ipc.Partition getPartition() {
        return this.partition;
    }

    /**
     * It gives the name of the Root Controller for the current partition as defined in the configuration database.
     * 
     * @return The Root Controller name
     */
    public String getRootControllerName() {
        return this.rootControllerData.getName();
    }

    /**
     * It gives the name of the segment of the Root Controller (i.e., the online segment name) for the current partition as defined in the
     * configuration database.
     * 
     * @return The Root Controller name
     */
    public String getRootControllerSegmentName() {
        return this.rootControllerData.getSegmentName();
    }

    /**
     * It writes a message to the ERS log table.
     * 
     * @param severity The message severity
     * @param message The message as a string
     */
    public void internalMessage(final IguiConstants.MessageSeverity severity, final String message) {
        final ErsPanel ersPanel = this.getPanel(ErsPanel.class);
        if(ersPanel != null) {
            ersPanel.addInternalMessage(severity, message);
        }
    }

    /**
     * It returns <code>true</code> if the config layers uses the GIT back-end.
     * 
     * @return <code>true</code> if the config layers uses the GIT back-end
     */
    public boolean configUsesGit() {
        return this.configUsesGitBackend.get();
    }
    
    /**
     * Static method returning the reference to this Igui instance.
     * 
     * @return The reference to this Igui instance
     * @see IguiInstanceHolder
     */
    final static Igui instance() {
        return IguiInstanceHolder.getInstance();
    }

    /**
     * It connects to the partition infrastructure (i.e., IS and database subscriptions), sets the Igui system properties (extracted from
     * the database), starts the frame checking the Root Controller infrastructure and at the end creates all the Igui panels. Once all the
     * panels have been created, the IS call-back processing is enabled so that there is no chance that a panel misses an update between the
     * panel initialization and the moment in which the panel is registered as a subscriber to changes in the RC state.
     * 
     * @param createPanels If <code>true</code> all the panels are created, otherwise only connection to the base partition infrastructure
     *            is done
     * @throws IguiException.ISException Failed connecting to the RunCntrl IS server
     * @throws IguiException.ConfigException Failed connecting to the DB
     * @throws IguiException.RMException Failed connecting to the RM server
     * @throws IguiException.InvalidPartition The specified partition is not valid
     * @throws IguiException.UndefinedPartition No partition has been specified
     * @throws IguiException.EDTException Some problems occurred in the EDT (should not!)
     * @throws IguiException.PanelInitializationFailed One of the "embedded" panel could not be initialized
     * @throws IguiException.PanelInstanceNotCreated One of the "embedded" panel could not be created
     * @throws InterruptedException The thread has been interrupted
     * @see Igui#connectBaseInfrastructure()
     * @see Igui#createEmbeddedPanels()
     * @see Igui#createTabbedPanels(Set)
     * @see Igui#setProperties()
     * @see Igui#createInitialTabPanelLists()
     * @see Igui#checkRootControllerInfrastructure()
     */
    void init(final boolean createPanels)
        throws IguiException.UndefinedPartition,
            IguiException.InvalidPartition,
            IguiException.RMException,
            IguiException.ConfigException,
            IguiException.ISException,
            IguiException.PanelInstanceNotCreated,
            IguiException.PanelInitializationFailed,
            IguiException.EDTException,
            InterruptedException
    {
        // Install the shutdown hook
        try {
            Runtime.getRuntime().addShutdownHook(this.shutdownHook);
            IguiLogger.debug("Shutdown hook has been installed");
        }
        catch(final Exception ex) {
            IguiLogger.warning("The shutdown hook could not be installed: " + ex, ex);
        }

        // Connect to the infrastructure
        {
            final String msg = "Connecting to partition infrastructure...";
            this.writeSplash(msg);
            IguiLogger.info(msg);
            this.connectBaseInfrastructure();
        }

        // This method locks until the Root Controller infrastructure is tested
        this.checkRootControllerInfrastructure();

        // Set Igui system properties extracted from DB
        {
            final String msg = "Getting Igui properties from database...";
            this.writeSplash(msg);
            IguiLogger.info(msg);
            this.setProperties();
        }

        // Create all the panels
        {
            if(createPanels == true) {
                // Build the list of panels to be created
                this.createInitialTabPanelLists();

                final String msg = "Starting to create panels...";
                this.writeSplash(msg);
                IguiLogger.info(msg);

                // Create first the "embedded" (non tabbed) panels (i.e., main, ers and elog panels)
                this.createEmbeddedPanels();

                // Create all the tabbed panels
                {
                    this.panelListLockR.lock();
                    try {
                        // Here create only private panels
                        final String errMsg = this.createTabbedPanels(this.privateTabPanelList);
                        if(errMsg.isEmpty() == false) {
                            Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                        }

                        // Add other panels to the list
                        final List<String> pList = new ArrayList<String>(this.userTabPanelList);
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                Igui.this.mainFrame.setPanelList(pList);
                            }
                        });
                        
                        this.forceLoadPanels();
                    }
                    finally {
                        this.panelListLockR.unlock();
                    }
                }
            }
        }

        // At this point enable the Root Controller IS call-backs processing
        this.isCallBackLatch.countDown();

        // Start the partition checker and the db changes checker
        this.periodicExecutor.scheduleWithFixedDelay(new PartitionChecker(), 10, 10, TimeUnit.SECONDS);
        this.periodicExecutor.scheduleWithFixedDelay(this.dbChagesCheckerTask, 5, 10, TimeUnit.SECONDS);
    }

    /**
     * It asks the Resource Manager server for a resource.
     * <p>
     * When the CONTROL resource is taken, the Igui is also published in IPC. The IPC reference is then removed when the DISPLAY resource is
     * acquired.
     * <p>
     * Additionally, the count-down to force the IGUI shutdown is started when the DISPLAY resource is taken and stopped when the CONTROL
     * resource is acquired.
     * 
     * @param accessLevel The resource corresponding to the defined access level
     * @param notifyPanels If <code>true</code> the main frame and all the panels are notified of the access level change
     * @throws IguiException.RMException Thrown if for any reason the Resource Manager server cannot grant the requested resource
     * @see RMInterface#acquireResource(Igui.IguiConstants.AccessControlStatus)
     * @see IguiFrame#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     * @see IguiPanel#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     * @see IguiFrame#stopForcedCloseCountdown()
     * @see IguiFrame#startForcedCloseCountdown()
     */
    void askRMResource(final IguiConstants.AccessControlStatus accessLevel, final boolean notifyPanels) throws IguiException.RMException {
        // Try to acquire the resource
        this.rmInterface.acquireResource(accessLevel);
        this.rmAccessLevel = accessLevel; // It is volatile

        // If not in status display then publish in IPC
        if(accessLevel.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            try {
                this.servant.publish();
                IguiLogger.info("IPC publication done");
            }
            catch(final Exception ex) {
                IguiLogger.error("Failed to publish in IPC: " + ex, ex);
            }

            // We are in control mode, stop the IGUI shutdown count-down
            this.mainFrame.stopForcedCloseCountdown();
        } else {
            try {
                this.servant.withdraw();
                IguiLogger.info("IPC publication removed (if previously done)");
            }
            catch(final ipc.InvalidPartitionException ex) {
                IguiLogger.warning("IPC publication not removed because the partition cannot be reached: " + ex, ex);
            }
            catch(final Exception ex) {
                IguiLogger.warning("Unexpected exception removing the IPC publication: " + ex, ex);
            }

            // We are in display mode, start the IGUI shutdown count-down
            this.mainFrame.startForcedCloseCountdown();
        }

        // Notify panels
        if(notifyPanels == true) {
            this.mainFrame.accessControlChanged(accessLevel);

            final Collection<IguiPanel> panels = this.panelMap.values();
            for(final IguiPanel p : panels) {
                p.execAccessControlChanged(accessLevel);
                IguiLogger.info("Panel \"" + p.getPanelName() + "\" has been notified about the access level change");
            }
        }
    }

    /**
     * It gives a reference to the read-only {@link config.Configuration} object.
     * 
     * @return A reference to the read-only {@link config.Configuration} object
     */
    final config.Configuration getDb() {
        return this.db;
    }

    /**
     * It gives a reference to the read/write {@link config.Configuration} object.
     * 
     * @return A reference to the read/write {@link config.Configuration} object.
     */
    final config.Configuration getRwDb() {
        return this.dbRW;
    }

    /**
     * It returns an instance of the entry-point class to the PMG client library.
     * 
     * @return An instance of the entry-point class to the PMG client library.
     */
    final PmgClient getPmgClient() {
        return PmgClientBridge.instance();
    }
    
    /**
     * It sets RDB credentials for the user.
     * <p>
     * This method requires a remote invocation to the RDB RW server.
     * 
     * @param userName The name of the user to authenticate
     * @param pswd The user password
     * @throws IguiException.ConfigException The authentication failed (i.e., wrong credentials)
     */
    void rdbAuthenticate(final String userName, final String pswd) throws IguiException.ConfigException {
        try {
            this.dbRW.set_commit_credentials(userName, pswd);
        }
        catch(final Exception ex) {
            throw new IguiException.ConfigException("Authentication to the RDb failed: " + ex, ex);
        }
    }

    /**
     * It gives a reference to the DAL partition object.
     * 
     * @return A reference to the DAL partition object.
     */
    final dal.Partition getDalPartition() {
        return this.dalPartition;
    }

    /**
     * It gives a reference to the {@link dal.Segment} object holding the Online Segment
     * 
     * @return A reference to the {@link dal.Segment} object holding the Online Segment
     */
    final dal.Segment getOnlineSegment() {
        return this.onlineSegment;
    }

    /**
     * Method called to let the Igui exit and terminate the Java Virtual Machine.
     * 
     * @see ExitThread
     * @param stopPartition <code>true</code> if the partition has to be terminated
     * @param forceExit If <code>true</code> all the ordered shutdown procedure will be bypassed
     */
    void iguiExit(final boolean stopPartition, final boolean forceExit) {
        final Thread et = new ExitThread(stopPartition, forceExit);
        et.setPriority(Thread.NORM_PRIORITY);
        et.start();
    }

    /**
     * It gives the reference to the Igui main frame.
     * 
     * @return The main frame
     */
    IguiFrame getMainFrame() {
        return this.mainFrame;
    }

    /**
     * It sends a transition command to the Root Controller.
     * <p>
     * Since this method implies a remote call it is suggested to execute it in a thread different than the EDT. This method should be
     * preferred to the {@link #sendControllerTransitionCommand(String, Igui.RunControlFSM.Transition, String)} one because it may implement
     * particular actions to be done before or after sending a command to the Root Controller.
     * <p>
     * Currently the command is sent only after all the panels have executed the
     * {@link IguiPanel#rcTransitionCommandSending(Igui.RunControlFSM.Transition)} method.
     * 
     * @param cmd The transition command
     * @throws InterruptedException The execution has been interrupted
     * @throws RCException Some error occurred sending the command to the RootController
     * @see IguiPanel#rcTransitionCommandSending(Igui.RunControlFSM.Transition)
     */
    void sendRootControllerTransitionCommand(final RunControlFSM.Transition cmd) throws InterruptedException, RCException {
        // Inform all the panels that a transition command is going to be sent to
        // the Root Controller. Wait for all of them before sending the command.
        final Map<IguiPanel, Future<?>> taskList = new HashMap<IguiPanel, Future<?>>();

        // Set the busy cursor
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Igui.this.mainFrame.setWaiting(true);
            }
        });

        // Exec actions on all the panels and set tab colors RED
        // (it may help understanding the slow panel)
        for(final IguiPanel p : this.panelMap.values()) {
            if(p.rcStateAware() == true) {
                this.mainFrame.setTabColor(p, Color.RED);
                taskList.put(p, p.execRcTransitionCommandSending(cmd));
            }
        }

        final Set<Map.Entry<IguiPanel, Future<?>>> me = taskList.entrySet();
        for(final Map.Entry<IguiPanel, Future<?>> e : me) {
            final String panelName = e.getKey().getPanelName();
            try {
                IguiLogger.debug("Waiting for panel \"" + panelName + "\" to complete its task...");
                e.getValue().get(IguiConstants.PANEL_ACTION_TIMEOUT, TimeUnit.SECONDS);
            }
            catch(final InterruptedException ex) {
                IguiLogger.warning("Thread interrupted!!!");
                Thread.currentThread().interrupt();
                throw ex;
            }
            catch(final ExecutionException ex) {
                final String errMsg = "Failed executing command on panel \"" + panelName + "\": " + ex.getCause();
                IguiLogger.error(errMsg, ex);
            }
            catch(final TimeoutException ex) {
                final String errMsg = "Time out for panel \"" + panelName + "\": " + ex;
                this.internalMessage(IguiConstants.MessageSeverity.WARNING, errMsg);
                IguiLogger.error(errMsg, ex);
            }
            finally {
                this.mainFrame.setTabColor(e.getKey(), null);
            }
        }

        // Set back normal cursor
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Igui.this.mainFrame.setWaiting(false);
            }
        });

        IguiLogger.debug("All panels are ready, the transition command \"" + cmd.toString() + "\" is now going to be sent...");

        // Now the command can be sent to the Root Controller
        this.rcCommander.makeTransition(this.getRootControllerName(), cmd.getRCFSMCommand());
    }

    /**
     * It sends a transition command to a controller.
     * <p>
     * Since this method implies a remote call it is suggested to execute it in a thread different than the EDT. To send a transition
     * command to the Root Controller, please use {@link #sendRootControllerTransitionCommand(Igui.RunControlFSM.Transition, String)}.
     * 
     * @param name The controller name
     * @param cmd The transition command
     * @throws daq.rc.RCException Some error occurred sending the command to the controller or the transition cannot be performed
     * @see #sendRootControllerTransitionCommand(Igui.RunControlFSM.Transition, String)
     */
    void sendControllerTransitionCommand(final String name, final RunControlFSM.Transition cmd) throws RCException {
        this.rcCommander.makeTransition(name, cmd.getRCFSMCommand());
    }

    /**
     * It is used to send commands (usually recovery commands and not transition ones) to a controller.
     * <p>
     * Since this method implies a remote call it is suggested to execute it in a thread different than the EDT.
     * 
     * @param name The controller name
     * @param cmd The command
     * @param args The command arguments
     * @throws daq.rc.RCException Some error occurred sending the command to the controller or the transition cannot be performed
     */
    void sendControllerCommand(final String name, final daq.rc.Command cmd) throws RCException {
        this.rcCommander.executeCommand(name, cmd);
    }

    /**
     * It asks all the panels to eventually refresh any IS subscription.
     * <p>
     * This method is executed by the {@link IguiFrame} when the "Refresh IS Subscriptions" item is selected from the "Command" menu.
     * 
     * @see RefreshISTask
     */
    void refreshISSubscriptions() {
        this.isRefresherExecutor.execute(new RefreshISTask());
    }

    /**
     * This method is used to commit database changes and ask the DB system to reload the database.
     * <p>
     * <ul>
     * <li>First changes are committed to the RW database;
     * <li>A list of the modified database files is retrieved and the user is asked about the ones to reload;
     * <li>The DB system is asked to reload the selected (if any) database files.
     * </ul>
     * 
     * @throws IguiException.ConfigException Some error occurred interacting with the DB system
     * @see RDBInterface#reloadDatabaseFiles(List)
     * @see ReloadDialog
     * @see ReloadDialogRepo
     */
    void commitAndReload() throws IguiException.ConfigException {
        this.dbChagesCheckerTask.pause();
        this.mainFrame.notifyExternalDbChanges(false);
        
        try {
            IguiLogger.debug("Getting the list of uncommitted files");

            // Keep trace of the commit action
            boolean commitSuccessful = true;

            // This string builder holds the user comment (used in two different threads, do not use a StringBuilder)
            final StringBuffer commitComment = new StringBuffer();

            // Check whether some file has been changed
            final String[] updFiles = this.dbRW.get_updated_dbs();
            if((updFiles != null) && (updFiles.length > 0)) {
                IguiLogger.info("Here is the list of uncommitted files: " + Arrays.toString(updFiles));

                // Show the user all the uncommitted files and ask him/her to actually commit or not
                final FutureTask<Integer> userCommentTask = new FutureTask<Integer>(new Callable<Integer>() {
                    @Override
                    public Integer call() {
                        // Panel container
                        final JPanel p = new JPanel(new BorderLayout());

                        // Information label
                        final MultilineLabel lb =
                                                new MultilineLabel("Here is a list of modified database files that will be committed, do you want to commit all of them?\n"
                                                                   + "A check on files to be reloaded will be done anyway.");

                        // List of updated files
                        final JList<String> l = new JList<String>(updFiles);
                        l.setVisibleRowCount(5);
                        final JScrollPane lsp = new JScrollPane(l);
                        lsp.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
                        lsp.setPreferredSize(new Dimension(450, 100));

                        // Text area for comments
                        final JTextArea jta = new JTextArea();
                        jta.setRows(3);
                        jta.setLineWrap(true);
                        jta.setWrapStyleWord(true);
                        jta.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
                        final JScrollPane jtaSp = new JScrollPane(jta);
                        jtaSp.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
                        final DefaultOverlayable overl = new DefaultOverlayable(jtaSp);
                        overl.addOverlayComponent(StyledLabelBuilder.createStyledLabel("{Enter comment here:f:gray}"));
                        jta.addFocusListener(new FocusListener() {
                            @Override
                            public void focusGained(final FocusEvent e) {
                                overl.setOverlayVisible(false);
                            }

                            @Override
                            public void focusLost(final FocusEvent e) {
                                overl.setOverlayVisible(jta.getDocument().getLength() == 0);
                            }
                        });
                        jta.getDocument().addDocumentListener(new DocumentListener() {
                            @Override
                            public void changedUpdate(final DocumentEvent e) {
                            }

                            @Override
                            public void insertUpdate(final DocumentEvent e) {
                                overl.setOverlayVisible(jta.getDocument().getLength() == 0);
                            }

                            @Override
                            public void removeUpdate(final DocumentEvent e) {
                                overl.setOverlayVisible(jta.getDocument().getLength() == 0);
                            }
                        });

                        // Add all to the panel
                        p.add(lb, BorderLayout.NORTH);
                        p.add(lsp, BorderLayout.CENTER);
                        p.add(overl, BorderLayout.SOUTH);

                        // Show the dialog
                        boolean done = false;
                        int chosenOpt = JOptionPane.NO_OPTION;
                        String cmt = "";
                        do {
                            chosenOpt = JOptionPane.showConfirmDialog(Igui.this.mainFrame,
                                                                       p,
                                                                       "IGUI - Database Commit",
                                                                       JOptionPane.YES_NO_OPTION);
                            
                            cmt = jta.getText().trim();
                            if((chosenOpt == JOptionPane.YES_OPTION) && 
                               (cmt.isEmpty() == true) &&
                               (Igui.instance().configUsesGit() == true)) 
                            {
                                JOptionPane.showMessageDialog(Igui.this.mainFrame,
                                                              "The commit message cannot be empty!",
                                                              "IGUI - Database Commit",
                                                              JOptionPane.WARNING_MESSAGE);
                                
                                done = false;
                            } else {
                                done = true;
                            }
                        }
                        while(done == false);
                        
                        // Register the comment
                        commitComment.append(cmt);

                        // Return the user choice
                        return Integer.valueOf(chosenOpt);
                    }
                });

                // Send the task to the EDT and get the result
                javax.swing.SwingUtilities.invokeLater(userCommentTask);
                final Integer decision = userCommentTask.get();
                if(decision.intValue() == JOptionPane.YES_OPTION) {
                    // Commit changes
                    commitSuccessful = this.dbRW.commit(commitComment.toString());
                }
            } else {
                IguiLogger.info("There are no files to be committed");
            }

            if(commitSuccessful == true) {
                this.pendingDbChanges.set(false);

                IguiLogger.debug("Getting the list of changes to be reloaded");

                final Version[] changesToReload = this.db.get_changes();
                boolean nothingToReload = true;
                if((changesToReload.length == 1) && (changesToReload[0].get_id().isEmpty() == true)) {
                    // No GIT, just files
                    
                    // Create the reload dialog in the EDT and wait for the user to select files to be reloaded
                    final FutureTask<List<String>> relDial = new FutureTask<List<String>>(new Callable<List<String>>() {
                        @Override
                        public List<String> call() throws Exception {
                            final ReloadDialog dialog = new ReloadDialog(Arrays.asList(changesToReload[0].get_files()));
                            dialog.setVisible(true); // The dialog is modal and this method blocks
                            return dialog.getFilesToReload();
                        }
                    });

                    javax.swing.SwingUtilities.invokeLater(relDial);
                    final List<String> filesToReload = relDial.get();

                    if(filesToReload.size() > 0) {
                        nothingToReload = false;
                        
                        final String msg = "Reloading database files " + filesToReload.toString();
                        IguiLogger.info(msg);
                        this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);

                        this.rdbInterface.reloadDatabaseFiles(filesToReload);
                    }
                } else {
                    // GIT is used
                    final FutureTask<String> relDial = new FutureTask<String>( () -> 
                    { 
                        final ReloadDialogRepo d = new ReloadDialogRepo(changesToReload);
                        d.setVisible(true);
                        return d.getSelectedVersion();
                    });
                    
                    javax.swing.SwingUtilities.invokeLater(relDial);
                    final String versionToReload = relDial.get();
                    
                    if(versionToReload.isEmpty() == false) {
                        nothingToReload = false;
                        
                        final String msg = "Setting configuration version to " + versionToReload;
                        IguiLogger.info(msg);
                        this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);

                        this.rdbInterface.setConfigurationAndReload("hash:" + versionToReload);                        
                    }
                }
                
                if(nothingToReload == true) {
                    final String msg = "No database files to reload";
                    IguiLogger.info(msg);
                    this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                }
            } else {
                throw new IguiException.ConfigException("Commit to DB failed!");
            }
        }
        catch(final config.SystemException | config.NotAllowedException ex) {
            final CommitError errorParser = new CommitError(ex.getMessage());

            
            final StringBuilder message = new StringBuilder();
            final type_t errorType = errorParser.get_error_type();
            switch(errorType) {
                case merge_conflict:
                    message.append("\nthe database has been externally modified and your changes cannot be properly merged.\n");
                    message.append(errorParser.get_error_text());
                    message.append("\nPlease \"Commit & Reload\" to accept external modifications, then re-apply your changes and commit again.");
                    break;
                case no_access_manager_permission:
                    message.append("\nsome files could not be updated because of some user permission issues:\n");
                    message.append(errorParser.get_error_text());
                    message.append("\nProbably the user running the partition misses some privileges.");
                    message.append("\nPossible solutions are to restart the partition under a different user or update the Access Manager roles for the current user.");
                    break;
                case consistency_error:
                    message.append("\nthe database is not in a consistent state:");
                    message.append(errorParser.get_error_text());
                    message.append("\nPlese, fix the errors in the database before retrying to \"Commit & Reload\"");
                    break;
                case lock_conflict:
                    message.append("\nan internal GIT error occurred:\n");
                    message.append(errorParser.get_error_text());
                    message.append("\nPlease try to \"Commit & Reload\" again.");
                    break;
                case access_manager_failure:
                    message.append("\nan unexpected Access Manager error occurred.\nPlease try to \"Commit & Reload\" again.\nError details:\n");
                    message.append(errorParser.get_commit_log());
                    break;
                case git_failure:
                case unknown:
                default:
                    message.append("\nan unexpected error occurred:\nPlease try to \"Commit & Reload\" again.\nError details:\n");
                    message.append(errorParser.get_commit_log());
                    break;
            }
            
            IguiLogger.error(errorParser.get_commit_log());
            
            throw new IguiException.ConfigException("DB commit failed: " + message.toString(), ex);
        }
        catch(final IguiException.RDBException ex) {
            throw new IguiException.ConfigException(ex.getMessage(), ex);
        }
        catch(final InterruptedException ex) {
            Thread.currentThread().interrupt();
            throw new IguiException.ConfigException("The database reloading has been interrupted: " + ex, ex);
        }
        catch(final Exception ex) {
            throw new IguiException.ConfigException("Unexpected exception while reloading the database: " + ex.getMessage(), ex);
        }
        finally {
            this.dbChagesCheckerTask.resume();
        }
    }

    /**
     * It asks all the panels to take some action becausue the DB changes have been discarded.
     * 
     * @see IguiFrame#showDBPostponeDiscardDialog(String)
     */
    void discardDb() {
        this.dbReloadedExecutor.execute(new AbortDbChangesTask());
    }

    /**
     * It returns <code>true</code> if there are pending DB changes.
     * 
     * @return <code>true</code> if there are pending DB changes
     */
    boolean dbChangesPending() {
        return this.pendingDbChanges.get();
    }

    /**
     * It sets the flag keeping trace of pending DB changes.
     * 
     * @param pendingChanges <code>true</code> if there are pending DB changes
     * @see IguiFrame#showDBPostponeDiscardDialog(String)
     */
    void setDbChangesPending(final boolean pendingChanges) {
        this.pendingDbChanges.compareAndSet(!pendingChanges, pendingChanges);
    }

    /**
     * It returns an array containing the names of uncommitted files
     * <p>
     * Note: this may imply a remote call to the DB server
     * 
     * @return An array containing the names of uncommitted files
     */
    String[] uncommittedFiles() {
        try {
            return this.dbRW.get_updated_dbs();
        }
        catch(final Exception ex) {
            // Assuming no changes
            return new String[] {};
        }
    }

    /**
     * Static method to notify a panel about changes in the RC state
     * 
     * @param p The reference to the panel to be notified
     * @param srcState The previous RC state
     * @param dstState The current RC state
     * @see RunControlFSM.Transition
     */
    static void notifyPanel(final IguiPanel p, final RunControlFSM.State srcState, final RunControlFSM.State dstState) {
        p.execRcStateChanged(srcState, dstState);
    }

    /**
     * Notifies the <code>p</code> tabbed panel that it has been selected.
     * 
     * @see IguiFrame#stateChanged(javax.swing.event.ChangeEvent)
     * @param p Reference to the selected panel.
     */
    static void notifySelectedPanel(final IguiPanel p) {
        p.execPanelSelected();
    }

    /**
     * Notifies the <code>p</code> panel that it has been de-selected.
     * 
     * @see IguiFrame#stateChanged(javax.swing.event.ChangeEvent)
     * @param p Reference to the de-selected panel
     */
    static void notifyDeselectedPanel(final IguiPanel p) {
        p.execPanelDeselected();
    }

    /**
     * Simple helper method to create {@link ImageIcon} objects from an icon file name (icon files have to be included in the jar).
     * 
     * @param iconFileName Icon simple (not full) file name (i.e., <name.extension>)
     * @return The {@link ImageIcon} object (if the icon file is not found then it will return an empty icon)
     * @see IguiConstants#IGUI_ICONS_JAR_REF
     */
    static ImageIcon createIcon(final String iconFileName) {
        final java.net.URL imageURL = Igui.class.getResource(IguiConstants.IGUI_ICONS_JAR_REF + iconFileName);

        final ImageIcon icon;
        if(imageURL != null) {
            icon = new ImageIcon(imageURL);
        } else {
            icon = new ImageIcon();
        }

        return icon;
    }

    /**
     * It asks the IguiFrame to show a modal dialog asking the user if he/she wants to discard, postpone or commit pending changes to the
     * database.
     * <p>
     * The dialog will not be shown if the Igui is in status display mode.
     * 
     * @param panelMessage This message will be shown in the modal dialog window.
     * @see IguiFrame#showDBPostponeDiscardDialog(String)
     * @see IguiPanel#showDiscardDbDialog(String)
     */
    void showDbReloadOrDiscardDialog(final String panelMessage) {
        final IguiConstants.AccessControlStatus acs = this.getAccessLevel();

        if(acs.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Igui.this.mainFrame.showDBPostponeDiscardDialog(panelMessage);
                }
            });
        } else {
            IguiLogger.warning("Cannot show the db postpone/discard changes window when the access level status is \"" + acs.toString()
                               + "\"");
        }
    }

    /**
     * It gives the IS information structure containing the description of the Root Controller status.
     * 
     * @return The Root Controller information
     */
    RunControlApplicationData getRCInfo() {
        return this.rootControllerData;
    }

    /**
     * It return <code>true</code> if the environment needed by the elog dialog is correctly defined.
     * <p>
     * At the beginning the IguiFrame uses this method to setup in a proper way the elog item in the "Settings" menu.
     * 
     * @see #enableElogInterface(boolean)
     * @see ElogInterface#isElogEnvReady()
     * @return <code>true</code> if the environment needed by the elog dialog is correctly defined
     */
    boolean isElogEnvDefined() {
        return ElogInterface.isElogEnvReady();
    }

    /**
     * This method enables or disables the elog interface.
     * <p>
     * It is called when the user wants to enable/disable the elog interface from the Igui "Settings" menu.
     * 
     * @see ElogDialog#activate(boolean)
     * @param enable <code>true</code> if the elog interface has to be enabled
     * @throws IguiException.ElogException The environment is not correctly defined or the ElogDialog does not exist
     */
    void enableElogInterface(final boolean enable) throws IguiException.ElogException {
        if((enable == true) && (this.isElogEnvDefined() == false)) {
            throw new IguiException.ElogException("Neither the environment variable \"" 
                                                   + IguiConstants.ELOG_SERVER_URL_VAR + "\" or the property \""
                                                   + IguiConstants.ELOG_SERVER_URL_PROPERTY + "\" is defined");
        }

        final ElogDialog eld = this.getPanel(ElogDialog.class);
        if(eld != null) {
            eld.activate(enable);
        } else {
            throw new IguiException.ElogException("Cannot find the e-log dialog, got a null reference!");
        }
    }

    /**
     * It looks for the elog panel and, if it exists, asks it to create and show the dialog used to submit an elog entry.
     * <p>
     * This method has no effect if the IGUI is running in status display mode
     * 
     * @param panelName Name of the panel requesting to start the elog dialog
     * @param dialogTitle The dialog title
     * @param entrySubject The elog entry subject
     * @param messageText The elog entry message (it will be shown in the elog dialog text area)
     * @see ElogDialog#showSpecificDialog(String, String, String)
     * @return <code>false</code> if the elog dialog cannot be shown because it is disabled (it can be enabled in the IGUI "Settings" menu)
     * @throws IguiException.ElogException The elog panel does not exist or some problem occurred creating the dialog
     * @throws IllegalArgumentException The provided elog entry subject is null or empty
     */
    boolean showElogDialog(final String panelName, final String dialogTitle, final String entrySubject, final String messageText)
        throws IguiException.ElogException,
            IllegalArgumentException
    {
        boolean toReturn = true;

        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY) == true) {
            final ElogDialog eld = this.getPanel(ElogDialog.class);
            if(eld != null) {
                toReturn = eld.showSpecificDialog(dialogTitle, entrySubject, messageText);
            } else {
                throw new IguiException.ElogException("The ELOG dialog does not exist, got a null reference to it!");
            }
        } else {
            IguiLogger.warning("Asked by panel \"" + panelName + "\" to show an e-log dialog in status display mode, doing nothing");
        }

        return toReturn;
    }

    /**
     * It collects the elog messages from all the panels.
     * <p>
     * Potentially a panel may take information from whatever online service to build its message. So execute this method in a thread
     * different than the EDT.
     * 
     * @see ElogDialog
     * @return A string containing all the panel messages
     * @throws InterruptedException The thread was interrupted
     */
    String getPanelLogMessages() throws InterruptedException {
        final StringBuilder msg = new StringBuilder();

        // Map that will contain all the tasks
        final Map<IguiPanel, Future<String>> taskMap = new HashMap<IguiPanel, Future<String>>();

        // Submit tasks to each panel own thread
        for(final IguiPanel p : this.panelMap.values()) {
            taskMap.put(p, p.execElogString());
        }

        // Get the results
        final Set<Map.Entry<IguiPanel, Future<String>>> entrSet = taskMap.entrySet();
        for(final Map.Entry<IguiPanel, Future<String>> me : entrSet) {
            final String panelName = me.getKey().getPanelName();
            final Future<String> tsk = me.getValue();

            try {
                IguiLogger.debug("Getting e-log message from panel \"" + panelName + "\"");
                final String emsg = tsk.get(IguiConstants.PANEL_ACTION_TIMEOUT, TimeUnit.SECONDS);
                if(emsg.isEmpty() == false) {
                    msg.append(panelName + " = " + emsg + "\n");
                }
            }
            catch(final ExecutionException ex) {
                final String errMsg = "Error getting e-log message for panel \"" + panelName + "\": " + ex.getCause().toString();
                IguiLogger.error(errMsg, ex);
            }
            catch(final TimeoutException ex) {
                final String errMsg = "Timeout elapsed getting e-log message from panel \"" + panelName + "\"";
                IguiLogger.error(errMsg, ex);
            }
        }

        return msg.toString();
    }

    /**
     * It forks a process using the arguments contained in <code>args</code>.
     * <p>
     * arg[0] is always the binary name.
     * <p>
     * Process are always started using the {@link #processExecutor} executor.
     * <p>
     * If the process cannot be started for any reason then an error dialog is shown.
     * 
     * @see ProcessForker
     * @param args The arguments to start the new process.
     */
    void startProcess(final List<String> args) {
        this.processExecutor.execute(new Runnable() {
            @Override
            public void run() {
                final String procName = args.get(0);
                final String logFileName = IguiConstants.WORK_DIR + procName + "_" + System.currentTimeMillis() + ".log";
                try {
                    IguiLogger.info("Starting process \"" + procName + "\" (full command line is " + args.toString()
                                    + " and log is redirected to file \"" + logFileName + "\")");
                    new ProcessForker(args, new File(logFileName), new File(IguiConstants.WORK_DIR)).start();
                }
                catch(final Exception ex) {
                    final String errMsg = "Process \"" + procName + "\" cannot be started: " + ex;
                    IguiLogger.error(errMsg, ex);
                    ErrorFrame.showError("IGUI Error", errMsg, ex);
                }
            }
        });
    }

    /**
     * It loads (creates and adds it to the Igui frame tabbed pane) a panel on an user request basis.
     * <p>
     * On demand panel loading is performed in the {@link #onDemandPanelLoader} executor.
     * 
     * @param panelClassName The class name of the panel to create
     * @see #createTabbedPanels(Set)
     * @return A {@link Future} representing the loading task
     */
    Future<WeakReference<IguiPanel>> loadPanelOnDemand(final String panelClassName) {
        return this.onDemandPanelLoader.submit(new Callable<WeakReference<IguiPanel>>() {
            @Override
            public WeakReference<IguiPanel> call() throws IguiException.PanelLoadingFailed, InterruptedException {
                WeakReference<IguiPanel> ref;
                try {
                    ref = new WeakReference<IguiPanel>(Igui.this.createTabbedPanel(panelClassName));
                }
                catch(final IguiException.PanelRegistrationFailed ex) {
                    // Ignore the case in which the panel cannot be registered, it is already there
                    ref = new WeakReference<IguiPanel>(null);
                    IguiLogger.warning(ex.getMessage(), ex);
                }
                catch(final IguiException ex) {
                    throw new IguiException.PanelLoadingFailed(panelClassName, ex);
                }
                catch(final InterruptedException ex) {
                    final String msg = "Thread interrupted while loading panel \"" + panelClassName + "\"";
                    IguiLogger.warning(msg);
                    throw new InterruptedException(msg);
                }

                return ref;
            }
        });
    }

    /**
     * It removes a panel on an user request basis.
     * <p>
     * On demand panel removal is performed in the {@link #onDemandPanelLoader} executor.
     * 
     * @param panelClassName The class name of the panel to remove
     * @see #unregisterPanel(IguiPanel)
     */
    void removePanelOnDemand(final String panelClassName) {
        this.onDemandPanelLoader.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final Class<? extends IguiPanel> pClass = Class.forName(panelClassName).asSubclass(IguiPanel.class);
                    final IguiPanel p = Igui.this.getPanel(pClass);
                    if(p != null) {
                        Igui.this.unregisterPanel(p);
                    } else {
                        IguiLogger.debug("Panel \"" + panelClassName + "\" cannot be removed because it does not exist");
                    }
                }
                catch(final Exception ex) {
                    // Not an issue actually: if we are here then it means that the panel class has not been found,
                    // that also means the panel has never been created
                    IguiLogger.debug("Failed to remove panel \"" + panelClassName + "\": " + ex, ex);
                }
            }
        });
    }

    /**
     * It makes the Igui visible. If the initial infrastructure frame is still shown then it is made not visible and the full Igui window is
     * started at the same location.
     * <p>
     * This method can be called from any thread and not only from the EDT.
     * 
     * @see IguiFrame#makeVisible(Point)
     */
    void show() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String msg = "All done! IGUI is going to appear...";
                Igui.this.writeSplash(msg);
                IguiLogger.info(msg);

                // Find the location where the Igui window should appear
                Point location = new Point();
                if(Igui.this.initialInfrFrame != null) {
                    location = Igui.this.initialInfrFrame.getLocation();
                    Igui.this.initialInfrFrame.setVisible(false);
                    Igui.this.initialInfrFrame.removeAll();
                    Igui.this.initialInfrFrame.dispose();
                    Igui.this.initialInfrFrame = null;
                }

                // Show the main frame
                Igui.this.mainFrame.makeVisible(location);
            }
        });
    }

    /**
     * It forces the creation of panels defined in the Igui configuration
     * @throws InterruptedException The loading of the panel was interrupted
     * 
     * @see IguiConfiguration#getPanelsToLoad()
     */
    private void forceLoadPanels() throws InterruptedException {
        // Load panels defined in Igui settings
        final String panelsToForciblyLoad = IguiConfiguration.instance().getPanelsToLoad().trim();
        if(panelsToForciblyLoad.isEmpty() == false) {
            for(final String p : panelsToForciblyLoad.split(":")) {
                if(this.userTabPanelList.contains(p) == true) {
                    if(this.isPanelRegistered(p) == false) {
                        final String startMsg = "Force-load of panel of class \"" + p + "\" started";
                        IguiLogger.info(startMsg);
                        Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, startMsg);
        
                        try {
                            this.loadPanelOnDemand(p).get();
                            
                            final String doneMsg = "Force-load of panel of class \"" + p + "\" has been completed";
                            IguiLogger.info(doneMsg);
                            Igui.this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, doneMsg);
                            
                            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    Igui.this.mainFrame.updatePanelSelectionStatus(p, true);
                                }
                            });
                        }
                        catch(final ExecutionException ex) {
                            final Throwable cause = ex.getCause();
                            if(InterruptedException.class.isInstance(cause) == true) {
                                throw (InterruptedException) cause;
                            }
                            
                            IguiLogger.error("Cannot create panel of class \"" + p + "\": " + ex.getCause(), ex);
                            Igui.this.internalMessage(IguiConstants.MessageSeverity.ERROR,
                                                      "Cannot create panel of class \"" + p + "\": " + ex.getCause().getMessage());
                        }
                    } else {
                        IguiLogger.warning("Could not load panel of class \"" + p + "\": and instance of that panel already exists");
                    }
                } else {
                    IguiLogger.error("Could not load panel of class \"" + p + "\": invalid class name");
                }
            }
        }
    }
    
    /**
     * Convenience method retrieving a panel from the map and returning it with the right type.
     * 
     * @param panelClass The class of the panel to look for
     * @return The panel instance
     * @throws ClassCastException
     * @see {@link Class#cast(Object)}
     */
    private <T extends IguiPanel> T getPanel(final Class<T> panelClass) throws ClassCastException {
        return panelClass.cast(this.panelMap.get(panelClass));
    }

    /**
     * It gets the Igui properties defined for the specified software repository.
     * <p>
     * Multi-value properties are separated by ":".
     * 
     * @see #getUserPanels()
     * @see #setProperties()
     * @param swRepo The software repository
     * @return Map associating values to each property
     * @throws ConfigException 
     */
    private Map<String, StringBuilder> getIguiProperties(final dal.SW_Repository swRepo) throws ConfigException {
        final Map<String, StringBuilder> propMap = new HashMap<String, StringBuilder>();

        try {
            final String[] props = swRepo.get_IGUIProperties();
            if(props != null) {
                for(final String prop : props) {
                    final int index = prop.indexOf('=');
                    if(index != -1) {
                        // Be sure that the first two chars are "-D"
                        if(prop.substring(0, 2).equals("-D")) {
                            final String name = prop.substring(2, index);
                            String value;
                            try {
                                value = prop.substring(index + 1);
                            }
                            catch(final IndexOutOfBoundsException ex) {
                                value = "";
                            }
    
                            final boolean present = propMap.containsKey(name);
                            if(present) {
                                propMap.get(name).append(":" + value);
                            } else {
                                propMap.put(name, new StringBuilder(value));
                            }
                        }
                    }
                }
            }
    
            return propMap;
        }
        catch(final config.ConfigException ex) {
            throw new IguiException.ConfigException("Cannot retrieve IGUI properties from the database: " + ex, ex);
        }
    }

    /**
     * It scans all the SW repositories, gets the associated Igui properties and sets them as system properties.
     * <p>
     * The <code>igui.panel</code> property is skipped to enable an easier way to dynamically load use panels. Since this method connects to
     * the database system it should be called after its initialization.
     * 
     * @see #getIguiProperties(dal.SW_Repository)
     * @see #init(boolean)
     * @throws IguiException.ConfigException Some error occurred getting properties from the database
     */
    private void setProperties() throws IguiException.ConfigException {
        try {
            // Map containing the Igui properties
            final Map<String, StringBuilder> iguiPropMap = new HashMap<String, StringBuilder>();

            // Get all enabled SW repositories from DB
            final dal.SW_Repository[] reps = dal.Algorithms.get_used_repositories(this.dalPartition, false);
            if(reps != null) {
                for(final dal.SW_Repository r : reps) {
                    // For each SW repository get the IGUI properties
                    final Map<String, StringBuilder> repoPropMap = this.getIguiProperties(r);
                    // Remove the igui panel property
                    repoPropMap.remove(IguiConstants.IGUI_PANELS_PROPERTY);
                    // Add repository properties to the map
                    iguiPropMap.putAll(repoPropMap);
                }
            }

            // Set system property
            final Set<Map.Entry<String, StringBuilder>> entrySet = iguiPropMap.entrySet();
            for(final Map.Entry<String, StringBuilder> entry : entrySet) {
                System.setProperty(entry.getKey(), entry.getValue().toString());
            }
        }
        catch(final Exception ex) {
            final String msg = "Failed getting igui properties from database: " + ex;
            IguiLogger.error(msg, ex);
            throw new IguiException.ConfigException(msg, ex);
        }
    }

    /**
     * It gets the list of panels to create from database.
     * <p>
     * Panels are defined as properties in the SW Repository database object. This method will get panels associated only to 'enabled' SW
     * Repositories.
     * 
     * @return Set containing the class name of panels to create (the set grants that there will not be duplicated panels)
     * @throws IguiException.ConfigException An error occurred getting information from the database
     * @see Igui#getIguiProperties(dal.SW_Repository)
     */
    private Set<String> getUserPanels() throws IguiException.ConfigException {
        final Set<String> panelSet = new LinkedHashSet<String>();

        try {
            final dal.SW_Repository[] reps = dal.Algorithms.get_used_repositories(this.dalPartition, true);
            if(reps != null) {
                for(final dal.SW_Repository rep : reps) {
                    final Map<String, StringBuilder> repMap = this.getIguiProperties(rep);
                    // Get only the 'igui.panel' property
                    final StringBuilder value = repMap.get(IguiConstants.IGUI_PANELS_PROPERTY);
                    if(value != null) {
                        final String[] panelArray = value.toString().split(":");
                        for(final String pnl : panelArray) {
                            if(panelSet.add(pnl) == false) {
                                IguiLogger.warning("Panel \"" + pnl + "\" defined more than once in the database!");
                            }
                        }
                    }
                }
            }
        }
        catch(final Exception ex) {
            final String errMsg = "Failed getting the panel list from database: " + ex;
            throw new IguiException.ConfigException(errMsg, ex);
        }

        return panelSet;
    }

    /**
     * It builds the list of tab panels to be created.
     * <p>
     * This list will include both 'internal' panels (i.e., RunControl, Resources, etc) and user panels (both defined in the database or
     * passed as system properties).
     * 
     * @see #getUserPanels()
     */
    private void createInitialTabPanelLists() {
        // Add panels to the list of panels to be created
        this.panelListLockW.lock();
        try {
            // Add 'internal' panels
            this.privateTabPanelList.add(Igui.rcMainPanelClass);
            IguiLogger.info("Panel \"" + Igui.rcMainPanelClass + "\" added to the panel list");

            this.privateTabPanelList.add(Igui.segPanelClass);
            IguiLogger.info("Panel \"" + Igui.segPanelClass + "\" added to the panel list");

            this.privateTabPanelList.add(Igui.dsPanelClass);
            IguiLogger.info("Panel \"" + Igui.dsPanelClass + "\" added to the panel list");

            // Get the list of panels from system property
            final String panelProperty = System.getProperty(IguiConstants.IGUI_PANELS_PROPERTY);
            if(panelProperty != null) {
                final String[] iguiPanels = panelProperty.split(":");
                for(final String str : iguiPanels) {
                    if(this.privateTabPanelList.add(str) == true) {
                        IguiLogger.info("Panel \"" + str + "\" added to the panel list (from command line property)");
                    } else {
                        IguiLogger.warning("Panel \"" + str + "\" defined more than once (from command line property)!");
                    }
                }
            }

            // Get panels from database Igui properties
            final Set<String> uPanelList = this.getUserPanels(); // Throws IguiException.ConfigException
            IguiLogger.info("Adding the following user panels to the list: " + uPanelList.toString());
            this.userTabPanelList.addAll(uPanelList);
        }
        catch(final IguiException.ConfigException ex) {
            final String errMsg = ex.getMessage();
            this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
            IguiLogger.error(errMsg, ex);
        }
        finally {
            this.panelListLockW.unlock();
        }
    }

    /**
     * It updates the list of user panels getting a fresh list of them from the database.
     * <p>
     * The old and new lists of panels are compared and some panels are removed while some others are created.
     * 
     * @see ReloadDbTask
     * @see #getUserPanels()
     * @see #unregisterPanel(IguiPanel)
     * @see #createTabbedPanels(Set)
     * @throws IguiException.ConfigException Error getting list of user panels from the database
     */
    private void updateUserPanels() throws IguiException.ConfigException {
        // Create the fresh list of user panels
        final Set<String> newUserTabPanelSet = this.getUserPanels();

        // Create lists for panels to create and to remove
        final Set<String> panelsToCreate = new LinkedHashSet<String>();
        final Set<String> panelsToRemove = new HashSet<String>();

        this.panelListLockW.lock(); // Guard the list
        try {
            IguiLogger.info("Here is the list of user panels defined in the db: " + newUserTabPanelSet.toString() + " (it was "
                            + this.userTabPanelList.toString() + ")");

            // Look for panels to be removed
            final Iterator<String> it = this.userTabPanelList.iterator(); // Iterator needed to modify the list in the loop
            while(it.hasNext()) {
                final String s = it.next();
                if(newUserTabPanelSet.contains(s) == false) {
                    // This panel has to be removed
                    IguiLogger.info("User panel \"" + s + "\" will be removed");
                    panelsToRemove.add(s);
                    it.remove();
                }
            }

            // Look for panels to be created
            for(final String s : newUserTabPanelSet) {
                if(this.userTabPanelList.add(s) == true) {
                    // This panel has to be added
                    IguiLogger.info("User panel \"" + s + "\" will be added");
                    panelsToCreate.add(s);
                }
            }
        }
        finally {
            this.panelListLockW.unlock();
        }

        // Remove panels: it may that the panel is only in the list of available panels (in the gui frame)
        // and it has not been loaded yet.
        for(final String s : panelsToRemove) {
            try {
                final IguiPanel pr = this.panelMap.get(Class.forName(s));
                if(pr != null) {
                    this.unregisterPanel(pr);
                } else {
                    final String errMsg = "Panel \"" + s
                                          + "\" cannot be removed because no instances of it can be found (probably has not been dinamically loaded)";
                    IguiLogger.warning(errMsg);
                }
            }
            catch(final ClassNotFoundException ex) {
                final String errMsg = "Panel \"" + s + "\" cannot be removed because its class cannot be determined: " + ex.getMessage();
                IguiLogger.error(errMsg, ex);
            }
        }

        // Here new/old panels must be added/removed from the list only
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                for(final String s : panelsToRemove) {
                    Igui.this.removePanelFromFrameList(s);
                }

                for(final String s : panelsToCreate) {
                    Igui.this.addPanelToFrameList(s);
                }
            }
        });
    }

    /**
     * This method connects the Igui to the partition base infrastructure.
     * <p>
     * Here is a list of executed actions:
     * <ul>
     * <li>The partition name is extracted from system property;
     * <li>The {@link config.Configuration} objects are initialized;
     * <li>The Root Controller name is extracted from database;
     * <li>Subscription to the Root Controller IS server is done;
     * <li>The Resource Manager server is asked for resources (CONTROL resource is asked only if the {@link IguiConstants#IGUI_USE_RM_PROPERTY}
     * property is defined);
     * <li>The Commander object is created (used to send commands to controllers).
     * </ul>
     * 
     * @throws IguiException.UndefinedPartition No partition has been defined
     * @throws IguiException.InvalidPartition The defined partition is not valid
     * @throws IguiException.RMException Failed to contact the RM server or resource allocation is denied
     * @throws IguiException.ConfigException Failed to initialize the connection to the DB
     * @throws IguiException.ISException Failed to initialize the connection to the IS server
     * @see #initDb()
     * @see #initIS()
     * @see #setRootControllerData()
     * @see #askRMResource(Igui.IguiConstants.AccessControlStatus, boolean)
     */
    private final void connectBaseInfrastructure()
        throws IguiException.UndefinedPartition,
            IguiException.InvalidPartition,
            IguiException.RMException,
            IguiException.ConfigException,
            IguiException.ISException
    {
        // Create the ipc.Partition object
        final String partitionName = System.getProperty(IguiConstants.PARTITION_PROPERTY);
        if(partitionName == null) {
            throw new IguiException.UndefinedPartition();
        }

        IguiLogger.info("Igui is starting in partition " + partitionName);

        this.partition = new ipc.Partition(partitionName);
        if(!this.partition.isValid()) {
            throw new IguiException.InvalidPartition(partitionName);
        }

        // Create the object handling remote calls and IPC publication
        this.servant = new IguiServant(this.partition);

        // Ask RM resources
        try {
            if(IguiConfiguration.instance().askControl() == true) {
                try {
                    IguiLogger.info("Asking the Resource Manager for the CONTROL resource");
                    this.askRMResource(IguiConstants.AccessControlStatus.CONTROL, false);
                }
                catch(final IguiException.RMException ex) {
                    IguiLogger.info("The Resource Manager denied the CONTROL resource, trying to acquire the DISPLAY one");
                    this.askRMResource(IguiConstants.AccessControlStatus.DISPLAY, false);
                }
            } else {
                IguiLogger.info("Asking the Resource Manager for the DISPLAY resource");
                this.askRMResource(IguiConstants.AccessControlStatus.DISPLAY, false);
            }
        }
        catch(final IguiException.RMException ex) {
            final String errMsg = "The Resource Manager denied resource allocation to start the IGUI: " + ex;
            IguiLogger.error(errMsg, ex);
            throw new IguiException.RMException(errMsg, ex);
        }

        // Create the Commander
        this.rcCommander = new daq.rc.CommandSender(this.partition.getName(), "IGUI");

        // Create the RDB Config objects
        this.initDb();

        // Set the RootControllerName (get it from DB)
        this.setRootControllerData();

        // Get RootController info and subscribe to RootController IS server
        this.initIS();

        // WARNING: it is important that initIS() follows setRootControllerData()
    }

    /**
     * The {@link config.Configuration} read-only and read-write objects are created. Subscription to database updates is done as well.
     * <p>
     * This method should always be called before {@link #setRootControllerData()} and {@link #initIS()}.
     * 
     * @throws IguiException.ConfigException The {@link config.Configuration} object cannot be created or the database change subscription
     *             fails
     */
    private final void initDb() throws IguiException.ConfigException {
        try {
            // The read-only object: create it and subscribe
            this.db = new config.Configuration(IguiConfiguration.instance().getConfigName());
            IguiLogger.debug("Created read-only Configuration object");

            this.dalPartition = dal.Partition_Helper.get(this.db, this.partition.getName());
            if(this.dalPartition == null) {
                final String errString = "The partition cannot be found in the database: got a null reference";
                IguiLogger.error(errString);
                throw new IguiException.ConfigException(errString);
            }

            // Register converter
            this.dalConverter = new dal.SubstituteVariables(this.dalPartition);
            this.db.register_converter(this.dalConverter);

            // Get the Online Segment
            this.setOnlineSegment(this.dalPartition);

            // Determine if the config layer uses the GIT back-end
            try {
                if(this.dalPartition.get_config_version().isEmpty() == false) {
                    this.configUsesGitBackend.set(true);
                    IguiLogger.info("OKS-GIT is active");
                } else {
                    this.configUsesGitBackend.set(false);
                    IguiLogger.info("OKS-GIT is not active");
                }
            }
            catch(final config.ConfigException ex) {
                this.configUsesGitBackend.set(false);
                IguiLogger.info("OKS-GIT is not active");
            }
            
            // The RW object
            this.dbRW = new config.Configuration(IguiConfiguration.instance().getRWConfigName());

            // Subscribe for db changes
            this.dbSubscription = new config.Subscription(new ConfigCallback(), null);
            this.db.subscribe(this.dbSubscription);
            IguiLogger.debug("Subscribed to database changes");
            // TODO: what if subscription fails?
        }
        catch(final config.ConfigException ex) {
            throw new IguiException.ConfigException("Failed initializing the configuration: " + ex.getMessage(), ex);
        }
    }

    /**
     * It retrieves (from the Configuration) and sets the reference to the Online Segment.
     * <p>
     * NOTE: the Online Segment needs to be retrieved again at every reload of the database. Indeed it is calculated in such a way to fill
     * the information about all the applications and for the whole depth of the database (it actually finds all the applications in the
     * system).
     * 
     * @param partition The {@link dal.Partition} object the Online Segment belongs to
     * @throws IguiException.ConfigException Some error occurred accessing the system configuration
     */
    private void setOnlineSegment(final dal.Partition partition) throws IguiException.ConfigException {
        try {
            final dal.OnlineSegment onl = partition.get_OnlineInfrastructure();
            if(onl == null) {
                throw new IguiException.ConfigException("Failed getting the Online Segment from database: got a null reference");
            }

            this.onlineSegment = partition.get_segment(onl.UID());
            if(this.onlineSegment.is_disabled() == true) {
                throw new IguiException.ConfigException("Failed getting the Online Segment from database: it is disabled");
            }
        }
        catch(final config.ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting the Online Segment from database: " + ex.getMessage(), ex);
        }
    }

    /**
     * It takes all the information about the Root Controller from the configuration database.
     * <p>
     * This method should always follow {@link #initDb()} and {@link #setOnlineSegment(dal.Partition)}, and precede {@link #initIS()}.
     * <p>
     * Be careful that any call to this method will reset any Root Controller information coming from IS.
     * 
     * @throws IguiException.ConfigException The Root Controller cannot be found in the database
     */
    private final void setRootControllerData() throws IguiException.ConfigException {
        this.rootControllerData = RunControlApplicationData.createControllerApplicationData(this.onlineSegment);
    }

    /**
     * This method connects the Igui to the IS infrastructure.
     * <p>
     * It should always be called after {@link #setRootControllerData()}.
     * <ul>
     * <li>The current partition IS repository is created;
     * <li>A subscription to the Root Controller IS information is done;
     * <li>The current Root Controller state information is taken from the IS server.
     * </ul>
     * 
     * @throws IguiException.ISException
     */
    private final void initIS() throws IguiException.ISException {
        final String rootControllerName = this.getRootControllerName();

        try {
            // Create subscription criteria
            this.rootCtrlReceiver = new RootCtrlReceiver(new is.Criteria(Pattern.compile(".*" + rootControllerName + ".*")));

            // NOTE: here the subscription is done but call-backs are executed only after all the
            // panels building and initialization have been performed. This permits to get fresh values
            // from IS here without bothering about any possible interference by IS call-backs.

            // Subscribe to the Root Controller IS information
            this.rootCtrlReceiver.subscribe();
            IguiLogger.debug("Subscribed to the IS server \"" + IguiConstants.RC_IS_SERVER_NAME + "\" for Root Controller information");

            // Get the Root Controller current state as it is published in IS: if you get an
            // InfoNotFoundException then it just means that the RootController state is not
            // published yet: it will be updated later via IS call-backs
            try {
                final rc.RCStateInfo rootCtlrStateInfo = new rc.RCStateInfo();
                this.rootCtrlReceiver.getRepository().getValue(IguiConstants.RC_IS_SERVER_NAME + "." + rootControllerName,
                                                               rootCtlrStateInfo);
                this.rootControllerData.updateInfo(rootCtlrStateInfo, true);
            }
            catch(final is.InfoNotFoundException ex) {
                IguiLogger.warning("Failed getting RootController information which is not published in IS yet", ex);
            }

            try {
                final rc.DAQApplicationInfo rootCtrlAppInfo = new rc.DAQApplicationInfo();
                this.rootCtrlReceiver.getRepository().getValue(IguiConstants.SUPERVISION_IS_INFO_NAME + "." + rootControllerName,
                                                               rootCtrlAppInfo);
                this.rootControllerData.updateInfo(rootCtrlAppInfo, true);
            }
            catch(final is.InfoNotFoundException ex) {
                IguiLogger.warning("Failed getting the RootController supervision information which is not published in IS yet", ex);
            }
        }
        catch(final Exception ex) {
            throw new IguiException.ISException("Error getting or subscribing to RootController info: " + ex, ex);
        }
    }

    /**
     * Method called after a panel instance has been created.
     * <p>
     * It adds the panel to the map keeping references of the created panels, registers the panel to the FSM and then adds it to the main
     * frame tab pane.
     * <p>
     * The method throws {@link PanelRegistrationFailed} if a panel of the same class is already built. It is a caller duty to properly
     * terminate the <code>panel</code> instance (i.e., calling the {@link #unregisterPanel(IguiPanel)} method) when the registration fails.
     * 
     * @param panel The panel to register
     * @throws PanelRegistrationFailed A panel of the same class is already present
     * @see RunControlFSM#addSubscriberPanel(IguiPanel)
     * @see IguiFrame#addPanelToTab(IguiPanel)
     */
    private void registerPanel(final IguiPanel panel) throws PanelRegistrationFailed {
        // Get the actual panel class
        final Class<? extends IguiPanel> pc = panel.getClass();

        // Put the panel into the map: if a panel of the same kind is already present then it is an Igui bug
        // because the Igui should never try to build the same kind of panel more than once.
        final IguiPanel oldPanel = this.panelMap.putIfAbsent(pc, panel);
        if(oldPanel == null) {
            IguiLogger.debug("The panel \"" + panel.getPanelName() + "\" has been added to the panel map");

            // Subscribe (if needed) the panel to FSM state changes
            if(panel.rcStateAware()) {
                RunControlFSM.instance().addSubscriberPanel(panel);
            }

            // Add the panel to the tab pane in the main frame
            if(panel.isTabbed()) {
                if(javax.swing.SwingUtilities.isEventDispatchThread()) {
                    Igui.this.addPanelToTab(panel);
                } else {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            Igui.this.addPanelToTab(panel);
                        }
                    });
                }
            }
        } else {
            // Check that we are not trying to register the same panel twice
            // Here with "same" we mean the same REFERENCE, not the same panel type
            // If the same then do nothing, if not then it means that a panel of that
            // class was already registered and an exception is thrown
            if(oldPanel.equals(panel) == false) {
                throw new IguiException.PanelRegistrationFailed(pc.toString());
            }
        }
    }

    /**
     * Method called when a panel is terminated or the Igui is going to exit.
     * <p>
     * The panel is removed from the panel map, it is removed from the main frame tab pane and the {@link IguiPanel#panelTerminated()}
     * method is executed in the panel own thread.
     * 
     * @param panel The panel to terminate
     * @return A Future representing the panel termination task
     * @see ExitThread
     * @see IguiPanel#panelTerminated()
     * @see IguiFrame#removePanelFromTab(IguiPanel)
     */
    private Future<?> unregisterPanel(final IguiPanel panel) {
        // Remove panel reference from the map: check that the reference is the one referring to
        // the panel we are actually going to unregister. The panel should be terminated even if its instance
        // is not in the map (e.g., the panel fails its initialization and has not been registered yet).
        this.panelMap.remove(panel.getClass(), panel);

        // Remove panel from the main frame tab pane: this is always executed in the EDT
        if(panel.isTabbed()) {
            if(javax.swing.SwingUtilities.isEventDispatchThread()) {
                Igui.this.removePanelFromTab(panel);
            } else {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Igui.this.removePanelFromTab(panel);
                    }
                });
            }
        }

        // Exec the panelTerminated method in the panel own thread
        return panel.execPanelTerminated();
    }

    /**
     * It returns a set containing the class names of all the registered panels. 
     * 
     * @return A set containing the class names of all the registered panel
     */
    Set<String> getRegisteredPanels() {
        final Set<String> panels = new TreeSet<>();
        
        for(final Class<? extends IguiPanel> c : this.panelMap.keySet()) {
            panels.add(c.getName());
        }
        
        return panels;
    }
    
    /**
     * It checks whether a certain panel of class <code>panelClassName</code> is registered.
     * 
     * @param panelClassName The panel class name
     * @return <code>true</code> if the panel is registered
     */    
    boolean isPanelRegistered(final String panelClassName) {
        try {
            return this.panelMap.containsKey(Class.forName(panelClassName));
        }
        catch(final ClassNotFoundException ex) {
            return false;
        }
    }
    
    /**
     * Used to create the ERS log panel, the MainCommand panel and the ElogDialog. This method will not return until the panel has been
     * created and fully initialized.
     * <p>
     * <ul>
     * <li>Panel constructors are called in the EDT;
     * <li>Panel initialization methods are called in their own thread;
     * <li>Panels are registered;
     * </ul>
     * <p>
     * Do not use this method to create tab-panels.
     * 
     * @param panelClass The class the panel will be instance of
     * @return The panel instance
     * @throws IguiException.PanelInstanceNotCreated Failed to create the panel instance
     * @throws IguiException.PanelInitializationFailed Failed to initialize the panel
     * @throws InterruptedException The thread has been interrupted
     * @see #initPanel(IguiPanel, Igui.RunControlFSM.State)
     * @see #registerPanel(IguiPanel)
     * @see #createEmbeddedPanels()
     */
    private <T extends IguiPanel> T createNonTabbedPanel(final Class<T> panelClass)
        throws IguiException.PanelInstanceNotCreated,
            IguiException.PanelInitializationFailed,
            InterruptedException
    {
        // The task to create the panel in the EDT
        final FutureTask<T> createPanelTask = new FutureTask<T>(new Callable<T>() {
            @Override
            public T call() throws Exception {
                final String msg = "Creating the panel instance of class \"" + panelClass.getName() + "\"...";
                Igui.this.writeSplash(msg);
                IguiLogger.info(msg);

                final Constructor<T> ctr = panelClass.getConstructor(Boolean.TYPE);
                return ctr.newInstance(Boolean.FALSE);
            }
        });
        // Send the task to the EDT
        javax.swing.SwingUtilities.invokeLater(createPanelTask);

        // Wait for the task termination
        final T panel;
        try {
            panel = createPanelTask.get();
        }
        catch(final InterruptedException ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new IguiException.PanelInstanceNotCreated(panelClass.getName(), ex);
        }

        // Initialize the panel in its own thread
        final RunControlFSM.State rcState = this.getRCState();
        final Future<?> initPanelTask = this.initPanel(panel, rcState);
        try {
            final String msg = "Waiting for the \"" + panel.getPanelName() + "\" panel to initialize...";
            this.writeSplash(msg);
            IguiLogger.info(msg);

            initPanelTask.get();
            this.registerPanel(panel);
        }
        catch(final InterruptedException ex) {
            throw ex;
        }
        catch(final IguiException.PanelRegistrationFailed ex) {
            final String errMsg = "Panel \"" + panel.getPanelName()
                                  + "\" has not been registered because a panel of the same kind already exists; the new instance will be cleaned up.";
            IguiLogger.warning(errMsg);
            this.unregisterPanel(panel);
        }
        catch(final Exception ex) {
            this.unregisterPanel(panel);
            throw new IguiException.PanelInitializationFailed(panelClass.getName(), ex);
        }

        return panel;
    }

    /**
     * It creates the ERS log panel and the MainCommand panel and puts them in the main frame.
     * <p>
     * The ElogDialog panel is created here as well: it is a non tabbed panel but it is not put into the IguiFrame.
     * 
     * @throws IguiException.PanelInitializationFailed Failed to initialize one of the panels
     * @throws IguiException.PanelInstanceNotCreated Failed to create one of the panels
     * @throws InterruptedException The thread has been interrupted
     * @see IguiFrame#setErsPanel(ErsPanel)
     * @see IguiFrame#setCmdPanel(MainPanel)
     * @see Igui#createNonTabbedPanel(Class)
     * @see ElogDialog
     */
    private void createEmbeddedPanels()
        throws IguiException.PanelInstanceNotCreated,
            IguiException.PanelInitializationFailed,
            InterruptedException
    {
        // The ERS log panel is created at the very beginning to be used when the
        // Root Controller infrastructure is tested
        ErsPanel ersPanel = this.getPanel(ErsPanel.class);
        if(ersPanel == null) {
            ersPanel = this.createNonTabbedPanel(ErsPanel.class);
        }

        MainPanel mainPanel = this.getPanel(MainPanel.class);
        if(mainPanel == null) {
            mainPanel = this.createNonTabbedPanel(MainPanel.class);
        }

        this.mainFrame.setErsPanel(ersPanel);
        this.mainFrame.setCmdPanel(mainPanel);

        // Create the ElogDialog and
        final ElogDialog elogPanel = this.getPanel(ElogDialog.class);
        if(elogPanel == null) {
            this.createNonTabbedPanel(ElogDialog.class);
        }
    }

    /**
     * It creates a tab-panel whose class is <code>panelClassName</code>.
     * <p>
     * For efficiency reason this method should be used only when panels are created one by one (e.g., for on demand loading) while the
     * {@link #createTabbedPanels(Set)} method should be used when more panels are created in a row.
     * 
     * @param panelClassName The class the new panel belongs to
     * @return The new panel instance
     * @throws IguiException.PanelInstanceNotCreated The panel instance cannot be created in the EDT
     * @throws InterruptedException Thread has been interrupted creating the panel
     * @throws PanelInitializationFailed The panel could not be initialized
     * @throws PanelRegistrationFailed The panel cannot be registered (i.e., an instance of that class already exists). In this case the
     *             caller has nothing to do for cleanup because the new panel instance is properly terminated internally.
     */
    private IguiPanel createTabbedPanel(final String panelClassName)
        throws IguiException.PanelInstanceNotCreated,
            InterruptedException,
            PanelInitializationFailed,
            PanelRegistrationFailed
    {
        // Do not process IS call-backs when panels are created: that may cause lost call-backs if executed between the
        // panel initialization and registration. It should be anyway a rare situation.
        // This lock is needed especially when new panels are created after a database reload or on-demand. The problem
        // with panels created at the Igui start time is solved with the count down latch itself.
        // The write lock is acquired by methods creating panels.
        this.isCallBackLock.readLock().lock();
        try {
            // RC state needed for panel initialization
            final RunControlFSM.State rcState = this.getRCState();

            // Create an instance of all the panels present in the panel list
            {
                final String msg = "Creating panel of class \"" + panelClassName + "\"...";
                this.internalMessage(MessageSeverity.INFORMATION, msg);
                IguiLogger.info(msg);
            }

            // Call panel constructor in the EDT
            final FutureTask<IguiPanel> panelCreationTask = new FutureTask<IguiPanel>(new Callable<IguiPanel>() {
                @Override
                public IguiPanel call() throws IguiException.PanelInstanceNotCreated {
                    return Igui.this.getPanelInstance(panelClassName);
                }
            });

            javax.swing.SwingUtilities.invokeLater(panelCreationTask);

            // Get panel references from EDT
            final IguiPanel panel;
            try {
                panel = panelCreationTask.get();
            }
            catch(final ExecutionException ex) {
                throw new IguiException.PanelInstanceNotCreated(panelClassName, ex.getCause());
            }

            // Initialize the panel and wait for it
            final Future<?> panelInitTask = this.initPanel(panel, rcState);
            try {
                final String msg = "Waiting for the \"" + panel.getPanelName() + "\" panel to initialize...";
                this.internalMessage(MessageSeverity.INFORMATION, msg);
                IguiLogger.info(msg);
                panelInitTask.get();
            }
            catch(final ExecutionException ex) {
                throw new IguiException.PanelInitializationFailed(panelClassName, ex.getCause());
            }

            // Register the panel
            try {
                this.registerPanel(panel);
            }
            catch(final IguiException.PanelRegistrationFailed ex) {
                this.unregisterPanel(panel);
                throw ex;
            }

            return panel;
        }
        finally {
            this.isCallBackLock.readLock().unlock();
        }
    }

    /**
     * It creates the requested panels.
     * <p>
     * <ul>
     * <li>Panel instances are created in the EDT;
     * <li>Panels are initialized in panel own threads;
     * <li>Panels are registered.
     * </ul>
     * If this procedure fails for some panel then that panel is not added to the main frame.
     * 
     * @param panelList Set of panels to be created (a set is used for avoid duplicated panels)
     * @throws InterruptedException The thread has been interrupted
     * @return A string collecting all the errors occurred while creating the panels
     * @see #getPanelInstance(String)
     * @see #initPanel(IguiPanel, Igui.RunControlFSM.State)
     * @see #registerPanel(IguiPanel)
     */
    private String createTabbedPanels(final Set<? extends String> panelList) throws InterruptedException {
        // Do not process IS call-backs when panels are created: that may cause lost call-backs if executed between the
        // panel initialization and registration. It should be anyway a rare situation.
        // This lock is needed especially when new panels are created after a database reload or on-demand. The problem
        // with panels created at the Igui start time is solved with the count down latch itself.
        // The write lock is acquired by methods processing IS call-backs.
        this.isCallBackLock.readLock().lock();
        try {
            final StringBuilder errorString = new StringBuilder();

            // RC state needed for panel initialization
            final RunControlFSM.State rcState = this.getRCState();

            // This map will contain the Future for the panel creation tasks
            // (scheduled into the EDT)
            // panel class name -> task Future
            final Map<String, Future<IguiPanel>> creationTaskMap = new LinkedHashMap<String, Future<IguiPanel>>(10);

            // This map will contain the Future for the panel initialization tasks
            // (executed in panel threads)
            final Map<IguiPanel, Future<?>> initTaskMap = new LinkedHashMap<IguiPanel, Future<?>>(10);

            // Create an instance of all the panels present in the panel list
            for(final String s : panelList) {
                final String msg = "Creating panel \"" + s + "\"...";
                this.writeSplash(msg);
                IguiLogger.info(msg);

                // Call panel constructor in the EDT
                final FutureTask<IguiPanel> panelCreationTask = new FutureTask<IguiPanel>(new Callable<IguiPanel>() {
                    @Override
                    public IguiPanel call() throws IguiException.PanelInstanceNotCreated {
                        return Igui.this.getPanelInstance(s);
                    }
                });
                javax.swing.SwingUtilities.invokeLater(panelCreationTask);
                creationTaskMap.put(s, panelCreationTask);
            }

            {
                // Get panel references from EDT and initialize them
                final Set<Map.Entry<String, Future<IguiPanel>>> mapEntrySet = creationTaskMap.entrySet();
                for(final Map.Entry<String, Future<IguiPanel>> mapEntry : mapEntrySet) {
                    try {
                        final IguiPanel panel = mapEntry.getValue().get();
                        // Panels are initialized in their own thread; put the task
                        // Future into the map to retrieve its result afterwards
                        final Future<?> panelInitTask = this.initPanel(panel, rcState);
                        initTaskMap.put(panel, panelInitTask);
                    }
                    catch(final InterruptedException ex) {
                        throw ex;
                    }
                    catch(final Exception ex) {
                        final StringBuilder errMsg = new StringBuilder();
                        errMsg.append("Failed creating panel \"" + mapEntry.getKey() + "\": ");
                        if(ExecutionException.class.isInstance(ex)) {
                            errMsg.append(ex.getCause());
                        } else {
                            errMsg.append(ex);
                        }
                        errorString.append("   " + errMsg + "\n");
                        IguiLogger.error(errMsg.toString(), ex);
                    }
                }
            }

            {
                // Wait for panels to initialize before registering them
                final Set<Map.Entry<IguiPanel, Future<?>>> mapEntrySet = initTaskMap.entrySet();
                for(final Map.Entry<IguiPanel, Future<?>> mapEntry : mapEntrySet) {
                    final String panelName = mapEntry.getKey().getPanelName();
                    try {
                        // Get the result of the panel initialization
                        final String msg = "Waiting for the \"" + panelName + "\" panel to initialize...";
                        this.writeSplash(msg);
                        IguiLogger.info(msg);
                        mapEntry.getValue().get();
                        // If the panel init was successful then register the panel
                        this.registerPanel(mapEntry.getKey());
                    }
                    catch(final InterruptedException ex) {
                        throw ex;
                    }
                    catch(final IguiException.PanelRegistrationFailed ex) {
                        final String errMsg = "Panel \"" + panelName
                                              + "\" has not been registered because a panel of the same kind already exists; the new instance will be cleaned up.";
                        IguiLogger.warning(errMsg);
                        this.unregisterPanel(mapEntry.getKey());
                    }
                    catch(final Exception ex) {
                        final StringBuilder errMsg = new StringBuilder();
                        errMsg.append("Panel \"" + panelName + "\" failed its initialization: ");
                        if(ExecutionException.class.isInstance(ex)) {
                            errMsg.append(ex.getCause());
                        } else {
                            errMsg.append(ex);
                        }
                        errorString.append("   " + errMsg + "\n");
                        IguiLogger.error(errMsg.toString(), ex);

                        this.unregisterPanel(mapEntry.getKey());
                        IguiLogger.info("Panel \"" + panelName + "\" has been un-registered");
                    }
                }
            }

            creationTaskMap.clear();
            initTaskMap.clear();

            return errorString.toString();
        }
        finally {
            this.isCallBackLock.readLock().unlock();
        }
    }

    /**
     * It builds a panel instance starting from the panel class name.
     * <p>
     * Since panel constructors have to be called into the EDT then this method has to be executed in the EDT.
     * 
     * @param panelClassName The panel class name
     * @return The panel instance
     * @throws IguiException.PanelInstanceNotCreated
     */
    private IguiPanel getPanelInstance(final String panelClassName) throws IguiException.PanelInstanceNotCreated {
        IguiLogger.debug("Creating instance of panel whose class is \"" + panelClassName + "\"");

        try {
            final Class<?> panelClass = Class.forName(panelClassName);
            final Constructor<?> cnstrctr = panelClass.getConstructor(Igui.class);

            return IguiPanel.class.cast(cnstrctr.newInstance(this));
        }
        catch(final Exception ex) {
            Throwable cause = null;
            if(InvocationTargetException.class.isInstance(ex)) {
                cause = ex.getCause();
            }
            throw new IguiException.PanelInstanceNotCreated(panelClassName, (cause != null) ? cause : ex);
        }
    }

    /**
     * It initializes the panel in its own thread.
     * <p>
     * It executes {@link IguiPanel#execPanelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)}.
     * 
     * @param panel The IguiPanel to initialize
     * @param rcState The current Root Controller state
     * @return A Future representing the panel initialization task
     * @see IguiPanel#execPanelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    private Future<?> initPanel(final IguiPanel panel, final RunControlFSM.State rcState) {
        IguiLogger.debug("Initializing panel \"" + panel.getPanelName() + "\" in state \"" + rcState.toString() + "\"");

        return panel.execPanelInit(rcState, this.getAccessLevel());
    }

    /**
     * It adds a panel to the main frame tab pane.
     * <p>
     * This method has to be executed into the EDT.
     * 
     * @param panel The panel to be added to the main frame tab pane
     * @see IguiFrame#addPanelToTab(IguiPanel)
     */
    private void addPanelToTab(final IguiPanel panel) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

        this.mainFrame.addPanelToTab(panel);
        IguiLogger.debug("Panel \"" + panel.getPanelName() + "\" added to the tab");
    }

    /**
     * It adds a panel to the list of panels available for on-demand loading in the gui frame toolbar.
     * 
     * @param panelClassName The class name of the panel to add.
     * @see #updateUserPanels()
     * @see IguiFrame#addPanelToList(String)
     */
    private void addPanelToFrameList(final String panelClassName) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

        this.mainFrame.addPanelToList(panelClassName);
        IguiLogger.debug("Panel \"" + panelClassName + "\" added to the frame panel list");
    }

    /**
     * It removes a panel from the main frame tab pane.
     * <p>
     * This method has to be executed into the EDT.
     * 
     * @param panel The panel to be removed
     * @see IguiFrame#removePanelFromTab(IguiPanel)
     */
    private void removePanelFromTab(final IguiPanel panel) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";
        this.mainFrame.removePanelFromTab(panel);

        IguiLogger.debug("Panel \"" + panel.getPanelName() + "\" removed from the tab");
    }

    /**
     * It removes the panel from the panel list in the gui toolbar.
     * <p>
     * To be executed in the EDT.
     * 
     * @param panelClassName The name of the class of the panel to remove from the list.
     * @return <code>true</code> if the panel has been successfully removed. #see {@link #updateUserPanels()}
     * @see IguiFrame#removePanelFromList(String)
     */
    private boolean removePanelFromFrameList(final String panelClassName) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

        return this.mainFrame.removePanelFromList(panelClassName);
    }

    /**
     * It writes a message on the splash screen.
     * <p>
     * Every message sent to the splash screen is also sent to the ERS log panel as an INFO message.
     * 
     * @param text The message text
     */
    private void writeSplash(final String text) {
        try {
            if((this.splashGraphic != null) && (this.splash != null)) {
                synchronized(this.splash) {
                    final Dimension splashDimension = this.splash.getSize();
                    this.splashGraphic.setComposite(java.awt.AlphaComposite.Clear);
                    this.splashGraphic.fillRect(0, 0, splashDimension.width, splashDimension.height);
                    this.splashGraphic.setPaintMode();
                    this.splashGraphic.setColor(java.awt.Color.BLACK);
                    this.splashGraphic.setFont(this.bold);
                    this.splashGraphic.drawString(text, 0, splashDimension.height - 10);
                    this.splash.update();
                }
            }
        }
        catch(final Exception ex) {
            // Ignore
        }
        finally {
            this.internalMessage(IguiConstants.MessageSeverity.INFORMATION, text);
        }
    }

    /**
     * This method processes call-back calls coming from the Root Controller IS.
     * <p>
     * The main frame is notified about the change in the Root Controller state and, whenever a state transition is detected, all the panels
     * registered to the FSM are updated.
     * 
     * @param rcInfo The IS event containing the Root Controller information
     * @see IguiFrame#rcInfoUpdated(RunControlApplicationData)
     * @see RunControlFSM#executeTransition(Igui.RunControlFSM.State, Igui.RunControlFSM.State)
     */
    private void rcStateChanged(final is.InfoEvent rcInfo) {
        try {
            // To not keep jacorb threads busy execute the code in a separate executor
            this.rcStateChangeExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        // Wait until the call-back processing is enabled (after all the panels are initialized and registered)
                        Igui.this.isCallBackLatch.await();

                        // This second synchronization is needed all the times new panels are created
                        // after a database reload or on-demand.
                        // The read lock is acquired by methods creating panels.
                        Igui.this.isCallBackLock.writeLock().lock();
                        try {
                            // Store the current RootController state
                            final RunControlFSM.State srcState = Igui.this.rootControllerData.getISStatus().getState();

                            // Update the RootController information
                            long timeCompare = -1L;

                            final is.Type infoType = rcInfo.getType();
                            if(infoType.equals(rc.RCStateInfo.type)) {
                                // RC info
                                final rc.RCStateInfo i = new rc.RCStateInfo();
                                rcInfo.getValue(i);
                                timeCompare = Igui.this.rootControllerData.updateInfo(i, false);
                                if(timeCompare == 0) {
                                    Igui.this.rootCtrlReceiver.getRepository().getValue(rcInfo.getName(), i);
                                    timeCompare = Igui.this.rootControllerData.updateInfo(i, true);
                                }
                            } else if(infoType.equals(rc.DAQApplicationInfo.type)) {
                                // DAQ app info
                                final rc.DAQApplicationInfo i = new rc.DAQApplicationInfo();
                                rcInfo.getValue(i);
                                timeCompare = Igui.this.rootControllerData.updateInfo(i, false);
                                if(timeCompare == 0) {
                                    Igui.this.rootCtrlReceiver.getRepository().getValue(rcInfo.getName(), i);
                                    timeCompare = Igui.this.rootControllerData.updateInfo(i, true);
                                }
                            }

                            // If the information is new then do something
                            if(timeCompare >= 0) {
                                // Notify the main frame
                                Igui.this.mainFrame.rcInfoUpdated(Igui.this.rootControllerData);

                                // Check if a state transition happened
                                final RunControlFSM.State dstState = Igui.this.rootControllerData.getISStatus().getState();

                                if(srcState.equals(dstState) == false) {
                                    // State transition happened: execute calls on registered panels
                                    IguiLogger.debug("State transition detected from \"" + srcState.toString() + "\" to \""
                                                     + dstState.toString() + "\". Subscribed panels are going to be notified");
                                    RunControlFSM.instance().executeTransition(srcState, dstState);
                                }
                            }
                        }
                        finally {
                            Igui.this.isCallBackLock.writeLock().unlock();
                        }
                    }
                    catch(final InterruptedException ex) {
                        IguiLogger.warning("The Root Controller IS call-back processing thread has been interrupted: " + ex, ex);
                        Thread.currentThread().interrupt();
                    }
                    catch(final Exception ex) {
                        IguiLogger.error("Error processing RootController IS callback: " + ex.getMessage(), ex);
                    }
                }
            });
        }
        catch(final Exception ex) {
            final String errMsg = "Impossible to process RootController IS callback: " + ex.getMessage();
            this.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
            IguiLogger.error(errMsg, ex);
        }
    }

    /**
     * It setups the initial frame used to check the Root Controller infrastructure.
     * <p>
     * The ERS panel is created here and and used in {@link #createEmbeddedPanels()}.
     * 
     * @see IguiFrame.InitialInfrastructureFrame
     * @see IguiFrame#buildInfrTestFrame(InitialInfrastructureTestsPanel, ErsPanel)
     * @see RunControlTestsPanel.InitialInfrastructureTestsPanel
     * @see #checkRootControllerInfrastructure()
     * @throws IguiException.PanelInstanceNotCreated Failed creating the ERS panel
     * @throws IguiException.PanelInitializationFailed Failed initializing the ERS panel
     * @throws IguiException.EDTException Exception occurred in the EDT creating the frame or the infrastructure tree panel
     * @throws InterruptedException The thread has been interrupted
     */
    private void setupInfrastructureCheckFrame()
        throws IguiException.PanelInstanceNotCreated,
            IguiException.PanelInitializationFailed,
            IguiException.EDTException,
            InterruptedException
    {
        // Create the ERS panel
        final ErsPanel ersPanel = Igui.this.createNonTabbedPanel(ErsPanel.class);

        // Create and setup the frame which will contain the ERS log panel and the infrastructure tree: execute it in the EDT
        final FutureTask<InitialInfrastructureFrame> task = new FutureTask<InitialInfrastructureFrame>(new Callable<InitialInfrastructureFrame>()
        {
            @Override
            public InitialInfrastructureFrame call() {
                final InitialInfrastructureTestsPanel infrP = new InitialInfrastructureTestsPanel(Igui.this.initialInfrReadyLock,
                                                                                                  Igui.this.initialInfrReadyCond);
                final InitialInfrastructureFrame fr = Igui.this.mainFrame.buildInfrTestFrame(infrP, ersPanel);
                fr.setVisible(true);
                return fr;
            }
        });

        // Send the task to the EDT
        try {
            javax.swing.SwingUtilities.invokeLater(task);
            Igui.this.initialInfrFrame = task.get();
        }
        catch(final InterruptedException ex) {
            throw ex;
        }
        catch(final Exception ex) {
            throw new IguiException.EDTException("Cannot create the infrastructure test frame: " + ex.getMessage(), ex);
        }
    }

    /**
     * This method locks until the Root Controller infrastructure is checked.
     * <p>
     * This method will take care of creating the initial frame with the ERS log panel and the Root Controller infrastructure panel.
     * <p>
     * The lock is done using the {@link #initialInfrReadyCond} condition variable.
     * 
     * @see RunControlTestsPanel.InitialInfrastructureTestsPanel#startInfrastructureCheck()
     * @see RunControlTestsPanel.InitialInfrastructureTestsPanel#isInfrastructureReady()
     * @see #setupInfrastructureCheckFrame()
     * @see IguiFrame.InitialInfrastructureFrame
     * @throws IguiException.PanelInstanceNotCreated Failed to create the initial frame
     * @throws IguiException.PanelInitializationFailed Failed to create the initial frame
     * @throws IguiException.EDTException Failed to create the initial frame
     * @throws InterruptedException Thread was interrupted
     */
    private void checkRootControllerInfrastructure()
        throws IguiException.PanelInstanceNotCreated,
            IguiException.PanelInitializationFailed,
            IguiException.EDTException,
            InterruptedException
    {
        // Setup the frame with the ERS log panel and the infrastructure tree
        this.setupInfrastructureCheckFrame();

        // Wait for the infrastructure tests to be finished
        final InitialInfrastructureTestsPanel infrPanel = this.initialInfrFrame.getInfrPanel();
        infrPanel.startInfrastructureCheck();

        this.initialInfrReadyLock.lock();
        try {
            while(infrPanel.isInfrastructureReady() == false) {
                this.initialInfrReadyCond.await();
            }
        }
        finally {
            this.initialInfrReadyLock.unlock();
            this.initialInfrFrame.terminate();
        }
    }
}
