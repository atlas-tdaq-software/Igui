package Igui;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;

import daq.rmgr.ResourceNotFound;
import daq.rmgr.UnavailableResources;


/**
 * This class implements the interface to the Resource Manager.
 * <p>
 * It is used by the Igui to ask for resources and to free them when resources are released.
 */
class RMInterface {

    /**
     * The Resource Manager client interface
     */
    private final daq.rmgr.RMClientImpl rmClient = new daq.rmgr.RMClientImpl();

    /**
     * Integer storing the handle of the current acquired resource
     */
    private final AtomicInteger resourceHandle = new AtomicInteger();

    /**
     * Constructor.
     */
    RMInterface() {
    }

    /**
     * It asks the Resource Manager server to grant for a defined resource.
     * <p>
     * Once the requested resource has been correctly acquired, the old one is released.
     * 
     * @param accessLevel The access level corresponding to the resource to acquire
     * @throws IguiException.RMException Thrown if the Resource Manager system has some troubles
     * @see Igui#askRMResource(Igui.IguiConstants.AccessControlStatus, boolean)
     */
    void acquireResource(final IguiConstants.AccessControlStatus accessLevel) throws IguiException.RMException {
        try {
            // Use the 'requestResourceForMyProcess' so that the process PID will be available
            // to free the resources by hand in case of troubles
            final String localHostName = InetAddress.getLocalHost().getCanonicalHostName();
            final int rmHandle = this.rmClient.requestResourceForMyProcess(Igui.instance().getPartition().getName(),
                                                                           accessLevel.resourceName(),
                                                                           localHostName);
            if(rmHandle >= 0) {
                // If the resource handle is > 0 than it means that the resource was correctly acquired
                try {
                    // Now that the new resource has been acquired release the previous one
                    this.freeCurrentResource();
                }
                catch(final Exception ex) {
                    final String message = "Failed to release resource with handle " + this.resourceHandle.get() + ": " + ex.getMessage()
                                           + ".\nManual clean-up may be needed.";
                    IguiLogger.error(message);
                    ErrorFrame.showError("IGUI Error", message, ex);
                }
                // Store the freshly acquired resource
                this.resourceHandle.set(rmHandle);
            } else {
                throw new IguiException.RMException("The Resource Manager has not assigned a valid handle");
            }
        }
        catch(final ResourceNotFound ex) {
            throw new IguiException.RMException("The requested resource cannot be found", ex);
        }
        catch(final UnavailableResources ex) {
            final StringBuilder resOwner = new StringBuilder();
            try {
                final String[] owns = this.rmClient.getResourceOwners(accessLevel.resourceName(), Igui.instance().getPartition().getName());
                for(final String o : owns) {
                    resOwner.append(o);
                    resOwner.append(" ");
                }
            }
            catch(final Exception ex1) {
                resOwner.append("Unknown");
                IguiLogger.debug(ex1.toString(), ex1);
            }
            throw new IguiException.RMException("The requested resource is not available (already acquired by user \""
                                                + resOwner.toString().trim() + "\")", ex);
        }
        catch(final UnknownHostException ex) {
            throw new IguiException.RMException("Cannot get the local host name", ex);
        }
        catch(final Exception ex) {
            throw new IguiException.RMException("Got exception: " + ex, ex);
        }
    }

    /**
     * It asks the Resource Manager to free the already acquired resource.
     * 
     * @throws IguiException.RMException Thrown if the Resource Manager system has some troubles
     * @see Igui#iguiExit(boolean)
     */
    void freeCurrentResource() throws IguiException.RMException {
        // Get the handle corresponding to the current acquired resource
        final int token = this.resourceHandle.get();
        if(token > 0) {
            // If the token is > 0 then it means that some resource was previously acquired
            // The RM always return a handle (aka token) > 0 when a resource is acquired
            try {
                this.rmClient.freeResources(token);

                // If the resource is correctly released set the stored handle to 0
                this.resourceHandle.set(0);
            }
            catch(final Exception ex) {
                throw new IguiException.RMException("Got exception", ex);
            }
        }
    }
}
