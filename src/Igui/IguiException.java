package Igui;

/**
 * This is the class from which all the exceptions generated in the Igui package derive.
 */
public abstract class IguiException extends Exception {

    private static final long serialVersionUID = 4850946786724543549L;

    /**
     * Exception used by the IguiPanel class to notify the Igui that some errors occurred.
     */
    public static class IguiPanelError extends IguiException {

        private static final long serialVersionUID = 2132438277419677146L;
        private final String panelName;

        /**
         * Constructor.
         * 
         * @param errMsg The error message
         */
        IguiPanelError(final String panelName, final String errMsg) {
            super("IguiPanelError exception from panel \"" + panelName + "\": " + errMsg);
            this.panelName = panelName;
        }

        /**
         * Constructor.
         * 
         * @param errMsg The error message
         * @param cause The cause of this exception
         */
        IguiPanelError(final String panelName, final String errMsg, final Throwable cause) {
            super("IguiPanelError exception from panel \"" + panelName + "\": " + errMsg + " (cause was " + cause + ")", cause);
            this.panelName = panelName;
        }

        /**
         * Constructor.
         * 
         * @param cause The cause of this exception
         */
        IguiPanelError(final String panelName, final Throwable cause) {
            super("IguiPanelError exception from panel \"" + panelName + "\": cause was " + cause, cause);
            this.panelName = panelName;
        }

        /**
         * It return the name of the panel throwing this exception.
         * 
         * @return The name of the panel throwing this exception
         */
        public String getPanelName() {
            return this.panelName;
        }

    }

    /**
     * This exception is throws when a Root Controller state is not known to the FSM. This most likely means an Igui bug or a missing sync
     * with the run control. Any occurrence of this exception is a serious problem.
     */
    public static class UnknownState extends IguiException {

        private static final long serialVersionUID = 6351385836745181619L;

        /**
         * String representing the RC state
         */
        private final String state;

        /**
         * The constructor.
         * 
         * @param state The state unknown to the FSM
         * @param exCause The cause of this exception
         */
        UnknownState(final String state, final Throwable exCause) {
            super("The state " + state + " is not known to the FSM (cause was " + exCause + ")", exCause);
            this.state = state;
        }

        /**
         * It gives the state unknown to the FSM
         * 
         * @return The unknown state as a string
         */
        public final String getState() {
            return this.state;
        }
    }

    /**
     * This exception is throws when an application status is not known. This most likely means an Igui bug or a missing sync with the run
     * control. Any occurrence of this exception is a serious problem.
     */
    public static class UnknownStatus extends IguiException {

        private static final long serialVersionUID = -3459445632235963229L;

        /**
         * The unknown application status
         */
        private final String status;

        /**
         * The constructor
         * 
         * @param status The unknown application status
         * @param exCause The cause of this exception
         */
        UnknownStatus(final String status, final Throwable exCause) {
            super("The status " + status + " is not known to the FSM (cause was " + exCause + ")", exCause);
            this.status = status;
        }

        /**
         * It gives the unknown application status
         * 
         * @return The unknown application status as a string
         */
        public final String getStatus() {
            return this.status;
        }
    }

    /**
     * This exception is throws when a state transition is not known to the FSM. This most likely means an Igui bug or a missing sync with
     * the run control. Any occurrence of this exception is a serious problem.
     */
    public static class UnknownTransition extends IguiException {

        private static final long serialVersionUID = -8008405214975744494L;

        /**
         * The transition source state
         */
        private final String src;

        /**
         * The transition destination state
         */
        private final String dst;

        /**
         * The constructor.
         * 
         * @param src The transition source state
         * @param dst The transition destination state
         */
        UnknownTransition(final String src, final String dst) {
            super("The transition from state " + src + " to state " + dst + " is not known to the FSM.");
            this.src = src;
            this.dst = dst;
        }

        /**
         * It gives the transition source state
         * 
         * @return The transition source state as a string
         */
        public final String getDestinationState() {
            return this.dst;
        }

        /**
         * The transition destination state as a string
         * 
         * @return The transition destination state
         */
        public final String getSourceState() {
            return this.src;
        }
    }

    /**
     * Exception thrown any time some error occurs interacting with the configuration database system
     */
    static class ConfigException extends IguiException {

        private static final long serialVersionUID = -1747159117652003745L;

        /**
         * Constructor.
         * 
         * @param errMsg Message string
         */
        ConfigException(final String errMsg) {
            super(errMsg);
        }

        /**
         * Constructor
         * 
         * @param errMsg Message string
         * @param exCause The cause of this exception
         */
        ConfigException(final String errMsg, final Throwable exCause) {
            super(errMsg, exCause);
        }
    }

    /**
     * Exception thrown when the partition the Igui is started in does not exits or is not valid.
     */
    static class InvalidPartition extends IguiException {

        private static final long serialVersionUID = -4617262773116599829L;

        /**
         * Constructor.
         * 
         * @param partitionName The partition name
         */
        InvalidPartition(final String partitionName) {
            super("Partition \"" + partitionName + "\" is not running or invalid.");
        }

    }

    /**
     * Exception thrown any time an error occurs interacting with the IS system.
     */
    static class ISException extends IguiException {

        private static final long serialVersionUID = 314983891053599750L;

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         * @param exCause The cause of this exception
         */
        ISException(final String errMsg, final Throwable exCause) {
            super(errMsg, exCause);
        }

    }

    /**
     * Exception raised when some problem occurs during the database reloading procedure.
     */
    static class RDBException extends IguiException {

        private static final long serialVersionUID = -1747183123652001111L;
        private final boolean isSchemaChanged;

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         * @param isSchemaChanged Whether the exception is caused by a changed in the schema
         */
        RDBException(final String errMsg, final boolean isSchemaChanged) {
            super(errMsg);
            this.isSchemaChanged = isSchemaChanged;
        }
        
        /**
         * Constructor.
         * 
         * @param errMsg Error message
         */        
        RDBException(final String errMsg) {
            super(errMsg);
            this.isSchemaChanged = false;
        }

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         * @param exCause The cause of this exception
         */
        RDBException(final String errMsg, final Throwable exCause) {
            super(errMsg, exCause);
            this.isSchemaChanged = false;
        }

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         * @param isSchemaChanged Whether the exception is caused by a changed in the schema
         * @param exCause The cause of this exception
         */
        RDBException(final String errMsg, final boolean isSchemaChanged, final Throwable exCause) {
            super(errMsg, exCause);
            this.isSchemaChanged = isSchemaChanged;
        }
       
        /**
         * It returns <code>true</code> if the exception is caused by a changed in the schema
         * 
         * @return <code>true</code> if the exception is caused by a changed in the schema
         */
        public boolean isSchemaChanged() {
            return this.isSchemaChanged;
        }
    }

    /**
     * Exception thrown any time an error occurs interacting with the Resource Manager
     */
    static class RMException extends IguiException {

        private static final long serialVersionUID = 2596326890690639622L;

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         */
        RMException(final String errMsg) {
            super(errMsg);
        }

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         * @param exCause The cause of this exception
         */
        RMException(final String errMsg, final Throwable exCause) {
            super(errMsg, exCause);
        }

        /**
         * Constructor.
         * 
         * @param exCause The cause of this exception
         */
        RMException(final Throwable exCause) {
            super(exCause);
        }
    }

    /**
     * Exception thrown any time an error occurs trying to perform/remove an ERS subscription
     */
    static class ERSException extends IguiException {

        private static final long serialVersionUID = 2596326890690639622L;

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         */
        ERSException(final String errMsg) {
            super(errMsg);
        }

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         * @param exCause The cause of this exception
         */
        ERSException(final String errMsg, final Throwable exCause) {
            super(errMsg, exCause);
        }

        /**
         * Constructor.
         * 
         * @param exCause The cause of this exception
         */
        ERSException(final Throwable exCause) {
            super(exCause);
        }
    }

    /**
     * Exception thrown any time an error occurs interacting with the elog server
     */
    static class ElogException extends IguiException {

        private static final long serialVersionUID = -5197379478188429872L;

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         */
        ElogException(final String errMsg) {
            super(errMsg);
        }

        /**
         * Constructor.
         * 
         * @param errMsg Error message
         * @param cause The cause of this exception
         */
        ElogException(final String errMsg, final Throwable cause) {
            super(errMsg, cause);
        }
    }

    /**
     * Exception thrown when an instance of a panel cannot be created
     */
    static class PanelInstanceNotCreated extends IguiException {

        private static final long serialVersionUID = -4476547106236686215L;
        private final String panelClassName;

        /**
         * Constructor.
         * 
         * @param panelClassName The class name of the panel that cannot be created
         */
        PanelInstanceNotCreated(final String panelClassName, final String msg) {
            super("Failed creating instance of panel whose class is \"" + panelClassName + "\": " + msg);
            this.panelClassName = panelClassName;
        }

        /**
         * Constructor.
         * 
         * @param panelClassName The class name of the panel that cannot be created
         * @param cause The cause of this exception
         */
        PanelInstanceNotCreated(final String panelClassName, final Throwable cause) {
            super("Failed creating instance of panel whose class is \"" + panelClassName + "\" (cause was " + cause + ")", cause);
            this.panelClassName = panelClassName;
        }

        /**
         * It returns the class name of the failing panel.
         * 
         * @return The class name of the panel
         */
        public String getPanelClassName() {
            return this.panelClassName;
        }

    }

    /**
     * Exception thrown when a panel fails its initialization
     */
    static class PanelRegistrationFailed extends IguiException {

        private static final long serialVersionUID = -8411576509255591682L;
        private final String panelClassName;

        /**
         * Constructor.
         * 
         * @param panelClassName The class name of the panel that cannot be registered
         */
        PanelRegistrationFailed(final String panelClassName) {
            super("Panel of class \"" + panelClassName + "\" has not been registered because a panel of the same type is already present");
            this.panelClassName = panelClassName;
        }

        /**
         * It returns the class name of the failing panel.
         * 
         * @return The class name of the panel
         */
        public String getPanelClassName() {
            return this.panelClassName;
        }

    }

    /**
     * Exception thrown when a panel cannot be loaded dynamically
     */
    static class PanelLoadingFailed extends IguiException {

        private static final long serialVersionUID = -4865629493122262395L;
        private final String panelClassName;

        /**
         * Constructor.
         * 
         * @param panelClassName The class name of the panel that cannot be loaded
         */
        PanelLoadingFailed(final String panelClassName, final String msg) {
            super("The panel of class \"" + panelClassName + "\" cannot be loaded: " + msg);
            this.panelClassName = panelClassName;
        }

        /**
         * Constructor.
         * 
         * @param panelClassName The class name of the panel that cannot be loaded
         * @param cause The cause of this exception
         */
        PanelLoadingFailed(final String panelClassName, final Throwable cause) {
            super("Failed loading panel of class \"" + panelClassName + "\" (cause was " + cause + ")", cause);
            this.panelClassName = panelClassName;
        }

        /**
         * It returns the class name of the failing panel.
         * 
         * @return The class name of the panel
         */
        public String getPanelClassName() {
            return this.panelClassName;
        }

    }

    /**
     * Exception thrown when a panel fails its initialization
     */
    static class PanelInitializationFailed extends IguiException {

        private static final long serialVersionUID = -8218534348364845158L;
        private final String panelClassName;

        /**
         * Constructor.
         * 
         * @param panelClassName The class name of the panel that cannot be initialized
         */
        PanelInitializationFailed(final String panelClassName, final String msg) {
            super("Panel of class \"" + panelClassName + "\" cannot be initialized: " + msg);
            this.panelClassName = panelClassName;
        }

        /**
         * Constructor.
         * 
         * @param panelClassName The class name of the panel that cannot be initialized
         * @param cause The cause of this exception
         */
        PanelInitializationFailed(final String panelClassName, final Throwable cause) {
            super("Failed initializing panel of class \"" + panelClassName + "\" (cause was " + cause + ")", cause);
            this.panelClassName = panelClassName;
        }

        /**
         * It returns the class name of the failing panel.
         * 
         * @return The class name of the panel
         */
        public String getPanelClassName() {
            return this.panelClassName;
        }

    }

    /**
     * Exception thrown any time a task is sent to the EDT and some errors occurs.
     */
    static class EDTException extends IguiException {

        private static final long serialVersionUID = -1913616890658370723L;

        /**
         * Constructor.
         * 
         * @param msg Error message.
         */
        EDTException(final String msg) {
            super(msg);
        }

        /**
         * Constructor.
         * 
         * @param msg Error message
         * @param cause The cause of this exception
         */
        EDTException(final String msg, final Throwable cause) {
            super(msg, cause);
        }
    }

    /**
     * Exception thrown when no partition has been defined
     */
    static class UndefinedPartition extends IguiException {

        private static final long serialVersionUID = -593566054044741541L;

        /**
         * The error message
         */
        private final static String errMsg = "No partition defined.\nPlease start the igui defining the "
                                             + IguiConstants.PARTITION_PROPERTY + " property.";

        /**
         * Constructor.
         */
        UndefinedPartition() {
            super(UndefinedPartition.errMsg);
        }

    }

    /**
     * Constructor.
     */
    public IguiException() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param message Error message
     */
    public IguiException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message Error message
     * @param exCause The cause of this exception
     */
    public IguiException(final String message, final Throwable exCause) {
        super(message, exCause);
    }

    /**
     * Constructor.
     * 
     * @param exCause The cause of this exception
     */
    public IguiException(final Throwable exCause) {
        super(exCause);
    }

}
