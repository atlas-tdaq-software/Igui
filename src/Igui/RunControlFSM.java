package Igui;

import java.awt.Color;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.RejectedExecutionException;

import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DirectedMultigraph;

import daq.rc.ApplicationState;
import daq.rc.Command.FSMCommands;
import daq.rc.ProcessStatus;


/**
 * Class implementing the interface to the the run control FSM.
 * <p>
 * It features enumerations describing
 * <ul>
 * <li>The status in which an application may be found;
 * <li>The state in which a run control application may be;
 * <li>The transitions moving the system between two different states.
 * </ul>
 * <p>
 * It also keeps a list of panels subscribed to FSM updates and notifies them any time some change occurs.
 * 
 * @see IguiPanel#rcStateAware()
 * @see IguiPanel#rcStateChanged(RunControlFSM.State, RunControlFSM.State)
 */
public final class RunControlFSM {

    /**
     * This class object reference (singleton pattern)
     */
    private static final RunControlFSM instance = new RunControlFSM();

    /**
     * The FSM represented as a graph: states are vertices while transitions are edges
     */
    private final Graph<RunControlFSM.State, RunControlFSM.TransitionGraphEdge> fsmGraph = new DirectedMultigraph<>(RunControlFSM.TransitionGraphEdge.class);

    /**
     * Set containing references to the panel subscribed to FSM state changes
     * <p>
     * Actually the set contains weak (and not strong) references to panels. This is crucial to allow panels to be garbage collected when
     * they are removed.
     */
    private static final Set<WeakReference<IguiPanel>> subSet = new ConcurrentSkipListSet<WeakReference<IguiPanel>>(new RunControlFSM.WRComparator());

    /**
     * Comparator needed to put WeakReference object in set
     * <p>
     * It uses the WeakReference hash code.
     */
    private static class WRComparator implements java.util.Comparator<WeakReference<IguiPanel>> {
        @Override
        public int compare(final WeakReference<IguiPanel> wr1, final WeakReference<IguiPanel> wr2) {
            return Integer.valueOf(wr1.hashCode()).compareTo(Integer.valueOf(wr2.hashCode()));
        }

        @Override
        public boolean equals(final Object obj) {
            if(obj != null) {
                return obj.equals(this);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    /**
     * Enumeration listing all the statii an application may be in.
     * <p>
     * For an easy graphical representation of the statii, each of them is associated to a color.
     */
    public enum ApplicationStatus {
        NOTAV(0, ProcessStatus.NOTAV, Color.gray),
        ABSENT(1, ProcessStatus.ABSENT, Color.gray),
        REQUESTED(2, ProcessStatus.REQUESTED, Color.lightGray),
        UP(3, ProcessStatus.UP, Color.green),
        TERMINATING(4, ProcessStatus.TERMINATING, Color.lightGray),
        EXITED(5, ProcessStatus.EXITED, Color.gray),
        FAILED(6, ProcessStatus.FAILED, Color.gray);

        /**
         * Integer to implement ordinal
         */
        private final int position;

        /**
         * The process status as defined by the RC
         */
        private final daq.rc.ProcessStatus rcProcStatus;

        /**
         * Color associated to each state
         */
        private final Color color;

        /**
         * Constructor
         * 
         * @param position Ordinal
         * @param color Color
         */
        private ApplicationStatus(final int position, final daq.rc.ProcessStatus rcProcStatus, final Color color) {
            this.position = position;
            this.rcProcStatus = rcProcStatus;
            this.color = color;
        }

        /**
         * It return <code>true</code> if this status follows <code>status</code>.
         * <p>
         * For instance the UP status logically follows the ABSENT one.
         * 
         * @param status The status to compare with
         * @return <code>true</code> if this status follows <code>status</code>
         */
        public final boolean follows(final ApplicationStatus status) {
            return(this.position > status.position);
        }

        /**
         * Gets the color associated to each status.
         * 
         * @return The color associated to each status.
         */
        public final Color getColor() {
            return this.color;
        }

        @Override
        public String toString() {
            return this.rcProcStatus.toString();
        }
    }

    /**
     * Enumeration listing all the states a run control application may be in.
     * <p>
     * For FSM consistency the state order matters!
     * <p>
     * To make graphical representation easier a color is associated to each state.
     */
    public enum State {
        ABSENT(-1, ApplicationState.ABSENT, Color.gray),
        NONE(0, ApplicationState.NONE, Color.lightGray),
        INITIAL(1, ApplicationState.INITIAL, Color.cyan),
        CONFIGURED(2, ApplicationState.CONFIGURED, Color.yellow),
        CONNECTED(3, ApplicationState.CONNECTED, Color.orange),
        GTHSTOPPED(4, ApplicationState.GTHSTOPPED, Color.green),
        SFOSTOPPED(5, ApplicationState.SFOSTOPPED, Color.green),
        HLTSTOPPED(6, ApplicationState.HLTSTOPPED, Color.green),
        DCSTOPPED(7, ApplicationState.DCSTOPPED, Color.green),
        ROIBSTOPPED(8, ApplicationState.ROIBSTOPPED, Color.green),
        RUNNING(9, ApplicationState.RUNNING, Color.green);

        /**
         * Integer to implement ordinal
         */
        private final int position;

        /**
         * The FSM state as defined by the RC
         */
        private final daq.rc.ApplicationState rcApplicationState;

        /**
         * The color associated to each state
         */
        private final Color color;

        /**
         * Constructor
         * 
         * @param position Ordinal
         * @param color Color
         */
        private State(final int position, final daq.rc.ApplicationState rcState, final Color color) {
            this.position = position;
            this.rcApplicationState = rcState;
            this.color = color;
        }

        /**
         * Returns <code>true</code> if this state follows in the FSM the <code>state</code> state.
         * <p>
         * To make an example the INITIAL state follows the BOOTED one.
         * 
         * @param state The state to compare with
         * @return <code>true</code> if this state follows in the FSM the <code>state</code> state
         */
        public final boolean follows(final State state) {
            return(this.position > state.position);
        }

        /**
         * It returns the color associated to the state.
         * 
         * @return The color associated to the state
         */
        public final Color getColor() {
            return this.color;
        }

        /**
         * It returns the RC FSM state
         * 
         * @return The RC FSM state
         */
        public daq.rc.ApplicationState getRCFSMState() {
            return this.rcApplicationState;
        }

        /**
         * It returns a string representation of the RC FSM state
         */
        @Override
        public String toString() {
            return this.rcApplicationState.toString();
        }
    }

    /**
     * Enumeration listing all the transitions connecting two different states.
     * <p>
     * Some transitions are "composite" (i.e., <code>CONFIG</code>): they are not single-step transitions but include multiple transitions
     * (the <code>CONFIG</code> transition goes from <code>INITIAL</code> to <code>CONNECTED</code> but the "real" transition is a two-step
     * transition, from <code>INITIAL</code> to <code>CONFIGURED</code> and from <code>CONFIGURED</code> to <code>CONNECTED</code>);
     * <p>
     * The <code>toString()</code> method returns the name of the transition command to let the system move from a state to another one.
     */
    public enum Transition {
        /**
         * Transition from {@link RunControlFSM.State#NONE} to {@link RunControlFSM.State#INITIAL}
         */
        INITIALIZE(FSMCommands.INITIALIZE, State.NONE, State.INITIAL, false),
        /**
         * Transition from {@link RunControlFSM.State#INITIAL} to {@link RunControlFSM.State#CONFIGURED}
         */
        CONFIGURE(FSMCommands.CONFIGURE, State.INITIAL, State.CONFIGURED, false),
        /**
         * Transition from {@link RunControlFSM.State#CONFIGURED} to {@link RunControlFSM.State#CONNECTED}
         */
        CONNECT(FSMCommands.CONNECT, State.CONFIGURED, State.CONNECTED, false),
        /**
         * Transition from {@link RunControlFSM.State#CONNECTED} to {@link RunControlFSM.State#RUNNING}
         */
        START(FSMCommands.START, State.CONNECTED, State.RUNNING, false),
        /**
         * Transition from {@link RunControlFSM.State#RUNNING} to {@link RunControlFSM.State#ROIBSTOPPED}
         */
        STOPROIB(FSMCommands.STOPROIB, State.RUNNING, State.ROIBSTOPPED, false),
        /**
         * Transition from {@link RunControlFSM.State#ROIBSTOPPED} to {@link RunControlFSM.State#DCSTOPPED}
         */
        STOPDC(FSMCommands.STOPDC, State.ROIBSTOPPED, State.DCSTOPPED, false),
        /**
         * Transition from {@link RunControlFSM.State#DCSTOPPED} to {@link RunControlFSM.State#HLTSTOPPED}
         */
        STOPHLT(FSMCommands.STOPHLT, State.DCSTOPPED, State.HLTSTOPPED, false),
        /**
         * Transition from {@link RunControlFSM.State#HLTSTOPPED} to {@link RunControlFSM.State#SFOSTOPPED}
         */
        STOPRECORDING(FSMCommands.STOPRECORDING, State.HLTSTOPPED, State.SFOSTOPPED, false),
        /**
         * Transition from {@link RunControlFSM.State#SFOSTOPPED} to {@link RunControlFSM.State#GTHSTOPPED}
         */
        STOPGATHERING(FSMCommands.STOPGATHERING, State.SFOSTOPPED, State.GTHSTOPPED, false),
        /**
         * Transition from {@link RunControlFSM.State#GTHSTOPPED} to {@link RunControlFSM.State#CONNECTED}
         */
        STOPARCHIVING(FSMCommands.STOPARCHIVING, State.GTHSTOPPED, State.CONNECTED, false),
        /**
         * Transition from {@link RunControlFSM.State#CONNECTED} to {@link RunControlFSM.State#CONFIGURED}
         */
        DISCONNECT(FSMCommands.DISCONNECT, State.CONNECTED, State.CONFIGURED, false),
        /**
         * Transition from {@link RunControlFSM.State#CONFIGURED} to {@link RunControlFSM.State#INITIAL}
         */
        UNCONFIGURE(FSMCommands.UNCONFIGURE, State.CONFIGURED, State.INITIAL, false),
        // /////////////////////////
        // Composite transitions //
        // /////////////////////////
        /**
         * Composite transition from {@link RunControlFSM.State#INITIAL} to {@link RunControlFSM.State#CONNECTED}
         */
        CONFIG(FSMCommands.CONFIG, State.INITIAL, State.CONNECTED, true),
        /**
         * Composite transition from {@link RunControlFSM.State#CONNECTED} to {@link RunControlFSM.State#INITIAL}
         */
        UNCONFIG(FSMCommands.UNCONFIG, State.CONNECTED, State.INITIAL, true),
        /**
         * Composite transition from {@link RunControlFSM.State#RUNNING} to {@link RunControlFSM.State#CONNECTED}
         */
        STOP(FSMCommands.STOP, State.RUNNING, State.CONNECTED, true),
        // /////////////////////////////////////////////////
        // Shutdown always possible //
        // /////////////////////////////////////////////////
        /**
         * Transition from any state greater than {@link RunControlFSM.State#INITIAL} to {@link RunControlFSM.State#NONE}
         */
        SHUTDOWN(FSMCommands.SHUTDOWN, EnumSet.range(State.INITIAL, State.RUNNING), State.NONE, false),
        /**
         * Transition from {@link RunControlFSM.State#ABSENT} to {@link RunControlFSM.State#NONE}
         * <p>
         * The ABSENT state is not really part of the FSM but it is published in IS as default value.
         */
        ABSENTTONONE(FSMCommands.INIT_FSM, State.ABSENT, State.NONE, false),
        /**
         * Transition from {@link RunControlFSM.State#ABSENT} to {@link RunControlFSM.State#INITIAL}
         * <p>
         * The ABSENT state is not really part of the FSM but it is published in IS as default value.
         */
        ABSENTTOINITIAL(FSMCommands.INIT_FSM, State.ABSENT, State.INITIAL, false);

        /**
         * Map used to implement the FSM: at each source state corresponds a map associating a transition to each possible destination
         * state.
         */
        private static final Map<State, Map<State, Transition>> m = new EnumMap<State, Map<State, Transition>>(State.class);

        /**
         * Map associating to each state all the possible transitions.
         */
        private static final Map<State, Set<Transition>> invMap = new EnumMap<State, Set<Transition>>(State.class);

        /**
         * The FSM transition command as defined by the RC
         */
        private final daq.rc.Command.FSMCommands rcFSmCmd;

        /**
         * <code>true</code> if this enum represents a composite (i.e., multi-step) transition.
         */
        private final boolean isComposite;

        /**
         * The transition destination state
         */
        private final State dst;

        /**
         * The transition possible source states
         */
        private final List<State> srcStates = new ArrayList<State>();

        static {
            // Fill maps with all the possible states
            for(final State s : State.values()) {
                Transition.m.put(s, new EnumMap<State, Transition>(State.class));
                Transition.invMap.put(s, EnumSet.noneOf(Transition.class));
            }

            // Fill maps with allowed transitions for each state
            for(final Transition t : Transition.values()) {
                for(final State src : t.srcStates) {
                    Transition.m.get(src).put(t.dst, t);
                    Transition.invMap.get(src).add(t);
                }
            }
        }

        /**
         * Constructor.
         * 
         * @param src Transition source state
         * @param dst Transition destination state
         * @param isComposite <code>true</code> if this enum represents a composite (i.e., multi-step) transition
         */
        private Transition(final daq.rc.Command.FSMCommands rcFSMCmd, final State src, final State dst, final boolean isComposite) {
            this(rcFSMCmd, EnumSet.of(src), dst, isComposite);
        }

        /**
         * Constructor.
         * 
         * @param src Transition source state enumeration: it should contain only states strictly greater than or less than the destination
         *            state
         * @param dst Transition destination state
         * @param isComposite <code>true</code> if this enum represents a composite (i.e., multi-step) transition
         * @see #checkSourceStates(EnumSet, RunControlFSM.State)
         */
        private Transition(final daq.rc.Command.FSMCommands rcFSMCmd, final EnumSet<State> src, final State dst, final boolean isComposite)
        {
            assert (src.size() > 0) : "Transition " + this.toString() + " with no source state!";
            assert (Transition.checkSourceStates(src, dst) == true) : "Transition " + this.toString() + " with wrong source States!";

            this.rcFSmCmd = rcFSMCmd;
            this.srcStates.addAll(src);
            this.dst = dst;
            this.isComposite = isComposite;
        }

        /**
         * It returns <code>true</code> if this enum represents a composite (i.e., multi-step) transition
         * 
         * @return <code>true</code> if this enum represents a composite (i.e., multi-step) transition.
         */
        public boolean isComposite() {
            return this.isComposite;
        }

        /**
         * It return <code>true</code> if this transition goes up in the FSM.
         * 
         * @return <code>true</code> if this transition goes up in the FSM
         */
        public boolean upTransition() {
            return this.dst.follows(this.srcStates.get(0));
        }

        /**
         * @return The transition destination state
         */
        public State getDestinationState() {
            return this.dst;
        }

        /**
         * @return The name of the FSM command associated to the transtion (as defined by the RC)
         */
        @Override
        public String toString() {
            return this.rcFSmCmd.toString();
        }

        /**
         * @return It returns the FSM command associated to the Transition (as defined by the RC)
         */
        public FSMCommands getRCFSMCommand() {
            return this.rcFSmCmd;
        }

        /**
         * It returns a set of states that may be sources of this transition.
         * 
         * @return A set of states that may sources of this transition
         */
        public Set<State> getSourceStates() {
            return EnumSet.copyOf(this.srcStates);
        }

        /**
         * Check if source and destination states are compatible with a possible transitions.
         * 
         * @param src Source state enumeration
         * @param dst Destination state
         * @return <code>true</code> only if source states are strictly greater than or less than the destination state
         */
        private static boolean checkSourceStates(final EnumSet<State> src, final State dst) {
            boolean testResult = true;
            boolean previous = true;

            int count = 0;
            for(final State s : src) {
                final boolean result = s.follows(dst);
                if(count > 0) {
                    if(previous != result) {
                        testResult = false;
                        break;
                    }
                }
                previous = result;
                count++;
            }

            return testResult;
        }

        /**
         * Method called when a state transition occurs.
         * <p>
         * All the panels subscribed to FSM state updates are notified.
         * 
         * @param srcState Transition source state
         * @param dstState Transition destination state
         * @see Igui#notifyPanel(IguiPanel, State, State)
         */
        private static void doTransition(final State srcState, final State dstState) {
            final Iterator<WeakReference<IguiPanel>> it = RunControlFSM.instance().getSubscriberIterator();

            // Loop over all the subscribed panels
            while(it.hasNext()) {
                final IguiPanel p = it.next().get();
                if(p != null) {
                    // The panel has not been garbage collected yet
                    IguiLogger.debug("Panel \"" + p.getPanelName() + "\" is going to be notified about FSM state transition");
                    try {
                        Igui.notifyPanel(p, srcState, dstState);
                    }
                    catch(final RejectedExecutionException ex) {
                        // If a panel is terminated but not yet garbage collected
                        // its executor is stopped and execution tasks are rejected
                    }
                } else {
                    // The panel has been garbage collected: remove it from the map
                    it.remove();
                }
            }
        }
    }

    /**
     * Class used to represent edges in the FSM graph: and edge is identified by a transition and its source state.
     */
    private static class TransitionGraphEdge {
        private final RunControlFSM.Transition transition;
        private final RunControlFSM.State source;

        TransitionGraphEdge(final RunControlFSM.Transition tr, final RunControlFSM.State src) {
            this.transition = tr;
            this.source = src;
        }

        public RunControlFSM.Transition getTransition() {
            return this.transition;
        }

        @SuppressWarnings("unused")
        public RunControlFSM.State getSourceState() {
            return this.source;
        }

        @Override
        public int hashCode() {
            int result = 1;

            final int prime = 31;
            result = (prime * result) + ((this.source == null) ? 0 : this.source.hashCode());
            result = (prime * result) + ((this.transition == null) ? 0 : this.transition.hashCode());

            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }

            if(obj == null) {
                return false;
            }

            if(!(obj instanceof TransitionGraphEdge)) {
                return false;
            }

            final TransitionGraphEdge other = (TransitionGraphEdge) obj;
            if(this.source != other.source) {
                return false;
            }

            if(this.transition != other.transition) {
                return false;
            }

            return true;
        }
    }

    /**
     * Constructor.
     */
    private RunControlFSM() {
        // Here add vertices and edges to the graph. Goal:
        // since the graph is going to be used to find the path between two states
        // using direct transitions, no composite transitions and no SHUTDOWNs (that
        // jump from a whatever state to NONE) have to appear.
        for(final RunControlFSM.Transition tr : RunControlFSM.Transition.values()) {
            // Neglect composite transitions
            if(tr.isComposite() == false) {
                final RunControlFSM.State dst = tr.getDestinationState();
                this.fsmGraph.addVertex(dst);
                for(final RunControlFSM.State src : tr.getSourceStates()) {
                    // Neglect SHUTDOWN unless linking to adjacent states
                    if((tr.equals(Transition.SHUTDOWN) == false) || (Math.abs(src.ordinal() - dst.ordinal()) == 1)) {
                        this.fsmGraph.addVertex(src);
                        final TransitionGraphEdge edge = new TransitionGraphEdge(tr, src);
                        final boolean added = this.fsmGraph.addEdge(src, dst, edge);

                        assert (added == true) : "Failed adding the transition " + tr.toString() + " (having " + src.toString()
                                                 + " as source state) to the FSM graph";
                    }
                }
            }
        }
    }

    /**
     * It gets the RunControlFSM reference.
     * 
     * @return The RunControlFSM reference
     */
    public static RunControlFSM instance() {
        return RunControlFSM.instance;
    }

    /**
     * It gets a {@link RunControlFSM.State} from a string (FSM state updates are taken from IS, where the FSM state information is
     * represented by a string).
     * 
     * @param state The state as a string
     * @return The {@link RunControlFSM.State} corresponding to <code>state</code>
     * @throws IguiException.UnknownState Thrown if the <code>state</code> string represents an unknown FSM state
     */
    public static State getStateFromString(final String state) throws IguiException.UnknownState {
        try {
            return java.lang.Enum.valueOf(RunControlFSM.State.class, state.toUpperCase());
        }
        catch(final Exception ex) {
            throw new IguiException.UnknownState(state, ex);
        }
    }

    /**
     * It gets a {@link RunControlFSM.ApplicationStatus} from a string (status updates are taken from IS, where the status information is
     * represented by a string).
     * 
     * @param status The status as a string
     * @return The {@link RunControlFSM.ApplicationStatus} corresponding to <code>status</code>
     * @throws IguiException.UnknownStatus Thrown if the <code>status</code> string represents an unknown application status
     */
    public static ApplicationStatus getStatusFromString(final String status) throws IguiException.UnknownStatus {
        try {
            return java.lang.Enum.valueOf(RunControlFSM.ApplicationStatus.class, status.toUpperCase());
        }
        catch(final Exception ex) {
            throw new IguiException.UnknownStatus(status, ex);
        }
    }

    /**
     * It gets a set containing all {@link RunControlFSM.Transition} transitions occurred from the <code>src</code> source state to the
     * <code>dst</code> destination state.
     * <p>
     * The returned set will NOT contain any composite transition.
     * 
     * @param src The transition source state
     * @param dst The transition destination state
     * @return The resulting transitions
     * @throws IllegalArgumentException <code>src</code> and/or <code>dst</code> are <code>null</code> or are equals
     * @throws IguiException.UnknownTransition Thrown if no transition is found with the specified source and destination states
     */
    public Set<Transition> from(final State src, final State dst) throws IguiException.UnknownTransition, IllegalArgumentException {
        if((src == null) || (dst == null) || src.equals(dst)) {
            throw new IllegalArgumentException("Source and destination states must be different and not null");
        }

        // This is the set containing all the needed transitions
        final Set<Transition> trSet = new LinkedHashSet<Transition>();

        final List<RunControlFSM.TransitionGraphEdge> path = DijkstraShortestPath.findPathBetween(this.fsmGraph, src, dst);
        if(path != null) {
            for(final RunControlFSM.TransitionGraphEdge edge : path) {
                trSet.add(edge.getTransition());
            }
        } else {
            throw new IguiException.UnknownTransition(src.toString(), dst.toString());
        }

        return trSet;
    }

    /**
     * It gets all the transitions available starting from the specified state.
     * 
     * @param state The {@link RunControlFSM.State} state
     * @return The list of possible transitions
     */
    public Set<Transition> getTransitions(final State state) {
        return EnumSet.copyOf(Transition.invMap.get(state));
    }

    /**
     * It adds the panel to the list of panels subscribed to FSM state updates.
     * <p>
     * When panels are garbage collected they are automatically removed from the list.
     * 
     * @param panel The panel to be added to the list of subscribed panels
     */
    void addSubscriberPanel(final IguiPanel panel) {
        RunControlFSM.subSet.add(new WeakReference<IguiPanel>(panel));
    }

    /**
     * Called any time a FSM transition occurs.
     * 
     * @param src The transition source state
     * @param dst The transition destination state
     * @see Igui#rcStateChanged(is.InfoEvent rcInfo)
     * @see Transition#doTransition(RunControlFSM.State, RunControlFSM.State)
     */
    void executeTransition(final State src, final State dst) {
        Transition.doTransition(src, dst);
    }

    /**
     * It gets the iterator to browse the list of subscribed panels.
     * 
     * @return The iterator to browse the list of subscribed panels
     */
    private Iterator<WeakReference<IguiPanel>> getSubscriberIterator() {
        return RunControlFSM.subSet.iterator();
    }

    // TODO: when the FSM is modified, execute the following test to be sure that the algorithm to move from one
    // state to another one works properly.

    // public static void main(final String[] args) {
    // final State[] all = State.values();
    // for(final State src : all) {
    // for(final State dst : all) {
    // try {
    // final long start = System.nanoTime();
    // final Set<Transition> path = RunControlFSM.instance().from(src, dst);
    // final long stop = System.nanoTime();
    //
    // System.err.println(src + " -> " + dst + ": " + path.toString() + " [" + (stop - start) + " ns]");
    // }
    // catch(final Exception ex) {
    // System.err.println(src + " -> " + dst + ": @@@INVALID!@@@");
    // }
    // }
    // }
    // }
}
