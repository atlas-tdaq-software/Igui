package Igui;

/**
 * Generic interface implemented by all the IguiPanel panels.
 */
interface IguiInterface {

    /**
     * @see IguiPanel#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     */
    public void accessControlChanged(final IguiConstants.AccessControlStatus newAccessStatus);

    /**
     * @see IguiPanel#dbReloaded()
     */
    public void dbReloaded();

    /**
     * @see IguiPanel#dbDiscarded()
     */
    public void dbDiscarded();

    /**
     * @see IguiPanel#getPanelName()
     */
    public String getPanelName();

    /**
     * @see IguiPanel#getTabName() \
     */
    public String getTabName();

    /**
     * @see IguiPanel#panelSelected()
     */
    public void panelSelected();

    /**
     * @see IguiPanel#panelDeselected()
     */
    public void panelDeselected();

    /**
     * @see IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    public void panelInit(final RunControlFSM.State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException;

    /**
     * @see IguiPanel#panelTerminated()
     */
    public void panelTerminated();

    /**
     * @see IguiPanel#rcStateAware()
     */
    public boolean rcStateAware();

    /**
     * @see IguiPanel#refreshISSubscription()
     */
    public void refreshISSubscription();

    /**
     * @see IguiPanel#elogString()
     */
    public String elogString();

}

/**
 * Interface to the run control FSM for IguiPanel panels.
 */
interface RCTransitionActions {

    /**
     * @see IguiPanel#rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)
     */
    public void rcStateChanged(final RunControlFSM.State oldState, final RunControlFSM.State newState);

    /**
     * @see IguiPanel#rcTransitionCommandSending(Igui.RunControlFSM.Transition)
     */
    @Deprecated
    public void rcTransitionCommandSending(final RunControlFSM.Transition transition);

}
