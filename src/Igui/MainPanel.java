package Igui;

import is.AnyInfo;
import is.InfoEvent;
import is.InfoNotCompatibleException;
import is.RepositoryNotFoundException;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.jidesoft.swing.ButtonStyle;
import com.jidesoft.swing.DefaultOverlayable;
import com.jidesoft.swing.JideSplitButton;
import com.jidesoft.swing.OverlayableUtils;

import rc.TriggerState;
import Igui.IguiConstants.MessageSeverity;
import Igui.RunControlApplicationData.ApplicationISStatus;
import TRIGGER.TriggerCommander;
import TTCInfo.GLOBALBUSY;
import config.ConfigException;
import dal.MasterTrigger;
import dal.RunControlApplicationBase;
import daq.rc.RCException;
import ddc.DdcIntInfo;


/**
 * This class implements the Igui main panel (i.e., the left panel containing all the buttons to send commands to the Root Controller and
 * other information like event and rate counters). All the commands to the Root Controller and the Master Trigger are sent in a separate
 * thread.
 * <p>
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI Builder, which is free for non-commercial use. If Jigloo is
 * being used commercially (ie, by a corporation, company or business for any purpose whatever) then you should purchase a license for each
 * developer using Jigloo. Please visit www.cloudgarden.com for details. Use of Jigloo implies acceptance of these licensing terms. A
 * COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR ANY CORPORATE OR COMMERCIAL
 * PURPOSE.
 * <p>
 * <a href="https://www.flaticon.com/free-icons/autopilot" title="autopilot icons">Autopilot icons created by Smashicons - Flaticon</a>
 * <a href="https://www.flaticon.com/free-icons/on" title="on icons">On icons created by Pixel perfect - Flaticon</a>
 * <a href="https://www.flaticon.com/free-icons/off" title="off icons">Off icons created by Pixel perfect - Flaticon</a>
 */
class MainPanel extends IguiPanel {

    private static final long serialVersionUID = 3955504276109308868L;
    
    // Simple utility class
    private static class StampedObject<T> {
        private final T object;
        private final long stamp;
        
        public StampedObject(final T object, final long stamp) {
            this.object = object;
            this.stamp = stamp;
        }
        
        public T getObject() {
            return this.object;
        }
        
        public long getStamp() {
            return this.stamp;
        }
    }
    
    // Why do we keep the following three volatile references? To update the hold/resume trigger buttons.
    // Those buttons state depends upon both the RC, Trigger and GlobalBusy states. This panel is notified
    // about the first one in the updateDisplay(rc.RCStateInfo) method, while the second and third ones are
    // obtained from an IS call-back (see TriggerStateReceiver and GlobalBusyReceiver). The mentioned methods update the
    // rootControllerState, triggerState and gBusyState references (they need to be volatile because
    // changed in different threads) and call updateDisplay(), which submits a task to the EDT.
    // Once in the EDT the up-to-date values of rootControllerState, triggerState and gBusyState are
    // used. The use of the EDT solves any issue with multiple threads. In this way we are sure
    // that the updateDisplay() is called for every update of the RC state or of the trigger state.
    /**
     * The Root Controller state
     */
    private volatile RunControlApplicationData rootControllerState = Igui.instance().getRCInfo();

    /**
     * The trigger state
     */    
    private final AtomicReference<StampedObject<rc.TriggerState>> triggerState = new AtomicReference<>(new StampedObject<>(new rc.TriggerState(), 0L));

    /**
     * The global busy state
     */
    // Variable initialized in the constructor
    private final AtomicReference<StampedObject<GLOBALBUSY>> gBusyState = new AtomicReference<>();

    /**
     * Status of the auto-pilot
     */
    private final AtomicBoolean autoPilotEnabled = new AtomicBoolean(false);
    
    /**
     * The beam state
     */
    private final AtomicBoolean beamState = new AtomicBoolean(false);

    /**
     * This panel name
     */
    private final static String panelName = "MainCommands";

    /**
     * Map containing buttons associated to RC/MT commands/transitions
     * <p>
     * To each transition is associated a button.
     */
    private final Map<String, JButton> bMap = new HashMap<String, JButton>();

    /**
     * Executor where RC commands are executed
     */
    private final ThreadPoolExecutor commandExecutor = ExecutorFactory.createSingleExecutor(MainPanel.panelName + "-RCCommands");

    /**
     * Label with the Root Controller current state
     */
    private final JLabel currentStateLabel = new JLabel();
    
    /**
     * Icon showing an error state 
     */
    private final static ImageIcon errorIcon = Igui.createIcon("error.png");

    /**
     * List containing all the IS receivers used for counters
     * <p>
     * There must not be concurrent writing/reading to this list. Visibility in case of changes is granted using the
     * {@link #counterListWhatcher} volatile.
     */
    private final List<CountersAndRatesReceiver> counterReceiversList = new ArrayList<CountersAndRatesReceiver>();

    /**
     * Volatile variable used to make changes to the {@link #counterReceiversList} visible amongst threads. Write to this volatile after the
     * list has been modified and read this volatile before any reading operation on the list.
     */
    private volatile boolean counterListWhatcher = true;

    /**
     * List containing all the IS receivers used to get info about the run (but the counters, of course)
     * <p>
     * There must not be concurrent writing/reading to this list. Visibility in case of changes is granted using the
     * {@link #runInfoListWhatcher} volatile.
     */
    private final List<ISRunInfoReceiver<?>> runInfoReceiversList = new ArrayList<ISRunInfoReceiver<?>>();

    /**
     * Volatile variable used to make changes to the {@link #runInfoReceiversList} visible amongst threads. Write to this volatile after the
     * list has been modified and read this volatile before any reading operation on the list.
     */
    private volatile boolean runInfoListWhatcher = true;

    /**
     * Possible values for the beam type
     */
    private final String[] BEAM_TYPES = {"No Beam", "Protons", "Ions"};

    /**
     * Default possible values for the run type (used if not defined in the DB)
     */
    private final String[] DEFAULT_RUNTYPES = {"Physics", "Calibration", "Cosmics"};

    /**
     * The RC state after which the IS subscriptions for counters and rates are performed
     */
    private final RunControlFSM.State stateToSubscribe = RunControlFSM.State.NONE;

    /**
     * The RC transition at which the IS subscriptions for counters and rates are performed
     */
    private final RunControlFSM.Transition transitionToSubscribe = RunControlFSM.Transition.INITIALIZE;

    /**
     * Boolean to show or not the warning at CONNECT
     */
    private final boolean showWarningAtConnect;

    /**
     * Swing timer used to let the RESUME TRIGGER button foreground color to blink when active.
     * <p>
     * The timer is started/stopped in {@link #updateDisplay()} and the AcctionListener is added in {@link #initGUI()}.
     */
    private final Timer edtTimer = new Timer(800, null);
    
    /**
     * The tab pane at the bottom of the panel
     */
    // IMPORTANT: the sub panel order matters for automatic selection and/or field updating:
    // 0 - Information panel
    // 1 - Counters panel
    // 2 - Run setting panel
    // see rcStateChanged (at CONNECT the panel no 2 is automatically selected) and initGUI (a ChangeListener is installed)
    private final JTabbedPane infoPane = new JTabbedPane();

    // Counter tab components
    private final JTextField lumiBlockValue = new JTextField();
    private final JTextField recRate = new JTextField();
    private final JTextField hltRate = new JTextField();
    private final JTextField l1Rate = new JTextField();
    private final JTextField recCounter = new JTextField();
    private final JTextField hltCounter = new JTextField();
    private final JTextField l1Counter = new JTextField();

    // Information tab components
    private final JTextField totalTimeValue = new JTextField();
    private final JTextField stopTimeValue = new JTextField();
    private final JTextField startTimeValue = new JTextField();
    private final JTextField recordingValue = new JTextField();
    private final JTextField runNumberValue = new JTextField();
    private final JTextField runTypeValue = new JTextField();
    private final JTextField superMasterKeyValue = new JTextField();
    private final JTextField clockType = new JTextField(); // LHC clock type

    // Settings tab components
    private final JRadioButton recDisableRadioButton = new JRadioButton();
    private final JRadioButton recEnableRadioButton = new JRadioButton();
    private final JComboBox<String> t0ComboBox = new JComboBox<String>(); // T0 project
    private final JComboBox<String> btComboBox = new JComboBox<String>(this.BEAM_TYPES); // Beam type
    private final JComboBox<String> rtComboBox = new JComboBox<String>(); // Run type
    private final JTextField fnTextField = new JTextField(); // File name
    private final JTextField beTextField = new JTextField(); // Beam energy
    private final JTextField maxEvTextField = new JTextField(); // Max events
    private final JButton okButton = new JButton(); // Set value button
    private final RunSettingsItemListener rsListener = new RunSettingsItemListener();

    // Auto-pilot, beam status and R4P labels
    private final static ImageIcon unknownIcon = Igui.createIcon("beam_unknown.png");
    private final static ImageIcon unstableIcon = Igui.createIcon("beam_unstable.png");
    private final static ImageIcon stableIcon = Igui.createIcon("beam_stable.png");
    private final JLabel beamStatusLabel = new JLabel(MainPanel.unknownIcon);
    private final JLabel r4pStatusLabel = new JLabel(MainPanel.unknownIcon);
    private final JLabel autopilotStatusLabel = new JLabel(MainPanel.unknownIcon);
    private final JRadioButtonMenuItem autopilotOnButton = new JRadioButtonMenuItem("On");
    private final JRadioButtonMenuItem autopilotOffButton = new JRadioButtonMenuItem("Off");
    private final AutoPilotSetter autoPilotSetter = new AutoPilotSetter();
    
    // Button for the new RC command panel   
    private final RCTransitionCommandButton goTowardsRunningButton = new RCTransitionCommandButton(RunControlFSM.Transition.INITIALIZE, 
                                                                                                   Igui.createIcon("go-next.png"));
    private final RCTransitionCommandButton goTowardsNoneButton = new RCTransitionCommandButton(RunControlFSM.Transition.SHUTDOWN, 
                                                                                                Igui.createIcon("go-previous.png"));    
    private final RCTransitionCommandButton newShutdownButton = new RCTransitionCommandButton(RunControlFSM.Transition.SHUTDOWN, 
                                                                                              Igui.createIcon("go-first-view.png"));
    private final JLabel currentRCStateLabel = new JLabel();
    private final JProgressBar stateProgressBar = new JProgressBar(0, RunControlFSM.State.RUNNING.ordinal());

    private final JLabel holdTriggerCounter = new JLabel(MainPanel.unstableIcon);
    private final JLabel autoPilotEnabledTag = new JLabel(Igui.createIcon("switch-on.png"));
    private final JLabel autoPilotDisabledTag = new JLabel(Igui.createIcon("switch-off.png"));

    private final JButton autoPilotButton = new JButton(Igui.createIcon("autopilot.png")) {
        private static final long serialVersionUID = 1L;

        // This is to enable the overlay icon
        @Override
        public void repaint(final long tm, final int x, final int y, final int width, final int height) {
            super.repaint(tm, x, y, width, height);
            OverlayableUtils.repaintOverlayable(this);
        }
        
        @Override
        public void setEnabled(final boolean enable) {
            super.setEnabled(enable);
            MainPanel.this.autoPilotEnabledTag.setEnabled(enable);
            MainPanel.this.autoPilotDisabledTag.setEnabled(enable);        
        }
    };
    
    private final MTCommandButton newPauseTriggerButton = new MTCommandButton(Igui.createIcon("media-playback-pause.png")) {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(final ActionEvent e) {
            MainPanel.this.bMap.get(MTCommands.HOLDTRIGGER.toString()).doClick();
        }       
     };

    private final MTCommandButton newResumeTriggerButton = new MTCommandButton(Igui.createIcon("media-playback-start.png")) {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(final ActionEvent e) {
            MainPanel.this.bMap.get(MTCommands.RESUMETRIGGER.toString()).doClick();
        }
        
        @Override
        public void setEnabled(final boolean enable) {
            super.setEnabled(enable);
            MainPanel.this.holdTriggerCounter.setEnabled(enable);
            
            if(enable == true) {                    
                MainPanel.this.edtTimer.start();
            } else {         
                MainPanel.this.edtTimer.stop();
                MainPanel.this.setHoldCounterVisibility(MainPanel.this.triggerState.get().getObject(),
                                                        MainPanel.this.gBusyState.get().getObject(),
                                                        MainPanel.this.rootControllerState.getISStatus());
            }
        }
        
        // This is to enable the overlay icon
        @Override
        public void repaint(final long tm, final int x, final int y, final int width, final int height) {
            super.repaint(tm, x, y, width, height);
            OverlayableUtils.repaintOverlayable(this);
        }
    };
    
    /**
     * Pattern to validate the file name tag: it should not contain any POSIX punctuation char or empty spaces. Documentation says that the
     * {@link Pattern} object is immutable and thread safe.
     */
    private final static Pattern fnRegEx = Pattern.compile(".*[\\p{Punct}\\p{Blank}].*");

    /**
     * Array containing allowed characters for the 'file name tag' but that would match the {@link #fnRegEx} regular expression.
     */
    private final char[] fnGoodChars = new char[] {'-'};

    /**
     * Max allowed length of the file name tag
     */
    private final static int maxFnLen = 40;

    /**
     * Commander used to send the hold and resume trigger commands to the master trigger
     */
    private volatile TriggerCommander trgCommander;

    /**
     * Commands sent to the Master Trigger
     */
    private enum MTCommands {
        HOLDTRIGGER,
        RESUMETRIGGER;
    }

    /**
     * Title for the "trigger on hold" dialog.
     */
    private final static String triggerHeldDialogTitle = "IGUI - Stop of Run Information";

    /**
     * Enumeration for the counter and rates information.
     * <p>
     * Each enumeration stores the rate and counter IS server name, information name and the attribute name. The default values for all of
     * them are defined in {@link IguiConstants}. Values can be modified using the {@link #setCounterInfoSource(String, String, String)} and
     * {@link #setRateInfoSource(String, String, String)} methods.
     */
    private enum InfoTypes {
        LVL1(),
        HLT(),
        RECORDING();

        private final StringBuilder counterInfoSource = new StringBuilder();
        private final StringBuilder rateInfoSource = new StringBuilder();

        private String counterServerName;
        private String counterInfoName;
        private String counterAttributeName;
        private String rateServerName;
        private String rateInfoName;
        private String rateAttributeName;

        private InfoTypes() {
            synchronized(this.counterInfoSource) {
                this.counterServerName = IguiConstants.DEFAULT_EVENT_COUNTER_INFO_SERVER;
                this.counterInfoName = IguiConstants.DEFAULT_EVENT_COUNTER_INFO_NAME;
                this.counterAttributeName = IguiConstants.DEFAULT_EVENT_COUNTER_INFO_ATTRIBUTE;
                this.counterInfoSource.append(this.counterServerName + "." + this.counterInfoName + "." + this.counterAttributeName);
            }

            synchronized(this.rateInfoSource) {
                this.rateServerName = IguiConstants.DEFAULT_EVENT_RATE_INFO_SERVER;
                this.rateInfoName = IguiConstants.DEFAULT_EVENT_RATE_INFO_NAME;
                this.rateAttributeName = IguiConstants.DEFAULT_EVENT_RATE_INFO_ATTRIBUTE;
                this.rateInfoSource.append(this.rateServerName + "." + this.rateInfoName + "." + this.rateAttributeName);
            }
        }

        // 0 - Server name
        // 1 - Info name
        // 2 - Attribute name
        public String[] getCounterInfoParams() {
            synchronized(this.counterInfoSource) {
                return new String[] {this.counterServerName, this.counterInfoName, this.counterAttributeName};
            }
        }

        public String getCounterInfoSource() {
            synchronized(this.counterInfoSource) {
                return this.counterInfoSource.toString();
            }
        }

        // 0 - Server name
        // 1 - Info name
        // 2 - Attribute name
        public String[] getRateInfoParams() {
            synchronized(this.rateInfoSource) {
                return new String[] {this.rateServerName, this.rateInfoName, this.rateAttributeName};
            }
        }

        public String getRateInfoSource() {
            synchronized(this.rateInfoSource) {
                return this.rateInfoSource.toString();
            }
        }

        void setCounterInfoSource(final String serverName, final String infoName, final String attributeName) {
            synchronized(this.counterInfoSource) {
                this.counterServerName = serverName;
                this.counterInfoName = infoName;
                this.counterAttributeName = attributeName;
                this.counterInfoSource.replace(0, this.counterInfoSource.length(), serverName + "." + infoName + "." + attributeName);
            }
        }

        void setRateInfoSource(final String serverName, final String infoName, final String attributeName) {
            synchronized(this.rateInfoSource) {
                this.rateServerName = serverName;
                this.rateInfoName = infoName;
                this.rateAttributeName = attributeName;
                this.rateInfoSource.replace(0, this.rateInfoSource.length(), serverName + "." + infoName + "." + attributeName);
            }
        }
    }

    /**
     * Abstract class extending {@link ISInfoReceiver} to implement IS receivers used to update counter and rate values.
     * <p>
     * Only {@link ISInfoReceiver#infoCreated(InfoEvent)} and {@link ISInfoReceiver#infoUpdated(InfoEvent)} methods are overridden (it means
     * that the {@link ISInfoReceiver#infoDeleted(InfoEvent)} call-back is not processes). Both will call
     * {@link #processInfoUpdate(AnyInfo, int)}, that is the only method any subclass should override to process call-backs.
     * <p>
     * This class offers and additional {@link #wrongInfo(boolean)} method which is called any time a problem occurs (when the error is
     * recovered it is called with a <code>false</code> argument).
     */
    private abstract class CountersAndRatesReceiver extends ISInfoReceiver {

        /**
         * The name of the IS server
         */
        private final String serverName;

        /**
         * The name of the IS information
         */
        private final String infoName;

        /**
         * The name of the attribute holding the needed information
         */
        private final String attributeName;

        /**
         * The index of {@link #attributeName}
         */
        private final AtomicInteger attributeIndex = new AtomicInteger(-1);

        /**
         * Constructor.
         * 
         * @param infoParams Array containing (in order): the name of the IS server, the name of the IS information and the name of the IS
         *            information attribute
         */
        CountersAndRatesReceiver(final String[] infoParams) {
            super(infoParams[0] + "." + infoParams[1]);

            this.serverName = infoParams[0];
            this.infoName = infoParams[1];
            this.attributeName = infoParams[2];
        }

        /**
         * In addition to perform the IS subscription (calling <code>super.subscribe()</code> it correctly calls {@link #wrongInfo(boolean)}
         * .
         * 
         * @see Igui.ISInfoReceiver#subscribe()
         */
        @Override
        public void subscribe() throws IguiException.ISException {
            try {
                super.subscribe();
                this.wrongInfo(false);
            }
            catch(final IguiException.ISException ex) {
                this.wrongInfo(true);
                throw ex;
            }
        }

        /**
         * This method is called any time the IS information we are interested in is updated. The arguments are the {@link is.AnyInfo}
         * information object and the index of the interesting attribute (with rate and counters only one information attribute is
         * important, i.e., the rate or the counter value).
         * 
         * @param info Updated information
         * @param infoIndex The index of the information attribute we are interested in
         */
        protected abstract void processInfoUpdate(final is.AnyInfo info, final int infoIndex);

        /**
         * It is called with <code>true</code> as argument if something goes wrong (i.e., the IS subscription fails or the attribute is not
         * found). When the error situation is recovered this method is called again with <code>false</code> as argument.
         * <p>
         * An use case may be the update of a GUI element (e.g., a label) showing the value of the IS information: if something fails this
         * method could change something on the GUI to tell the user about the error.
         * 
         * @param isWrong
         */
        protected void wrongInfo(final boolean isWrong) {
        }

        /**
         * It extracts the attribute index from <code>info</code> and the calls {@link #processInfoUpdate(AnyInfo, int)}.
         * 
         * @see Igui.ISInfoReceiver#infoCreated(is.InfoEvent)
         * @see #getAttributeIndex(AnyInfo)
         * @param info The created IS information
         */
        @Override
        protected final void infoCreated(final InfoEvent info) {
            final is.AnyInfo isInfo = this.getInfo(info);
            final int index = this.getAttributeIndex(isInfo);
            if(index >= 0) {
                this.processInfoUpdate(isInfo, index);
            }
        }

        /**
         * It extracts the attribute index from <code>info</code> and the calls {@link #processInfoUpdate(AnyInfo, int)}.
         * 
         * @see Igui.ISInfoReceiver#infoUpdated(is.InfoEvent)
         * @see #getAttributeIndex(AnyInfo)
         * @param info The updated IS information
         */
        @Override
        protected final void infoUpdated(final InfoEvent info) {
            final is.AnyInfo isInfo = this.getInfo(info);
            final int index = this.getAttributeIndex(isInfo);
            if(index >= 0) {
                this.processInfoUpdate(isInfo, index);
            }
        }

        /**
         * It gets the {@link is.AnyInfo} object from <code>info</code>.
         * 
         * @param info The IS information subject of the call-back
         * @return The {@link is.AnyInfo} object containing the IS information
         */
        private is.AnyInfo getInfo(final InfoEvent info) {
            final is.AnyInfo anyInfo = new is.AnyInfo();
            info.getValue(anyInfo);
            return anyInfo;
        }

        /**
         * It finds the index of attribute {@link #attributeName} for the IS information {@link #infoName}.
         * 
         * @param isInfo The IS information object causing the call-back event
         * @return The (positive) index of the {@link #attributeName} attribute or a negative value if the attribute index could not be
         *         found.
         */
        private final int getAttributeIndex(final is.AnyInfo isInfo) {
            int valueToReturn = -2;

            // Check if the attribute index has already been calculated, do not do
            // it every time a call-back is received
            if(this.attributeIndex.get() == -1) {
                // -1 means that we are here for the first time
                // or all the other times the index cannot be found
                // because of an error communicating with the IS server (CORBA timeout)
                try {
                    boolean found = false;

                    // Loop over the is.InfoDocument to find the attribute index
                    final is.InfoDocument isInfoDoc = new is.InfoDocument(Igui.instance().getPartition(), isInfo);
                    final int attrCount = isInfoDoc.getAttributeCount();
                    for(int j = 0; j < attrCount; ++j) {
                        final is.InfoDocument.Attribute attr = isInfoDoc.getAttribute(j);
                        if(attr.getName().equals(this.attributeName)) {
                            valueToReturn = j;
                            found = true;
                            break;
                        }
                    }

                    if(found == false) {
                        final String msg = "The info \"" + this.serverName + "." + this.infoName + "." + this.attributeName
                                           + "\" cannot be found in the IS repository; the associated counter will not be updated";
                        IguiLogger.error(msg);
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    }
                }
                catch(final Exception ex) {
                    final String msg = "An error occurred while getting the IS info \"" + this.serverName + "." + this.infoName + "."
                                       + this.attributeName + "\": " + ex + ". The associated counter will not be correctly updated";
                    IguiLogger.error(msg, ex);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);

                    final Throwable exCause = ex.getCause();
                    if(org.omg.CORBA.TIMEOUT.class.isInstance(exCause)) {
                        valueToReturn = -1;
                    }
                }

                // At this point the value can be:
                // > 0 - The index has been found
                // -1 - CORBA timeout talking to the server (we should try again)
                // -2 - The information is not found (i.e., we are looking for something which does not exist, do not try again)
                this.attributeIndex.set(valueToReturn);
            } else {
                // Here the value can be:
                // > 0 - The index has been found
                // -2 - Attribute not found
                valueToReturn = this.attributeIndex.get();
            }

            // Call wrong info according to the result
            if(valueToReturn < 0) {
                this.wrongInfo(true);
            } else {
                this.wrongInfo(false);
            }

            return valueToReturn;
        }
    }

    /**
     * Class extending {@link CountersAndRatesReceiver} to implement an IS receiver used to update the counter values.
     */
    private class CounterReceiver extends CountersAndRatesReceiver {
        private final JTextField textField;
        private final InfoTypes infoType;

        /**
         * Constructor.
         * 
         * @param infoType The {@link InfoTypes} for the counter
         * @param textField The text field used to display the counter
         */
        CounterReceiver(final InfoTypes infoType, final JTextField textField) {
            super(infoType.getCounterInfoParams());

            this.textField = textField;
            this.infoType = infoType;

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    CounterReceiver.this.textField.setToolTipText(infoType.getCounterInfoSource());
                }
            });
        }

        /**
         * @see Igui.MainPanel.CountersAndRatesReceiver#subscribe()
         */
        @Override
        public void subscribe() throws IguiException.ISException {
            try {
                super.subscribe();
            }
            catch(final IguiException.ISException ex) {
                final Throwable cause = ex.getCause();
                final String msg = ex.getMessage() + ". The " + this.infoType + " event counter will not be correctly updated";
                throw new IguiException.ISException(msg, cause);
            }
        }

        /**
         * It fills the text area with the value coming from the IS call-back.
         * 
         * @see Igui.MainPanel.CountersAndRatesReceiver#processInfoUpdate(is.AnyInfo, int)
         */
        @Override
        protected void processInfoUpdate(final AnyInfo info, final int infoIndex) {
            try {
                final Object value = info.getAttribute(infoIndex);
                final byte valueType = info.getAttributeType(infoIndex);

                final String valueString;
                switch(valueType) {
                    case is.Type.U8:
                        valueString = Integer.toString(Byte.toUnsignedInt(Byte.parseByte(value.toString())));
                        break;
                    case is.Type.U16:
                        valueString = Integer.toString(Short.toUnsignedInt(Short.parseShort(value.toString())));
                        break;
                    case is.Type.U32:
                        valueString = Long.toString(Integer.toUnsignedLong(Integer.parseInt(value.toString())));
                        break;
                    default:
                        valueString = value.toString();
                }

                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        CounterReceiver.this.textField.setText(valueString);
                    }
                });
            }
            catch(final Exception ex) {
                final String errMsg = "Failed processing a counter update for info \"" + info.toString() + "\": " + ex;
                IguiLogger.error(errMsg, ex);

                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        CounterReceiver.this.textField.setText("NaN");
                    }
                });
            }
        }

        /**
         * If <code>wrong</code> is <code>true</code> then the text area background color is painted in red.
         * 
         * @see Igui.MainPanel.CountersAndRatesReceiver#wrongInfo(boolean)
         */
        @Override
        protected void wrongInfo(final boolean wrong) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if(wrong == true) {
                        CounterReceiver.this.textField.setBackground(Color.red);
                    } else {
                        CounterReceiver.this.textField.setBackground(null);
                    }
                }
            });
        }
    }

    /**
     * Class extending {@link CountersAndRatesReceiver} to implement an IS receiver used to update the rate values.
     */
    private class RateReceiver extends CountersAndRatesReceiver {
        private final JTextField textField;
        private final InfoTypes infoType;
        private final NumberFormat nf = NumberFormat.getInstance();

        /**
         * Constructor.
         * 
         * @param infoType The {@link InfoTypes} for the counter
         * @param textField The text field used to display the counter
         */
        RateReceiver(final InfoTypes infoType, final JTextField textField) {
            super(infoType.getRateInfoParams());
            this.textField = textField;
            this.infoType = infoType;

            this.nf.setMaximumFractionDigits(2);
            this.nf.setMinimumFractionDigits(2);

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    RateReceiver.this.textField.setToolTipText(infoType.getRateInfoSource());
                }
            });
        }

        /**
         * @see Igui.MainPanel.CountersAndRatesReceiver#subscribe()
         */
        @Override
        public void subscribe() throws IguiException.ISException {
            try {
                super.subscribe();
            }
            catch(final IguiException.ISException ex) {
                final Throwable cause = ex.getCause();
                final String msg = ex.getMessage() + ". The " + this.infoType + " rate counter will not be correctly updated";
                throw new IguiException.ISException(msg, cause);
            }
        }

        /**
         * It fills the text area with the value coming from the IS call-back.
         * 
         * @see Igui.MainPanel.CountersAndRatesReceiver#processInfoUpdate(is.AnyInfo, int)
         */
        @Override
        protected void processInfoUpdate(final AnyInfo info, final int infoIndex) {
            final StringBuffer value = new StringBuffer();

            try {
                final Object infoValue = info.getAttribute(infoIndex);
                final byte infoValueType = info.getAttributeType(infoIndex);

                final String infoValueString;
                switch(infoValueType) {
                    case is.Type.U8:
                        infoValueString = Integer.toString(Byte.toUnsignedInt(Byte.parseByte(infoValue.toString())));
                        break;
                    case is.Type.U16:
                        infoValueString = Integer.toString(Short.toUnsignedInt(Short.parseShort(infoValue.toString())));                      
                        break;
                    case is.Type.U32:                        
                        infoValueString = Long.toString(Integer.toUnsignedLong(Integer.parseInt(infoValue.toString())));
                        break;
                    default:
                        infoValueString = infoValue.toString();
                }

                final String unit;
                double doubleValue = Double.parseDouble(infoValueString);
                if(doubleValue < 1) {
                    unit = "mHz";
                    doubleValue *= 1000.;
                } else if(doubleValue > 1000) {
                    unit = "kHz";
                    doubleValue /= 1000.;
                } else {
                    unit = "Hz";
                }

                this.nf.format(doubleValue, value, new FieldPosition(0));
                value.append(" " + unit);
            }
            catch(final Exception ex) {
                final String errMsg = "Error processing rate callback for info \"" + info.toString() + "\": " + ex;
                IguiLogger.error(errMsg, ex);
                value.append("NaN");
            }

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    RateReceiver.this.textField.setText(value.toString());
                }
            });
        }

        /**
         * If <code>wrong</code> is <code>true</code> then the text area background color is painted in red.
         * 
         * @see Igui.MainPanel.CountersAndRatesReceiver#wrongInfo(boolean)
         */
        @Override
        protected void wrongInfo(final boolean wrong) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if(wrong == true) {
                        RateReceiver.this.textField.setBackground(Color.red);
                    } else {
                        RateReceiver.this.textField.setBackground(null);
                    }
                }
            });
        }
    }

    /**
     * Abstract class extending {@link ISInfoReceiver} to retrieve IS information and keep being notified about the status of the run.
     * <p>
     * Subclasses that wants to be notified about changes in IS should override {@link #processInfoUpdate(AnyInfo, int)} and make a
     * subscription using {@link #subscribe()}. Subscribed clients will be notified when the IS information is created and/or updated but
     * not when it is removed.
     */
    private abstract class ISRunInfoReceiver<T extends is.Info> extends ISInfoReceiver {
        private final Class<T> infoType;
        private final String infoName;

        /**
         * Constructor.
         * 
         * @param infoName The full name of the IS information
         * @param infoType The type of the IS information
         */
        ISRunInfoReceiver(final String infoName, final Class<T> infoType) {
            super(infoName);
            this.infoName = infoName;
            this.infoType = infoType;
        }

        /**
         * Constructor.
         * <p>
         * Use this constructor only if the IS server is running in the context of a different partition then the one the Igui is running in
         * (i.e., the beam status information).
         * 
         * @param partName The partition name
         * @param infoName The full name of the IS information
         * @param infoType The type of the IS information
         */
        ISRunInfoReceiver(final String partName, final String infoName, final Class<T> infoType) {
            super(new ipc.Partition(partName), infoName);
            this.infoName = infoName;
            this.infoType = infoType;
        }

        /**
         * This method is called any time the IS information we are interested in is updated.
         * 
         * @param updatedInfo The updated IS information
         */
        protected abstract void processUpdate(final T updatedInfo);

        /**
         * @see Igui.ISInfoReceiver#infoCreated(is.InfoEvent)
         * @see #processCallback(InfoEvent)
         */
        @Override
        protected final void infoCreated(final InfoEvent info) {
            this.processCallback(info);
        }

        /**
         * @see Igui.ISInfoReceiver#infoUpdated(is.InfoEvent)
         * @see #processCallback(InfoEvent)
         */
        @Override
        protected final void infoUpdated(final InfoEvent info) {
            this.processCallback(info);
        }

        /**
         * @see Igui.ISInfoReceiver#infoSubscribed(is.infoEvent)
         * @see #processCallback(InfoEvent)
         */
        @Override
        protected void infoSubscribed(final InfoEvent info) {
            this.processCallback(info);
        }
        
        /**
         * Method called to force an information update. It gets the IS information from IS and calls {@link #processUpdate(is.Info)}.
         */
        protected void forceUpdate() {
            try {
                final T isInfo = this.getInfoInstance();
                this.getRepository().getValue(this.infoName, isInfo);
                this.processUpdate(isInfo);
            }
            catch(final Exception ex) {
                final String msg = "Failed to get the \"" + this.infoName + "\" object from IS (" + ex + ")";
                IguiLogger.warning(msg, ex);
            }
        }

        /**
         * It return a new instance (with default values for all the fields) of the IS information object.
         * 
         * @return A new instance (with default values for all the fields) of the IS information object
         * @throws InstantiationException @see {@link Class#getDeclaredConstructor(Class...)#newInstance()}
         * @throws IllegalAccessException @see {@link Class#getDeclaredConstructor(Class...)#newInstance()}
         * @throws SecurityException @see {@link Class#getDeclaredConstructor(Class...)#newInstance()}
         * @throws NoSuchMethodException @see {@link Class#getDeclaredConstructor(Class...)#newInstance()}
         * @throws InvocationTargetException @see {@link Class#getDeclaredConstructor(Class...)#newInstance()}
         * @throws IllegalArgumentException @see {@link Class#getDeclaredConstructor(Class...)#newInstance()}
         */
        private T getInfoInstance()
            throws InstantiationException,
                IllegalAccessException,
                IllegalArgumentException,
                InvocationTargetException,
                NoSuchMethodException,
                SecurityException
        {
            return this.infoType.getDeclaredConstructor().newInstance();
        }

        /**
         * It processes the {@link InfoEvent} to build the updated IS information and then calls {@link #processUpdate(is.Info)}.
         * 
         * @param info The {@link InfoEvent} coming from the IS call-back
         */
        private void processCallback(final InfoEvent info) {
            try {
                final T isInfo = this.getInfoInstance();
                info.getValue(isInfo);
                this.processUpdate(isInfo);
            }
            catch(final Exception ex) {
                final String msg = "Failed to process IS callback for info \"" + info.getName() + "\": " + ex
                                   + ". Run information may not be correctly updated";
                IguiLogger.error(msg, ex);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
            }
        }
    }

    /**
     * Class extending {@link ISRunInfoReceiver} to get information from the RunParams IS server.
     */
    private final class RunParamsReceiver extends ISRunInfoReceiver<rc.RunParams> {
        RunParamsReceiver() {
            super(IguiConstants.RUNPARAMS_IS_INFO_NAME, rc.RunParams.class);
        }

        /**
         * @see Igui.MainPanel.ISRunInfoReceiver#processUpdate(is.Info)
         */
        @Override
        protected void processUpdate(final rc.RunParams runParamsInfo) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainPanel.this.runNumberValue.setText(Long.toString(runParamsInfo.run_number));

                    final long sor = runParamsInfo.timeSOR.getTime();
                    MainPanel.this.startTimeValue.setText(RunParamsReceiver.this.timeToString(sor));

                    final long eor = runParamsInfo.timeEOR.getTime();
                    MainPanel.this.stopTimeValue.setText(RunParamsReceiver.this.timeToString(eor));

                    MainPanel.this.maxEvTextField.setText(Integer.toString(runParamsInfo.max_events));

                    final String runType = runParamsInfo.run_type;
                    MainPanel.this.runTypeValue.setText(runType);

                    {
                        MainPanel.this.rtComboBox.setEditable(true);
                        MainPanel.this.rtComboBox.setSelectedItem(runType);
                        MainPanel.this.rtComboBox.setEditable(false);
                    }

                    MainPanel.this.btComboBox.setSelectedIndex(runParamsInfo.beam_type);
                    MainPanel.this.beTextField.setText(Integer.toString(runParamsInfo.beam_energy));

                    {
                        MainPanel.this.t0ComboBox.setEditable(true);
                        MainPanel.this.t0ComboBox.setSelectedItem(runParamsInfo.T0_project_tag);
                        MainPanel.this.t0ComboBox.setEditable(false);
                    }

                    MainPanel.this.recordingValue.setText((runParamsInfo.recording_enabled == 0) ? "Disabled" : "Enabled");

                    final int rec = runParamsInfo.recording_enabled;
                    if(rec == 0) {
                        MainPanel.this.recordingValue.setText("Disabled");
                        MainPanel.this.recDisableRadioButton.setSelected(true);
                    } else {
                        MainPanel.this.recordingValue.setText("Enabled");
                        MainPanel.this.recEnableRadioButton.setSelected(true);
                    }

                    MainPanel.this.fnTextField.setText(runParamsInfo.filename_tag);
                }
            });
        }

        /**
         * It translates a time in ms to a human readable string.
         * 
         * @param time Time value in ms
         * @return String representation of <code>time</code>
         */
        private String timeToString(final long time) {
            return (time == 0) ? ""
                              : DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.UK).format(new Date(time));
        }
    }

    /**
     * Class extending {@link ISRunInfoReceiver} used to get the status of the AutoPilot
     */
    private final class AutoPilotReceiver extends ISRunInfoReceiver<rc.AutoPilot> {
        AutoPilotReceiver() {
            super(IguiConstants.AUTOPILOT_IS_INFO_NAME, rc.AutoPilot.class);
        }

        @Override
        protected void processUpdate(final rc.AutoPilot updatedInfo) {
            final boolean value = updatedInfo.enabled;
            
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainPanel.this.autopilotOnButton.setSelected(value);
                    MainPanel.this.autopilotOffButton.setSelected(!value);
                    MainPanel.this.autopilotStatusLabel.setIcon(value == true ? MainPanel.stableIcon : MainPanel.unstableIcon);
                    MainPanel.this.autopilotStatusLabel.setToolTipText(value == true ? "Auto-pilot is ENABLED" : "Auto-pilot is DISABLED");
                    MainPanel.this.autoPilotEnabled.set(value);
                    MainPanel.this.updateDisplay();
                }                
            });
        }
    }
    
    /**
     * Class implementing actions to enable/disable the auto-pilot
     */
    private final class AutoPilotSetter implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            final boolean enableAutoPilot = MainPanel.this.autopilotOnButton.isSelected();
            
            // Do something only if the current state is different
            if(enableAutoPilot != MainPanel.this.autoPilotEnabled.get()) {
                new SwingWorker<Void, Void>() {
                    class RejectedByUser extends Exception {
                        private static final long serialVersionUID = 8362469415126386779L;                        
                    }
                    
                    @Override
                    protected Void doInBackground() throws Exception {                        
                        if((enableAutoPilot == true) && 
                           (MainPanel.this.rootControllerState.getISStatus().getState().equals(RunControlFSM.State.NONE)))
                        {
                            final String[] files = Igui.instance().uncommittedFiles();
                            if(files != null && files.length > 0) {
                                final FutureTask<Boolean> askUser = new FutureTask<>(new Callable<Boolean>() {
                                    @Override
                                    public Boolean call() throws Exception {
                                        Boolean goOn = Boolean.TRUE;
                                        
                                        final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                                         "There are some uncommitted DB files, are you sure you want to enable the AutoPilot?",
                                                                                         "IGUI - AutoPilot Warning",
                                                                                         JOptionPane.YES_NO_OPTION);
                                        if(answer != JOptionPane.YES_OPTION) {
                                            goOn = Boolean.FALSE;
                                        }  
                                        
                                        return goOn;
                                    }                                
                                });
                                
                                javax.swing.SwingUtilities.invokeLater(askUser);
                                
                                final Boolean goWithAutoPilot = askUser.get();
                                if(goWithAutoPilot.equals(Boolean.TRUE) == false) {
                                    throw new RejectedByUser();
                                }
                            }
                        }
                                                  
                        try {
                            final rc.AutoPilotNamed isInfo = new rc.AutoPilotNamed(Igui.instance().getPartition(), IguiConstants.AUTOPILOT_IS_INFO_NAME);
                            isInfo.enabled = enableAutoPilot;
                            isInfo.checkin();
                        }
                        catch(final RepositoryNotFoundException | InfoNotCompatibleException ex) {
                            throw ex;
                        }
                        
                        return null;
                    }                
                    
                    @Override
                    protected void done() {
                        try {
                            this.get();
                        }
                        catch(final ExecutionException ex) {
                            // Writing to IS failed, restore the check-box status properly
                            MainPanel.this.autopilotOnButton.setSelected(!enableAutoPilot);
                            MainPanel.this.autopilotOffButton.setSelected(enableAutoPilot);
                            
                            if(RejectedByUser.class.isInstance(ex.getCause()) == false) {
                                final String errMsg = "Failed enabling the auto-pilot: failed to write the proper value to IS: " + ex;
                                IguiLogger.error(errMsg, ex);
                                ErrorFrame.showError("IGUI - Error", errMsg, ex);
                            }
                        }
                        catch(final InterruptedException ex) {
                            Thread.currentThread().interrupt();

                            final String errMsg = "Failed enabling the auto-pilot: the execution has been interrupted";
                            IguiLogger.error(errMsg, ex);
                            ErrorFrame.showError("IGUI - Error", errMsg, ex);
                        }
                    }
                }.execute();
            }      
        }
    }
    
    /**
     * Class extending {@link ISRunInfoReceiver} used to get information from the RunInfo IS server.
     */
    private final class RunInfoReceiver extends ISRunInfoReceiver<rc.RunInfo> {
        RunInfoReceiver() {
            super(IguiConstants.RUNINFO_IS_INFO_NAME, rc.RunInfo.class);
        }

        /**
         * @see Igui.MainPanel.ISRunInfoReceiver#processUpdate(is.Info)
         */
        @Override
        protected void processUpdate(final rc.RunInfo runInfo) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final long runTime = Integer.toUnsignedLong(runInfo.activeTime);
                    MainPanel.this.totalTimeValue.setText(RunInfoReceiver.this.durationToString(runTime));
                }
            });
        }

        /**
         * It translates a time interval (in seconds) to a human readable string.
         * 
         * @param duration Time interval in seconds
         * @return A string representing the time interval (h, m, s)
         */
        private String durationToString(final long duration) {
            long dur = duration;

            final long hours = dur / 3600;

            dur -= 3600 * hours;
            final long minutes = dur / 60;

            dur -= 60 * minutes;
            final long seconds = dur;

            final StringBuilder str = new StringBuilder();
            str.append(hours);
            str.append(" h, ");
            str.append(minutes);
            str.append(" m, ");
            str.append(seconds);
            str.append(" s");

            return str.toString();
        }
    }

    /**
     * Class extending {@link ISRunInfoReceiver} to get information from the BeamStatus IS server.
     */
    private final class BeamStatusReceiver extends ISRunInfoReceiver<ddc.DdcIntInfo> {
        BeamStatusReceiver() {
            super(IguiConstants.TDAQ_INITIAL_PARTITION, IguiConstants.BEAM_STATUS_IS_SERVER, ddc.DdcIntInfo.class);
        }

        @Override
        protected void processUpdate(final DdcIntInfo updatedInfo) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final int status = updatedInfo.value;
                    switch(status) {
                        case 1:
                            MainPanel.this.beamStatusLabel.setIcon(MainPanel.stableIcon);
                            MainPanel.this.beamStatusLabel.setToolTipText("Beams are STABLE");
                            MainPanel.this.beamState.set(true);
                            break;
                        default:
                            MainPanel.this.beamStatusLabel.setIcon(MainPanel.unstableIcon);
                            MainPanel.this.beamStatusLabel.setToolTipText("Beams are UNSTABLE");
                            MainPanel.this.beamState.set(false);
                            break;
                    }

                    MainPanel.this.updateDisplay();
                }
            });
        }
    }

    /**
     * Class extending {@link ISRunInfoReceiver} to get information about the R4P flag.
     */
    private final class R4PStatusReceiver extends ISRunInfoReceiver<rc.Ready4PhysicsInfo> {
        R4PStatusReceiver() {
            super(IguiConstants.READY4PHYSICS_IS_INFO_NAME, rc.Ready4PhysicsInfo.class);
        }

        @Override
        protected void processUpdate(final rc.Ready4PhysicsInfo updatedInfo) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if(updatedInfo.ready4physics == true) {
                        MainPanel.this.r4pStatusLabel.setIcon(MainPanel.stableIcon);
                        MainPanel.this.r4pStatusLabel.setToolTipText("Ready for Physics flag is set");
                    } else {
                        MainPanel.this.r4pStatusLabel.setIcon(MainPanel.unstableIcon);
                        MainPanel.this.r4pStatusLabel.setToolTipText("Ready for Physics flag is not set");
                    }

                    MainPanel.this.updateDisplay();
                }
            });
        }
    }

    /**
     * Class extending {@link ISRunInfoReceiver} to get information about the LHC clock type.
     */
    private final class ClockTypeReceiver extends ISRunInfoReceiver<TTCInfo.RF2TTC_DCS> {
        ClockTypeReceiver() {
            super(IguiConstants.TDAQ_INITIAL_PARTITION, IguiConstants.CLOCK_TYPE_IS_SERVER, TTCInfo.RF2TTC_DCS.class);
        }

        @Override
        protected void processUpdate(final TTCInfo.RF2TTC_DCS updatedInfo) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainPanel.this.clockType.setText(updatedInfo.BCmainSelection);
                }
            });
        }
    }

    /**
     * Class extending {@link ISRunInfoReceiver} to get information from the LumiBlock IS server.
     */
    private final class LumiInfoReceiver extends ISRunInfoReceiver<TTCInfo.LumiBlock> {
        LumiInfoReceiver() {
            super(IguiConstants.LUMINFO_IS_INFO_NAME, TTCInfo.LumiBlock.class);
        }

        /**
         * @see Igui.MainPanel.ISRunInfoReceiver#processUpdate(is.Info)
         */
        @Override
        protected void processUpdate(final TTCInfo.LumiBlock lumInfo) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainPanel.this.lumiBlockValue.setText(Long.toString(lumInfo.LumiBlockNumber));
                }
            });
        }
    }

    /**
     * Receiver used to receive information about the trigger state (needed to determine the state of the HOLD/RESUME trigger buttons).
     */
    private final class TriggerStateReceiver extends ISRunInfoReceiver<rc.TriggerState> {
        TriggerStateReceiver() {
            super(IguiConstants.TRIGGERINFO_IS_INFO_NAME, rc.TriggerState.class);
        }

        @Override
        protected void processUpdate(final TriggerState updatedInfo) {
            // This is the key: update the volatile reference and then update the display
            final long updateTime = updatedInfo.getTime().getTimeMicro();
            
            boolean stop = false;
            do {
                final StampedObject<rc.TriggerState> currentInfo = MainPanel.this.triggerState.get();
                final long currentTime = currentInfo.getStamp();
                
                if(updateTime >= currentTime) {
                    stop = MainPanel.this.triggerState.compareAndSet(currentInfo, new StampedObject<>(updatedInfo, updateTime));
                } else {
                    stop = true;
                }                
            } while(stop == false);
            
            // Always update the display (stay on the safe side)
            MainPanel.this.updateDisplay();
        }
    }

    /**
     * Receiver used to receive information about the global busy state (needed to determine the state of the HOLD/RESUME trigger buttons).
     */
    private final class GlobalBusyReceiver extends ISRunInfoReceiver<GLOBALBUSY> {
        GlobalBusyReceiver() {
            super(IguiConstants.GLOBALBUSY_IS_INFO_NAME, GLOBALBUSY.class);
        }

        @Override
        protected void processUpdate(final GLOBALBUSY updatedInfo) {
            // This is the key: update the volatile reference and then update the display
            final long updateTime = updatedInfo.getTime().getTimeMicro();
            
            boolean stop = false;
            do {
                final StampedObject<GLOBALBUSY> currentInfo = MainPanel.this.gBusyState.get();
                final long currentTime = currentInfo.getStamp();
                
                if(updateTime >= currentTime) {
                    stop = MainPanel.this.gBusyState.compareAndSet(currentInfo, new StampedObject<>(updatedInfo, updateTime));
                } else {
                    stop = true;
                }
            } while(stop == false);
            
            // Always update the display (stay on the safe side)
            MainPanel.this.updateDisplay();
        }
    }

    /**
     * Multiple listener used by the components in the run settings tab: when the used modifies some value the
     * {@link MainPanel#runSettingsNeedUpdate(boolean)} is called so that the user is informed that the information has been updated only
     * locally and not on the IS server.
     * <p>
     * Implemented interfaces:
     * <ul>
     * <li>ActionListener for recording enabled/disabled radio buttons (triggered when a radio button is selected);
     * <li>PopupMenuListener for all the JComboBox;
     * <li>KeyListener (extending KeyAdapter) for all the JTextField items.
     * </ul>
     */
    private class RunSettingsItemListener extends KeyAdapter implements PopupMenuListener, ActionListener {
        /**
         * String keeping trace of the old/new selected items for the combo box generating the PopupMenuEvent event.
         */
        // Why a PopupMenu listener? We want a listener that called only when the user actually changes something.
        // The content of the combo boxes may change also because of the IS call-backs, and in this case what is
        // shown in the panel is the most up-to-date information. Our goal is to avoid that the user changes
        // something in the panel and forgets to send the new settings to the IS server.
        // Here is the trick: the combo box selected item is cached in 'popupMenuWillBecomeVisible' (i.e., the value
        // when the user clicks on the combo box at the beginning) and then is compared with the one obtained in
        // 'popupMenuWillBecomeInvisible'
        // (i.e., the user may have selected some item and the combo box menu is closing). If the two values are different
        // then 'MainPanel.this.runSettingsNeedUpdate' is called.
        private String selectedComboBoxItem = "";

        RunSettingsItemListener() {
        }

        @Override
        public void popupMenuCanceled(final PopupMenuEvent e) {
        }

        @Override
        public void popupMenuWillBecomeInvisible(final PopupMenuEvent e) {
            // Compare the old selected item with the current one
            @SuppressWarnings("unchecked")
            final JComboBox<String> cb = (JComboBox<String>) e.getSource();
            final String selItem = cb.getSelectedItem().toString();
            if(selItem.equals(this.selectedComboBoxItem) == false) {
                this.impl(e.getSource());
            }
        }

        @Override
        public void popupMenuWillBecomeVisible(final PopupMenuEvent e) {
            // Get the selected item
            @SuppressWarnings("unchecked")
            final JComboBox<String> cb = (JComboBox<String>) e.getSource();
            this.selectedComboBoxItem = cb.getSelectedItem().toString();
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            this.impl(e.getSource());
        }

        @Override
        public void keyTyped(final KeyEvent e) {
            // Process the event generated only when something is typed in the text field
            this.impl(e.getSource());
        }

        private void impl(final Object source) {
            MainPanel.this.runSettingsNeedUpdate(true);
        }
    }

    /**
     * Class describing a button used to send RC commands.
     * <p>
     * All buttons whose aim is to send commands to controllers should extend this class.
     */
    private abstract static class CommandButton extends JButton {

        private static final long serialVersionUID = 5991468231997736180L;

        private final Border defB = BorderFactory.createLineBorder(Color.GRAY);
        private final Border wB = BorderFactory.createLineBorder(Color.RED);

        {
            this.setBorder(this.getDefaultBorder());
            this.setForeground(this.getDefaultForegroundColor());
        }

        /**
         * Constructor
         */
        CommandButton(final String label, final Icon icon) {
            super(label, icon);
        }

        /**
         * It returns the default border.
         * 
         * @return The default border
         */
        public Border getDefaultBorder() {
            return this.defB;
        }

        public Color getDefaultForegroundColor() {
            return Color.BLACK;
        }

        public Color getWarningForegroundColor() {
            return Color.RED;
        }

        /**
         * It returns the border used to take the user attention.
         * 
         * @return The border used to take the user attention
         */
        public Border getWarningBorder() {
            return this.wB;
        }
    }

    /**
     * Class representing a button that, when pressed, will send a transition command to the RootController.
     * 
     * @see MainPanel#transitionButtonAction(Igui.RunControlFSM.Transition)
     */
    private class RCTransitionCommandButton extends CommandButton implements ActionListener {
        private static final long serialVersionUID = 8053646007673186353L;

        private RunControlFSM.Transition tr;

        RCTransitionCommandButton(final RunControlFSM.Transition tr) {
            super(tr.toString(), null);
            this.tr = tr;
            this.addActionListener(this);
            this.setToolTip(tr);
        }
        
        RCTransitionCommandButton(final RunControlFSM.Transition tr, final Icon icon) {
            this(tr);
            this.setIcon(icon);
            this.setText("");
        }
        
        void setTrasition(final RunControlFSM.Transition tr, final String text) {
            this.tr = tr;
            this.setText(text);
            this.setToolTip(tr);
        }
        
        private void setToolTip(final RunControlFSM.Transition tr) {
            this.setToolTipText("Send the \"" + tr.toString() + "\" transition command to the Root Controller to reach the \"" + 
                                    tr.getDestinationState().toString() + "\" state");
        }
        
        @Override
        public void actionPerformed(final ActionEvent e) {
            MainPanel.this.transitionButtonAction(this.tr);
        }
    }

    /**
     * Class representing a button that, when pressed, will send a command to the partition Master Trigger.
     * <p>
     * Subclasses need to override {@link MTCommandButton#actionPerformed(ActionEvent)} to implement the command to send to the Master
     * Trigger.
     */
    private abstract static class MTCommandButton extends CommandButton implements ActionListener {
        private static final long serialVersionUID = -1529498525253611922L;

        MTCommandButton(final Icon icon) {
            this("", icon);
        }
        
        MTCommandButton(final String label) {
            this(label, null);
        }

        MTCommandButton(final String label, final Icon icon) {
            super(label, icon);
            this.addActionListener(this);
        }

        @Override
        public abstract void actionPerformed(final ActionEvent e);
    }

    /**
     * SwingWorker used to update the RunParameters IS information (using the "Settings" tab).
     */
    private class RunParamsUpdater extends SwingWorker<Void, Void> {
        final rc.RunParams newInfo;

        /**
         * Constructor.
         * 
         * @param newInfo The IS information to update.
         */
        RunParamsUpdater(final rc.RunParams newInfo) {
            this.newInfo = newInfo;
        }

        /**
         * The current IS information is checked out, updated with new parameters and sent back to the IS server.
         * 
         * @see javax.swing.SwingWorker#doInBackground()
         */
        @Override
        protected Void doInBackground() throws IguiException.ISException, InterruptedException {
            try {
                javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        Igui.instance().getMainFrame().setWaiting(true);
                        MainPanel.this.okButton.setEnabled(false);
                    }
                });
            }
            catch(final InvocationTargetException ex) {
                IguiLogger.error(ex.toString(), ex);
            }

            final rc.RunParamsNamed isInfo = new rc.RunParamsNamed(Igui.instance().getPartition(),
                                                                   IguiConstants.RUNPARAMS_IS_INFO_NAME,
                                                                   rc.RunParamsNamed.type.getName());

            try {
                isInfo.checkout();
            }
            catch(final is.InfoNotFoundException ex) {
                // Can be ignored
            }
            catch(final Exception ex) {
                throw new IguiException.ISException("Checkout from RunParams IS server failed: " + ex, ex);
            }

            // Update only the attributes modified by the user
            isInfo.max_events = this.newInfo.max_events;
            isInfo.run_type = this.newInfo.run_type;
            isInfo.beam_type = this.newInfo.beam_type;
            isInfo.beam_energy = this.newInfo.beam_energy;
            isInfo.T0_project_tag = this.newInfo.T0_project_tag;
            isInfo.filename_tag = this.newInfo.filename_tag;
            isInfo.recording_enabled = this.newInfo.recording_enabled;

            try {
                isInfo.checkin();
            }
            catch(final Exception ex) {
                throw new IguiException.ISException("Failed to update RunParams IS information: " + ex, ex);
            }

            return null;
        }

        /**
         * It just reports any error.
         * 
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                this.get();
                // Remember to put the button to its defaults
                MainPanel.this.runSettingsNeedUpdate(false);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, "Run settings have been correctly updated");
            }
            catch(final InterruptedException ex) {
                final String errMsg = "Task interruped while waiting for the run parameters to be set";
                IguiLogger.warning(errMsg, ex);
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final String errMsg = "An error occurred while setting run parameters: " + ex.getCause();
                IguiLogger.error(errMsg, ex);
                ErrorFrame.showError("Main Panel Error", errMsg, ex);
            }
            finally {
                Igui.instance().getMainFrame().setWaiting(false);
                MainPanel.this.okButton.setEnabled(true);
            }
        }

    }

    /**
     * This {@link FutureTask} shows a modal dialog asking the user to select the sub-system causing the trigger to be held and the reason
     * why. The task returns an instance of {@link ShowTriggerHeldDialogTask.TaskResult} specifying whether the user has pushed the OK
     * button and the items selected by the user (they will be <code>null</code> if the information should not be written to IS).
     * <p>
     * If the user has pushed the OK button, then no empty values are accepted.
     * <p>
     * To be always executed in the EDT!
     */
    private static class ShowTriggerHeldDialogTask extends FutureTask<ShowTriggerHeldDialogTask.TaskResult> {

        /**
         * An instance of this class is returned by the enclosing {@link FutureTask}.
         */
        private static class TaskResult {
            private final Boolean okSelected;
            private final rc.HoldTriggerInfoNamed.CausedBy system;
            private final rc.HoldTriggerInfoNamed.Reason reason;

            /**
             * Constructor.
             * 
             * @param okSelected Has the user pressed the OK button?
             * @param system Sub-system selected by the user
             * @param reason Reason selected by the user
             */
            public TaskResult(final Boolean okSelected,
                              final rc.HoldTriggerInfoNamed.CausedBy system,
                              final rc.HoldTriggerInfoNamed.Reason reason)
            {
                this.okSelected = okSelected;
                this.system = system;
                this.reason = reason;
            }

            public Boolean okSelected() {
                return this.okSelected;
            }

            public rc.HoldTriggerInfoNamed.CausedBy system() {
                return this.system;
            }

            public rc.HoldTriggerInfoNamed.Reason reason() {
                return this.reason;
            }
        }

        /**
         * Constructor
         */
        public ShowTriggerHeldDialogTask() {
            super(new Callable<ShowTriggerHeldDialogTask.TaskResult>() {
                @Override
                public ShowTriggerHeldDialogTask.TaskResult call() throws Exception {
                    assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

                    // Create panel holding the "sub-systems" check buttons: there will be a check-box for
                    // each enum defined in "rc.StopWhileBeamInfoNamed"
                    final JPanel systemsPanel = new JPanel();
                    final ButtonGroup systemsGroup = new ButtonGroup();
                    final int numCol = 3;
                    final rc.HoldTriggerInfoNamed.CausedBy[] allCauseValues = rc.HoldTriggerInfoNamed.CausedBy.values();
                    {
                        final int ratio = allCauseValues.length / numCol;
                        final int numRow = ((ratio * numCol) < allCauseValues.length) ? ratio + 1 : ratio;
                        systemsPanel.setLayout(new GridLayout(numRow, numCol));
                    }

                    // Create list containing the "sub-systems" check-boxes (they are added to the panel as well)
                    final List<JRadioButton> chbSystemsList = new ArrayList<JRadioButton>();
                    for(final rc.HoldTriggerInfoNamed.CausedBy i : allCauseValues) {
                        final JRadioButton rb = new JRadioButton(i.name());
                        chbSystemsList.add(rb);
                        systemsPanel.add(rb);
                        systemsGroup.add(rb);
                    }

                    systemsPanel.setBorder(BorderFactory.createTitledBorder("List of sub-systems causing the trigger to be held"));

                    // Create panel holding the "reason" check buttons: there will be a check-box for
                    // each enum defined in "rc.StopWhileBeamInfoNamed"
                    final JPanel reasonsPanel = new JPanel();
                    final ButtonGroup reasonsGroup = new ButtonGroup();
                    final rc.HoldTriggerInfoNamed.Reason[] allReasonValues = rc.HoldTriggerInfoNamed.Reason.values();
                    {
                        final int ratio = allReasonValues.length / numCol;
                        final int numRow = ((ratio * numCol) < allReasonValues.length) ? ratio + 1 : ratio;
                        reasonsPanel.setLayout(new GridLayout(numRow, numCol));
                    }

                    // Create list containing the "reasons" check-boxes (they are added to the panel as well)
                    final List<JRadioButton> chbReasonsList = new ArrayList<JRadioButton>();
                    for(final rc.HoldTriggerInfoNamed.Reason i : allReasonValues) {
                        final JRadioButton rb = new JRadioButton(i.name());
                        chbReasonsList.add(rb);
                        reasonsPanel.add(rb);
                        reasonsGroup.add(rb);
                    }

                    reasonsPanel.setBorder(BorderFactory.createTitledBorder("Reasons causing the need for the trigger to be held"));

                    // Show the dialog (and keep showing it if the user does not select anything or until the "cancel" button is pushed)
                    ShowTriggerHeldDialogTask.TaskResult tskResult = null;
                    boolean userHasDone = false;
                    while(userHasDone == false) {
                        // All the panels are added to the JOptionPane
                        final int userAnswer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                             new Object[] {
                                                                                           "Please, provide some additional information about the reason why the trigger has been put on hold",
                                                                                           systemsPanel, reasonsPanel},
                                                                             MainPanel.triggerHeldDialogTitle,
                                                                             JOptionPane.OK_CANCEL_OPTION,
                                                                             JOptionPane.QUESTION_MESSAGE);

                        // These sets keep the list of user selections
                        rc.HoldTriggerInfoNamed.CausedBy selectedSystem = null;
                        rc.HoldTriggerInfoNamed.Reason selectedReason = null;

                        if(userAnswer == JOptionPane.OK_OPTION) {
                            // The user has selected "OK"

                            // Loop over the radio buttons and find the one selected by the user
                            for(final JRadioButton rb : chbSystemsList) {
                                if(rb.isSelected() == true) {
                                    selectedSystem = Enum.valueOf(rc.HoldTriggerInfoNamed.CausedBy.class, rb.getText());
                                    break;
                                }
                            }

                            for(final JRadioButton rb : chbReasonsList) {
                                if(rb.isSelected() == true) {
                                    selectedReason = Enum.valueOf(rc.HoldTriggerInfoNamed.Reason.class, rb.getText());
                                    break;
                                }
                            }

                            // Check whether the user has selected something, if not show again the dialog
                            if(selectedSystem == null) {
                                JOptionPane.showMessageDialog(Igui.instance().getMainFrame(),
                                                              "Please, select one sub-system!",
                                                              MainPanel.triggerHeldDialogTitle,
                                                              JOptionPane.WARNING_MESSAGE);
                            } else if(selectedReason == null) {
                                JOptionPane.showMessageDialog(Igui.instance().getMainFrame(),
                                                              "Please, select one reason!",
                                                              MainPanel.triggerHeldDialogTitle,
                                                              JOptionPane.WARNING_MESSAGE);
                            } else {
                                tskResult = new ShowTriggerHeldDialogTask.TaskResult(Boolean.TRUE, selectedSystem, selectedReason);
                                userHasDone = true;
                            }
                        } else {
                            tskResult = new ShowTriggerHeldDialogTask.TaskResult(Boolean.FALSE, selectedSystem, selectedReason);
                            userHasDone = true;
                        }
                    }

                    return tskResult;
                }
            });
        }
    }

    /**
     * Constructor.
     * <p>
     * It just builds this panel interface.
     * 
     * @param tabbed <code>true</code> if this panel has to be added to the Igui frame tab pane.
     */
    public MainPanel(final boolean tabbed) {
        super(tabbed);

        this.showWarningAtConnect = !IguiConfiguration.instance().hideConnectWarning();

        // Init GlobalBusy data structure (we want to be sure that the RC busy state is set to false;
        // this will allow safe operations even if the global busy state is not published).
        final GLOBALBUSY gb = new GLOBALBUSY();
        gb.RunControlBusy = 0;
        this.gBusyState.set(new StampedObject<GLOBALBUSY>(gb, 0L));

        // Init GUI
        this.initGUI();
    }

    /**
     * If in STATUS display all the buttons are disabled, otherwise {@link #updateDisplay(RunControlApplicationData)} is called.
     * 
     * @see Igui.IguiPanel#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     * @see #enableRunSettings(boolean)
     * @see #disableAllButtons()
     * @see #updateDisplay(RunControlApplicationData)
     */
    @Override
    public void accessControlChanged(final IguiConstants.AccessControlStatus newAccessStatus) {
        if(newAccessStatus.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            this.enableRunSettings(true);
            this.updateDisplay(Igui.instance().getRCInfo()); // It takes care of enabling the right buttons
        } else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainPanel.this.enableRunSettings(false);
                    MainPanel.this.disableAllButtons();
                }
            });
        }
    }

    /**
     * @see Igui.IguiPanel#refreshISSubscription()
     */
    @Override
    public void refreshISSubscription() {
        // Re-subscribe to counter and rate receivers
        {
            this.subscribeCounterAndRateReceivers(false);

            // If the RC status is greater than 'stateToSubscribe' then subscribe again
            if(Igui.instance().getRCState().follows(this.stateToSubscribe)) {
                this.subscribeCounterAndRateReceivers(true);
            }
        }

        // Re-subscribe to run info
        {
            this.subscribeRunInfoReceivers(false);
            this.subscribeRunInfoReceivers(true);
        }
    }

    /**
     * IS information sources are retrieved from the DB and new IS receivers are created. If the RC state is greater than
     * {@link #stateToSubscribe} then the IS subscriptions are performed.
     * <p>
     * The Master Trigger controller is calculated again in case of changes.
     * 
     * @see Igui.IguiPanel#dbReloaded()
     * @see #initSMKTextField()
     * @see #initRunTypeComboBox()
     * @see #initT0ComboBox()
     * @see #subscribeCounterAndRateReceivers(boolean)
     * @see #setCountersAndRatesSources()
     * @see #createCounterAndRateReceivers()
     */
    @Override
    public void dbReloaded() {
        // Get T0 project names, master key and run types from DB
        this.initSMKTextField();
        this.initT0ComboBox();
        this.initRunTypeComboBox();

        // After (new) values have been retrieved from the DB, re-initialize run info taking
        // current values from the IS servers
        @SuppressWarnings("unused")
        final boolean readVolatile = this.runInfoListWhatcher; // The volatile read grants visibility for the 'runInfoReceiversList'
        for(final ISRunInfoReceiver<?> rec : this.runInfoReceiversList) {
            rec.forceUpdate();
        }

        // Remove current subscription to counters and rates IS sources
        this.subscribeCounterAndRateReceivers(false);

        // Get info sources from DB
        this.setCountersAndRatesSources();

        // Create new IS receivers
        this.createCounterAndRateReceivers();

        // Get the Master Trigger
        this.getMasterTrigger();

        // If the RC status is greater than 'stateToSubscribe' then subscribe again
        if(Igui.instance().getRCState().follows(this.stateToSubscribe)) {
            this.subscribeCounterAndRateReceivers(true);
        }
    }

    /**
     * @see Igui.IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return MainPanel.panelName;
    }

    /**
     * @see Igui.IguiPanel#getTabName()
     */
    @Override
    public String getTabName() {
        return MainPanel.panelName;
    }

    /**
     * The DB is read to get the Master Trigger and the IS sources for counter and rates (corresponding IS receivers are created and
     * subscriptions are done if the RC state follows {@link #stateToSubscribe}).
     * <p>
     * IS receivers are created as well to get general information about the status of the run.
     * <p>
     * The panel is updated (i.e., some buttons are enabled/disabled) taking into account the Root Controller state and the access control
     * level.
     * 
     * @see Igui.IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     * @see #initSMKTextField()
     * @see #initRunTypeComboBox()
     * @see #initT0ComboBox()
     * @see #subscribeCounterAndRateReceivers(boolean)
     * @see #subscribeRunInfoReceivers(boolean)
     * @see #setCountersAndRatesSources()
     * @see #createCounterAndRateReceivers()
     * @see #createRunInfoReceivers()
     * @see #getMasterTrigger()
     * @see #updateDisplay(RunControlApplicationData)
     */
    @Override
    public void panelInit(final RunControlFSM.State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException {
        // Get T0 project names, master key and run types from DB
        this.initSMKTextField();
        this.initT0ComboBox();
        this.initRunTypeComboBox();

        // Get IS event (counters and rates) from DB
        this.setCountersAndRatesSources();

        // Create IS receivers for rates and counters and subscribe
        this.createCounterAndRateReceivers();
        if(rcState.follows(this.stateToSubscribe)) {
            this.subscribeCounterAndRateReceivers(true);
        }

        // Create IS receivers for run info and subscribe
        this.createRunInfoReceivers();
        this.subscribeRunInfoReceivers(true);

        // Get the master trigger
        this.getMasterTrigger();

        this.updateDisplay(Igui.instance().getRCInfo());

        // Check the access status level for disabling run settings items
        if(accessStatus.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY) == false) {
            this.enableRunSettings(false);
        }
    }

    /**
     * The executor used to send Root Controller commands is stopped. IS subscriptions are removed.
     * 
     * @see Igui.IguiPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        this.subscribeRunInfoReceivers(false);
        this.subscribeCounterAndRateReceivers(false);

        this.commandExecutor.shutdown();

        IguiLogger.debug("Panel \"" + this.getPanelName() + "\" has been terminated");
    }

    /**
     * It returns <code>true<code>.
     * <p>
     * This panel behavior depends not only upon the Root Controller state, but in general on the 
     * overall Root Controller information (i.e., the busy or the error state).
     * 
     * @see Igui.IguiPanel#rcStateAware()
     */
    @Override
    public boolean rcStateAware() {
        return true;
    }

    /**
     * Actions taken when the RC state changes:
     * <ul>
     * <li>Subscribe to IS servers for rates and counters at {@link #transitionToSubscribe};
     * <li>Select the "Run Settings" tab at {@link RunControlFSM.Transition#CONNECT};
     * <li>Reset counters and select the "Counters" tab at {@link RunControlFSM.Transition#START}.
     * </ul>
     * <p>
     * All the changes for which the only RC state information is not enough are performed in
     * {@link #updateDisplay(RunControlApplicationData)}.
     * 
     * @see Igui.IguiPanel#rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)
     */
    @Override
    public void rcStateChanged(final RunControlFSM.State oldState, final RunControlFSM.State newState) {
        try {
            final Set<RunControlFSM.Transition> trs = RunControlFSM.instance().from(oldState, newState);
            // Be aware about the transition order to avoid overwriting changes!!!

            // Subscribe to IS servers for rates and counters at 'transitionToSubscribe'
            if(trs.contains(this.transitionToSubscribe)) {
                this.subscribeCounterAndRateReceivers(true);
            }

            // Select the "Run Settings" tab at CONNECT and show a warning
            if(trs.contains(RunControlFSM.Transition.CONNECT)) {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        MainPanel.this.infoPane.setSelectedIndex(2);
                        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)
                           && (MainPanel.this.showWarningAtConnect == true) && (MainPanel.this.autoPilotEnabled.get() == false))
                        {
                            ErrorFrame.showWarning("Remember to check the Run Settings before starting the run!");
                        }
                    }
                });
            }

            // Reset counters at START and select the "counters" tab
            if(trs.contains(RunControlFSM.Transition.START)) {
                this.resetCounters();
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        MainPanel.this.infoPane.setSelectedIndex(1);
                    }
                });
            }
        }
        catch(final IllegalArgumentException ex) { // It may be thrown by "from()"
            IguiLogger.warning(ex.getMessage(), ex);
        }
        catch(final IguiException.UnknownTransition ex) {
            IguiLogger.warning(ex.getMessage(), ex);
        }
    }

    /**
     * This methods updates the panel according to Root Controller state.
     * <p>
     * It is called by the IguiFrame any time the Root Controller IS information is updated.
     * <ul>
     * <li>The label showing the Root Controller state is updated (taking into account the busy and error state);
     * <li>If in STATUS DISPLAY all the buttons to send commands to the Root Controller are disabled, otherwise only the ones corresponding
     * to the allowed state transitions are enabled.
     * </ul>
     * 
     * @param rcInfo The IS structure containing all the Root Controller information
     * @see IguiFrame#rcInfoUpdated(RunControlApplicationData)
     * @see #disableAllButtons()
     * @see #setTransitionButtonsState(ApplicationISStatus, boolean)
     * @see #updateDisplay()
     */
    void updateDisplay(final RunControlApplicationData rcInfo) {
        // Here is the trick: update the volatile reference and the update the display
        this.rootControllerState = rcInfo;
        this.updateDisplay();
    }
    
    /**
     * It makes the counter showing the number of times the trigger is on-hold visible or not.
     * <p>
     * To be executed in the EDT.
     * 
     * @param trgState The trigger state
     * @param gbState The global busy state
     * @param rcState The root controller state
     */
    private void setHoldCounterVisibility(final TriggerState trgState, 
                                          final GLOBALBUSY gbState, 
                                          RunControlApplicationData.ApplicationISStatus rcInfo) 
    {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";
        
        if(rcInfo.getState().equals(RunControlFSM.State.RUNNING) == false) {
            this.holdTriggerCounter.setVisible(false);
        } else {        
            switch(trgState.triggerStatus) {
                case TriggerOnHold:
                    this.holdTriggerCounter.setVisible(true);
                    break;
                case TriggerActive:
                    this.holdTriggerCounter.setVisible(gbState.RunControlBusy > 0 ? true : false);
                    break;
                default:
                    this.holdTriggerCounter.setVisible(false);
            }
        }
    }
    
    /**
     * Implementation of {@link #updateDisplay(RunControlApplicationData)}, {@link TriggerStateReceiver#processUpdate(TriggerState)} and
     * {@link GlobalBusyReceiver#processUpdate(GLOBALBUSY)}.
     */
    private void updateDisplay() {
        // REMEMBER: the fact that we use the EDT (a single defined thread) here is the key: if we had
        // multiple threads executing this code then the 'volatile' trick would not be enough to
        // guarantee thread safe operations
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Use the available values for RC, Trigger and GlobalBusy state info (remember that they are volatile)
                // Keep a reference to the current values, if they change we are granted to work anyway
                // with a valid reference (volatile magic).
                // This trick avoids loosing some state combination between RC, Trigger and GlobalBusy states due to the fact
                // that the information comes from different sources in different threads
                final RunControlApplicationData.ApplicationISStatus rcInfo = MainPanel.this.rootControllerState.getISStatus();
                final rc.TriggerState trgState = MainPanel.this.triggerState.get().getObject();
                final GLOBALBUSY gbState = MainPanel.this.gBusyState.get().getObject();
                final boolean isAutoPilotEnabled = MainPanel.this.autoPilotEnabled.get();
                final JButton resumeButton = MainPanel.this.bMap.get(MTCommands.RESUMETRIGGER.toString());
                final JButton holdButton = MainPanel.this.bMap.get(MTCommands.HOLDTRIGGER.toString());
                
                final RunControlFSM.State rcState = rcInfo.getState();

                // Update buttons in the new RC command panel
                MainPanel.this.goTowardsRunningButton.setToolTipText("");
                MainPanel.this.goTowardsNoneButton.setToolTipText("");
                final Set<RunControlFSM.Transition> transitionsFromHere = RunControlFSM.instance().getTransitions(rcState);
                for(final RunControlFSM.Transition t : transitionsFromHere) {
                    boolean upAlreadySet = false;
                    boolean downAlreadySet = false;
                    // Ignore SHUTDOWN if not in INITIAL
                    if((t.equals(RunControlFSM.Transition.SHUTDOWN) == false) || (rcState.equals(RunControlFSM.State.INITIAL) == true)) {
                        if(t.upTransition() == true) {
                            if((upAlreadySet == false) || (t.isComposite() == true)) {
                                MainPanel.this.goTowardsRunningButton.setTrasition(t, "");
                                upAlreadySet = true;
                            }
                        } else {
                            if((downAlreadySet == false) || (t.isComposite() == true)) {
                                MainPanel.this.goTowardsNoneButton.setTrasition(t, "");
                                downAlreadySet = true;
                            }                            
                        }
                    }
                }
                
                // Set the label showing the RC state
                if((rcInfo.isUp() == true) && (rcState.equals(RunControlFSM.State.ABSENT) == false)) {
                    MainPanel.this.currentStateLabel.setText(rcState.toString());
                    MainPanel.this.currentRCStateLabel.setText(rcState.toString());
                    MainPanel.this.stateProgressBar.setValue(rcState.ordinal());
                    MainPanel.this.currentStateLabel.setBackground(rcState.getColor());
                    MainPanel.this.currentRCStateLabel.setBackground(rcState.getColor());
                } else {
                    final RunControlFSM.ApplicationStatus rcStatus = rcInfo.getStatus();
                    switch(rcStatus) {
                        case NOTAV:
                        case ABSENT:
                        case EXITED:
                        case FAILED:
                            MainPanel.this.currentStateLabel.setText(RunControlFSM.ApplicationStatus.ABSENT.toString());
                            MainPanel.this.currentRCStateLabel.setText(RunControlFSM.ApplicationStatus.ABSENT.toString());
                            MainPanel.this.stateProgressBar.setValue(0);
                            break;
                        default:
                            MainPanel.this.currentStateLabel.setText(rcStatus.toString());
                            MainPanel.this.currentRCStateLabel.setText(rcStatus.toString());
                            MainPanel.this.stateProgressBar.setValue(0);
                    }

                    MainPanel.this.currentStateLabel.setBackground(rcStatus.getColor());
                    MainPanel.this.currentRCStateLabel.setBackground(rcStatus.getColor());
                }

                if(rcInfo.isBusy() == true) {
                    MainPanel.this.currentStateLabel.setBorder(BorderFactory.createLineBorder(Color.red));
                    MainPanel.this.currentRCStateLabel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
                } else {
                    MainPanel.this.currentStateLabel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
                                                                                                  BorderFactory.createEmptyBorder(1,
                                                                                                                                  1,
                                                                                                                                  1,
                                                                                                                                  1)));
                    MainPanel.this.currentRCStateLabel.setBorder(BorderFactory.createEtchedBorder());
                }

                if(rcInfo.isError() == true) {
                    MainPanel.this.currentStateLabel.setIcon(MainPanel.errorIcon);
                    MainPanel.this.currentRCStateLabel.setIcon(MainPanel.errorIcon);
                    MainPanel.this.currentStateLabel.setForeground(Color.red);
                    MainPanel.this.currentRCStateLabel.setForeground(Color.red);
                } else {
                    MainPanel.this.currentStateLabel.setIcon(null);
                    MainPanel.this.currentRCStateLabel.setIcon(null);
                    MainPanel.this.currentStateLabel.setForeground(Color.black);
                    MainPanel.this.currentRCStateLabel.setForeground(Color.black);
                }

                // Set the tool-tip for the new auto-pilot button                
                if(isAutoPilotEnabled == true) {
                    MainPanel.this.autoPilotEnabledTag.setVisible(true);
                    MainPanel.this.autoPilotDisabledTag.setVisible(false);

                    MainPanel.this.autoPilotButton.setToolTipText("The auto-pilot is enabled: click to stop it");
                } else if(rcState.equals(RunControlFSM.State.NONE) || rcState.equals(RunControlFSM.State.RUNNING)) {
                    MainPanel.this.autoPilotEnabledTag.setVisible(false);
                    MainPanel.this.autoPilotDisabledTag.setVisible(true);

                    
                    MainPanel.this.autoPilotButton.setToolTipText("The auto-pilot is disabled: enable it to reach the "
                                                                   + (rcState.equals(RunControlFSM.State.NONE) ? RunControlFSM.State.RUNNING.toString()
                                                                                                               : RunControlFSM.State.NONE.toString())
                                                                   + " state");
                } else {
                    MainPanel.this.autoPilotEnabledTag.setVisible(false);
                    MainPanel.this.autoPilotDisabledTag.setVisible(true);
                    
                    MainPanel.this.autoPilotButton.setToolTipText("The auto-pilot is disabled and cannot be enabled in state " + rcState.toString());
                }
                
                // Set proper label for the auto-pilot button
                if(rcState.equals(RunControlFSM.State.NONE) || rcState.equals(RunControlFSM.State.RUNNING)) {
                    MainPanel.this.autopilotOnButton.setText("Go to "
                                                             + (rcState.equals(RunControlFSM.State.NONE) ? RunControlFSM.State.RUNNING.toString()
                                                                                                         : RunControlFSM.State.NONE.toString()));
                } else {
                    MainPanel.this.autopilotOnButton.setText("Cannot be enabled in state " + rcState.toString());
                }
                
                // Set proper text for the "resume trigger" button    
                if((gbState.RunControlBusy == 0)
                    || (trgState.triggerStatus.equals(rc.TriggerState.TriggerStatus.NoTrigger) == true))
                {                    
                    resumeButton.setText("RESUME TRG");
                    MainPanel.this.holdTriggerCounter.setText("");
                } else {
                    final String sv = Integer.toString(gbState.RunControlBusy);
                    resumeButton.setText("RESUME TRG (" + sv + ")");
                    MainPanel.this.holdTriggerCounter.setText(sv);
                }                                    
                
                MainPanel.this.setHoldCounterVisibility(trgState, gbState, rcInfo);

                // Check the access level
                if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {                    
                    // Enable only the right buttons
                    MainPanel.this.setTransitionButtonsState(rcInfo, isAutoPilotEnabled);
                                        
                    // The button to start the autopilot can be enabled only in NONE and RUNNING                    
                    MainPanel.this.autopilotOffButton.setEnabled(true);
                    
                    final boolean autoPilotOnButtonToBeEnabled = ((rcState.equals(RunControlFSM.State.NONE)
                                                                   || rcState.equals(RunControlFSM.State.RUNNING))
                                                                  && ((rcInfo.isBusy() == false) && (rcInfo.isError() == false)));
                    
                    // The new button start/stop the auto-pilot should be always enabled if the auto-pilot is on
                    // When the auto-pilot i off, it should be enabled only in NONE and RUNNING
                    final boolean autoPilotButtonToBeEnabled = (((rcState.equals(RunControlFSM.State.NONE)
                                                                  || rcState.equals(RunControlFSM.State.RUNNING))
                                                                 && ((rcInfo.isBusy() == false) && (rcInfo.isError() == false))))
                                                               || (isAutoPilotEnabled == true);
                    
                    // Do it in the right order
                    if(autoPilotButtonToBeEnabled == true) {
                        MainPanel.this.autopilotOnButton.setEnabled(autoPilotOnButtonToBeEnabled);
                        MainPanel.this.autoPilotButton.setEnabled(autoPilotButtonToBeEnabled);
                    } else {
                        MainPanel.this.autoPilotButton.setEnabled(autoPilotButtonToBeEnabled);
                        MainPanel.this.autopilotOnButton.setEnabled(autoPilotOnButtonToBeEnabled);
                    }
                                        
                    // Enable the button to update run settings only if state is less then CONNECTED
                    final boolean enableRunSettingsButton = (rcState.follows(RunControlFSM.State.CONNECTED) == false)
                                                            && ((rcInfo.isBusy() == false)
                                                                || (rcInfo.getCurrentTransition().equals(RunControlFSM.Transition.START.toString()) == false));
                    MainPanel.this.okButton.setEnabled(enableRunSettingsButton); 
                    
                    // HOLD and RESUME trigger buttons: they need to be treated separately because they
                    // are not transition buttons
                    if(rcState.equals(RunControlFSM.State.RUNNING) == false) {
                        // The HOLD/RESUME trigger buttons are enabled only if the RC is in the RUNNING state
                        MainPanel.this.newPauseTriggerButton.setEnabled(false);
                        holdButton.setEnabled(false);
                        MainPanel.this.newResumeTriggerButton.setEnabled(false);
                        resumeButton.setEnabled(false);
                    } else {
                        // We are in RUNNING, but the HOLD and RESUME buttons should be
                        // disabled if we are moving away from this state (we could check
                        // the busy status, but we want to be able to hold and resume the
                        // trigger even if the RootController is busy). Basically we want
                        // to avoid to have the RESUME button enabled any time the run is
                        // stopped
                        final String currTr = rcInfo.getCurrentTransition();

                        boolean movingFromRunning = false;
                        final Set<RunControlFSM.Transition> trs = RunControlFSM.instance().getTransitions(RunControlFSM.State.RUNNING);
                        for(final RunControlFSM.Transition t : trs) {
                            if(t.toString().equalsIgnoreCase(currTr) == true) {
                                movingFromRunning = true;
                                break;
                            }
                        }
                        
                        if(movingFromRunning == false) {
                            switch(trgState.triggerStatus) {
                                case NoTrigger:
                                    MainPanel.this.newPauseTriggerButton.setEnabled(false);
                                    holdButton.setEnabled(false);
                                    MainPanel.this.newResumeTriggerButton.setEnabled(false);
                                    resumeButton.setEnabled(false);
                                    break;
                                case TriggerActive:
                                    if(gbState.RunControlBusy > 0) {
                                        MainPanel.this.newPauseTriggerButton.setEnabled(false);
                                        holdButton.setEnabled(false);
                                        resumeButton.setEnabled(true);
                                        MainPanel.this.newResumeTriggerButton.setEnabled(true);
                                    } else {
                                        holdButton.setEnabled(true);
                                        MainPanel.this.newPauseTriggerButton.setEnabled(true);
                                        MainPanel.this.newResumeTriggerButton.setEnabled(false);
                                        resumeButton.setEnabled(false);
                                    }
                                    break;
                                case TriggerOnHold:
                                    MainPanel.this.newPauseTriggerButton.setEnabled(false);
                                    holdButton.setEnabled(false);
                                    resumeButton.setEnabled(true);
                                    MainPanel.this.newResumeTriggerButton.setEnabled(true);
                                    break;
                                default:
                                    MainPanel.this.newPauseTriggerButton.setEnabled(false);
                                    holdButton.setEnabled(false);
                                    MainPanel.this.newResumeTriggerButton.setEnabled(false);
                                    resumeButton.setEnabled(false);
                            }
                        } else {
                            MainPanel.this.newPauseTriggerButton.setEnabled(false);
                            holdButton.setEnabled(false);
                            MainPanel.this.newResumeTriggerButton.setEnabled(false);
                            resumeButton.setEnabled(false);
                        }
                    }
                } else {
                    MainPanel.this.disableAllButtons();
                }
            }
        });
    }
   
    /**
     * It gets the Master Trigger as defined in the partition.
     */
    private void getMasterTrigger() {
        String mtControllerName = null;

        try {
            final MasterTrigger mt = Igui.instance().getDalPartition().get_MasterTrigger();
            if(mt != null) {
                final RunControlApplicationBase rc = mt.get_Controller();
                if(rc != null) {
                    mtControllerName = rc.UID();
                    IguiLogger.info("The partition Master Trigger is " + mtControllerName);
                } else {
                    IguiLogger.warning("The Master Trigger does not define a controller; it will not be possible to hold or resume the trigger");
                }
            } else {
                IguiLogger.warning("The Master Trigger is not defined in the partition; it will not be possible to hold or resume the trigger");
            }
        }
        catch(final Exception ex) {
            IguiLogger.error("Error looking for the partition Master Trigger: " + ex, ex);
        }

        if(mtControllerName != null) {
            this.trgCommander = new TriggerCommander(Igui.instance().getPartition(), mtControllerName);
        } else {
            this.trgCommander = null;
        }
    }

    /**
     * It gets from the database the value of the SMK.
     * 
     * @return The value of the SMK as a string
     */
    private String getSuperMasterKeyFromDAL() {
        final StringBuilder mk = new StringBuilder();

        try {
            final dal.TriggerConfiguration trgConf = Igui.instance().getDalPartition().get_TriggerConfiguration();
            if(trgConf != null) {
                final dal.TriggerDBConnection trgConn = trgConf.get_TriggerDBConnection();
                if(trgConn != null) {
                    final int masterKey = trgConn.get_SuperMasterKey();
                    mk.append(String.valueOf(masterKey));
                } else {
                    IguiLogger.warning("Cannot get the Super Master Key value from database: the \"TriggerDBConnection\" object is not defined");
                }
            } else {
                IguiLogger.warning("Cannot get the Super Master Key value from database: the \"TriggerConfiguration\" object is not defined");
            }
        }
        catch(final Exception ex) {
            IguiLogger.error("Cannot get the Super Master Key value from database, got an exception: " + ex, ex);
        }

        return mk.toString();
    }

    /**
     * It initializes the text field showing the value of the SMK.
     * 
     * @see #getSuperMasterKeyFromDAL()
     */
    private void initSMKTextField() {
        final String spm = this.getSuperMasterKeyFromDAL();

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainPanel.this.superMasterKeyValue.setText(spm);
            }
        });
    }

    /**
     * It gets the list of possible run types from the database.
     * 
     * @return The list of possible run types.
     */
    private Set<String> getRunTypesFromDAL() {
        final Set<String> runTypes = new LinkedHashSet<String>();

        try {
            final String[] types = Igui.instance().getDalPartition().get_RunTypes();
            if((types != null) && (types.length > 0)) {
                Collections.addAll(runTypes, types);
            }
        }
        catch(final Exception ex) {
            IguiLogger.error("Cannot get the list of Run Types from the database, got an exception: " + ex, ex);
        }

        return runTypes;
    }

    /**
     * It initializes the combo-box listing the possible run types.
     * 
     * @see #getRunTypesFromDAL()
     */
    private void initRunTypeComboBox() {
        final Set<String> runTypes = this.getRunTypesFromDAL();

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainPanel.this.rtComboBox.removeAllItems();
                if(runTypes.size() > 0) {
                    for(final String s : runTypes) {
                        MainPanel.this.rtComboBox.addItem(s);
                    }
                } else {
                    for(final String s : MainPanel.this.DEFAULT_RUNTYPES) {
                        MainPanel.this.rtComboBox.addItem(s);
                    }
                }
            }
        });
    }

    /**
     * It gets a list of possible T0 project tags from the database.
     * 
     * @return A list of possible T0 project tags
     */
    private Set<String> getT0ProjectTagsFromDAL() {
        final Set<String> t0Tags = new LinkedHashSet<String>();

        try {
            final dal.OnlineSegment onlSeg = Igui.instance().getDalPartition().get_OnlineInfrastructure();
            if(onlSeg != null) {
                final String tags[] = onlSeg.get_T0_ProjectTag();
                if((tags != null) && (tags.length > 0)) {
                    Collections.addAll(t0Tags, tags);
                }
            } else {
                IguiLogger.error("Cannot get the list of TO Project Tags from the database: the \"OnlineSegment\" object id not defined");
            }
        }
        catch(final Exception ex) {
            IguiLogger.error("Cannot get the list of TO Project Tags from the database, got an exception: " + ex, ex);
        }

        return t0Tags;
    }

    /**
     * It initializes the combo-box listing the possible values for the T0 project tag.
     * 
     * @see #getT0ProjectTagsFromDAL()
     */
    private void initT0ComboBox() {
        final Set<String> t0Names = this.getT0ProjectTagsFromDAL();

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainPanel.this.t0ComboBox.removeAllItems();
                for(final String s : t0Names) {
                    MainPanel.this.t0ComboBox.addItem(s);
                }
            }
        });
    }

    /**
     * It creates the IS receivers to get run status information.
     * 
     * @see RunInfoReceiver
     * @see RunParamsReceiver
     * @see LumiInfoReceiver
     * @see TriggerStateReceiver
     * @see BeamStatusReceiver
     * @see R4PStatusReceiver
     * @see GlobalBusyReceiver
     * @see ClockTypeReceiver
     * @see AutoPilotReceiver
     */
    private void createRunInfoReceivers() {
        this.runInfoReceiversList.clear();

        this.runInfoReceiversList.add(new RunInfoReceiver());
        this.runInfoReceiversList.add(new RunParamsReceiver());
        this.runInfoReceiversList.add(new LumiInfoReceiver());
        this.runInfoReceiversList.add(new TriggerStateReceiver());
        this.runInfoReceiversList.add(new BeamStatusReceiver());
        this.runInfoReceiversList.add(new GlobalBusyReceiver());
        this.runInfoReceiversList.add(new ClockTypeReceiver());
        this.runInfoReceiversList.add(new R4PStatusReceiver());
        this.runInfoReceiversList.add(new AutoPilotReceiver());

        // Create happen-before relationship
        // This will work fine as soon as the list is not read and modified at the same time
        this.runInfoListWhatcher = true;
    }

    /**
     * It subscribes or un-subscribes from the IS information about the run status.
     * <p>
     * The subscription to run information is always kept.
     * 
     * @param subscribe If <code>true</code> then the subscription is performed, otherwise the subscription is removed
     * @see #createRunInfoReceivers()
     */
    private void subscribeRunInfoReceivers(final boolean subscribe) {
        // Create happen-before relationship
        // This will work fine as soon as the list is not read and modified at the same time
        @SuppressWarnings("unused")
        final boolean readVolatile = this.runInfoListWhatcher;

        for(final ISInfoReceiver r : this.runInfoReceiversList) {
            if(subscribe == true) {
                try {
                    r.subscribe();
                    IguiLogger.debug("Subscribed to IS \"" + r.getName() + "\"");
                }
                catch(final IguiException.ISException ex) {
                    final String msg = ex.getMessage() + ". The corresponding information will not be correctly updated";
                    IguiLogger.error(msg, ex);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                }
            } else {
                try {
                    r.unsubscribe();
                    IguiLogger.debug("Unsubscribed from IS \"" + r.getName() + "\"");
                }
                catch(final IguiException.ISException ex) {
                    IguiLogger.warning(ex.getMessage());
                }
            }
        }
    }

    /**
     * It crates all the IS receivers for counters and rates.
     * 
     * @see CounterReceiver
     * @see RateReceiver
     */
    private void createCounterAndRateReceivers() {
        this.counterReceiversList.clear();

        Collections.addAll(this.counterReceiversList,
                           new CounterReceiver(InfoTypes.LVL1, this.l1Counter),
                           new CounterReceiver(InfoTypes.HLT, this.hltCounter),
                           new CounterReceiver(InfoTypes.RECORDING, this.recCounter),
                           new RateReceiver(InfoTypes.LVL1, this.l1Rate),
                           new RateReceiver(InfoTypes.HLT, this.hltRate),
                           new RateReceiver(InfoTypes.RECORDING, this.recRate));

        // Create happen-before relationship
        // This will work fine as soon as the list is not read and modified at the same time
        this.counterListWhatcher = true;
    }

    /**
     * It crates or removes subscriptions from the IS servers holding the information about the counter and rates.
     * 
     * @param subscribe If <code>true</code> the subscription is performed, otherwise it is removed
     */
    private void subscribeCounterAndRateReceivers(final boolean subscribe) {
        // Create happen-before relationship
        // This will work fine as soon as the list is not read and modified at the same time
        @SuppressWarnings("unused")
        final boolean readVolatile = this.counterListWhatcher;

        for(final CountersAndRatesReceiver r : this.counterReceiversList) {
            if(subscribe == true) {
                try {
                    if(r.isSubscribed() == false) {
                        r.subscribe();
                        IguiLogger.debug("Subscribed to IS \"" + r.getName() + "\"");
                    }
                }
                catch(final IguiException.ISException ex) {
                    final String msg = ex.getMessage();
                    IguiLogger.error(msg, ex);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                }
            } else {
                try {
                    r.unsubscribe();
                    IguiLogger.debug("Unsubscribed from IS \"" + r.getName() + "\"");
                }
                catch(final IguiException.ISException ex) {
                    IguiLogger.warning(ex.getMessage());
                }
            }
        }
    }

    /**
     * It process the <code>token</code> array to return an array containing the IS server name, info name and attribute name.
     * 
     * @param tokens Array coming from the IS source description in the database
     * @return An array of size 3 containing the IS server, info and attribute names
     * @see #setCounterAndRatesAttributes(dal.IS_EventsAndRates, InfoTypes)
     */
    private String[] processInfoSourceAttribute(final String[] tokens) {
        final int paramsSize = tokens.length;

        // At least three parameters: server, info and attribute
        // Example: <server name>.<the information>.<name may contain>.<several dots>.<attribute name>
        assert (paramsSize >= 3);

        final String serverName = tokens[0];
        final String attributeName = tokens[paramsSize - 1];

        // Calculate the attribute name: it can have '.'
        final java.lang.StringBuilder pathName = new java.lang.StringBuilder();
        for(int i = 1; i < (paramsSize - 1); ++i) {
            pathName.append(tokens[i] + ".");
        }
        final String infoName = pathName.substring(0, pathName.length() - 1); // Do not take into account last '.'

        return new String[] {serverName, infoName, attributeName};
    }

    /**
     * It sets the appropriate values for the specified <code>infoType</code>.
     * 
     * @param dalIS The database object containing the IS description for the rate/counter
     * @param infoType The {@link InfoTypes} to properly setup using the database information
     * @throws IguiException.ConfigException Some error occurred retrieving IS counter information from the database
     * @see #processInfoSourceAttribute(String[])
     * @see #setCountersAndRatesSources()
     */
    private void setCounterAndRatesAttributes(final dal.IS_EventsAndRates dalIS, final InfoTypes infoType) throws IguiException.ConfigException {
        try {
            if(dalIS != null) {
                // First the counter IS information description
                final String counter = dalIS.get_EventCounter();
                if((counter != null) && (counter.length() > 0)) {
                    // Fields are separated by dots
                    final String[] tokens = counter.split("\\.");
                    if(tokens.length >= 3) {
                        // Use the tokens to extract the meaningful information: IS server, information and attribute name
                        final String[] info = this.processInfoSourceAttribute(tokens);
                        infoType.setCounterInfoSource(info[0], info[1], info[2]);
                    } else {
                        IguiLogger.warning("Invalid format for " + infoType.toString() + " event counter (" + counter + ")"
                                           + " it must have the \"server_name.info_name.attribute_name\" format");
                    }
                } else {
                    IguiLogger.info("No IS source defined for " + infoType.toString() + " event counter, using defaults");
                }
    
                // Now do the same for the rate
                final String rate = dalIS.get_Rate();
                if((rate != null) && (rate.length() > 0)) {
                    // Fields are separated by dots
                    final String[] tokens = rate.split("\\.");
                    if(tokens.length >= 3) {
                        // Use the tokens to extract the meaningful information: IS server, information and attribute name
                        final String[] info = this.processInfoSourceAttribute(tokens);
                        infoType.setRateInfoSource(info[0], info[1], info[2]);
                    } else {
                        IguiLogger.warning("Invalid format for " + infoType.toString() + " rate counter (" + counter + ")"
                                           + " it must have the \"server_name.info_name.attribute_name\" format");
                    }
                } else {
                    IguiLogger.info("No IS source defined for " + infoType.toString() + " rate, using defaults");
                }
            } else {
                IguiLogger.info("No IS source defined for " + infoType.toString() + " counters, using defaults");
            }
        }
        catch(final ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting info from database about the IS counter " + dalIS.UID(), ex);
        }
    }

    /**
     * It sets the IS information sources for all the rates and counters.
     * 
     * @see #setCounterAndRatesAttributes(dal.IS_EventsAndRates, InfoTypes)
     */
    private void setCountersAndRatesSources() {
        try {
            final dal.IS_InformationSources isInfoSources = Igui.instance().getDalPartition().get_IS_InformationSource();

            if(isInfoSources != null) {
                this.setCounterAndRatesAttributes(isInfoSources.get_LVL1(), InfoTypes.LVL1);
                this.setCounterAndRatesAttributes(isInfoSources.get_HLT(), InfoTypes.HLT);
                this.setCounterAndRatesAttributes(isInfoSources.get_Recording(), InfoTypes.RECORDING);
            } else {
                IguiLogger.warning("No IS info source for event rates and counters found in the database, using default values");
            }
        }
        catch(final Exception ex) {
            IguiLogger.error("Failed to get counter and event rates IS info source from database: " + ex, ex);
        }
        finally {
            for(final InfoTypes t : InfoTypes.values()) {
                IguiLogger.debug(t.toString() + " IS info sources: \"" + t.getCounterInfoSource() + "\" (event counter), \""
                                 + t.getRateInfoSource() + "\" (rate)");
            }
        }
    }

    /**
     * It updates the IS run information: called when the "OK" button in the "Run Settings" tab is pushed.
     * 
     * @see RunParamsUpdater
     */
    private void updateRunSettings() {
        final rc.RunParams info = new rc.RunParams();

        final StringBuilder errString = new StringBuilder();

        // Max Events
        {
            final String maxEv = this.maxEvTextField.getText();
            try {
                final int maxEvts = Integer.parseInt(maxEv);
                if(maxEvts >= 0) {
                    info.max_events = maxEvts;
                } else {
                    errString.append("\nThe \"Max Events\" field must be a non negative integer");
                }
            }
            catch(final NumberFormatException ex) {
                errString.append("\nThe \"Max Events\" field must be a non negative integer");
            }
        }

        // Run Type
        {
            final String runTypeString = this.rtComboBox.getSelectedItem().toString();
            if(runTypeString.isEmpty() == false) {
                info.run_type = this.rtComboBox.getSelectedItem().toString();
            } else {
                errString.append("\nOne of the available \"Run Type\" items must be selected");
            }
        }

        // Beam Type
        {
            final int beamType = this.btComboBox.getSelectedIndex();
            if(beamType != -1) {
                info.beam_type = beamType;
            } else {
                errString.append("\nOne of the available \"Beam Type\" items must be selected");
            }
        }

        // Beam energy
        {
            final String beamEnergy = this.beTextField.getText();
            try {
                final int nrg = Integer.parseInt(beamEnergy);
                if(nrg >= 0) {
                    info.beam_energy = nrg;
                } else {
                    errString.append("\nThe \"Beam Energy\" field must be a non negative integer");
                }
            }
            catch(final NumberFormatException ex) {
                errString.append("\nThe \"Beam Energy\" field must be a non negative integer");
            }
        }

        // T0 project name
        {
            final String t0String = this.t0ComboBox.getSelectedItem().toString();
            if(t0String.isEmpty() == false) {
                info.T0_project_tag = this.t0ComboBox.getSelectedItem().toString();
            } else {
                errString.append("\nOne of the available T0 Project Tag must be selected");
            }
        }

        // File name tag
        {
            final String fnString = this.fnTextField.getText().trim();

            // The regular expression will match all the POSIX punctuation chars, but some of them
            // are allowed: the trick is to replace the ones which are allowed but match the regular
            // expression with a 'regular' char, and pass the new string to the matcher.
            String stringToRegEx = fnString;
            for(final char ch : this.fnGoodChars) {
                stringToRegEx = stringToRegEx.replace(ch, 'a');
            }

            if((MainPanel.fnRegEx.matcher(stringToRegEx).matches() == true) || (fnString.length() > MainPanel.maxFnLen)) {
                errString.append("The \"File Name Tag\" length must not exceed " + MainPanel.maxFnLen
                                 + " characters\nand not contain any POSIX punctuation character or empty spaces");
            } else {
                info.filename_tag = fnString;
            }
        }

        // Recording
        {
            if(this.recEnableRadioButton.isSelected()) {
                info.recording_enabled = 1;
            } else {
                info.recording_enabled = 0;
            }
        }

        // If no errors then update the information in IS
        if(errString.toString().isEmpty() == true) {
            new RunParamsUpdater(info).execute();
        } else {
            ErrorFrame.showError("The provided Run Settings contain some errors:\n" + errString.toString()
                                 + "\n\nPlease, correct the wrong information.");
        }
    }

    /**
     * This method is executed every time a transition command button is pushed.
     * <p>
     * The transition command is sent to the Root Controller using an executor service with only one worker thread.
     * 
     * @param tr The RC transition the button refers to
     * @see Igui#sendRootControllerTransitionCommand(Igui.RunControlFSM.Transition, String)
     * @see RunControlFSM.Transition
     */
    private void transitionButtonAction(final RunControlFSM.Transition tr) {
        boolean sendCommand = true;

        final String cmd = tr.toString();
        final String value = System.getenv(IguiConstants.P1_ENV_VAR);
        final boolean isP1 = ((value != null) && (value.isEmpty() == false));
        if(tr.equals(RunControlFSM.Transition.SHUTDOWN) == true) {
            // Do a check if the command is SHUTDOWN and the RC state is not INITIAL: in this
            // case ask the user if he/she really wants to shutdown
            
            final RunControlFSM.State rcState = this.rootControllerState.getISStatus().getState();
            
            if((rcState.equals(RunControlFSM.State.INITIAL) == false) && (isP1 == true)) {
                final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                 "Do you really want to execute SHUTDOWN even if you are in state different than INITIAL?",
                                                                 "Transition Command Warning",
                                                                 JOptionPane.YES_NO_OPTION);
                if(answer != JOptionPane.YES_OPTION) {
                    sendCommand = false;
                }
            }
        } else if((tr.equals(RunControlFSM.Transition.CONFIGURE) == true) || (tr.equals(RunControlFSM.Transition.CONFIG) == true)) {
            // Do a check if CONFIG/CONFGURE is executed without previous SHUTDOWN 
            
            final String lastTransition = this.rootControllerState.getISStatus().getLastTransition();
            if((lastTransition.equals(RunControlFSM.Transition.INITIALIZE.toString()) == false) && (isP1 == true)) {
                final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                 "Do you really want to execute CONFIGURE/CONFIG even if you have not executed a SHUTDOWN?",
                                                                 "Transition Command Warning",
                                                                 JOptionPane.YES_NO_OPTION);
                if(answer != JOptionPane.YES_OPTION) {
                    sendCommand = false;
                }                
            }            
        } else if(tr.equals(RunControlFSM.Transition.STOP) == true) {
            // If it is a STOP transition, then check whether a warning message should be shown
            final String stopMsgProperty = IguiConfiguration.instance().getStopWarning();
            if(stopMsgProperty.isEmpty() == false) {
                final String[] msgAtPart = stopMsgProperty.split(":");
                for(final String prop : msgAtPart) {
                    final String[] msgPart = prop.split("@");
                    if(msgPart.length == 2) {
                        final String msg = msgPart[0].trim();
                        final String part = msgPart[1].trim();

                        if(part.equals(Igui.instance().getPartition().getName())) {
                            final JOptionPane pane = new ErrorFrame.IguiOptionPane("Do you really want to stop the run?\n\n" + msg,
                                                                                   JOptionPane.WARNING_MESSAGE,
                                                                                   JOptionPane.YES_NO_OPTION);
                            final JDialog diag = pane.createDialog(Igui.instance().getMainFrame(), "Transition Command Warning");
                            diag.setVisible(true);

                            final Object result = pane.getValue();
                            if(Integer.class.isInstance(result) == true) {
                                final Integer v = (Integer) result;
                                if(v.intValue() != JOptionPane.YES_OPTION) {
                                    sendCommand = false;
                                }
                            } else {
                                sendCommand = false;
                            }

                            break;
                        }
                    } else {
                        IguiLogger.warning("The \"" + IguiConstants.IGUI_STOP_MSG_PROPERTY + "\" property is mal-formed: "
                                           + stopMsgProperty);
                    }
                }
            }
        }

        if(sendCommand == true) {
            this.commandExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    IguiLogger.info("Sending the \"" + cmd + "\" command to the Root Controller");
                    try {
                        Igui.instance().sendRootControllerTransitionCommand(tr);
                    }
                    catch(final InterruptedException ex) {
                        Thread.currentThread().interrupt();

                        final String errMsg = "Failed sending the \"" + cmd
                                              + "\" command to the Root Controller: the execution has been interrupted";
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("IGUI - Transition Error", errMsg, ex);
                    }
                    catch(final RCException ex) {
                        final String errMsg = "Failed sending the \"" + cmd + "\" command to the Root Controller: " + ex;
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("IGUI - Transition Error", errMsg, ex);
                    }
                }
            });
        }
    }

    /**
     * It sends a command to the Master Trigger (if it exists) to hold the trigger.
     */
    private void holdTrigger() {
        this.commandExecutor.execute(new Runnable() {
            @Override
            public void run() {
                final TriggerCommander t = MainPanel.this.trgCommander;
                if(t != null) {
                    try {
                        IguiLogger.info("Asking the Master Trigger to hold the trigger");

                        // Send the command to the master trigger
                        t.hold();

                        // Add info in IS to take note that the trigger has been put on hold (only with stable beams)
                        if(MainPanel.this.beamState.get() == true) {
                            final FutureTask<ShowTriggerHeldDialogTask.TaskResult> tsk = new ShowTriggerHeldDialogTask();
                            javax.swing.SwingUtilities.invokeLater(tsk);

                            try {
                                final ShowTriggerHeldDialogTask.TaskResult res = tsk.get();
                                if(res.okSelected().equals(Boolean.TRUE)) {
                                    final rc.HoldTriggerInfoNamed hti = new rc.HoldTriggerInfoNamed(Igui.instance().getPartition(),
                                                                                                    IguiConstants.HOLD_TRIGGER_IS_INFO_NAME);

                                    hti.reason = res.reason();
                                    hti.reasonString = hti.reason.name();

                                    hti.causedBy = res.system();
                                    hti.causedByString = hti.causedBy.name();

                                    hti.checkin();
                                }
                            }
                            catch(final ExecutionException ex) {
                                final String errMsg = "Failed getting the information provided by the user about the trigger put on hold: "
                                                      + ex;
                                IguiLogger.warning(errMsg, ex);
                                Igui.instance().internalMessage(MessageSeverity.WARNING, errMsg);
                            }
                            catch(final InterruptedException ex) {
                                final String errMsg = "Failed getting the information provided by the user about the trigger put on hold: "
                                                      + ex;
                                IguiLogger.warning(errMsg, ex);
                                Igui.instance().internalMessage(MessageSeverity.WARNING, errMsg);
                                Thread.currentThread().interrupt();
                            }
                            catch(final Exception ex) {
                                final String errMsg = "Failed writing the trigger on hold information to the IS server \""
                                                      + IguiConstants.HOLD_TRIGGER_IS_INFO_NAME + "\": " + ex;
                                IguiLogger.warning(errMsg, ex);
                                Igui.instance().internalMessage(MessageSeverity.WARNING, errMsg);
                            }
                        }
                    }
                    catch(final TRIGGER.CommandExecutionFailed ex) {
                        final String errMsg = "Failed sending command to the Master Trigger to hold the trigger: " + ex.reason;
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("IGUI - Command Failed", errMsg, ex);
                    }
                    catch(final Exception ex) {
                        final String errMsg = "Unexpected error sending command to the Master Trigger to hold the trigger: " + ex;
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("IGUI - Command Failed", errMsg, ex);
                    }
                } else { // TriggerCommander is null here
                    final String errMsg = "Failed sending command to the Master Trigger to hold the trigger: Master Trigger not defined in partition";
                    IguiLogger.error(errMsg);
                    Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                }
            }
        });

    }

    /**
     * It send a command to the Master Trigger (if it exists) to resume the trigger.
     */
    private void resumeTrigger() {
        // Do a check if the command is RESUME TRIGGER
        final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                         "Do you really want to resume the trigger? You shall not do it when:"
                                                             + "\n1) Any of the detectors is recovering/resynchronizing.",
                                                         "Run Control Command Warning",
                                                         JOptionPane.YES_NO_OPTION);
        final boolean sendCommand;
        if(answer != JOptionPane.YES_OPTION) {
            sendCommand = false;
        } else {
            sendCommand = true;
        }

        if(sendCommand == true) {
            this.commandExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    final TriggerCommander t = MainPanel.this.trgCommander;
                    if(t != null) {
                        try {
                            IguiLogger.info("Asking the Master Trigger to resume the trigger");

                            // Send the command to the Master Trigger
                            t.resume();
                        }
                        catch(final TRIGGER.CommandExecutionFailed ex) {
                            final String errMsg = "Failed sending command to the Master Trigger to resume the trigger: " + ex.reason;
                            IguiLogger.error(errMsg, ex);
                            ErrorFrame.showError("IGUI - Command Failed", errMsg, ex);
                        }
                        catch(final Exception ex) {
                            final String errMsg = "Unexpected error sending command to the Master Trigger to resume the trigger: " + ex;
                            IguiLogger.error(errMsg, ex);
                            ErrorFrame.showError("IGUI - Command Failed", errMsg, ex);
                        }
                    } else { // TriggerCommander is null here
                        final String errMsg = "Failed sending command to the Master Trigger to resume the trigger: Master Trigger not defined in partition";
                        IguiLogger.error(errMsg);
                        Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                    }
                }
            });
        }
    }

    /**
     * It takes actions to let the user know that the run settings need to be updates (i.e., new values are selected in the panel but the
     * "set Value" buttons has not been pushed yet and information is not updated to the IS server).
     * <p>
     * The "Set Value" button background and its border will appear red if <code>isNeeded</code> is <code>true</code>, otherwise it will
     * have the default colors.
     * <p>
     * This method can be called from every thread.
     * 
     * @param isNeeded <code>true</code> if the user should update the information to IS
     */
    private void runSettingsNeedUpdate(final boolean isNeeded) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if(isNeeded == true) {
                    // Keep the initial button dimension; I do not know why but changing the border
                    // will give you the button a non-sense dimension
                    final Dimension d = MainPanel.this.okButton.getSize();
                    MainPanel.this.okButton.setForeground(Color.RED);
                    MainPanel.this.okButton.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    MainPanel.this.okButton.setPreferredSize(d);
                    MainPanel.this.infoPane.setBackgroundAt(2, Color.RED);
                } else {
                    MainPanel.this.okButton.setForeground(null);
                    MainPanel.this.okButton.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                    MainPanel.this.infoPane.setBackgroundAt(2, null);
                }
            }
        });
    }

    /**
     * It enables or disables all the components in the "Run Settings" tab.
     * 
     * @param enable If <code>true</code> the components are enabled, otherwise they are disabled.
     */
    private void enableRunSettings(final boolean enable) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainPanel.this.maxEvTextField.setEnabled(enable);
                MainPanel.this.rtComboBox.setEnabled(enable);
                MainPanel.this.btComboBox.setEnabled(enable);
                MainPanel.this.beTextField.setEnabled(enable);
                MainPanel.this.t0ComboBox.setEnabled(enable);
                MainPanel.this.fnTextField.setEnabled(enable);
                MainPanel.this.recEnableRadioButton.setEnabled(enable);
                MainPanel.this.recDisableRadioButton.setEnabled(enable);
            }
        });
    }

    /**
     * It disables all the buttons.
     */
    private void disableAllButtons() {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

        this.goTowardsNoneButton.setEnabled(false);
        this.goTowardsRunningButton.setEnabled(false);
        this.newShutdownButton.setEnabled(false);
        this.newPauseTriggerButton.setEnabled(false);
        this.newResumeTriggerButton.setEnabled(false);        
        this.autoPilotButton.setEnabled(false);
        
        final Collection<JButton> buttonsList = this.bMap.values();
        for(final JButton b : buttonsList) {
            b.setEnabled(false);
        }
        
        this.okButton.setEnabled(false);
        this.autopilotOffButton.setEnabled(false);
        this.autopilotOnButton.setEnabled(false);
    }

    /**
     * Method enabling/disabling the right transition buttons for each Root Controller state.
     * 
     * @param rcInfo Information about the Root Controller state
     * @param isAutoPilotEnabled <code>True</code> if the auto-pilot is enabled
     * @see #updateDisplay(RunControlApplicationData)
     */
    private void setTransitionButtonsState(final ApplicationISStatus rcInfo,
                                           final boolean isAutoPilotEnabled) 
    {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

        final RunControlFSM.State rcState = rcInfo.getState();
        final boolean enable = (rcInfo.isBusy() == false) && (rcInfo.isError() == false) && (isAutoPilotEnabled == false);
        
        this.goTowardsNoneButton.setEnabled((enable == true) && (rcState.equals(RunControlFSM.State.NONE) == false));
        this.goTowardsRunningButton.setEnabled((enable == true) && (rcState.follows(RunControlFSM.State.CONNECTED) == false));
        this.newShutdownButton.setEnabled((enable == false) 
                                          || ((rcState.equals(RunControlFSM.State.NONE) == false)
                                              && (rcState.equals(RunControlFSM.State.INITIAL) == false)));

        // Get all the transitions possible from the 'rcState' state
        final Set<RunControlFSM.Transition> trs = RunControlFSM.instance().getTransitions(rcState);
        final Set<RunControlFSM.Transition> allTransitions = EnumSet.allOf(RunControlFSM.Transition.class);
        for(final RunControlFSM.Transition t : allTransitions) {
            final JButton b = this.bMap.get(t.toString());
            if(b != null) {
                b.setEnabled(((enable == true) && (trs.contains(t) == true))
                             || ((enable == false) && (t.equals(RunControlFSM.Transition.SHUTDOWN))));
            }
        }
    }

    /**
     * It resets (i.e., sets their values to 0) all the counters and rates text panes.
     */
    private void resetCounters() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainPanel.this.l1Counter.setText("0");
                MainPanel.this.hltCounter.setText("0");
                MainPanel.this.recCounter.setText("0");

                MainPanel.this.l1Rate.setText("0");
                MainPanel.this.hltRate.setText("0");
                MainPanel.this.recRate.setText("0");
            }
        });
    }

    /**
     * It builds this panel GUI.
     * <p>
     * Once a button is created it is associated to the corresponding transition.
     */
    private void initGUI() {
        final BorderLayout thisLayout = new BorderLayout();
        this.setLayout(thisLayout);
        this.setPreferredSize(new java.awt.Dimension(300, 450));
        this.setSize(300, 450);

        // START >> New CommandPanel        
        final JPanel newRCCommandPanel = new JPanel(true);
        {
            final GridBagLayout newRCCommandPanelLayout = new GridBagLayout();
            newRCCommandPanelLayout.columnWidths = new int[] {80, 80, 60};
            newRCCommandPanelLayout.rowHeights = new int[] {45, 40, 45};
            newRCCommandPanel.setLayout(newRCCommandPanelLayout);
            
            // Go towards NONE
            this.goTowardsNoneButton.setOpaque(false);
            this.goTowardsNoneButton.setContentAreaFilled(false);
            this.goTowardsNoneButton.setBorder(new EmptyBorder(1, 1, 1, 1));
            newRCCommandPanel.add(this.goTowardsNoneButton, new GridBagConstraints(0,
                                                                                   0,
                                                                                   1,       
                                                                                   1,
                                                                                   0.0,
                                                                                   0.0,
                                                                                   GridBagConstraints.WEST,
                                                                                   GridBagConstraints.NONE,
                                                                                   new Insets(1, 1, 1, 1),
                                                                                   0,
                                                                                   0));           
            
            // Autopilot
            this.autoPilotButton.setOpaque(false);
            this.autoPilotButton.setContentAreaFilled(false);
            this.autoPilotButton.setBorder(new EmptyBorder(1, 1, 1, 1));
            this.autoPilotButton.addActionListener((final ActionEvent e) -> {
                final boolean isAutoPilotOn = MainPanel.this.autoPilotEnabled.get();
                if(isAutoPilotOn == true) {
                     MainPanel.this.autopilotOffButton.doClick();
                } else {
                    MainPanel.this.autopilotOnButton.doClick();
                } 
             });
            
            final DefaultOverlayable overlayAutoPilotButton = new DefaultOverlayable(this.autoPilotButton, this.autoPilotEnabledTag, DefaultOverlayable.NORTH_EAST);
            overlayAutoPilotButton.addOverlayComponent(this.autoPilotDisabledTag, DefaultOverlayable.NORTH_EAST);
            overlayAutoPilotButton.setOverlayLocationInsets(new Insets(5, 0, 0, 15));

            newRCCommandPanel.add(overlayAutoPilotButton , new GridBagConstraints(1,
                                                                                0,
                                                                                1,       
                                                                                1,
                                                                                0.0,
                                                                                0.0,
                                                                                GridBagConstraints.CENTER,
                                                                                GridBagConstraints.NONE,
                                                                                new Insets(1, 1, 1, 1),
                                                                                0,
                                                                                0));      
            
            // Go towards RUNNING
            this.goTowardsRunningButton.setOpaque(false);
            this.goTowardsRunningButton.setContentAreaFilled(false);
            this.goTowardsRunningButton.setBorder(new EmptyBorder(1, 1, 1, 1));
            newRCCommandPanel.add(this.goTowardsRunningButton, new GridBagConstraints(2,
                                                                                      0,
                                                                                      1,       
                                                                                      1,
                                                                                      0.0,
                                                                                      0.0,
                                                                                      GridBagConstraints.EAST,
                                                                                      GridBagConstraints.NONE,
                                                                                      new Insets(1, 1, 1, 1),
                                                                                      0,
                                                                                      0));

            // State label
            this.currentRCStateLabel.setOpaque(true);
            this.currentRCStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
            
            final JPanel labelPanel = new JPanel();
            labelPanel.setLayout(new BorderLayout(0, 0));
            labelPanel.add(this.currentRCStateLabel, BorderLayout.CENTER);
            labelPanel.setMaximumSize(new Dimension(220, 22));

            this.stateProgressBar.setMaximumSize(new Dimension(220, 3));
            this.stateProgressBar.setStringPainted(false);
                        
            final Box b = Box.createVerticalBox();            
            b.add(labelPanel);
            b.add(this.stateProgressBar);
                       
            newRCCommandPanel.add(b, new GridBagConstraints(0,
                                                            1,
                                                            3,
                                                            1,
                                                            0.0,
                                                            0.0,
                                                            GridBagConstraints.CENTER,
                                                            GridBagConstraints.HORIZONTAL,
                                                            new Insets(1, 1, 1, 1),
                                                            0,
                                                            0));                       
            
            // SHUTDOWN
            this.newShutdownButton.setContentAreaFilled(false);
            this.newShutdownButton.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.RED, 1), 
                                                                             BorderFactory.createRaisedBevelBorder()));
            newRCCommandPanel.add(this.newShutdownButton, new GridBagConstraints(0,
                                                                              2,
                                                                              1,       
                                                                              1,
                                                                              0.0,
                                                                              0.0,
                                                                              GridBagConstraints.WEST,
                                                                              GridBagConstraints.NONE,
                                                                              new Insets(1, 1, 1, 1),
                                                                              0,
                                                                              0));
            
            // Pause the trigger            
            this.newPauseTriggerButton.setContentAreaFilled(false);
            this.newPauseTriggerButton.setBorder(new EmptyBorder(1, 1, 1, 1));            
            this.newPauseTriggerButton.setToolTipText("Pause the trigger");
            
            // Resume the trigger
            this.newResumeTriggerButton.setContentAreaFilled(false);
            this.newResumeTriggerButton.setBorder(new EmptyBorder(1, 1, 1, 1));     
            this.newResumeTriggerButton.setToolTipText("Resume the trigger");
                       
            this.holdTriggerCounter.setHorizontalTextPosition(SwingConstants.CENTER);
            this.holdTriggerCounter.setVisible(false);
            final DefaultOverlayable overlayResumeButton = new DefaultOverlayable(this.newResumeTriggerButton, this.holdTriggerCounter, DefaultOverlayable.NORTH_EAST);
            overlayResumeButton.setOverlayLocationInsets(new Insets(0, 0, 0, 5));
            
            this.edtTimer.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    MainPanel.this.holdTriggerCounter.setVisible(!MainPanel.this.holdTriggerCounter.isVisible());
                }
            });
            
            final Box triggerButtonsBox = Box.createHorizontalBox();
            triggerButtonsBox.add(Box.createHorizontalStrut(5));
            triggerButtonsBox.add(this.newPauseTriggerButton);
            triggerButtonsBox.add(Box.createHorizontalStrut(30));
            triggerButtonsBox.add(overlayResumeButton);
            triggerButtonsBox.add(Box.createHorizontalStrut(5));
     
            final JPanel triggerButtonsPanel = new JPanel(new BorderLayout());
            triggerButtonsPanel.add(triggerButtonsBox, BorderLayout.CENTER);
            triggerButtonsPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
            
            newRCCommandPanel.add(triggerButtonsPanel,
                                  new GridBagConstraints(1,
                                                         2,
                                                         2,
                                                         1,
                                                         0.0,
                                                         0.0,
                                                         GridBagConstraints.EAST,
                                                         GridBagConstraints.NONE,
                                                         new Insets(1, 1, 1, 1),
                                                         0,
                                                         0));
        }
        
        newRCCommandPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED),
                                                                       BorderFactory.createBevelBorder(BevelBorder.RAISED)));        
        
        // START >> CommandPanel
        final JTabbedPane rcTab = new JTabbedPane();        
        final JPanel rcCommandPanel = new JPanel(true);
        final GridBagLayout CommandPanelLayout = new GridBagLayout();
        CommandPanelLayout.columnWidths = new int[] {7, 7};
        CommandPanelLayout.rowHeights = new int[] {30, 30, 30, 30};
        CommandPanelLayout.columnWeights = new double[] {0.1, 0.1};
        CommandPanelLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0};
        rcCommandPanel.setLayout(CommandPanelLayout);
        rcCommandPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED),
                                                                                                     BorderFactory.createBevelBorder(BevelBorder.RAISED)),
                                                                  "Run Control Commands",
                                                                  TitledBorder.LEADING,
                                                                  TitledBorder.TOP,
                                                                  new java.awt.Font("Dialog", 2, 12)));
        rcTab.addTab("Legacy", rcCommandPanel);
        rcTab.addTab("New", newRCCommandPanel);
        rcTab.setSelectedIndex(1);
        // START >> shutdownButton
        final JButton shutdownButton = new RCTransitionCommandButton(RunControlFSM.Transition.SHUTDOWN);
        rcCommandPanel.add(shutdownButton, new GridBagConstraints(0,
                                                                  0,
                                                                  1,
                                                                  1,
                                                                  0.5,
                                                                  0.5,
                                                                  GridBagConstraints.WEST,
                                                                  GridBagConstraints.BOTH,
                                                                  new Insets(7, 10, 7, 10),
                                                                  0,
                                                                  0));
        this.bMap.put(RunControlFSM.Transition.SHUTDOWN.toString(), shutdownButton);
        // END << shutdownButton
        // START >> initializeButton
        final JButton initializeButton = new RCTransitionCommandButton(RunControlFSM.Transition.INITIALIZE);
        rcCommandPanel.add(initializeButton, new GridBagConstraints(1,
                                                                    0,
                                                                    1,
                                                                    1,
                                                                    0.5,
                                                                    0.5,
                                                                    GridBagConstraints.CENTER,
                                                                    GridBagConstraints.BOTH,
                                                                    new Insets(7, 10, 7, 10),
                                                                    0,
                                                                    0));
        this.bMap.put(RunControlFSM.Transition.INITIALIZE.toString(), initializeButton);
        // END << initializeButton
        // START >> unconfigButton
        final JButton unconfigButton = new RCTransitionCommandButton(RunControlFSM.Transition.UNCONFIG);
        rcCommandPanel.add(unconfigButton, new GridBagConstraints(0,
                                                                  1,
                                                                  1,
                                                                  1,
                                                                  0.5,
                                                                  0.5,
                                                                  GridBagConstraints.CENTER,
                                                                  GridBagConstraints.BOTH,
                                                                  new Insets(7, 10, 7, 10),
                                                                  0,
                                                                  0));
        this.bMap.put(RunControlFSM.Transition.UNCONFIG.toString(), unconfigButton);
        // END << unconfigButton
        // START >> configButton
        final JButton configButton = new RCTransitionCommandButton(RunControlFSM.Transition.CONFIG);
        rcCommandPanel.add(configButton, new GridBagConstraints(1,
                                                                1,
                                                                1,
                                                                1,
                                                                0.5,
                                                                0.5,
                                                                GridBagConstraints.CENTER,
                                                                GridBagConstraints.BOTH,
                                                                new Insets(7, 10, 7, 10),
                                                                0,
                                                                0));
        this.bMap.put(RunControlFSM.Transition.CONFIG.toString(), configButton);
        // END << configButton
        // START >> stopButton
        final JButton stopButton = new RCTransitionCommandButton(RunControlFSM.Transition.STOP);
        rcCommandPanel.add(stopButton, new GridBagConstraints(0,
                                                              2,
                                                              1,
                                                              1,
                                                              0.5,
                                                              0.5,
                                                              GridBagConstraints.CENTER,
                                                              GridBagConstraints.BOTH,
                                                              new Insets(7, 10, 7, 10),
                                                              0,
                                                              0));
        this.bMap.put(RunControlFSM.Transition.STOP.toString(), stopButton);
        // END << stopButton
        // START >> startButton
        final JButton startButton = new RCTransitionCommandButton(RunControlFSM.Transition.START);
        rcCommandPanel.add(startButton, new GridBagConstraints(1,
                                                               2,
                                                               1,
                                                               1,
                                                               0.5,
                                                               0.5,
                                                               GridBagConstraints.CENTER,
                                                               GridBagConstraints.BOTH,
                                                               new Insets(7, 10, 7, 10),
                                                               0,
                                                               0));
        this.bMap.put(RunControlFSM.Transition.START.toString(), startButton);
        // END << startButton
        // START >> pauseButton
        final JButton holdTriggerButton = new MTCommandButton("HOLD TRG") {
            private static final long serialVersionUID = -8350627735667685176L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                MainPanel.this.holdTrigger();
            }
        };
        holdTriggerButton.setToolTipText("Put the trigger on hold");
        rcCommandPanel.add(holdTriggerButton, new GridBagConstraints(0,
                                                                     3,
                                                                     1,
                                                                     1,
                                                                     0.5,
                                                                     0.5,
                                                                     GridBagConstraints.CENTER,
                                                                     GridBagConstraints.BOTH,
                                                                     new Insets(7, 10, 7, 10),
                                                                     0,
                                                                     0));
        this.bMap.put(MTCommands.HOLDTRIGGER.toString(), holdTriggerButton);
        // END << pauseButton
        // START >> continueButton
        final CommandButton resumeTriggerButton = new MTCommandButton("RESUME TRG") {
            private static final long serialVersionUID = 5153055914477196417L;

            @Override
            public void setEnabled(final boolean enable) {
                super.setEnabled(enable);
                
                if(enable == true) {                    
                    MainPanel.this.edtTimer.start();
                } else {                    
                    MainPanel.this.edtTimer.stop();
                    // Be sure that the button is not left red
                    this.setForeground(this.getDefaultForegroundColor());
                    this.setBorder(this.getDefaultBorder());
                }
            }

            @Override
            public void actionPerformed(final ActionEvent e) {
                MainPanel.this.resumeTrigger();
            }
        };
        resumeTriggerButton.setToolTipText("Resume the trigger");

        this.edtTimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if(resumeTriggerButton.isEnabled() == true) {
                    final Color bc = resumeTriggerButton.getForeground();
                    if(bc.equals(resumeTriggerButton.getWarningForegroundColor()) == true) {
                        resumeTriggerButton.setForeground(resumeTriggerButton.getDefaultForegroundColor());
                        resumeTriggerButton.setBorder(resumeTriggerButton.getDefaultBorder());
                    } else {
                        resumeTriggerButton.setForeground(resumeTriggerButton.getWarningForegroundColor());
                        resumeTriggerButton.setBorder(resumeTriggerButton.getWarningBorder());
                    }
                } else {
                    resumeTriggerButton.setForeground(resumeTriggerButton.getDefaultForegroundColor());
                    resumeTriggerButton.setBorder(resumeTriggerButton.getDefaultBorder());
                }
            }
        });
        
        rcCommandPanel.add(resumeTriggerButton, new GridBagConstraints(1,
                                                                       3,
                                                                       1,
                                                                       1,
                                                                       0.5,
                                                                       0.5,
                                                                       GridBagConstraints.CENTER,
                                                                       GridBagConstraints.BOTH,
                                                                       new Insets(7, 10, 7, 10),
                                                                       0,
                                                                       0));
        this.bMap.put(MTCommands.RESUMETRIGGER.toString(), resumeTriggerButton);

        // rcCommandPanel.setPreferredSize(new Dimension(300, 200));
        rcCommandPanel.setMinimumSize(new Dimension(0, 170));

        // END << continueButton
        // END << CommandPanel
        // START >> statePanel
        final JPanel statePanel = new JPanel(true);
        final GridBagLayout statePanelLayout = new GridBagLayout();
        statePanelLayout.columnWidths = new int[] {99, 100};
        statePanelLayout.rowHeights = new int[] {27};
        statePanelLayout.columnWeights = new double[] {0.0, 0.1};
        statePanelLayout.rowWeights = new double[] {0.0};
        statePanel.setLayout(statePanelLayout);
        // statePanel.setPreferredSize(new java.awt.Dimension(400, 40));
        // statePanel.setSize(400, 30);
        // START >> infoPanel
        final JPanel infoPanel = new JPanel(true);
        final GridBagLayout infoPanelLayout = new GridBagLayout();
        infoPanelLayout.columnWidths = new int[] {126, 7};
        infoPanelLayout.rowHeights = new int[] {10, 10, 20, 10, 10, 10, 10};
        infoPanelLayout.columnWeights = new double[] {0.0, 0.1};
        infoPanelLayout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
        infoPanel.setLayout(infoPanelLayout);
        // infoPanel.setSize(100, 400);
        // START >> runTypeLabel
        final JLabel runTypeLabel = new JLabel();
        infoPanel.add(runTypeLabel, new GridBagConstraints(0,
                                                           0,
                                                           1,
                                                           1,
                                                           0.0,
                                                           0.0,
                                                           GridBagConstraints.CENTER,
                                                           GridBagConstraints.HORIZONTAL,
                                                           new Insets(0, 0, 0, 0),
                                                           0,
                                                           0));
        runTypeLabel.setText("Run type");
        // END << runTypeLabel
        // START >> recordingLabel
        final JLabel recordingLabel = new JLabel();
        infoPanel.add(recordingLabel, new GridBagConstraints(0,
                                                             3,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        recordingLabel.setText("Recording");
        // END << recordingLabel
        // START >> startTileLabel
        final JLabel startTileLabel = new JLabel();
        infoPanel.add(startTileLabel, new GridBagConstraints(0,
                                                             4,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        startTileLabel.setText("Start time");
        // END << startTileLabel
        // START >> stopTimeLabel
        final JLabel stopTimeLabel = new JLabel();
        infoPanel.add(stopTimeLabel, new GridBagConstraints(0,
                                                            5,
                                                            1,
                                                            1,
                                                            0.0,
                                                            0.0,
                                                            GridBagConstraints.CENTER,
                                                            GridBagConstraints.HORIZONTAL,
                                                            new Insets(0, 0, 0, 0),
                                                            0,
                                                            0));
        stopTimeLabel.setText("Stop time");
        // END << stopTimeLabel
        // START >> totalTimeLabel
        final JLabel totalTimeLabel = new JLabel();
        infoPanel.add(totalTimeLabel, new GridBagConstraints(0,
                                                             6,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        totalTimeLabel.setText("Total time");
        // END << totalTimeLabel
        // START >> runTypeValue
        infoPanel.add(this.runTypeValue, new GridBagConstraints(1,
                                                                0,
                                                                1,
                                                                1,
                                                                0.0,
                                                                0.0,
                                                                GridBagConstraints.CENTER,
                                                                GridBagConstraints.HORIZONTAL,
                                                                new Insets(0, 0, 0, 0),
                                                                0,
                                                                0));
        this.runTypeValue.setEditable(false);
        this.runTypeValue.setHorizontalAlignment(SwingConstants.CENTER);
        // END << runTypeValue
        // START >> recordingValue
        infoPanel.add(this.recordingValue, new GridBagConstraints(1,
                                                                  3,
                                                                  1,
                                                                  1,
                                                                  0.0,
                                                                  0.0,
                                                                  GridBagConstraints.CENTER,
                                                                  GridBagConstraints.HORIZONTAL,
                                                                  new Insets(0, 0, 0, 0),
                                                                  0,
                                                                  0));
        this.recordingValue.setEditable(false);
        this.recordingValue.setHorizontalAlignment(SwingConstants.CENTER);
        // END << recordingValue
        // START >> startTimeValue
        infoPanel.add(this.startTimeValue, new GridBagConstraints(1,
                                                                  4,
                                                                  1,
                                                                  1,
                                                                  0.0,
                                                                  0.0,
                                                                  GridBagConstraints.CENTER,
                                                                  GridBagConstraints.HORIZONTAL,
                                                                  new Insets(0, 0, 0, 0),
                                                                  0,
                                                                  0));
        this.startTimeValue.setEditable(false);
        this.startTimeValue.setHorizontalAlignment(SwingConstants.CENTER);
        // END << startTimeValue
        // START >> stopTimeValue
        infoPanel.add(this.stopTimeValue, new GridBagConstraints(1,
                                                                 5,
                                                                 1,
                                                                 1,
                                                                 0.0,
                                                                 0.0,
                                                                 GridBagConstraints.CENTER,
                                                                 GridBagConstraints.HORIZONTAL,
                                                                 new Insets(0, 0, 0, 0),
                                                                 0,
                                                                 0));
        this.stopTimeValue.setEditable(false);
        this.stopTimeValue.setHorizontalAlignment(SwingConstants.CENTER);
        // END << stopTimeValue
        // START >> totalTimeValue
        infoPanel.add(this.totalTimeValue, new GridBagConstraints(1,
                                                                  6,
                                                                  1,
                                                                  1,
                                                                  0.0,
                                                                  0.0,
                                                                  GridBagConstraints.CENTER,
                                                                  GridBagConstraints.HORIZONTAL,
                                                                  new Insets(0, 0, 0, 0),
                                                                  0,
                                                                  0));
        final JLabel spmLabel = new JLabel("Super Master Key");
        infoPanel.add(spmLabel, new GridBagConstraints(0,
                                                       1,
                                                       1,
                                                       1,
                                                       0.0,
                                                       0.0,
                                                       GridBagConstraints.CENTER,
                                                       GridBagConstraints.HORIZONTAL,
                                                       new Insets(0, 0, 0, 0),
                                                       0,
                                                       0));

        infoPanel.add(this.superMasterKeyValue, new GridBagConstraints(1,
                                                                       1,
                                                                       1,
                                                                       1,
                                                                       0.0,
                                                                       0.0,
                                                                       GridBagConstraints.CENTER,
                                                                       GridBagConstraints.HORIZONTAL,
                                                                       new Insets(0, 0, 0, 0),
                                                                       0,
                                                                       0));
        final JLabel ctLabel = new JLabel("LHC Clock Type");
        infoPanel.add(ctLabel, new GridBagConstraints(0,
                                                      2,
                                                      1,
                                                      1,
                                                      0.0,
                                                      0.0,
                                                      GridBagConstraints.CENTER,
                                                      GridBagConstraints.HORIZONTAL,
                                                      new Insets(0, 0, 0, 0),
                                                      0,
                                                      0));
        infoPanel.add(this.clockType, new GridBagConstraints(1,
                                                             2,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        this.clockType.setEditable(false);
        this.clockType.setHorizontalAlignment(SwingConstants.CENTER);
        this.superMasterKeyValue.setEditable(false);
        this.superMasterKeyValue.setHorizontalAlignment(SwingConstants.CENTER);
        this.totalTimeValue.setEditable(false);
        this.totalTimeValue.setHorizontalAlignment(SwingConstants.CENTER);
        // END << totalTimeValue
        // END << infoPanel
        // START >> stateLabel
        final JLabel stateLabel = new JLabel();
        statePanel.add(stateLabel, new GridBagConstraints(0,
                                                          0,
                                                          1,
                                                          1,
                                                          0.0,
                                                          0.0,
                                                          GridBagConstraints.CENTER,
                                                          GridBagConstraints.BOTH,
                                                          new Insets(3, 15, 3, 15),
                                                          0,
                                                          0));
        stateLabel.setText("RUN CONTROL STATE");
        stateLabel.setHorizontalAlignment(SwingConstants.CENTER);
        stateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        // this.stateLabel.setPreferredSize(new java.awt.Dimension(100, 40));
        // this.stateLabel.setSize(100, 26);
        // END << stateLabel
        // START >> currentStateLabel
        statePanel.add(this.currentStateLabel, new GridBagConstraints(1,
                                                                      0,
                                                                      1,
                                                                      1,
                                                                      0.0,
                                                                      0.0,
                                                                      GridBagConstraints.CENTER,
                                                                      GridBagConstraints.BOTH,
                                                                      new Insets(3, 15, 3, 15),
                                                                      0,
                                                                      0));
        this.currentStateLabel.setOpaque(true);
        this.currentStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.currentStateLabel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        // currentStateLabel.setPreferredSize(new java.awt.Dimension(150, 40));
        // this.currentStateLabel.setSize(150, 26);
        // END << currentStateLabel
        // END << statePanel

        // Create a panel to contain the tabbed pane (border have problems with components other than JPanel and JLabel)
        final JPanel container = new JPanel(new BorderLayout(0, 5));
        container.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED),
                                                             "Run Information & Settings",
                                                             TitledBorder.LEADING,
                                                             TitledBorder.TOP,
                                                             new java.awt.Font("Dialog", 2, 12)));
        container.add(this.infoPane, BorderLayout.CENTER);
        // container.setPreferredSize(new java.awt.Dimension(300, 250));
        container.setMinimumSize(new Dimension(0, 230));

        // Simple panel containing the run number label and value
        final JPanel rnPanel = new JPanel();
        final GridBagLayout rnPanelLayout = new GridBagLayout();
        rnPanelLayout.columnWidths = new int[] {126, 7};
        rnPanelLayout.rowHeights = new int[] {10};
        rnPanelLayout.columnWeights = new double[] {0.0, 0.1};
        rnPanelLayout.rowWeights = new double[] {0.1};
        rnPanel.setLayout(rnPanelLayout);
        rnPanel.add(new JLabel("Run number"), new GridBagConstraints(0,
                                                                     0,
                                                                     1,
                                                                     1,
                                                                     0.0,
                                                                     0.0,
                                                                     GridBagConstraints.CENTER,
                                                                     GridBagConstraints.HORIZONTAL,
                                                                     new Insets(0, 0, 0, 0),
                                                                     0,
                                                                     0));
        rnPanel.add(this.runNumberValue, new GridBagConstraints(1,
                                                                0,
                                                                1,
                                                                1,
                                                                0.0,
                                                                0.0,
                                                                GridBagConstraints.CENTER,
                                                                GridBagConstraints.HORIZONTAL,
                                                                new Insets(0, 0, 0, 0),
                                                                0,
                                                                0));
        this.runNumberValue.setHorizontalAlignment(SwingConstants.CENTER);
        this.runNumberValue.setEditable(false);
        this.runNumberValue.setBorder(null);
        this.runNumberValue.setForeground(Color.RED);
        this.runNumberValue.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
        container.add(rnPanel, BorderLayout.NORTH);

        this.infoPane.setTabPlacement(SwingConstants.BOTTOM);
        this.infoPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        this.infoPane.addTab("Information", infoPanel);

        final JPanel ratesPanel = new JPanel();
        this.infoPane.addTab("Counters", null, ratesPanel, null);

        final GridBagLayout ratesPanelLayout = new GridBagLayout();
        ratesPanelLayout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.1, 0.1};
        ratesPanelLayout.rowHeights = new int[] {20, 7, 7, 7, 7};
        ratesPanelLayout.columnWeights = new double[] {0.0, 0.0, 0.0};
        ratesPanelLayout.columnWidths = new int[] {89, 70, 50};
        ratesPanel.setLayout(ratesPanelLayout);

        final JLabel l1Label = new JLabel();
        ratesPanel.add(l1Label, new GridBagConstraints(0,
                                                       2,
                                                       1,
                                                       1,
                                                       0.0,
                                                       0.0,
                                                       GridBagConstraints.CENTER,
                                                       GridBagConstraints.HORIZONTAL,
                                                       new Insets(0, 0, 0, 0),
                                                       0,
                                                       0));
        l1Label.setText("Level 1");

        final JLabel hltLabel = new JLabel();
        ratesPanel.add(hltLabel, new GridBagConstraints(0,
                                                        3,
                                                        1,
                                                        1,
                                                        0.0,
                                                        0.0,
                                                        GridBagConstraints.CENTER,
                                                        GridBagConstraints.HORIZONTAL,
                                                        new Insets(0, 0, 0, 0),
                                                        0,
                                                        0));
        hltLabel.setText("HLT");

        final JLabel recLabel = new JLabel();
        ratesPanel.add(recLabel, new GridBagConstraints(0,
                                                        4,
                                                        1,
                                                        1,
                                                        0.0,
                                                        0.0,
                                                        GridBagConstraints.CENTER,
                                                        GridBagConstraints.HORIZONTAL,
                                                        new Insets(0, 0, 0, 0),
                                                        0,
                                                        0));
        recLabel.setText("Recorded");

        final JLabel counterLabel = new JLabel();
        ratesPanel.add(counterLabel, new GridBagConstraints(1,
                                                            1,
                                                            1,
                                                            1,
                                                            0.0,
                                                            0.0,
                                                            GridBagConstraints.CENTER,
                                                            GridBagConstraints.NONE,
                                                            new Insets(0, 0, 0, 0),
                                                            0,
                                                            0));
        counterLabel.setText("Number");

        final JLabel rateLabel = new JLabel();
        ratesPanel.add(rateLabel, new GridBagConstraints(2,
                                                         1,
                                                         1,
                                                         1,
                                                         0.0,
                                                         0.0,
                                                         GridBagConstraints.CENTER,
                                                         GridBagConstraints.NONE,
                                                         new Insets(0, 0, 0, 0),
                                                         0,
                                                         0));
        rateLabel.setText("Rate");

        // START >> ratesPanel
        // START >> runSettingsPanel
        final JPanel runSettingsPanel = new JPanel();
        final GridBagLayout runSettingsPanelLayout = new GridBagLayout();
        this.infoPane.addTab("Settings", null, runSettingsPanel, null);

        runSettingsPanelLayout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
        runSettingsPanelLayout.rowHeights = new int[] {7, 7, 7, 7, 7, 7, 7, 7};
        runSettingsPanelLayout.columnWeights = new double[] {0.0, 0.1};
        runSettingsPanelLayout.columnWidths = new int[] {127, 7};
        runSettingsPanel.setLayout(runSettingsPanelLayout);
        // START >> maxEvLabel
        final JLabel maxEvLabel = new JLabel();
        runSettingsPanel.add(maxEvLabel, new GridBagConstraints(0,
                                                                0,
                                                                1,
                                                                1,
                                                                0.0,
                                                                0.0,
                                                                GridBagConstraints.CENTER,
                                                                GridBagConstraints.HORIZONTAL,
                                                                new Insets(0, 0, 0, 0),
                                                                0,
                                                                0));
        maxEvLabel.setText("Max Events");
        // END << maxEvLabel
        // START >> rtLabel
        final JLabel rtLabel = new JLabel();
        runSettingsPanel.add(rtLabel, new GridBagConstraints(0,
                                                             1,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        rtLabel.setText("Run Type");
        // END << rtLabel
        // START >> btLabel
        final JLabel btLabel = new JLabel();
        runSettingsPanel.add(btLabel, new GridBagConstraints(0,
                                                             2,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        btLabel.setText("Beam Type");
        // END << btLabel
        // START >> beLabel
        final JLabel beLabel = new JLabel();
        runSettingsPanel.add(beLabel, new GridBagConstraints(0,
                                                             3,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        beLabel.setText("Beam Energy (GeV)");
        // END << beLabel
        // START >> t0Label
        final JLabel t0Label = new JLabel();
        runSettingsPanel.add(t0Label, new GridBagConstraints(0,
                                                             4,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        t0Label.setText("Tier0 Project Name");
        // END << t0Label
        // START >> fnLabel
        final JLabel fnLabel = new JLabel();
        runSettingsPanel.add(fnLabel, new GridBagConstraints(0,
                                                             5,
                                                             1,
                                                             1,
                                                             0.0,
                                                             0.0,
                                                             GridBagConstraints.CENTER,
                                                             GridBagConstraints.HORIZONTAL,
                                                             new Insets(0, 0, 0, 0),
                                                             0,
                                                             0));
        fnLabel.setText("File Name Tag");
        // END << fnLabel
        // START >> recEnabledLabel
        final JLabel recEnabledLabel = new JLabel();
        runSettingsPanel.add(recEnabledLabel, new GridBagConstraints(0,
                                                                     6,
                                                                     1,
                                                                     1,
                                                                     0.0,
                                                                     0.0,
                                                                     GridBagConstraints.CENTER,
                                                                     GridBagConstraints.HORIZONTAL,
                                                                     new Insets(0, 0, 0, 0),
                                                                     0,
                                                                     0));
        recEnabledLabel.setText("Recording");
        // END << recEnabledLabel
        // START >> okButton
        runSettingsPanel.add(this.okButton, new GridBagConstraints(0,
                                                                   7,
                                                                   2,
                                                                   1,
                                                                   0.0,
                                                                   0.0,
                                                                   GridBagConstraints.CENTER,
                                                                   GridBagConstraints.NONE,
                                                                   new Insets(0, 0, 0, 0),
                                                                   0,
                                                                   0));
        this.okButton.setText("Set Values");
        this.okButton.setSize(98, 10);
        this.okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                MainPanel.this.updateRunSettings();
            }
        });
        // END << okButton
        // START >> maxEvTextField
        runSettingsPanel.add(this.maxEvTextField, new GridBagConstraints(1,
                                                                         0,
                                                                         1,
                                                                         1,
                                                                         0.0,
                                                                         0.0,
                                                                         GridBagConstraints.CENTER,
                                                                         GridBagConstraints.HORIZONTAL,
                                                                         new Insets(0, 0, 0, 0),
                                                                         0,
                                                                         0));
        this.maxEvTextField.setHorizontalAlignment(SwingConstants.TRAILING);
        this.maxEvTextField.addKeyListener(this.rsListener);
        // END << maxEvTextField
        // START >> beTextField
        runSettingsPanel.add(this.beTextField, new GridBagConstraints(1,
                                                                      3,
                                                                      1,
                                                                      1,
                                                                      0.0,
                                                                      0.0,
                                                                      GridBagConstraints.CENTER,
                                                                      GridBagConstraints.HORIZONTAL,
                                                                      new Insets(0, 0, 0, 0),
                                                                      0,
                                                                      0));
        this.beTextField.setHorizontalAlignment(SwingConstants.TRAILING);
        this.beTextField.addKeyListener(this.rsListener);
        // END << beTextField
        // START >> fnTextField
        runSettingsPanel.add(this.fnTextField, new GridBagConstraints(1,
                                                                      5,
                                                                      1,
                                                                      1,
                                                                      0.0,
                                                                      0.0,
                                                                      GridBagConstraints.CENTER,
                                                                      GridBagConstraints.HORIZONTAL,
                                                                      new Insets(0, 0, 0, 0),
                                                                      0,
                                                                      0));
        this.fnTextField.setHorizontalAlignment(SwingConstants.TRAILING);
        this.fnTextField.addKeyListener(this.rsListener);
        // END << fnTextField
        // START >> rtComboBox
        runSettingsPanel.add(this.rtComboBox, new GridBagConstraints(1,
                                                                     1,
                                                                     1,
                                                                     1,
                                                                     0.0,
                                                                     0.0,
                                                                     GridBagConstraints.CENTER,
                                                                     GridBagConstraints.HORIZONTAL,
                                                                     new Insets(0, 0, 0, 0),
                                                                     0,
                                                                     0));
        this.rtComboBox.setPreferredSize(new java.awt.Dimension(32, 19));
        this.rtComboBox.addPopupMenuListener(this.rsListener);
        // END << rtComboBox
        // START >> btComboBox
        runSettingsPanel.add(this.btComboBox, new GridBagConstraints(1,
                                                                     2,
                                                                     1,
                                                                     1,
                                                                     0.0,
                                                                     0.0,
                                                                     GridBagConstraints.CENTER,
                                                                     GridBagConstraints.HORIZONTAL,
                                                                     new Insets(0, 0, 0, 0),
                                                                     0,
                                                                     0));
        this.btComboBox.setPreferredSize(new java.awt.Dimension(32, 19));
        this.btComboBox.addPopupMenuListener(this.rsListener);
        // END << btComboBox
        // START >> t0ComboBox
        runSettingsPanel.add(this.t0ComboBox, new GridBagConstraints(1,
                                                                     4,
                                                                     1,
                                                                     1,
                                                                     0.0,
                                                                     0.0,
                                                                     GridBagConstraints.CENTER,
                                                                     GridBagConstraints.HORIZONTAL,
                                                                     new Insets(0, 0, 0, 0),
                                                                     0,
                                                                     0));
        this.t0ComboBox.setPreferredSize(new java.awt.Dimension(32, 19));
        this.t0ComboBox.setEditable(false);
        this.t0ComboBox.addPopupMenuListener(this.rsListener);
        // END << t0ComboBox
        // START >> recPanel
        final JPanel recPanel = new JPanel();
        final GridLayout recPanelLayout = new GridLayout(1, 1);
        recPanelLayout.setColumns(1);
        recPanelLayout.setHgap(5);
        recPanelLayout.setVgap(5);
        recPanel.setLayout(recPanelLayout);
        runSettingsPanel.add(recPanel, new GridBagConstraints(1,
                                                              6,
                                                              1,
                                                              1,
                                                              0.0,
                                                              0.0,
                                                              GridBagConstraints.CENTER,
                                                              GridBagConstraints.HORIZONTAL,
                                                              new Insets(0, 0, 0, 0),
                                                              0,
                                                              0));
        // START >> recEnableRadioButton
        final ButtonGroup recButtonGroup = new ButtonGroup();
        recPanel.add(this.recEnableRadioButton);
        this.recEnableRadioButton.addActionListener(this.rsListener);
        this.recEnableRadioButton.setText("Enabled");
        this.recEnableRadioButton.setPreferredSize(new java.awt.Dimension(21, 17));
        recButtonGroup.add(this.recEnableRadioButton);
        // END << recEnableRadioButton
        // START >> recDisableRadioButton
        recPanel.add(this.recDisableRadioButton);
        this.recDisableRadioButton.addActionListener(this.rsListener);
        this.recDisableRadioButton.setText("Disabled");
        this.recDisableRadioButton.setPreferredSize(new java.awt.Dimension(21, 17));
        recButtonGroup.add(this.recDisableRadioButton);
        // END << recDisableRadioButton
        // END << recPanel
        // END << runSettingsPanel
        // START >> l1Counter
        ratesPanel.add(this.l1Counter, new GridBagConstraints(1,
                                                              2,
                                                              1,
                                                              1,
                                                              0.6,
                                                              0.0,
                                                              GridBagConstraints.CENTER,
                                                              GridBagConstraints.HORIZONTAL,
                                                              new Insets(0, 0, 0, 3),
                                                              0,
                                                              0));
        this.l1Counter.setEditable(false);
        this.l1Counter.setHorizontalAlignment(SwingConstants.RIGHT);
        // END << l1Counter
        // START >> hltCounter
        ratesPanel.add(this.hltCounter, new GridBagConstraints(1,
                                                               3,
                                                               1,
                                                               1,
                                                               0.6,
                                                               0.0,
                                                               GridBagConstraints.CENTER,
                                                               GridBagConstraints.HORIZONTAL,
                                                               new Insets(0, 0, 0, 3),
                                                               0,
                                                               0));
        this.hltCounter.setEditable(false);
        this.hltCounter.setHorizontalAlignment(SwingConstants.RIGHT);
        // END << hltCounter
        // START >> recCounter
        ratesPanel.add(this.recCounter, new GridBagConstraints(1,
                                                               4,
                                                               1,
                                                               1,
                                                               0.6,
                                                               0.0,
                                                               GridBagConstraints.CENTER,
                                                               GridBagConstraints.HORIZONTAL,
                                                               new Insets(0, 0, 0, 2),
                                                               0,
                                                               0));
        this.recCounter.setEditable(false);
        this.recCounter.setHorizontalAlignment(SwingConstants.RIGHT);
        // END << recCounter
        // START >> l1Rate
        ratesPanel.add(this.l1Rate, new GridBagConstraints(2,
                                                           2,
                                                           GridBagConstraints.REMAINDER,
                                                           1,
                                                           0.4,
                                                           0.0,
                                                           GridBagConstraints.CENTER,
                                                           GridBagConstraints.HORIZONTAL,
                                                           new Insets(0, 3, 0, 0),
                                                           0,
                                                           0));
        this.l1Rate.setEditable(false);
        this.l1Rate.setHorizontalAlignment(SwingConstants.RIGHT);
        // END << l1Rate
        // START >> hltRate
        ratesPanel.add(this.hltRate, new GridBagConstraints(2,
                                                            3,
                                                            GridBagConstraints.REMAINDER,
                                                            1,
                                                            0.4,
                                                            0.0,
                                                            GridBagConstraints.CENTER,
                                                            GridBagConstraints.HORIZONTAL,
                                                            new Insets(0, 3, 0, 0),
                                                            0,
                                                            0));
        this.hltRate.setEditable(false);
        this.hltRate.setHorizontalAlignment(SwingConstants.RIGHT);
        // END << hltRate
        // START >> recRate
        ratesPanel.add(this.recRate, new GridBagConstraints(2,
                                                            4,
                                                            GridBagConstraints.REMAINDER,
                                                            1,
                                                            0.4,
                                                            0.0,
                                                            GridBagConstraints.CENTER,
                                                            GridBagConstraints.HORIZONTAL,
                                                            new Insets(0, 3, 0, 0),
                                                            0,
                                                            0));

        ratesPanel.add(this.lumiBlockValue, new GridBagConstraints(1,
                                                                   0,
                                                                   2,
                                                                   1,
                                                                   0.0,
                                                                   0.0,
                                                                   GridBagConstraints.CENTER,
                                                                   GridBagConstraints.HORIZONTAL,
                                                                   new Insets(0, 0, 0, 0),
                                                                   0,
                                                                   0));
        this.lumiBlockValue.setEditable(false);
        this.lumiBlockValue.setHorizontalAlignment(SwingConstants.CENTER);

        final JLabel lumiLabel = new JLabel("Lumi Block");
        ratesPanel.add(lumiLabel, new GridBagConstraints(0,
                                                         0,
                                                         1,
                                                         1,
                                                         0.0,
                                                         0.0,
                                                         GridBagConstraints.CENTER,
                                                         GridBagConstraints.HORIZONTAL,
                                                         new Insets(0, 0, 0, 0),
                                                         0,
                                                         0));
        this.recRate.setEditable(false);
        this.recRate.setHorizontalAlignment(SwingConstants.RIGHT);
        // END << recRate
        // END << ratesPanel

        // The autopilot/beam/r4p status panel
        final JPanel statusPanel = new JPanel(true);
        final BoxLayout wsPanelLayout = new BoxLayout(statusPanel, BoxLayout.X_AXIS);
        statusPanel.setLayout(wsPanelLayout);

        this.autopilotStatusLabel.setPreferredSize(new Dimension(25, 25));
        this.autopilotStatusLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.autopilotStatusLabel.setVerticalAlignment(SwingConstants.CENTER);
        this.autopilotStatusLabel.setToolTipText("Auto-pilot is DISABLED");
        
        this.beamStatusLabel.setPreferredSize(new Dimension(25, 25));
        this.beamStatusLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.beamStatusLabel.setVerticalAlignment(SwingConstants.CENTER);
        this.beamStatusLabel.setToolTipText("UNKNOWN");

        this.r4pStatusLabel.setPreferredSize(new Dimension(25, 25));
        this.r4pStatusLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.r4pStatusLabel.setVerticalAlignment(SwingConstants.CENTER);
        this.r4pStatusLabel.setToolTipText("Ready for Physics status is UNKNOWN");        
        
        final JideSplitButton bbb = new JideSplitButton("Auto Pilot");
        bbb.setAlwaysDropdown(true);
        bbb.setButtonStyle(ButtonStyle.FLAT_STYLE);        
        bbb.add(this.autopilotOnButton);
        bbb.add(this.autopilotOffButton);
        
        final ButtonGroup bg = new ButtonGroup();
        bg.add(this.autopilotOnButton);
        bg.add(this.autopilotOffButton);        
        
        this.autopilotOffButton.setSelected(true);
        
        this.autopilotOffButton.addActionListener(this.autoPilotSetter);
        this.autopilotOnButton.addActionListener(this.autoPilotSetter);
        
        statusPanel.add(Box.createHorizontalGlue());
        statusPanel.add(bbb);
        statusPanel.add(this.autopilotStatusLabel);
        statusPanel.add(Box.createHorizontalGlue());
        statusPanel.add(new JLabel("Stable Beams"));
        statusPanel.add(this.beamStatusLabel);
        statusPanel.add(Box.createHorizontalGlue());
        statusPanel.add(new JLabel("R4P"));
        statusPanel.add(this.r4pStatusLabel);
        statusPanel.add(Box.createHorizontalGlue());

        final JPanel commandPanel = new JPanel(new BorderLayout());
        commandPanel.add(rcTab, BorderLayout.CENTER);
        commandPanel.add(statusPanel, BorderLayout.SOUTH);

        final JSplitPane spl = new JSplitPane(JSplitPane.VERTICAL_SPLIT, false, commandPanel, container);
        spl.setDividerSize(5);
        spl.setDividerLocation(-1);
        spl.setResizeWeight(0.5D);
        this.add(statePanel, BorderLayout.NORTH);
        this.add(spl, BorderLayout.CENTER);
    }
}
