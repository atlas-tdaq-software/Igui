package Igui;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Class implementing simple methods to create thread pool executors.
 * <p>
 * The created executors have a fixed number of working threads and are backed up by an unlimited queue. All the working threads have a
 * timeout of 60 seconds and are started with normal priority. An uncaught exception handler is installed showing an error message (with the
 * exception content) any time that an uncaught exception is detected.
 * 
 * @see java.util.concurrent.ThreadPoolExecutor
 */
final class ExecutorFactory {

    /**
     * Timeout for working threads.
     */
    private final static int executorTimeout = 60;

    /**
     * Interface to compare two {@link ComparableTask} tasks.
     * <p>
     * Tasks are compared taking into account their priority.
     */
    private interface ComparableTask extends Comparable<ComparableTask> {
        final static int DEFAULT_PRIORITY = 0;

        public int getPriority();
    }

    /**
     * Abstract class implementing a {@link Runnable} with a given given priority.
     * <p>
     * Subclasses need to implement the {@link Runnable#run()} method.
     * 
     * @see PriorityThreadPoolExecutor
     */
    abstract static class PriorityRunnable implements Runnable, ComparableTask {
        private final Integer priority;

        /**
         * It creates a {@link PriorityRunnable} with the default priority.
         */
        PriorityRunnable() {
            this.priority = Integer.valueOf(ComparableTask.DEFAULT_PRIORITY);
        }

        /**
         * It creates a {@link PriorityRunnable} with priority set to <code>p</code>.
         * 
         * @param p The priority value.
         */
        PriorityRunnable(final Integer p) {
            this.priority = p;
        }

        @Override
        public int getPriority() {
            return this.priority.intValue();
        }

        @Override
        public int compareTo(final ComparableTask o) {
            return this.priority.compareTo(Integer.valueOf(o.getPriority()));
        }
    }

    /**
     * Abstract class implementing a {@link Callable} with a given priority.
     * <p>
     * Subclasses need to implement the {@link Callable#call()} method.
     * 
     * @see PriorityThreadPoolExecutor
     */
    abstract static class PriorityCallable<V> implements Callable<V>, ComparableTask {
        private final Integer priority;

        /**
         * It creates a {@link PriorityCallable} with default priority.
         */
        PriorityCallable() {
            this.priority = Integer.valueOf(ComparableTask.DEFAULT_PRIORITY);
        }

        /**
         * It creates a {@link PriorityCallable} having priority equal to <code>p</code>.
         * 
         * @param p The value of priority.
         */
        PriorityCallable(final Integer p) {
            this.priority = p;
        }

        @Override
        public int getPriority() {
            return this.priority.intValue();
        }

        @Override
        public int compareTo(final ComparableTask o) {
            return this.priority.compareTo(Integer.valueOf(o.getPriority()));
        }
    }

    /**
     * Class extending {@link ThreadPoolExecutor} to schedule tasks taking into account their priority.
     * <p>
     * To do that this executor uses a {@link PriorityBlockingQueue} as task queue, so that tasks are enqueued according to their priority.
     * <p>
     * It is possible to submit jobs to the executor both using the "execute" and "submit" methods (note that some "submit" method may throw
     * and {@link UnsupportedOperationException}.
     * 
     * @see PriorityThreadPoolExecutor#execute(PriorityRunnable)
     * @see PriorityThreadPoolExecutor#execute(Runnable)
     * @see PriorityThreadPoolExecutor#submit(Callable)
     * @see PriorityThreadPoolExecutor#submit(PriorityCallable)
     * @see PriorityThreadPoolExecutor#submit(PriorityRunnable)
     * @see PriorityThreadPoolExecutor#submit(Runnable)
     * @see PriorityThreadPoolExecutor#submit(PriorityRunnable, Object)
     * @see PriorityThreadPoolExecutor#submit(Runnable, Object)
     */
    final static class PriorityThreadPoolExecutor extends ThreadPoolExecutor {

        /**
         * Class extending {@link FutureTask} and implementing the {@link ComparableTask} interface to assign a priority to the task.
         * 
         * @see PriorityThreadPoolExecutor#newTaskFor(Callable)
         * @see PriorityThreadPoolExecutor#newTaskFor(Runnable, Object)
         */
        private static class PriorityFutureTask<V> extends FutureTask<V> implements ComparableTask {
            private final Integer priority;

            PriorityFutureTask(final PriorityCallable<V> callable) {
                super(callable);
                this.priority = Integer.valueOf(callable.getPriority());
            }

            PriorityFutureTask(final PriorityRunnable runnable, final V result) {
                super(runnable, result);
                this.priority = Integer.valueOf(runnable.getPriority());
            }

            PriorityFutureTask(final Callable<V> callable) {
                super(callable);
                this.priority = Integer.valueOf(ComparableTask.DEFAULT_PRIORITY);
            }

            PriorityFutureTask(final Runnable runnable, final V result) {
                super(runnable, result);
                this.priority = Integer.valueOf(ComparableTask.DEFAULT_PRIORITY);
            }

            @Override
            public int getPriority() {
                return this.priority.intValue();
            }

            @Override
            public int compareTo(final ComparableTask o) {
                return this.priority.compareTo(Integer.valueOf(o.getPriority()));
            }

            @Override
            public boolean equals(final Object obj) {
                boolean returnValue = false;
                if(ComparableTask.class.isInstance(obj)) {
                    returnValue = this.compareTo((ComparableTask) obj) == 0 ? true : false;
                }
                return returnValue;
            }

            @Override
            public int hashCode() {
                return this.priority.hashCode();
            }
        }

        /**
         * It uses a {@link PriorityBlockingQueue} to enqueue the tasks.
         * 
         * @see ThreadPoolExecutor#ThreadPoolExecutor(int, int, long, TimeUnit, java.util.concurrent.BlockingQueue,
         *      RejectedExecutionHandler)
         */
        PriorityThreadPoolExecutor(final int corePoolSize,
                                   final int maximumPoolSize,
                                   final long keepAliveTime,
                                   final TimeUnit unit,
                                   final RejectedExecutionHandler handler)
        {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new PriorityBlockingQueue<Runnable>(), handler);
        }

        /**
         * It uses a {@link PriorityBlockingQueue} to enqueue the tasks.
         * 
         * @see ThreadPoolExecutor#ThreadPoolExecutor(int, int, long, TimeUnit, java.util.concurrent.BlockingQueue, ThreadFactory,
         *      RejectedExecutionHandler)
         */
        PriorityThreadPoolExecutor(final int corePoolSize,
                                   final int maximumPoolSize,
                                   final long keepAliveTime,
                                   final TimeUnit unit,
                                   final ThreadFactory threadFactory,
                                   final RejectedExecutionHandler handler)
        {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new PriorityBlockingQueue<Runnable>(), threadFactory, handler);
        }

        /**
         * It uses a {@link PriorityBlockingQueue} to enqueue the tasks.
         * 
         * @see ThreadPoolExecutor#ThreadPoolExecutor(int, int, long, TimeUnit, java.util.concurrent.BlockingQueue, ThreadFactory)
         */
        PriorityThreadPoolExecutor(final int corePoolSize,
                                   final int maximumPoolSize,
                                   final long keepAliveTime,
                                   final TimeUnit unit,
                                   final ThreadFactory threadFactory)
        {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new PriorityBlockingQueue<Runnable>(), threadFactory);
        }

        /**
         * It uses a {@link PriorityBlockingQueue} to enqueue the tasks.
         * 
         * @see ThreadPoolExecutor#ThreadPoolExecutor(int, int, long, TimeUnit, java.util.concurrent.BlockingQueue)
         */
        PriorityThreadPoolExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new PriorityBlockingQueue<Runnable>());
        }

        /**
         * Here is the key trick to let this executor work properly. The <code>callable</code> {@link Callable} is casted to a
         * {@link PriorityCallable} and a {@link PriorityFutureTask} is returned.
         * <p>
         * This implies that <code>callable</code> has always to be a {@link PriorityCallable} and makes impossible to use the "submit"
         * methods with simple {@link Callable}s.
         * 
         * @see java.util.concurrent.AbstractExecutorService#newTaskFor(java.util.concurrent.Callable)
         */
        @Override
        protected <T> RunnableFuture<T> newTaskFor(final Callable<T> callable) {
            return new PriorityFutureTask<T>((PriorityCallable<T>) callable);
        }

        /**
         * Here is the key trick to let this executor work properly. The <code>runnable</code> {@link Runnable} is casted to a
         * {@link PriorityRunnable} and a {@link PriorityFutureTask} is returned.
         * <p>
         * This implies that <code>runnable</code> has always to be a {@link PriorityRunnable} and makes impossible to use the "submit"
         * methods with simple {@link Runnable}s.
         * 
         * @see java.util.concurrent.AbstractExecutorService#newTaskFor(java.lang.Runnable, java.lang.Object)
         */
        @Override
        protected <T> RunnableFuture<T> newTaskFor(final Runnable runnable, final T value) {
            return new PriorityFutureTask<T>((PriorityRunnable) runnable, value);
        }

        /**
         * It actually creates a {@link PriorityRunnable} from <code>command</code>. The {@link Runnable} will be queued with default
         * priority.
         * 
         * @see java.util.concurrent.ThreadPoolExecutor#execute(java.lang.Runnable)
         */
        @Override
        public void execute(final Runnable command) {
            super.execute(new PriorityRunnable() {
                @Override
                public void run() {
                    command.run();
                }
            });
        }

        /**
         * @see #execute(Runnable)
         */
        public void execute(final PriorityRunnable command) {
            super.execute(command);
        }

        /**
         * Not supported.
         * 
         * @throws UnsupportedOperationException
         */
        @Override
        public <T> Future<T> submit(final Callable<T> task) {
            throw new UnsupportedOperationException();
        }

        /**
         * @see ThreadPoolExecutor#submit(Callable)
         */
        public <T> Future<T> submit(final PriorityCallable<T> task) {
            return super.submit(task);
        }

        /**
         * Not supported.
         * 
         * @throws UnsupportedOperationException
         */
        @Override
        public <T> Future<T> submit(final Runnable task, final T result) {
            throw new UnsupportedOperationException();
        }

        /**
         * @see ThreadPoolExecutor#submit(Runnable, Object)
         */
        public <T> Future<T> submit(final PriorityRunnable task, final T result) {
            return super.submit(task, result);
        }

        /**
         * Not supported.
         * 
         * @throws UnsupportedOperationException
         */
        @Override
        public Future<?> submit(final Runnable task) {
            throw new UnsupportedOperationException();
        }

        /**
         * @see ThreadPoolExecutor#submit(Runnable)
         */
        public Future<?> submit(final PriorityRunnable task) {
            return super.submit(task);
        }
    }

    /**
     * ThreadFactory used by {@link ThreadPoolExecutor} and {@link PriorityThreadPoolExecutor}. It takes care of installing the uncaught
     * exception handler and naming the working threads in a proper way (useful for debugging purposes).
     */
    private final static class ExecutorThreadFactory implements ThreadFactory {

        /**
         * Thread number counter. It is incremented any time this ThreadFactory starts a new thread.
         */
        private final AtomicInteger threadNumber = new AtomicInteger(0);

        /**
         * The working thread name prefix.
         */
        private final String threadName;

        /**
         * The constructor.
         * 
         * @param threadName Name used to identify working threads (i.e., name will be "pool-threadName-counter".
         */
        ExecutorThreadFactory(final String threadName) {
            this.threadName = "pool-" + threadName;
        }

        /**
         * The thread number counter is incremented, the uncaught exception handler is installed and the spawn thread priority is set to
         * normal.
         * 
         * @see java.util.concurrent.ThreadFactory#newThread(java.lang.Runnable)
         */
        @Override
        public Thread newThread(final Runnable r) {
            // Increment the thread number (it is used to identify the working threads)
            final int currentThreadId = this.threadNumber.getAndIncrement();

            // Create a new thread with an 'easy to identify' name
            final Thread t = new Thread(r, this.threadName.concat("-").concat(Integer.toString(currentThreadId)));

            // Set the thread exception handler
            t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                // Any uncaught exception is reported to the error stream and an error dialog is shown to alert the user.
                @Override
                public void uncaughtException(final Thread tr, final Throwable e) {
                    IguiLogger.error("Uncaught Exception in thread \"" + tr.getName() + "\": " + e.getMessage(), e);
                }
            });

            // Set the thread priority
            t.setPriority(Thread.NORM_PRIORITY);

            return t;
        }
    }

    /**
     * Empty constructor: all the user methods are static.
     */
    private ExecutorFactory() {
    }

    /**
     * This method creates an executor with a fixed number of working threads.
     * 
     * @param executorId String to identify the threads belonging to the created executor.
     * @param size Number of working threads.
     * @return The fresh created ThreadPoolExecutor.
     */
    static ThreadPoolExecutor createExecutor(final String executorId, final int size) {

        final ThreadPoolExecutor tp = new ThreadPoolExecutor(size,
                                                             size,
                                                             ExecutorFactory.executorTimeout,
                                                             TimeUnit.SECONDS,
                                                             new LinkedBlockingQueue<Runnable>(),
                                                             new ExecutorFactory.ExecutorThreadFactory(executorId));

        // Since all the threads are 'core' threads allow timeout for them
        tp.allowCoreThreadTimeOut(true);

        // Discard policy for rejected tasks: since we use an unbounded queue tasks are rejected only when the executor has been shut down
        tp.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());

        IguiLogger.debug("Created executor \"" + executorId + "\" with size " + size);

        return tp;
    }

    /**
     * This method creates an executor with a single working thread.
     * 
     * @param executorId String to identify the thread belonging to the created executor.
     * @return The fresh created ThreadPoolExecutor.
     */
    static ThreadPoolExecutor createSingleExecutor(final String executorId) {
        return ExecutorFactory.createExecutor(executorId, 1);
    }

    /**
     * It creates an executor with a fixed number of working threads where tasks are enqueued taking into account their priority
     * 
     * @param executorId String to identify the thread belonging to the created executor
     * @param size Number of working threads
     * @return A new {@link PriorityThreadPoolExecutor}
     */
    static PriorityThreadPoolExecutor createPriorityExecutor(final String executorId, final int size) {
        final PriorityThreadPoolExecutor tp = new PriorityThreadPoolExecutor(size,
                                                                             size,
                                                                             ExecutorFactory.executorTimeout,
                                                                             TimeUnit.SECONDS,
                                                                             new ExecutorFactory.ExecutorThreadFactory(executorId));
        // Since all the threads are 'core' threads allow timeout for them
        tp.allowCoreThreadTimeOut(true);

        // Discard policy for rejected tasks: since we use an unbounded queue tasks are rejected only when the executor has been shut down
        tp.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());

        IguiLogger.debug("Created priority executor \"" + executorId + "\" with size " + size);

        return tp;
    }

    /**
     * It creates an executor with a single working thread where tasks are enqueued taking into account their priority
     * 
     * @param executorId String to identify the thread belonging to the created executor
     * @return A new {@link PriorityThreadPoolExecutor}
     */
    static PriorityThreadPoolExecutor createSinglePriorityExecutor(final String executorId) {
        return ExecutorFactory.createPriorityExecutor(executorId, 1);
    }

    /**
     * It creates an executor with a fixed number of working threads that can schedule commands to run after a given delay, or to execute
     * periodically.
     * 
     * @param executorId String to identify the thread belonging to the created executor
     * @param size Number of working threads
     * @return A new {@link ScheduledThreadPoolExecutor}
     */
    static ScheduledThreadPoolExecutor createScheduledThreadPoolExecutor(final String executorId, final int size) {
        final ScheduledThreadPoolExecutor tp = new ScheduledThreadPoolExecutor(size,
                                                                               new ExecutorFactory.ExecutorThreadFactory(executorId),
                                                                               new ThreadPoolExecutor.DiscardPolicy());
        tp.setKeepAliveTime(ExecutorFactory.executorTimeout, TimeUnit.SECONDS);
        tp.allowCoreThreadTimeOut(true);
        return tp;
    }

    /**
     * It creates an executor with a single of working thread that can schedule commands to run after a given delay, or to execute
     * periodically.
     * 
     * @param executorId String to identify the thread belonging to the created executor
     * @return A new {@link ScheduledThreadPoolExecutor}
     */
    static ScheduledThreadPoolExecutor createSingleScheduledThreadPoolExecutor(final String executorId) {
        return ExecutorFactory.createScheduledThreadPoolExecutor(executorId, 1);
    }
}
