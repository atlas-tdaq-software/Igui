package Igui;

import java.io.File;
import java.lang.reflect.Array;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.parsers.FactoryConfigurationError;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.log4j.xml.DOMConfigurator;


/**
 * <p>
 * It offers static methods to report debug, information, warning and error messages via log4j. Debug and information messages are sent to
 * the standard output stream, while warning and error messages are sent to the standard error stream. This is made possible by using 2
 * log4j appends in the log4j xml config file. Debug messages are only shown if the <code>tdaq.igui.debug</code> property is set to
 * <code>true</code> (or the appropriate logging level is selected from the Igui menu).
 * <p>
 * The preferred and only fully supported way to use the Igui logging facility is through the previously mentioned static methods but, for
 * special and/or debugging purposes, the internally used Logger may be accessed using the
 * <code>Logger.getLogger(IguiLogger.getName())</code> call. The internal Logger usage is anyway strongly deprecated and may be removed in a
 * short time.
 * <p>
 * Some of the provided static methods accept a {@link Throwable} as an argument. That {@link Throwable} is intended to be the cause of the
 * messages and its stack trace is reported if the logging level is set to debug.
 */
public final class IguiLogger {
    /**
     * Reference to the {@link org.apache.log4j.Logger} used internally for message reporting
     */
    private final static Logger log4jLogger = IguiLogger.createLogger();

    /**
     * Reference to this class instance (singleton pattern)
     */
    private final static IguiLogger instance = new IguiLogger();

    /**
     * The name associated to this logger
     */
    private final static String loggerName = "Igui.IguiLogger";

    /**
     * Executor used
     */
    private final static ExecutorService logWriter = Executors.newSingleThreadExecutor(new ThreadFactory() {

        private final AtomicInteger threadNum = new AtomicInteger(0);
        private final ThreadFactory delegate = Executors.defaultThreadFactory();
        private final static String poolName = "pool-Igui-Logger";

        @Override
        public Thread newThread(final Runnable r) {
            final Thread tr = this.delegate.newThread(r);
            tr.setName(poolName + "-" + this.threadNum.getAndIncrement());
            tr.setPriority(Thread.MIN_PRIORITY);
            tr.setDaemon(true);
            return tr;
        }
    });

    /**
     * Common method that does the actual reporting via log4j. Used by all the IguiLogger logging methods.
     * 
     * @param reportLvl Level according to the report method used.
     * @param msg Actual message prefixed with extra info (see {@link #getMessageWithInfo(String)} )
     * @param ex The exception source of the reported message. null if there is no given source
     */
    private static void log4jLog(final Level reportLvl, final String msg, final Throwable ex) {

        IguiLogger.logWriter.execute(new Runnable() {
            @Override
            public void run() {
                final Level logLevel = IguiLogger.log4jLogger.getLevel();

                switch(reportLvl.toInt()) {
                    case Priority.DEBUG_INT:
                        if((ex == null) || ((logLevel != null) && (logLevel.toInt() > Priority.DEBUG_INT))) {
                            IguiLogger.log4jLogger.debug(msg);
                        } else {
                            IguiLogger.log4jLogger.debug(msg, ex);
                        }
                        break;
                    case Priority.INFO_INT:
                        if((ex == null) || ((logLevel != null) && (logLevel.toInt() > Priority.DEBUG_INT))) {
                            IguiLogger.log4jLogger.info(msg);
                        } else {
                            IguiLogger.log4jLogger.info(msg, ex);
                        }
                        break;
                    case Priority.WARN_INT:
                        if((ex == null) || ((logLevel != null) && (logLevel.toInt() > Priority.DEBUG_INT))) {
                            IguiLogger.log4jLogger.warn(msg);
                        } else {
                            IguiLogger.log4jLogger.warn(msg, ex);
                        }
                        break;
                    case Priority.ERROR_INT:
                        if((ex == null) || ((logLevel != null) && (logLevel.toInt() > Priority.DEBUG_INT))) {
                            IguiLogger.log4jLogger.error(msg);
                        } else {
                            IguiLogger.log4jLogger.error(msg, ex);
                        }
                        break;
                    default:
                        // Should never happen! Default to information print something just in case
                        if((ex == null) || ((logLevel != null) && (logLevel.toInt() > Priority.DEBUG_INT))) {
                            IguiLogger.log4jLogger.info(msg);
                        } else {
                            IguiLogger.log4jLogger.info(msg, ex);
                        }

                        break;
                }
            }
        });
    }

    /**
     * The constructor.
     */
    private IguiLogger() {
    }

    /**
     * It gives the reference to the IguiLogger object.
     * 
     * @return The reference to the IguiLogger object
     */
    static IguiLogger instance() {
        return IguiLogger.instance;
    }

    /**
     * It returns the name associated to the logger facility.
     * 
     * @return The name associated to the logger facility
     */
    public static String getName() {
        return IguiLogger.loggerName;
    }

    /**
     * It logs a debug message.
     * 
     * @param msg The debug message
     */
    public static void debug(final String msg) {
        IguiLogger.log4jLog(Level.DEBUG, IguiLogger.getMessageWithInfo(msg), null);
    }

    /**
     * It logs a debug message and a stack trace of the causing exception.
     * 
     * @param msg The debug message
     * @param ex The exception source of the debug message
     */
    public static void debug(final String msg, final Throwable ex) {
        IguiLogger.log4jLog(Level.DEBUG, IguiLogger.getMessageWithInfo(msg), ex);
    }

    /**
     * It logs an information message.
     * 
     * @param msg The information message
     */
    public static void info(final String msg) {
        IguiLogger.log4jLog(Level.INFO, IguiLogger.getMessageWithInfo(msg), null);
    }

    /**
     * It logs an information message and a stack trace of the causing exception.
     * 
     * @param msg The information message
     * @param ex The exception source of the information message
     */
    public static void info(final String msg, final Throwable ex) {
        IguiLogger.log4jLog(Level.INFO, IguiLogger.getMessageWithInfo(msg), ex);
    }

    /**
     * It logs an warning message.
     * 
     * @param msg The warning message
     */
    public static void warning(final String msg) {
        IguiLogger.log4jLog(Level.WARN, IguiLogger.getMessageWithInfo(msg), null);
    }

    /**
     * It logs a warning message and a stack trace of the causing exception.
     * 
     * @param msg The warning message
     * @param ex The exception source of the warning message
     */
    public static void warning(final String msg, final Throwable ex) {
        IguiLogger.log4jLog(Level.WARN, IguiLogger.getMessageWithInfo(msg), ex);
    }

    /**
     * It logs an error message.
     * 
     * @param msg The error message
     */
    public static void error(final String msg) {
        IguiLogger.log4jLog(Level.ERROR, IguiLogger.getMessageWithInfo(msg), null);
    }

    /**
     * It logs an error message and a stack trace of the causing exception.
     * 
     * @param msg The error message
     * @param ex The exception source of the error message
     */
    public static void error(final String msg, final Throwable ex) {
        IguiLogger.log4jLog(Level.ERROR, IguiLogger.getMessageWithInfo(msg), ex);
    }

    /**
     * Formats the original message by adding as a prefix the following info: <br>
     * Class name, method name, filename and line number
     * 
     * @param msg The actual message reported by the programmer
     * @return The same MSG message prefixed with the extra information
     */
    private static String getMessageWithInfo(final String msg) {
        final StackTraceElement el = IguiLogger.getStackElement();
        final StringBuilder str = new StringBuilder();

        str.append(" [");
        str.append(el.getClassName() + " at ");
        str.append(el.getMethodName() + "(...) in ");
        str.append(el.getFileName() + ":" + el.getLineNumber());
        str.append("]: " + msg);

        return str.toString();
    }

    /**
     * It gives the stack trace element corresponding to the piece of code creating the log event.
     * 
     * @return The stack trace element
     */
    private static StackTraceElement getStackElement() {
        final Thread thisThread = Thread.currentThread();
        final StackTraceElement[] els = thisThread.getStackTrace();
        final int stackTraceSize = Array.getLength(els);

        if(stackTraceSize > 4) {
            // 0 = call to fill the stack - java.lang.Thread.getStackTrace
            // 1 = call to get the stack trace elements - Igui.IguiLogger.getStackElement
            // 2 = call to add extra info (file, class, line etc) to the message reported - Igui.IguiLogger.getMessageWithInfo
            // 3 = call to the IguiLogger logging method - i.e., Igui.IguiLogger.debug
            // 4 = 'interesting' call
            return els[4];
        }

        return els[stackTraceSize - 1];
    }

    /**
     * Set a filter on the log level.
     * <p>
     * Messages with a log level less than <code>dbgLevel</code> will not be showed.
     * 
     * @param dbgLevel The new log filter level
     */
    void setDebugLevel(final Level dbgLevel) {
        IguiLogger.log4jLogger.setLevel(dbgLevel);
    }

    /**
     * It gets the current filter on the log level.
     * 
     * @return The current filter on the log level
     */
    Level getDebugLevel() {
        return IguiLogger.log4jLogger.getLevel();
    }

    /**
     * It creates and configures the Logger object internally used by this class.
     * 
     * @return The Logger instance
     */
    private static Logger createLogger() {
        final Logger logger = LogManager.getLogger(IguiLogger.loggerName);

        boolean confRead = false;

        final String confLocation = IguiConfiguration.instance().getLog4jConfFile();
        if((confLocation != null) && (confLocation.isEmpty() == false)) {
            final File confFile = new File(confLocation);
            if(confFile.canRead() == true) {
                try {
                    DOMConfigurator.configure(confFile.getAbsolutePath());
                    confRead = true;
                }
                catch(final FactoryConfigurationError ex) {
                    ex.printStackTrace();
                }
            }
        }

        if(confRead == false) {
            BasicConfigurator.configure();
        }

        logger.setLevel(Level.ALL);

        // Set the logger logging level
        if(Boolean.getBoolean(IguiConstants.IGUI_DEBUG_PROPERTY) == true) {
            logger.setLevel(Level.DEBUG);
        } else {
            logger.setLevel(Level.INFO);
        }

        return logger;
    }
}
