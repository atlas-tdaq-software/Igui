package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolTip;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.ToolTipManager;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.text.Position.Bias;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.jdesktop.swingx.JXTree;

import com.jidesoft.swing.PartialGradientLineBorder;
import com.jidesoft.swing.PartialSide;
import com.jidesoft.swing.SearchableBar;
import com.jidesoft.swing.TitledSeparator;
import com.jidesoft.swing.TreeSearchable;

import Igui.IguiException.ConfigException;
import Igui.IguiException.ISException;
import Igui.IguiException.IguiPanelError;
import Igui.RunControlApplicationData.ApplicationISStatus;
import Igui.RunControlFSM.State;
import Igui.Common.MultiLineToolTip;
import Igui.Common.TreeSelectionFilteredModel;
import daq.rc.Command;
import daq.rc.RCException;
import daq.rc.RCException.BadCommandException;
import rc.DAQApplicationInfo;
import rc.RCStateInfo;


/**
 * This panel contains the run control tree.
 * <p>
 * The tree structure is built taking information from the configuration database. Information about tree nodes is updated from IS
 * call-backs. For each segment all the applications are shown. Infrastructure applications are grouped under a special node. IS information
 * for each node are 'lazy' loaded the first time a node is expanded. The tree nodes are instances of the {@link RCTreeNode} class.
 */
final class RunControlTreePanel extends AbstractRunControlPanel {

    private static final long serialVersionUID = 5060784102328353201L;

    /**
     * This panel name.
     */
    private final static String name = "Run-Control-Tree-Panel";

    /**
     * Name of the "virtual" infrastructure node
     */
    private final static String infrNodeName = "Infrastructure";

    /**
     * Name of the "virtual" Online Segment node
     */
    private final static String onlSegNodeName = "Online Segment";

    /**
     * Hash map containing tree nodes
     */
    private final ConcurrentMap<String, RCTreeNode> nodeMap = new ConcurrentHashMap<>(3000, 0.75f, 25);

    /**
     * Thread local variable used to retrieve information from IS. Used to reduce the number of allocations of very short living objects.
     */
    private final static ThreadLocal<DAQApplicationInfo> DAQ_APP_INFO = new ThreadLocal<DAQApplicationInfo>() {
        @Override
        protected DAQApplicationInfo initialValue() {
            return new DAQApplicationInfo();
        }
    };

    /**
     * Thread local variable used to retrieve information from IS. Used to reduce the number of allocations of very short living objects.
     */
    private final static ThreadLocal<RCStateInfo> RC_STATE_INFO = new ThreadLocal<RCStateInfo>() {
        @Override
        protected RCStateInfo initialValue() {
            return new RCStateInfo();
        }
    };

    /**
     * Thread pool executor.
     * <p>
     * It is used to accomplish the following tasks:
     * <ul>
     * <li>Execute call-backs coming from the RunCtrl IS server;
     * <li>Lazy loading initial IS information for nodes;
     * <li>Getting node IS information any time a node is selected;
     * <li>Sending commands to controllers;
     * <li>Building the tree.
     * </ul>
     */
    private final ThreadPoolExecutor executor =
                                              ExecutorFactory.createExecutor(this.getPanelName(),
                                                                             Math.max(5,
                                                                                      (int) (1.5
                                                                                             * Runtime.getRuntime().availableProcessors())));

    /**
     * The run control tree model
     */
    // private final RCTreeFilteredModel treeModel = new RCTreeFilteredModel(null);
    private final DefaultTreeModel treeModel = new DefaultTreeModel(null);

    /**
     * The {@link TreeWillExpandListener} listener
     */
    private final RCTreeExpansionListener treeExpListener = new RCTreeExpansionListener();

    /**
     * The run control tree: the tree is made up of {@link RCTreeNode} nodes.
     */
    private final JXTree rcTree = new JXTree(this.treeModel) {
        private static final long serialVersionUID = -5913540320378526259L;

        @Override
        public JToolTip createToolTip() {
            final MultiLineToolTip tt = new MultiLineToolTip(true);
            tt.setComponent(this);
            return tt;
        }

        // This is overridden to use the tree search interface from Jide
        @Override
        public TreePath getNextMatch(final String prefix, final int startingRow, final Bias bias) {
            return null;
        }

        // Overridden because the JXTree implementation collapses only the visible rows
        @Override
        public void collapseAll() {
            final Object root = this.getModel().getRoot();
            final TreePath tp = new TreePath(root);

            // NOTE: the TreeWillExpandListener is removed before collapsing the tree
            // because that recursive action seems to fire a lot of 'treeWillExpandEvent';
            // in such a case there is the possibility to send several calls to IS not needed
            // at this stage
            this.removeTreeWillExpandListener(RunControlTreePanel.this.treeExpListener);
            this.collapseAll(tp);
            this.addTreeWillExpandListener(RunControlTreePanel.this.treeExpListener);
        }

        private void collapseAll(final TreePath tp) {
            final TreeNode lpc = (TreeNode) tp.getLastPathComponent();
            final int chNum = lpc.getChildCount();
            if(chNum > 0) {
                for(int i = 0; i < chNum; ++i) {
                    this.collapseAll(tp.pathByAddingChild(lpc.getChildAt(i)));
                }

                this.collapsePath(tp);
            }
        }
    };

    /**
     * The ISInfoReceiver instance to process rc IS call-backs
     */
    private final transient ISInfoReceiver runCtrlReceiver = new RunCtrlReceiver(this);

    /**
     * Boolean keeping trace of the searching bar installation
     * <p>
     * This variable should be volatile (or replaced by AtomicBoolean) if accessed from a thread different that the EDT (remember that the
     * constructor is granted to be executed in the EDT).
     */
    private boolean isSearchBarInstalled = false;

    /**
     * Abstract class used to find nodes in the tree matching a certain criteria.
     * <p>
     * This interface is different with respect to the one used to look for nodes in the tree by name. In principle that interface may be
     * extended for other searches, but this is made difficult by the fact that the node IS information is lazy loaded.
     * <p>
     * The implementing class has to override the {@link #matches(RCTreeNode)} method. The first node in the passed list for which that
     * method returns <code>true</code> will be automatically selected and expanded in the tree. This class extends from @see
     * {@link SwingWorker}, so implementing class must call {@link SwingWorker#execute()} on the created object to start the computation in
     * a background thread.
     */
    private abstract class NodeAttributeFinder extends SwingWorker<RCTreeNode, Void> {
        private final List<? extends RCTreeNode> searchList;
        private final RCTreeNode startNode;
        private final boolean recursive;

        /**
         * Constructor.
         * 
         * @param list The list of node that will be searched
         * @param startNode The node the search start at (it is a good idea to set this node to the currently selected one). If
         *            <code>list</code> contains <code>startNode</code> then it will be used to detect when the search is closed.
         * @param recursive <code>true</code> whether the search should be recursive (i.e., if a node matches the search criteria then the
         *            algorithm will go on looking for its children in a recursive way).
         */
        NodeAttributeFinder(final List<? extends RCTreeNode> list, final RCTreeNode startNode, final boolean recursive) {
            this.searchList = list;
            this.startNode = startNode;
            this.recursive = recursive;
        }

        @Override
        protected RCTreeNode doInBackground() throws InterruptedException {
            RCTreeNode nodeToReturn = null;

            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final IguiPanel p = RunControlTreePanel.this.getMainPanel();
                    if(p != null) {
                        p.setBusy(true);
                        p.setBusyMessage("Searching the RC tree...");
                    }
                }
            });

            // Divide the list into two pieces: it makes easier to implement a closed loop search
            // The list is divided into two parts: from index 0 to the node where the search is started (exclusive)
            // and from that node (exclusive) till the end of the list (inclusive)
            final int lastAppIndex = this.searchList.indexOf(this.startNode);
            final List<? extends RCTreeNode> lowList;
            final List<? extends RCTreeNode> highList;
            if(lastAppIndex != -1) {
                // In this case the starting node is part of the list (i.e., the starting node is a leaf node and the
                // list represents all other leaf nodes in that sub-tree)
                lowList = this.searchList.subList(0, lastAppIndex);
                highList = this.searchList.subList(lastAppIndex + 1, this.searchList.size());
            } else {
                // In this case the starting node is not part of the list (i.e., the starting node is a non leaf node
                // and the list represents its children)
                lowList = null;
                highList = this.searchList.subList(0, this.searchList.size());
            }

            // Start searching the high list
            nodeToReturn = this.findNode(highList);

            // Nothing found in the high list, try the low list
            if((nodeToReturn == null) && (lowList != null)) {
                nodeToReturn = this.findNode(lowList);
            }

            return nodeToReturn;
        }

        @Override
        protected void done() {
            try {
                // Get the node: if one has been found then select it
                final RCTreeNode foundNode = this.get();
                if(foundNode != null) {
                    final TreePath tp = new TreePath(foundNode.getPath());
                    RunControlTreePanel.this.rcTree.setSelectionPath(tp);
                    RunControlTreePanel.this.rcTree.scrollPathToVisible(tp);
                } else {
                    ErrorFrame.showInfo("No application has been found matching the search criteria");
                }
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();
                IguiLogger.warning("The EDT thread has been interrupted!", ex);
            }
            catch(final ExecutionException ex) {
                final String errMsg = "Error performing a search in the tree: " + ex.getCause();
                IguiLogger.error(errMsg, ex);
                ErrorFrame.showError("IGUI - RC Tree Error", errMsg, ex);
            }
            finally {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        final IguiPanel p = RunControlTreePanel.this.getMainPanel();
                        if(p != null) {
                            p.setBusy(false);
                            p.setBusyMessage("");
                        }
                    }
                });
            }
        }

        /**
         * Method to be implemented by the derived classes.
         * 
         * @param node A node in the list of node to search
         * @return <code>true</code> if the node fulfills the search criteria
         */
        protected abstract boolean matches(final RCTreeNode node);

        /**
         * It looks in the list for a node matching the search criteria.
         * 
         * @param l The list of nodes
         * @return The node matching the search criteria (see {@link #matches(RCTreeNode)}) or <code>null</code> if such a node is not
         *         found.
         * @throws InterruptedException
         */
        private RCTreeNode findNode(final List<? extends RCTreeNode> l) throws InterruptedException {
            RCTreeNode foundNode = null;

            // If the IS information for a node has not been loaded yet, then submit a task to the executor
            // The idea is to submit the tasks and then get the result only if needed
            final Map<RCTreeNode, Future<RCTreeNode>> futureMap = new HashMap<RCTreeNode, Future<RCTreeNode>>();
            for(final RCTreeNode n : l) {
                if(n.isInfoLoaded() == false) {
                    final Future<RCTreeNode> f = RunControlTreePanel.this.executor.submit(new ISNodeInfoWorker(n), n);
                    futureMap.put(n, f);
                }
            }

            // Loop over the list and find the first matching node
            for(final RCTreeNode n : l) {
                if(n.isInfoLoaded() == false) {
                    // IS info not loaded yet: retrieve the Future from the map and wait
                    final Future<RCTreeNode> f = futureMap.get(n);
                    if(f != null) {
                        try {
                            f.get();
                        }
                        catch(final ExecutionException ex) {
                            IguiLogger.warning(ex.toString(), ex);
                        }
                    }
                }

                if(this.matches(n) == true) {
                    // If the search is recursive then look for node children
                    if((this.recursive == true) && (n.isLeaf() == false)) {
                        final List<RCTreeNode> childList = n.getChildren();

                        // Recursively go through the node children
                        final RCTreeNode nn = this.findNode(childList);
                        if(nn != null) {
                            // A matching child node has been found
                            foundNode = nn;
                        } else {
                            // If no new node has been found then stay with the parent
                            foundNode = n;
                        }
                    } else {
                        foundNode = n;
                    }

                    break;
                }
            }

            return foundNode;
        }
    }

    /**
     * ISInfoReceiver for processing IS call-backs from the RunCtrl IS server.
     * <p>
     * Call-backs are executed in the {@link RunControlTreePanel#executor} thread pool.
     * 
     * @see RunControlTreePanel#rcISCallback(is.InfoEvent, boolean)
     */
    private final static class RunCtrlReceiver extends ISInfoReceiver {
        private final WeakReference<RunControlTreePanel> treePanel;

        RunCtrlReceiver(final RunControlTreePanel treePanel) {
            super(IguiConstants.RC_IS_SERVER_NAME, new is.Criteria(Pattern.compile(".*")));
            this.treePanel = new WeakReference<RunControlTreePanel>(treePanel);
        }

        @Override
        protected void infoCreated(final is.InfoEvent info) {
            this.processCallback(info, false);
        }

        @Override
        protected void infoUpdated(final is.InfoEvent info) {
            this.processCallback(info, false);
        }

        @Override
        protected void infoDeleted(final is.InfoEvent info) {
            this.processCallback(info, true);
        }

        private void processCallback(final is.InfoEvent infoEvent, final boolean removed) {
            try {
                this.treePanel.get().executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            RunCtrlReceiver.this.treePanel.get().rcISCallback(infoEvent, removed);
                        }
                        catch(final NullPointerException ex) {
                            IguiLogger.debug("Trying to process an IS callback after the panel has been garbage collected", ex);
                        }
                    }
                });
            }
            catch(final NullPointerException ex) {
                IguiLogger.debug("Trying to process an IS callback after the panel has been garbage collected", ex);
            }
        }
    }

    /**
     * The tree cell renderer
     * <p>
     * A <code>JPanel</code> is used to represent the tree nodes. It contains:
     * <ul>
     * <li>A label with the application state;
     * <li>A second label with the application name.
     * </ul>
     */
    private final static class RCTreeCellRenderer extends DefaultTreeCellRenderer {
        private static final long serialVersionUID = 2582920148957198784L;

        private final JPanel panel = new JPanel();
        private final JLabel nameLabel = new JLabel();
        private final JLabel busyLabel = new JLabel();
        private final JLabel stateLabel = new JLabel();

        private final JLabel virtualNodeLabel = new JLabel();

        private final Font bold = new Font("Dialog", Font.BOLD, 12);
        private final Font boldItalic = new Font("Dialog", Font.ITALIC | Font.BOLD, 12);
        
        private final Color greenFaded = new Color(190, 255, 190);
        private final Color orangeFaded = new Color(255, 204, 102);
        private final Color yellowFaded = new Color(255, 255, 150);
        private final Color errorAndInColor = new Color(255, 100, 100);
        private final Color paleTurquoise1 = new Color(187, 255, 255);

        private final Border lineBorder = BorderFactory.createLineBorder(Color.red);
        private final Border compoundBorder = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
                                                                                 BorderFactory.createEmptyBorder(1, 1, 1, 1));

        private final ImageIcon busyIcon = Igui.createIcon("busy.png");
        private final ImageIcon erroIcon = Igui.createIcon("error.png");

        /**
         * Constructor building the renderer component.
         */
        RCTreeCellRenderer() {
            super();

            this.nameLabel.setOpaque(true);
            this.nameLabel.setDoubleBuffered(true);
            this.nameLabel.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));

            this.busyLabel.setOpaque(false);
            this.busyLabel.setDoubleBuffered(true);
            // Fix this label dimension to just fit the icon size, this avoids the other component to move
            // from their position when the busy icon is removed
            final Dimension busyLabelDim = new Dimension(this.busyIcon.getIconWidth(), this.busyIcon.getIconHeight());
            this.busyLabel.setMinimumSize(busyLabelDim);
            this.busyLabel.setMaximumSize(busyLabelDim);
            this.busyLabel.setPreferredSize(busyLabelDim);

            this.stateLabel.setOpaque(true);
            this.stateLabel.setDoubleBuffered(true);
            this.stateLabel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
                                                                         BorderFactory.createEmptyBorder(1, 0, 1, 0)));
            this.stateLabel.setPreferredSize(new Dimension(100, 20));
            this.stateLabel.setHorizontalAlignment(SwingConstants.CENTER);

            final BorderLayout panelLayout = new BorderLayout(1, 0);

            this.panel.setLayout(panelLayout);
            this.panel.add(this.stateLabel, BorderLayout.WEST);
            this.panel.add(this.busyLabel, BorderLayout.CENTER);
            this.panel.add(this.nameLabel, BorderLayout.EAST);
            this.panel.setBorder(BorderFactory.createEmptyBorder(1, 0, 1, 2));
            this.panel.setBackground(Color.white);
            this.panel.setOpaque(true);
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean,
         * boolean, int, boolean)
         */
        @Override
        public Component getTreeCellRendererComponent(final JTree tree,
                                                      final Object value,
                                                      final boolean sel,
                                                      final boolean expanded,
                                                      final boolean leaf,
                                                      final int row,
                                                      final boolean focus)
        {
            Component componentToRenderer;

            if(RCTreeNode.class.isInstance(value)) {
                final RCTreeNode tn = (RCTreeNode) value;
                if(tn.isVirtual() == false) {
                    final RunControlApplicationData appData = tn.getUserObject();
                    final boolean isTTCRestartEnabled = appData.isTTPartitionController();
                    final RunControlApplicationData.ApplicationISStatus appISStatus = appData.getISStatus();

                    final RunControlFSM.State appState = appISStatus.getState();
                    final RunControlFSM.ApplicationStatus appStatus = appISStatus.getStatus();
                    final boolean isIn = appISStatus.isEnabled();
                    final boolean isUp = appISStatus.isUp();
                    final boolean isError = appISStatus.isError();
                    final boolean isBusy = appISStatus.isBusy();

                    // Set different backgrounds for the panel and the name label
                    // if the node is selected or not
                    if(!sel) {
                        this.panel.setBackground(Color.white);
                        this.nameLabel.setBackground((isTTCRestartEnabled == false) ? Color.white : this.paleTurquoise1);
                    } else {
                        this.panel.setBackground(Color.yellow);
                        this.nameLabel.setBackground(Color.yellow);
                    }

                    // Set different foregrounds for the state label if the node is in error or not
                    // If it is in error show a tool-tip with the error message as well
                    if((isError == false) || (isUp == false)) {
                        this.panel.setToolTipText(null);
                        this.stateLabel.setIcon(null);
                        
                        if(isIn) {
                            this.stateLabel.setForeground(Color.black);
                        } else {
                            this.stateLabel.setForeground(Color.gray);
                        }
                    } else {
                        this.stateLabel.setIcon(this.erroIcon);
                        
                        final StringBuilder tt = new StringBuilder();
                        tt.append("<html>");
                        tt.append("<b>");
                        tt.append("\""
                                  + appData.toString().replaceAll("\\n",
                                                                  Matcher.quoteReplacement("<br />")).replaceAll("  ",
                                                                                                                 Matcher.quoteReplacement("&nbsp;"))
                                  + "\" error reason<br />");
                        tt.append("</b>");
                        tt.append(appISStatus.getError().replaceAll("\\n",
                                                                    Matcher.quoteReplacement("<br />")).replaceAll("  ",
                                                                                                                   Matcher.quoteReplacement("&nbsp;")));
                        tt.append("</html>");
                        this.panel.setToolTipText(tt.toString());

                        if(isIn) {
                            this.stateLabel.setForeground(Color.red);
                        } else {
                            this.stateLabel.setForeground(this.errorAndInColor);
                        }
                    }

                    // Set different borders for the state label if the node is busy or not
                    if((isBusy == true) && (isUp == true)) {
                        this.busyLabel.setIcon(this.busyIcon);
                        this.stateLabel.setBorder(this.lineBorder);
                    } else {
                        this.busyLabel.setIcon(null);
                        this.stateLabel.setBorder(this.compoundBorder);
                    }

                    // Set text and background for the state label
                    Color backgrndColor;
                    if((isUp == true) && (appState.equals(RunControlFSM.State.ABSENT) == false)) {
                        // If the application is running and its status is not ABSENT
                        // then show its RC state
                        this.stateLabel.setText(appState.toString());
                        backgrndColor = appState.getColor();
                    } else {
                        // If we are here the node represents a non run-control application
                        // or the application is not running: in this case show the application status
                        switch(appStatus) {
                            case NOTAV:
                            case ABSENT:
                            case EXITED:
                            case FAILED:
                                this.stateLabel.setText(RunControlFSM.ApplicationStatus.ABSENT.toString());
                                break;
                            default:
                                this.stateLabel.setText(appStatus.toString());
                        }

                        backgrndColor = appStatus.getColor();
                    }

                    // Set name label background and state label foreground taking into account the node membership
                    if(isIn) {
                        this.nameLabel.setForeground(Color.black);
                        this.stateLabel.setBackground(backgrndColor);
                    } else {
                        this.nameLabel.setForeground(Color.gray);
                        this.stateLabel.setBackground(this.getFadedOutBackgroundColor(backgrndColor));
                    }

                    // Set the node name equal to the application name
                    this.nameLabel.setText(value.toString());
                    this.nameLabel.setFont((isTTCRestartEnabled == false) ? this.bold : this.boldItalic);
                    
                    componentToRenderer = this.panel;
                } else {
                    this.virtualNodeLabel.setText(tn.alternativeLabel());
                    this.virtualNodeLabel.setIcon(tn.icon());
                    componentToRenderer = this.virtualNodeLabel;
                }
            } else {
                componentToRenderer = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, focus);
            }

            return componentToRenderer;
        }

        private Color getFadedOutBackgroundColor(final Color originalColor) {
            if(originalColor.equals(Color.green)) {
                return this.greenFaded;
            } else if(originalColor.equals(Color.orange)) {
                return this.orangeFaded;
            } else if(originalColor.equals(Color.yellow)) {
                return this.yellowFaded;
            } else if(originalColor.equals(Color.gray)) {
                return Color.lightGray;
            } else {
                return Color.lightGray;
            }
        }
    }

    /**
     * Class representing a node in the Run Control tree.
     * <p>
     * The data about the object represented by the node are stored in the node user object and are of the {@link RunControlApplicationData}
     * type.
     * <p>
     * Each information about the node as part of the tree (i.e., child count, etc.) should be retrieved using the tree model.
     * <p>
     * NOTE: Use {@link RCTreeNode#getAllChildren()} in order to get also the children of any vhild virtual node.
     */
    static class RCTreeNode extends DefaultMutableTreeNode {
        private static final long serialVersionUID = 6040169170758539098L;

        /**
         * Reference to the user object
         */
        private final RunControlApplicationData appData;

        /**
         * Flag to know if child info from IS have been loaded
         */
        private final AtomicBoolean isInfoLoaded = new AtomicBoolean(false);

        /**
         * Flag to know if a node has ever been expanded
         */
        private final AtomicBoolean hasEverBeenExpanded = new AtomicBoolean(false);

        /**
         * Label to be shown by "virtual" node
         */
        private final String alternativeLabel;

        /**
         * Icon to be used for "virtual" nodes
         */
        private final ImageIcon icon;

        /**
         * Whether the node is virtual (i.e., it does not really represent an application) or not
         */
        private final boolean isVirtual;

        /**
         * Constructor.
         * 
         * @param data Reference to the {@link RunControlApplicationData} object holding all the info about the object represented by the
         *            node.
         */
        private RCTreeNode(final RunControlApplicationData data) {
            super(data);
            this.appData = data;
            this.alternativeLabel = this.appData.getName();
            this.isVirtual = false;
            this.icon = null;
        }

        /**
         * This constructor is used to create "virtual" nodes (i.e., nodes that do not really represent an application but a category of
         * them).
         * 
         * @param tn The parent node
         * @param alternativeLabel The label to be shown in the tree
         */
        private RCTreeNode(final RCTreeNode tn, final String alternativeLabel, final ImageIcon icon) {
            // In order to make this node "transparent" it must have the same user object
            // of the parent: in this way this node is actually a proxy of its parent and
            // contains the same information
            super(tn.appData);
            this.appData = tn.appData;
            this.alternativeLabel = alternativeLabel;
            this.isVirtual = true;
            this.icon = icon;
        }

        public boolean isVirtual() {
            return this.isVirtual;
        }

        /**
         * It returns all the children of this node (including the children of any child virtual node)
         * 
         * @return All the children of this node
         */
        public List<RCTreeNode> getAllChildren() {
            final List<RCTreeNode> all = new ArrayList<>();

            final Enumeration<TreeNode> ch = this.children();
            while(ch.hasMoreElements()) {
                final RCTreeNode n = (RCTreeNode) ch.nextElement();
                if(n.isVirtual() == true) {
                    all.addAll(n.getAllChildren());
                } else {
                    all.add(n);
                }
            }

            return all;
        }

        /**
         * It returns all the children of this node (NOT including the children of any child virtual node)
         * <p>
         * Use this method instead of {@link #children()} in order to have the list of children with the right type.
         * 
         * @return All the children of this node
         */
        public List<RCTreeNode> getChildren() {
            final List<RCTreeNode> all = new ArrayList<>();

            final Enumeration<TreeNode> ch = this.children();
            while(ch.hasMoreElements()) {
                final RCTreeNode n = (RCTreeNode) ch.nextElement();
                all.add(n);
            }
            
            return all;
        }
        
        /**
         * Returns <code>true</code> if the application IS information has been loaded at least once.
         * <p>
         * Needed to implement the Run Control tree lazy loading of application information from IS.
         * 
         * @return <code>true</code> if the application IS information has been loaded at least once.
         * @see #setInfoLoaded(boolean)
         */
        public boolean isInfoLoaded() {
            return this.isInfoLoaded.get();
        }

        /**
         * Returns <code>true</code> if the node has been expanded at least once.
         * 
         * @return <code>true</code> if the node has been expanded at least once
         */
        public boolean hasBeenExpanded() {
            return this.hasEverBeenExpanded.get();
        }

        /**
         * It sets whether the node has been expanded at least once.
         * 
         * @param expanded <code>true</code> if the node has been expanded at least once
         */
        void setHasBeenExpanded(final boolean expanded) {
            this.hasEverBeenExpanded.set(expanded);
        }

        /**
         * It sets whether the application information has been loaded from IS at least once.
         * 
         * @param childInfoLoaded
         */
        void setInfoLoaded(final boolean childInfoLoaded) {
            this.isInfoLoaded.set(childInfoLoaded);
        }

        /**
         * Overridden to return the text the node is represented with in the tree (i.e., the name of the application it represents)
         * <p>
         * The string returned by {@link #alternativeLabel()} is used for "virtual" nodes.
         */
        @Override
        public String toString() {
            return this.appData.getName();
        }

        @Override
        public RunControlApplicationData getUserObject() {
            return this.appData;
        }

        /**
         * It returns the label to be used when the node is "virtual"
         * 
         * @return The label to be used when the node is "virtual"
         */
        public String alternativeLabel() {
            return this.alternativeLabel;
        }

        /**
         * It returns the icon to be shown in the tree
         * 
         * @return <em>null</em> if the node is not "virtual"
         */
        public ImageIcon icon() {
            return this.icon;
        }
    }

    /**
     * Class extending {@link TreeSearchable} to add searching feature to the RC tree.
     * <p>
     * It just redefines some methods to avoid that the internal cache of tree paths is not rebuilt when not needed.
     */
    private final class RCTreeSearchable extends TreeSearchable {
        public RCTreeSearchable() {
            super(RunControlTreePanel.this.rcTree);
        }

        @Override
        protected String convertElementToString(final Object object) {
            final TreePath path = (TreePath) object;

            final RCTreeNode node = (RCTreeNode) path.getLastPathComponent();
            if(node.isVirtual() == false) {
                return node.toString();
            }

            return node.alternativeLabel();
        }

        @Override
        public void treeNodesChanged(final TreeModelEvent e) {
        }

        @Override
        public void treeNodesInserted(final TreeModelEvent e) {
        }

        @Override
        public void treeNodesRemoved(final TreeModelEvent e) {
        }

        // @Override
        // public void treeStructureChanged(final TreeModelEvent e) {
        // super.treeStructureChanged(e); // This will clean the tree path cache
        // }
    }

    /**
     * Selection model for the RC tree.
     * <p>
     * The current implementation allows to select multiple nodes only if they have the same parent and a common state (
     * {@link RCTreeSelectionModel#areNodesEquivalent(Object, Object)}). This is done to have a consistent set of items enabled in the
     * contextual menu shown when the right mouse button is clicked on a tree item.
     * 
     * @see TreeSelectionFilteredModel
     */
    private final static class RCTreeSelectionModel extends TreeSelectionFilteredModel {
        private static final long serialVersionUID = -431717066021367562L;
        private final Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
        
        RCTreeSelectionModel() {
            super();
        }

        @Override
        protected boolean canBeSelected(final Object n) {
            final RCTreeNode n1 = (RCTreeNode) n;
            return !n1.isVirtual();
        }

        /**
         * It compares two tree nodes and returns <code>true</code> if they can be both selected at the same time (i.e., they are
         * equivalent).
         * <p>
         * Two nodes are equivalent if they result in the same items to be enabled/disabled in the contextual menu shown using the right
         * mouse button (see {@link RCTreeMouseListenerAdapter#enableItems()}) (this indirectly implies that they must have the same
         * parent).
         * 
         * @param n A tree node
         * @param m A tree node
         * @return <code>true</code> if the two nodes are equivalent.
         */
        @Override
        protected boolean areNodesEquivalent(final Object n, final Object m) {
            boolean equivalent = false;

            final RCTreeNode n1 = (RCTreeNode) n;
            final RCTreeNode m1 = (RCTreeNode) m;

            if(n1.getParent() == m1.getParent()) { // Here we really want to compare the references!
                final RunControlApplicationData n1Data = n1.getUserObject();
                final RunControlApplicationData m1Data = m1.getUserObject();

                final ApplicationISStatus nStatus = n1Data.getISStatus();
                final ApplicationISStatus mStatus = m1Data.getISStatus();

                equivalent = (nStatus.isUp() == mStatus.isUp()) && (nStatus.isBusy() == mStatus.isBusy())
                             && (nStatus.isError() == mStatus.isError()) && (nStatus.isEnabled() == mStatus.isEnabled())
                             && (n1Data.isController() == m1Data.isController());
            }

            return equivalent;
        }
        
        /**
         * {@inheritDoc}
         * 
         * The name of the selected node is copied to the system clipboard.
         */
        @Override
        protected void fireValueChanged(final TreeSelectionEvent tse) {
            final TreePath path = tse.getNewLeadSelectionPath();
            if(path != null) {
                final String appName = path.getLastPathComponent().toString();
                final StringSelection str = new StringSelection(appName);
                try {
                    this.clipBoard.setContents(str, str);                
                }
                catch(final IllegalStateException ex) {
                    ex.printStackTrace();
                }
            }
            
            super.fireValueChanged(tse);
        }
    }

    /**
     * SwingWorker used to get node information from IS.
     * <p>
     * It is used to retrieve IS information both when a node is selected or is expanded for the first time.
     */
    private final class ISNodeInfoWorker extends SwingWorker<Void, Void> {

        /**
         * The selected or expanded node
         */
        private final RCTreeNode treeNode;

        /**
         * Constructor.
         * 
         * @param node The node selected or expanded
         */
        ISNodeInfoWorker(final RCTreeNode node) {
            this.treeNode = node;
        }

        /**
         * Information about the tree node is retrieved from IS.
         * 
         * @see javax.swing.SwingWorker#doInBackground()
         */
        @Override
        protected Void doInBackground() {
            final RunControlApplicationData treeNodeData = this.treeNode.getUserObject();
            final String nodeName = this.treeNode.toString();

            try {
                final rc.DAQApplicationInfo appDAQInfo = RunControlTreePanel.DAQ_APP_INFO.get();
                RunControlTreePanel.this.runCtrlReceiver.getRepository().getValue(IguiConstants.SUPERVISION_IS_INFO_NAME + "." + nodeName,
                                                                                  appDAQInfo);
                treeNodeData.updateInfo(appDAQInfo, false);
            }
            catch(final is.InfoNotFoundException ex) {
                // The info is not found in IS
                treeNodeData.resetSupervisorInfo();
            }
            catch(final Exception ex) {
                final String msg = "Error getting DAQApplicationInfo IS information for \"" + nodeName + "\": " + ex;
                IguiLogger.error(msg, ex);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
            }

            try {
                final rc.RCStateInfo appRCInfo = RunControlTreePanel.RC_STATE_INFO.get();
                RunControlTreePanel.this.runCtrlReceiver.getRepository().getValue(IguiConstants.RC_IS_SERVER_NAME + "." + nodeName,
                                                                                  appRCInfo);
                treeNodeData.updateInfo(appRCInfo, false);
            }
            catch(final is.InfoNotFoundException ex) {
                // The info is not found in IS
                treeNodeData.resetRCInfo();
            }
            catch(final Exception ex) {
                final String msg = "Error getting RCStateInfo IS information for \"" + nodeName + "\": " + ex;
                IguiLogger.error(msg, ex);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
            }

            return null;
        }

        /**
         * Signal the tree model about the node changes.
         * 
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                this.get();
            }
            catch(final InterruptedException ex) {
                IguiLogger.warning(ex.toString(), ex);
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                final String errMsg = "Unexpected error while getting IS info for the node \"" + this.treeNode.toString() + "\": " + ex;
                IguiLogger.error(errMsg, ex);
            }
            finally {
                this.treeNode.setInfoLoaded(true);
                RunControlTreePanel.this.treeModel.nodeChanged(this.treeNode);
            }
        }
    }

    /**
     * This class implements the <code>TreeWillExpandListener</code> interface.
     * <p>
     * It is used to implement IS information lazy loading for the run control tree. Any time a node is expanded for the first time,
     * information about all its children is retrieved from IS to show the user the current application status.
     * 
     * @see ISNodeInfoWorker
     */
    private final class RCTreeExpansionListener implements TreeWillExpandListener {

        /*
         * (non-Javadoc)
         * @see javax.swing.event.TreeWillExpandListener#treeWillCollapse(javax.swing.event.TreeExpansionEvent)
         */
        @Override
        public void treeWillCollapse(final TreeExpansionEvent event) {
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.event.TreeWillExpandListener#treeWillExpand(javax.swing.event.TreeExpansionEvent)
         */
        @Override
        public void treeWillExpand(final TreeExpansionEvent event) throws ExpandVetoException {
            // Get the expanded node
            final TreePath expandingPath = event.getPath();

            final RCTreeNode lastNode = (RCTreeNode) expandingPath.getLastPathComponent();
            final boolean pathExpanded = RunControlTreePanel.this.rcTree.hasBeenExpanded(expandingPath);
            final RCTreeNode child = (RCTreeNode) lastNode.getChildAt(0);

            // If the path has already been expanded once then look at the first child to know if the
            // IS info needs to be loaded (needed for the refresh IS to work)
            if((pathExpanded == false) || (lastNode.hasBeenExpanded() == false) || (child.hasBeenExpanded() == false)) {
                IguiLogger.debug("Loading IS information for the expanding path \"" + expandingPath.toString() + "\"");

                // The "has been expanded" flag is important to allow the IS call-back processing for the node
                // and it HAS TO BE SET before the execution of the SwingWorker: this makes impossible to
                // not process a valid call-back. Suppose that the flag is set after the SwingWorker is submitted
                // to the executor: at that point it may happen that the worker finishes its job whenthe flag has
                // not been set yet; and if a call-back comes in the while it would be lost.
                lastNode.setHasBeenExpanded(true);
                try {
                    // Get info about the expanded node
                    RunControlTreePanel.this.executor.execute(new ISNodeInfoWorker(lastNode));
                }
                catch(final Exception ex) {
                    IguiLogger.error("Failed processing a \"TreeWillExpand\" notification: " + ex.getMessage(), ex);
                }

                // Get info about all the node children
                final List<RCTreeNode> children = lastNode.getChildren();
                for(final RCTreeNode ch : children) {
                    ch.setHasBeenExpanded(true);
                    try {
                        RunControlTreePanel.this.executor.execute(new ISNodeInfoWorker(ch));
                    }
                    catch(final Exception ex) {
                        IguiLogger.error("Failed processing a \"TreeWillExpand\" notification: " + ex.getMessage(), ex);
                    }
                }
            }
        }
    }

    /**
     * Class implementing the <code>TreeSelectionListener</code> interface for the run control tree.
     * <p>
     * Every time a node is selected its status (and the status of their direct children) is updated getting information from IS. This is
     * needed in order to provide other RC panels.
     * 
     * @see ISNodeInfoWorker
     */
    private final class RCTreeSelectionListener implements TreeSelectionListener {
        /*
         * (non-Javadoc)
         * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
         */
        @Override
        public void valueChanged(final TreeSelectionEvent e) {
            try {
                final TreePath[] tpa = e.getPaths();
                for(final TreePath p : tpa) {
                    if(e.isAddedPath(p)) {
                        // This check is really important and without it there may be problems at reload
                        // (or whenever the tree is re-built): indeed at that time you could ask the tree
                        // model for a node which does not exist any more and this may cause some exception
                        // in the model itself.
                        final RCTreeNode node = (RCTreeNode) p.getLastPathComponent();
                        RunControlTreePanel.this.executor.execute(new ISNodeInfoWorker(node));

                        // Now load all the children (their status may be needed by the other panels, like
                        // the Tests Panel, for instance, that shows the status of tests for child nodes)
                        final List<RCTreeNode> ch = node.getAllChildren();
                        for(final RCTreeNode n : ch) {
                            RunControlTreePanel.this.executor.execute(new ISNodeInfoWorker(n));
                        }
                    }
                }
            }
            catch(final Exception ex) {
                IguiLogger.error("Failed processing a \"TreeSelectionListener\" notification: " + ex.getMessage(), ex);
            }
        }

    }

    /**
     * Class extending <code>MouseAdapter</code> for the run control tree.
     * <p>
     * When a tree node (or more) is selected and the right mouse button is pushed a pop-menu menu will appear giving the user the
     * possibility to send commands to the selected application or controller. The menu will not be available if the Igui is in STATUS
     * DISPLAY.
     * <p>
     * Give a look at the tree selection model (@see {@link RCTreeSelectionModel}) to have details of the which nodes can be selected at the
     * same time (the actual contextual menu working schema works only if the nodes have all the same parent).
     */
    private class RCTreeMouseListenerAdapter extends MouseAdapter {
        // The pop-up menu
        private final JPopupMenu popUpMenu = new JPopupMenu();

        // Icon file name
        private final static String inIconName = "mem_in.png";
        private final static String outIconName = "mem_out.png";
        private final static String restartIconName = "restart.png";
        private final static String killIconName = "kill.png";
        private final static String ignoreIconName = "ignore.png";
        private final static String searchIconName = "search.png";
        private final static String infoIconName = "info.png";

        // All the menu items
        private final JMenu advancedSegMenu = new JMenu("Full Segment");
        private final JMenuItem detailedInfo = new JMenuItem("Detailed information");
        private final JMenuItem inMenuItem = new JMenuItem("IN");
        private final JMenuItem inMenuItemSeg = new JMenuItem("IN");
        private final JMenuItem outMenuItem = new JMenuItem("OUT");
        private final JMenuItem outMenuItemSeg = new JMenuItem("OUT");
        private final JMenuItem ignoreMenuItem = new JMenuItem("Ignore Error");
        private final JMenuItem restartMenuItem = new JMenuItem("Restart");
        private final JMenuItem restartMenuItemSeg = new JMenuItem("Restart");
        private final JMenuItem killMenuItem = new JMenuItem("Kill");
        private final JMenuItem killMenuItemSeg = new JMenuItem("Kill");
        private final JMenuItem findChildErrorAppMenuItem = new JMenuItem("Find first child in error");
        private final JMenuItem findLeafErrorAppMenuItem = new JMenuItem("Find sibling in error");
        private final JMenuItem findLastChildErrorAppMenuItem = new JMenuItem("Find last child in error");
        private final JMenuItem restartTTCPartitionMenuItem = new JMenuItem("Restart TTC Partition");
        private final JLabel titleLabel = new JLabel();

        // Here the selection model matters: with the current selection model
        // all the selected nodes do belong to the same parent node.
        // WARNING: this has to be adapted to any change in the selection model
        // WARNING: keeping a reference to these nodes after a tree reload means to
        // keep references to almost all the nodes in the tree (and potentially keep several
        // tree duplications...) until the right mouse button is clicked again. As a solution
        // the mouse listener can be re-created after a reload or the selected nodes
        // may be reset (i.e., set them to the new root node) or better use weak references.
        // This has to be done because this class has the same life time of the panel

        // The contextual menu is shown only if something in the tree is really selected;
        // this means that the following list is never empty when accessed from listeners
        // or methods that enable/disable items in the menu.
        private final List<WeakReference<RCTreeNode>> selectedNodes = new ArrayList<WeakReference<RCTreeNode>>();
        private WeakReference<RCTreeNode> parentNode = new WeakReference<RCTreeNode>(null);

        /**
         * Constructor: the pop-up menu is initialized and actions are associated to each menu item.
         * 
         * @see #initMenuItems()
         * @see #setActionListener()
         */
        RCTreeMouseListenerAdapter() {
            this.setActionListener();
            this.initMenuItems();
        }

        /**
         * The pop-up menu is showed and only the right items (depending on the application status) are enabled. The menu is not showed if
         * the Igui is in STATUS DISPLAY.
         * 
         * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
         */
        @Override
        public void mouseClicked(final MouseEvent e) {
            // Check if we are in status display
            if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
                // Be sure that the right mouse button is the source of this event
                if(javax.swing.SwingUtilities.isRightMouseButton(e) == true) {
                    // We want to show the pop-up menu only if the rmb is clicked over a tree item
                    final TreePath tp = RunControlTreePanel.this.rcTree.getPathForLocation(e.getX(), e.getY());
                    final boolean isSelected = RunControlTreePanel.this.rcTree.getSelectionModel().isPathSelected(tp);
                    if(isSelected == false) {
                        RunControlTreePanel.this.rcTree.setSelectionPath(tp);
                    }

                    // The selection path may still be null because the mouse pointer is far from any tree node
                    final TreePath[] selPaths = RunControlTreePanel.this.rcTree.getSelectionPaths();
                    if((selPaths != null) && (selPaths.length > 0)) {
                        // If we are here it means that the mouse has been pressed over a tree item
                        if(selPaths.length > 1) {
                            // Multiple selection
                            this.titleLabel.setText("Multiple Selection");
                        } else {
                            // Nothing or only 1 item is selected: select the tree path the mouse is over
                            final RCTreeNode lsc = (RCTreeNode) selPaths[0].getLastPathComponent();
                            this.titleLabel.setText(lsc.toString());
                        }

                        // Here we are sure that some tree item is selected

                        // Cache the selected nodes and parent
                        this.setSelectedNodes(selPaths);

                        // Enable only the right items
                        this.enableItems();

                        // Show the menu
                        this.popUpMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        }

        /**
         * It sets the selected nodes and the common parent node.
         * <p>
         * Remember that this works only if the selection model allows to select nodes with the same parent.
         * 
         * @param selPaths The currently selected paths
         */
        private void setSelectedNodes(final TreePath[] selPaths) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            this.selectedNodes.clear();
            final int size = selPaths.length;
            for(int i = 0; i < size; ++i) {
                this.selectedNodes.add(new WeakReference<RCTreeNode>((RCTreeNode) selPaths[i].getLastPathComponent()));
            }

            this.parentNode = new WeakReference<RCTreeNode>((RCTreeNode) ((RCTreeNode) selPaths[0].getLastPathComponent()).getParent());
        }

        /**
         * Initialize all the pop-up menu items.
         */
        private final void initMenuItems() {
            this.popUpMenu.addSeparator();

            final PartialGradientLineBorder grad = new PartialGradientLineBorder(new Color[] {Color.RED.brighter(), Color.BLUE.brighter()},
                                                                                 1,
                                                                                 PartialSide.SOUTH);

            {
                this.titleLabel.setForeground(Color.RED.brighter());
                final TitledSeparator titlesep = new TitledSeparator(this.titleLabel,
                                                                     BorderFactory.createEmptyBorder(),
                                                                     SwingConstants.CENTER);
                titlesep.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED,
                                                                                                       Color.BLUE,
                                                                                                       Color.BLUE),
                                                                      BorderFactory.createEmptyBorder(3, 3, 3, 3)));
                this.popUpMenu.add(titlesep);
            }

            {
                final JLabel memLabel = new JLabel("MEMBERSHIP");
                memLabel.setForeground(Color.BLUE.brighter());
                final TitledSeparator memb = new TitledSeparator(memLabel, grad, SwingConstants.CENTER);
                this.popUpMenu.add(memb);
            }

            this.popUpMenu.add(this.inMenuItem);
            this.inMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.inIconName));

            this.popUpMenu.add(this.outMenuItem);
            this.outMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.outIconName));

            {
                final JLabel recLabel = new JLabel("RECOVERY");
                recLabel.setForeground(Color.BLUE.brighter());
                final TitledSeparator rec = new TitledSeparator(recLabel, grad, SwingConstants.CENTER);
                this.popUpMenu.add(rec);
            }

            this.popUpMenu.add(this.ignoreMenuItem);
            this.ignoreMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.ignoreIconName));

            this.popUpMenu.add(this.restartMenuItem);
            this.restartMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.restartIconName));

            this.popUpMenu.add(this.restartTTCPartitionMenuItem);
            this.restartTTCPartitionMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.restartIconName));

            this.popUpMenu.add(this.killMenuItem);
            this.killMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.killIconName));

            {
                final JLabel searchLabel = new JLabel("SEARCH");
                searchLabel.setForeground(Color.BLUE.brighter());
                final TitledSeparator ser = new TitledSeparator(searchLabel, grad, SwingConstants.CENTER);
                this.popUpMenu.add(ser);
            }

            this.findLeafErrorAppMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.searchIconName));
            this.popUpMenu.add(this.findLeafErrorAppMenuItem);

            this.findChildErrorAppMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.searchIconName));
            this.popUpMenu.add(this.findChildErrorAppMenuItem);

            this.findLastChildErrorAppMenuItem.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.searchIconName));
            this.popUpMenu.add(this.findLastChildErrorAppMenuItem);

            {
                final JLabel advanced = new JLabel("ADVANCED");
                advanced.setForeground(Color.BLUE.brighter());
                final TitledSeparator memb = new TitledSeparator(advanced, grad, SwingConstants.CENTER);
                this.popUpMenu.add(memb);
            }

            this.advancedSegMenu.add(this.restartMenuItemSeg);
            this.restartMenuItemSeg.setToolTipText("Restart the selected controller and the corresponsing segment's infrastructure");
            this.restartMenuItemSeg.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.restartIconName));

            this.advancedSegMenu.add(this.killMenuItemSeg);
            this.killMenuItemSeg.setToolTipText("Stop the selected controller and the corresponsing segment's infrastructure");
            this.killMenuItemSeg.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.killIconName));

            this.advancedSegMenu.add(this.inMenuItemSeg);
            this.inMenuItemSeg.setToolTipText("Include the selected controller and the corresponsing segment's infrastructure");
            this.inMenuItemSeg.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.inIconName));

            this.advancedSegMenu.add(this.outMenuItemSeg);
            this.outMenuItemSeg.setToolTipText("Put out of membership the selected controller and the corresponsing segment's infrastructure");
            this.outMenuItemSeg.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.outIconName));

            this.popUpMenu.add(this.advancedSegMenu);

            this.detailedInfo.setToolTipText("Show detailed information about the selected application");
            this.detailedInfo.setIcon(Igui.createIcon(RCTreeMouseListenerAdapter.infoIconName));
            this.popUpMenu.add(this.detailedInfo);

            this.popUpMenu.addSeparator();

            this.popUpMenu.pack();
        }

        /**
         * It returns an array containing the names of the selected nodes.
         * 
         * @return An array containing the names of the selected nodes
         */
        private String[] nodeListAsArray() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            final List<String> ar = new ArrayList<String>();
            for(final WeakReference<RCTreeNode> nr : this.selectedNodes) {
                final RCTreeNode n = nr.get();
                if(n != null) {
                    ar.add(n.toString());
                } else {
                    IguiLogger.debug("Trying to access an already garbage collected node");
                }
            }

            return ar.toArray(new String[ar.size()]);
        }

        /**
         * It returns an array containing the names of the selected nodes and of the infrastructure applications of the segment managed by
         * every selected controller.
         * <p>
         * Used to send "segment wide" commands.
         * 
         * @return An array containing the names of the concerned applications
         */
        private String[] nodeListAsArray_Extended() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            final List<String> ar = new ArrayList<String>();
            for(final WeakReference<RCTreeNode> nr : this.selectedNodes) {
                final RCTreeNode n = nr.get();
                if(n != null) {
                    ar.add(n.toString());

                    final RunControlApplicationData nodeData = n.getUserObject();
                    if(nodeData.isController() == true) {
                        ar.addAll(nodeData.infrastructureApplications());
                    }
                } else {
                    IguiLogger.debug("Trying to access an already garbage collected node");
                }
            }

            return ar.toArray(new String[ar.size()]);
        }

        /**
         * Associate actions to each menu item.
         * <p>
         * All the action listeners sending commands to the parent of the selected nodes work taking into account the current selection
         * model.
         */
        private final void setActionListener() {
            this.inMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    try {
                        RCTreeMouseListenerAdapter.this.sendParentCommand(new Command.ChangeStatusCmd(true,
                                                                                                      RCTreeMouseListenerAdapter.this.nodeListAsArray()));
                    }
                    catch(final BadCommandException ex) {
                        IguiLogger.error(ex.getMessage(), ex);
                        ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                    }
                }
            });

            this.inMenuItemSeg.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final String msg =
                                     "Do not execute this command if you are not 100% sure about it, it may affect a large number of applications! Do you want to proceed?";
                    final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                     msg,
                                                                     "Segment Wide Command Warning",
                                                                     JOptionPane.YES_NO_OPTION);

                    if(answer == JOptionPane.YES_OPTION) {
                        try {
                            RCTreeMouseListenerAdapter.this.sendParentCommand(new Command.ChangeStatusCmd(true,
                                                                                                          RCTreeMouseListenerAdapter.this.nodeListAsArray_Extended()));
                        }
                        catch(final BadCommandException ex) {
                            IguiLogger.error(ex.getMessage(), ex);
                            ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                        }
                    }
                }
            });

            this.detailedInfo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final RCTreeNode selNode = RCTreeMouseListenerAdapter.this.selectedNodes.get(0).get();
                    if(selNode != null) {
                        RunControlInfoPanel.createAndShow(selNode.getUserObject());
                    }
                }
            });

            this.outMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    try {
                        RCTreeMouseListenerAdapter.this.sendParentCommand(new Command.ChangeStatusCmd(false,
                                                                                                      RCTreeMouseListenerAdapter.this.nodeListAsArray()));
                    }
                    catch(final BadCommandException ex) {
                        IguiLogger.error(ex.getMessage(), ex);
                        ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                    }
                }
            });

            this.outMenuItemSeg.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final String msg =
                                     "Do not execute this command if you are not 100% sure about it, it may affect a large number of applications! Do you want to proceed?";
                    final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                     msg,
                                                                     "Segment Wide Command Warning",
                                                                     JOptionPane.YES_NO_OPTION);

                    if(answer == JOptionPane.YES_OPTION) {
                        try {
                            RCTreeMouseListenerAdapter.this.sendParentCommand(new Command.ChangeStatusCmd(false,
                                                                                                          RCTreeMouseListenerAdapter.this.nodeListAsArray_Extended()));
                        }
                        catch(final BadCommandException ex) {
                            IguiLogger.error(ex.getMessage(), ex);
                            ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                        }
                    }
                }
            });

            this.ignoreMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    RCTreeMouseListenerAdapter.this.sendIgnoreError();
                }
            });

            this.restartMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    try {
                        RCTreeMouseListenerAdapter.this.sendParentCommand(new Command.RestartAppsCmd(RCTreeMouseListenerAdapter.this.nodeListAsArray()));
                    }
                    catch(final BadCommandException ex) {
                        IguiLogger.error(ex.getMessage(), ex);
                        ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                    }
                }
            });

            this.restartMenuItemSeg.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final String msg =
                                     "Do not execute this command if you are not 100% sure about it, you are going to restart several applications! Do you want to proceed?";
                    final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                     msg,
                                                                     "Segment Wide Command Warning",
                                                                     JOptionPane.YES_NO_OPTION);

                    if(answer == JOptionPane.YES_OPTION) {
                        try {
                            RCTreeMouseListenerAdapter.this.sendParentCommand(new Command.RestartAppsCmd(RCTreeMouseListenerAdapter.this.nodeListAsArray_Extended()));
                        }
                        catch(final BadCommandException ex) {
                            IguiLogger.error(ex.getMessage(), ex);
                            ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                        }
                    }
                }
            });

            this.restartTTCPartitionMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final int answer =
                                     JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                   "You are probably going to reconfigure a big piece of the detector, are you sure?",
                                                                   "TTC Partition Restart Command Warning",
                                                                   JOptionPane.YES_NO_OPTION);
                    if(answer == JOptionPane.YES_OPTION) {
                        try {
                            RCTreeMouseListenerAdapter.this.sendParentCommand(new Command.DynRestartCmd(RCTreeMouseListenerAdapter.this.nodeListAsArray()));
                        }
                        catch(final BadCommandException ex) {
                            IguiLogger.error(ex.getMessage(), ex);
                            ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                        }
                    }
                }
            });

            this.killMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final String[] selectedApps = RCTreeMouseListenerAdapter.this.nodeListAsArray();

                    try {
                        final List<daq.rc.Command> cmdList = new ArrayList<>();
                        cmdList.add(new Command.ChangeStatusCmd(false, selectedApps));
                        cmdList.add(new Command.StopAppsCmd(selectedApps));

                        RCTreeMouseListenerAdapter.this.sendParentCommands(cmdList);
                    }
                    catch(final BadCommandException ex) {
                        IguiLogger.error(ex.getMessage(), ex);
                        ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                    }
                }
            });

            this.killMenuItemSeg.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final String msg =
                                     "Do not execute this command if you are not 100% sure about it, you are going to kill several applications! Do you want to proceed?";
                    final int answer = JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                     msg,
                                                                     "Segment Wide Command Warning",
                                                                     JOptionPane.YES_NO_OPTION);

                    if(answer == JOptionPane.YES_OPTION) {
                        final String[] selectedApps = RCTreeMouseListenerAdapter.this.nodeListAsArray_Extended();

                        try {
                            final List<daq.rc.Command> cmdList = new ArrayList<>();
                            cmdList.add(new Command.ChangeStatusCmd(false, selectedApps));
                            cmdList.add(new Command.StopAppsCmd(selectedApps));

                            RCTreeMouseListenerAdapter.this.sendParentCommands(cmdList);
                        }
                        catch(final BadCommandException ex) {
                            IguiLogger.error(ex.getMessage(), ex);
                            ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                        }
                    }
                }
            });

            this.findChildErrorAppMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final RCTreeNode selNode = RCTreeMouseListenerAdapter.this.selectedNodes.get(0).get();
                    if(selNode != null) {
                        final List<RCTreeNode> l = selNode.getAllChildren();
                        new NodeAttributeFinder(l, selNode, false) {
                            @Override
                            protected boolean matches(final RCTreeNode node) {
                                // We are looking only for applications in error
                                boolean found = false;

                                final RunControlApplicationData.ApplicationISStatus isStatus = node.getUserObject().getISStatus();
                                if(isStatus.isEnabled() == true) {
                                    if(isStatus.isError() == true) {
                                        found = true;
                                    } else {
                                        final TreeNode p = node.getParent();
                                        if(p != null) {
                                            final RCTreeNode pNode = (RCTreeNode) p;
                                            if(pNode.getUserObject().getISStatus().getBadChildren().contains(node.toString()) == true) {
                                                found = true;
                                            }
                                        }
                                    }
                                }

                                return found;
                            }
                        }.execute();
                    } else {
                        IguiLogger.debug("Trying to access an already garbage collected node");
                    }
                }
            });

            this.findLeafErrorAppMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final RCTreeNode selNode = RCTreeMouseListenerAdapter.this.selectedNodes.get(0).get();
                    if(selNode != null) {
                        final TreeNode pNode_ = selNode.getParent();
                        if(pNode_ != null) {
                            final RCTreeNode pNode = (RCTreeNode) pNode_;
                            final List<RCTreeNode> l = pNode.getChildren();

                            new NodeAttributeFinder(l, selNode, false) {
                                @Override
                                protected boolean matches(final RCTreeNode node) {
                                    // We are looking only for applications in error
                                    final RunControlApplicationData.ApplicationISStatus isStatus = node.getUserObject().getISStatus();
                                    return((isStatus.isEnabled() == true)
                                           && ((isStatus.isError() == true)
                                               || (pNode.getUserObject().getISStatus().getBadChildren().contains(node.toString()))));
                                }
                            }.execute();
                        }
                    } else {
                        IguiLogger.debug("Trying to access an already garbage collected node");
                    }
                }
            });

            this.findLastChildErrorAppMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final RCTreeNode selNode = RCTreeMouseListenerAdapter.this.selectedNodes.get(0).get();
                    if(selNode != null) {
                        final List<RCTreeNode> l = selNode.getAllChildren();
                        new NodeAttributeFinder(l, selNode, true) {
                            @Override
                            protected boolean matches(final RCTreeNode node) {
                                // We are looking only for applications in error
                                boolean found = false;

                                final RunControlApplicationData.ApplicationISStatus isStatus = node.getUserObject().getISStatus();
                                if(isStatus.isEnabled() == true) {
                                    if(isStatus.isError() == true) {
                                        found = true;
                                    } else {
                                        final TreeNode p = node.getParent();
                                        if(p != null) {
                                            final RCTreeNode pNode = (RCTreeNode) p;
                                            if(pNode.getUserObject().getISStatus().getBadChildren().contains(node.toString()) == true) {
                                                found = true;
                                            }
                                        }
                                    }
                                }

                                return found;
                            }
                        }.execute();
                    } else {
                        IguiLogger.debug("Trying to access an already garbage collected node");
                    }
                }
            });
        }

        /**
         * It sends a command to the parent of the selected node.
         * 
         * @param command The command to send
         * @see Igui#sendControllerCommand(String, daq.rc.Command)
         */
        private void sendParentCommand(final daq.rc.Command command) {
            assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

            final RCTreeNode n = this.parentNode.get();
            if(n != null) {
                final String parentNodeName = n.toString();

                RunControlTreePanel.this.executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        final String commandName = command.toString();
                        IguiLogger.info("Sending the command \"" + commandName + "\" to \"" + parentNodeName + "\"");

                        try {
                            Igui.instance().sendControllerCommand(parentNodeName, command);
                        }
                        catch(final RCException ex) {
                            final String errMsg = "Failed sending the \"" + command + "\" command  to \"" + parentNodeName + "\": " + ex;
                            IguiLogger.error(errMsg, ex);
                            ErrorFrame.showError("IGUI - Command Error", errMsg, ex);
                        }
                    }
                });
            }
        }

        /**
         * It sends a series of commands to the parent of the selected node.
         * <p>
         * Commands are executed sequentially (a command is considered as executed when the remote call returns).
         * 
         * @param commands The commands to be executed
         */
        private void sendParentCommands(final List<? extends daq.rc.Command> commands) {
            assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

            final RCTreeNode n = this.parentNode.get();
            if(n != null) {
                final String parentNodeName = n.toString();

                RunControlTreePanel.this.executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        for(final daq.rc.Command command : commands) {
                            final String commandName = command.toString();
                            IguiLogger.info("Sending the command \"" + commandName + "\" to \"" + parentNodeName + "\"");

                            try {
                                Igui.instance().sendControllerCommand(parentNodeName, command);
                            }
                            catch(final RCException ex) {
                                final String errMsg =
                                                    "Failed sending the \"" + command + "\" command  to \"" + parentNodeName + "\": " + ex;
                                IguiLogger.error(errMsg, ex);
                                ErrorFrame.showError("IGUI - Command Error", errMsg, ex);

                                // Stop sending commands if one fails
                                break;
                            }
                        }
                    }
                });
            }
        }

        /**
         * It sends the "Ignore Error" command to the selected nodes.
         * 
         * @see Igui#sendControllerCommand(String, Command)
         */
        private void sendIgnoreError() {
            assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

            final String[] selNodes = this.nodeListAsArray();

            RunControlTreePanel.this.executor.execute(new Runnable() {
                @Override
                public void run() {
                    for(final String nodeName : selNodes) {
                        IguiLogger.info("Sending the command \"" + daq.rc.Command.RCCommands.IGNORE_ERROR.toString() + "\" to \"" + nodeName
                                        + "\"");
                        try {
                            Igui.instance().sendControllerCommand(nodeName, new Command.IgnoreErrorCmd());
                        }
                        catch(final RCException ex) {
                            final String errMsg = "Failed sending the \"" + daq.rc.Command.RCCommands.IGNORE_ERROR.toString()
                                                  + "\" command to \"" + nodeName + "\"";
                            IguiLogger.error(errMsg, ex);
                            ErrorFrame.showError("IGUI - Command Error", errMsg, ex);
                        }
                    }
                }
            });
        }

        /**
         * Enables or disables all the menu items.
         * 
         * @param enable If <code>true<code> all the items are enabled
         */
        private void enableItems(final boolean enable) {
            this.inMenuItem.setEnabled(enable);
            this.inMenuItemSeg.setEnabled(enable);
            this.outMenuItem.setEnabled(enable);
            this.outMenuItemSeg.setEnabled(enable);
            this.ignoreMenuItem.setEnabled(enable);
            this.restartMenuItem.setEnabled(enable);
            this.restartMenuItemSeg.setEnabled(enable);
            this.restartTTCPartitionMenuItem.setEnabled(enable);
            this.killMenuItem.setEnabled(enable);
            this.killMenuItemSeg.setEnabled(enable);
            this.findChildErrorAppMenuItem.setEnabled(enable);
            this.findLeafErrorAppMenuItem.setEnabled(enable);
            this.findLastChildErrorAppMenuItem.setEnabled(enable);
            this.detailedInfo.setEnabled(enable);
        }

        /**
         * It enables only the right items taking into account the nodes (and the parent) status.
         * <p>
         * <ul>
         * <li><b>IN</b>: disabled if the parent is not running or is busy;
         * <li><b>OUT</b>: disabled only if the parent is not running;
         * <li><b>KILL, RESTART</b>: disabled only if the parent is not running;
         * <li><b>IGNORE</b>: disabled if the application is not running, or is not a controller, or is not in error;
         * <li><b>RETRY</b>: disabled if the application is not running, or is not a controller, or is not in error, or is busy;
         * </ul>
         * <p>
         * If the Root Controller is selected then IN, OUT, KILL and RESTART(S) are always disabled.
         * <p>
         * To enable/disable menu items only one selected node is taken into account since the selection model enables to select only
         * equivalent nodes (see {@link RCTreeSelectionModel}). This could become wrong if the selection model is changed.
         */
        private void enableItems() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // By default all the items are enabled: look for situations
            // in which they have to be disabled
            this.enableItems(true);

            final RCTreeNode firstSelNode = this.selectedNodes.get(0).get();
            if(firstSelNode != null) {
                // Here we consider only the first one of the selected nodes: this is fine until
                // the current selection model is not changed
                final RunControlApplicationData selNodeData = firstSelNode.getUserObject();
                final RunControlApplicationData.ApplicationISStatus selNodeISStatus = selNodeData.getISStatus();
                final boolean isAppUp = selNodeISStatus.isUp();
                final boolean isAppError = selNodeISStatus.isError();
                final boolean isAppEnabled = selNodeISStatus.isEnabled();
                final boolean isController = selNodeData.isController();
                final boolean isRoot = firstSelNode.isRoot();

                this.advancedSegMenu.setEnabled(isController);

                // IGNORE
                if((isController == false) || (isAppUp == false) || (isAppError == false)) {
                    this.ignoreMenuItem.setEnabled(false);
                }

                // ERROR FINDER
                if(isAppError == false) {
                    this.findChildErrorAppMenuItem.setEnabled(false);
                    this.findLeafErrorAppMenuItem.setEnabled(false);
                    this.findLastChildErrorAppMenuItem.setEnabled(false);
                } else {
                    // Here do not ask the model to know whether the node is a leaf or not.
                    // Example: if the Online Segment is hidden and the RootController has no child segments, then
                    // the model would say that the RootController is a leaf (and this is right from the model
                    // point of view) but we want to search anyway in the Online Segment applications.
                    if(firstSelNode.isLeaf() == true) {
                        this.findChildErrorAppMenuItem.setEnabled(false);
                        this.findLastChildErrorAppMenuItem.setEnabled(false);
                    }
                }

                if(isRoot == true) {
                    // If Root Controller disable some items
                    this.inMenuItem.setEnabled(false);
                    this.inMenuItemSeg.setEnabled(false);
                    this.outMenuItem.setEnabled(false);
                    this.outMenuItemSeg.setEnabled(false);
                    this.killMenuItem.setEnabled(false);
                    this.killMenuItemSeg.setEnabled(false);
                    this.restartMenuItem.setEnabled(false);
                    this.restartMenuItemSeg.setEnabled(false);
                    this.restartTTCPartitionMenuItem.setEnabled(false);
                    this.findLeafErrorAppMenuItem.setEnabled(false);
                } else {
                    // This is not the Root Controller, get info about the parent
                    final RCTreeNode pn = this.parentNode.get();
                    if(pn != null) {
                        if(isAppEnabled == true) {
                            this.inMenuItem.setEnabled(false);
                        } else {
                            this.outMenuItem.setEnabled(false);
                        }

                        final RunControlApplicationData.ApplicationISStatus rcParentNodeISStatus = pn.getUserObject().getISStatus();

                        final boolean isParentUp = rcParentNodeISStatus.isUp();
                        if(isParentUp == false) {
                            this.inMenuItem.setEnabled(false);
                            this.inMenuItemSeg.setEnabled(false);
                            this.outMenuItem.setEnabled(false);
                            this.outMenuItemSeg.setEnabled(false);
                            this.restartMenuItem.setEnabled(false);
                            this.restartMenuItemSeg.setEnabled(false);
                            this.restartTTCPartitionMenuItem.setEnabled(false);
                            this.killMenuItem.setEnabled(false);
                            this.killMenuItemSeg.setEnabled(false);
                        } else {
                            if(isAppUp == false) {
                                this.killMenuItem.setEnabled(false);
                            }

                            // The 'restart TTC partition' command is enabled only if the controller controls a TTC partition and
                            // the parent is in the RUNNING state
                            // The two types of restart (TTC partition and 'normal' restarts) are never enabled at the same time
                            final RunControlFSM.State parentState = rcParentNodeISStatus.getState();
                            final boolean isTTCPartitionController = selNodeData.isTTPartitionController();
                            final RunControlFSM.State rootState =
                                                                ((RCTreeNode) RunControlTreePanel.this.treeModel.getRoot()).getUserObject().getISStatus().getState();
                            if((isController == false) || (isTTCPartitionController == false)
                               || (parentState.equals(RunControlFSM.State.RUNNING) == false)
                               || (rootState.equals(RunControlFSM.State.RUNNING) == false))
                            {
                                this.restartTTCPartitionMenuItem.setEnabled(false);

                                if((parentState.equals(RunControlFSM.State.RUNNING) == true)
                                   && (selNodeData.isRestartableInRunning() == false))
                                {
                                    this.restartMenuItem.setEnabled(false);
                                    this.restartMenuItemSeg.setEnabled(false);
                                }

                            } else {
                                this.restartMenuItem.setEnabled(false);
                                this.restartMenuItemSeg.setEnabled(false);
                            }
                        }
                    } else {
                        IguiLogger.debug("Trying to access an already garbage collected node");
                    }
                }
            } else {
                IguiLogger.debug("Trying to access an already garbage collected node");
            }
        }
    }

    /**
     * Constructor: it build the panel GUI.
     * 
     * @param mainRCPanel The parent panel
     */
    RunControlTreePanel(final RunControlMainPanel mainRCPanel) {
        super(mainRCPanel);
        this.initGUI();
    }

    /**
     * It builds the run control tree.
     * 
     * @throws IguiPanelError
     * @throws ConfigException
     * @throws ISException
     * @see Igui.IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     * @see #buildRCTree()
     */
    @Override
    public void panelInit(final State rcState, final IguiConstants.AccessControlStatus accessStatus)
        throws ConfigException,
            IguiPanelError,
            ISException
    {
        // Build the tree
        try {
            this.buildRCTree();
        }
        catch(final InterruptedException ex) {
            final String errMsg = "The initialization task for panel \"" + this.getPanelName() + "\" has been interrupted";
            IguiLogger.warning(errMsg);
            Thread.currentThread().interrupt();
            throw new IguiPanelError(this.getPanelName(), errMsg, ex);
        }

        // Subscribe to IS servers
        if(this.runCtrlReceiver.isSubscribed() == false) {
            this.runCtrlReceiver.subscribe();
            IguiLogger.info("Subscribed to IS server \"" + IguiConstants.RC_IS_SERVER_NAME + "\"");
        }

        // Install the search bar: it can be installed only after the tree is ready
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                RunControlTreePanel.this.installSearchBar();
            }
        });
    }

    /**
     * Empty method: the access control status is checked by the mouse listener when building the contextual menu.
     * 
     * @see Igui.IguiPanel#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void accessControlChanged(final IguiConstants.AccessControlStatus newAccessStatus) {
    }

    /**
     * The run control tree is re-built.
     * 
     * @see Igui.IguiPanel#dbReloaded()
     */
    @Override
    public void dbReloaded() {
        // Reinitialize the tree
        try {
            this.panelInit(Igui.instance().getRCState(), Igui.instance().getAccessLevel());
        }
        catch(final IguiException ex) {
            ErrorFrame.showError("Run Control Panel Error", "Database reload operation failed: " + ex.getMessage(), ex);
            IguiLogger.error("Panel \"" + this.getPanelName() + "\" failed the DB reloading: " + ex.getMessage(), ex);
        }
    }

    /**
     * @see Igui.IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return RunControlTreePanel.name;
    }

    /**
     * @see IguiPanel#panelDeselected()
     */
    @Override
    public void panelDeselected() {
    }

    /**
     * @see IguiPanel#panelSelected()
     */
    @Override
    public void panelSelected() {
    }

    /**
     * The executor is stopped.
     * 
     * @see IguiPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        try {
            this.runCtrlReceiver.unsubscribe();
            IguiLogger.info("Unsubscribed from IS \"" + this.runCtrlReceiver.getName() + "\"");
        }
        catch(final IguiException.ISException ex) {
            IguiLogger.error("Error terminating panel \"" + this.getPanelName() + "\": " + ex.getMessage(), ex);
        }
        finally {
            // Shutdown the call-back executor
            this.executor.shutdown();
        }

        IguiLogger.debug("Panel \"" + this.getPanelName() + "\" has been terminated");
    }

    /**
     * @see IguiPanel#refreshISSubscription()
     */
    @Override
    public void refreshISSubscription() {
        try {
            // Unsubscribe
            try {
                this.runCtrlReceiver.unsubscribe();
            }
            catch(final ISException ex) {
                final String errMsg = "Failed to unsubscribe from \"" + this.runCtrlReceiver.getName()
                                      + "\" while refreshing IS subscription";
                IguiLogger.warning(errMsg + ". Got exception: " + ex, ex);
            }

            // Set all the nodes as if the IS info has never been loaded
            // and the node has never been expanded
            final Collection<RCTreeNode> allNodes = this.nodeMap.values();
            for(final RCTreeNode n : allNodes) {
                n.setInfoLoaded(false);
                n.setHasBeenExpanded(false);
            }

            // Subscribe again
            this.runCtrlReceiver.subscribe();

            // Collapse and expand the root node (load info again from IS)
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final RCTreeNode rn = (RCTreeNode) RunControlTreePanel.this.treeModel.getRoot();
                    RunControlTreePanel.this.rcTree.collapseAll();
                    RunControlTreePanel.this.rcTree.expandRow(0);
                    RunControlTreePanel.this.rcTree.setSelectionPath(new TreePath(new Object[] {rn}));

                    // Fix for the case in which the RootController has no children
                    if(RunControlTreePanel.this.treeModel.getChildCount(rn) == 0) {
                        RunControlTreePanel.this.executor.execute(new ISNodeInfoWorker(rn));
                    }
                }
            });
        }
        catch(final ISException ex) {
            final String errMsg = "Failed to refresh the subscription to IS servers, the Run Control tree may not be updated correctly";
            IguiLogger.warning(errMsg + ". Got exception: " + ex, ex);
            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
            ErrorFrame.showError("Run Control Panel error", errMsg + ".\n Try again to refresh the IS subscription.", ex);
        }
    }

    /**
     * It return the currently selected tree node.
     * <p>
     * It is safe to call this method outside the EDT.
     * 
     * @return The currently selected tree node (or <code>null</code> if no node is selected)
     */
    RCTreeNode getSelectedNode() {
        RCTreeNode selNode = null;

        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            final TreePath[] selPaths = RunControlTreePanel.this.rcTree.getSelectionPaths();
            if((selPaths != null) && (selPaths.length > 0)) {
                selNode = ((RCTreeNode) selPaths[selPaths.length - 1].getLastPathComponent());
            }
        } else {
            final FutureTask<RCTreeNode> task = new FutureTask<RCTreeNode>(new Callable<RCTreeNode>() {
                @Override
                public RCTreeNode call() throws Exception {
                    final TreePath[] selPaths = RunControlTreePanel.this.rcTree.getSelectionPaths();
                    if((selPaths != null) && (selPaths.length > 0)) {
                        return((RCTreeNode) selPaths[selPaths.length - 1].getLastPathComponent());
                    }

                    return null;
                }
            });

            javax.swing.SwingUtilities.invokeLater(task);

            try {
                selNode = task.get();
            }
            catch(final Exception ex) {
                final String errMsg = "Cannot get the currently selected RC tree node: " + ex;
                IguiLogger.error(errMsg, ex);
                if(InterruptedException.class.isInstance(ex)) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        return selNode;
    }

    /**
     * It returns the tree node corresponding to the application whose name is <em>appName</em>
     * 
     * @param appName The name of the application
     * @return The tree node corresponding to the application or <em>null</em> is the application does not exist
     */
    RCTreeNode getNode(final String appName) {
        return this.nodeMap.get(appName);
    }

    /**
     * It adds an <code>AbstractRunControlPanel</code>tree to the list of tree selection listeners.
     * 
     * @param tsl The tree selection listener
     */
    void addSelectionListener(final TreeSelectionListener tsl) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        this.rcTree.addTreeSelectionListener(tsl);
    }

    /**
     * It adds an <code>AbstractRunControlPanel</code>tree to the list of tree model listeners.
     * 
     * @param tml The tree model listener
     */
    void addModelListener(final TreeModelListener tml) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        this.treeModel.addTreeModelListener(tml);
    }

    /**
     * The node information (and its visual representation) is updated taking into account the updates coming from the run control IS
     * server. This method is always executed in a thread pool when an IS call-back is received.
     * 
     * @param infoEvent The IS information update
     * @param removed If <code>true</code> the information has been removed from IS
     * @see RunControlApplicationData#updateInfo(rc.DAQApplicationInfo, boolean)
     * @see RunControlApplicationData#updateInfo(rc.RCStateInfo, boolean)
     */
    private void rcISCallback(final is.InfoEvent infoEvent, final boolean removed) {
        try {
            final String fullInfoName = infoEvent.getName();
            final is.Type infoType = infoEvent.getType();

            RCTreeNode nodeChanged = null;

            if(infoType.equals(rc.DAQApplicationInfo.type)) {
                // DAQApplicationInfo: from the supervisor
                final rc.DAQApplicationInfo daqApplicationInfo = RunControlTreePanel.DAQ_APP_INFO.get();
                infoEvent.getValue(daqApplicationInfo);

                // Get a reference to the node this information refers to
                final String controllerName = daqApplicationInfo.applicationName;
                nodeChanged = RunControlTreePanel.this.nodeMap.get(controllerName);

                if(nodeChanged != null) {
                    if((nodeChanged.hasBeenExpanded() == true) || (nodeChanged.isInfoLoaded() == true)) {
                        // Make a check on the information time stamp only if the IS call-back refers
                        // to a not removed information
                        final RunControlApplicationData nodeData = nodeChanged.getUserObject();
                        if(!removed) {
                            final long timeCompare = nodeData.updateInfo(daqApplicationInfo, false);
                            if(timeCompare == 0) {
                                try {
                                    this.runCtrlReceiver.getRepository().getValue(fullInfoName, daqApplicationInfo);

                                    // Update the node information
                                    nodeData.updateInfo(daqApplicationInfo, true);
                                }
                                catch(final is.InfoNotFoundException ex) {
                                    // If we are here it means the the info is no more in IS
                                    nodeData.resetSupervisorInfo();
                                }
                            }
                        } else {
                            // If the information has been removed from IS reset to its default values
                            nodeData.resetSupervisorInfo(daqApplicationInfo.getTime().getTimeMicro(), false);
                        }

                        RunControlTreePanel.this.nodeChanged(nodeChanged);
                    }
                }
            } else if(infoType.equals(rc.RCStateInfo.type)) {
                // RCStateInfo: from the controller itself
                final rc.RCStateInfo runControlInfo = RunControlTreePanel.RC_STATE_INFO.get();
                infoEvent.getValue(runControlInfo);

                // Get a reference to the node this information refers to
                final String controllerName = fullInfoName.substring(IguiConstants.RC_IS_SERVER_NAME.length() + 1);
                nodeChanged = RunControlTreePanel.this.nodeMap.get(controllerName);

                if(nodeChanged != null) {
                    if((nodeChanged.hasBeenExpanded() == true) || (nodeChanged.isInfoLoaded() == true)) {
                        // Make a check on the information time stamp only if the IS call-back refers
                        // to a not removed information
                        final RunControlApplicationData nodeData = nodeChanged.getUserObject();
                        if(!removed) {
                            final long timeCompare = nodeData.updateInfo(runControlInfo, false);
                            if(timeCompare == 0) {
                                try {
                                    this.runCtrlReceiver.getRepository().getValue(fullInfoName, runControlInfo);

                                    // Update the node information
                                    nodeData.updateInfo(runControlInfo, true);
                                }
                                catch(final is.InfoNotFoundException ex) {
                                    // If we are here it means the the info is no more in IS
                                    nodeData.resetRCInfo();
                                }
                            }
                        } else {
                            // If the information has been removed from IS reset to its default values
                            nodeData.resetRCInfo(runControlInfo.getTime().getTimeMicro(), false);
                        }

                        RunControlTreePanel.this.nodeChanged(nodeChanged);
                    }
                }
            }
        }
        catch(final Exception ex) {
            IguiLogger.error("Error processing IS callback for event \"" + infoEvent.getName() + "\": " + ex.getMessage(), ex);
        }
    }

    /**
     * It creates the tree root node (i.e., the tree node representing the Root Controller and all its direct children - applications from
     * the online segment) and set it as the root of the tree (calling the tree model).
     * 
     * @param onlSeg The object representing the Online Segment
     * @throws IguiException.ConfigException It was not possible to create the root node because of a problem with the database system.
     */
    private void createAndSetRootNode(final dal.Segment onlSeg) throws IguiException.ConfigException {
        try {
            // Create the root node
            final dal.BaseApplication rootCtrl = onlSeg.get_controller();
            final RCTreeNode rootNode = new RCTreeNode(RunControlApplicationData.createControllerApplicationData(onlSeg));

            this.addNodeToMap(rootCtrl.UID(), rootNode);

            // Add applications

            // Online segment applications applications
            final RCTreeNode virtualOnlSeg = new RCTreeNode(rootNode, RunControlTreePanel.onlSegNodeName, Igui.createIcon("segment.png"));
            {
                final dal.BaseApplication[] apps = onlSeg.get_applications();
                if(apps != null) {
                    for(final dal.BaseApplication app_ : apps) {
                        final RCTreeNode childNode = new RCTreeNode(RunControlApplicationData.createApplicationData(app_));
                        virtualOnlSeg.add(childNode);

                        this.addNodeToMap(app_.UID(), childNode);
                    }
                }
            }

            // Infrastructure
            final RCTreeNode virtualInfr = new RCTreeNode(rootNode, RunControlTreePanel.infrNodeName, Igui.createIcon("infrNode.png"));
            {
                final dal.BaseApplication[] infrApps = onlSeg.get_infrastructure();
                if(infrApps != null) {
                    for(final dal.BaseApplication app_ : infrApps) {
                        final RCTreeNode childNode =
                                                   new RCTreeNode(RunControlApplicationData.createInfrastructureApplicationData(app_,
                                                                                                                                onlSeg.UID()));
                        virtualInfr.add(childNode);

                        this.addNodeToMap(app_.UID(), childNode);
                    }
                }

                final dal.Segment[] nes = onlSeg.get_nested_segments();
                if(nes != null) {
                    for(final dal.Segment ns : nes) {
                        if(ns.is_disabled() == false) {
                            final dal.BaseApplication[] apps = ns.get_infrastructure();
                            if(apps != null) {
                                for(final dal.BaseApplication app_ : apps) {
                                    final RCTreeNode childNode =
                                                               new RCTreeNode(RunControlApplicationData.createInfrastructureApplicationData(app_,
                                                                                                                                            onlSeg.UID()));
                                    virtualInfr.add(childNode);

                                    this.addNodeToMap(app_.UID(), childNode);
                                }
                            }
                        }
                    }
                }
            }

            if(virtualOnlSeg.getChildCount() > 0) {
                rootNode.add(virtualOnlSeg);
            }

            if(virtualInfr.getChildCount() > 0) {
                rootNode.add(virtualInfr);
            }

            // Set the rootNode as the root of the RC tree
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    RunControlTreePanel.this.treeModel.setRoot(rootNode);
                }
            });
        }
        catch(final config.ConfigException | IguiException.ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting the RootController: " + ex.getMessage(), ex);
        }
    }

    /**
     * It creates a top segment node and populates it with all the child applications and, recursively, with all its sub-segments.
     * <p>
     * Call this method for a top node and all the sub-tree structure will be filled. This method is called for all the segments whose first
     * parent is the Root Controller.
     * 
     * @param seg The {@link dal.Segment} object describing the segment tree node
     * @return The segment tree node, or <code>null</code> if that segment is disabled in the database
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     * @see #populateSegment(RCTreeNode, dal.Segment)
     */
    private RCTreeNode createAndPopulateSegmentNode(final dal.Segment seg) throws IguiException.ConfigException {
        try {
            RCTreeNode segmentNode = null;

            // Get the segment controller
            final dal.BaseApplication segController = seg.get_controller();
            if(segController != null) {
                // Create the segment node
                segmentNode = new RCTreeNode(RunControlApplicationData.createControllerApplicationData(seg));
                this.addNodeToMap(segController.UID(), segmentNode);

                // Recursively populate the segment and all its sub-segments
                this.populateSegment(segmentNode, seg);
            }

            return segmentNode;
        }
        catch(final config.ConfigException | IguiException.ConfigException ex) {
            throw new IguiException.ConfigException("Failed creating segment " + seg.UID() + ": " + ex.getMessage(), ex);
        }
    }

    /**
     * It creates a segment node, populates it with all the child applications and sub-segments, and then adds the segment to the parent
     * node list.
     * 
     * @param parentNode The parent node
     * @param seg The {@link dal.Segment} object describing the segment tree node
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     * @see #createAndPopulateSegmentNode(dal.Segment)
     */
    private void createAndPopulateSegmentNode(final RCTreeNode parentNode, final dal.Segment seg) throws IguiException.ConfigException {
        final RCTreeNode segmentNode = this.createAndPopulateSegmentNode(seg);
        if(segmentNode != null) {
            // Add the segment to its parent
            synchronized(parentNode) {
                parentNode.add(segmentNode);
            }
        }
    }

    /**
     * It adds child applications to a tree node.
     * 
     * @param parentNode The parent node
     * @param apps The {@link dal.BaseApplication} array containing all the child applications
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     */
    private void addNodeApplications(final RCTreeNode parentNode, final dal.BaseApplication[] apps) throws IguiException.ConfigException {
        try {
            synchronized(parentNode) {
                for(final dal.BaseApplication app : apps) {
                    final RCTreeNode childNode = new RCTreeNode(RunControlApplicationData.createApplicationData(app));
                    parentNode.add(childNode);
                    this.addNodeToMap(app.UID(), childNode);
                }
            }
        }
        catch(final IguiException.ConfigException ex) {
            throw new IguiException.ConfigException("Failed adding application to the node " + parentNode.toString() + ": "
                                                    + ex.getMessage(),
                                                    ex);
        }
    }

    /**
     * It recursively populates segments with applications and sub-segments.
     * 
     * @param parentNode The node to populate
     * @param topSegment The {@link dal.Segment} object describing the node to populate
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     * @see #createAndPopulateSegmentNode(RCTreeNode, dal.Segment)
     */
    private void populateSegment(final RCTreeNode parentNode, final dal.Segment topSegment) throws IguiException.ConfigException {
        try {
            // Get all the applications belonging to the topSegment

            // First the infrastructure applications
            final dal.Segment[] ns = topSegment.get_nested_segments();
            if((ns != null) && (ns.length != 0)) {
                final RCTreeNode virtual = new RCTreeNode(parentNode, RunControlTreePanel.infrNodeName, Igui.createIcon("infrNode.png"));

                for(final dal.Segment nestedSeg : ns) {
                    if(nestedSeg.is_disabled() == false) {
                        final dal.BaseApplication[] infrApps = nestedSeg.get_infrastructure();
                        if(infrApps != null) {
                            for(final dal.BaseApplication app_ : infrApps) {
                                final RCTreeNode childNode =
                                                           new RCTreeNode(RunControlApplicationData.createInfrastructureApplicationData(app_,
                                                                                                                                        topSegment.UID()));
                                virtual.add(childNode);

                                this.addNodeToMap(app_.UID(), childNode);
                            }
                        }
                    }
                }

                // Do not show the "virtual" node if no applications are available
                if(virtual.getChildCount() > 0) {
                    synchronized(parentNode) {
                        parentNode.add(virtual);
                    }
                }
            }

            // Now all the other apps
            final dal.BaseApplication[] segApps = topSegment.get_applications();
            if(segApps != null) {
                this.addNodeApplications(parentNode, segApps);
            }

            // Get (if any) all the topSegment sub-segments
            final dal.Segment[] subSegments = topSegment.get_nested_segments();
            if(subSegments != null) {
                for(final dal.Segment seg : subSegments) {
                    if(seg.is_disabled() == false) {
                        this.createAndPopulateSegmentNode(parentNode, seg);
                    }
                }
            }
        }
        catch(final config.ConfigException | IguiException.ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting applications for segment " + topSegment.UID() + ": "
                                                    + ex.getMessage(),
                                                    ex);
        }
    }

    /**
     * It build the run control tree.
     * 
     * @throws IguiException.ConfigException Thrown if some error occurs with the database system interaction
     * @throws IguiException.IguiPanelError
     * @throws InterruptedException
     */
    private void buildRCTree() throws IguiException.ConfigException, IguiException.IguiPanelError, InterruptedException {
        IguiLogger.debug("Building the RC tree...");

        final long startTime = System.nanoTime();

        // Clear the tree
        this.nodeMap.clear();

        // Reset the tree
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                RunControlTreePanel.this.treeModel.setRoot(null);
                // RunControlTreePanel.this.treeModel.treeStructureChanged();
            }
        });

        // All segments container
        final List<dal.Segment> segList = new ArrayList<dal.Segment>();

        try {
            // Create the node representing the RootController and set it as the root of the tree
            final dal.Segment onlSeg = Igui.instance().getOnlineSegment();
            this.createAndSetRootNode(onlSeg);

            // Get top segments and navigate into all the sub-segments
            final dal.Segment[] segArray = onlSeg.get_nested_segments();
            if(segArray != null) {
                Collections.addAll(segList, segArray);
            }
        }
        catch(final config.ConfigException ex) {
            throw new IguiException.ConfigException("Failed getting partition top segments: " + ex.getMessage(), ex);
        }

        // List that will contain all the children of the root node
        final List<RCTreeNode> rootChildList = new ArrayList<RCTreeNode>();

        if(segList.size() > 0) {
            // Top segments will be built in different threads
            final List<Future<RCTreeNode>> taskList = new ArrayList<Future<RCTreeNode>>();

            // Submit tasks building top segments to the executor
            try {
                for(final dal.Segment seg : segList) {
                    if(seg.is_disabled() == false) {
                        final Future<RCTreeNode> task = this.executor.submit(new Callable<RCTreeNode>() {
                            @Override
                            public RCTreeNode call() throws Exception {
                                // WARNING: return may be null
                                return RunControlTreePanel.this.createAndPopulateSegmentNode(seg);
                            }
                        });
    
                        taskList.add(task);
                    }
                }
            }
            catch(final config.ConfigException ex) {
                throw new IguiException.ConfigException("Failed getting the disabled status of segments: " + ex.getMessage(), ex);
            }

            try {
                for(final Future<RCTreeNode> tsk : taskList) {
                    final RCTreeNode node = tsk.get();
                    if(node != null) {
                        rootChildList.add(node);
                    }
                }
            }
            catch(final InterruptedException ex) {
                throw ex;
            }
            catch(final Exception ex) {
                throw new IguiException.IguiPanelError(this.getPanelName(), ex);
            }
        }

        // Add all the children to the root node and set the root of the tree
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final RCTreeNode root = (RCTreeNode) RunControlTreePanel.this.treeModel.getRoot();

                for(final RCTreeNode node : rootChildList) {
                    // We need to use this method because the root of the tree has already been
                    // specified (see createAndSetRootNode)
                    // RunControlTreePanel.this.treeModel.addNodeToRoot(node);
                    root.add(node);
                }

                RunControlTreePanel.this.treeModel.nodeStructureChanged(root);
                RunControlTreePanel.this.rcTree.collapseAll();
                RunControlTreePanel.this.rcTree.expandRow(0);
                RunControlTreePanel.this.rcTree.setSelectionPath(new TreePath(new Object[] {root}));

                // Fix for the case in which the RootController has no children
                if(RunControlTreePanel.this.treeModel.getChildCount(root) == 0) {
                    RunControlTreePanel.this.executor.execute(new ISNodeInfoWorker(root));
                }
            }
        });

        final long stopTime = System.nanoTime();

        IguiLogger.debug("RC tree built in " + (stopTime - startTime) + " ns");
    }

    /**
     * It installs the tree search bar.
     * <p>
     * The search bar is installed in {@link #panelInit(State, Igui.IguiConstants.AccessControlStatus)}. It cannot be added in
     * {@link #initGUI()} because it needs the tree fully built to work properly.
     */
    private void installSearchBar() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        if(this.isSearchBarInstalled == false) {
            final TreeSearchable ts = new RCTreeSearchable();
            ts.setRecursive(true);
            ts.setRepeats(true);
            ts.setSearchingDelay(300);

            final SearchableBar sb = new SearchableBar(ts, true);
            sb.setOpaque(false);
            sb.setVisibleButtons(SearchableBar.SHOW_MATCHCASE | SearchableBar.SHOW_NAVIGATION | SearchableBar.SHOW_REPEATS
                                 | SearchableBar.SHOW_HIGHLIGHTS);
            sb.setBorderPainted(false);
            sb.setFloatable(false);

            RunControlTreePanel.this.getMainPanel().addToToolbar(sb);
            this.isSearchBarInstalled = true;
        }
    }

    /**
     * It signals the tree model that a node changed its status.
     * <p>
     * It can be invoked from any thread.
     * 
     * @param node The node whose status has changed.
     */
    private void nodeChanged(final RCTreeNode node) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                RunControlTreePanel.this.treeModel.nodeChanged(node);
            }
        });
    }

    /**
     * It adds a node to the map used to retrieve tree nodes from application's name
     * <p>
     * It checks whether an attempt to add two nodes with the same name is done
     * 
     * @param nodeID The ID of the node (i.e., the application name)
     * @param node The corresponding node in the tree
     * @throws ConfigException A node with the same ID was already existing
     */
    private void addNodeToMap(final String nodeID, final RCTreeNode node) throws ConfigException {
        final RCTreeNode prev = this.nodeMap.putIfAbsent(nodeID, node);
        if(prev != null) {
            final StringBuilder msg = new StringBuilder();
            msg.append("Cannot properly build the RC tree: two applications with the same name have been detected:\n");
            msg.append("- application \"").append(node.getUserObject().getName()).append("@").append(node.getUserObject().getClassName()).append("\" belonging to segment \"").append(node.getUserObject().getSegmentName()).append("\"\n");
            msg.append("- application \"").append(prev.getUserObject().getName()).append("@").append(prev.getUserObject().getClassName()).append("\" belonging to segment \"").append(prev.getUserObject().getSegmentName()).append("\"\n");
            throw new ConfigException(msg.toString());
        }
    }

    /**
     * It initializes this panel GUI.
     */
    private final void initGUI() {
        this.setLayout(new BorderLayout());

        this.rcTree.setShowsRootHandles(true);
        this.rcTree.setLargeModel(true);
        this.rcTree.setDoubleBuffered(true);
        this.rcTree.setAutoscrolls(true);
        this.rcTree.setSelectionModel(new RCTreeSelectionModel());
        this.rcTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        this.rcTree.setCellRenderer(new RCTreeCellRenderer());
        this.rcTree.addTreeWillExpandListener(this.treeExpListener);
        this.rcTree.addTreeSelectionListener(new RCTreeSelectionListener());
        this.rcTree.addMouseListener(new RCTreeMouseListenerAdapter());

        ToolTipManager.sharedInstance().registerComponent(this.rcTree);

        final JScrollPane scr = new JScrollPane(this.rcTree);
        this.add(scr, BorderLayout.CENTER);
    }

}
