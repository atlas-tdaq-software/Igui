package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.text.html.HTMLEditorKit;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;


/**
 * Class to show a frame with the error messages. All static methods can be called in any thread (i.e., there is no need to call them
 * explicitly in the EDT).
 * 
 * @author L. Moneta
 * @author J. Flammer
 * @author G. Avolio
 */

public class ErrorFrame {

    /**
     * A simple JOptionPane sub-class overriding {@link #getMaxCharactersPerLineCount()}.
     */
    static class IguiOptionPane extends JOptionPane {
        private static final long serialVersionUID = 6212361690509696238L;

        IguiOptionPane() {
            super();
        }

        IguiOptionPane(final Object message,
                       final int messageType,
                       final int optionType,
                       final Icon icon,
                       final Object[] options,
                       final Object initialValue)
        {
            super(message, messageType, optionType, icon, options, initialValue);
        }

        IguiOptionPane(final Object message, final int messageType, final int optionType, final Icon icon, final Object[] options) {
            super(message, messageType, optionType, icon, options);
        }

        IguiOptionPane(final Object message, final int messageType, final int optionType, final Icon icon) {
            super(message, messageType, optionType, icon);
        }

        IguiOptionPane(final Object message, final int messageType, final int optionType) {
            super(message, messageType, optionType);
        }

        IguiOptionPane(final Object message, final int messageType) {
            super(message, messageType);
        }

        IguiOptionPane(final Object message) {
            super(message);
        }

        @Override
        public int getMaxCharactersPerLineCount() {
            return IguiConstants.OPTION_PANE_MAX_CHAR_PER_LINE;
        }
    }

    /**
     * Empty constructor (it is useless to have instance of this class).
     */
    private ErrorFrame() {
    }

    /**
     * Method to show error messages.
     * 
     * @param msg The error message.
     */
    public static void showError(final String msg) {
        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            ErrorFrame.showDialog(msg, "IGUI ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ErrorFrame.showDialog(msg, "IGUI ERROR", JOptionPane.ERROR_MESSAGE);
                }
            });
        }
    }

    /**
     * Method to show error messages very useful when the content of an exception has to be reported to the user.
     * 
     * @param title The dialog panel title.
     * @param msg The error message.
     * @param ex The exception causing the error.
     */
    public static void showError(final String title, final String msg, final Throwable ex) {
        ErrorFrame.showError(title, msg, ex, true);
    }
    
    /**
     * Method to show error messages very useful when the content of an exception has to be reported to the user.
     * 
     * @param title The dialog panel title.
     * @param msg The error message.
     * @param ex The exception causing the error.
     * @param showDetails Whether to expand the Throwable trace and show it
     */
    public static void showError(final String title, final String msg, final Throwable ex, final boolean showDetails) {
        final ErrorInfo errorInfo;
        
        if(showDetails == true) {
            final StringBuilder detailedMsg = new StringBuilder();

            try(final java.io.OutputStream ersoutput = new java.io.ByteArrayOutputStream()) {
                ers.IssuePrinter.println(ersoutput, ex, new ers.Severity(ers.Severity.level.Error), -1).close();
                detailedMsg.append(ersoutput.toString());
            }
            catch(final IOException exx) {
                exx.printStackTrace();
            }
            
            if(detailedMsg.length() > 0) {
                errorInfo = new ErrorInfo(title, msg + "\n\nFULL MESSAGE:\n" + detailedMsg.toString(), null, null, ex, null, null);            
            } else {
                errorInfo = new ErrorInfo(title, msg, null, null, ex, null, null);
            }
        } else {
            errorInfo = new ErrorInfo(title, msg, null, null, ex, null, null);
        }
                   
        JXErrorPane.showDialog(Igui.instance().getMainFrame(), errorInfo);
    }

    /**
     * Method to show information message
     * 
     * @param msg The information message.
     */
    public static void showInfo(final String msg) {
        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            ErrorFrame.showDialog(msg, "IGUI INFORMATION", JOptionPane.INFORMATION_MESSAGE);
        } else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ErrorFrame.showDialog(msg, "IGUI INFORMATION", JOptionPane.INFORMATION_MESSAGE);
                }
            });
        }

    }

    /**
     * Method to show log/diagnosis message.
     * 
     * @param msg The message text (html).
     * @param component The name of component.
     * @param icon The icon to be displayed for dialog (it may be null).
     * @param typeText The type of message (log/diagnosis).
     */
    public static void showMessage(final String msg, final String component, final ImageIcon icon, final String typeText) {
        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            ErrorFrame.showMessageImpl(msg, component, icon, typeText);
        } else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ErrorFrame.showMessageImpl(msg, component, icon, typeText);
                }
            });
        }
    }

    /**
     * Method to show warning messages.
     * 
     * @param msg The warning message.
     */
    public static void showWarning(final String msg) {
        if(javax.swing.SwingUtilities.isEventDispatchThread()) {
            ErrorFrame.showDialog(msg, "IGUI WARNING", JOptionPane.WARNING_MESSAGE);
        } else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ErrorFrame.showDialog(msg, "IGUI WARNING", JOptionPane.WARNING_MESSAGE);
                }
            });
        }
    }

    /**
     * Base method used to show information dialogs.
     * 
     * @param msg The message to be shown.
     * @param title The dialog title.
     * @param messageType The message type.
     * @see javax.swing.JOptionPane
     */
    private static void showDialog(final String msg, final String title, final int messageType) {
        final JOptionPane pane = new IguiOptionPane(msg, messageType, JOptionPane.DEFAULT_OPTION);
        final JDialog dialog = pane.createDialog(Igui.instance().getMainFrame(), title);
        dialog.setVisible(true);
    }

    /**
     * Implementation of {@link ErrorFrame#showMessage(String, String, ImageIcon, String)}.
     * 
     * @param msg The message text (html).
     * @param component The name of component.
     * @param icon The icon to be displayed for dialog (it may be null).
     * @param typeText The type of message (log/diagnosis).
     */
    private static void showMessageImpl(final String msg, final String component, final ImageIcon icon, final String typeText) {
        final JEditorPane textPane = new JEditorPane();
        textPane.setEditorKit(new HTMLEditorKit());
        textPane.setEditable(false);
        textPane.setText(msg);

        final JScrollPane scrollPane = new JScrollPane(textPane);
        scrollPane.setBorder(BorderFactory.createLoweredBevelBorder());

        final JPanel componentPanel = new JPanel();
        componentPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        final JLabel componentLabel1 = new JLabel("COMPONENT : ");
        componentLabel1.setFont(new Font("Default", 1, 14));

        final JLabel componentLabel2 = new JLabel("  " + component + "  ");
        componentLabel2.setBackground(Color.white);
        componentLabel2.setOpaque(true);
        componentLabel2.setBorder(BorderFactory.createLoweredBevelBorder());
        componentLabel2.setFont(new Font("Default", 1, 14));

        componentPanel.add(componentLabel1);
        componentPanel.add(componentLabel2);

        final JPanel mainPanel = new JPanel();

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(componentPanel, BorderLayout.NORTH);
        mainPanel.add(scrollPane, BorderLayout.CENTER);

        final JOptionPane pane = new IguiOptionPane(mainPanel, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, icon);
        pane.setMinimumSize(new Dimension(320, 320));

        final JDialog dialog = pane.createDialog(null, typeText + " for " + component);
        dialog.setResizable(true);
        dialog.setSize(new Dimension(800, 400));
        dialog.setLocationRelativeTo(Igui.instance().getMainFrame());
        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        dialog.setVisible(true);
    }
}
