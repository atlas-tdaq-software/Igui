package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.FormatStringValue;

import Igui.IguiConstants.AccessControlStatus;
import Igui.RunControlFSM.State;
import config.ConfigException;


/**
 * This class implements the "Data Set Tags" panel functionalities.
 * <p>
 * The panel shows a table reporting for each tag its name, its current value and whether or not the tag is used (i.e., it is published in
 * IS). If at the beginning no tags are present and then a new one is created, the panel takes care of creating the right object in the DB.
 * <p>
 * Tags are retrieved at the beginning from the DB and, if marked as used, their values are published in IS.
 */
class DSPanel extends IguiPanel {

    private final static long serialVersionUID = -4181876011678986419L;

    /**
     * The panel name.
     */
    private final static String pName = "Dataset Tags";

    /**
     * The IS server name used to store data set information.
     */
    private final static String isServerName = "RunParams.UserMetaData";

    /**
     * Separator used to separate the tag name and the tag value in IS.
     */
    private final static String tagISSeparator = ":=";

    /**
     * Delimiters used in DB containing the possible values for a defined tag.
     */
    private final static String[] dbTagValuesDelimeters = new String[] {"{", "}"};

    /**
     * Separator used in DB to separate the possible values for a defined tag.
     */
    private final static String dbTagValuesSeparator = ",";

    /**
     * Separator used in DB to separate the tage name and the tag values.
     */
    private final static String dbTagNameValuesSeparator = "=";

    /**
     * Prefix used as a name for the created RunTagList object (the full name will contain the partition name and a timestamp).
     */
    private final static String rtObjectPrefix = "RunTagList_";

    /**
     * Warning message to alert the used about not applied changes in tags.
     */
    private final static String rtlModifiedMsg = "The tag list has been modified, push the \"Apply Changes\" button to apply the changes";

    // Swing objects for the GUI
    private final JTextField tagNameField = new JTextField();
    private final JTextField tagValuesField = new JTextField();
    private final JButton addNewTagButton = new JButton("Add/Modify");
    private final JButton applyButton = new JButton("Apply Changes");
    private final ListCellRenderer listRenderer = new ListCellRenderer();
    private final ListCellEditor listEditor = new ListCellEditor();
    private final transient CellEditorListener edtListener = new TableEditorListener();
    private final TagTableModel tableModel = new TagTableModel();
    private final JXTable tagTable = new JXTable(this.tableModel);
    private final JLabel msgLabel = new JLabel();

    /**
     * Object implementing actions associated to buttons in the GUI.
     */
    private final ButtonActions bActions = new ButtonActions();

    /**
     * Boolean used to keep trace of DB changes.
     */
    private final AtomicBoolean dbChanged = new AtomicBoolean(false);

    /**
     * State after which changes in data set tags are not allowed
     */
    private final static RunControlFSM.State rcLimitState = RunControlFSM.State.NONE;

    /**
     * This class describes a Data Set Tag.
     * <p>
     * Each tag has a name, a list of possible values and a current value.
     */
    private class TagEntry {
        /**
         * Array containing the tag parameters: 0 = Tag name (String); 1 = Possible tag values (String); 2 = Boolean telling if the tag is
         * used or not.
         */
        private final Object[] params = new Object[3];

        /**
         * The currently selected tag value amongst all the possible values.
         */
        private final StringBuffer selTag;

        /**
         * Constructor to be used when a tag is described in the DB but it is not used (i.e., it is not present in IS).
         * 
         * @param tagName The tag name
         * @param tagValues A list of possible tag values
         */
        TagEntry(final String tagName, final List<String> tagValues) {
            this.params[0] = tagName;
            this.params[1] = tagValues;
            this.params[2] = Boolean.FALSE;
            this.selTag = new StringBuffer(tagValues.size() > 0 ? tagValues.get(0) : "");
        }

        /**
         * Constructor to be used when a tag is described in the DB and it is currently used (i.e., its value is present in IS).
         * 
         * @param tagName The tag name
         * @param tagValues A list of possible tag values
         * @param currentValue The current value of the data set tag
         */
        TagEntry(final String tagName, final List<String> tagValues, final String currentValue) {
            this.params[0] = tagName;
            this.params[1] = tagValues;
            this.params[2] = Boolean.TRUE;
            this.selTag = new StringBuffer(currentValue);
        }

        /**
         * It gets the list of possible tag values.
         * <p>
         * The returned list is unmodifiable.
         * 
         * @return The list of possible tag values.
         */
        public List<String> getTagList() {
            @SuppressWarnings("unchecked")
            final List<String> l = (List<String>) this.getParam(1);
            return Collections.unmodifiableList(l);
        }

        /**
         * It tells if the tag is used or not.
         * 
         * @return <code>true</code> if the tag is used.
         */
        public Boolean getUsed() {
            return (Boolean) this.getParam(2);
        }

        /**
         * It returns the tag name.
         * 
         * @return The tag name
         */
        public String getTagName() {
            return (String) this.getParam(0);
        }

        /**
         * It returns the current tag value.
         * 
         * @return The current tag value.
         */
        public String getTagValue() {
            return this.selTag.toString();
        }

        /**
         * It sets the current value for the tag.
         * 
         * @param value The tag value
         */
        public void setTagValue(final String value) {
            this.selTag.replace(0, Integer.MAX_VALUE, value);
        }

        /**
         * It sets whether the current tag is used or not.
         * 
         * @param isUsed <code>true</code> if the tag is used
         */
        public void setUsed(final Boolean isUsed) {
            synchronized(this.params) {
                this.params[2] = isUsed;
            }
        }

        /**
         * It gets the parameters associated to the tag.
         * 
         * @param i The parameter index (0 < i < 3);
         * @return The 'i' parameter.
         */
        Object getParam(final int i) {
            return this.params[i];
        }

        /**
         * Use the tag name hash as hash code.
         */
        @Override
        public int hashCode() {
            return this.params[0].hashCode();
        }

        /**
         * Two tags are equal if they have the same name.
         */
        @Override
        public boolean equals(final Object obj) {
            boolean isEqual = false;

            if(TagEntry.class.isInstance(obj)) {
                final TagEntry t = (TagEntry) obj;
                isEqual = this.params[0].equals(t.params[0]);
            }

            return isEqual;
        }
    }

    /**
     * Table cell renderer for lists (used for tag values).
     */
    private class ListCellRenderer extends DefaultTableCellRenderer {
        private static final long serialVersionUID = 8538687502036826565L;

        ListCellRenderer() {
            super();
            this.setHorizontalAlignment(SwingConstants.CENTER);
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            // Get the TagEntry corresponding the right row and show the current tag value
            final TagEntry te = DSPanel.this.tableModel.getRow(row);
            final String tagValue = te.getTagValue();
            this.setText(tagValue);
            return this;
        }

    }

    /**
     * Table cell editor for lists (used for tag values).
     * <p>
     * It uses a {@link JComboBox} as an editor.
     */
    private class ListCellEditor extends AbstractCellEditor implements TableCellEditor {
        private static final long serialVersionUID = -1583442115755704707L;

        ListCellEditor() {
            super();
        }

        @Override
        public Component getTableCellEditorComponent(final JTable table,
                                                     final Object val,
                                                     final boolean isSelected,
                                                     final int row,
                                                     final int column)
        {
            // Get the editor
            final JComboBox<Object> cb = this.getCellEditorValue();

            // Fill the JComboBox with all the values a tag can have
            final List<?> l = (List<?>) table.getModel().getValueAt(row, column);
            for(final Object obj : l) {
                if(String.class.isInstance(obj) == false) {
                    cb.addItem(obj.toString());
                } else {
                    // ComboBox may have issues with duplicated string objects
                    cb.addItem(new Object() {
                        @Override
                        public String toString() {
                            return obj.toString();
                        }
                    });
                }
            }

            // Add a listener to the JComboBox so that the table model
            // can be updated when a new item is selected
            cb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    // The first value passed to 'setValueAt' is not the currently
                    // selected item in the JComboBox because this would be a 'fixed'
                    // reference to the item selected 'now', but you want to know
                    // the item selected at the moment in time the listener is
                    // called: the easiest way is to pass the JComboBox reference.
                    DSPanel.this.tableModel.setValueAt(cb, row, column);
                    ListCellEditor.this.stopCellEditing();
                }
            });

            return cb;
        }

        /**
         * Use a {@link JComboBox} as an editor.
         * 
         * @see javax.swing.CellEditor#getCellEditorValue()
         */
        @Override
        public JComboBox<Object> getCellEditorValue() {
            final JComboBox<Object> cb = new JComboBox<Object>();
            return cb;
        }
    }

    /**
     * Class implementing {@link CellEditorListener} to be notified whether a table item has been edited.
     * <p>
     * When the editing is stopped a message is written on the panel tool-bar to alert the user and the 'apply' button background is set to
     * red.
     */
    private class TableEditorListener implements CellEditorListener {
        TableEditorListener() {
        }

        @Override
        public void editingCanceled(final ChangeEvent e) {
        }

        @Override
        public void editingStopped(final ChangeEvent e) {
            if(DSPanel.this.isShowing() == true) {
                DSPanel.this.msgLabel.setText(DSPanel.rtlModifiedMsg);
                DSPanel.this.applyButton.setBackground(Color.RED);
            }
        }
    }

    /**
     * The table model. The table has four columns: tag number (Number), tag name (String), tag values (list) and tag used (Boolean).
     * <p>
     * The table items are stored in a TagEntry list.
     * <p>
     * The table model has to be accessed from the EDT only.
     */
    private static class TagTableModel extends AbstractTableModel {
        private static final long serialVersionUID = -5434828683716281149L;

        private final static int colNum = 4;
        private final Class<?>[] colClass = new Class<?>[] {Number.class, String.class, List.class, Boolean.class};
        private final String[] colNames = new String[] {"#", "Tag Name", "Tag Value", "Used"};

        private final List<TagEntry> rowList = new ArrayList<TagEntry>();

        TagTableModel() {
        }

        public void cleanTable() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            this.rowList.clear();
            this.fireTableDataChanged(); // Never forget to fire the right event
        }

        public void removeRow(final int rowIndex) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            this.rowList.remove(rowIndex);
            this.fireTableRowsDeleted(rowIndex, rowIndex); // Never forget to fire the right event
        }

        public void addRows(final List<? extends TagEntry> te) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            // If none of the TagEnty is already in the table then add all of them
            // otherwise replace the old one with the new one.
            if(Collections.disjoint(te, this.rowList) == true) {
                // No entry is present, add all
                this.rowList.addAll(te);
            } else {
                for(final TagEntry t : te) {
                    final int index = this.rowList.indexOf(t);
                    if(index != -1) {
                        // The entry is already present, replace it
                        this.rowList.set(index, t);
                    } else {
                        // The entry is not present, add it
                        this.rowList.add(t);
                    }
                }
            }

            this.fireTableDataChanged(); // Never forget to fire the right event
        }

        public void addRow(final TagEntry te) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            if(this.rowList.contains(te) == true) {
                final int index = this.rowList.indexOf(te);
                this.rowList.set(index, te);
                this.fireTableRowsUpdated(index, index); // Never forget to fire the right event
            } else {
                this.rowList.add(te);
                final int index = this.rowList.size() - 1;
                this.fireTableRowsInserted(index, index); // Never forget to fire the right event
            }
        }

        /**
         * @see javax.swing.table.TableModel#getColumnCount()
         */
        @Override
        public int getColumnCount() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            return TagTableModel.colNum;
        }

        /**
         * @see javax.swing.table.AbstractTableModel#getColumnName(int)
         */
        @Override
        public String getColumnName(final int column) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            return this.colNames[column];
        }

        /**
         * Only columns 2 and 3 are editable.
         * 
         * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
         */
        @Override
        public boolean isCellEditable(final int row, final int column) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            boolean toReturn;

            if((column == 2) || (column == 3)) {
                toReturn = true;
            } else {
                toReturn = false;
            }

            return toReturn;
        }

        /**
         * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
         */
        @Override
        public Class<?> getColumnClass(final int columnIndex) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            return this.colClass[columnIndex];
        }

        /**
         * @see javax.swing.table.TableModel#getRowCount()
         */
        @Override
        public int getRowCount() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            return this.rowList.size();
        }

        public TagEntry getRow(final int rowIndex) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            return this.rowList.get(rowIndex);
        }

        /**
         * @see javax.swing.table.TableModel#getValueAt(int, int)
         */
        @Override
        public Object getValueAt(final int rowIndex, final int columnIndex) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            final Object retObj;

            if(columnIndex == 0) {
                // The tag number starts from 1 in the table
                retObj = Integer.valueOf(rowIndex + 1);
            } else {
                // Return the TagEntry parameter
                retObj = this.rowList.get(rowIndex).getParam(columnIndex - 1);
            }

            return retObj;
        }

        /**
         * Used only for editable columns (i.e., column number 2 and 3).
         * 
         * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
         */
        @Override
        public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            switch(columnIndex) {
                case 2:
                    // For column 2 the editor is a JComboBox, so pass
                    // the JComboBox as 'value'. The JComboBox selected item
                    // is used to set the TagEntry current value.
                    @SuppressWarnings("unchecked")
                    final JComboBox<Object> cb = (JComboBox<Object>) value;
                    final Object selItem = cb.getSelectedItem();
                    if(selItem != null) {
                        this.rowList.get(rowIndex).setTagValue(selItem.toString());
                    }
                    break;
                case 3:
                    // Column 3 is a simple Boolean
                    this.rowList.get(rowIndex).setUsed((Boolean) value);
                    break;
                default:
                    break;
            }

            this.fireTableCellUpdated(rowIndex, columnIndex); // Never forget to fire the right event
        }
    }

    /**
     * {@link MouseAdapter} to manage mouse events.
     * <p>
     * Pressing the right mouse button a contextual menu will appear offering the user the possibility to remove the selected tag.
     */
    private class TableMouseListener extends MouseAdapter {
        final JPopupMenu menu = new JPopupMenu();
        final JMenuItem removeItem = new JMenuItem("Remove tag", Igui.createIcon("kill.png"));

        TableMouseListener() {
            this.initMenu();
        }

        @Override
        public void mouseClicked(final MouseEvent e) {
            if(SwingUtilities.isRightMouseButton(e) && (Igui.instance().isDBChangeAllowed() == true)) {
                final int selRow = DSPanel.this.tagTable.getSelectedRow();
                if(selRow != -1) {
                    this.menu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }

        private void initMenu() {
            this.menu.addSeparator();
            this.menu.add(this.removeItem);
            this.menu.addSeparator();

            // Add an ActionListener taking care to remove the selected row
            this.removeItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final int selRow = DSPanel.this.tagTable.getSelectedRow();
                    if(selRow != -1) {
                        DSPanel.this.tableModel.removeRow(selRow);
                        DSPanel.this.msgLabel.setText(DSPanel.rtlModifiedMsg);
                        DSPanel.this.applyButton.setBackground(Color.RED);
                    }
                }
            });
        }
    }

    /**
     * Class implementing {@link ListSelectionListener}.
     * <p>
     * When a table row is selected the fields at the bottom of the table are correctly filled allowing the user to edit the tag.
     */
    private class TableSelectionListener implements ListSelectionListener {
        @Override
        public void valueChanged(final ListSelectionEvent e) {
            if(e.getValueIsAdjusting() == false) { // Wait for "final" event
                final int selectedRow = DSPanel.this.tagTable.getSelectedRow();
                if(selectedRow != -1) {
                    // Get the TagEntry corresponding to the selected row
                    final TagEntry te = DSPanel.this.tableModel.getRow(selectedRow);

                    // Put the tag name in the right field
                    DSPanel.this.tagNameField.setText(te.getTagName());

                    // Put the tag possible values in the other text field
                    final List<String> tvl = te.getTagList();
                    final Iterator<String> listIt = tvl.iterator();
                    final StringBuilder vl = new StringBuilder();
                    while(listIt.hasNext() == true) {
                        vl.append(listIt.next());
                        if(listIt.hasNext() == true) {
                            vl.append(DSPanel.dbTagValuesSeparator);
                        }
                    }
                    DSPanel.this.tagValuesField.setText(vl.toString());
                }
            }
        }
    }

    /**
     * SwingWorker used the update the run tags.
     * <p>
     * It takes care of updating run tags values both in IS (the selected tag value changed or the tag is (un)used) and in the DB (whether
     * some tag is removed or added).
     */
    private class UpdateRunTags extends SwingWorker<Void, String> {
        private final List<String> isValues;
        private final List<String> dbValues;

        /**
         * Constructor
         * 
         * @param isValues List of tags to write to IS
         * @param dbValues List of tags to write to DB
         */
        UpdateRunTags(final List<String> isValues, final List<String> dbValues) {
            this.isValues = isValues;
            this.dbValues = dbValues;
        }

        @Override
        protected Void doInBackground() throws IguiException.IguiPanelError {
            final StringBuilder err = new StringBuilder();

            // Disable the 'apply' button and set the panel busy
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    DSPanel.this.applyButton.setEnabled(false);
                    DSPanel.this.setBusy(true);
                }
            });

            final String[] selectedRunTags = this.isValues.toArray(new String[this.isValues.size()]);
            Arrays.sort(selectedRunTags);
            
            // Update run tag values in IS
            this.publish("Updating run tag list in IS...");
            try {
                final UserMetaDataNamed isTags = new UserMetaDataNamed(Igui.instance().getPartition(), DSPanel.isServerName);
                isTags.RunTags = selectedRunTags;
                isTags.checkin();
            }
            catch(final Exception ex) {
                final String errMsg = "Failed updating run tag values in IS: " + ex;
                IguiLogger.error(errMsg, ex);
                err.append(errMsg + "\n");
            }

            // Write run tag values in DB: do it only if the current tag set differs from the DB one
            try {
                final dal.Partition rwp = dal.Partition_Helper.get(DSPanel.this.getRwDb(), Igui.instance().getPartition().getName());
                if(rwp != null) {
                    final String[] currentRunTags = rwp.get_RunTags();
                    if(currentRunTags != null) {
                        Arrays.sort(currentRunTags);
                    }
                    
                    if(Arrays.equals(currentRunTags, selectedRunTags) == false) {
                        rwp.set_RunTags(selectedRunTags);
                        
                        // The DB has been modified
                        DSPanel.this.dbChanged.set(true);
                    }
                    
                    final dal.RunTagList rtl = rwp.get_RunTagList();
                    if(rtl != null) {
                        // The DB (partition) already contains an object holding the run tags list
                        if(this.writeToDB(rtl) == true) {
                            // The list is different wrt the DB list -> update the DB
                            this.publish("Writing new run tag list in the database...");
                            rtl.set_RunTags(this.dbValues.toArray(new String[this.dbValues.size()]));

                            // The DB has been modified
                            DSPanel.this.dbChanged.set(true);
                        }
                    } else {
                        // The DB (partition) does not contain any RunTagList object: create a new one and
                        // initialize it with the right run tags list.
                        // The name of the object will be equal to DSPanel.rtObjectPrefix_<part name>_Igui_<time stamp>
                        this.publish("Creating a new run tag list in the database...");
                        final String newRTLObjName = DSPanel.rtObjectPrefix + Igui.instance().getPartition().getName() + "_Igui_"
                                                     + String.valueOf(new Date().getTime());
                        {
                            final String msg = "No \"RunTagList\" object is defined in the DB for the current partition;\na new one named \""
                                               + newRTLObjName + "\" will be created in the partition file";
                            IguiLogger.info(msg);
                        }
                        final dal.RunTagList rtlNew = dal.RunTagList_Helper.create(DSPanel.this.getRwDb(), rwp, newRTLObjName);
                        rtlNew.set_RunTags(this.dbValues.toArray(new String[this.dbValues.size()]));
                        rwp.set_RunTagList(rtlNew);

                        // The DB has been modified
                        DSPanel.this.dbChanged.set(true);
                    }
                } else {
                    final String errMsg = "Cannot write the run tag list into the database because\nthe \"dal.Partition\" object is not found in the R/W RDB";
                    IguiLogger.error(errMsg);
                    ErrorFrame.showError(errMsg);
                }
            }
            catch(final Exception ex) {
                final String errMsg = "Failed updating run tag values in the database: " + ex;
                IguiLogger.error(errMsg, ex);
                err.append(errMsg);
            }

            if(err.length() != 0) {
                throw new IguiException.IguiPanelError(DSPanel.this.getPanelName(), err.toString());
            }

            return null;
        }

        /**
         * It writes messages to the glass pane.
         * 
         * @see javax.swing.SwingWorker#process(java.util.List)
         */
        @Override
        protected void process(final List<String> chunks) {
            final String msg = chunks.get(chunks.size() - 1);
            DSPanel.this.setBusyMessage(msg);
        }

        /**
         * It checks that the performed procedure has been correctly executed.
         * <p>
         * If the DB has been modified than a message is reported on the panel tool-bar. Any error is reported to the user.
         * 
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                this.get();
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, "The Run Tag list has been successfully updated");
                if(DSPanel.this.dbChanged.get() == true) {
                    DSPanel.this.msgLabel.setText("Changes not committed to database yet, remember to press \"Commit & Reload\"!");
                } else {
                    DSPanel.this.msgLabel.setText("");
                }
            }
            catch(final InterruptedException ex) {
                final String errMsg = "The task updating the run tags has been interrupted";
                IguiLogger.warning(errMsg);
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final Throwable cause = ex.getCause();
                final String errMsg = "Run tag value updating error:\n" + cause;
                IguiLogger.error(errMsg, cause);
                ErrorFrame.showError(errMsg);
            }
            finally {
                // Remember to remove the panel busy state!
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        DSPanel.this.applyButton.setBackground(null);
                        DSPanel.this.applyButton.setEnabled(true);
                        DSPanel.this.setBusy(false);
                    }
                });
            }
        }

        /**
         * It makes a comparison between the actual list of run tags to be written to the DB and the list actually contained in the DB. If
         * the two lists have different elements then the DB will be updated.
         * 
         * @param dbRTL The DB object containing the run tags list
         * @return <code>true</code> if the Db needs to be updated
         * @throws IguiException.ConfigException Some error occurred retrieving information from the database
         */
        private boolean writeToDB(final dal.RunTagList dbRTL) throws IguiException.ConfigException {
            try {
                boolean differ = true;
    
                final String[] rt = dbRTL.get_RunTags();
                if(rt != null) {
                    final List<String> dbRunTagList = Arrays.asList(rt);
                    Collections.sort(dbRunTagList);
                    Collections.sort(this.dbValues);
                    differ = !this.dbValues.equals(dbRunTagList);
                }
    
                return differ;
            }
            catch(final ConfigException ex) {
                throw new IguiException.ConfigException("Failed getting the run tags from the database: " + ex, ex);
            }
        }
    }

    /**
     * Classes whose methods are associated to actions performed by buttons.
     */
    private class ButtonActions {
        ButtonActions() {
        }

        /**
         * Method executed when the 'Apply' button is pushed.
         * <p>
         * It gets all the elements contained in the table and build to lists containing the tag lists correctly formatted for IS and for
         * the DB.
         * 
         * @see UpdateRunTags
         */
        void updateChanges() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            final List<String> isValues = new ArrayList<String>();
            final List<String> dbValues = new ArrayList<String>();
            final StringBuilder dbTagValues = new StringBuilder(); // Buffer holding the tag values for each tag following the DB format

            final int r = DSPanel.this.tableModel.getRowCount();
            for(int i = 0; i < r; ++i) {
                final TagEntry te = DSPanel.this.tableModel.getRow(i);

                {
                    // Build a list of all the tags for the DB
                    final List<String> tagValues = te.getTagList();
                    final int num = tagValues.size();
                    for(int j = 0; j < num; ++j) {
                        dbTagValues.append(tagValues.get(j));
                        if(j < (num - 1)) {
                            dbTagValues.append(DSPanel.dbTagValuesSeparator);
                        }
                    }
                    dbValues.add(te.getTagName() + DSPanel.dbTagNameValuesSeparator + DSPanel.dbTagValuesDelimeters[0]
                                 + dbTagValues.toString() + DSPanel.dbTagValuesDelimeters[1]);
                }

                // Add the tag to the IS list only if the tag has been marked in the table as used
                final Boolean isUsed = te.getUsed();
                if(isUsed.equals(Boolean.TRUE)) {
                    isValues.add(te.getTagName() + DSPanel.tagISSeparator + te.getTagValue());
                }

                // Delete the string buffer before moving to the next table row
                dbTagValues.delete(0, dbTagValues.length());
            }

            // Update IS and the DB (it starts a SwingWorker, potentially time consuming tasks always outside the EDT)
            new UpdateRunTags(isValues, dbValues).execute();
        }

        /**
         * Method executed when the "Add/Modify" button is pushed.
         */
        void addRowToTable() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            final String tagName = DSPanel.this.tagNameField.getText();
            // Get the currently selected tag
            if(tagName.isEmpty() == false) {
                // Get the tag values
                final String tagValues = DSPanel.this.tagValuesField.getText();
                if(tagValues.isEmpty() == false) {
                    // Format the tag values to have a list of values
                    final String[] tags = tagValues.split(DSPanel.dbTagValuesSeparator);
                    final List<String> tagList = new ArrayList<String>();
                    for(final String s : tags) {
                        final String valueToAdd = s.trim();
                        if(tagList.contains(valueToAdd) == false) {
                            tagList.add(valueToAdd);
                        }
                    }

                    // Ask the table model to add (or just update) the tag
                    DSPanel.this.tableModel.addRow(new TagEntry(tagName, tagList));
                    // Alert the user that some tag structure has been changed
                    DSPanel.this.msgLabel.setText(DSPanel.rtlModifiedMsg);
                    DSPanel.this.applyButton.setBackground(Color.RED);

                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION,
                                                    "Created Run Tag \"" + tagName + "\" with values " + tagList.toString());
                } else {
                    ErrorFrame.showError("The tag value list cannot be empty");
                }
            } else {
                ErrorFrame.showError("Please, specify the tag name");
            }
        }
    }

    /**
     * Constructor: it just initializes the GUI.
     * 
     * @param mainIgui Reference to the Igui
     */
    public DSPanel(final Igui mainIgui) {
        super(mainIgui);
        this.helpFilName = IguiConstants.HELP_FILE_PREFIX + IguiConstants.HELP_PATH + "DataSetTagsPanel.htm";
        this.initGUI();
    }

    /**
     * @see Igui.IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return DSPanel.pName;
    }

    /**
     * @see Igui.IguiPanel#getTabName()
     */
    @Override
    public String getTabName() {
        return DSPanel.pName;
    }

    /**
     * The panel is initialized: it connects to the online infrastructure to retrieve all the needed information.
     * <p>
     * Tags are retrieved both from IS and from the DB and then the table is filled.
     * 
     * @see Igui.IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void panelInit(final State rcState, final AccessControlStatus controlStatus) throws IguiException {
        this.accessControlChanged(controlStatus);

        // Get current tags from IS
        final Map<String, String> isTagMap; // Map: tagName -> tagValue
        {
            String[] tags = null;
            try {
                final UserMetaDataNamed isTags = new UserMetaDataNamed(Igui.instance().getPartition(), DSPanel.isServerName);
                isTags.checkout();
                tags = isTags.RunTags;
            }
            catch(final Exception ex) {
                final String errMsg = "Cannot get the run tag list from IS: " + ex;
                IguiLogger.warning(errMsg, ex);
            }
            finally {
                if(tags != null) {
                    isTagMap = new HashMap<String, String>(this.parseTagFromIS(tags));
                } else {
                    isTagMap = Collections.emptyMap();
                }
            }
        }

        // Get tags defined in DB
        final Map<String, List<String>> dbTagMap; // Map: tagName -> possible tagValues
        {
            String[] tags = null;
            try {
                final dal.RunTagList rtList = Igui.instance().getDalPartition().get_RunTagList();
                if(rtList != null) {
                    tags = rtList.get_RunTags();
                } else {
                    IguiLogger.info("No run tags are defined in the database");
                }
            }
            catch(final Exception ex) {
                final String errStr = "Cannot get the run tag list from the databse: " + ex;
                IguiLogger.error(errStr, ex);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errStr);
            }
            finally {
                if(tags != null) {
                    dbTagMap = new HashMap<String, List<String>>(this.parseStringFromDB(tags));
                } else {
                    dbTagMap = Collections.emptyMap();
                }
            }
        }

        // Compare tags in IS and in DB and create the TagEntry list used to fill the table
        final List<TagEntry> tagList = new ArrayList<TagEntry>();
        {
            // Loop over all the tags described in the DB
            final Set<Map.Entry<String, List<String>>> es = dbTagMap.entrySet();
            for(final Map.Entry<String, List<String>> me : es) {
                final String tagName = me.getKey();
                final List<String> tagValues = me.getValue();
                final String tagCurrentValue = isTagMap.get(tagName); // Here a check is done whether the tag is in IS (i.e., it is used)
                if(tagCurrentValue != null) {
                    // The tag is used
                    tagList.add(new TagEntry(tagName, tagValues, tagCurrentValue));
                } else {
                    // The tag is not used
                    tagList.add(new TagEntry(tagName, tagValues));
                }
            }
        }

        // Make a check to test consistency between IS and DB tag values
        {
            // Loop over all the tags present in IS
            final Set<Map.Entry<String, String>> isTagMapEntry = isTagMap.entrySet();
            for(final Map.Entry<String, String> me : isTagMapEntry) {
                // Check that the tag is in the DB
                if(dbTagMap.containsKey(me.getKey()) == true) {
                    // Check that the corresponding value is there as well
                    if(dbTagMap.get(me.getKey()).contains(me.getValue()) == false) {
                        // The value is not in the DB, issue a warning
                        final String errMsg = "The value \""
                                              + me.getValue()
                                              + "\" for the run tag \""
                                              + me.getKey()
                                              + "\" has been found in IS but not in the database, the Dataset Tag panel may show inconsistent information";
                        IguiLogger.warning(errMsg);
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.WARNING, errMsg);

                        // Here the value is not inserted into the table, the user has the possibility
                        // to modify the tag afterwards.
                    }
                } else {
                    // The tag is not in the DB!
                    final String errMsg = "The run tag \""
                                          + me.getKey()
                                          + "\" has been found in IS but not in the database, the Dataset Tag panel may show inconsistent information";

                    // The tag found only in IS is then inserted into the table so that it
                    // could be written to the DB
                    tagList.add(new TagEntry(me.getKey(), Arrays.asList(me.getValue()), me.getValue()));

                    IguiLogger.error(errMsg);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                }
            }
        }

        this.dbChanged.set(false);

        // Fill the table
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Reset the table: this method is called also when the DB is reloaded or discarded
                DSPanel.this.msgLabel.setText("");
                DSPanel.this.applyButton.setBackground(null);
                DSPanel.this.tableModel.cleanTable();

                // Add elements to the table
                DSPanel.this.tableModel.addRows(tagList);
                DSPanel.this.tagTable.packAll();
                DSPanel.this.enableButtons(rcState);
            }
        });
    }

    /**
     * @see Igui.IguiPanel#dbReloaded()
     */
    @Override
    public void dbReloaded() {
        try {
            this.panelInit(Igui.instance().getRCState(), Igui.instance().getAccessLevel());
        }
        catch(final IguiException ex) {
            final String errMsg = "DB reloading failed: " + ex;
            IguiLogger.error(errMsg, ex);
        }
    }

    /**
     * @see Igui.IguiPanel#dbDiscarded()
     */
    @Override
    public void dbDiscarded() {
        try {
            // Do this because the dal.RunTagList seems to not reset correctly
            final config.Configuration rwConf = this.getRwDb();
            if(rwConf != null) {
                final dal.Partition rwp = dal.Partition_Helper.get(rwConf, Igui.instance().getPartition().getName());
                if(rwp != null) {
                    final dal.RunTagList rtl = rwp.get_RunTagList();
                    if(rtl != null) {
                        rtl.update();
                    }
                }
            }

            // Re-initialize the panel
            this.panelInit(Igui.instance().getRCState(), Igui.instance().getAccessLevel());
        }
        catch(final Exception ex) {
            final String errMsg = "Failed aborting database changes for panel \"" + DSPanel.pName + "\": " + ex;
            IguiLogger.error(errMsg, ex);
            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
        }
    }

    /**
     * @see Igui.IguiPanel#panelDeselected()
     */
    @Override
    public void panelDeselected() {
        // Show the dialog if the DB has been modified
        if(this.dbChanged.get() == true) {
            this.showDiscardDbDialog("The \"RunTagList\" object has been modified but the\ncommit command has not been issued to the database.");
        }
    }

    /**
     * @see Igui.IguiPanel#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void accessControlChanged(final AccessControlStatus newAccessStatus) {
        if(newAccessStatus.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            this.disable(false);
        } else {
            this.disable(true);
        }
    }

    /**
     * @see Igui.IguiPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
    }

    /**
     * This panel is RC state aware.
     * 
     * @see Igui.IguiPanel#rcStateAware()
     */
    @Override
    public boolean rcStateAware() {
        return true;
    }

    /**
     * It enables or disables the 'Apply' button.
     * 
     * @see Igui.IguiPanel#rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)
     */
    @Override
    public void rcStateChanged(final State oldState, final State newState) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                DSPanel.this.enableButtons(newState);
            }
        });
    }

    /**
     * It parses the IS information to have a map holding the tag name (key) and its value.
     * <p>
     * The is format is like: tagName:=tagValue.
     * 
     * @param tagArray Array of tags coming from IS
     * @return A map holding the tag name (map key) and its value
     */
    private Map<String, String> parseTagFromIS(final String[] tagArray) {
        final Map<String, String> tagMap = new HashMap<String, String>();

        for(final String tag : tagArray) {
            try {
                final String[] pair = tag.split(DSPanel.tagISSeparator, 2);
                tagMap.put(pair[0].trim(), pair[1].trim());
            }
            catch(final Exception ex) {
                final String msg = "Failed parsing the run tag \"" + tag + "\" taken from IS: " + ex + ". Maybe bad format?";
                IguiLogger.error(msg, ex);
            }
        }

        return tagMap;
    }

    /**
     * It parses the DB information to have a map holding the tag name (key) and its possible value.
     * <p>
     * The DB format is like: tagName={tagValue1,tagValue2,...}
     * 
     * @param tagArray Array of tags coming from DB
     * @return A map holding the tag name (map key) and all its possible values
     */
    private Map<String, List<String>> parseStringFromDB(final String[] tagArray) {
        // In DB tags are defined as name={value1, value2, value3, ...}
        final Map<String, List<String>> tagMap = new HashMap<String, List<String>>();

        for(final String tag : tagArray) {
            try {
                final String[] splt = tag.split(DSPanel.dbTagNameValuesSeparator, 2);

                final int last = splt[1].lastIndexOf(DSPanel.dbTagValuesDelimeters[1]);
                final int first = splt[1].indexOf(DSPanel.dbTagValuesDelimeters[0]);

                final String[] values = splt[1].substring(first + 1, last).split(DSPanel.dbTagValuesSeparator);
                final List<String> valueList = new ArrayList<String>();
                for(final String v : values) {
                    final String valueToAdd = v.trim();
                    if(valueList.contains(valueToAdd) == false) {
                        valueList.add(valueToAdd);
                    }
                }

                tagMap.put(splt[0].trim(), valueList);
            }
            catch(final Exception ex) {
                final String msg = "Failed parsing the run tag \"" + tag + "\" taken from the database: " + ex + ". Maybe bad format?";
                IguiLogger.error(msg, ex);
            }
        }

        return tagMap;
    }

    /**
     * It enables or disables the "Apply Changes" and "Add/Modify" buttons taking into account the current RC state
     * 
     * @param state The current RC state
     * @see {@link #rcStateChanged(State, State)}
     * @see {@link #panelInit(State, AccessControlStatus)}
     */
    private void enableButtons(final RunControlFSM.State state) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        this.addNewTagButton.setEnabled(!state.follows(DSPanel.rcLimitState));
        this.applyButton.setEnabled(!state.follows(DSPanel.rcLimitState));
    }

    /**
     * It initialized the panel GUI.
     * <p>
     * Called by the constructor.
     */
    private void initGUI() {
        this.setLayout(new BorderLayout());

        this.addNewTagButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                DSPanel.this.bActions.addRowToTable();
            }
        });

        this.applyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                DSPanel.this.bActions.updateChanges();
            }
        });

        final JScrollPane scrP = new JScrollPane(this.tagTable);
        final Border titleBorder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20),
                                                                      BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                                                                                                       "Dataset Tags",
                                                                                                       TitledBorder.LEFT,
                                                                                                       TitledBorder.TOP,
                                                                                                       new Font("Dialog", Font.ITALIC, 12)));

        // Set renderers, editors and editor listeners
        this.listEditor.addCellEditorListener(this.edtListener);
        this.tagTable.setDefaultRenderer(List.class, this.listRenderer);
        this.tagTable.setDefaultEditor(List.class, this.listEditor);
        this.tagTable.setDefaultRenderer(Number.class, new DefaultTableRenderer(new FormatStringValue(NumberFormat.getInstance()),
                                                                                SwingConstants.CENTER));
        this.tagTable.setDefaultRenderer(String.class, new DefaultTableRenderer(null, SwingConstants.CENTER));
        final TableCellEditor edt = this.tagTable.getDefaultEditor(Boolean.class);
        edt.addCellEditorListener(this.edtListener);

        this.tagTable.setRowHeight(20);
        this.tagTable.setColumnSelectionAllowed(false);
        this.tagTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.tagTable.getTableHeader().setReorderingAllowed(false);
        this.tagTable.setSortable(false);
        this.tagTable.setHighlighters(HighlighterFactory.createAlternateStriping());
        this.tagTable.setTerminateEditOnFocusLost(true);
        this.tagTable.addMouseListener(new TableMouseListener());
        this.tagTable.getSelectionModel().addListSelectionListener(new TableSelectionListener());

        final JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.setBorder(titleBorder);
        final JPanel southPanel = new JPanel();
        southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.LINE_AXIS));
        southPanel.add(new JLabel("Tag Name"));
        southPanel.add(Box.createRigidArea(new Dimension(3, 0)));
        southPanel.add(this.tagNameField);
        southPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        southPanel.add(new JLabel("Tag Values"));
        southPanel.add(Box.createRigidArea(new Dimension(3, 0)));
        southPanel.add(this.tagValuesField);
        southPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        southPanel.add(this.addNewTagButton);

        tablePanel.add(scrP, BorderLayout.CENTER);
        tablePanel.add(southPanel, BorderLayout.SOUTH);

        this.add(tablePanel, BorderLayout.CENTER);
        this.add(this.toolBar, BorderLayout.SOUTH);

        this.msgLabel.setMinimumSize(new Dimension(0, 20));

        this.tagNameField.setToolTipText("The name of the run tag");
        this.tagValuesField.setToolTipText("Put tag values in a comma separated format (i.e, value1,value2,...,value99)");
        this.addNewTagButton.setToolTipText("Modify or add the new tag to the table");
        this.applyButton.setToolTipText("Write tag information to IS and to the database");

        this.toolBar.setLayout(new BoxLayout(this.toolBar, BoxLayout.LINE_AXIS));
        this.toolBar.add(Box.createHorizontalGlue());
        this.toolBar.add(this.msgLabel);
        this.toolBar.add(Box.createHorizontalGlue());
        this.toolBar.add(this.applyButton);
    }

}
