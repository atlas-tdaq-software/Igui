package Igui;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import org.omg.CORBA.SystemException;

import com.jidesoft.swing.AutoResizingTextArea;

import Igui.IguiConstants.MessageSeverity;
import iguiCommander.Client;
import iguiCommander.ClientRequest;
import iguiCommander.Command;
import iguiCommander.Server;
import iguiCommander.ServerOperations;
import iguiCommander.UserAnswer;
import iguiCommander.servantName;
import ipc.InvalidPartitionException;
import ipc.Partition;
import rc.StopWhileBeamInfoNamed;


/**
 * Class implementing the server interface for CORBA remote calls.
 * <p>
 * Remote calls showing information or asking the user via dialogs are executed in an asynchronous way using a single executor (this avoids
 * having many windows at the same time), while direct commands are executed synchronously.
 */
class IguiServantImpl extends ipc.NamedObject<Server> implements ServerOperations {
    /**
     * This executor is used to process all the remote requests which will cause a dialog to appear on the screen. This will help avoiding
     * having many dialogs at the same time.
     */
    private final ThreadPoolExecutor executor = ExecutorFactory.createSingleExecutor("Servant-executor");

    /**
     * Title for the "stop alert" dialog.
     */
    private final static String stopAlertDialogTitle = "IGUI - Stop of Run Information";

    /**
     * <i>True</i> if the underlining CORBA object is released
     */
    private boolean isReleased = false;

    /**
     * FutureTask to be sent to the EDT: it shows a dialog asking the user what to do with a remote request (basically the user can answer
     * 'yes' or 'no' to the question). It returns an Integer with values: -1 - The dialog has been simply closed; JOptionPane.YES_OPTION -
     * The user pushed the 'yes' button; JOptionPane.NO_OPTION - The user pushed the 'no' button.
     * <p>
     * The task must be executed in the EDT.
     */
    private static class AskUserTask extends FutureTask<Integer> {
        /**
         * Constructor
         * 
         * @param id The remote caller identifier (i.e., friendly name)
         * @param msg The message to show to the user
         */
        public AskUserTask(final String id, final String msg) {
            super(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    // We need to play a little bit with the JOptionPane:
                    // we do not want the dialog buttons to receive events using the
                    // keyboard (some times the dialog appears while the user is writing
                    // the elog entry and a button click is triggered by the space bar) but
                    // only using the mouse. To do that two buttons are directly added to
                    // the option pane and they are not allowed to get the focus.

                    // Create the 'yes' and 'no' buttons
                    final JButton yesButton = new JButton("Yes");
                    final JButton noButton = new JButton("No");

                    // Create the option pane (note the 'null' value as the last argument)
                    final JOptionPane pane = IguiServantImpl.createOptionPane("Remote message from \"" + id + "\":\n" + msg,
                                                                              JOptionPane.QUESTION_MESSAGE,
                                                                              JOptionPane.DEFAULT_OPTION,
                                                                              null,
                                                                              new Object[] {yesButton, noButton},
                                                                              null);

                    // Create the dialog to be shown
                    final JDialog dialog = pane.createDialog(Igui.instance().getMainFrame(), "IGUI - Message from \"" + id + "\"");
                    dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                    // Actions are needed for the buttons (which are not focusable):
                    // first the option pane selected value is set and then the dialog is disposed
                    yesButton.setFocusable(false);
                    yesButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(final ActionEvent e) {
                            // It's important to dispose the dialog only after setting the value!
                            pane.setValue(Integer.valueOf(JOptionPane.YES_OPTION));
                            dialog.dispose();
                        }
                    });

                    noButton.setFocusable(false);
                    noButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(final ActionEvent e) {
                            // It's important to dispose the dialog only after setting the value!
                            pane.setValue(Integer.valueOf(JOptionPane.NO_OPTION));
                            dialog.dispose();
                        }
                    });

                    // Make the dialog visible: this blocks until the dialog is no more visible
                    dialog.setVisible(true);

                    // Get the user selection from the JOptionPane
                    final Object retVal = pane.getValue();
                    if((retVal == null) || (retVal.equals(JOptionPane.UNINITIALIZED_VALUE) == true)) {
                        // The user has made no selection (i.e., the window has been simply closed): return -1
                        return Integer.valueOf(-1);
                    }

                    // The user has made a selection: here it is safe to cast to Integer
                    // (see the ActionListeners associated to the two buttons)
                    return (Integer) retVal;
                }
            });
        }
    }

    /**
     * This {@link FutureTask} shows a modal dialog asking the user to select the sub-systems causing the run to be stopped and the reason
     * why. The task returns an instance of {@link ShowStopAlertDialogTask.TaskResult} specifying whether the user has pushed the OK button
     * and the list of items selected by the user (the lists will be empty if the information should not be written to IS).
     * <p>
     * If the user has pushed the OK button, then no empty values are accepted.
     * <p>
     * To be always executed in the EDT!
     */
    private static class ShowStopAlertDialogTask extends FutureTask<ShowStopAlertDialogTask.TaskResult> {

        /**
         * An instance of this class is returned by the enclosing {@link FutureTask}.
         */
        private static class TaskResult {
            private final Boolean okSelected;
            private final Set<rc.StopWhileBeamInfoNamed.CausedBy> systemList;
            private final Set<rc.StopWhileBeamInfoNamed.Reason> reasonList;

            /**
             * Constructor.
             * 
             * @param okSelected Has the user pressed the OK button?
             * @param systemList List of sub-systems selected by the user
             * @param reasonList List of reasons selected by the user
             */
            public TaskResult(final Boolean okSelected,
                              final Set<rc.StopWhileBeamInfoNamed.CausedBy> systemList,
                              final Set<rc.StopWhileBeamInfoNamed.Reason> reasonList)
            {
                this.okSelected = okSelected;
                this.systemList = Collections.unmodifiableSet(systemList);
                this.reasonList = Collections.unmodifiableSet(reasonList);
            }

            public Boolean okSelected() {
                return this.okSelected;
            }

            public Set<rc.StopWhileBeamInfoNamed.CausedBy> systemList() {
                return this.systemList;
            }

            public Set<rc.StopWhileBeamInfoNamed.Reason> reasonList() {
                return this.reasonList;
            }
        }

        /**
         * Constructor
         */
        public ShowStopAlertDialogTask() {
            super(new Callable<ShowStopAlertDialogTask.TaskResult>() {
                @Override
                public ShowStopAlertDialogTask.TaskResult call() throws Exception {
                    assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

                    // Create panel holding the "sub-systems" check buttons: there will be a check-box for
                    // each enum defined in "rc.StopWhileBeamInfoNamed"
                    final JPanel systemsPanel = new JPanel();
                    final int numCol = 3;
                    final rc.StopWhileBeamInfoNamed.CausedBy[] allCauseValues = rc.StopWhileBeamInfoNamed.CausedBy.values();
                    {
                        final int ratio = allCauseValues.length / numCol;
                        final int numRow = ((ratio * numCol) < allCauseValues.length) ? ratio + 1 : ratio;
                        systemsPanel.setLayout(new GridLayout(numRow, numCol));
                    }

                    // Create list containing the "sub-systems" check-boxes (they are added to the panel as well)
                    final List<JCheckBox> chbSystemsList = new ArrayList<JCheckBox>();
                    for(final rc.StopWhileBeamInfoNamed.CausedBy i : allCauseValues) {
                        final JCheckBox cb = new JCheckBox(i.name());
                        chbSystemsList.add(cb);
                        systemsPanel.add(cb);
                    }

                    systemsPanel.setBorder(BorderFactory.createTitledBorder("List of sub-systems causing this stop of run"));

                    // Create panel holding the "reason" check buttons: there will be a check-box for
                    // each enum defined in "rc.StopWhileBeamInfoNamed"
                    final JPanel reasonsPanel = new JPanel();
                    final rc.StopWhileBeamInfoNamed.Reason[] allReasonValues = rc.StopWhileBeamInfoNamed.Reason.values();
                    {
                        final int ratio = allReasonValues.length / numCol;
                        final int numRow = ((ratio * numCol) < allReasonValues.length) ? ratio + 1 : ratio;
                        reasonsPanel.setLayout(new GridLayout(numRow, numCol));
                    }

                    // Create list containing the "reasons" check-boxes (they are added to the panel as well)
                    final List<JCheckBox> chbReasonsList = new ArrayList<JCheckBox>();
                    for(final rc.StopWhileBeamInfoNamed.Reason i : allReasonValues) {
                        final JCheckBox cb = new JCheckBox(i.name());
                        chbReasonsList.add(cb);
                        reasonsPanel.add(cb);
                    }

                    reasonsPanel.setBorder(BorderFactory.createTitledBorder("Reasons causing the need for a stop"));

                    // Show the dialog (and keep showing it if the user does not select anything or until the "cancel" button is pushed)
                    ShowStopAlertDialogTask.TaskResult tskResult = null;
                    boolean userHasDone = false;
                    while(userHasDone == false) {
                        // All the panels are added to the JOptionPane
                        final int userAnswer =
                                             JOptionPane.showConfirmDialog(Igui.instance().getMainFrame(),
                                                                           new Object[] {"Please, provide some additional information about the reason why the run has been stopped",
                                                                                         systemsPanel, reasonsPanel},
                                                                           IguiServantImpl.stopAlertDialogTitle,
                                                                           JOptionPane.OK_CANCEL_OPTION,
                                                                           JOptionPane.QUESTION_MESSAGE);

                        // These sets keep the list of user selections
                        final Set<rc.StopWhileBeamInfoNamed.CausedBy> selectedSystems =
                                                                                      EnumSet.noneOf(rc.StopWhileBeamInfoNamed.CausedBy.class);
                        final Set<rc.StopWhileBeamInfoNamed.Reason> selectedReasons =
                                                                                    EnumSet.noneOf(rc.StopWhileBeamInfoNamed.Reason.class);

                        if(userAnswer == JOptionPane.OK_OPTION) {
                            // The user has selected "OK"

                            // Loop over the check-boxes: if some is selected then add to the set
                            // Remember that the check-box name is the same as the corresponding enumeration
                            for(final JCheckBox cb : chbSystemsList) {
                                if(cb.isSelected() == true) {
                                    selectedSystems.add(Enum.valueOf(rc.StopWhileBeamInfoNamed.CausedBy.class, cb.getText()));
                                }
                            }

                            for(final JCheckBox cb : chbReasonsList) {
                                if(cb.isSelected() == true) {
                                    selectedReasons.add(Enum.valueOf(rc.StopWhileBeamInfoNamed.Reason.class, cb.getText()));
                                }
                            }

                            // Check whether the user has selected something, if not show again the dialog
                            if(selectedSystems.isEmpty() == true) {
                                JOptionPane.showMessageDialog(Igui.instance().getMainFrame(),
                                                              "Please, select at least one sub-system!",
                                                              IguiServantImpl.stopAlertDialogTitle,
                                                              JOptionPane.WARNING_MESSAGE);
                            } else if(selectedReasons.isEmpty() == true) {
                                JOptionPane.showMessageDialog(Igui.instance().getMainFrame(),
                                                              "Please, select at least one reason!",
                                                              IguiServantImpl.stopAlertDialogTitle,
                                                              JOptionPane.WARNING_MESSAGE);
                            } else {
                                tskResult = new ShowStopAlertDialogTask.TaskResult(Boolean.TRUE, selectedSystems, selectedReasons);
                                userHasDone = true;
                            }
                        } else {
                            tskResult = new ShowStopAlertDialogTask.TaskResult(Boolean.FALSE, selectedSystems, selectedReasons);
                            userHasDone = true;
                        }
                    }

                    return tskResult;
                }
            });
        }
    }

    /**
     * Constructor.
     */
    IguiServantImpl(final Partition partition) {
        super(partition, servantName.value);
    }

    /**
     * It asks the user to take a decision.
     * <p>
     * This is always the result of a remote call.
     * 
     * @param clr Object encapsulating the client request
     * @see iguiCommander.ServerOperations#askUser(iguiCommander.ClientRequest)
     */
    @Override
    public void askUser(final ClientRequest clr) {
        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            final String id = clr.clientId;
            final String msg = clr.msg;
            final Client clientRef = clr.clientRef;

            IguiLogger.info("Received a request from \"" + id + "\" to ask user: \"" + msg + "\"");

            // Execute all the needed actions in the executor thread
            this.executor.execute(new Runnable() {
                @Override
                public void run() {
                    final FutureTask<Integer> task = new AskUserTask(id, msg);

                    javax.swing.SwingUtilities.invokeLater(task);

                    try {
                        final Integer userAnswer = task.get();
                        switch(userAnswer.intValue()) {
                            case JOptionPane.YES_OPTION:
                                clientRef.done(UserAnswer.YES);
                                break;
                            case JOptionPane.NO_OPTION:
                                clientRef.done(UserAnswer.NO);
                                break;
                            case -1:
                                clientRef.done(UserAnswer.CANCELLED);
                                break;
                            default:
                                break;
                        }
                    }
                    catch(final InterruptedException ex) {
                        IguiLogger.warning("Thread interrupted while executing a remote request: " + ex, ex);
                        Thread.currentThread().interrupt();
                    }
                    catch(final ExecutionException ex) {
                        final String errMsg = "Unexpected exception waiting for a user decision on a remote call: " + ex.getCause();
                        IguiLogger.error(errMsg, ex);
                        Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                    }
                    catch(final org.omg.CORBA.SystemException ex) {
                        final String errMsg = "Failed to notify the remote caller \"" + id + "\": " + ex;
                        IguiLogger.error(errMsg, ex);
                        Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                    }
                }
            });
        }
    }

    /**
     * It informs the user showing the message in a modal dialog.
     * 
     * @param msg The message to be shown
     * @param ms The message severity
     */
    @Override
    public void informUser(final String msg, final iguiCommander.MessageSeverity ms) {
        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            IguiLogger.info("Received a remote request to inform the user with the message: \"" + msg + "\". Message priority is "
                            + ms.toString());

            // Execute the action in the executor
            this.executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                            @Override
                            public void run() {
                                final int msgType;
                                switch(ms.value()) {
                                    case iguiCommander.MessageSeverity._ERROR:
                                        msgType = JOptionPane.ERROR_MESSAGE;
                                        break;
                                    case iguiCommander.MessageSeverity._WARNING:
                                        msgType = JOptionPane.WARNING_MESSAGE;
                                        break;
                                    case iguiCommander.MessageSeverity._INFORMATION:
                                        msgType = JOptionPane.INFORMATION_MESSAGE;
                                        break;
                                    default:
                                        msgType = JOptionPane.PLAIN_MESSAGE;
                                        break;
                                }

                                final JOptionPane pane = IguiServantImpl.createOptionPane("Received a remote message:\n" + msg,
                                                                                          msgType,
                                                                                          JOptionPane.DEFAULT_OPTION,
                                                                                          null,
                                                                                          null,
                                                                                          null);
                                pane.createDialog(Igui.instance().getMainFrame(), "IGUI - Remote Message").setVisible(true);
                            }
                        });
                    }
                    catch(final InterruptedException ex) {
                        IguiLogger.warning("Thread unexpectedly interrupted: " + ex, ex);
                        Thread.currentThread().interrupt();
                    }
                    catch(final InvocationTargetException ex) {
                        IguiLogger.error("Unexpected exception trying to show a remote message to the user: " + ex, ex);
                    }
                }
            });
        }
    }

    /**
     * It executes a command requested by a remote caller.
     * 
     * @param cmd The command to be executed
     */
    @Override
    public void executeCommand(final Command cmd) {
        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            IguiLogger.warning("Received unknown remote request to execute the command \"" + cmd.cmd + "\" with args \""
                               + Arrays.toString(cmd.args));
        }
    }

    /**
     * Method implementing the "stop alert" command. A dialog asking the user input about the sub-systems and the reasons why the run is
     * stopped is shown. If the user pushes the "OK" button in the dialog, then the information is written to the IS server
     * {@link IguiConstants#STOP_WHILE_BEAM_IS_INFO_NAME}
     * <p>
     * It is always executed it the {@link #executor} executor.
     * 
     * @see ShowStopAlertDialogTask
     */
    @Override
    public void executeStopAlertCommand() {
        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            this.executor.execute(new Runnable() {
                @Override
                public void run() {
                    // Create the task and execute it into the EDT
                    final FutureTask<ShowStopAlertDialogTask.TaskResult> task = new ShowStopAlertDialogTask();
                    javax.swing.SwingUtilities.invokeLater(task);

                    try {
                        // Get the result of the task
                        final ShowStopAlertDialogTask.TaskResult res = task.get();
                        if(res.okSelected().equals(Boolean.TRUE)) {
                            // The user has pushed "OK" in the dialog: write info to IS
                            final rc.StopWhileBeamInfoNamed isInfo =
                                                                   new rc.StopWhileBeamInfoNamed(Igui.instance().getPartition(),
                                                                                                 IguiConstants.STOP_WHILE_BEAM_IS_INFO_NAME);

                            {
                                final Set<rc.StopWhileBeamInfoNamed.CausedBy> selSystems = res.systemList();
                                isInfo.causedBy = selSystems.toArray(new StopWhileBeamInfoNamed.CausedBy[selSystems.size()]);

                                isInfo.causedByString = new String[isInfo.causedBy.length];
                                int i = 0;
                                for(final rc.StopWhileBeamInfoNamed.CausedBy el : isInfo.causedBy) {
                                    isInfo.causedByString[i] = el.name();
                                    ++i;
                                }
                            }

                            {
                                final Set<rc.StopWhileBeamInfoNamed.Reason> selReasons = res.reasonList();
                                isInfo.reason = selReasons.toArray(new StopWhileBeamInfoNamed.Reason[selReasons.size()]);

                                isInfo.reasonString = new String[isInfo.reason.length];
                                int j = 0;
                                for(final rc.StopWhileBeamInfoNamed.Reason el : isInfo.reason) {
                                    isInfo.reasonString[j] = el.name();
                                    ++j;
                                }
                            }

                            // Write the info to IS: in case of errors try a couple of times
                            int retries = 3;
                            while(retries-- > 0) {
                                try {
                                    isInfo.checkin();
                                    break;
                                }
                                catch(final Exception ex) {
                                    final String errMsg = "StopAlert task: failed writing information to the IS server \""
                                                          + IguiConstants.STOP_WHILE_BEAM_IS_INFO_NAME + "\": " + ex;
                                    if(retries == 0) {
                                        IguiLogger.error(errMsg, ex);
                                        Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                                        ErrorFrame.showError(IguiServantImpl.stopAlertDialogTitle, errMsg, ex);
                                    } else {
                                        IguiLogger.warning(errMsg, ex);
                                        try {
                                            Thread.sleep(1000);
                                        }
                                        catch(final InterruptedException exx) {
                                            // Thread interrupted, continuing will have no sense
                                            IguiLogger.error(errMsg, ex);
                                            Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                                            Thread.currentThread().interrupt();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch(final InterruptedException ex) {
                        final String errStr = "StopAlert task: thread interrupted while waiting for the task to complete: " + ex;
                        IguiLogger.error(errStr, ex);
                        Igui.instance().internalMessage(MessageSeverity.ERROR, errStr);
                        Thread.currentThread().interrupt();
                        ErrorFrame.showError(IguiServantImpl.stopAlertDialogTitle, errStr, ex);
                    }
                    catch(final ExecutionException ex) {
                        final String errStr = "StopAlert task: some errors occurred during the execution: " + ex.getCause();
                        IguiLogger.error(errStr, ex);
                        Igui.instance().internalMessage(MessageSeverity.ERROR, errStr);
                        ErrorFrame.showError(IguiServantImpl.stopAlertDialogTitle, errStr, ex);
                    }
                }
            });
        }
    }

    @Override
    public void shutdown() {
        // Do nothing
    }

    @Override
    synchronized public void publish() throws InvalidPartitionException {
        if(this.isReleased == true) {
            throw new IllegalStateException("The underlining CORBA object has already been destroyed");
        }

        super.publish();
    }

    @Override
    synchronized public void withdraw() throws InvalidPartitionException {
        if(this.isReleased == true) {
            throw new IllegalStateException("The underlining CORBA object has already been destroyed");
        }

        // We need to protect the withdraw otherwise any IGUI instance
        // can remove the publication of a different instance
        if(this.checkPublishedObject() == true) {
            super.withdraw();
        } else {
            IguiLogger.info("IPC publication not removed because this instance is not currently published");
        }
    }

    /**
     * It checks if the currently object published in IPC is this object
     * 
     * @return <em>true</em> if the current object published in IPC is this one
     */
    private boolean checkPublishedObject() {
        boolean thatsMe = false;

        try {
            thatsMe = this.isPublished();
        }
        catch(final InvalidPartitionException | SystemException ex) {
            IguiLogger.warning("Most likely the current instance is not published in IPC: " + ex, ex);
        }

        return thatsMe;
    }

    /**
     * It destroys the underlining CORBA object.
     * <p>
     * Note: after a call to this method, the object cannot be used anymore
     */
    synchronized public void release() {
        try {
            super.finalize();
        }
        catch(final Throwable e) {
            e.printStackTrace();
        }

        this.isReleased = true;
    }

    private static JOptionPane createOptionPane(final String msg,
                                                final int messageType,
                                                final int optionType,
                                                final Icon icon,
                                                final Object[] options,
                                                final Object initialValue)
    {
        // Allow scrolling if the message is too long
        final JTextArea jta = new AutoResizingTextArea(10, 50, 40);
        jta.setEditable(false);
        jta.setLineWrap(true);
        jta.setWrapStyleWord(true);
        jta.setFont(new java.awt.Font("Sans", Font.PLAIN, 13));
        jta.setBorder(null);
        jta.setText(msg);
        jta.setCaretPosition(0);

        return new JOptionPane(new JScrollPane(jta), messageType, optionType, icon, options, initialValue);
    }
}
