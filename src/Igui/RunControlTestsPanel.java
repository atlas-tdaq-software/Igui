package Igui;

import is.InfoEvent;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import rc.ApplicationTestInfo;
import rc.TestInfo;
import Igui.IguiConstants.AccessControlStatus;
import Igui.IguiException.ISException;
import Igui.RunControlFSM.ApplicationStatus;
import Igui.RunControlFSM.State;
import Igui.RunControlTreePanel.RCTreeNode;
import Igui.Common.TreeSelectionFilteredModel;
import TM.Test.Scope;
import daq.rc.Command;
import daq.rc.RCException;
import daq.rc.RCException.BadCommandException;


/**
 * This panel shows a tree structure representing the status of tests performed on the applications running on the system.
 * <p>
 * If a segment's controller is selected in the Run Control tree, then the status of all the applications in that segment is shown; if a
 * leaf application is selected, then the status of that specific application is shown. Every application in the tree can be expanded in
 * order to have a list of all the components that are linked to that application (e.g., the SW or HW components that the application uses
 * and that should properly work to be fully functional).
 * <p>
 * Status of tests are reported with different colors in the tree: green if the test is passed, red if the test failed, blue if tests have
 * not been performed yet (that is the default value).
 * <p>
 * All the nodes in the tree are instances of the {@link TestTreeNode} class and the used tree model is {@link TestTreeModel}.
 * <p>
 * The {@link RunControlMainPanel} panel is the parent of this panel: this means that it is not the {@link Igui} to call methods on this
 * panel.
 */

final class RunControlTestsPanel extends AbstractRunControlPanel {
    private static final long serialVersionUID = -4212842295490405L;

    /**
     * This panel name
     */
    private final static String panelName = "TestResults";

    /**
     * Timeout (in seconds) used to wait for the IS subscription to be removed
     */
    private final static int TIMEOUT = 10;

    /**
     * Executor used to process IS call-backs and build the application's test tree
     */
    private final ThreadPoolExecutor isCallbackExecutor = ExecutorFactory.createExecutor(this.getPanelName() + "-IS-Executor", 3);

    /**
     * Single executor used to perform or remove IS subscriptions (using a single thread for doing that make life much easier)
     */
    private final ThreadPoolExecutor isSubscriberExecutor = ExecutorFactory.createSingleExecutor(this.getPanelName() + "-IS-Subscriber");

    /**
     * Single executor used to send commands to the selected application
     */
    private final ThreadPoolExecutor cmdSender = ExecutorFactory.createSingleExecutor(this.getPanelName() + "-Cmd-Sender");

    /**
     * IS info receiver to manage IS call-backs and subscriptions
     */
    private final SetupISReceiver isReceiver = new SetupISReceiver(this);

    /**
     * RW lock used to synchronize call-backs execution and tree building actions
     */
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock(true);

    /**
     * The label at the top of the panel
     */
    private final JLabel cLabel = new JLabel() {
        private static final long serialVersionUID = 3346177336934745472L;

        @Override
        public void setText(final String text) {
            super.setText(text);
            this.setToolTipText(text);
        }
    };

    /**
     * The tree instance
     */
    private final JTree testTree = new JTree();

    /**
     * The root node
     */
    private final TestTreeNode rootNode = TestTreeNode.createRootNode();

    /**
     * The tree model
     */
    private final RunControlTestsPanel.TestTreeModel testTreeModel = new TestTreeModel(this.rootNode);

    // Menu items
    private final JMenuItem popLog = new JMenuItem("Get Log");
    private final JMenuItem popTestComp = new JMenuItem("Test");

    // Pop up menus
    private final JPopupMenu popupIL = new JPopupMenu();

    // Icon file names
    private final static String testIconFile = "retry.png";
    private final static String logIconFile = "log.png";

    /**
     * Object button actions are associated with
     */
    private final ButtonActions bActions = new ButtonActions();

    /**
     * Boolean saying whether this panel instance is used for the initial infrastructure test or not
     */
    private final boolean isInitialInfrastructureTests;

    /**
     * Enum for the test status
     */
    private enum TestStatus {
        PASSED,
        UNDEFINED,
        FAILED,
        UNRESOLVED,
        UNTESTED,
        UNSUPPORTED;
    }

    /**
     * Enum describing the IS call-back type
     */
    private enum CallbackType {
        CREATED,
        UPDATED,
        DELETED
    }

    /**
     * Panel used for the initial infrastructure test when the Igui is started.
     * 
     * See also {@link Igui#setupInfrastructureCheckFrame()} and {@link Igui#checkRootControllerInfrastructure()}
     */
    static class InitialInfrastructureTestsPanel extends JPanel {
        private static final long serialVersionUID = -1180869748664252182L;

        // This panel contains the tree tree
        private final RunControlTestsPanel testPanel = new RunControlTestsPanel();

        // This panel contains two buttons to send commands
        private final JButton ignoreButton = new JButton("Ignore & Continue");
        private final JButton restartAppButton = new JButton("Restart app(s)");

        // This boolean says whether the base infrastructure is ready or not
        private final AtomicBoolean isInfrReady = new AtomicBoolean(false);

        // The lock and the conditional variables are used to synchronize with the Igui and notify it as
        // soon as the infrastructure has been successfully tested (so that the full Igui may be shown)
        private final ReentrantLock lock;
        private final Condition cond;

        // The thread used to check the infrastructure status
        private final CheckInfrastructureThread infrThread;

        /**
         * This thread is used to perform all the operations needed to check whether the infrastructure is healthy and the full Igui can
         * then be shown.
         */
        private class CheckInfrastructureThread extends Thread {
            CheckInfrastructureThread() {
            }

            /**
             * Subscribe to the setup IS server.
             * 
             * @throws InterruptedException The thread has been interrupted
             */
            private void subscribeSetupIS() throws InterruptedException {
                {
                    final String msg = "Connecting to the setup IS server...";
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                }

                long timeToStop = new Date().getTime() + (IguiConstants.SETUP_TIMEOUT * 1000L);
                boolean subscriptionSuccess = false;
                // Loop until the subscription is done or the timeout expires
                while(subscriptionSuccess == false) {
                    try {
                        InitialInfrastructureTestsPanel.this.subScribeIS();
                        subscriptionSuccess = true;
                    }
                    catch(final ISException ex) {
                        // When this panel is shown the setup IS server may not be running yet...
                        // Check whether the timeout is elapsed
                        final long now = new Date().getTime();
                        if(now > timeToStop) {
                            timeToStop = now + (IguiConstants.SETUP_TIMEOUT * 1000);
                            final String msg = "Failed to check the infrastructure status: " + ex + ".\nStill keeping trying...";
                            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                            IguiLogger.error(msg);
                        }
                    }
                    finally {
                        // Slow down the loop
                        Thread.sleep(333);
                    }
                }

                {
                    final String msg = "Connected to the setup IS server";
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                }
            }

            /**
             * It checks the status of the Root Controller
             * 
             * @throws InterruptedException The thread has been interrupted
             */
            private void checkRootCtrlStatus() throws InterruptedException {
                {
                    final String msg = "Waiting for the Root Controller to reach the NONE state...";
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                }

                final is.Repository isRepo = InitialInfrastructureTestsPanel.this.testPanel.isReceiver.getRepository();
                final rc.RCStateInfo rootCtrlInfo = new rc.RCStateInfo();

                // Start looping until the Root Controller reaches the NONE state
                long timeToStop = new Date().getTime() + (IguiConstants.RC_NONE_STATE_TIMEOUT * 1000);
                boolean isRootCtrlReady = false;
                boolean previouslyFault = false;
                while(isRootCtrlReady == false) {
                    try {
                        isRepo.getValue(IguiConstants.RC_IS_SERVER_NAME + "." + Igui.instance().getRootControllerName(), rootCtrlInfo);
                        final RunControlFSM.State rcState = RunControlFSM.getStateFromString(rootCtrlInfo.state);
                        final boolean isFault = rootCtrlInfo.fault;
                        if(rcState.equals(RunControlFSM.State.NONE) || rcState.follows(RunControlFSM.State.NONE)) {
                            // If the Root Controller is a state >= NONE we stop the loop
                            isRootCtrlReady = true;
                        } else {
                            // NONE has not been reached yet, check the error state
                            if(isFault == true) {
                                // The RC is in error
                                if(previouslyFault == false) {
                                    // If it was not in error previously then show some message and enable the buttons if needed
                                    if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
                                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                final String msg = "The Root Controller is in error state: "
                                                                   + Arrays.toString(rootCtrlInfo.errorReasons)
                                                                   + ". Fix the problem or consider to ignore";
                                                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                                                InitialInfrastructureTestsPanel.this.enableButtons(true);
                                            }
                                        });
                                    } else {
                                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                final String msg = "The Root Controller is in error state ("
                                                                   + Arrays.toString(rootCtrlInfo.errorReasons)
                                                                   + ") and the IGUI is in STATUS DISPLAY mode: please, exit and try again";
                                                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                                                InitialInfrastructureTestsPanel.this.enableButtons(false);
                                            }
                                        });
                                    }
                                }
                            }
                        }

                        // Keep note whether we already noticed the RC error state
                        previouslyFault = isFault;
                    }
                    catch(final Exception ex) {
                        final long now = new Date().getTime();
                        if(now > timeToStop) {
                            timeToStop = now + (IguiConstants.RC_NONE_STATE_TIMEOUT * 1000);
                            final String msg = "Failed get the Root Controller state from IS: " + ex + ".\nStill keeping trying...";
                            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                            IguiLogger.error(msg);
                        }
                    }
                    finally {
                        // Slow down the loop
                        Thread.sleep(333);
                    }
                }

                {
                    final String msg = "The Root Controller reached the NONE state";
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                }
            }

            @Override
            public void run() {
                try {
                    // Try to subscribe to the SETUP IS server first
                    this.subscribeSetupIS();

                    // Build the tree
                    InitialInfrastructureTestsPanel.this.buildTree();

                    // Wait until the Root Controller is in the NONE state
                    this.checkRootCtrlStatus();

                    {
                        final String msg = "Infrastructure check done, creating the full IGUI...";
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                        IguiLogger.info(msg);
                    }

                    if(this.isInterrupted() == true) {
                        throw new InterruptedException();
                    }

                    // Infrastructure check finished: notify who is waiting on the condition
                    InitialInfrastructureTestsPanel.this.lock.lock();
                    try {
                        InitialInfrastructureTestsPanel.this.isInfrReady.set(true);
                        InitialInfrastructureTestsPanel.this.cond.signalAll();
                    }
                    finally {
                        InitialInfrastructureTestsPanel.this.lock.unlock();
                    }
                }
                catch(final InterruptedException ex) {
                    final String msg = "The thread checking the initial infrastructure has been interrupted";
                    IguiLogger.warning(msg, ex);
                }
            }
        }

        /**
         * Constructor
         * 
         * @param lock Lock used with the <code>cond</code> condition variable
         * @param cond Conditional variable used to notify listeners that the infrastructure is ready
         */
        InitialInfrastructureTestsPanel(final ReentrantLock lock, final Condition cond) {
            super(new BorderLayout());

            assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

            this.lock = lock;
            this.cond = cond;

            this.initGUI();

            this.infrThread = new CheckInfrastructureThread();
            this.infrThread.setPriority(Thread.NORM_PRIORITY);
        }

        /**
         * It returns <code>true</code> if the infrastructure is ready.
         * 
         * @return <code>true</code> if the infrastructure is ready
         */
        public boolean isInfrastructureReady() {
            return this.isInfrReady.get();
        }

        /**
         * It starts the thread checking the infrastructure status.
         */
        void startInfrastructureCheck() {
            this.infrThread.start();
        }

        /**
         * It stops the checking thread and makes some cleanup.
         */
        void terminate() {
            this.infrThread.interrupt();
            this.testPanel.panelTerminated();
        }

        /**
         * It performs the subscription to the setup IS server
         * 
         * @throws ISException The subscription failed.
         */
        private void subScribeIS() throws ISException {
            final ISInfoReceiver setupISrec = this.testPanel.isReceiver;
            if(!setupISrec.isSubscribed()) {
                setupISrec.subscribe();
            }
        }

        /**
         * It builds the tree of applications.
         */
        private final void buildTree() {
            this.testPanel.isCallbackExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        // You do not need to wait for the tree to be built to go on, that's why we do it in the executor
                        InitialInfrastructureTestsPanel.this.testPanel.buildTestTree(Igui.instance().getRootControllerName(),
                                                                                     Igui.instance().getRootControllerSegmentName(),
                                                                                     true);
                    }
                    catch(final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
            });
        }

        /**
         * It enables/disables the panel buttons.
         * 
         * @param enable If <code>true</code> the buttons are enabled
         */
        private void enableButtons(final boolean enable) {
            assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

            this.ignoreButton.setEnabled(enable);
            this.restartAppButton.setEnabled(enable);
        }

        /**
         * It builds and initializes the GUI and its components.
         */
        private void initGUI() {
            // Action listeners for the buttons
            this.ignoreButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    try {
                        InitialInfrastructureTestsPanel.this.testPanel.bActions.sendCommand(Igui.instance().getRootControllerName(),
                                                                                            new daq.rc.Command.IgnoreErrorCmd());

                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION,
                                                        "The \"" + daq.rc.Command.RCCommands.IGNORE_ERROR.name()
                                                            + "\" command has been sent to \"" + Igui.instance().getRootControllerName()
                                                            + "\"");
                    }
                    catch(final BadCommandException ex) {
                        final String msg = "Failed sending the \"" + daq.rc.Command.RCCommands.IGNORE_ERROR.name()
                                           + "\" command to the Root Controller: " + ex.toString();
                        IguiLogger.error(msg, ex);
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    }
                }
            });

            this.restartAppButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    final Set<String> as = new TreeSet<>();

                    for(final TestTreeNode n : InitialInfrastructureTestsPanel.this.testPanel.getSelectedNode()) {
                        final TreeNode parent = n.getParent();
                        if(parent != null) {
                            final TestTreeNode p = (TestTreeNode) parent;
                            if(p.isRoot() == true) {
                                // The parent is root: an application node is selected
                                as.add(n.toString());
                            }
                        }
                    }

                    if(as.isEmpty() == false) {
                        try {
                            InitialInfrastructureTestsPanel.this.testPanel.bActions.sendCommand(Igui.instance().getRootControllerName(),
                                                                                                new daq.rc.Command.RestartAppsCmd(as.toArray(new String[as.size()])));

                            Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION,
                                                            "The \"" + daq.rc.Command.RCCommands.RESTARTAPP.name()
                                                                + "\" command has been sent to \""
                                                                + Igui.instance().getRootControllerName() + "\"");
                        }
                        catch(final BadCommandException ex) {
                            final String msg = "Failed sending the \"" + daq.rc.Command.RCCommands.RESTARTAPP.name()
                                               + "\" command to the Root Controller: " + ex.toString();
                            IguiLogger.error(msg, ex);
                            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                        }
                    } else {
                        ErrorFrame.showInfo("Please, select at least one application to be restarted");
                    }
                }

            });

            final JPanel emptyPanel = new JPanel();
            emptyPanel.setBackground(Color.WHITE);
            this.add(emptyPanel, BorderLayout.NORTH);
            this.setBackground(new Color(249, 219, 97));
            this.add(this.testPanel, BorderLayout.CENTER);
            final JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
            buttonPanel.add(this.restartAppButton);
            buttonPanel.add(this.ignoreButton);
            buttonPanel.setBackground(Color.WHITE);
            this.add(buttonPanel, BorderLayout.SOUTH);

            this.enableButtons(false);
        }
    }

    /**
     * IS information receiver to manage the setup IS server call-backs and perform or remove subscriptions.
     * <p>
     * Call-back are processed in {@link RunControlTestsPanel#processSetupISCallback(InfoEvent, CallbackType)}.
     */
    private final static class SetupISReceiver extends ISInfoReceiver {
        // This keeps a weak reference to the panel: we do not want strong references to it around; only the Igui
        // keeps strong references to its panels
        private final WeakReference<RunControlTestsPanel> infrPanel;

        /**
         * Constructor.
         * 
         * @param testPanel Reference to the enclosing panel
         */
        SetupISReceiver(final RunControlTestsPanel infrPanel) {
            super(IguiConstants.SETUP_IS_SERVER_NAME, new is.Criteria(Pattern.compile(".*"),
                                                                      ApplicationTestInfo.type,
                                                                      is.Criteria.Logic.AND));

            this.infrPanel = new WeakReference<RunControlTestsPanel>(infrPanel);
        }

        @Override
        protected void infoCreated(final InfoEvent info) {
            try {
                this.infrPanel.get().processSetupISCallback(info, CallbackType.CREATED);
            }
            catch(final NullPointerException ex) {
                IguiLogger.debug("Trying to process an IS callback after the panel has been garbage collected", ex);
            }
        }

        @Override
        protected void infoDeleted(final InfoEvent info) {
            try {
                this.infrPanel.get().processSetupISCallback(info, CallbackType.DELETED);
            }
            catch(final NullPointerException ex) {
                IguiLogger.debug("Trying to process an IS callback after the panel has been garbage collected", ex);
            }
        }

        @Override
        protected void infoUpdated(final InfoEvent info) {
            try {
                this.infrPanel.get().processSetupISCallback(info, CallbackType.UPDATED);
            }
            catch(final NullPointerException ex) {
                IguiLogger.debug("Trying to process an IS callback after the panel has been garbage collected", ex);
            }
        }
    }

    /**
     * Class used to keep the nodes in the tree sorted
     */
    private static class NodeComparator implements Comparator<TreeNode> {
        static final RunControlTestsPanel.NodeComparator instance = new RunControlTestsPanel.NodeComparator();
        
        private NodeComparator() {            
        }
        
        @Override
        public int compare(final TreeNode o1, final TreeNode o2) {
            final TestTreeNode o1_c = (TestTreeNode) o1;
            final TestTreeNode o2_c = (TestTreeNode) o2;

            return o1_c.compareTo(o2_c);
        }
    }
    
    /**
     * Model for the tree. The tree can be modified <b>only</b> by methods of this model.
     * <p>
     * NOTE: call methods of this model only in the EDT!
     * <p>
     * This model implements sorting.
     */
    private final class TestTreeModel extends DefaultTreeModel {
        private static final long serialVersionUID = -4983887126396036485L;

        /**
         * Map containing the tree nodes
         * <p>
         * Application nodes are identified by their name. Component nodes are identified by the full name of the IS information (the same
         * component with the same name may appear several times in the tree)
         */
        private final Map<String, TestTreeNode> nodeMap = new HashMap<>(100);

        /**
         * Constructor
         * 
         * @param root The root node
         */
        TestTreeModel(final TestTreeNode root) {
            super(root);
        }

        /**
         * It updates application nodes in the the tree
         * <p>
         * If an application is not there, then it is added
         * 
         * @param infos Collections containing the information about the nodes
         */
        public void updateApplications(final List<? extends rc.ApplicationTestInfo> infos) {
            for(final rc.ApplicationTestInfo info : infos) {
                this.updateApplication(info);
            }
        }

        /**
         * It updates an application node in the tree
         * <p>
         * If the application is not there, the it is added
         * 
         * @param info Information about the application
         */
        public void updateApplication(final rc.ApplicationTestInfo info) {
            TestTreeNode app = this.nodeMap.get(info.globalResult.objectId);
            if(app != null) {
                if(info.getTime().getTimeMicro() >= app.getUserObject().getTime().getTimeMicro()) {
                    // First remove the node from parent
                    this.removeNodeFromParent(app);

                    // Now update the node's information
                    app.updateInfo(info.globalResult);

                    // Remove all the children and add the new ones
                    app.removeAllChildren();
                    for(final TestInfo chInfo : info.componentsResult) {
                        app.add(new TestTreeNode(chInfo));
                    }

                    Collections.sort(app.getChildVector(), RunControlTestsPanel.NodeComparator.instance);

                    // Calculate the new position and insert the node back
                    final int insPoint = -1
                                         - Collections.binarySearch(RunControlTestsPanel.this.rootNode.getChildVector(),
                                                                    app,
                                                                    RunControlTestsPanel.NodeComparator.instance);

                    this.insertNodeInto(app, RunControlTestsPanel.this.rootNode, insPoint);
                }
            } else {
                app = new TestTreeNode(info);
                this.nodeMap.put(info.globalResult.objectId, app);

                // Calculate the new position and insert the node back
                final int insPoint = -1
                                     - Collections.binarySearch(RunControlTestsPanel.this.rootNode.getChildVector(),
                                                                app,
                                                                RunControlTestsPanel.NodeComparator.instance);

                this.insertNodeInto(app, RunControlTestsPanel.this.rootNode, insPoint);
            }

            RunControlTestsPanel.this.testTree.expandPath(new TreePath(RunControlTestsPanel.this.rootNode));
        }

        /**
         * It removes an application node from the tree
         * 
         * @param appName The name of the application
         */
        public void removeApplication(final String appName) {
            final TestTreeNode app = this.nodeMap.remove(appName);
            if(app != null) {
                this.removeNodeFromParent(app);
            }
        }

        /**
         * It removes all the nodes from the tree
         */
        public void cleanAll() {
            this.nodeMap.clear();
            RunControlTestsPanel.this.rootNode.removeAllChildren();
            this.nodeStructureChanged(RunControlTestsPanel.this.rootNode);
        }
    }

    /**
     * Class for nodes in the tree
     * <p>
     * Information about the nodes are stored in a #{@link rc.TestInfo} user object.
     */
    private static class TestTreeNode extends DefaultMutableTreeNode implements Comparable<TestTreeNode> {
        private static final long serialVersionUID = -1388106322264078268L;

        /**
         * String used to identify the node: it is made up of the name of the class of the corresponding IS information and the object id
         */
        private final String fullObjId;

        /**
         * Lock to protect mutable node data
         */
        private final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
        
        /**
         * Constructor for child nodes (not applications)
         * 
         * @param info Child node information
         */
        TestTreeNode(final TestInfo info) {
            super(info);
            // Use the objectId attribute as id
            this.fullObjId = info.getClass().getName() + "/" + info.objectId;
        }

        /**
         * Constructor for application nodes
         * 
         * @param info Application node information
         */
        TestTreeNode(final ApplicationTestInfo info) {
            this(info.globalResult);

            for(final TestInfo chInfo : info.componentsResult) {
                this.add(new TestTreeNode(chInfo));

                Collections.sort(this.getChildVector(), RunControlTestsPanel.NodeComparator.instance);
            }
        }

        /**
         * To compare two nodes the strings resulting from the concatenation of the result of the {@link #strToCompare()} method and this
         * {@link #fullObjId} are used. This gives the nodes ordered per test status and name.
         * 
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         * @see #strToCompare()
         */
        @Override
        public int compareTo(final TestTreeNode obj) {
            final String thisObjCmprStr = this.strToCompare().concat(this.fullObjId);
            final String otherObjCmprStr = obj.strToCompare().concat(obj.fullObjId);
            return thisObjCmprStr.compareTo(otherObjCmprStr);
        }

        @Override
        public int hashCode() {
            return this.strToCompare().concat(this.fullObjId).hashCode();
        }

        @Override
        public boolean equals(final Object obj) {
            if((obj != null) && (TestTreeNode.class.isInstance(obj))) {
                final TestTreeNode other = (TestTreeNode) obj;
                return(this.compareTo(other) == 0);
            }

            return false;
        }

        @Override
        public rc.TestInfo getUserObject() {
            try {
                this.rwLock.readLock().lock();
                return (rc.TestInfo) super.getUserObject();
            }
            finally {
                this.rwLock.readLock().unlock();
            }
        }

        @Override
        public String toString() {
            return this.getUserObject().objectId;
        }

        static TestTreeNode createRootNode() {
            final TestInfo rootinfo = new TestInfo();
            rootinfo.objectId = "ROOT";
            rootinfo.testStatus = TestStatus.UNDEFINED.name();
            rootinfo.testLog = "";
            return new TestTreeNode(rootinfo);
        }

        /**
         * It updates this node status using the information from the <code>info</code> update.
         * <p>
         * The <code>info</code> information has to represent the same object of this node.
         * 
         * @param updtNode The "updated" node.
         */
        void updateInfo(final rc.TestInfo info) {
            try {
                this.rwLock.writeLock().lock();
                this.setUserObject(info);
            }
            finally {
                this.rwLock.writeLock().unlock();
            }
        }

        /**
         * It returns the vector containing this node children.
         * <p>
         * The children vector is exported to allow node sorting in the model.
         * 
         * @return A vector containing all this node children
         */
        final Vector<TreeNode> getChildVector() {
            Vector<TreeNode> v = this.children;
            if(v == null) {
                v = new Vector<TreeNode>();
                this.children = v;
            }

            return v;
        }

        /**
         * It returns a string used to compare two nodes (needed to sort).
         * 
         * @return A string used to compare two nodes
         * @see #compareTo(TestTreeNode)
         */
        private String strToCompare() {
            String returnStr;

            final String testStatus = this.getUserObject().testStatus;

            // The nodes are sorted first by the test status
            try {
                final TestStatus ts = TestStatus.valueOf(testStatus);
                switch(ts) {
                    case FAILED:
                        returnStr = "1.";
                        break;
                    case UNTESTED:
                    case UNRESOLVED:
                        returnStr = "2.";
                        break;
                    case PASSED:
                        returnStr = "3.";
                        break;
                    case UNDEFINED:
                    case UNSUPPORTED:
                    default:
                        returnStr = "4.";
                        break;
                }
            }
            catch(final IllegalArgumentException ex) {
                returnStr = TestStatus.UNDEFINED.name();
            }

            return returnStr;
        }
    }

    /**
     * Tree cell renderer.
     */
    private static final class TestTreeRenderer implements TreeCellRenderer {
        private final ImageIcon red_icon = Igui.createIcon("folder-red.png");
        private final ImageIcon green_icon = Igui.createIcon("folder-green.png");
        private final ImageIcon yellow_icon = Igui.createIcon("folder-yellow.png");
        private final ImageIcon blue_icon = Igui.createIcon("folder-blue.png");

        private final Color ALIVE_COLOR = Color.black;
        private final Color SELECTED_COLOR = Color.yellow;

        private final JPanel panel = new JPanel();
        private final JLabel name = new JLabel();

        TestTreeRenderer() {
            this.name.setOpaque(false);
            this.name.setBorder(BorderFactory.createEmptyBorder());

            this.panel.setLayout(new GridLayout());
            this.panel.add(this.name);
            this.panel.setBorder(BorderFactory.createEmptyBorder(1, 0, 1, 2));
            this.panel.setOpaque(false);
        }

        @Override
        public Component getTreeCellRendererComponent(final JTree tree,
                                                      final Object value,
                                                      final boolean sel,
                                                      final boolean expanded,
                                                      final boolean leaf,
                                                      final int row,
                                                      final boolean hasFocus)
        {
            final TestTreeNode node = (TestTreeNode) value;

            final rc.TestInfo nodeInfo = node.getUserObject();
            final String testStatus = nodeInfo.testStatus;

            this.name.setForeground(this.ALIVE_COLOR);
            this.name.setText(nodeInfo.objectId);

            try {
                final TestStatus ts = TestStatus.valueOf(testStatus);
                switch(ts) {
                    case FAILED:
                        this.name.setIcon(this.red_icon);
                        break;
                    case UNRESOLVED:
                    case UNTESTED:
                        this.name.setIcon(this.yellow_icon);
                        break;
                    case PASSED:
                        this.name.setIcon(this.green_icon);
                        break;
                    case UNDEFINED:
                    case UNSUPPORTED:
                    default:
                        this.name.setIcon(this.blue_icon);
                        break;
                }
            }
            catch(final IllegalArgumentException ex) {
                this.name.setIcon(this.blue_icon);

                final String msg = "Test result \"" + testStatus + "\" for object \"" + nodeInfo.objectId + "\" is not valid status";
                IguiLogger.error(msg, ex);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
            }

            if(sel) {
                this.name.setBackground(this.SELECTED_COLOR);
                this.name.setOpaque(true);
            } else {
                this.name.setOpaque(false);
            }

            return this.panel;
        }
    }

    /**
     * Mouse listener: pressing the right mouse button on a tree node, a contextual menu appears allowing the user to take actions and
     * recover a bad situation.
     */
    private final class MouseListener extends MouseAdapter {
        MouseListener() {
        }

        @Override
        public void mousePressed(final MouseEvent e) {
            if(javax.swing.SwingUtilities.isRightMouseButton(e)) {
                {
                    // The tree allows multiple node selection: when the button is clicked first check if the mouse
                    // cursor is upon some tree node; the check if that tree node is already selected: if it is then do nothing
                    // (this will not alter the current selection), otherwise select it.
                    final TreePath tp = RunControlTestsPanel.this.testTree.getPathForLocation(e.getX(), e.getY());
                    final boolean isSelected = RunControlTestsPanel.this.testTree.getSelectionModel().isPathSelected(tp);
                    if(isSelected == false) {
                        RunControlTestsPanel.this.testTree.setSelectionPath(tp);
                    }
                }

                // Here the selected path may be still null (the mouse pointer is far from any tree node)
                final TreePath selectedPath = RunControlTestsPanel.this.testTree.getSelectionPath();
                if(selectedPath != null) {
                    if(!Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
                        this.enableMenuItems(false);
                    } else {
                        this.enableMenuItems(true);
                    }

                    RunControlTestsPanel.this.popupIL.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }

        /**
         * It enables or disables the menu items in the pop-up menu.
         * <p>
         * If the way menu items are enabled or disabled is changed then the tree selection model most likely needs to be modified
         * accordingly.
         * 
         * @param enable If <code>true</code> the menu items are enabled
         */
        private void enableMenuItems(final boolean enable) {
            RunControlTestsPanel.this.popTestComp.setEnabled(enable);
            // The menu item for log is always enabled
        }
    }

    /**
     * Selection model for the tree: the model allows multiple selection only if the selected nodes are of the same 'kind': they have the
     * same parent.
     * <p>
     * What is done here must be consistent with the decision taken in mouse listener about what menus/items should be shown/enabled when
     * the right mouse button is pressed.
     * <p>
     * Whenever this model is changed most likely the actions associated to the menu items should be modified as well.
     * 
     * @see MouseListener
     * @see RunControlTestsPanel.MouseListener#enableMenuItems(boolean)
     */
    private final static class TestTreeSelectionModel extends TreeSelectionFilteredModel {
        private static final long serialVersionUID = 8940030686430267030L;

        TestTreeSelectionModel() {
        }

        /**
         * @see Igui.Common.TreeSelectionFilteredModel#areNodesEquivalent(java.lang.Object, java.lang.Object)
         */
        @Override
        protected boolean areNodesEquivalent(final Object n, final Object m) {
            boolean equivalent = false;

            final TestTreeNode trn1 = (TestTreeNode) n;
            final TestTreeNode trn2 = (TestTreeNode) m;

            final TestTreeNode p1 = (TestTreeNode) trn1.getParent();
            final TestTreeNode p2 = (TestTreeNode) trn2.getParent();
            if(p1 == p2) {
                equivalent = true;
            }

            return equivalent;
        }
    }

    /**
     * Class containing methods associated to actions performed when a menu item is selected.
     */
    private final class ButtonActions {
        ButtonActions() {
        }

        /**
         * It sends the <code>cmd</code> command with <code>args</code> argument to the <code>cName</code> application.
         * <p>
         * Since sending the command implies a remote call, this action is executed using the {@link RunControlTestsPanel#cmdSender}
         * executor.
         * 
         * @param cName The name of the application the command is sent to
         * @param cmd The command to sent
         * @see Igui#sendControllerCommand(String, daq.rc.Command)
         */
        void sendCommand(final String cName, final daq.rc.Command cmd) {
            RunControlTestsPanel.this.cmdSender.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Igui.instance().sendControllerCommand(cName, cmd);
                    }
                    catch(final RCException ex) {
                        final String errMsg = "Failed sending the \"" + cmd.toString() + "\" command with to \"" + cName + "\": " + ex;
                        IguiLogger.error(errMsg, ex);
                        ErrorFrame.showError("IGUI - Command Error", errMsg, ex);
                    }
                }
            });
        }

        /**
         * It shows a frame containing the log of the test for the component associated to the <code>nodeId</code> node.
         * <p>
         * It is safe to execute this method outside the EDT.
         * 
         * @param log The test log as a string
         * @param nodeId The string identifying the node
         */
        void showLog(final String log, final String nodeId) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final JFrame window = new JFrame("Test Log for " + nodeId);
                    window.getContentPane().setLayout(new BorderLayout());
                    window.setPreferredSize(new Dimension(600, 600));
                    window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                    final JTextArea logText = new JTextArea();
                    logText.setEditable(false);
                    logText.setBackground(new Color(255, 255, 232));
                    logText.setLineWrap(true);
                    logText.setFont(new Font("Roman", Font.PLAIN, 15));
                    logText.setText(log);

                    final JScrollPane scrollText = new JScrollPane(logText);

                    window.add(scrollText);
                    window.pack();
                    window.setLocationRelativeTo(RunControlTestsPanel.this);
                    window.setVisible(true);
                }
            });

        }

    }

    /**
     * Constructor.
     * <p>
     * This is the constructor to be used when the panel is shown as a {@link RunControlMainPanel} sub-panel.
     * 
     * @param mainRCPanel Reference to the {@link RunControlMainPanel} panel
     */
    RunControlTestsPanel(final RunControlMainPanel mainRCPanel) {
        super(mainRCPanel);
        this.isInitialInfrastructureTests = false;
        this.initGUI();
        // This panel needs to know when a different RC tree node is selected
        this.subscribeNodeSelection();
        // this.subscribeNodeChanges();
    }

    /**
     * Constructor.
     * <p>
     * This is the constructor to be used when the panel is used at the beginning to show the base infrastructure test status.
     */
    RunControlTestsPanel() {
        super(null);
        this.isInitialInfrastructureTests = true;
        this.initGUI();
    }

    /**
     * Nothing to do: the mouse listener checks the access control status before showing the contextual menu, enabling and disabling the
     * menu items properly.
     * 
     * @see Igui.IguiInterface#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void accessControlChanged(final AccessControlStatus newAccessStatus) {
    }

    /**
     * Nothing to do when the database is reloaded.
     * 
     * @see Igui.IguiInterface#dbReloaded()
     */
    @Override
    public void dbReloaded() {
    }

    /**
     * @see Igui.IguiInterface#getPanelName()
     */
    @Override
    public String getPanelName() {
        return RunControlTestsPanel.panelName;
    }

    /**
     * Nothing to do: the panel takes all the needed actions when it is (de)selected.
     * 
     * @see Igui.IguiInterface#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void panelInit(final State rcState, final AccessControlStatus accessStatus) {
    }

    /**
     * The IS subscription is removed.
     * <p>
     * Note that the {@link RunControlMainPanel} executes this method in the EDT.
     * 
     * @see Igui.IguiInterface#panelDeselected()
     * @see RunControlMainPanel#panelDeselected()
     */
    @Override
    public void panelDeselected() {
        // Use the right executor to remove the IS subscription!
        // This executor has a single thread, so that subscriptions and unsubscriptions cannot be out of order
        this.isSubscriberExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    RunControlTestsPanel.this.unsubscribeIS();
                }
                catch(final ISException ex) {
                    IguiLogger.warning(ex.getMessage(), ex);
                }
            }
        });
    }

    /**
     * The IS subscription is performed and the tree is built taking into account the selected RC tree node.
     * <p>
     * Note the the {@link RunControlMainPanel} panel executes this method in the EDT.
     * 
     * @see Igui.IguiInterface#panelSelected()
     * @see #buildTestTree(RunControlApplicationData)
     */
    @Override
    public void panelSelected() {
        // Get the selected RC tree node
        final RCTreeNode selContr = this.getMainPanel().getSelectedTreeNode();
        if(selContr != null) {
            // This is really important!
            // This HAS to be the first thing to do when the panel is selected because this information
            // is used afterwards basically for everything. Not doing so will break this panel consistency.
            // This contract may look fragile but it is an easy way to book keep the selected node in an
            // easy way and without taking care of locking and thread safety: the 'cLabel' is always accessed
            // in the EDT and thread confinement is used.
            this.cLabel.setText(selContr.getUserObject().getName());
        } else {
            // IMPORTANT: set the label to an empty string if nothing is selected in the RC tree
            // Look at 'buildSegmentTree(selContr)'
            this.cLabel.setText("");
        }

        // Perform the IS subscription using the proper executor
        this.isSubscriberExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    RunControlTestsPanel.this.subscribeIS();
                }
                catch(final ISException ex) {
                    IguiLogger.error(ex.getMessage(), ex);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR,
                                                    "Failed to subscribe to \"" + IguiConstants.SETUP_IS_SERVER_NAME + "\" IS server: "
                                                        + ex + ". Test results will not be updated correctly");
                    ErrorFrame.showError("TestResults Panel Error",
                                         ex.getMessage() + ".\nThe \"TestResults\" panel will not be updated!",
                                         ex);
                }

                // Build the tree in the proper executor: this executor ping-pong is a bit tricky but effective:
                // do not keep busy for too long the IS subscriber thread but be sure that the tree is built
                // only AFTER the IS subscription has been performed
                RunControlTestsPanel.this.isCallbackExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            RunControlTestsPanel.this.buildTestTree(selContr);
                        }
                        catch(final InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                    }
                });
            }
        });
    }

    /**
     * It removes the IS subscription and shuts down the executors.
     * 
     * @see Igui.IguiInterface#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        try {
            // Subscription removed in the proper executor
            this.isSubscriberExecutor.submit(new Callable<Void>() {
                @Override
                public Void call() throws ISException {
                    RunControlTestsPanel.this.unsubscribeIS();
                    return null;
                }
            }).get(RunControlTestsPanel.TIMEOUT, TimeUnit.SECONDS);
        }
        catch(final InterruptedException ex) {
            IguiLogger.warning("Thread interrupted");
            Thread.currentThread().interrupt();
        }
        catch(final ExecutionException ex) {
            IguiLogger.warning("Unsubscription from \"" + this.isReceiver.getName() + "\" IS server failed when terminating the panel", ex);
        }
        catch(final TimeoutException ex) {
            IguiLogger.warning("Timeout elapsed unsubscribing from \"" + this.isReceiver.getName() + "\" IS server", ex);
        }

        this.isSubscriberExecutor.shutdown();
        this.isCallbackExecutor.shutdown();
        this.cmdSender.shutdown();

        IguiLogger.info("Panel \"" + RunControlTestsPanel.panelName + "\" has been terminated");
    }

    /**
     * It removes and performs the IS subscription. After the subscription the tree is re-built.
     * 
     * @see Igui.AbstractRunControlPanel#refreshISSubscription()
     */
    @Override
    public void refreshISSubscription() {
        // Subscribe and un-subscribe using the right subscriber
        final Future<?> task = this.isSubscriberExecutor.submit(new Callable<Void>() {
            @Override
            public Void call() throws IguiException.IguiPanelError, InterruptedException {
                try {
                    RunControlTestsPanel.this.unsubscribeIS();
                    RunControlTestsPanel.this.subscribeIS();

                    // After subscription re-built the tree, do it in the call-back executor
                    final RCTreeNode selContr = RunControlTestsPanel.this.getMainPanel().getSelectedTreeNode();
                    if(selContr != null) {
                        try {
                            final Future<?> task2 = RunControlTestsPanel.this.isCallbackExecutor.submit(new Callable<Void>() {
                                @Override
                                public Void call() throws InterruptedException {
                                    RunControlTestsPanel.this.buildTestTree(selContr);
                                    return null;
                                }
                            });

                            // Wait for the tree to be rebuilt
                            task2.get();
                        }
                        catch(final ExecutionException ex) {
                            throw new IguiException.IguiPanelError(RunControlTestsPanel.this.getPanelName(),
                                                                   "Failed building the segment tree for segment \""
                                                                       + selContr.getUserObject().getSegmentName() + "\": "
                                                                       + ex.getCause().getMessage(),
                                                                   ex.getCause());
                        }
                    }
                }
                catch(final ISException ex) {
                    throw new IguiException.IguiPanelError(RunControlTestsPanel.this.getPanelName(), ex.getMessage(), ex);
                }

                return null;
            }
        });

        // Wait for the task to be executed
        try {
            task.get();
        }
        catch(final InterruptedException ex) {
            IguiLogger.warning("Thread interrupted!", ex);
            Thread.currentThread().interrupt();
        }
        catch(final ExecutionException ex) {
            final String errMsg = "Error refreshing IS subscriptions: " + ex.getCause().getMessage();
            IguiLogger.error(errMsg, ex);
            ErrorFrame.showError("TestResults Panel Error",
                                 errMsg + ".\nTo fix the problem try to deselect/select the panel or\n to refresh IS subscription again.",
                                 ex);
        }
    }

    /**
     * It performs IS subscription.
     * <p>
     * Remember to perform it in the {@link #isCallbackExecutor} executor.
     * 
     * @throws IguiException.ISException The IS subscription failed
     */
    private void subscribeIS() throws IguiException.ISException {
        try {
            if(!this.isReceiver.isSubscribed()) {
                this.isReceiver.subscribe();
                IguiLogger.debug("Subscribed to the \"" + this.isReceiver.getName() + "\" IS server");
            }
        }
        catch(final Exception ex) {
            final String errMsg = "Subscription to the \"" + this.isReceiver.getName() + "\" IS server failed: " + ex;
            throw new IguiException.ISException(errMsg, ex);
        }
    }

    /**
     * It removes the IS subscription.
     * <p>
     * Remember to perform it in the {@link #isCallbackExecutor} executor.
     * 
     * @throws IguiException.ISException Some error occurred removing the IS subscription
     */
    private void unsubscribeIS() throws IguiException.ISException {
        try {
            this.isReceiver.unsubscribe();
            IguiLogger.debug("Unsubscribed from the \"" + this.isReceiver.getName() + "\" IS server");
        }
        catch(final Exception ex) {
            throw new IguiException.ISException("Unsubscription from the \"" + this.isReceiver.getName() + " IS server failed: " + ex, ex);
        }
    }

    /**
     * Method processing the call-backs received from the IS server.
     * 
     * @param infoEvent The IS call-back event
     * @param cbt The call-back type
     */
    private void processSetupISCallback(final is.InfoEvent infoEvent, final CallbackType cbt) {
        this.isCallbackExecutor.execute(new Runnable() {
            @Override
            public void run() {
                // Lock the read lock: it allows concurrent call-backs execution but it avoids to mix
                // up with the tree building (there the write lock is locked)
                RunControlTestsPanel.this.rwl.readLock().lock();
                try {
                    final ApplicationTestInfo info = new ApplicationTestInfo();
                    infoEvent.getValue(info);

                    // Segment name and info type the IS information refers to (info name is like
                    // <server>.<segment>.<application>)
                    final String fullInfoName = infoEvent.getName();
                    final String[] vars = fullInfoName.split("\\.", 3);
                    if(vars.length == 3) {
                        final String segmentName = vars[1];
                        final String applicationName = vars[2];

                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                // Get the currently selected segment in the RC tree
                                String currentSelectedApp = null;
                                boolean isController = false;
                                String currentSegment = null;
                                if(RunControlTestsPanel.this.isInitialInfrastructureTests == false) {
                                    // Running a RunControl sub-panel
                                    final RCTreeNode rcNode = RunControlTestsPanel.this.getMainPanel().getSelectedTreeNode();
                                    if(rcNode != null) {
                                        final RunControlApplicationData data = rcNode.getUserObject();
                                        currentSelectedApp = data.getName();
                                        isController = data.isController();
                                        currentSegment = data.getSegmentName();
                                    }
                                } else {
                                    // When used in the initial infrastructure check, the "selected" controller is the RootController
                                    final RunControlApplicationData data = Igui.instance().getRCInfo();
                                    currentSelectedApp = data.getName();
                                    isController = true;
                                    currentSegment = data.getSegmentName();
                                }

                                if((currentSelectedApp != null) && (currentSegment != null)) {
                                    final boolean process = ((isController == true) && (currentSegment.equals(segmentName) == true))
                                                            || ((isController == false) && (currentSelectedApp.equals(applicationName) == true));
                                    if(process == true) {
                                        switch(cbt) {
                                            case CREATED:
                                            case UPDATED:
                                                RunControlTestsPanel.this.testTreeModel.updateApplication(info);
                                                break;
                                            case DELETED:
                                                RunControlTestsPanel.this.testTreeModel.removeApplication(applicationName);
                                                break;
                                            default:
                                        }
                                    }
                                } else {
                                    RunControlTestsPanel.this.testTreeModel.cleanAll();
                                }
                            }
                        });
                    } else {
                        final String msg = "Cannot decode the IS call-back for information \"" + fullInfoName
                                           + "\"; the test panel may not show updated information";
                        IguiLogger.error(msg);
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    }
                }
                catch(final Exception ex) {
                    final String msg = "Failed getting info from IS: " + ex.toString() + ". Test results may not be correctly updated.";
                    IguiLogger.error(msg, ex);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                }
                finally {
                    RunControlTestsPanel.this.rwl.readLock().unlock();
                }
            }
        });
    }

    /**
     * It builds the "test" tree for the <code>rcItem</code> selected RC tree node.
     * <p>
     * This method should always be executed in the {@link #isCallbackExecutor} executor.
     * 
     * @param rcItem The selected node in the RC tree (<code>null</code> value may be used in case of empty selection in the RC tree)
     * @see #buildTestTree(String, String, boolean)
     * @throws InterruptedException The thread has been interrupted
     */
    private void buildTestTree(final RCTreeNode rcItem) throws InterruptedException {
        if(rcItem != null) {
            final RunControlApplicationData data = rcItem.getUserObject();
            this.buildTestTree(data.getName(), data.getSegmentName(), data.isController());
        } else {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    RunControlTestsPanel.this.testTreeModel.cleanAll();
                }
            });
        }
    }

    /**
     * It builds the test tree for the selected application.
     * <p>
     * This method should always be executed in the {@link #isCallbackExecutor} executor.
     * 
     * @param nodeName The name of the selected application in the RC tree
     * @param segmentName The name of the application's segment
     * @param isController <code>true</code> if the selected node in the RC tree is a segment's controller
     * @throws InterruptedException The thread has been interrupted
     */
    private void buildTestTree(final String nodeName, final String segmentName, final boolean isController) throws InterruptedException {
        try {
            // Acquire the write lock: the read lock is acquired while processing IS call-backs.
            // When we build the tree we do not want any call-back to be processed
            this.rwl.writeLock().lock();

            // Here is the list of all the applications (from IS)
            final List<ApplicationTestInfo> apps = new ArrayList<>();

            if(isController == true) {
                // If the selected node is a controller, then get all the applications for that segment

                // Regex to be used for information retrieval from IS
                final StringBuilder regexp = new StringBuilder();
                regexp.append(segmentName);
                regexp.append("\\.");
                regexp.append(".*");

                // Retrieve the information from IS
                final is.InfoList appList = new is.InfoList(Igui.instance().getPartition(),
                                                            IguiConstants.SETUP_IS_SERVER_NAME,
                                                            new is.Criteria(Pattern.compile(regexp.toString()),
                                                                            ApplicationTestInfo.type,
                                                                            is.Criteria.Logic.AND));

                for(int j = 0; j < appList.size(); j++) {
                    final ApplicationTestInfo info = new ApplicationTestInfo();
                    appList.getInfo(j, info);

                    apps.add(info);
                }
            } else {
                // The selected node is not a controller, just get the information about the selected application
                try {
                    final ApplicationTestInfo appInfo = ISInfoReceiver.getInfo(IguiConstants.SETUP_IS_SERVER_NAME + "." + segmentName + "."
                                                                               + nodeName, ApplicationTestInfo.class);
                    apps.add(appInfo);
                }
                catch(final ISException ex) {
                    final Throwable cause = ex.getCause();
                    if((cause == null) || (is.RepositoryNotFoundException.class.isInstance(cause))) {
                        throw ex;
                    }
                }
            }

            try {
                // After getting info from IS build the tree
                // Here we call 'invokeAndWait' because this method should exit only when the tree is really built
                // In this way the tree building process is not interleaved in any way by call-backs execution
                javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        RunControlTestsPanel.this.testTreeModel.cleanAll();

                        if(nodeName.equals(RunControlTestsPanel.this.cLabel.getText())) {
                            RunControlTestsPanel.this.testTreeModel.updateApplications(apps);
                        }
                    }
                });
            }
            catch(final InvocationTargetException ex) {
                IguiLogger.error("Unexpected exception while building the test-status tree: " + ex.toString(), ex);
            }
        }
        catch(final InterruptedException ex) {
            IguiLogger.warning("The task building the test-status tree has been interrupted", ex);
            throw ex;
        }
        catch(final Exception ex) {
            final String msg = "Failed getting info from IS: " + ex.getMessage()
                               + ". The results of tests shown in the \"TestResults\" panel may not be complete.";
            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
            IguiLogger.error(msg, ex);
        }
        finally {
            // Unlock the lock
            this.rwl.writeLock().unlock();
        }
    }

    /**
     * It returns an array containing the selected nodes in the tree.
     * <p>
     * To be executed in the EDT.
     * 
     * @return An array containing the selected nodes in the tree
     */
    private TestTreeNode[] getSelectedNode() {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation!";

        final int selNum = this.testTree.getSelectionCount();
        final TestTreeNode[] na = new TestTreeNode[selNum];
        if(selNum > 0) {
            final TreePath[] tpa = this.testTree.getSelectionPaths();
            for(int i = 0; i < selNum; ++i) {
                na[i] = (TestTreeNode) tpa[i].getLastPathComponent();
            }
        }

        return na;
    }

    /**
     * It builds and initialized all the GUI components.
     */
    private void initGUI() {
        this.setLayout(new BorderLayout());

        // Add action listeners to the pop-up menu items
        this.popTestComp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                // Collect here all the applications to be tested
                final Set<String> as = new TreeSet<>();

                for(final TestTreeNode n : RunControlTestsPanel.this.getSelectedNode()) {
                    final TreeNode parent = n.getParent();
                    if(parent != null) {
                        final TestTreeNode p = (TestTreeNode) parent;
                        if(p.isRoot() == true) {
                            // The parent is root: an application node is selected
                            as.add(n.toString());
                        } else {
                            // The parent is not root: a component node is selected (the application is the parent)
                            as.add(p.toString());
                        }
                    }
                }

                if(as.isEmpty() == false) {
                    String recipient = null;

                    // Look for the controller commands should be sent to
                    if(RunControlTestsPanel.this.isInitialInfrastructureTests == false) {
                        // Find the selected node in the RC tree
                        final RunControlMainPanel mp = RunControlTestsPanel.this.getMainPanel();
                        final RCTreeNode selNode = mp.getSelectedTreeNode();
                        if(selNode != null) {
                            final RunControlApplicationData data = selNode.getUserObject();
                            if(data.isController() == true) {
                                // The node is a controller, fine
                                recipient = data.getName();
                            } else {
                                // The node is not a controller, the command should be sent to its parent
                                recipient = selNode.getParent().toString();
                            }
                        }
                    } else {
                        // When running at the very beginning commands are always sent to the Root Controller
                        recipient = Igui.instance().getRootControllerName();
                    }

                    if(recipient != null) {
                        if(RunControlTestsPanel.this.isInitialInfrastructureTests == false) {
                            // Group the applications per test's scope
                            final Map<Scope, List<String>> scopes = new EnumMap<>(Scope.class);

                            final RunControlMainPanel mp = RunControlTestsPanel.this.getMainPanel();
                            for(final String appName : as) {
                                final Scope sc;
                                final RCTreeNode n = mp.getTreeNode(appName);
                                if(n != null) {
                                    if((n.isInfoLoaded() == false)
                                       || (n.getUserObject().getISStatus().getStatus().equals(ApplicationStatus.UP)))
                                    {
                                        // The information about the application is not available yet or the application is UP
                                        sc = TM.Test.Scope.Any;
                                    } else {
                                        sc = TM.Test.Scope.Precondition;
                                    }
                                } else {
                                    sc = TM.Test.Scope.Any;
                                }

                                final List<String> l = scopes.get(sc);
                                if(l != null) {
                                    l.add(appName);
                                } else {
                                    final List<String> ll = new ArrayList<>();
                                    ll.add(appName);
                                    scopes.put(sc, ll);
                                }
                            }

                            for(final Entry<Scope, List<String>> me : scopes.entrySet()) {
                                try {
                                    RunControlTestsPanel.this.bActions.sendCommand(recipient,
                                                                                   new Command.TestAppCmd(me.getValue().toArray(new String[] {}),
                                                                                                          me.getKey(),
                                                                                                          1));
                                }
                                catch(final BadCommandException ex) {
                                    IguiLogger.error(ex.getMessage(), ex);
                                    ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                                }
                            }
                        } else {
                            try {
                                RunControlTestsPanel.this.bActions.sendCommand(recipient,
                                                                               new Command.TestAppCmd(as.toArray(new String[as.size()])));
                            }
                            catch(final BadCommandException ex) {
                                IguiLogger.error(ex.getMessage(), ex);
                                ErrorFrame.showError("IGUI - Command Error", ex.getMessage(), ex);
                            }
                        }
                    } else {
                        final String msg = "The command to execute tests cannot be sent because no application is selected in the RC tree";
                        IguiLogger.error(msg);
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    }
                }
            }
        });

        this.popLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final TestTreeNode[] selNode = RunControlTestsPanel.this.getSelectedNode();
                if(selNode.length > 0) {
                    final TestInfo ti = selNode[0].getUserObject();
                    RunControlTestsPanel.this.bActions.showLog(ti.testLog, ti.objectId);
                }
            }
        });

        this.popupIL.addSeparator();
        this.popupIL.add(this.popTestComp);
        this.popTestComp.setIcon(Igui.createIcon(RunControlTestsPanel.testIconFile));
        this.popupIL.addSeparator();
        this.popupIL.add(this.popLog);
        this.popLog.setIcon(Igui.createIcon(RunControlTestsPanel.logIconFile));
        this.popupIL.addSeparator();

        this.cLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.cLabel.setBackground(Color.white);
        this.cLabel.setOpaque(true);
        this.cLabel.setMinimumSize(new Dimension(0, 20));

        final JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.add(this.cLabel, BorderLayout.CENTER);
        topPanel.setBackground(Color.cyan);
        topPanel.setOpaque(true);
        topPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 50, 5, 50),
                                                              BorderFactory.createLoweredBevelBorder()));

        this.testTree.setModel(this.testTreeModel);
        this.testTree.setCellRenderer(new TestTreeRenderer());
        this.testTree.setRootVisible(false);
        this.testTree.setLargeModel(false);
        this.testTree.setShowsRootHandles(true);
        this.testTree.setOpaque(true);
        this.testTree.setScrollsOnExpand(true);
        this.testTree.addMouseListener(new MouseListener());
        this.testTree.setSelectionModel(new TestTreeSelectionModel());

        final JScrollPane treeScrollPane = new JScrollPane(this.testTree);
        treeScrollPane.setBorder(new javax.swing.border.CompoundBorder(new javax.swing.border.CompoundBorder(new javax.swing.border.LineBorder(Color.gray),
                                                                                                             new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.RAISED,
                                                                                                                                                 Color.white,
                                                                                                                                                 Color.lightGray)),
                                                                       new javax.swing.border.LineBorder(Color.darkGray)));

        if(this.isInitialInfrastructureTests == false) {
            this.add(topPanel, BorderLayout.NORTH);
            this.setBackground(Color.WHITE);
        } else {
            // When the panel is used at the very beginning to check the partition infrastructure
            // the label is not shown but it is important to set its text equal to the Root Controller
            // name to keep the panel schema consistency
            this.cLabel.setText(Igui.instance().getRootControllerName());
            this.testTree.setBackground(new Color(249, 219, 97));
        }
        this.add(treeScrollPane, BorderLayout.CENTER);
        this.setOpaque(true);
    }

    /**
     * Method called every time a node is selected in the RC tree.
     * 
     * @see Igui.AbstractRunControlPanel#treeNodeSelected(RCTreeNode)
     */
    @Override
    public void treeNodeSelected(final RCTreeNode selectedTreeItem) {
        // Do something only if the panel is currently selected
        if(this.equals(this.getMainPanel().getSelectedSubPanel())) {
            final String selectedControllerName;
            if(selectedTreeItem != null) {
                selectedControllerName = selectedTreeItem.getUserObject().getName();
            } else {
                selectedControllerName = "";
            }

            // This is the first thing to do! VERY important! Give a look at the comment in 'panelSelected'!
            this.cLabel.setText(selectedControllerName);

            // Build the panel in the right executor
            this.isCallbackExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        RunControlTestsPanel.this.buildTestTree(selectedTreeItem);
                    }
                    catch(final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
            });
        }
    }
}
