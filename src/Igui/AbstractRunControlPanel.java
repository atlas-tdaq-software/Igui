package Igui;

import java.lang.ref.WeakReference;

import javax.swing.JPanel;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import Igui.RunControlTreePanel.RCTreeNode;


/**
 * Abstract class for RunControl panel sub-panels.
 * <p>
 * A panel of this class is not RC state aware and this abstract class gives dummy implementation of some methods of the
 * {@link IguiInterface} interface.
 * <p>
 * An instance of {@link RunControlMainPanel} will be the "parent" of all the class implementing {@link AbstractRunControlPanel}, will keep
 * a reference of all its children and will take care of calling {@link IguiInterface} methods on them when needed.
 * <p>
 * This class creates listeners to be notified about a change in the Run Control tree selection or changes of Run Control tree nodes. To be
 * notified about a tree selection change the implementing class should subscribe using the {@link #subscribeNodeSelection()} method and
 * then override {@link #treeNodeSelected(RCTreeNode)}. To be notified about changes in the status of tree nodes the implementing class
 * should subscribe using the {@link #subscribeNodeChanges()} method and then override {@link #treeNodesChanged(RCTreeNode[])}.
 * <p>
 * Please, look at the {@link RunControlMainPanel} documentation to have a full picture.
 */
abstract class AbstractRunControlPanel extends JPanel implements IguiInterface {

    private static final long serialVersionUID = -5140630403852436262L;

    /**
     * The tree selection listener (to be notified about changes in the node selection).
     */
    private final TreeSelectionListener rcTreeSelectionListener = new RCTreeSelectionListener();

    /**
     * The tree model listener (to be notified about changes in the tree nodes).
     */
    private final TreeModelListener rcTreeModelListener = new RCTreeModelListener();

    /**
     * Weak reference to the {@link RunControlMainPanel}. It keeps a reference of all the run control sub-panels.
     */
    private final WeakReference<RunControlMainPanel> mainRCPanel;

    /**
     * Class implementing the {@link TreeSelectionListener} interface.
     * <p>
     * When the node selection is changed the {@link AbstractRunControlPanel#treeNodeSelected(RCTreeNode)} method is called.
     */
    private final class RCTreeSelectionListener implements TreeSelectionListener {
        RCTreeSelectionListener() {
        }

        @Override
        public void valueChanged(final TreeSelectionEvent e) {
            // Get the new selected node from the TreeSelectionEvent event
            RCTreeNode newSelNode = null;

            final TreePath tp = e.getNewLeadSelectionPath();
            if(tp != null) {
                final Object obj = tp.getLastPathComponent();
                if((obj != null) && (RCTreeNode.class.isInstance(obj) == true)) {
                    newSelNode = (RCTreeNode) obj;
                }
            }

            AbstractRunControlPanel.this.treeNodeSelected(newSelNode);
        }
    }

    /**
     * Class implementing the {@link TreeModelListener} interface.
     * <p>
     * It has dummy implementation for all the methods but {@link TreeModelListener#treeNodesChanged(TreeModelEvent)}. When the status of
     * some node changes the {@link AbstractRunControlPanel#treeNodesChanged(RCTreeNode[])} method is called.
     */
    private final class RCTreeModelListener implements TreeModelListener {
        RCTreeModelListener() {
        }

        @Override
        public void treeNodesChanged(final TreeModelEvent e) {
            try {
                final RCTreeNode[] changedNodes;
                final Object[] nodes = e.getChildren();
                if(nodes != null) {
                    changedNodes = new RCTreeNode[nodes.length];
                    for(int i = 0; i < nodes.length; ++i) {
                        changedNodes[i] = (RCTreeNode) nodes[i];
                    }
                } else {
                    // If we are here it means that the Root node changed
                    changedNodes = new RCTreeNode[] {(RCTreeNode) e.getPath()[0]};
                }

                AbstractRunControlPanel.this.treeNodesChanged(changedNodes);
            }
            catch(final ArrayStoreException ex) {
                IguiLogger.error("Error processing a \"TreeModelListener\" event: " + ex.getMessage(), ex);
            }
        }

        @Override
        public void treeNodesInserted(final TreeModelEvent e) {
        }

        @Override
        public void treeNodesRemoved(final TreeModelEvent e) {
        }

        @Override
        public void treeStructureChanged(final TreeModelEvent e) {
        }
    }

    /**
     * Constructor.
     * 
     * @param parent A reference to the {@link RunControlMainPanel}. Implementing classes should not keep a strong reference to
     *            <code>parent</code> but get a reference to it (when needed) using the {@link AbstractRunControlPanel#getMainPanel()}
     *            method.
     */
    AbstractRunControlPanel(final RunControlMainPanel parent) {
        super();
        this.mainRCPanel = new WeakReference<RunControlMainPanel>(parent);
    }

    /**
     * It returns <code>false</code>.
     * 
     * @see IguiPanel#rcStateAware().
     */
    @Override
    public final boolean rcStateAware() {
        return false;
    }

    /**
     * It returns the panel name as defined by {@link #getPanelName()}.
     * 
     * @see IguiPanel#getTabName().
     */
    @Override
    public String getTabName() {
        return this.getPanelName();
    }

    /**
     * Default dummy implementation of {@link IguiPanel#dbDiscarded()}.
     * <p>
     * To be overridden by implementing class if needed.
     * 
     * @see RunControlMainPanel#dbDiscarded().
     */
    @Override
    public void dbDiscarded() {
    }

    /**
     * Default dummy implementation of {@link IguiPanel#refreshISSubscription()}.
     * <p>
     * To be overridden by implementing class if needed.
     * 
     * @see IguiPanel#refreshISSubscription().
     */
    @Override
    public void refreshISSubscription() {
    }

    /**
     * It returns an empty string.
     * 
     * @see IguiPanel#elogString().
     */
    @Override
    public final String elogString() {
        return "";
    }

    /**
     * It returns a reference to the {@link RunControlMainPanel} panel.
     * 
     * @return <code>null</code> if the {@link RunControlMainPanel} has already been garbage collected.
     */
    public final RunControlMainPanel getMainPanel() {
        return this.mainRCPanel.get();
    }

    /**
     * Method called when the Run Control tree selection is changed.
     * <p>
     * This method is called only if the panel has been subscribed to tree selection changes using the {@link #subscribeNodeSelection()}
     * method.
     * <p>
     * The default implementation does nothing, implementing classes should override this method properly.
     * 
     * @see RunControlMainPanel#addTreeSelectionSubscriber(AbstractRunControlPanel)
     * @see RCTreeSelectionListener
     * @param node The new selected node (<code>null</code> if the tree selection is empty)
     */
    protected void treeNodeSelected(final RCTreeNode node) {
    }

    /**
     * Method called when the status of some Run Control tree node changes.
     * <p>
     * This method is called only if the panel has been subscribed to tree node changes using the {@link #subscribeNodeChanges()} method.
     * <p>
     * The default implementation does nothing, implementing classes should override this method properly.
     * 
     * @see RunControlMainPanel#addTreeChangeSubscriber(AbstractRunControlPanel)
     * @see RCTreeModelListener
     * @param nodes The changed nodes.
     */

    protected void treeNodesChanged(final RCTreeNode[] nodes) {
    }

    /**
     * Call this method to receive notifications any time some node in the Run Control tree changes its status.
     * 
     * @see AbstractRunControlPanel#treeNodesChanged(RunControlApplicationData[]).
     */
    protected final void subscribeNodeChanges() {
        final RunControlMainPanel mainP = this.mainRCPanel.get();
        if(mainP != null) {
            mainP.addTreeChangeSubscriber(this);
        }
    }

    /**
     * Call this method to receive notifications any time a new node is selected in the Run Control tree.
     * 
     * @see AbstractRunControlPanel#treeNodeSelected(RCTreeNode)
     */
    protected final void subscribeNodeSelection() {
        final RunControlMainPanel mainP = this.mainRCPanel.get();
        if(mainP != null) {
            mainP.addTreeSelectionSubscriber(this);
        }
    }

    /**
     * It returns a reference to the {@link TreeSelectionListener} listener.
     * 
     * @return A reference to the {@link TreeSelectionListener} listener.
     */
    protected final TreeSelectionListener getTreeSelectionListener() {
        return this.rcTreeSelectionListener;
    }

    /**
     * It returns a reference to the {@link TreeModelListener} listener.
     * 
     * @return A reference to the {@link TreeModelListener} listener.
     */
    protected final TreeModelListener getTreeModelListener() {
        return this.rcTreeModelListener;
    }
}
