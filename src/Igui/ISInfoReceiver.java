package Igui;

import is.InfoEvent;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import ipc.Partition;


/**
 * Utility abstract class used as a wrapper around frequently used IS calls.
 * <p>
 * Use it to make IS subscription (the class has an internal {@link is.InfoWatcher} - the user only needs to override
 * {@link #infoSubscribed(InfoEvent)}, {@link #infoCreated(InfoEvent)}, {@link #infoDeleted(InfoEvent)} or {@link #infoUpdated(InfoEvent)}
 * as needed and call {@link #subscribe()} to actually make the IS subscription) or directly get information from IS
 * ({@link #getInfo(String, Class)}).
 */
abstract class ISInfoReceiver {

    /**
     * Boolean keeping trace whether the IS subscription is active or not
     */
    private final AtomicBoolean subscribed = new AtomicBoolean(false);

    /**
     * Full name of the IS information
     */
    private final String infoName;

    /**
     * IS server name
     */
    private final String serverName;

    /**
     * IS subscription criteria
     */
    private final is.Criteria subCriteria;

    /**
     * The IPC partition
     */
    private final Partition partition;

    /**
     * The IS repository
     */
    private final is.Repository isRepo;

    /**
     * Reference to the {@link is.InfoWatcher}
     */
    // Here the atomic reference is needed to implement a safe constructor idiom and for thread safeness.
    // The info listener is created the first time it is needed (i.e., at the moment of the subscription)
    // and it uses some of this class methods as call-back functions
    private final AtomicReference<is.InfoWatcher> infoWatcher = new AtomicReference<is.InfoWatcher>();

    /**
     * Constructor.
     * 
     * @param infoName Full name of the IS information
     */
    ISInfoReceiver(final String infoName) {
        this(Igui.instance().getPartition(), infoName);
    }

    /**
     * Constructor.
     * 
     * @param partition Name of the partition
     * @param infoName Full name of the IS information
     */
    ISInfoReceiver(final Partition partition, final String infoName) {
        this.infoName = infoName;
        this.serverName = null;
        this.subCriteria = null;
        this.partition = partition;
        this.isRepo = new is.Repository(partition);
    }

    /**
     * Constructor.
     * 
     * @param serverName The name of the IS server
     * @param subCriteria The subscription criteria
     */
    ISInfoReceiver(final String serverName, final is.Criteria subCriteria) {
        this(Igui.instance().getPartition(), serverName, subCriteria);
    }

    /**
     * Constructor.
     * 
     * @param partition The name of the partition
     * @param serverName The name of the IS server
     * @param subCriteria The subscription criteria
     */
    ISInfoReceiver(final Partition partition, final String serverName, final is.Criteria subCriteria) {
        this.infoName = null;
        this.serverName = serverName;
        this.subCriteria = subCriteria;
        this.partition = partition;
        this.isRepo = new is.Repository(partition);
    }

    /**
     * It returns the name of the IS information or of the IS server (depending on the used constructor).
     * 
     * @return The name of the IS information or of the IS server
     */
    public String getName() {
        String name;

        if(this.infoName != null) {
            name = this.infoName;
        } else {
            name = this.serverName;
        }

        return name;
    }

    /**
     * It returns <code>true</code> if the IS subscription has been done.
     * 
     * @return <code>true</code> if the IS subscription has been done
     */
    public boolean isSubscribed() {
        return this.subscribed.get();
    }

    /**
     * It returns the used IS repository.
     * 
     * @return The used IS repository
     */
    public is.Repository getRepository() {
        return this.isRepo;
    }

    /**
     * It subscribes to the specified IS information or IS server with the selected criteria.
     * <p>
     * To process call-back events the subclass should override one (or all of them, as needed) of the following methods:
     * {@link #infoCreated(InfoEvent)}, {@link #infoDeleted(InfoEvent)}, {@link #infoUpdated(InfoEvent)}.
     * 
     * @throws IguiException.ISException The subscription failed
     */
    public void subscribe() throws IguiException.ISException {
        try {
            if(this.subscribed.get() == false) { // Do not subscribe multiple times!
                this.getInfoWatcher().subscribe();
                this.subscribed.set(true);
            } else {
                IguiLogger.warning("Will not subscribe to \"" + this.getName() + "\": subscription already exists");
            }
        }
        catch(final Exception ex) {
            this.subscribed.set(false);
            throw new IguiException.ISException("Failed to subscribe to \"" + this.getName() + "\": " + ex, ex);
        }
    }

    /**
     * It un-subscribes from the IS server.
     * <p>
     * No error is reported if the un-subscription fails because the subscription is not found.
     * 
     * @throws IguiException.ISException The un-subcription failed
     */
    public void unsubscribe() throws IguiException.ISException {
        try {
            this.getInfoWatcher().unsubscribe();
        }
        catch(final is.SubscriptionNotFoundException ex) {
            IguiLogger.debug("Failed to unsubscribe from \"" + this.getName() + "\": " + ex, ex);
        }
        catch(final Exception ex) {
            throw new IguiException.ISException("Failed to unsubscribe from \"" + this.getName() + "\": " + ex, ex);
        }
        finally {
            this.subscribed.set(false);
        }
    }

    /**
     * It retrieves information from the IS server
     * 
     * @param informationName The IS information full name
     * @param infoType The IS information type
     * @return The requested IS information
     * @throws IguiException.ISException Some error occurred getting the requested information from the IS server
     */
    public static <T extends is.Info> T getInfo(final String informationName, final Class<T> infoType) throws IguiException.ISException {
        try {
            // Create the info instance and get its value from the repository
            final T info = infoType.getDeclaredConstructor().newInstance();
            final is.Repository rep = new is.Repository(Igui.instance().getPartition());
            rep.getValue(informationName, info);
            return info;
        }
        catch(final Exception ex) {
            final String errMsg = "Failed getting value for IS information \"" + informationName + "\": " + ex;
            throw new IguiException.ISException(errMsg, ex);
        }
    }

    /**
     * Override to be notified every time the information is created.
     * 
     * @param info The created information
     */
    protected void infoCreated(final InfoEvent info) {
    }

    /**
     * Override to be notified every time the information is removed.
     * 
     * @param info The removed information
     */
    protected void infoDeleted(final InfoEvent info) {
    }

    /**
     * Override to be notified every time the information is updated.
     * 
     * @param info The updated information
     */
    protected void infoUpdated(final InfoEvent info) {
    }

    /**
     * Override to be notified every time a subscription is done.
     * 
     * @param info The value of the information at the subscription time
     */
    protected void infoSubscribed(final InfoEvent info) {
    }

    /**
     * It creates the {@link is.InfoWatcher} used to get call-backs from the IS server.
     * <p>
     * The {@link is.InfoWatcher} will use the {@link #infoSubscribed(InfoEvent)}, {@link #infoCreated(InfoEvent)},
     * {@link #infoDeleted(InfoEvent)} and {@link #infoUpdated(InfoEvent)} methods when call-backs are received.
     * 
     * @return The {@link is.InfoWatcher}
     */
    private is.InfoWatcher getInfoWatcher() {
        if(this.infoWatcher.get() == null) {
            final is.InfoWatcher isWatchToReturn;

            if(this.infoName != null) {
                isWatchToReturn = new is.InfoWatcher(this.partition, this.infoName) {
                    @Override
                    public void infoSubscribed(final InfoEvent info) {
                        ISInfoReceiver.this.infoSubscribed(info);
                    }

                    @Override
                    public void infoCreated(final InfoEvent info) {
                        ISInfoReceiver.this.infoCreated(info);
                    }

                    @Override
                    public void infoDeleted(final InfoEvent info) {
                        ISInfoReceiver.this.infoDeleted(info);
                    }

                    @Override
                    public void infoUpdated(final InfoEvent info) {
                        ISInfoReceiver.this.infoUpdated(info);
                    }
                };
            } else {
                isWatchToReturn = new is.InfoWatcher(this.partition, this.serverName, this.subCriteria) {
                    @Override
                    public void infoSubscribed(final InfoEvent info) {
                        ISInfoReceiver.this.infoSubscribed(info);
                    }

                    @Override
                    public void infoCreated(final InfoEvent info) {
                        ISInfoReceiver.this.infoCreated(info);
                    }

                    @Override
                    public void infoDeleted(final InfoEvent info) {
                        ISInfoReceiver.this.infoDeleted(info);
                    }

                    @Override
                    public void infoUpdated(final InfoEvent info) {
                        ISInfoReceiver.this.infoUpdated(info);
                    }
                };
            }

            // Update the atomic reference
            this.infoWatcher.compareAndSet(null, isWatchToReturn);
        }

        return this.infoWatcher.get();
    }
}
