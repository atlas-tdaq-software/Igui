package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;


// -----------------------------------------------------------
/**
 * The ReloadDialog class is used for managing the reloading of the database. It will display different dialogs, depending on whether files
 * have been removed or updated. A reload will only be possible if there are externally updated files, otherwise a message will be displayed
 * explaining the reason why reloading cannot be done.
 * 
 * @author Anja Eline Bekkelien
 * @author Giuseppe Avolio
 */
// -----------------------------------------------------------
class ReloadDialog extends JDialog {

    private static final long serialVersionUID = 1871467855319408317L;

    /** Title of dialog */
    private final static String dialogTitle = "Reload database";

    /** Text for Reload all button */
    private final JButton ButtonReloadAll = new JButton("Reload all");

    /** Text for Reload button */
    private final JButton ButtonReload = new JButton("Reload");

    /** Text for Cancel button */
    private final JButton ButtonCancel = new JButton("Cancel");

    /** Text for OK button */
    private final JButton ButtonOK = new JButton("OK");

    /** Minimum width of dialog */
    private final static int MIN_WIDTH = 350;

    /** Maximum height of dialog */
    private final static int MAX_HEIGHT = 200;

    /** Minimum height of dialog */
    private final static int MIN_HEIGHT = 100;

    /** Check-boxes for modified files */
    private final List<JCheckBox> checkBoxes = new ArrayList<JCheckBox>();

    /** Main panel of dialog */
    private final JPanel mainPanel = new JPanel(true);

    /** Dialog Panel */
    private JOptionPane optionPane;

    /** List of file names */
    private final List<String> updatedDatafiles;

    /** List of files selected bu the user to be reloaded */
    private final List<String> filesToRelod = new ArrayList<String>();

    /** Instance of the class keeping the button actions */
    private final ButtonActions bActions = new ButtonActions();

    /** React on buttons pressed */
    private final class ButtonActions {

        ButtonActions() {
        }

        void cancel() {
            ReloadDialog.this.dispose();
        }

        void ok() {
            ReloadDialog.this.dispose();
        }

        void reload() {
            for(final JCheckBox chkBox : ReloadDialog.this.checkBoxes) {
                if(chkBox.isSelected()) {
                    ReloadDialog.this.filesToRelod.add(chkBox.getText());
                }
            }
            ReloadDialog.this.dispose();
        }

        void reloadAll() {
            ReloadDialog.this.filesToRelod.addAll(ReloadDialog.this.updatedDatafiles);
            ReloadDialog.this.dispose();
        }
    }

    /**
     * Constructor for ReloadDialog. If there are modified files in the database, the user is presented a list of files from which he can
     * select the ones to be reloaded. Otherwise an error message is displayed.
     * 
     * @param updatedFiles List of updated database files
     */
    ReloadDialog(final List<String> updatedFiles) {
        super();

        // --------------------------
        // Initialize dialog
        // --------------------------
        this.updatedDatafiles = updatedFiles;
        this.setModal(true);
        this.setTitle(ReloadDialog.dialogTitle);
        this.setLocationRelativeTo(Igui.instance().getMainFrame());

        this.ButtonReloadAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ReloadDialog.this.bActions.reloadAll();
            }
        });
        this.ButtonReload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ReloadDialog.this.bActions.reload();
            }
        });
        this.ButtonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ReloadDialog.this.bActions.cancel();
            }
        });
        this.ButtonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ReloadDialog.this.bActions.ok();
            }
        });

        // ----------------------
        // Create dialog
        // ----------------------
        this.mainPanel.setLayout(new java.awt.GridBagLayout());

        final GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.NORTHWEST;
        c.weightx = 1.0;
        c.weighty = 0.0;

        Object[] options;

        // Files have been updated
        if(this.updatedDatafiles.size() > 0) {

            c.gridx = 1;
            c.gridy = 1;

            // ----------------------
            // Add the filenames with checkboxes to the dialog
            // ----------------------
            for(final String str : this.updatedDatafiles) {
                this.checkBoxes.add(new JCheckBox(str));
            }

            // Panel containing all the check-boxes
            final JPanel p = new JPanel(new BorderLayout());
            final Box vBox = Box.createVerticalBox();
            for(final JCheckBox cb : this.checkBoxes) {
                final Box hBox = Box.createHorizontalBox();
                cb.setOpaque(false);
                hBox.add(cb);
                hBox.add(Box.createHorizontalGlue());
                vBox.add(hBox);
            }
            p.add(vBox, BorderLayout.CENTER);
            p.setOpaque(false);
            p.setBorder(BorderFactory.createEtchedBorder());

            // Add the panel to a scroll pane
            final JScrollPane cbScrollPane = new JScrollPane(p);
            final int width = cbScrollPane.getPreferredSize().width;
            final int height = cbScrollPane.getPreferredSize().height;
            cbScrollPane.setPreferredSize(new Dimension(width < ReloadDialog.MIN_WIDTH ? ReloadDialog.MIN_WIDTH : width + 20,
                                                        height > ReloadDialog.MAX_HEIGHT
                                                                                        ? ReloadDialog.MAX_HEIGHT
                                                                                        : (height < ReloadDialog.MIN_HEIGHT)
                                                                                                                            ? ReloadDialog.MIN_HEIGHT
                                                                                                                            : height));
            cbScrollPane.getViewport().setBackground(Color.WHITE);

            this.setTitle("Select the files to be reloaded");
            options = new JButton[] {this.ButtonReloadAll, this.ButtonReload, this.ButtonCancel};
            this.optionPane = new JOptionPane(cbScrollPane,
                                              JOptionPane.PLAIN_MESSAGE,
                                              JOptionPane.OK_CANCEL_OPTION,
                                              null,
                                              options,
                                              options[2]);
        } else {

            // ----------------------
            // Display a message
            // ----------------------
            c.gridx = 1;
            c.gridy = 1;
            c.insets = new Insets(5, 0, 5, 0);
            this.mainPanel.add(new JLabel("No need to reload the database, there are no modified files"), c);

            options = new Object[] {this.ButtonOK};

            this.optionPane = new JOptionPane(this.mainPanel,
                                              JOptionPane.INFORMATION_MESSAGE,
                                              JOptionPane.OK_CANCEL_OPTION,
                                              null,
                                              options,
                                              options[0]);
        }

        this.optionPane.setBackground(Color.white);
        this.optionPane.setOpaque(true);

        this.setContentPane(this.optionPane);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // ------------------------
        // Display dialog
        // ------------------------
        this.pack();
    }

    List<String> getFilesToReload() {
        return Collections.unmodifiableList(this.filesToRelod);
    }

    // public static void main(String[] args) {
    // javax.swing.SwingUtilities.invokeLater(new Runnable() {
    // @Override
    // public void run() {
    // try {
    // final ReloadDialog d = new ReloadDialog(Collections.nCopies(300, new String("pippo")));
    // d.setVisible(true);
    // }
    // catch(final Exception ex) {
    // ex.printStackTrace();
    // }
    // }
    // });
    // }
}
