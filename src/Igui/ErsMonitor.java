package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXFrame;
import org.jdesktop.swingx.error.ErrorInfo;


class ErsMonitor implements ItemListener {

    private final static long DEF_TIMEOUT = 10;

    private final ErsPanel ersPanel;
    private final JComboBox<String> partList = new JComboBox<String>();
    private final JXFrame mainFrame;

    private volatile String partition;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final MTSServerChecker mtsServerCheckerRunnable = new MTSServerChecker();
    private final ScheduledExecutorService mtsServerChecker = Executors.newSingleThreadScheduledExecutor();

    /**
     * {@link Runnable} used to perform ERS subscriptions.
     * <p>
     * It should always be executed in the {@link ErsMonitor#executor} executor.
     */
    private class ERSSubscriber implements Runnable {
        private final String partName;
        private final boolean unsubscribe;

        public ERSSubscriber(final String partName, final boolean unsubscribe) {
            this.partName = partName;
            this.unsubscribe = unsubscribe;
        }

        @Override
        public void run() {
            synchronized(ErsMonitor.this) {
                try {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ErsMonitor.this.mainFrame.setWaiting(true);
                        }
                    });

                    // We are changing the working partition, reset the MTS server checker state
                    ErsMonitor.this.mtsServerCheckerRunnable.resetState();

                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ErsMonitor.this.partList.setEnabled(false);
                            ErsMonitor.this.ersPanel.setBusy(true);
                            ErsMonitor.this.ersPanel.setBusyMessage("Subscribing to messages in partition \"" + ERSSubscriber.this.partName
                                                                    + "\"...");
                        }
                    });

                    ErsMonitor.this.partition = this.partName;
                    ErsMonitor.this.ersPanel.changePartitionSubscription(this.partName, this.unsubscribe).get();

                    ErsMonitor.this.setFrameTitle("ERS Monitor [" + this.partName + "]");

                    final String msg = "Subscribed to messages in partition \"" + this.partName + "\"";
                    ErsMonitor.this.ersPanel.addInternalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                }
                catch(final InterruptedException ex) {
                    IguiLogger.warning("Thread interrupted!", ex);
                    Thread.currentThread().interrupt();
                }
                catch(final ExecutionException ex) {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ErsMonitor.this.setFrameTitle("ERS Monitor []");
                        }
                    });

                    final Throwable cause = ex.getCause();
                    final String errMsg = "Failed subscribing to ers messages in partition \"" + this.partName + "\": " + cause;
                    IguiLogger.error(errMsg, ex);
                    ErsMonitor.this.ersPanel.addInternalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                    ErsMonitor.this.ersPanel.showErrorDialog("ERS Monitor Error", errMsg, ex);
                }
                finally {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ErsMonitor.this.partList.setEnabled(true);
                            ErsMonitor.this.ersPanel.setBusy(false);
                            ErsMonitor.this.mainFrame.setWaiting(false);
                        }
                    });

                }
            }
        }
    }

    /**
     * {@link Runnable} used to check regularly that the MTS server is alive.
     * <p>
     * This {@link Runnable} is scheduled using the {@link ErsMonitor#mtsServerChecker} scheduled executor.
     */
    private class MTSServerChecker implements Runnable {
        private final AtomicBoolean hasBeenStopped = new AtomicBoolean(false);

        MTSServerChecker() {
        }

        @Override
        public void run() {
            synchronized(ErsMonitor.this) {
                final String part = ErsMonitor.this.partition;

                final ipc.Partition p = new ipc.Partition(part);
                final boolean partitionFound = p.isValid();

                if(partitionFound == false) {
                    // The MTS server has been stopped, keep trace of it and inform the user
                    this.hasBeenStopped.set(true);
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            final String msg = "It looks like the MTS server is not available in partition \"" + part
                                               + "\": the ERS monitor will automatically re-subscribe when the server is up again";
                            IguiLogger.info(msg);
                            ErsMonitor.this.ersPanel.setBusy(true);
                            ErsMonitor.this.ersPanel.setBusyMessage(msg);
                        }
                    });
                } else {
                    // The MTS server is up, check whether it was down previously: the subscription has to be re-done only in that case
                    boolean isObjectValid = false;
                    try {
                        final ipc.ObjectEnumeration<mts.worker> mtsServers = new ipc.ObjectEnumeration<mts.worker>(p,
                                                                                                                   mts.worker.class,
                                                                                                                   true);
                        isObjectValid = !mtsServers.isEmpty();
                    }
                    catch(final ipc.InvalidPartitionException ex) {
                        IguiLogger.warning("Cannot reach the partition IPC server: " + ex, ex);
                    }

                    if((this.hasBeenStopped.get() == true) && (isObjectValid == true)) {
                        // Keep trace that the partition is up again
                        this.hasBeenStopped.set(false);

                        IguiLogger.info("The MTS server is up again: re-subscribing...");
                        ErsMonitor.this.executor.execute(new ERSSubscriber(part, false));

                        // The GUI busy state is removed by the ERSSubscriber
                    }
                }
            }
        }

        // Reset the state of this checker when the subscription is performed by the ERSSubscriber
        void resetState() {
            this.hasBeenStopped.set(false);
        }
    }

    ErsMonitor(final String partName) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        this.partition = partName;
        this.mainFrame = new JXFrame("ERS Monitor [" + partName + "]");
        this.ersPanel = new ErsPanel(partName) {
            private static final long serialVersionUID = 9143380840250941988L;

            // We need to override the 'showErrorDialog' methods because
            // in the ErsPanel the try to set the dialog location wrt to the
            // Igui, but there is no Igui (and we do not want to instantiate it)
            // when running the ErsMonitor.

            /**
             * @see Igui.ErsPanel#showErrorDialog(java.lang.String)
             */
            @Override
            void showErrorDialog(final String msg) {
                if(javax.swing.SwingUtilities.isEventDispatchThread() == true) {
                    JOptionPane.showMessageDialog(ErsMonitor.this.mainFrame, msg, "ERS Monitor Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            JOptionPane.showMessageDialog(ErsMonitor.this.mainFrame, msg, "ERS Monitor Error", JOptionPane.ERROR_MESSAGE);
                        }
                    });
                }
            }

            /**
             * @see Igui.ErsPanel#showErrorDialog(java.lang.String, java.lang.String, java.lang.Throwable)
             */
            @Override
            void showErrorDialog(final String title, final String msg, final Throwable ex) {
                final ErrorInfo errorInfo = new ErrorInfo(title, msg, null, null, ex, null, null);
                JXErrorPane.showDialog(ErsMonitor.this.mainFrame, errorInfo);
            }
        };

        this.initGUI();
    }

    void setFrameTitle(final String title) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ErsMonitor.this.mainFrame.setTitle(title);
            }
        });
    }

    @Override
    public void itemStateChanged(final ItemEvent e) {
        final String itemName = e.getItem().toString();

        if((e.getStateChange() == ItemEvent.SELECTED) && (itemName.equals(ErsMonitor.this.partition) == false)) {
            ErsMonitor.this.executor.execute(new ERSSubscriber(itemName, true));
        }
    }

    private Future<?> init() {
        return this.executor.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ErsMonitor.this.ersPanel.panelInit(true);
                ErsMonitor.this.mtsServerChecker.scheduleWithFixedDelay(ErsMonitor.this.mtsServerCheckerRunnable, 0, 30, TimeUnit.SECONDS);
                return null;
            }
        });
    }

    private Future<?> terminate() {
        return this.executor.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                ErsMonitor.this.ersPanel.panelTerminated();
                return null;
            }
        });
    }

    private void initGUI() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation";

        this.partList.setMaximumSize(new Dimension(200, 20));

        final JButton cloneButton = new JButton("Clone");
        cloneButton.setToolTipText("Clone the window keeping a copy of the shown messages");
        cloneButton.setIcon(Igui.createIcon("snapshot.png"));
        cloneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final JFrame fr = ErsMonitor.this.ersPanel.cloneTable("ERS Monitor Clone Window [" + ErsMonitor.this.partition + "]");
                fr.setLocationRelativeTo(ErsMonitor.this.mainFrame);
            }
        });

        final JToolBar tb = new JToolBar();
        tb.setFloatable(false);
        tb.setBorder(BorderFactory.createMatteBorder(2, 1, 2, 1, Color.DARK_GRAY));
        tb.add(Box.createHorizontalStrut(5));
        tb.add(new JLabel("Partition"));
        tb.add(Box.createHorizontalStrut(5));
        tb.add(this.partList);
        tb.add(Box.createHorizontalGlue());
        tb.add(cloneButton);
        tb.add(Box.createHorizontalStrut(3));

        this.mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                ErsMonitor.this.mainFrame.setVisible(false);
                ErsMonitor.exitErsMonitor(ErsMonitor.this, 0);
            }
        });
        this.mainFrame.setLayout(new BorderLayout());
        this.mainFrame.add(tb, BorderLayout.NORTH);

        final JPanel p = new JPanel(new BorderLayout());
        p.add(this.ersPanel.getPanelLayer(), BorderLayout.CENTER);
        p.setBorder(BorderFactory.createLoweredBevelBorder());

        this.mainFrame.add(p, BorderLayout.CENTER);
    }

    private void showFrame(final String[] partitionList) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                for(final String s : partitionList) {
                    ErsMonitor.this.partList.addItem(s);
                }
                ErsMonitor.this.partList.setSelectedItem(ErsMonitor.this.partition);
                ErsMonitor.this.partList.addItemListener(ErsMonitor.this);

                ErsMonitor.this.mainFrame.setSize(new Dimension(900, 600));
                ErsMonitor.this.mainFrame.setMinimumSize(new Dimension(800, 500));
                ErsMonitor.this.mainFrame.pack();
                ErsMonitor.this.mainFrame.setVisible(true);
            }
        });
    }

    static void exitErsMonitor(final ErsMonitor ersMon, final int exitCode) {
        new Thread() {
            @Override
            public void run() {
                try {
                    if(ersMon != null) {
                        ersMon.terminate().get(ErsMonitor.DEF_TIMEOUT, TimeUnit.SECONDS);
                    }
                }
                catch(final Exception ex) {
                    IguiLogger.error("An error occurred while exiting: " + ex);
                }
                finally {
                    System.exit(exitCode);
                }
            }
        }.start();
    }

    static ErsMonitor create(final String partName) throws InterruptedException, ExecutionException {
        // Create the panel in the EDT
        IguiLogger.info("Creating the main panel...");

        final FutureTask<ErsMonitor> task = new FutureTask<ErsMonitor>(new Callable<ErsMonitor>() {
            @Override
            public ErsMonitor call() throws Exception {
                return new ErsMonitor(partName);
            }
        });
        javax.swing.SwingUtilities.invokeLater(task);
        final ErsMonitor p = task.get();

        IguiLogger.info("The main panel has been created");

        return p;
    }

    static String[] getPartitionList() {
        final Set<String> partitions = new TreeSet<String>();

        try {
            final ipc.PartitionEnumeration partIt = new ipc.PartitionEnumeration();
            while(partIt.hasMoreElements() == true) {
                partitions.add(partIt.nextElement().getName());
            }
        }
        catch(final ipc.InvalidPartitionException ex) {
            IguiLogger.error("Failed getting the list of available partitions from IPC: " + ex, ex);
        }

        partitions.add("initial");

        return partitions.toArray(new String[partitions.size()]);
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        ErsMonitor p = null;
        try {
            final StringBuilder partNameBuilder = new StringBuilder();

            IguiLogger.info("Getting the list of running partitions");
            final String[] partitionList = ErsMonitor.getPartitionList();
            IguiLogger.info("Running partitions are: " + Arrays.toString(partitionList));

            // Get the name of the partition
            if((args.length == 0) || ((args.length > 0) && (args[0].isEmpty() == true))) {
                final FutureTask<Object> task = new FutureTask<Object>(new Callable<Object>() {
                    @Override
                    public Object call() {
                        return JOptionPane.showInputDialog(null,
                                                           "Please, select a partition:",
                                                           "ERS Monitor",
                                                           JOptionPane.QUESTION_MESSAGE,
                                                           null,
                                                           partitionList,
                                                           partitionList[0]);
                    }
                });

                javax.swing.SwingUtilities.invokeLater(task);

                final Object selectedPartition = task.get();
                if(selectedPartition != null) {
                    partNameBuilder.append(selectedPartition.toString());
                }
            } else {
                partNameBuilder.append(args[0]);
            }

            final String partName = partNameBuilder.toString();
            if(partName.isEmpty() == true) {
                IguiLogger.info("No partition has been provided, exiting...");
                ErsMonitor.exitErsMonitor(p, 0);
            } else {
                IguiLogger.info("Starting the ERS monitor in partition \"" + partName + "\"");

                // Check whether the partition is alive
                final ipc.Partition ipcPart = new ipc.Partition(partName);
                if(ipcPart.isValid() == false) {
                    throw new IguiException.InvalidPartition(partName);
                }

                try {
                    final ipc.ObjectEnumeration<mts.worker> mtsServers = new ipc.ObjectEnumeration<mts.worker>(ipcPart,
                                                                                                               mts.worker.class,
                                                                                                               true);
                    if(mtsServers.isEmpty() == true) {
                        throw new IguiException.ERSException("No MTS workers can be found in partition \"" + partName + "\"");
                    }
                }
                catch(final ipc.InvalidPartitionException ex) {
                    throw new IguiException.ERSException("The partition \"" + partName + "\" cannot be reached: " + ex, ex);
                }

                // Create, initialize and show the panel
                p = ErsMonitor.create(partName);

                p.init().get(ErsMonitor.DEF_TIMEOUT, TimeUnit.SECONDS);

                IguiLogger.info("ERS monitor has been initialized");

                p.showFrame(partitionList);

                IguiLogger.info("Main frame is now visible");
            }
        }
        catch(final IguiException ex) {
            final String msg = ex.getMessage();
            IguiLogger.error(msg);
            JOptionPane.showMessageDialog(null, msg, "ERS Monitor", JOptionPane.ERROR_MESSAGE);

            ErsMonitor.exitErsMonitor(p, 0);
        }
        catch(final Exception ex) {
            final String msg = "Unexpected exception while creating the ERS monitor: " + ex;
            IguiLogger.error(msg, ex);
            JXErrorPane.showDialog(null, new ErrorInfo("ERS Monitor Error", msg, null, null, ex, null, null));

            ErsMonitor.exitErsMonitor(p, -1);
        }
    }
}
