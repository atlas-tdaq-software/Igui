package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolTip;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.AbstractTableModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import Igui.Common.MultiLineToolTip;
import config.Version;


class ReloadDialogRepo extends JDialog {
    private final static long serialVersionUID = -2087943242722456505L;

    private final static String USE_BROWSER = IguiConfiguration.instance().getBrowser();
    private final static String DB_GIT_BASE = IguiConfiguration.instance().getGitURL();

    private volatile String selectedCommit = "";
    private final TableModel tableModel;
    private final JButton reloadButton = new JButton("Reload");
    private final JButton cancelButton = new JButton("Cancel");
    private final ButtonActions bActions = new ButtonActions(this);

    /**
     *
     */
    private static class ButtonActions {
        private final WeakReference<ReloadDialogRepo> mainDialog;

        ButtonActions(final ReloadDialogRepo mainDialog) {
            this.mainDialog = new WeakReference<ReloadDialogRepo>(mainDialog);
        }

        static void openBrowser(final String version) {
            if(ReloadDialogRepo.USE_BROWSER.isEmpty() == false) {
                if(ReloadDialogRepo.DB_GIT_BASE.isEmpty() == false) {
                    // The URL the browser should point to
                    final StringBuilder url = new StringBuilder();
                    // The web link base address
                    url.append(ReloadDialogRepo.DB_GIT_BASE);
                    // Append a "/" for safety wrt to the variable definitions
                    url.append("/");
                    // The commit ID
                    url.append(version);

                    // Now start the browser and direct it to the URL
                    final List<String> args = new ArrayList<String>();

                    // The USE_BROWSER may in principle contain arguments passed to the browser
                    final String[] browserArgs = ReloadDialogRepo.USE_BROWSER.trim().split(" ");
                    for(final String s : browserArgs) {
                        final String tr = s.trim();
                        if(tr.isEmpty() == false) {
                            args.add(tr);
                        }
                    }
                    args.add(url.toString());

                    Igui.instance().startProcess(args);
                } else {
                    final String errMsg = "Cannot open the web GIT viewer for commit \"" + version
                                          + "\" because the web base address is not defined. Please, define it using the \""
                                          + IguiConstants.DB_GIT_BASE_ENV_VAR + "\" variable in your environment or setting the \""
                                          + IguiConstants.DB_GIT_BASE_PROPERTY + "\" property";
                    IguiLogger.error(errMsg);
                    ErrorFrame.showError(errMsg);
                }
            } else {
                final String errMsg = "Cannot open the web GIT viewer for commit \"" + version
                                      + "\" because the default web browser to use is not defined. Please, define it using the \""
                                      + IguiConstants.USE_BROWSER_ENV_VAR + "\" variable in your environment or the \""
                                      + IguiConstants.USE_BROWSER_PROPERTY + "\" property";
                IguiLogger.error(errMsg);
                ErrorFrame.showError(errMsg);
            }
        }

        void reload() {
            final ReloadDialogRepo d = this.mainDialog.get();
            if(d != null) {
                final String selected = d.tableModel.lastSelectedItem();
                if(selected.isEmpty() == false) {
                    d.selectedCommit = d.tableModel.lastSelectedItem();
                    d.dispose();
                } else {
                    ErrorFrame.showWarning("Please, select a commit version in order to reload!");
                }
            }
        }

        void cancel() {
            final ReloadDialogRepo d = this.mainDialog.get();
            if(d != null) {
                d.selectedCommit = "";
                d.dispose();
            }
        }
    }

    /**
     * 
     */
    private static class TableItem {
        private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy hh:mm:ss");

        boolean selected;
        final String id;
        final String comment;
        final String author;
        final long rawTime;
        final String time;
        final String[] files;

        TableItem(final Version v) {
            this.selected = false;
            this.id = v.get_id();
            this.comment = v.get_comment();
            this.author = v.get_user();
            this.rawTime = v.get_timestamp().toEpochMilli();
            this.time = TableItem.DATE_FORMAT.format(new Date(this.rawTime));
            this.files = v.get_files();
        }
    }

    /**
     * 
     */
    private static class TableModel extends AbstractTableModel {
        private static final long serialVersionUID = -3823250952325637456L;

        private final static int NUMBER_OF_COL = 6;

        private final List<TableItem> tableItems;
        private String lastSelected = "";

        TableModel(final List<TableItem> items) {
            this.tableItems = items;
        }

        @Override
        public int getRowCount() {
            return this.tableItems.size();
        }

        @Override
        public int getColumnCount() {
            return TableModel.NUMBER_OF_COL;
        }

        @Override
        public Object getValueAt(final int rowIndex, final int columnIndex) {
            final TableItem it = this.tableItems.get(rowIndex);
            switch(columnIndex) {
                case 0:
                    return Boolean.valueOf(it.selected);
                case 1: {
                    final JLabel l = new JLabel(it.comment);

                    final StringBuilder tooltip = new StringBuilder();
                    tooltip.append("<b>Full comment:</b><br>" + it.comment);
                    tooltip.append("<br><b>Modified files:</b><br>" + Arrays.toString(it.files));

                    l.setToolTipText(tooltip.toString());
                    l.setBackground(null);
                    l.setOpaque(true);

                    return l;
                }
                case 2:
                    return it.author;
                case 3:
                    return it.time;
                case 4:
                    return Arrays.toString(it.files);
                case 5: {
                    // The goal is to show the GIT short hash
                    String buttonText;
                    try {
                        buttonText = it.id.substring(0, 8);
                    }
                    catch(final IndexOutOfBoundsException ex) {
                        buttonText = it.id;
                    }

                    final JButton b = new JButton(buttonText);
                    b.setToolTipText("See details on the GIT web view (opens a web browser)");
                    b.addActionListener((final ActionEvent a) -> {
                        ButtonActions.openBrowser(it.id);
                    });
                    return b;
                }
                default:
                    throw new IndexOutOfBoundsException();
            }
        }

        @Override
        public Class<?> getColumnClass(final int columnIndex) {
            switch(columnIndex) {
                case 0:
                    return Boolean.class;
                case 5:
                    return JButton.class;
                default:
                    return String.class;
            }
        }

        @Override
        public String getColumnName(final int column) {
            switch(column) {
                case 0:
                    return "";
                case 1:
                    return "Comment";
                case 2:
                    return "Author";
                case 3:
                    return "Date";
                case 4:
                    return "Modified Files";
                case 5:
                    return "Commit ID";
                default:
                    throw new IndexOutOfBoundsException();
            }
        }

        @Override
        public boolean isCellEditable(final int rowIndex, final int columnIndex) {
            return ((columnIndex == 0) || (columnIndex == 5)) ? true : false;
        }

        @Override
        public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
            if(columnIndex == 0) {
                final boolean newValue = ((Boolean) aValue).booleanValue();
                final TableItem it = this.tableItems.get(rowIndex);
                it.selected = newValue;

                if(newValue == true) {
                    this.lastSelected = it.id;
                } else {
                    if(rowIndex == (this.tableItems.size() - 1)) {
                        this.lastSelected = "";
                    } else {
                        this.lastSelected = this.tableItems.get(rowIndex + 1).id;
                    }
                }

                for(int i = 0; i < rowIndex; ++i) {
                    this.tableItems.get(i).selected = false;
                }

                for(int i = rowIndex + 1; i < this.tableItems.size(); ++i) {
                    this.tableItems.get(i).selected = true;
                }

                this.fireTableDataChanged();
            }
        }

        String lastSelectedItem() {
            return this.lastSelected;
        }
    }

    /**
     * 
     */
    private static class Table extends JXTable {
        private static final long serialVersionUID = 8685087158604450720L;

        Table(final TableModel model) {
            super(model);

            this.getColumnModel().getColumn(0).setPreferredWidth(5);
            this.getColumnModel().getColumn(1).setPreferredWidth(450);
            this.getColumnModel().getColumn(2).setPreferredWidth(100);
            this.getColumnModel().getColumn(3).setPreferredWidth(90);
            this.getColumnModel().getColumn(4).setPreferredWidth(300);
            this.getColumnModel().getColumn(5).setPreferredWidth(85);

            this.setRowHeight(25);

            this.getColumnModel().getColumn(1).setCellRenderer((final JTable table,
                                                                final Object value,
                                                                final boolean isSelected,
                                                                final boolean hasFocus,
                                                                final int row,
                                                                final int column) -> {
                return (JLabel) value;
            });

            this.getColumnModel().getColumn(5).setCellRenderer((final JTable table,
                                                                final Object value,
                                                                final boolean isSelected,
                                                                final boolean hasFocus,
                                                                final int row,
                                                                final int column) -> {
                return (JButton) value;
            });

            this.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(new JCheckBox()) {
                private static final long serialVersionUID = 1124390547733818220L;

                @Override
                public Component getTableCellEditorComponent(final JTable table,
                                                             final Object value,
                                                             final boolean isSelected,
                                                             final int row,
                                                             final int column)
                {
                    return (JButton) value;
                }
            });

            this.setIntercellSpacing(new Dimension(3, 1));
            this.setFillsViewportHeight(true);
            this.setShowGrid(false);
            this.setShowHorizontalLines(false);
            this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            this.setColumnControlVisible(true);
            this.setSortable(false);
            this.getColumnExt(4).setVisible(false);
            this.setHighlighters(new ColorHighlighter(new HighlightPredicate.AndHighlightPredicate(HighlightPredicate.EVEN,
                                                                                                   new HighlightPredicate.ColumnHighlightPredicate(0,
                                                                                                                                                   1,
                                                                                                                                                   2,
                                                                                                                                                   3,
                                                                                                                                                   4)),
                                                      Color.LIGHT_GRAY,
                                                      null));
        }

        @Override
        public JToolTip createToolTip() {
            final MultiLineToolTip tt = new MultiLineToolTip(true);
            tt.setComponent(this);
            return tt;
        }
    }

    /**
     * 
     */
    ReloadDialogRepo(final Version[] changes) {
        final List<TableItem> items = new ArrayList<>(changes.length);
        for(final Version v : changes) {
            items.add(new TableItem(v));
        }
        items.sort((final TableItem i1, final TableItem i2) -> {
            return Long.compare(i2.rawTime, i1.rawTime);
        });

        this.tableModel = new TableModel(items);

        this.cancelButton.addActionListener((final ActionEvent a) -> {
            this.bActions.cancel();
        });

        this.reloadButton.addActionListener((final ActionEvent a) -> {
            this.bActions.reload();
        });

        final JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.setOpaque(true);

        final JOptionPane op;
        if(items.isEmpty() == false) {
            final Table t = new Table(this.tableModel);
            p.add(new JScrollPane(t), BorderLayout.CENTER);

            op = new JOptionPane(p,
                                 JOptionPane.PLAIN_MESSAGE,
                                 JOptionPane.OK_CANCEL_OPTION,
                                 null,
                                 new JButton[] {this.reloadButton, this.cancelButton},
                                 null);

            this.setPreferredSize(new Dimension(950, 350));
        } else {
            p.add(new JLabel("No need to reload the database, there are no pending changes", SwingConstants.CENTER), BorderLayout.CENTER);

            op = new JOptionPane(p, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, new JButton[] {this.cancelButton}, null);

            this.setPreferredSize(new Dimension(500, 150));
        }

        op.setOpaque(true);
        op.setBackground(Color.white);

        this.setModal(true);
        this.setTitle("Igui - Database Reload");
        this.setLocationRelativeTo(Igui.instance().getMainFrame());
        this.setContentPane(op);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.pack();
    }

    String getSelectedVersion() {
        return this.selectedCommit;
    }

    // public static void main(final String[] args) {
    // javax.swing.SwingUtilities.invokeLater(new Runnable() {
    // @Override
    // public void run() {
    // final int num = 10;
    // final Version[] l = new Version[num];
    //
    // for(int i = 0; i < num; ++i) {
    // l[i] = new Version(UUID.randomUUID().toString(),
    // "Giuseppe Avolio",
    // Instant.now().toEpochMilli(),
    // "This is a very nice comment from GIT jsdh khskdj kajhsdlkjgfhpiueuy9p8 oias 09asd",
    // new String[] {"a.txt", "b.txt", "c.txt", "d.txt"});
    // try {
    // Thread.sleep(1);
    // }
    // catch(InterruptedException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }
    //
    // final ReloadDialogRepo rd = new ReloadDialogRepo(l);
    // rd.setVisible(true);
    // }
    // });
    // }
}
