package Igui.TestPanels;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Igui.Igui;
import Igui.IguiConstants;
import Igui.IguiException;
import Igui.IguiLogger;
import Igui.IguiPanel;
import Igui.RunControlFSM;


// //////////////////////////////////////////////////////////////////////////////////////////////////
// WARNING: Be aware of the thread policy in place! Give a careful read to the IguiPanel javadoc. //
// //////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Simple test panel showing the current Run Control FSM state and the allowed state transitions.
 * <p>
 * To have this panel in the IGUI set the <code>igui.panel</code> property to <code>Igui.TestPanels.TestPanel2</code>.
 */
public class TestPanel2 extends IguiPanel {

    private static final long serialVersionUID = 3694113203033593700L;
    private final JLabel label = new JLabel();
    private final Map<RunControlFSM.Transition, JButton> bMap = new EnumMap<RunControlFSM.Transition, JButton>(RunControlFSM.Transition.class);
    private final JPanel leftPanel = new JPanel(new GridLayout());
    private final JPanel rightPanel = new JPanel(new GridLayout());

    {
        // It is safe to directly access Swing component here: the constructor is called in the EDT
        this.createButtons();
        this.label.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        this.setLayout(new BorderLayout());
        this.add(this.leftPanel, BorderLayout.WEST);
        this.add(this.label);
        this.add(this.rightPanel, BorderLayout.EAST);
    }

    // This constructor is never called by the IGUI framework.
    // It may be used to develop a stand-alone version of the panel.
    public TestPanel2() throws Exception {
        super();
    }

    // This is the constructor called by the IGUI.
    // Remember that it is called in the Swing EDT: it means that
    // here mainly graphics should be built and no time-consuming tasks
    public TestPanel2(final Igui mainIgui) {
        super(mainIgui);
    }

    private final void createButtons() {
        for(final RunControlFSM.Transition t : RunControlFSM.Transition.values()) {
            this.bMap.put(t, new JButton(t.toString()));
        }
    }

    // Initialize here your panel: it will be made visible only after it
    // is successfully completed.
    // This is executed in the panel's own thread: it means that it is
    // fine to execute here time-consuming tasks.
    // At the same time Swing components shall not be directly accessed
    // but by the "invokeLater" and "invokeAndWait" methods provided by Swing.
    @Override
    public void panelInit(final RunControlFSM.State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException {
        this.stateChanged(rcState);
    }

    @Override
    public String getPanelName() {
        return "TestPanel2";
    }

    @Override
    public String getTabName() {
        return "TestPanel2";
    }

    // This is executed in the panel's thread when the panel is terminated
    @Override
    public void panelTerminated() {
        IguiLogger.debug(this.getPanelName() + " terminated");
    }

    // If this returns "true" then the panel is notified
    @Override
    public boolean rcStateAware() {
        return true;
    }

    // Called any time there is a change in the RC FSM state.
    // Remember that it is not safe to directly access Swing components here.
    // Give a look at how "stateChenged" is implemented.
    @Override
    public void rcStateChanged(final RunControlFSM.State oldState, final RunControlFSM.State newState) {
        this.stateChanged(newState);
    }

    // No IS subscription is performed, then nothing to do
    @Override
    public void refreshISSubscription() {
    }

    // Called every time the panel is selected
    @Override
    public void panelSelected() {
        IguiLogger.debug(this.getPanelName() + " selected.");
    }

    // Called every time the panel is de-selected
    @Override
    public void panelDeselected() {
        IguiLogger.debug(this.getPanelName() + " deselected.");
    }

    // NOTE: "invokeLater" is used!!!
    private void stateChanged(final RunControlFSM.State rcState) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Clean the left and right panels
                TestPanel2.this.leftPanel.removeAll();
                TestPanel2.this.rightPanel.removeAll();

                // Set the label with the current FSM state
                TestPanel2.this.label.setText(rcState.toString());

                // Get the available transitions for the current state
                final Set<RunControlFSM.Transition> trs = RunControlFSM.instance().getTransitions(rcState);
                final Iterator<RunControlFSM.Transition> it = trs.iterator();
                while(it.hasNext()) {
                    final RunControlFSM.Transition t = it.next();
                    final JButton b = TestPanel2.this.bMap.get(t);
                    if(t.upTransition()) {
                        TestPanel2.this.rightPanel.add(b);
                    } else {
                        TestPanel2.this.leftPanel.add(b);
                    }
                }

                if(TestPanel2.this.isVisible()) {
                    TestPanel2.this.leftPanel.validate();
                    TestPanel2.this.rightPanel.validate();
                    TestPanel2.this.repaint();
                }
            }
        });
    }

}
