package Igui.TestPanels;

import javax.swing.JLabel;

import Igui.Igui;
import Igui.IguiConstants;
import Igui.IguiException;
import Igui.IguiLogger;
import Igui.IguiPanel;
import Igui.RunControlFSM;


// //////////////////////////////////////////////////////////////////////////////////////////////////
// WARNING: Be aware of the thread policy in place! Give a careful read to the IguiPanel javadoc. //
// //////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Simple test panel showing a label reporting the current Run Control FSM state.
 * <p>
 * To have this panel in the IGUI set the <code>igui.panel</code> property to <code>Igui.TestPanels.TestPanel</code>.
 */
public class TestPanel extends IguiPanel {

    private static final long serialVersionUID = 3087295806472790290L;
    private final JLabel label = new JLabel();

    {
        // It is safe to directly access Swing component here: the constructor is called in the EDT
        this.label.setText("Initial Label");
        this.add(this.label);
    }

    // This constructor is never called by the IGUI framework.
    // It may be used to develop a stand-alone version of the panel.
    public TestPanel() throws Exception {
        super();
    }

    // This is the constructor called by the IGUI.
    // Remember that it is called in the Swing EDT: it means that
    // here mainly graphics should be built and no time-consuming tasks
    public TestPanel(final Igui mainIgui) {
        super(mainIgui);
    }

    // Initialize here your panel: it will be made visible only after it
    // is successfully completed.
    // This is executed in the panel's own thread: it means that it is
    // fine to execute here time-consuming tasks.
    // At the same time Swing components shall not be directly accessed
    // but by the "invokeLater" and "invokeAndWait" methods provided by Swing.
    @Override
    public void panelInit(final RunControlFSM.State rcState, final IguiConstants.AccessControlStatus accessStatus) throws IguiException {
        this.setLabelText(rcState.toString());
    }

    @Override
    public String getPanelName() {
        return "TestPanel";
    }

    @Override
    public String getTabName() {
        return "TestPanel";
    }

    // This is executed in the panel's thread when the panel is terminated
    @Override
    public void panelTerminated() {
        IguiLogger.debug(this.getPanelName() + " terminated");
    }

    // If this returns "true" then the panel is notified
    @Override
    public boolean rcStateAware() {
        return true;
    }

    // Called any time there is a change in the RC FSM state.
    // Remember that it is not safe to directly access Swing components here.
    // Give a look at how "setLabelText" is implemented.
    @Override
    public void rcStateChanged(final RunControlFSM.State oldState, final RunControlFSM.State newState) {
        this.setLabelText(newState.toString());
    }

    // No IS subscription is performed, then nothing to do
    @Override
    public void refreshISSubscription() {
    }

    // Called every time the panel is selected
    @Override
    public void panelSelected() {
        IguiLogger.debug(this.getPanelName() + " selected.");
    }

    // Called every time the panel is de-selected
    @Override
    public void panelDeselected() {
        IguiLogger.debug(this.getPanelName() + " deselected.");
    }

    // NOTE: "invokeLater" is used!!!
    private void setLabelText(final String text) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                TestPanel.this.label.setText(text);
            }
        });
    }
}
