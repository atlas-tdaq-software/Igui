package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;
import org.jdesktop.swingx.JXFrame;
import org.jdesktop.swingx.JXLabel;

import Igui.IguiConstants.MessageSeverity;
import Igui.IguiException.ElogException;
import Igui.IguiException.ISException;
import Igui.IguiException.RDBException;
import Igui.RunControlApplicationData.ApplicationISStatus;
import Igui.RunControlFSM.State;
import Igui.RunControlTestsPanel.InitialInfrastructureTestsPanel;
import Igui.Common.BlinkingComponentTimer;
import Igui.Common.BusyUI;
import Igui.Common.LoginDialog;
import Igui.Common.TimerDialog;
import TTCInfo.GLOBALBUSY;
import config.ConfigException;
import is.InfoEvent;
import rc.HoldTriggerInfo;

import com.jidesoft.swing.ButtonStyle;
import com.jidesoft.swing.CheckBoxList;
import com.jidesoft.swing.JideSplitButton;


/**
 * This class represents the Igui main frame and takes care of building the initial infrastructure check frame.
 * <p>
 * This class strictly cooperates with the {@link Igui} class. Usually the {@link #IguiFrame()} reacts to the user actions on the GUI and
 * then forwards the decisions to the {@link Igui} instance.
 * <p>
 * Since some methods of this class access Swing components they have to be executed in the EDT. When needed this is clearly specified in
 * method descriptions.
 */
final class IguiFrame extends JXFrame implements ChangeListener {

    private static final long serialVersionUID = -7433278123001572183L;

    /**
     * Tab pane containing all the IguiPanels
     */
    private final JTabbedPane tabPanel = new JTabbedPane();

    /**
     * Weak reference to the selected panel (access it only from the EDT, otherwise synchronize)
     */
    private WeakReference<IguiPanel> selectedPanel = new WeakReference<IguiPanel>(null);

    /**
     * Weak reference to the previously selected panel (access it only from the EDT, otherwise synchronize)
     */
    private WeakReference<IguiPanel> previousSelectedPanel = new WeakReference<IguiPanel>(null);

    /**
     * Icon file name for the exit menu item
     */
    private final static String exitMenuItemIconName = "exit_small.png";

    /**
     * Icon file name for the exit menu item
     */
    private final static String closeMenuItemIconName = "close.png";

    /**
     * Icon file name for the "reload and commit" button
     */
    private final static String reloadAndCommitIconName = "reload.png";

    /**
     * "Warning" icon used to flag the panel tabs
     */
    private final static ImageIcon warningIcon = Igui.createIcon("warning.png");

    /**
     * The UI to implement the "busy" state
     * 
     * @see {@link Igui.Common#BusyUI}
     */
    private final transient LockableUI busyUI = new BusyUI();

    /**
     * The "Commit and Reload" button
     */
    private final JButton commitAndReloadButton = new JButton();
    
    /**
     * Timer allowing the "Commit & Reload" button to blink
     */
    private final BlinkingComponentTimer commitAndReloadBlink = new BlinkingComponentTimer(this.commitAndReloadButton,
                                                                                           Color.GREEN,
                                                                                           this.commitAndReloadButton.getForeground(),
                                                                                           this.commitAndReloadButton.getBackground(),
                                                                                           this.commitAndReloadButton.getForeground(),
                                                                                           false);
    
    /**
     * Icon file name for the button starting the ERS monitor
     */
    private final static String ersIconName = "ERS.png";

    /**
     * Button starting the ERS monitor
     */
    private final JButton startERSButton = new ToolBarButton(Igui.createIcon(IguiFrame.ersIconName), "", "Start the ERS monitor");

    /**
     * Icon file name for the button starting the IS monitor
     */
    private final static String isIconName = "IS.png";
    
    /**
     * String used in the menu item to close the IGUi and exit the partition
     */
    private final static String closeAndExitLabel = "Terminate partition infrastructure";

    /**
     * Button starting the IS monitor
     */
    private final JButton startISButton = new ToolBarButton(Igui.createIcon(IguiFrame.isIconName), "", "Start the IS monitor");

    /**
     * Icon file name for the button starting the DVS
     */
    private final static String dvsIconName = "DVS.png";

    /**
     * Button starting the DVS
     */
    private final JButton startDVSButton = new ToolBarButton(Igui.createIcon(IguiFrame.dvsIconName), "", "Start the DVS");

    /**
     * Icon file name for the button starting the Event Viewer
     */
    private final static String evIconName = "ED.png";

    /**
     * Button starting the Event Viewer
     */
    private final JButton startEVButton = new ToolBarButton(Igui.createIcon(IguiFrame.evIconName), "", "Start the Event Viewer");

    /**
     * Icon file name for the button starting the database editor (DBE)
     */
    private final static String dbeIconName = "DBE.png";

    /**
     * Button starting the database editor (DBE)
     */
    private final JButton startDBEButton = new ToolBarButton(Igui.createIcon(IguiFrame.dbeIconName), "", "Start the database editor (DBE)");

    /**
     * Icon file name for the button starting the log manager
     */
    private final static String lmIconName = "LM.png";

    /**
     * Button starting the log manager
     */
    private final JButton startLMButton = new ToolBarButton(Igui.createIcon(IguiFrame.lmIconName), "", "Start the Log Manager");

    /**
     * Icon file name for the button starting the OH display
     */
    private final static String ohIconName = "OH.png";

    /**
     * Button starting the OH display
     */
    private final JButton startOHButton = new ToolBarButton(Igui.createIcon(IguiFrame.ohIconName), "", "Start the Online Histogram Display");

    /**
     * Reference to the ERS log panel
     */
    private final AtomicReference<ErsPanel> ersPanelRef = new AtomicReference<ErsPanel>();

    /**
     * Reference to the main command panel
     */
    private final AtomicReference<MainPanel> mainPanelRef = new AtomicReference<MainPanel>();

    /**
     * The JXLayer covering the horizontal spit pane: to be used with the busy UI
     */
    private final JXLayer<JComponent> panelLayer = new JXLayer<JComponent>();

    /**
     * Reference to the class implementing button actions
     */
    private final ButtonActions bAction = new ButtonActions();

    /**
     * The "Display" menu item in the "Access Control" menu
     */
    private final JRadioButtonMenuItem displayMenuItem = new JRadioButtonMenuItem();

    /**
     * The "Control" menu item in the "Access Control" menu
     */
    private final JRadioButtonMenuItem controlMenuItem = new JRadioButtonMenuItem();

    /**
     * The elog interface menu item in the "Settings" menu
     */
    private final JCheckBoxMenuItem elogMenuItem = new JCheckBoxMenuItem();

    /**
     * Icon file name for the "About" menu entry
     */
    private static final String aboutMenuItemIconName = "about.png";

    /**
     * The "About" menu item
     */
    private final JMenuItem aboutMenuItem = new JMenuItem("About IGUI", Igui.createIcon(IguiFrame.aboutMenuItemIconName));

    /**
     * Icon file name for the "Online Help" menu entry
     */
    private final static String helpMenuItemIconName = "help.png";

    /**
     * The "About" menu item
     */
    private final JMenuItem helpMenuItem = new JMenuItem("Online Help", Igui.createIcon(IguiFrame.helpMenuItemIconName));

    /**
     * The "Close" item in the "File" menu
     */
    private final JMenuItem closeMenuItem = new JMenuItem();

    /**
     * The "Close & Exit" item in the "File" menu
     */
    private final JMenuItem exitMenuItem = new JMenuItem();
    
    /**
     * Icon file name for the "Refresh IS Subscriptions" menu item
     */
    private static final String refreshISMenuItemIconName = "ISSubscription.png";

    /**
     * The "Refresh IS Subscriptions" menu item
     */
    private final JMenuItem refreshISMenuItem = new JMenuItem(Igui.createIcon(IguiFrame.refreshISMenuItemIconName));

    /**
     * Icon file name for the RDB authentication menu item
     */
    private static final String rdbAuthMenuItemIconName = "rdbAuth.png";

    /**
     * The "RDB Authentication" menu item
     */
    private final JMenuItem rdbAuthMenuItem = new JMenuItem(Igui.createIcon(IguiFrame.rdbAuthMenuItemIconName));

    /**
     * The "Debug" logging level item
     */
    private final JRadioButtonMenuItem debugItem = new JRadioButtonMenuItem("Debug");

    /**
     * The "Information" logging level item
     */
    private final JRadioButtonMenuItem infoItem = new JRadioButtonMenuItem("Information");

    /**
     * The "Warning" logging level item
     */
    private final JRadioButtonMenuItem warningItem = new JRadioButtonMenuItem("Warning");

    /**
     * The "Error" logging level item
     */
    private final JRadioButtonMenuItem errorItem = new JRadioButtonMenuItem("Error");

    /**
     * Map associating to each logging level the corresponding item in the menu
     */
    private final Map<org.apache.log4j.Level, JRadioButtonMenuItem> loggingLevelMap = new HashMap<org.apache.log4j.Level, JRadioButtonMenuItem>();

    /**
     * Help file location
     */
    private final static String helpFileName = IguiConstants.HELP_FILE_PREFIX + IguiConstants.HELP_PATH + "Introduction.htm";

    /**
     * List of panels available for dynamic loading
     */
    private final PanelList panelList = new PanelList();

    /**
     * Icon for the panel list
     */
    private final ImageIcon panelListIcon = Igui.createIcon("panelLoad.png");

    /**
     * Icon for the utility list
     */
    private final ImageIcon utilsListIcon = Igui.createIcon("utils.png"); 
    
    /**
     * RDB login dialog
     */
    private transient final LoginDialog rdbLoginDialog = new RDBLoginDialog(this, "RDB Authentication");

    /**
     * "Count-down" timeout for the forced stop dialog
     */
    private final static long stopDialogCountDown = 15L;

    /**
     * The "count-down" dialog used to force the IGUI termination
     * <p>
     * The "count-down" dialog can be disabled setting a timeout value less than or equal to zero via the corresponding VM property.
     * 
     * @see IguiConstants#FORCED_STOP_TIMEOUT_MINUTES
     * @see IguiFrame#IguiFrame()
     */
    private final ForcedStopDialog stopDialog;

    /**
     * The progress bar showing the total dead-time
     */
    private final JProgressBar deadTimeBar = new JProgressBar(0, 100);
    
    /**
     * Timer for having the dead-time progress bar blinking
     */
    private final BlinkingComponentTimer deadTimeBarBlink = new BlinkingComponentTimer(this.deadTimeBar,
                                                                                       this.deadTimeBar.getForeground(),
                                                                                       Color.RED,
                                                                                       this.deadTimeBar.getBackground(),
                                                                                       this.deadTimeBar.getForeground(),
                                                                                       true);

    /**
     * Collection of IS receivers (thread safe collection)
     */
    private final List<ISInfoReceiver> isReceivers = new CopyOnWriteArrayList<>();
    
    /**
     * Object holding information about the detector and the reason why the trigger is on-hold
     */
    private final ConcurrentMap<String, HoldTriggerInfo> holdTriggerRecords = new ConcurrentHashMap<>();
    
    /**
     * Object holding information about the global status of the trigger
     */
    private final AtomicReference<GLOBALBUSY> globalBusyInfo = new AtomicReference<>();

    /**
     * The total dead-time
     */
    private volatile float deadTime = -1f;
    
    /**
     * Utility class to receive IS updates about total hold-trigger information
     */    
    private final class HoldTriggerInfoReceiver extends ISInfoReceiver {
        private final Lock lk = new ReentrantLock();
        private final Set<String> outOfOrder = new HashSet<>(); // Used only in the synchronized "processEvent" method
        
        HoldTriggerInfoReceiver() {
            super(Igui.instance().getPartition(), IguiConstants.HOLD_TRIGGER_IS_SERVER, new is.Criteria(Pattern.compile(IguiConstants.HOLD_TRIGGER_IS_INFO_NAME + ".*"), 
                                                                                                        HoldTriggerInfo.type, 
                                                                                                        is.Criteria.Logic.AND));
        }
        
        @Override
        protected void infoDeleted(final InfoEvent info) {
            this.processEvent(info, false);
        }
        
        @Override
        protected void infoCreated(final InfoEvent info) {
            this.processEvent(info, true);
        }

        @Override
        protected void infoUpdated(final InfoEvent info) {
            // Do nothing
        }

        @Override
        protected void infoSubscribed(final InfoEvent info) {
            this.processEvent(info, true);
        }
        
        private void processEvent(final InfoEvent info, final boolean added) {            
            final String infoName = info.getName();
                        
            // Avoid concurrent execution of infoCreated/Subscribed with infoDeleted
            this.lk.lock();
            try {
                if(added == true) {
                    // Do not add if the info has actually been already deleted                    
                    if(this.outOfOrder.remove(infoName) == false) {
                        final HoldTriggerInfo newInfo = new HoldTriggerInfo();
                        info.getValue(newInfo);
                    
                        IguiFrame.this.holdTriggerRecords.put(infoName, newInfo);
                    }
                } else {
                    final HoldTriggerInfo old = IguiFrame.this.holdTriggerRecords.remove(infoName);
                    if(old == null) {
                        // A delete is received before creation: take note
                        this.outOfOrder.add(infoName);
                    }                
                }
            }
            finally {
                this.lk.unlock();
            }
                        
            IguiFrame.this.setDeadTimeAndHoldInfo();
        }
    }
    
    /**
     * Utility class to receive IS updates about the global busy status
     */    
    private final class GlobalBusyInfoReceiver extends ISInfoReceiver {
        GlobalBusyInfoReceiver() {
            super(IguiConstants.GLOBALBUSY_IS_INFO_NAME);
        }
        
        @Override
        protected void infoCreated(final InfoEvent info) {
            this.processEvent(info);
        }

        @Override
        protected void infoUpdated(final InfoEvent info) {
            this.processEvent(info);
        }

        @Override
        protected void infoSubscribed(final InfoEvent info) {
            this.processEvent(info);
        }
        
        private void processEvent(final InfoEvent info) {
            final GLOBALBUSY gb = new GLOBALBUSY();
            info.getValue(gb);                        
            IguiFrame.this.globalBusyInfo.set(gb);
            
            IguiFrame.this.setDeadTimeAndHoldInfo();
        }
    }
    
    /**
     * Utility class to receive IS updates about total dead-time
     */
    private class DeadTimeReceiver extends ISInfoReceiver {
        DeadTimeReceiver() {
            super(IguiConstants.DEAD_TIME_IS_INFO_NAME);
        }

        @Override
        protected void infoCreated(final InfoEvent info) {
            this.processEvent(info);
        }

        @Override
        protected void infoUpdated(final InfoEvent info) {
            this.processEvent(info);
        }

        @Override
        protected void infoSubscribed(final InfoEvent info) {
            this.processEvent(info);
        }

        private void processEvent(final InfoEvent info) {
            try {            
                final is.AnyInfo i = new is.AnyInfo();
                info.getValue(i);
    
                final is.AnyInfo[] item = i.getArrayAttribute(3);
                for(final is.AnyInfo j : item) {
                    final int n = j.getAttributeCount();
                    if(j.getPrimitiveAttribute(n - 1).toString().equals("Total0")) {
                        final float dtValue = 100 * j.<Float>getPrimitiveAttribute(1).floatValue();
                        IguiFrame.this.deadTime = dtValue;
                                                
                        break;
                    }
                }
            }
            catch(final Exception ex) {
                // It is not beautiful but no better way to avoid issues in case the IS
                // information structure is changed
                IguiFrame.this.deadTime = -1f;
                                
                ex.printStackTrace();
            }
            
            IguiFrame.this.setDeadTimeAndHoldInfo();
        }
    }
           
    /**
     * A TimerDialog whose action is to terminate the IGUI when the timeout elapses. The "count-down" can be stopped/started using the
     * {@link IguiFrame#stopForcedCloseCountdown()} and {@link IguiFrame#startForcedCloseCountdown()} methods.
     * 
     * @see TimerDialog
     * @see IguiFrame#exitIgui(boolean)
     */
    private final class ForcedStopDialog extends TimerDialog {
        ForcedStopDialog(final Component parentComponent, final long timeout, final long alertTimeout, final TimeUnit timeUnit) {
            super(parentComponent, timeout, alertTimeout, timeUnit);
        }

        @Override
        protected void doAction() {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    // exitIgui has to be called in the EDT
                    IguiFrame.this.exitIgui(false);
                }
            });
        }

        @Override
        protected String message() {
            final StringBuilder msg = new StringBuilder();
            msg.append("You are receiving this message because the IGUI is running\n");
            msg.append("in status-display mode and, in order to prevent any waste of resources,\n");
            msg.append("the IGUI is going to be terminated as soon as the reported timeout\n");
            msg.append("is elapsed (unless the count-down is stopped closing this dialog).");

            return msg.toString();
        }
    }

    /**
     * Login dialog used to authenticate the user to the RDB.
     * 
     * @see Igui#rdbAuthenticate(String, String).
     */
    private static class RDBLoginDialog extends LoginDialog {
        RDBLoginDialog(final Component parent, final String title) {
            super(parent, title);
        }

        @Override
        protected void authenticate(final String user, final String passWord) throws Exception {
            IguiLogger.info("Authenticating user " + user + " to the RDB");
            try {
                Igui.instance().rdbAuthenticate(user, passWord);
            }
            catch(final Exception ex) {
                IguiLogger.warning("RDB authentication failed for user " + user + ": " + ex, ex);
                throw ex;
            }
        }
    }

    /**
     * {@link CheckBoxList} to represent the list of available panels.
     * <p>
     * List items are the names of classes the panels belong to.
     * <p>
     * All methods must be executed in the EDT.
     * 
     * @see ButtonActions#loadPanel(String, boolean)
     */
    private class PanelList extends CheckBoxList {
        private static final long serialVersionUID = -678427967642197835L;

        // Use the list model to add/remove items from the list
        private final DefaultListModel<String> listModel = new DefaultListModel<String>();

        // This bit set will keep trace of which item is selected
        private final BitSet selectionBits = new BitSet();

        PanelList() {
            super();
            this.setClickInCheckBoxOnly(false);
            this.setModel(this.listModel);

            // Add a selection listener to react to changes in list item selection
            this.getCheckBoxListSelectionModel().addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(final ListSelectionEvent e) {
                    // Accept events only if the check box list is enabled: extra check to avoid
                    // a mixture of loading/removing the same panel
                    if((PanelList.this.isCheckBoxEnabled() == true) && (e.getValueIsAdjusting() == false)) {
                        // Loop over all the list items and check whether the new state is different than
                        // the previous one: if so then load or remove the corresponding panel
                        final int size = PanelList.this.listModel.size();
                        for(int i = 0; i < size; ++i) {
                            final boolean previousState = PanelList.this.selectionBits.get(i);
                            final boolean currentState = PanelList.this.getCheckBoxListSelectionModel().isSelectedIndex(i);
                            if(previousState != currentState) {
                                if(currentState == true) {
                                    // Disable the check box: it is enabled again in 'updateSelectionStatus'.
                                    // Disabling it avoids the risk to start loading and removing of a panel
                                    // at the same time (events are not processes if the check box is disabled)
                                    PanelList.this.setCheckBoxEnabled(false);
                                    IguiFrame.this.bAction.loadPanel(PanelList.this.listModel.get(i).toString(), true);
                                } else {
                                    // Disable the check box as before
                                    PanelList.this.setCheckBoxEnabled(false);
                                    IguiFrame.this.bAction.loadPanel(PanelList.this.listModel.get(i).toString(), false);
                                }
                            }
                        }
                    }
                }
            });
        }

        // It sets the content of the list (removing everything already present)
        public void setList(final List<? extends String> list) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // Do that to avoid spurious selection signals to be processed (the listener
            // checks whether the check box is enabled)
            this.setCheckBoxEnabled(false);

            // Remove all the current items
            this.listModel.clear();

            // Clear the bit set
            this.selectionBits.clear();

            // Add the new items
            for(final String s : list) {
                this.listModel.addElement(s);
            }

            // Enable again the check box: use 'invokeLater' to be 100% sure
            // that all the listeners have been executed
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    PanelList.this.setCheckBoxEnabled(true);
                }
            });
        }

        // It adds a new element to the list
        public void addToList(final String element) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // Do that to avoid spurious selection signals to be processed (the listener
            // checks whether the check box is enabled)
            this.setCheckBoxEnabled(false);

            // Set the new item as non selected
            this.selectionBits.set(this.listModel.getSize(), false);

            // Append the new item to the list
            this.listModel.addElement(element);

            // Enable again the check box: use 'invokeLater' to be 100% sure
            // that all the listeners have been executed
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    PanelList.this.setCheckBoxEnabled(true);
                }
            });
        }

        // It removes a panel from the list
        public boolean removeFromList(final String element) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            // Do that to avoid spurious selection signals to be processed (the listener
            // checks whether the check box is enabled)
            this.setCheckBoxEnabled(false);

            // Remove and check if the removal has been successful
            final boolean removed = this.listModel.removeElement(element);
            if(removed == true) {
                // Removal is ok: reset the bit set (the removed element may be in whatever position)
                // and update it wrt the currently selected items
                this.selectionBits.clear();
                final int[] sel = this.getCheckBoxListSelectedIndices();
                for(final int i : sel) {
                    this.selectionBits.set(i);
                }
            }

            // Enable again the check box: use 'invokeLater' to be 100% sure
            // that all the listeners have been executed
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    PanelList.this.setCheckBoxEnabled(true);
                }
            });

            return removed;
        }

        // This method has to be called after a panel loading/removing has finished: that is needed
        // to update properly the panel list selection and keep the bit set consistent.
        // One reason to do that is to uncheck a panel from the list when the panel fails to be loaded.
        void updateSelectionStatus(final String element, final boolean selected) {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

            final int index = this.listModel.indexOf(element);
            if(index != -1) {
                this.selectionBits.set(index, selected);
                if(selected == true) {
                    this.addCheckBoxListSelectedIndex(index);
                } else {
                    this.removeCheckBoxListSelectedIndex(index);
                }
            }

            // Enable the check box list: it is disabled by the change listener before starting the
            // panel loading/removing procedure.
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    PanelList.this.setCheckBoxEnabled(true);
                }
            });
        }
    }

    /**
     * WindowAdapter defining the action to execute when the Igui frame is asked to exit
     * 
     * @see Igui#iguiExit(boolean)
     */
    private final transient WindowAdapter windowListener = new WindowAdapter() {

        /**
         * Method executed when the frame is going to be closed.
         * 
         * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
         * @see IguiFrame#exitIgui(boolean)
         */
        @Override
        public void windowClosing(final WindowEvent e) {
            IguiFrame.this.exitIgui(false);
        }
    };

    /**
     * Simple button extending {@link JButton} used in the top tool-bar
     */
    private static class ToolBarButton extends JButton {
        private static final long serialVersionUID = -5694531151234856889L;

        /**
         * Constructor
         * 
         * @param icon Button icon
         * @param text Button text
         * @param tooltip Button tooltip
         */
        ToolBarButton(final Icon icon, final String text, final String tooltip) {
            super(text, icon);
            this.setToolTipText(tooltip);
            this.setOpaque(false);
            this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        }
    }

    /**
     * Class representing the frame used to show the ERS panel and the Root Controller infrastructure tree at the beginning, when the Root
     * Controller infrastructure is tested.
     * <p>
     * Once this frame is no more needed the {@link InitialInfrastructureFrame#terminate()} method has to be called.
     * 
     * @see Igui#init(boolean)
     * @see RunControlTestsPanel.InitialInfrastructureTestsPanel
     */
    final class InitialInfrastructureFrame extends JFrame {
        private static final long serialVersionUID = 5216560904700922759L;

        /**
         * Reference to the Root Controller infrastructure panel
         */
        private final InitialInfrastructureTestsPanel infrPanel;

        /**
         * Reference to the ERS log panel.
         * <p>
         * It is important to be a weak reference so that only the {@link Igui} keeps a stromg reference to it.
         */
        private final WeakReference<ErsPanel> ersLogPanel;

        /**
         * Name of the image file to show in the frame
         */
        private final static String imageFileName = "AtlasOnlineSoftware.jpg";

        /**
         * Constructor.
         * <p>
         * Always to be executed in the EDT!
         * 
         * @param infr The Root Controller infrastructure panel
         * @param ers The ERS panel
         */
        private InitialInfrastructureFrame(final InitialInfrastructureTestsPanel infr, final ErsPanel ers) {
            super("ATLAS TDAQ SOFTWARE");

            assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

            this.infrPanel = infr;
            this.ersLogPanel = new WeakReference<ErsPanel>(ers);

            // Do nothing at close, delegate the window adapter
            this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(final WindowEvent e) {
                    IguiFrame.this.windowListener.windowClosing(e);
                }
            });

            // Initialize the GUI
            this.initGUI();
        }

        /**
         * Call this method for final clean-up when this frame is no more needed.
         */
        void terminate() {
            this.infrPanel.terminate();
        }

        /**
         * It returns a reference to the Root Controller infrastructure panel.
         * 
         * @return A reference to the Root Controller infrastructure panel
         */
        public InitialInfrastructureTestsPanel getInfrPanel() {
            return this.infrPanel;
        }

        /**
         * It returns a reference to the ERS log panel.
         * 
         * @return A reference to the ERS panel (or <code>null</code> if the panel has been garbage collected for some reason)
         */
        public ErsPanel getErsLogPanel() {
            return this.ersLogPanel.get();
        }

        /**
         * It initializes the GUI.
         */
        private void initGUI() {
            this.setLayout(new BorderLayout());

            final JPanel topLeftPanel = new JPanel(new BorderLayout());
            topLeftPanel.setBackground(Color.WHITE);
            final JXLabel infLabel = new JXLabel("Infrastructure Status");
            infLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 25));
            infLabel.setTextRotation((3.0 * Math.PI) / 2.0);
            infLabel.setHorizontalAlignment(SwingConstants.CENTER);
            final JLabel picture = new JLabel(Igui.createIcon(InitialInfrastructureFrame.imageFileName));
            final JPanel picturePanel = new JPanel(new BorderLayout());
            picturePanel.add(picture, BorderLayout.CENTER);
            picturePanel.setMinimumSize(new Dimension(100, 0));
            picturePanel.setBackground(Color.WHITE);
            topLeftPanel.add(picturePanel, BorderLayout.CENTER);
            topLeftPanel.add(infLabel, BorderLayout.EAST);

            final JSplitPane hspl = new JSplitPane();
            hspl.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            hspl.setDividerSize(0);
            hspl.add(topLeftPanel, JSplitPane.LEFT);
            hspl.add(this.infrPanel, JSplitPane.RIGHT);
            hspl.setPreferredSize(new Dimension(919, 400));

            final JSplitPane vspl = new JSplitPane();
            vspl.setOrientation(JSplitPane.VERTICAL_SPLIT);
            vspl.add(hspl, JSplitPane.TOP);
            vspl.add(this.ersLogPanel.get(), JSplitPane.BOTTOM);

            final JToolBar topBar = new JToolBar();
            topBar.setBackground(Color.WHITE);
            topBar.setPreferredSize(new Dimension(919, 30));
            topBar.setFloatable(false);

            this.add(topBar, BorderLayout.NORTH);
            this.add(vspl, BorderLayout.CENTER);
            this.setPreferredSize(new Dimension(919, 650));

            this.pack();
        }
    }

    /**
     * Class implementing actions associated to some buttons.
     * <p>
     * To see the different actions associated to all the buttons see {@link IguiFrame#setObjectActions()}.
     */
    private final class ButtonActions {

        ButtonActions() {
        }

        /**
         * This method is executed every time the access control status is changed using the "Access Control" menu.
         * <p>
         * Since this action requires the Resource Manager to be interrogated a SwingWorker is started.
         * 
         * @param accessLevel The new access status
         * @see Igui#askRMResource(Igui.IguiConstants.AccessControlStatus, boolean)
         */
        void changeAccessLevel(final IguiConstants.AccessControlStatus accessLevel) {
            new SwingWorker<Void, Void>() {

                /**
                 * The Resource Manager is asked to grant the required access level. If the action is successful then the user panels are
                 * notified.
                 * 
                 * @return null
                 * @throws IguiException.RMException Error interrogating the Resource Manager
                 */
                @Override
                protected Void doInBackground() throws IguiException.RMException {
                    IguiLogger.info("Asking RM to acquire resource \"" + accessLevel.toString() + "\"");

                    // Ask the RM notifying the user panels
                    Igui.instance().askRMResource(accessLevel, true);

                    IguiLogger.info("Access control changed to \"" + accessLevel.toString() + "\"");
                    return null;
                }

                /**
                 * Wait for the background task execution and report any error.
                 */
                @Override
                protected void done() {
                    try {
                        this.get();
                    }
                    catch(final InterruptedException ex) {
                        final String errMsg = "Task interrupted while waiting for the Resource Manager server answer";
                        IguiLogger.warning(errMsg, ex);
                        Thread.currentThread().interrupt();
                    }
                    catch(final Exception ex) {
                        // In case of error do not change the selected menu item
                        this.revertSelection();

                        Throwable cause = null;
                        if(ExecutionException.class.isInstance(ex)) {
                            cause = ex.getCause();
                        }

                        final String message = (cause != null) ? cause.toString() : ex.toString();
                        IguiLogger.error("Error changing the access control level to \"" + accessLevel.toString() + "\": " + message, ex);
                        ErrorFrame.showError("Access Control Error",
                                             "Cannot change the Access Control level: " + message + ".",
                                             (cause != null) ? cause : ex);
                    }
                }

                /**
                 * In case of error restore the previously selected menu item
                 */
                private void revertSelection() {
                    if(accessLevel.equals(IguiConstants.AccessControlStatus.CONTROL)) {
                        IguiFrame.this.displayMenuItem.setSelected(true);
                    } else if(accessLevel.equals(IguiConstants.AccessControlStatus.DISPLAY)) {
                        IguiFrame.this.controlMenuItem.setSelected(true);
                    }
                }

            }.execute();

        }

        /**
         * Action associated to the "Commit and Reload" button.
         * <p>
         * Since this action requires remote invocations to the RDB system a SwingWorker is started.
         * 
         * @see Igui#commitAndReload()
         */
        void commitAndReload() {
            new SwingWorker<Void, Void>() {

                /**
                 * It executes the database commit and reload procedure.
                 * 
                 * @return null
                 * @throws IguiException.ConfigException Error with the RDB communication
                 * @throws InterruptedException The thread was interrupted
                 * @see Igui#commitAndReload()
                 */
                @Override
                protected Void doInBackground() throws IguiException.ConfigException, InterruptedException {
                    // Set the waiting cursor and disable the "Commit and Reload" button
                    try {
                        javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                            @Override
                            public void run() {
                                IguiFrame.this.setWaiting(true);
                                IguiFrame.this.commitAndReloadButton.setEnabled(false);
                            }
                        });
                    }
                    catch(final InvocationTargetException ex) {
                        IguiLogger.warning(ex.toString(), ex);
                    }

                    {
                        final String msg = "Committing database changes and reloading it...";
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                        IguiLogger.info(msg);
                    }

                    Igui.instance().commitAndReload();

                    return null;
                }

                /**
                 * Wait for background task termination and report any error.
                 */
                @Override
                protected void done() {
                    try {
                        this.get();
                        final String msg = "Database commit and reload is done";
                        IguiLogger.info(msg);
                        Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    }
                    catch(final InterruptedException ex) {
                        IguiLogger.warning("Task interrupted while waiting for the database \"commit & reload\" to finish", ex);
                        Thread.currentThread().interrupt();
                    }
                    catch(final ExecutionException ex) {
                        Throwable cause = ex.getCause();
                        final String message = cause.getMessage();
                        {
                            final String msg = "Cannot commit and reload the database: " + message;
                            IguiLogger.error(msg, ex);
                            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                        }                        
                                                
                        // The "cause" should be an Igui.ConfigException (thrown by Igui.instance().commitAndReload())
                        // Check that the "cause of the cause" in an Igui.RDBException in order to show the error dialog without all the error details
                        final boolean showDetails = (RDBException.class.isInstance(cause.getCause()) == false) || (RDBException.class.cast(cause.getCause()).isSchemaChanged() == false);
                        ErrorFrame.showError("DB Commit and Reload Failed", message, cause, showDetails);  
                    }
                    catch(final Exception ex) {
                        final String message = "unexpected error: " + ex.getMessage();
                        {
                            final String msg = "Cannot commit and reload the database: " + message;
                            IguiLogger.error(msg, ex);
                            Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                        }
                        ErrorFrame.showError("DB Commit and Reload Failed", message, ex);
                    }
                    finally {
                        // Back to the default cursor and enable again the "Commit and Reload" button
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                IguiFrame.this.setWaiting(false);
                                IguiFrame.this.commitAndReloadButton.setEnabled(true);
                            }
                        });

                    }
                }
            }.execute();
        }

        /**
         * Action linked to the {@link IguiFrame#panelList}: it is called every time a list item is (de)selected to dynamically load or
         * remove an user panel from the tab panel.
         * 
         * @param panelClassName The class name of the panel to load/remove
         * @param toBeLoaded <code>true</code> if the panel has to be loaded
         * @see Igui#loadPanelOnDemand(String)
         * @see Igui#removePanelOnDemand(String)
         */
        void loadPanel(final String panelClassName, final boolean toBeLoaded) {
            new SwingWorker<WeakReference<IguiPanel>, Void>() {

                @Override
                protected WeakReference<IguiPanel> doInBackground() throws Exception {
                    WeakReference<IguiPanel> ref = null;

                    // Change the cursor to busy
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            IguiFrame.this.setWaiting(true);
                        }
                    });

                    // Ask the Igui to load/remove the panel
                    final String msg;
                    if(toBeLoaded == true) {
                        msg = "Loading panel whose class is \"" + panelClassName + "\"...";
                        IguiLogger.info(msg);
                        Igui.instance().internalMessage(MessageSeverity.INFORMATION, msg);

                        final Future<WeakReference<IguiPanel>> task = Igui.instance().loadPanelOnDemand(panelClassName);
                        try {
                            // Wait for the panel to be created and initialized and report any error
                            ref = task.get();
                        }
                        catch(final InterruptedException ex) {
                            throw new InterruptedException("Loading of panel \"" + panelClassName + "\" has been interrupted");
                        }
                    } else {
                        msg = "Removing panel whose class is \"" + panelClassName + "\"...";
                        IguiLogger.info(msg);
                        Igui.instance().internalMessage(MessageSeverity.INFORMATION, msg);

                        // The panel removal is not supposed to fail, it does not supply any Future
                        Igui.instance().removePanelOnDemand(panelClassName);
                    }

                    return ref;
                }

                @Override
                protected void done() {
                    try {
                        // The reference is 'null' for the removal and valid for the loading
                        final WeakReference<IguiPanel> ref = this.get();

                        // Use invoke later so that we are sure to switch to the new tab only after
                        // the panel has been actually added to the pane
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                IguiFrame.this.panelList.updateSelectionStatus(panelClassName, toBeLoaded);

                                final String msg;
                                if(toBeLoaded == true) {
                                    msg = "Panel of class \"" + panelClassName + "\" has been loaded";
                                    final IguiPanel p = ref.get();
                                    if(p != null) {
                                        IguiFrame.this.switchToPanel(p);
                                    }
                                } else {
                                    msg = "Panel of class \"" + panelClassName + "\" has been removed";
                                }
                                IguiLogger.info(msg);
                                Igui.instance().internalMessage(MessageSeverity.INFORMATION, msg);
                            }
                        });
                    }
                    catch(final InterruptedException ex) {
                        IguiLogger.warning("EDT interrupted while waiting for dynamic panel loading/removal");
                        Thread.currentThread().interrupt();
                        IguiFrame.this.panelList.updateSelectionStatus(panelClassName, toBeLoaded);
                    }
                    catch(final ExecutionException ex) {
                        IguiFrame.this.panelList.updateSelectionStatus(panelClassName, !toBeLoaded);

                        final String msg;
                        if(toBeLoaded == true) {
                            msg = "Panel of class \"" + panelClassName + "\" could not be loaded: " + ex.getCause();
                        } else {
                            msg = "Panel of class \"" + panelClassName + "\" could not be removed: " + ex.getCause();
                        }
                        IguiLogger.error(msg, ex);
                        Igui.instance().internalMessage(MessageSeverity.ERROR, msg);
                        ErrorFrame.showError("IGUI Error", msg, ex);
                    }
                    finally {
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                IguiFrame.this.setWaiting(false);
                            }
                        });
                    }
                }
            }.execute();
        }

        /**
         * Action associated to the button starting the ERS monitor.
         * 
         * @see Igui#startProcess(List)
         */
        void startERS() {
            final List<String> args = new ArrayList<String>();
            Collections.addAll(args, IguiConstants.ERS_EXEC, "-p", Igui.instance().getPartition().getName());

            Igui.instance().startProcess(args);
        }

        /**
         * Action associated to the button starting the IS monitor.
         * 
         * @see Igui#startProcess(List)
         */
        void startIS() {
            final List<String> args = new ArrayList<String>();
            Collections.addAll(args, IguiConstants.IS_EXEC);

            Igui.instance().startProcess(args);
        }

        /**
         * Action associated to the button starting the DVS.
         * @throws IguiException.ConfigException Cannot retrieve the needed information from the database
         * 
         * @see Igui#startProcess(List)
         */
        void startDVS() throws IguiException.ConfigException {
            try {
                final List<String> args = new ArrayList<String>();
                Collections.addAll(args,
                                   IguiConstants.DVS_EXEC,
                                   Igui.instance().getDalPartition().get_DBName(),
                                   Igui.instance().getPartition().getName());
    
                Igui.instance().startProcess(args);
            }
            catch(final ConfigException ex) {
                throw new IguiException.ConfigException("Failed to retrieve the needed information from the database: " + ex, ex);
            }
        }

        /**
         * Action associated to the button starting the Event Viewer.
         * 
         * @see Igui#startProcess(List)
         */
        void startEV() {
            final List<String> args = new ArrayList<String>();
            Collections.addAll(args, IguiConstants.EV_EXEC);

            Igui.instance().startProcess(args);
        }

        /**
         * Action associated to the button starting the database editor (DBE).
         * <p>
         * The DBE is not started of the {@link IguiConstants#P1_ENV_VAR} is defined.
         * 
         * @see Igui#startProcess(List)
         */
        void startDBE() {
            try {
                final String value = System.getenv(IguiConstants.P1_ENV_VAR);
                if((value == null) || (value.equals(Integer.toString(1)) == false)) {
                    final List<String> args = new ArrayList<String>();
                    Collections.addAll(args, IguiConstants.DBE_EXEC, "-f", Igui.instance().getDalPartition().get_DBName());
    
                    Igui.instance().startProcess(args);
                } else {
                    ErrorFrame.showError("The database editor (DBE) cannot be started\nfrom the IGUI in the P1 environment.");
                }
            }
            catch(final ConfigException ex) {
                final String errMsg = "The DBE cannot be started: " + ex;
                IguiLogger.error(errMsg, ex);
                ErrorFrame.showError("IGUI Error", errMsg, ex);
            }
        }

        /**
         * Action associated to the button starting the log manager.
         * 
         * @see Igui#startProcess(List)
         */
        void startLM() {
            final List<String> args = new ArrayList<String>();
            Collections.addAll(args, IguiConstants.LM_EXEC);

            Igui.instance().startProcess(args);
        }

        /**
         * Action associated to the button starting the OH display.
         * 
         * @see Igui#startProcess(List)
         */
        void startOH() {
            final List<String> args = new ArrayList<String>();
            Collections.addAll(args, IguiConstants.OH_EXEC);

            Igui.instance().startProcess(args);
        }
    }

    /**
     * Constructor.
     * <p>
     * It initializes the mapping between logging levels and menu items and registers a ChangeListener for the tab pane. To be executed in
     * the EDT.
     */
    IguiFrame() {
        super();

        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.setLayout(new BorderLayout());
        this.setTitle("ATLAS TDAQ Software");
        this.setName("IguiFrame");
        this.addWindowListener(this.windowListener);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE); // Delegate the window listener

        // Initialize the mapping between logging levels and menu items
        this.loggingLevelMap.put(org.apache.log4j.Level.ERROR, this.errorItem);
        this.loggingLevelMap.put(org.apache.log4j.Level.WARN, this.warningItem);
        this.loggingLevelMap.put(org.apache.log4j.Level.INFO, this.infoItem);
        this.loggingLevelMap.put(org.apache.log4j.Level.DEBUG, this.debugItem);

        this.tabPanel.addChangeListener(this);

        // Create the dialog only if the defined timeout is greater than zero
        final long stopTimeout = IguiConfiguration.instance().getStopIguiTimeout();
        this.stopDialog = stopTimeout > 0 ? new ForcedStopDialog(this, stopTimeout, IguiFrame.stopDialogCountDown, TimeUnit.MINUTES) 
                                          : null;
    }

    /**
     * It starts the "count-down" dialog used to force the IGUI shutdown
     * 
     * @return <code>false</code> if the "count-down" dialog cannot be started because it has been disabled
     */
    boolean startForcedCloseCountdown() {
        boolean toReturn = true;

        if(this.stopDialog != null) {
            this.stopDialog.start();
        } else {
            toReturn = false;
            IguiLogger.info("Cannot start the forced shutdown count-down: not enabled");
        }

        return toReturn;
    }

    /**
     * It stops the "count-down" dialog used to force the IGUI shutdown
     * 
     * @return <code>false</code> if the "count-down" dialog cannot be stopped because it has been disabled
     */
    boolean stopForcedCloseCountdown() {
        boolean toReturn = true;

        if(this.stopDialog != null) {
            this.stopDialog.stop();
        } else {
            toReturn = false;
            IguiLogger.info("Cannot stop the forced shutdown count-down: not enabled");
        }

        return toReturn;
    }

    /**
     * It implements the ChangeListener interface for the tab pane.
     * <p>
     * It detects when an IguiPanel is selected or de-selected and calls the corresponding methods on the panels.
     * 
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     * @see IguiPanel#panelSelected()
     * @see IguiPanel#panelDeselected()
     */
    @Override
    public void stateChanged(final ChangeEvent e) {
        // Get the new and the old selected panels
        @SuppressWarnings("unchecked")
        final IguiPanel newSelectedPanel = (IguiPanel) ((JXLayer<JComponent>) this.tabPanel.getSelectedComponent()).getView();
        final IguiPanel oldSelectedPanel = this.selectedPanel.get();

        if(newSelectedPanel != oldSelectedPanel) {
            IguiLogger.debug("The new selected panel is \"" + newSelectedPanel.getPanelName() + "\"");

            // Call panel methods in their own thread
            Igui.notifySelectedPanel(newSelectedPanel);
            if(oldSelectedPanel != null) {
                Igui.notifyDeselectedPanel(oldSelectedPanel);
            }
        }

        // Cache a reference to the new selected panel: it must be a weak reference
        // to let the dynamic panel construction and termination work. If a strong reference
        // is kept a panel may not be garbage collected.
        this.selectedPanel = new WeakReference<IguiPanel>(newSelectedPanel);
        this.previousSelectedPanel = new WeakReference<IguiPanel>(oldSelectedPanel);
    }

    /**
     * It shows a dialog asking the user if he/she wants to discard DB changes, commit or just postpone them. The method is called by
     * {@link IguiPanel#showDiscardDbDialog(String)}.
     * <p>
     * Once the user has made its choice the correct action is performed. For simplicity sake the decision is taken here (the same place
     * where the dialog is shown) but the implementation is performed in the {@link Igui} class.
     * <p>
     * To be executed in the EDT!
     * 
     * @see IguiPanel#showDiscardDbDialog(String)
     * @see Igui#discardDb()
     * @see Igui#setDbChangesPending(boolean)
     * @see ButtonActions#commitAndReload()
     * @param panelMessage Message to be shown in the dialog.
     */
    void showDBPostponeDiscardDialog(final String panelMessage) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread()) : "EDT violation!";

        // The previously selected panel, it's the one calling this method
        final IguiPanel deselectedPanel = this.previousSelectedPanel.get();

        // Show the option pane
        int result = JOptionPane.CLOSED_OPTION;

        // Build the options for the user: the "Commit & Reload" button should not be enabled after NONE
        final Object[] options;
        if((Igui.instance().isDBChangeAllowed() == false) || (Igui.instance().getRCInfo().getISStatus().isBusy() == true)
           || (Igui.instance().getRCInfo().getISStatus().isError() == true))
        {
            options = new Object[] {"Discard", "Postpone"};
        } else {
            options = new Object[] {"Discard", "Postpone", "Commit & Reload"};
        }

        final String question = panelMessage
                                + "\n\nPlease, choose one of the following options, remembering that if you postpone\nchanges then the database will remain locked until the next commit:\n";

        // Continue showing the option pane if the user just closes it
        while(result == JOptionPane.CLOSED_OPTION) {
            result = JOptionPane.showOptionDialog(this,
                                                  question,
                                                  "Database Changes Not Committed"
                                                      + ((deselectedPanel != null) ? " - " + deselectedPanel.getPanelName() : ""),
                                                  JOptionPane.DEFAULT_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE,
                                                  null,
                                                  options,
                                                  options[0]);
        }

        switch(result) {
            case 0: // Discard: ask the Igui to take all the needed actions
                Igui.instance().discardDb();
                break;
            case 1: // Postpone: inform the Igui that there are DB pending changes and change the panel tab icon
                Igui.instance().setDbChangesPending(true);
                if(deselectedPanel != null) {
                    this.setTabWarning(deselectedPanel, true);
                }
                break;
            case 2: // Commit and reload: same action taken as pressing the "Commit & Reload" button
                this.bAction.commitAndReload();
                break;
            default:
                break;
        }
    }

    /**
     * Method executed any time the access control status is changed.
     * 
     * @param newAccessStatus The new access control status
     * @see Igui#askRMResource(Igui.IguiConstants.AccessControlStatus, boolean)
     */
    void accessControlChanged(final IguiConstants.AccessControlStatus newAccessStatus) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if(newAccessStatus.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
                    IguiFrame.this.rdbAuthMenuItem.setEnabled(true);
                    IguiFrame.this.exitMenuItem.setEnabled(true);
                } else {
                    IguiFrame.this.rdbAuthMenuItem.setEnabled(false);
                    IguiFrame.this.exitMenuItem.setEnabled(false);
                }
            }
        });

        this.updateDisplay(Igui.instance().getRCInfo());
    }

    /**
     * It sets to <code>color</code> the color of the tab of panel <code>panel</code>.
     * <p>
     * It can be executed from any thread.
     * 
     * @param panel The panel whose tab has to be "colored"
     * @param color The new color for the tab
     */
    void setTabColor(final IguiPanel panel, final Color color) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final int index = IguiFrame.this.tabPanel.indexOfComponent(panel.getPanelLayer());
                if(index != -1) {
                    IguiFrame.this.tabPanel.setForegroundAt(index, color);
                }
            }
        });
    }

    /**
     * It flags the tab of the <code>panel</code> panel as beeing in a "warning" state (i.e., using a warning icon).
     * <p>
     * Actually this method is used to flag a panel which has postponed some DB changes (usually the "warning" state should be removed after
     * a DB reloading or discarding).
     * <p>
     * It can be executed from any thread.
     * 
     * @see #showDBPostponeDiscardDialog(String)
     * @param panel The panel the tab belongs to
     * @param warning <code>true</code> if the tab should be put in a warning state
     */
    void setTabWarning(final IguiPanel panel, final boolean warning) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final int index = IguiFrame.this.tabPanel.indexOfComponent(panel.getPanelLayer());
                if(index != -1) {
                    if(warning == true) {
                        IguiFrame.this.tabPanel.setIconAt(index, IguiFrame.warningIcon);
                    } else {
                        IguiFrame.this.tabPanel.setIconAt(index, null);
                    }
                }
            }
        });
    }

    /**
     * It selects the <code>panel</code> panel in the Igui tabbed pane.
     * 
     * @param panel The panel to be selected
     */
    void switchToPanel(final IguiPanel panel) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        try {
            this.tabPanel.setSelectedComponent(panel.getPanelLayer());
        }
        catch(final IllegalArgumentException ex) {
            IguiLogger.warning("Cannot select the panel \"" + panel.getPanelName() + "\": " + ex, ex);
        }
    }

    /**
     * It adds a panel to the tab pane.
     * <p>
     * To be executed in the EDT.
     * 
     * @param panel The panel to add to the pane
     */
    void addPanelToTab(final IguiPanel panel) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.tabPanel.addTab(panel.getTabName(), panel.getPanelLayer());
    }

    /**
     * It fills the panel list with all the panels contained in <code>panelList</code>.
     * 
     * @param panelList List containing the panel (class names) to put into the list of panels available to be loaded on demand
     */
    void setPanelList(final List<? extends String> panelList) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.panelList.setList(panelList);
    }

    /**
     * It adds a panel whose class is <code>panelClassName</code> to the available panel list.
     * <p>
     * To be executed in the EDT
     * 
     * @param panelClassName The class name of the panel to be added
     */
    void addPanelToList(final String panelClassName) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.panelList.addToList(panelClassName);
    }

    /**
     * It removes the panel from the tab pane.
     * <p>
     * To be executed in the EDT.
     * 
     * @param panel The panel to remove
     */
    void removePanelFromTab(final IguiPanel panel) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.tabPanel.remove(panel.getPanelLayer());
    }

    /**
     * It updates the panel list when a panel is loaded or unloaded. 
     * 
     * @param panelClassName The class name of the panel
     * @param loaded <code>true</code> if the panel is loaded, <code>false</code> otherwise
     */
    void updatePanelSelectionStatus(final String panelClassName, final boolean loaded) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";
        
        this.panelList.updateSelectionStatus(panelClassName, loaded);
    }
    
    /**
     * It removes a panel whose class is <code>panelClassName</code> from the available panel list.
     * <p>
     * To be executed in the EDT
     * 
     * @param panelClassName The class name of the panel to be removed
     * @return <code>true</code> if the panel has been successfully removed from the list
     */
    boolean removePanelFromList(final String panelClassName) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        return this.panelList.removeFromList(panelClassName);
    }

    /**
     * When this method is called the frame is made visible.
     * <p>
     * <ul>
     * <li>All the Swing components are created and initialized;
     * <li>Buttons are enabled or disabled taking into account the RC state;
     * <li>The correct item is selected in the "Access Control" menu (taking into account the current access level);
     * </ul>
     * To be executed in the EDT.
     * 
     * @param p The position the frame should be placed at
     * @see IguiFrame#show()
     */
    void makeVisible(final Point p) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        // Build all the Swing components
        this.initGUI();

        // Check the RC state to enable/disable buttons
        if(Igui.instance().isDBChangeAllowed() == false) {
            this.disableCommitButton();
        } else {
            this.enableCommitButton();
        }

        // Select the right item in the "Access Control" menu
        final IguiConstants.AccessControlStatus ac = Igui.instance().getAccessLevel();
        if(ac.equals(IguiConstants.AccessControlStatus.DISPLAY)) {
            this.displayMenuItem.setSelected(true);
        } else if(ac.equals(IguiConstants.AccessControlStatus.CONTROL)) {
            this.controlMenuItem.setSelected(true);
        }
        
        this.accessControlChanged(ac);
        
        // SwingUtilities.updateComponentTreeUI(this);

        this.setLocation(p);
        this.setSize(1019, 800);
        this.setTitle("ATLAS TDAQ SOFTWARE - Partition " + Igui.instance().getPartition().getName());
        this.pack();
        this.setVisible(true);
    }

    /**
     * Called any time the Root Controller state is changed.
     * <p>
     * It updates the main command panel and subscribes to IS for dead-time information as well.
     * 
     * @param rcInfo IS structure containing the current state of the Root Controller
     * @see Igui#rcStateChanged(is.InfoEvent rcInfo)
     * @see MainPanel#updateDisplay(RunControlApplicationData)
     * @see IguiFrame#updateDisplay(RunControlApplicationData)
     */
    void rcInfoUpdated(final RunControlApplicationData rcInfo) {
        // Subscribe to get notifications about dead-time and hold trigger info (from IS)
        if(rcInfo.getISStatus().getState().equals(State.RUNNING) == true) {            
            // Initialize the IS receiver list
            if(this.isReceivers.isEmpty() == true) {
                this.isReceivers.add(new DeadTimeReceiver());
                this.isReceivers.add(new HoldTriggerInfoReceiver());
                this.isReceivers.add(new GlobalBusyInfoReceiver());
            }
            
            for(final ISInfoReceiver i : this.isReceivers) {
                try {
                    if(i.isSubscribed() == false) {
                        i.subscribe();
                    }
                }
                catch(final ISException ex) {
                    IguiLogger.error("Failed to subscribe to the IS information \"" + i.getName() + "\": " + ex, ex);
                }
            }            
        } else {
            for(final ISInfoReceiver i : this.isReceivers) {
                try {
                    if(i.isSubscribed() == true) {
                        i.unsubscribe();
                    }
                }
                catch(final ISException ex) {
                    IguiLogger.warning("Failed to un-subscribe to the IS information \"" + i.getName() + "\": " + ex, ex);
                }
            }
        }
        
        // Update the main panel
        try {
            this.mainPanelRef.get().updateDisplay(rcInfo);
        }
        catch(final NullPointerException ex) {
            // Ignore this: a call-back from RC IS has been received before the
            // main panel has been created
        }

        // Update this display (i.e., the toolbar)
        this.updateDisplay(rcInfo);        
    }

    /**
     * Set the main frame in a busy state.
     * <p>
     * To be executed in the EDT.
     * 
     * @param busy If <code>true</code> the frame in put in a busy state, if <code>false</code> the frame is put back in a normal state.
     * @see BusyUI#setLocked(boolean)
     */
    void setBusy(final boolean busy) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        if((busy == true) && !(this.panelLayer.getUI() instanceof BusyUI)) {
            this.panelLayer.setUI(this.busyUI);
        }
        this.busyUI.setLocked(busy);
    }

    /**
     * To be used when the frame is put in a busy state: it shows a message on the glass pane.
     * <p>
     * This method can be called multiple times to update the message. To be executed in the EDT.
     * 
     * @param message The message to show.
     * @see BusyUI#setBusyMessage(String)
     */
    void setBusyMessage(final String message) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        ((BusyUI) this.busyUI).setBusyMessage(message);
    }

    /**
     * Set the main command panel.
     * <p>
     * This method is needed because the panel creation and initialization is delegated to the Igui class. It has to be called before the
     * frame is made visible.
     * 
     * @param panel The main command panel
     * @see Igui#createEmbeddedPanels()
     */
    void setCmdPanel(final MainPanel panel) {
        this.mainPanelRef.compareAndSet(null, panel);
    }

    /**
     * Set the MSR log panel.
     * <p>
     * This method is needed because the panel creation and initialization is delegated to the Igui class. It has to be called before the
     * frame is made visible.
     * 
     * @param panel The ERS log panel
     * @see Igui#createEmbeddedPanels()
     */
    void setErsPanel(final ErsPanel panel) {
        this.ersPanelRef.compareAndSet(null, panel);
    }

    /**
     * It removes the ChangeListener used to detect when a tabbed panel is (de)selected.
     * <p>
     * Always do that before starting removing the IguiPanel from the IguiFrame at shutdown.
     * <p>
     * It can be executed from any thread.
     * 
     * @see Igui#iguiExit(boolean)
     */
    void removeTabListener() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                IguiFrame.this.tabPanel.removeChangeListener(IguiFrame.this);
            }
        });
    }

    /**
     * It enables or disables the panel list button.
     * <p>
     * To be executed in the EDT.
     * 
     * @param enable If <code>true</code> the button is enabled
     */
    void enablePanelListButton(final boolean enable) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.panelList.setEnabled(enable);
    }

    /**
     * Clean-up when exiting
     */
    void onExit() {
        for(final ISInfoReceiver i : this.isReceivers) {
            try {
                if(i.isSubscribed() == true) {
                    i.unsubscribe();
                }
            }
            catch(final ISException ex) {
                IguiLogger.warning("Failed to un-subscribe to the IS information \"" + i.getName() + "\": " + ex, ex);
            }
        }
    }

    /**
     * Use to notify components of the Igui frame about external database changes not reloaded yet.
     * <p>
     * In the current implementation, the "Commit & Reload" button id made blinking
     * 
     * @param changes <em>true</em> in case of external database changes not reloaded yet
     */
    void notifyExternalDbChanges(final boolean changes) {
        if((changes == true) && (this.commitAndReloadBlink.isRunning() == false)) {
            this.commitAndReloadBlink.start();
            this.commitAndReloadButton.setToolTipText("There are external database changes not reloaded yet! Press this button to load them.");
        } else if((changes == false) && (this.commitAndReloadBlink.isRunning() == true)) {
            this.commitAndReloadBlink.stop();
            this.commitAndReloadButton.setToolTipText("");
        }
    }
   
    /**
     * It creates the frame used for the initial infrastructure test.
     * <p>
     * To be executed in the EDT.
     * 
     * @see Igui#init(boolean)
     * @return A reference to the {@link InitialInfrastructureFrame}
     */
    InitialInfrastructureFrame buildInfrTestFrame(final InitialInfrastructureTestsPanel infr, final ErsPanel ers) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        return new InitialInfrastructureFrame(infr, ers);
    }
    
    /**
     * It sets the value of the dead-time in the progress-bar. 
     * <p>
     * A value of -1 sets the value as undefined.\n
     * Can be executed in any thread.
     */
    private void setDeadTimeAndHoldInfo() {
        final float value = IguiFrame.this.deadTime;
        final GLOBALBUSY busyInfo = IguiFrame.this.globalBusyInfo.get();
        
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                IguiFrame.this.deadTimeBar.setValue((int) value);

                if(value == -1) {
                    IguiFrame.this.deadTimeBar.setString("N/A");
                    IguiFrame.this.deadTimeBar.setToolTipText(null);
                    IguiFrame.this.deadTimeBarBlink.stop();
                } else {
                    final StringBuilder txt = new StringBuilder();
                    txt.append(new DecimalFormat("###.##").format(value));
                                        
                    
                    if((busyInfo != null) && (busyInfo.MasterTriggerBusy > 0) && (busyInfo.RunControlBusy > 0))
                    {                        
                        
                        // Collect the information and group by detector
                        final Map<String, Set<String>> m = new TreeMap<>();
                        IguiFrame.this.holdTriggerRecords.forEach((k, v) -> 
                                                                   {  
                                                                       final Set<String> old = m.putIfAbsent(v.causedByString, 
                                                                                                             Stream.of(v.reasonString)
                                                                                                                   .collect(Collectors.toCollection(TreeSet::new)));
                                                                       if(old != null) {
                                                                           old.add(v.reasonString);
                                                                       }
                                                                   });
                        m.forEach((k, v) -> {
                                                txt.append(" - ");
                                                txt.append(k);
                                                txt.append(" (");
                                                txt.append(String.join(",", v));
                                                txt.append(")");
                                            });
                    }
                    
                    final String toolBarTxt = txt.toString();
                    IguiFrame.this.deadTimeBar.setString(toolBarTxt);
                    IguiFrame.this.deadTimeBar.setToolTipText(toolBarTxt);
                    
                    if(value > IguiConstants.DEAD_TIME_THRESHOLD) {
                        IguiFrame.this.deadTimeBarBlink.start();
                    } else {
                        IguiFrame.this.deadTimeBarBlink.stop();
                    }
                }
            }
        });
    }
    
    /**
     * It updates the display according to the current Root Controller state.
     * <p>
     * This method is indispensable to enable or disable buttons when some actions are not allowed because the Root Controller is in a
     * certain state, is busy or is in error.
     * 
     * @param rcInfo IS structure containing information about the current Root Controller state
     * @see #rcInfoUpdated(RunControlApplicationData)
     */
    private void updateDisplay(final RunControlApplicationData rcInfo) {
        // Disable the 'commit & reload' button if needed
        final ApplicationISStatus rcISStatus = rcInfo.getISStatus();
        final boolean rcError = rcISStatus.isError();
        final boolean rcBusy = rcISStatus.isBusy();
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if((Igui.instance().isDBChangeAllowed() == false) || (rcBusy == true) || (rcError == true)) {
                    IguiFrame.this.disableCommitButton();
                } else {
                    IguiFrame.this.enableCommitButton();
                }
            }
        });
        
        if(rcISStatus.getState().equals(State.RUNNING) == false) {
            this.deadTime = -1f;
            this.setDeadTimeAndHoldInfo();
        }
    }

    /**
     * It disables the "Commit & Reload" button.
     * <p>
     * To be executed in the EDT.
     */
    private void disableCommitButton() {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.commitAndReloadButton.setEnabled(false);
    }

    /**
     * It enables the "Commit & Reload" button.
     * <p>
     * A check on the access control level is done: if in status display then the button is not enabled. To be executed in the EDT.
     */
    private void enableCommitButton() {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            this.commitAndReloadButton.setEnabled(true);
        } else {
            this.disableCommitButton();
        }
    }

    /**
     * It returns the string shown in the "close and exit partition" menu item
     * 
     * @return The string shown in the "close and exit partition" menu item
     */
    static String closeAndExitLabel() {
        return IguiFrame.closeAndExitLabel;
    }
    
    /**
     * It asks the Igui to start the exit procedure. If not in status display then a dialog is shown to the user asking whether the
     * partition should be terminated or not.
     * <p>
     * Who is taking care of exiting the Igui has to remove the ChangeListener ({@link #removeTabListener()}) to avoid null references when
     * all the panels are removed.
     * <p>
     * To be executed in the EDT.
     * 
     * @param askExitPartition <em>true</em> if the user should be asked whether to stop or not the partition
     * 
     * @see Igui#iguiExit(boolean)
     */
    private void exitIgui(final boolean askExitPartition) {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        boolean shouldExit = false;
        boolean shouldStopPartition = false;

        // Show the option pane
        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY) && (askExitPartition == true)) {
            final int selectedOption = JOptionPane.showConfirmDialog(this,
                                                                     "You are going to terminate the partition infrastructure and close the IGUI.\nAre you sure?");

            switch(selectedOption) {
                case JOptionPane.CANCEL_OPTION:
                    shouldExit = false;
                    shouldStopPartition = false;
                    break;
                case JOptionPane.YES_OPTION:
                    shouldExit = true;
                    shouldStopPartition = true;
                    break;
                case JOptionPane.NO_OPTION:
                    shouldExit = true;
                    shouldStopPartition = false;
                    break;
                default:
            }
        } else {
            shouldExit = true;
            shouldStopPartition = false;
        }

        if(shouldExit == true) {
            Igui.instance().iguiExit(shouldStopPartition, false);
        }
    }

    /**
     * It sets the actions to be executed by the various buttons.
     * <p>
     * <ul>
     * <li>Adds ItemListeners for items in the "Commands" menu;
     * <li>Adds ItemListeners for items in the "Access Control" menu;
     * <li>Adds ItemListeners for items in the "Settings" menu;
     * <li>Adds ItemListeners for items in the "Logging Level" menu;
     * <li>Adds ItemListeners for items in the "About" menu;
     * <li>Set action for the "Exit" button;
     * <li>Set actions for the button starting common used applications;
     * <li>Set action for the "Commit & Reload" button.
     * </ul>
     * Called in {@link IguiFrame#initGUI()}. To be executed in the EDT.
     * 
     * @see ButtonActions
     * @see IguiFrame#exitIgui(boolean)
     */
    private void setObjectActions() {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.aboutMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                AboutWindow.show();
            }
        });

        this.helpMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    final HelpFrame helpWindow = new HelpFrame(IguiFrame.helpFileName);
                    helpWindow.makeVisible();
                }
                catch(final IOException ex) {
                    final String errMsg = "Failed opening online help url \"" + IguiFrame.helpFileName + "\": " + ex;
                    IguiLogger.error(errMsg, ex);
                    ErrorFrame.showError("Online Help Error", errMsg, ex);
                }
            }
        });

        this.refreshISMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                Igui.instance().refreshISSubscriptions();
            }
        });

        this.rdbAuthMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.rdbLoginDialog.show();
            }
        });

        this.elogMenuItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                final JCheckBoxMenuItem source = (JCheckBoxMenuItem) e.getSource();
                final boolean selected = source.isSelected();
                try {
                    Igui.instance().enableElogInterface(selected);
                }
                catch(final ElogException ex) {
                    final String errMsg = "Failed enabling/disabling the e-log interface: " + ex.getMessage();
                    IguiLogger.error(errMsg);
                    ErrorFrame.showError(errMsg);

                    source.setSelected(!selected);
                }
            }
        });

        this.controlMenuItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                // Try to change the access level to CONTROL only if the source of the event is selected and
                // the current access level is different than CONTROL
                final JRadioButtonMenuItem source = (JRadioButtonMenuItem) e.getSource();
                if(source.isSelected() && !(Igui.instance().getAccessLevel().equals(IguiConstants.AccessControlStatus.CONTROL))) {
                    IguiFrame.this.bAction.changeAccessLevel(IguiConstants.AccessControlStatus.CONTROL);
                }
            }
        });

        this.displayMenuItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                // Try to change the access level to DISPLAY only if the source of the event is selected and
                // the current access level is different than DISPLAY
                final JRadioButtonMenuItem source = (JRadioButtonMenuItem) e.getSource();
                if(source.isSelected() && !(Igui.instance().getAccessLevel().equals(IguiConstants.AccessControlStatus.DISPLAY))) {
                    IguiFrame.this.bAction.changeAccessLevel(IguiConstants.AccessControlStatus.DISPLAY);
                }
            }
        });

        this.closeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.exitIgui(false);
            }

        });
        
        this.exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.exitIgui(true);
            }

        });

        this.commitAndReloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.bAction.commitAndReload();
            }
        });

        this.errorItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                final JRadioButtonMenuItem source = (JRadioButtonMenuItem) e.getSource();
                if(source.isSelected()) {
                    IguiLogger.instance().setDebugLevel(org.apache.log4j.Level.ERROR);
                }
            }
        });

        this.warningItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                final JRadioButtonMenuItem source = (JRadioButtonMenuItem) e.getSource();
                if(source.isSelected()) {
                    IguiLogger.instance().setDebugLevel(org.apache.log4j.Level.WARN);
                }
            }
        });

        this.infoItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                final JRadioButtonMenuItem source = (JRadioButtonMenuItem) e.getSource();
                if(source.isSelected()) {
                    IguiLogger.instance().setDebugLevel(org.apache.log4j.Level.INFO);
                }
            }
        });

        this.debugItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                final JRadioButtonMenuItem source = (JRadioButtonMenuItem) e.getSource();
                if(source.isSelected()) {
                    IguiLogger.instance().setDebugLevel(org.apache.log4j.Level.DEBUG);
                }
            }
        });

        this.startERSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.bAction.startERS();
            }
        });

        this.startISButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.bAction.startIS();
            }
        });

        this.startDVSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    IguiFrame.this.bAction.startDVS();
                }
                catch(final IguiException.ConfigException ex) {
                    final String errMsg = "DVS GUI cannot be started: " + ex;
                    IguiLogger.error(errMsg, ex);
                    ErrorFrame.showError("IGUI Error", errMsg, ex);
                }
            }
        });

        this.startEVButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.bAction.startEV();
            }
        });

        this.startDBEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.bAction.startDBE();
            }
        });

        this.startLMButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.bAction.startLM();
            }
        });

        this.startOHButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                IguiFrame.this.bAction.startOH();
            }
        });
    }

    /**
     * It creates and initializes all the SwingComponents.
     * <p>
     * To be executed in the EDT.
     */
    private void initGUI() {
        assert javax.swing.SwingUtilities.isEventDispatchThread() : "EDT violation";

        this.tabPanel.setPreferredSize(new java.awt.Dimension(700, 450));
        this.tabPanel.setMinimumSize(new Dimension(20, 20));
        this.tabPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, (Icon) null));
        this.tabPanel.setOpaque(true);

        // Create the menu bar
        final JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        menuBar.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
        menuBar.setEnabled(true);
        menuBar.setDoubleBuffered(true);

        // Add the "File" menu to the menu bar
        final JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);

        // Add the "Commands" menu to the menu bar
        final JMenu cmdMenu = new JMenu("Commands");
        this.refreshISMenuItem.setText("Re-subscribe to IS servers");
        cmdMenu.add(this.refreshISMenuItem);
        menuBar.add(cmdMenu);

        // Create and setup the "Access Control" menu
        // START >> accessControlMenu
        final JMenu accessControlMenu = new JMenu();
        menuBar.add(accessControlMenu);
        accessControlMenu.setText("Access Control");
        // START >> controlMenuItem
        accessControlMenu.add(this.controlMenuItem);
        this.controlMenuItem.setText("Control");
        final ButtonGroup accessControlButtonGroup = new ButtonGroup();
        accessControlButtonGroup.add(this.controlMenuItem);
        // END << controlMenuItem
        // START >> displayMenuItem
        accessControlMenu.add(this.displayMenuItem);
        this.displayMenuItem.setText("Display");
        accessControlButtonGroup.add(this.displayMenuItem);
        // END << displayMenuItem
        // END << accessControlMenu
        // START >> closeMenuItem
        fileMenu.add(this.closeMenuItem);
        this.closeMenuItem.setText("Close IGUI");
        this.closeMenuItem.setToolTipText("The IGUI will be closed leaving the partition untouched and up");
        this.closeMenuItem.setIcon(Igui.createIcon(IguiFrame.closeMenuItemIconName));
        // END << closeMenuItem

        // START >> exitMenuItem
        fileMenu.add(this.exitMenuItem);
        this.exitMenuItem.setText(IguiFrame.closeAndExitLabel);
        this.exitMenuItem.setIcon(Igui.createIcon(IguiFrame.exitMenuItemIconName));
        this.exitMenuItem.setToolTipText("The full partition will be terminated and the IGUI closed");
        // END << exitMenuItem
        
        // Setup the "Settings" menu
        final JMenu settingsMenu = new JMenu();
        menuBar.add(settingsMenu);
        settingsMenu.setText("Settings");
        settingsMenu.add(this.elogMenuItem);
        this.elogMenuItem.setText("Enable ELisA interface");
        this.elogMenuItem.setSelected(Igui.instance().isElogEnvDefined());
        settingsMenu.add(this.rdbAuthMenuItem);
        this.rdbAuthMenuItem.setText("RDB Authentication");

        // Create and setup the "Logging Level" menu
        final JMenu loggingLevelMenu = new JMenu("Logging Level");
        loggingLevelMenu.add(this.errorItem);
        loggingLevelMenu.add(this.warningItem);
        loggingLevelMenu.add(this.infoItem);
        loggingLevelMenu.add(this.debugItem);
        menuBar.add(loggingLevelMenu);
        final ButtonGroup dbgButtonGroup = new ButtonGroup();
        dbgButtonGroup.add(this.errorItem);
        dbgButtonGroup.add(this.warningItem);
        dbgButtonGroup.add(this.infoItem);
        dbgButtonGroup.add(this.debugItem);

        // Create the 'Help' menu
        final JMenu helpMenu = new JMenu("Help");
        helpMenu.add(this.helpMenuItem);
        helpMenu.add(this.aboutMenuItem);
        menuBar.add(helpMenu);

        // Create the main split panel (NORTH, SOUTH)
        final JSplitPane mainSplitPane = new JSplitPane();
        this.getContentPane().add(mainSplitPane, BorderLayout.CENTER);
        mainSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        mainSplitPane.setSize(new java.awt.Dimension(1000, 800));
        mainSplitPane.setResizeWeight(0.15);
        mainSplitPane.setBorder(BorderFactory.createCompoundBorder(null, null));
        mainSplitPane.setDoubleBuffered(true);
        mainSplitPane.setContinuousLayout(false);
        mainSplitPane.setOneTouchExpandable(true);
        mainSplitPane.add(this.panelLayer, JSplitPane.TOP);

        // Create the horizontal split pane which will contain the main panel and the tabbed panels
        final JSplitPane horSplitPane = new JSplitPane();
        this.panelLayer.setView(horSplitPane);
        horSplitPane.setResizeWeight(0.15);
        horSplitPane.setDividerSize(5);
        horSplitPane.setDoubleBuffered(true);
        horSplitPane.add(this.tabPanel, JSplitPane.RIGHT);

        this.tabPanel.setDoubleBuffered(true);

        // Add the main panel
        final JScrollPane mainCommandsScrollPane = new JScrollPane();
        mainCommandsScrollPane.setPreferredSize(new java.awt.Dimension(300, 450));
        mainCommandsScrollPane.setDoubleBuffered(true);
        final MainPanel mp = this.mainPanelRef.get();
        if(mp != null) {
            mainCommandsScrollPane.setViewportView(mp);
            mp.setPreferredSize(new Dimension(290, 450));
            mp.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 0, 0)));
            // mp.setBackground(new java.awt.Color(255, 255, 255));
        } else {
            mainCommandsScrollPane.setViewportView(new JPanel(true));
        }
        horSplitPane.add(mainCommandsScrollPane, JSplitPane.LEFT);
        horSplitPane.setDividerLocation(-1);

        // Add the ers log panel
        final ErsPanel ersp = this.ersPanelRef.get();
        if(ersp != null) {
            ersp.setPreferredSize(new java.awt.Dimension(1000, 350));
            ersp.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
            // ersp.setBackground(new java.awt.Color(255, 255, 255));
            mainSplitPane.add(ersp, JSplitPane.BOTTOM);
        } else {
            mainSplitPane.add(new JPanel(true), JSplitPane.BOTTOM);
        }
        mainSplitPane.setDividerLocation(0.6);

        // Create the top toolbar which will contain action buttons
        final JToolBar toolBar = new JToolBar();
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        toolBar.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
        toolBar.setDoubleBuffered(true);
        toolBar.setOpaque(true);

        // START >> commitAndReloadButton
        toolBar.add(this.commitAndReloadButton);
        this.commitAndReloadButton.setText("Commit & Reload");
        this.commitAndReloadButton.setOpaque(false);
        this.commitAndReloadButton.setIcon(Igui.createIcon(IguiFrame.reloadAndCommitIconName));
        // END << commitAndReloadButton

        // Add the button to load panels on demand
        final JideSplitButton jsb = new JideSplitButton("Load Panels");
        jsb.setAlwaysDropdown(true);
        jsb.setIcon(this.panelListIcon);
        jsb.setButtonStyle(ButtonStyle.TOOLBOX_STYLE);
        jsb.add(this.panelList);
        toolBar.add(jsb);
         
        // Add the progress bar for the dead time 
        toolBar.add(Box.createGlue());
        toolBar.add(new JLabel("Total dead-time (%)"));
        toolBar.add(Box.createHorizontalStrut(10));
        toolBar.add(this.deadTimeBar);
        this.deadTimeBar.setStringPainted(true);
        this.deadTimeBar.setValue(-1);
        this.deadTimeBar.setString("N/A");
        
        toolBar.addSeparator();        
        
        // Add button to launch utilities
        final JideSplitButton appSplitButton = new JideSplitButton("Utilities");
        appSplitButton.setIcon(this.utilsListIcon);
        appSplitButton.setAlwaysDropdown(true);
        appSplitButton.setButtonStyle(ButtonStyle.TOOLBOX_STYLE);
        appSplitButton.add(this.startERSButton);
        appSplitButton.add(this.startISButton);
        appSplitButton.add(this.startDVSButton);
        appSplitButton.add(this.startEVButton);
        appSplitButton.add(this.startDBEButton);
        appSplitButton.add(this.startLMButton);
        appSplitButton.add(this.startOHButton);
        
        toolBar.add(appSplitButton);
        
        // Set action for menu items and button
        this.setObjectActions();

        // Select the current logging level
        final org.apache.log4j.Level l = IguiLogger.instance().getDebugLevel();
        if(this.loggingLevelMap.containsKey(l)) {
            this.loggingLevelMap.get(l).setSelected(true);
        }
    }

}
