package Igui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;


/**
 * Help Window for IGUI
 * 
 * @author L. Moneta
 * @author J. Flammer
 */
class HelpFrame extends JEditorPane {

    private static final long serialVersionUID = -6054185170588301370L;

    private static final String homeButtonIconName = "home.png";
    private static final String backButtonIconName = "back.png";
    private static final String forwardButtonIconName = "forward.png";
    private static final String exitButtonIconName = "exit.png";
    private static final String closeItemMenuIconName = "exit_small.png";

    private final JFrame helpFrame = new JFrame();

    // Menu
    private final JMenuBar JMenuBar1 = new JMenuBar(); // Menu Bar
    private final JMenu fileMenu = new JMenu(); // File Menu
    private final JMenuItem closeMenuItem = new JMenuItem("Close", Igui.createIcon(HelpFrame.closeItemMenuIconName)); // File -> Close

    // Toolbar
    private final JToolBar toolBar = new JToolBar();

    // Back button
    private final JButton backButton = new JButton(Igui.createIcon(HelpFrame.backButtonIconName));

    // Forward button
    private final JButton forwardButton = new JButton(Igui.createIcon(HelpFrame.forwardButtonIconName));

    // Home button
    private final JButton homeButton = new JButton(Igui.createIcon(HelpFrame.homeButtonIconName));

    // Exit button
    private final JButton exitButton = new JButton(Igui.createIcon(HelpFrame.exitButtonIconName));

    // Simple border
    private final Border emptyBorder = BorderFactory.createEmptyBorder(2, 2, 2, 2);

    // Online Help directory / address
    private final String helpUrl;

    private final JEditorPane ep = new JEditorPane();
    private final List<URL> PageList = new ArrayList<URL>();
    private int icurrent = 0;

    /**
     * Help Window Constructor.
     */

    HelpFrame(final String helpUrl) throws IOException {
        this.helpUrl = helpUrl;

        // Build the gui
        this.initGUI();

        IguiLogger.info("Load online help from \n " + helpUrl);

        // try {
        this.ep.setPage(helpUrl);
        // }
        // catch(final IOException e) {
        // final String errorMsg = "Failed to read HTML page - page is " + helpUrl;
        // IguiLogger.error(errorMsg, e);
        // ErrorFrame.showError(errorMsg);
        // }

        this.homeButton.setEnabled(true);
        this.forwardButton.setEnabled(false);
        this.backButton.setEnabled(false);

        this.icurrent = 0;
        this.PageList.add(this.ep.getPage());

        // -----------------------------
        // add hyperlink events
        // -----------------------------

        this.ep.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(final HyperlinkEvent evt) {
                // check event type (if is activated i.e. mouse clicked)
                if(evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    final URL thisPage = HelpFrame.this.ep.getPage();
                    final Cursor oldCursor = HelpFrame.this.helpFrame.getCursor();
                    try {
                        HelpFrame.this.helpFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                        // switch to the new page
                        HelpFrame.this.ep.setPage(evt.getURL().toURI().toURL());
                        HelpFrame.this.icurrent++;
                        final int pageSize = HelpFrame.this.PageList.size();
                        if(HelpFrame.this.icurrent < pageSize) {
                            for(int j = HelpFrame.this.icurrent; j < pageSize; j++) {
                                HelpFrame.this.PageList.get(HelpFrame.this.icurrent);
                            }
                        }
                        HelpFrame.this.PageList.add(HelpFrame.this.ep.getPage());
                        HelpFrame.this.backButton.setEnabled(true);
                        HelpFrame.this.forwardButton.setEnabled(false);

                    }
                    catch(final Exception e) {
                        IguiLogger.error("Page switch failed", e);
                        try {
                            HelpFrame.this.ep.setPage(thisPage);
                        }
                        catch(final IOException e2) {
                            IguiLogger.error("Failed to revert to old page", e2);
                        }
                    }
                    finally {
                        HelpFrame.this.helpFrame.setCursor(oldCursor);
                    }
                }
            }
        });

    }

    public void makeVisible() {
        final JFrame mainFrame = Igui.instance().getMainFrame();
        if(mainFrame == null) {
            // center to window
            final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            final Dimension size = this.helpFrame.getSize();
            screenSize.height = screenSize.height / 2;
            screenSize.width = screenSize.width / 2;
            size.height = size.height / 2;
            size.width = size.width / 2;
            final int y = screenSize.height - size.height;
            final int x = screenSize.width - size.width;
            this.helpFrame.setLocation(x, y);
        } else {
            this.helpFrame.setLocationRelativeTo(Igui.instance().getMainFrame());
        }

        this.helpFrame.setVisible(true);
    }

    /**
     * Method to change to new URL page.
     * 
     * @param page New URL
     * @return error code (0=ok)
     */
    private int changePage(final URL page) {
        int iret = 0;
        final URL thisPage = this.ep.getPage();
        final Cursor oldCursor = this.helpFrame.getCursor();
        try {
            this.helpFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            // switch to the new page
            this.ep.setPage(page);
        }
        catch(final IOException e) {
            IguiLogger.error("Page switch failed", e);
            iret = -1;
            try {
                this.ep.setPage(thisPage);
            }
            catch(final IOException e2) {
                IguiLogger.error("Failed to revert old page", e2);
                iret = -2;
            }
        }
        finally {
            this.helpFrame.setCursor(oldCursor);
        }

        return iret;
    }

    /**
     * Exit Help Window.
     */
    private void exit() {
        this.helpFrame.dispose();
    }

    /**
     * Method to go backwards in URL history.
     */
    private void goBack() {
        final URL oldPage = this.PageList.get(this.icurrent - 1);
        final int ierr = this.changePage(oldPage);
        if(ierr == 0) {
            this.icurrent--;
            if(this.icurrent == 0) {
                this.backButton.setEnabled(false);
                this.backButton.setBorder(this.emptyBorder);
            }
            this.forwardButton.setEnabled(true);
        }
    }

    /**
     * Method to go forward in URL history.
     */
    private void goForward() {
        final URL newPage = this.PageList.get(this.icurrent + 1);
        final int ierr = this.changePage(newPage);
        if(ierr == 0) {
            this.icurrent++;
            if((this.icurrent + 1) == this.PageList.size()) {
                this.forwardButton.setBorder(this.emptyBorder);
                this.forwardButton.setEnabled(false);
            }
            this.backButton.setEnabled(true);
        }
    }

    /**
     * Method to go to home.
     */
    private void goHome() {
        try {
            final URL homePage = new URL(this.helpUrl);
            final int ierr = this.changePage(homePage);
            if(ierr == 0) {
                this.icurrent++;

                if((this.icurrent + 1) == this.PageList.size()) {
                    this.forwardButton.setBorder(this.emptyBorder);
                    this.forwardButton.setEnabled(false);
                } else {
                    this.forwardButton.setEnabled(true);

                }
                if(this.icurrent == 0) {
                    this.backButton.setBorder(this.emptyBorder);
                    this.backButton.setEnabled(false);
                } else {
                    this.backButton.setEnabled(true);
                }
            }
        }
        catch(final Exception ex) {
            IguiLogger.error("Home page incorrect: " + this.helpUrl + " .Error occured " + ex.getMessage(), ex);
        }

    }

    private void initGUI() {
        // -----------------------------
        // Set up new Editor Panel
        // ------------------------------
        this.ep.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.ep.setEditable(false);

        // ------------------------------
        // Set up Frame with Panel and Scroll Panel
        // ------------------------------
        this.helpFrame.setTitle("Help for ATLAS TDAQ Software IGUI");

        final JRootPane rootPane1 = this.helpFrame.getRootPane();
        final JPanel contentPane1 = (JPanel) rootPane1.getContentPane();

        final JScrollPane scrollPanel = new JScrollPane(this.ep);
        scrollPanel.setPreferredSize(new java.awt.Dimension(820, 500));

        // ------------------------------
        // Set up Toolbar
        // ------------------------------
        this.toolBar.setFloatable(false);
        this.toolBar.add(this.homeButton);
        this.toolBar.add(this.backButton);
        this.toolBar.add(this.forwardButton);
        this.toolBar.add(Box.createHorizontalGlue());
        this.toolBar.add(this.exitButton);

        // home button
        this.homeButton.setFocusPainted(false);
        this.homeButton.setBorder(this.emptyBorder);
        this.homeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                HelpFrame.this.goHome();
            }
        });

        // Back Button
        this.backButton.setToolTipText("Back");
        this.backButton.setFocusPainted(false);
        this.backButton.setBorder(this.emptyBorder);
        this.backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                HelpFrame.this.goBack();
            }
        });

        // Forward Button
        this.forwardButton.setToolTipText("Forward");
        this.forwardButton.setFocusPainted(false);
        this.forwardButton.setBorder(this.emptyBorder);
        this.forwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                HelpFrame.this.goForward();
            }
        });

        // Exit Button
        this.exitButton.setToolTipText("Exit online help");
        this.exitButton.setFocusPainted(false);
        this.exitButton.setBorder(this.emptyBorder);
        this.exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                HelpFrame.this.exit();
            }
        });

        contentPane1.add(scrollPanel, BorderLayout.CENTER);
        contentPane1.add(this.toolBar, BorderLayout.NORTH);

        // ------------------------------
        // Setup Menu bar
        // ------------------------------
        this.fileMenu.setText("File");

        this.closeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                HelpFrame.this.exit();
            }
        });

        this.fileMenu.add(this.closeMenuItem);

        this.JMenuBar1.add(this.fileMenu);

        this.helpFrame.setJMenuBar(this.JMenuBar1);
        this.helpFrame.pack();
    }

}
