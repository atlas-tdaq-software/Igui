package Igui;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import Igui.IguiException.ConfigException;
import Igui.Common.ERSContext;
import Igui.Common.ERSFilterCriteria;
import Igui.Common.ERSFilterCriteria_Helper;
import Igui.Common.ERSParameter;

import com.jidesoft.utils.DefaultWildcardSupport;
import com.jidesoft.utils.WildcardSupport;

import config.Configuration;
import config.Query;


/**
 * This class takes care of loading the default ERS filter used by the IGUI and the ERS monitor.
 * <p>
 * The filters to apply are described in an OKS database as objects of the {@link ERSFilterCriteria} class. That class is described in the
 * "Igui-ers.schema.xml" file; a copy of this file (with an example database) is available in the "Igui/data" directory.
 * <p>
 * All the valid filters (i.e., the ones referring to the specified partition, matching the user profile and within the interval of
 * validity) are put together in a logical OR and then the whole set is negated. In this way the ERS subscription string returned by
 * {@link #getSubscriptionString(String)} will act as a filter, letting the ERS client to not receive any message matching the defined
 * filters. Of course the single criteria parameters are put in a logical AND. The final result is something like: <br>
 * not (((filter1_param1) and ... and (filter1_paramN)) or ((filter2_param1) and ... and (filter2_paramN)) or ... or ((filterM_param1) and
 * ... and (filterM_paramN)))
 * <p>
 * The current user profiles should be defined in the "igui.user.profile" system property. This class expects the user profiles listed as a
 * single string without any blank space and separated by the separator returned by {@link #getProfileSeparator()}. If that property is not
 * defined then the filter will be considered as valid.
 * <p>
 * For the filter to be valid is mandatory to define the Message Type or the Application Name.
 * <p>
 * This class is thread safe.
 */

class ErsFilterLoader {
    /**
     * The "oksconfig" plug-in is used to load the database
     */
    private final static String DB_IMPLEMENTATION = "oksconfig";

    /**
     * Separator used for user profiles
     */
    private final static String PROFILE_SEPARATOR = ",";

    /**
     * Utility class to deal with wild-cards (allowed in the filter parameters)
     */
    private final WildcardSupport wildCard = new DefaultWildcardSupport();

    /**
     * Full path to the database file
     */
    private final String filePath;

    /**
     * Map caching the filters for each partition
     */
    private final ConcurrentMap<String, String> filterMap = new ConcurrentHashMap<String, String>();

    /**
     * Read/write lock used to synchronize cache access and its invalidation
     */
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

    /**
     * The Configuration object giving access to the OKS database
     */
    // This has to be volatile to grant thread safety!
    volatile Configuration config = null;

    /**
     * Constructor.
     * 
     * @param filePath Full path to the OKS database file.
     */
    ErsFilterLoader(final String filePath) {
        this.filePath = filePath;
    }

    /**
     * It returns the separator used for the user profiles.
     * 
     * @return The separator used for the user profiles
     */
    public static String getProfileSeparator() {
        return ErsFilterLoader.PROFILE_SEPARATOR;
    }

    /**
     * It returns the full path to the OKS database file.
     * 
     * @return The full path to the OKS database file
     */
    public String getFile() {
        return this.filePath;
    }

    /**
     * Calling this method invalidates the cache.
     * <p>
     * At the next call of the {@link #getSubscriptionString(String)} method, the cache is cleared and the Configuration object is
     * re-created.
     * <p>
     * This method should be used, for instance, when the content of the database file is changed and access to the new data is required.
     * <p>
     * This method does not block.
     */
    public void invalidateFilter() {
        // No need to protect with locks: this would not work if not volatile!
        this.config = null;
    }

    /**
     * This method returns the subscription string in order to apply the filters defined in the OKS database.
     * <p>
     * Even if it caches the subscription string per partition, it may imply reading information from the physical file (possibly time
     * consuming if used in GUI).
     * 
     * @param partName The name of the partition the subscription string should be built for
     * @return The subscription string in order to apply the filters defined in the OKS database
     * @throws ConfigException Exception thrown for any error encountered reading the OKS database
     */
    public String getSubscriptionString(final String partName) throws ConfigException {
        // In principle this method maybe called from different threads and for different partitions
        // The read lock is acquired by every caller (so that multiple concurrent readers are allowed) but
        // the write lock may be acquired when the configuration object is (re)created or the cache is cleared
        this.rwl.readLock().lock();
        try {
            // Check if a valid configuration exists; remember that the config object is volatile and may be
            // nullified at every moment by calls to the "invalidateFilter" method
            Configuration conf = this.config;
            if(conf == null) {
                // It looks like it does not exist: then acquire the write lock
                this.rwl.readLock().unlock();
                this.rwl.writeLock().lock();
                try {
                    // We need to check again the configuration: between the release/acquire of the read/write locks, a different thread
                    // may already have created the configuration. Here we have exclusive access; if the configuration is not valid
                    // then we can create it, otherwise we do nothing. Just a simple double check idiom (and this.config MUST be volatile).
                    conf = this.config;
                    if(conf == null) {
                        // Create the configuration and clear the cache
                        // Please, note the "&norepo" suffix: that is needed to permit a proper filter reload when OKS-GIT is used
                        this.config = conf = new Configuration(ErsFilterLoader.DB_IMPLEMENTATION + ":" + this.filePath + "&norepo");
                        this.filterMap.clear();
                    }
                }
                finally {
                    this.rwl.readLock().lock();
                    this.rwl.writeLock().unlock();
                }
            }

            // From this point on, the Configuration object MUST be accesses through the local variable and not through the class member:
            // a different thread may set that reference to null to invalidate it. This is valid because the reference to
            // the Configuration object is volatile.

            String filterString = this.filterMap.get(partName);
            // Here we do not lock. The map used as a cache is thread safe and allows concurrent readers and writers (and this may
            // happen because we lock only a read lock at the beginning of this method). The consistency of the map is not at risk,
            // but not locking may cause concurrent clients to build the subscription string at the same time. Given the low
            // level of concurrency expected this should not be an issue.
            if(filterString == null) {
                // Subscription string not found in the cache, let's build it!
                final StringBuilder strBuff = new StringBuilder();

                final String userProfile = System.getProperty(IguiConstants.IGUI_USER_PROFILE, "");
                final Date currentDate = new Date();
                final DateFormat df = new SimpleDateFormat("yyyy-MMM-dd"); // This is the date format used by OKS

                final ERSFilterCriteria[] allSubs = ERSFilterCriteria_Helper.get(conf, new Query());
                for(final ERSFilterCriteria sub : allSubs) {
                    // First check if the criteria is enabled
                    if(sub.get_Enabled() == true) {
                        try {
                            // Check if the criteria matches the current partition, the current user profile and the current date
                            if((this.dateMatches(sub, currentDate, df) == true) && (this.partitionMatches(sub, partName) == true)
                               && (this.profileMatches(sub, userProfile) == true))
                            {
                                // Build the string for this criteria
                                final String criteria = this.buildFilterString(sub);
                                if(criteria.isEmpty() == false) {
                                    // If the buffer already contains some criteria, then put them in OR
                                    if(strBuff.length() != 0) {
                                        strBuff.append(" or ");
                                    }

                                    IguiLogger.debug("Found ERS filter \"" + sub.UID() + "\": " + criteria);

                                    // Enclosing the single criteria in parentheses is much more safe
                                    strBuff.append("(");
                                    strBuff.append(criteria);
                                    strBuff.append(")");
                                }
                            }
                        }
                        catch(final Exception ex) {
                            final String errMsg = "Failed retrieving the ERS filter \"" + sub.UID() + "\" from the database: " + ex;
                            IguiLogger.error(errMsg, ex);
                        }
                    } else {
                        IguiLogger.info("ERS filter \"" + sub.UID() + "\" is disabled and will be discarded");
                    }
                }

                // Negate the full expression
                if(strBuff.length() > 0) {
                    strBuff.insert(0, " not (");
                    strBuff.append(")");
                }

                // Write to the local variable to return
                filterString = strBuff.toString();

                // Cache the string: that is added to the map only if not present yet
                this.filterMap.putIfAbsent(partName, filterString);
            }

            return filterString;
        }
        catch(final Exception ex) {
            final String errMsg = "Error retrieving the default ERS filters from the database: " + ex;
            throw new IguiException.ConfigException(errMsg, ex);
        }
        finally {
            this.rwl.readLock().unlock();
        }
    }

    /**
     * It builds the subscription string for the specified ERS filter criteria.
     * <p>
     * For the string to be successfully built, both the message type and the application name have to be defined.
     * 
     * @param sub The ERS filter criteria the string should be built upon
     * @return The subscription string for the specified ERS filter criteria
     * @throws ConfigException Some error occurred accessing the OKS data
     */
    private String buildFilterString(final ERSFilterCriteria sub) throws ConfigException {
        final StringBuilder strBuff = new StringBuilder();

        try {
            final String applicationName = sub.get_ApplicationName().trim();
            final String msgType = sub.get_MessageType().trim();
            if((applicationName.isEmpty() == true) && (msgType.isEmpty() == true)) {
                // Do not build the string for this criteria if both the message type and
                // the application name are not defined
                IguiLogger.warning("The ERS filter \"" + sub.UID()
                                   + "\" does not define an application name or a message type; it will be discarded");
            } else {
                if(applicationName.isEmpty() == false) {
                    strBuff.append("(app=");
                    strBuff.append(applicationName);
                    strBuff.append(")");
                }

                if(msgType.isEmpty() == false) {
                    if(strBuff.length() != 0) {
                        strBuff.append(" and ");
                    }

                    strBuff.append("(msg=");
                    strBuff.append(msgType);
                    strBuff.append(")");
                }

                {
                    final String[] severity = sub.get_Severity();
                    if(severity.length > 0) {
                        strBuff.append(" and (");
                        for(int i = 0; i < severity.length; ++i) {
                            strBuff.append("sev=");
                            strBuff.append(severity[i]);
                            if(i != (severity.length - 1)) {
                                strBuff.append(" or ");
                            }
                        }
                        strBuff.append(")");
                    }
                }

                {
                    final String[] qualifiers = sub.get_Qualifiers();

                    if(qualifiers.length != 0) {
                        // Trim the qualifiers and take into account only the not empty ones
                        final List<String> trimmedQualifiers = new ArrayList<String>();
                        for(final String q : qualifiers) {
                            final String tq = q.trim();
                            if(tq.isEmpty() == false) {
                                trimmedQualifiers.add(tq);
                            }
                        }

                        if(trimmedQualifiers.isEmpty() == false) {
                            final int size = trimmedQualifiers.size();
                            strBuff.append(" and (");
                            for(int i = 0; i < size; ++i) {
                                strBuff.append("qual=");
                                strBuff.append(trimmedQualifiers.get(i));
                                if(i != (size - 1)) {
                                    strBuff.append(" and ");
                                }
                            }
                            strBuff.append(")");
                        }
                    }
                }

                {
                    final ERSContext[] ctxs = sub.get_Contexts();
                    if(ctxs.length != 0) {
                        final List<ERSContext> ctxSanitize = new ArrayList<>();
                        for(final ERSContext ctx : ctxs) {
                            final String context = ctx.get_Context().trim();
                            final String value = ctx.get_Value().trim();
                            if((context.isEmpty() == false) && (value.isEmpty() == false)) {
                                ctxSanitize.add(ctx);
                            } else {
                                final String msg = "Cannot add the \"" + ctx.UID() + "\" context to the \"" + sub.UID()
                                                   + "\" filter because either the context name or its value is empty";
                                IguiLogger.warning(msg);
                            }
                        }

                        if(ctxSanitize.isEmpty() == false) {
                            final int size = ctxSanitize.size();
                            strBuff.append(" and (");
                            int i = 0;
                            for(final ERSContext ctx : ctxSanitize) {
                                strBuff.append("context(");
                                strBuff.append(ctx.get_Context().trim());
                                strBuff.append(ctx.get_Equal() ? ")=" : ")!=");
                                strBuff.append(ctx.get_Value().trim());

                                if(i++ != (size - 1)) {
                                    strBuff.append(" and ");
                                }
                            }
                            strBuff.append(")");
                        }
                    }
                }

                {
                    final ERSParameter[] params = sub.get_Parameters();
                    if(params.length != 0) {
                        final List<ERSParameter> paramsSanitize = new ArrayList<>();
                        for(final ERSParameter param : params) {
                            final String name = param.get_Parameter().trim();
                            final String value = param.get_Value().trim();
                            if((name.isEmpty() == false) && (value.isEmpty() == false)) {
                                paramsSanitize.add(param);
                            } else {
                                final String msg = "Cannot add the \"" + param.UID() + "\" parameter to the \"" + sub.UID()
                                                   + "\" filter because either the parameter name or its value is empty";
                                IguiLogger.warning(msg);
                            }
                        }

                        if(paramsSanitize.isEmpty() == false) {
                            final int size = paramsSanitize.size();
                            strBuff.append(" and (");
                            int i = 0;
                            for(final ERSParameter param : paramsSanitize) {
                                strBuff.append("param(");
                                strBuff.append(param.get_Parameter().trim());
                                strBuff.append(param.get_Equal() ? ")=" : ")!=");
                                strBuff.append(param.get_Value().trim());

                                if(i++ != (size - 1)) {
                                    strBuff.append(" and ");
                                }
                            }
                            strBuff.append(")");
                        }
                    }
                }
            }
        }
        catch(final Exception ex) {
            final String errMsg = "Error getting filter attributes: " + ex;
            throw new IguiException.ConfigException(errMsg, ex);
        }

        return strBuff.toString();
    }

    /**
     * It checks if the given ERS filter criteria is valid for the given partition.
     * 
     * @param sub The ERS filter criteria to check
     * @param currentPartitionName The partition name the filter should be valid for
     * @return <code>TRUE</code> if the filter is valid for the specified partition
     * @throws ConfigException Some error occurred accessing the OKS data
     */
    private boolean partitionMatches(final ERSFilterCriteria sub, final String currentPartitionName) throws ConfigException {
        boolean partitionMatches = false;

        try {
            final String[] partitions = sub.get_Partition();
            for(final String part : partitions) {
                final String trimmedPartition = part.trim();
                try {
                    partitionMatches = Pattern.matches(this.wildCard.convert(trimmedPartition), currentPartitionName);
                }
                catch(final PatternSyntaxException ex) {
                    final String errMsg = "Error parsing the regular expression for one of the \"Partition\" attributes of the \""
                                          + sub.UID() + "\" ERS filter: " + ex;
                    IguiLogger.warning(errMsg, ex);
                }

                if(partitionMatches == true) {
                    break;
                }
            }

            if(partitionMatches == false) {
                IguiLogger.info("ERS filter \"" + sub.UID()
                                + "\" is not meant for the current partition and will be discarded (valid partitions are "
                                + Arrays.toString(partitions) + ")");
            }
        }
        catch(final Exception ex) {
            // We are here for one of the following reasons: some exception is thrown by Config trying
            // to access the 'sub' attributes or the 'get_Partition' method returns a null array (which is not fair at all)
            final String errMsg = "Unexpected error retrieving the \"Partition\" attribute list: " + ex;
            throw new IguiException.ConfigException(errMsg, ex);
        }

        return partitionMatches;
    }

    /**
     * It checks if the given ERS filter criteria is valid for the specified user profile.
     * <p>
     * If the user profile is not defined (i.e., the passed string is empty) then this method will return <code>true</code>.
     * 
     * @param sub The ERS filter criteria to check
     * @param userProfile The user profile
     * @return <code>TRUE</code> if the filter is valid for the specified user profile
     * @throws ConfigException Some error occurred accessing the OKS data
     */
    private boolean profileMatches(final ERSFilterCriteria sub, final String userProfile) throws ConfigException {
        boolean profileMatches = true;

        final String trimmedUserProfile = userProfile.trim();
        if(trimmedUserProfile.isEmpty() == false) {
            // Add separators to the beginning and the end of the string to make life easier
            // with the regular expressions. At the end we have a pattern like ,XXXX,XXXX,XXXX,...,
            final StringBuilder userProfileMod = new StringBuilder();
            userProfileMod.append(ErsFilterLoader.PROFILE_SEPARATOR);
            userProfileMod.append(trimmedUserProfile);
            userProfileMod.append(ErsFilterLoader.PROFILE_SEPARATOR);

            try {
                final String[] profiles = sub.get_ExcludedProfiles();
                for(final String prof : profiles) {
                    final String trimmedProf = prof.trim();
                    if(trimmedProf.isEmpty() == false) {
                        try {
                            final StringBuilder matcherString = new StringBuilder();
                            matcherString.append(".*");
                            matcherString.append(ErsFilterLoader.PROFILE_SEPARATOR);
                            matcherString.append("{1}");
                            matcherString.append(this.wildCard.convert(trimmedProf));
                            matcherString.append(ErsFilterLoader.PROFILE_SEPARATOR);
                            matcherString.append("{1}");
                            matcherString.append(".*");

                            final Pattern patt = Pattern.compile(matcherString.toString(), Pattern.CASE_INSENSITIVE);
                            profileMatches = !patt.matcher(userProfileMod.toString()).matches();
                        }
                        catch(final PatternSyntaxException ex) {
                            final String errMsg = "Error parsing the regular expression for one of the \"Profile\" attributes of the \""
                                                  + sub.UID() + "\" ERS filter: " + ex;
                            IguiLogger.warning(errMsg, ex);
                        }

                        if(profileMatches == false) {
                            break;
                        }
                    }
                }

                if(profileMatches == false) {
                    IguiLogger.info("ERS filter \"" + sub.UID()
                                    + "\" is discarded because the current profile is in the list of profiles to exclude (excluded profiles are "
                                    + Arrays.toString(profiles) + ")");
                }
            }
            catch(final Exception ex) {
                // We are here for one of the following reasons: some exception is thrown by Config trying
                // to access the 'sub' attributes or the 'get_Profile' method returns a null array (which is not fair at all)
                final String errMsg = "Unexpected error retrieving the \"Profile\" attribute list: " + ex;
                throw new IguiException.ConfigException(errMsg, ex);

            }
        }

        return profileMatches;
    }

    /**
     * It checks if the given ERS filter criteria is within the interval of validity
     * 
     * @param sub The ERS filter criteria to check
     * @param reference The date to be used as a reference
     * @param df The data format to be used to read the data attributes of the ERS filter criteria in OKS
     * @return <code>TRUE</code> if the filter criteria is within the interval of validity
     * @throws ConfigException Some error occurred accessing the OKS data
     */
    private boolean dateMatches(final ERSFilterCriteria sub, final Date reference, final DateFormat df) throws ConfigException {
        boolean dateMatches = false;

        try {
            final Date fromDate = df.parse(sub.get_From());
            final Date toDate = df.parse(sub.get_To());

            if((reference.compareTo(fromDate) >= 0) && (reference.compareTo(toDate) <= 0)) {
                dateMatches = true;
            } else {
                IguiLogger.info("ERS filter \"" + sub.UID()
                                + "\" is not within the time validity period and will be discarded (valid time period is between "
                                + fromDate.toString() + " and " + toDate.toString() + ")");
            }
        }
        catch(final ParseException ex) {
            final String errMsg = "Error parsing the time validity interval: " + ex;
            throw new IguiException.ConfigException(errMsg, ex);
        }
        catch(final Exception ex) {
            // We are here for one of the following reasons: some exception is thrown by Config trying
            // to access the 'sub' attributes or the 'get_From' or 'get_To' method returns a null string (which is not fair at all)
            final String errMsg = "Unexpected error retrieving the time validity interval: " + ex;
            throw new IguiException.ConfigException(errMsg, ex);
        }

        return dateMatches;
    }

    //    public static void main(String[] args) {
    //        try {
    //            final ErsFilterLoader sl = new ErsFilterLoader("/afs/cern.ch/work/a/avolio/working/HEAD/Igui/data/Igui-ers.data.xml");
    //            System.err.println(sl.getSubscriptionString("part"));
    //        }
    //        catch(ConfigException ex) {
    //            ex.printStackTrace();
    //        }
    //        catch(Exception ex) {
    //            ex.printStackTrace();
    //        }
    //    }
}
