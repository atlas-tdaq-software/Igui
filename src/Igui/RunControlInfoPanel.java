package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Arrays;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import com.jidesoft.utils.SwingWorker;

import Igui.Common.MultiLineTable;
import is.InfoDocument.Attribute;
import is.InfoNotFoundException;
import is.RepositoryNotFoundException;
import is.UnknownTypeException;


/**
 * Simple utility class to create and show a table reporting detailed information for a specific application that is part of the Run Control
 * tree. Information is retrieved from IS.
 */

class RunControlInfoPanel {
    private final RunControlApplicationData rcAppInfo;

    /**
     * Simple cell renderer for the table
     * <p>
     * NOTE: It MUST extend {@link MultiLineTable.MultiLineCellRenderer}
     */
    private final static class CellRenderer extends MultiLineTable.MultiLineCellRenderer {
        private static final long serialVersionUID = -6918186130321696372L;

        CellRenderer() {
            super();
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            switch(column) {
                case 0:
                    this.setFont(new Font("Dialog", Font.BOLD, 12));
                    break;
                case 2:
                    this.setFont(new Font("Dialog", Font.ITALIC, 12));
                    break;
                default:
                    this.setFont(new Font("Dialog", Font.PLAIN, 12));
            }

            if(row % 2 == 0) {
                this.setBackground(Color.LIGHT_GRAY);
            } else {
                this.setBackground(Color.WHITE);
            }

            return this;
        }
    }

    /**
     * Constructor.
     * 
     * @param rcAppInfo Data structure representing the application to retrieve information for
     */
    private RunControlInfoPanel(final RunControlApplicationData rcAppInfo) {
        this.rcAppInfo = rcAppInfo;
    }

    /**
     * In creates and shows a table reporting the application's information
     * <p>
     * Note: to be called in the EDT.
     * 
     * @param appInfo Data structure representing the application to retrieve information for
     */
    public static void createAndShow(final RunControlApplicationData appInfo) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        final RunControlInfoPanel infoPanel = new RunControlInfoPanel(appInfo);

        // Collect data in a background thread and then show the table in the EDT
        new SwingWorker<Vector<Vector<String>>, Void>() {
            @Override
            protected Vector<Vector<String>> doInBackground() throws Exception {
                // "collectData" may be expensive because it implies remote calls
                return infoPanel.collectData();
            }

            @Override
            protected void done() {
                try {
                    final Vector<Vector<String>> rows = this.get();

                    if(rows.isEmpty() == false) {
                        final Vector<String> colNames = new Vector<>();
                        colNames.add("Name");
                        colNames.add("Value");
                        colNames.add("Description");

                        final MultiLineTable t = new MultiLineTable(rows, colNames);

                        // General options
                        t.setIntercellSpacing(new Dimension(5, 15));
                        t.setShowGrid(false);
                        t.setShowHorizontalLines(false);
                        t.setColumnControlVisible(true);
                        t.setSortable(true);
                        t.setEditable(false);
                        t.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                        t.setCellSelectionEnabled(true);

                        // Tune column width
                        t.getColumnModel().getColumn(0).setMinWidth(95);
                        t.getColumnModel().getColumn(0).setPreferredWidth(100);

                        t.getColumnModel().getColumn(1).setMinWidth(180);
                        t.getColumnModel().getColumn(1).setPreferredWidth(180);

                        t.getColumnModel().getColumn(2).setMinWidth(120);
                        t.getColumnModel().getColumn(2).setPreferredWidth(120);

                        t.getColumnModel().getColumn(0).setCellRenderer(new CellRenderer());
                        t.getColumnModel().getColumn(1).setCellRenderer(new CellRenderer());
                        t.getColumnModel().getColumn(2).setCellRenderer(new CellRenderer());

                        final JScrollPane sp = new JScrollPane(t);
                        final JPanel p = new JPanel(new BorderLayout());
                        p.add(sp, BorderLayout.CENTER);

                        final JFrame frame = new JFrame("IGUI - Detailed Information for Application \"" + infoPanel.rcAppInfo.getName()
                                                        + "\"");
                        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        frame.setLayout(new BorderLayout());
                        frame.add(p, BorderLayout.CENTER);
                        frame.setSize(new Dimension(600, 600));
                        frame.setMinimumSize(new Dimension(600, 600));
                        frame.pack();
                        frame.setVisible(true);
                    } else {
                        ErrorFrame.showWarning("Impossible to get information about the selected application, try again!");
                    }
                }
                catch(final InterruptedException | ExecutionException ex) {
                    final String msg = "Some error occurred retrieving the information about the selected application";
                    ErrorFrame.showError("IGUI - Error", msg, ex);
                }
            }

        }.execute();
    }

    /**
     * It collects all the needed data about the concerned application
     * 
     * @return A vector of vector of strings (i.e., rows for the table). Every row contains - in order - the attribute name, the attribute
     *         value and the attribute description.
     */
    private Vector<Vector<String>> collectData() {
        final Vector<Vector<String>> rows = new Vector<>();
        final is.Repository isRepo = new is.Repository(Igui.instance().getPartition());

        final Consumer<String> getInfo = (final String infoname) -> {
            try {
                final is.AnyInfo rcInfo = new is.AnyInfo();
                isRepo.getValue(infoname, rcInfo);

                final is.InfoDocument isDoc = new is.InfoDocument(Igui.instance().getPartition(), rcInfo);
                for(int i = 0; i < isDoc.getAttributeCount(); ++i) {
                    final Attribute attr = isDoc.getAttribute(i);
                    final String attrName = attr.getName();
                    final String attrDescr = attr.getDescription();

                    final StringBuilder attrValue = new StringBuilder();
                    if(attr.isArray() == true) {
                        final Object[] a = (Object[]) rcInfo.getAttribute(i);
                        attrValue.append(Arrays.toString(a));
                    } else {
                        attrValue.append(rcInfo.getAttribute(i).toString());
                    }

                    final Vector<String> row = new Vector<>();
                    row.add(attrName);
                    row.add(attrValue.toString());
                    row.add(attrDescr);

                    rows.add(row);
                }
            }
            catch(final InfoNotFoundException ex) {
                final String errMsg = "Information \"" + infoname + "\" does not exist: " + ex;
                IguiLogger.debug(errMsg, ex);
            }
            catch(final RepositoryNotFoundException | UnknownTypeException ex) {
                final String errMsg = "Failed to get information \"" + infoname + "\": " + ex;
                IguiLogger.error(errMsg, ex);
            }
        };

        getInfo.accept("RunCtrl.Supervision." + this.rcAppInfo.getName());
        getInfo.accept("RunCtrl." + this.rcAppInfo.getName());

        return rows;
    }
}
