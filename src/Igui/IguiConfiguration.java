package Igui;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang.text.StrSubstitutor;

/**
 * This class implements access to the Igui's configuration.
 * <p>
 * Configuration items can be potentially defined in three different ways:
 * <ul>
 * <li>Via a property file: the file itself can be defined via the <code>igui.property.file</code> property. If the that property is not
 * found, then a the <code>USER-HOME/.igui/igui_<TDAQ-RELEASE>.properties</code> location is used.
 * <li>Via property values passed as command line arguments.
 * <li>Via environment variables (when possible).
 * </ul>
 * In case of multiple definitions of the same property, the previous list mirrors the priority given to the defined configuration items
 * (i.e., items defined in the property file have the priority with respect to the same item passed as a command line property and or
 * defined via an environment variable.
 * 
 * @see https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltIGUI#Settings for additional details.
 */
class IguiConfiguration {
    /**
     * Object replacing environment variables with their actual values.
     */
    private final static ThreadLocal<StrSubstitutor> ENV_RESOLVER = new ThreadLocal<StrSubstitutor>() {
        @Override
        protected StrSubstitutor initialValue() {          
            final StrSubstitutor sub = new StrSubstitutor(new StrLookup() {              
                @Override
                public String lookup(final String text) {
                    // Text is ENV_VAR_NAME:-defaultValue
                    final String[] tokens = text.split(":\\-");
                    
                    final String key = tokens[0];
                    String defValue = "";
                    if(tokens.length > 1) {
                        defValue = tokens[1];
                    }
                                       
                    final String value = System.getenv(key);
                    if(value != null) {
                        return value;
                    } 
                    
                    return defValue;
                }
            }, StrSubstitutor.DEFAULT_PREFIX, StrSubstitutor.DEFAULT_SUFFIX, '$');
            return sub;
        }        
    };
    
    /**
     * The property object holding the properties defined in the property file.
     */
    private final Properties properties = new Properties();

    /**
     * Holder class used for lazy-loading
     */
    private static class Holder {
        private final static IguiConfiguration INSTANCE = new IguiConfiguration();
    }

    /**
     * Constructor.
     * <p>
     * It loads properties from file.
     */
    private IguiConfiguration() {
        // Dot not use the IguiLogger in the costructor: it gets the log4j configuration file from this class!!!
        String propertyFilePath = System.getProperty(IguiConstants.IGUI_PROPERTY_FILE);
        if(propertyFilePath == null) {
            final String userHome = System.getProperty("user.home", "~");
            final String tdaqRelease = System.getProperty("igui.tdaq.version", "unknown");
            propertyFilePath = userHome + "/.igui/igui_" + tdaqRelease + ".properties";
        }

        try (final FileReader f = new FileReader(propertyFilePath)) {
            this.properties.load(f);
            
            System.out.println("Igui properties loaded from \"" + propertyFilePath + "\":");
            for(final Map.Entry<Object, Object> entry : this.properties.entrySet()) {
                System.out.println(entry.getKey().toString() + "=" + IguiConfiguration.substituteEnvVars(entry.getValue().toString()));
            }
        }
        catch(final IOException ex) {
            System.err.println("Cannot read property file \"" + propertyFilePath + "\": " + ex);
            ex.printStackTrace();
        }
    }

    /**
     * It returns an instance of this class.
     * 
     * @return An instance of this class.
     */
    public static IguiConfiguration instance() {
        return IguiConfiguration.Holder.INSTANCE;
    }

    /**
     * It returns the list of panels (i.e, their class names) to forcibly load.
     * <p>
     * That is defined via the <code>igui.panel.force.load</code> property or the <code>TDAQ_IGUI_PANEL_FORCE_LOAD</code> environment variable.
     * <p>
     * Multiple panel class names can be defined using the <code>:</code> separator.
     * 
     * @return The list of panels (i.e, their class names) to forcibly load or an empty string if the corresponding property is not defined.
     */
    public String getPanelsToLoad() {
        return this.getValue(IguiConstants.IGUI_PANELS_FORCE_LOAD_PROPERTY, IguiConstants.IGUI_PANELS_FORCE_LOAD_ENV_VAR, "");
    }
    
    /**
     * It returns the default ERS subscription to be used.
     * <p>
     * That is defined via the <code>igui.ers.subscription</code> property or the <code>TDAQ_IGUI_ERS_SUBSCRIPTION</code> environment variable.
     * 
     * @return The default ERS subscription to be used or an empty string if the corresponding property is not defined.
     */
    public String getDefaultErsSubscription() {
        return this.getValue(IguiConstants.ERS_SUB_PROPERTY, IguiConstants.ERS_SUB_ENV_VAR, "");
    }

    /**
     * It returns the location of the OKS file holding the description of the default ERS filter to be used.
     * <p>
     * That can be defined via the <code>igui.ers.filter.file</code> property or the <code>TDAQ_IGUI_ERS_FILTER_FILE</code> environment
     * variable.
     * 
     * @return The location of the OKS file holding the description of the default ERS filter to be used, or an empty string if that option is not
     *         defined.
     */
    public String getErsFilterFile() {
        return this.getValue(IguiConstants.IGUI_ERS_FILTER_FILE_PROPERTY, IguiConstants.IGUI_ERS_FILTER_FILE_ENV_VAR, "");
    }

    /**
     * It returns the URL of the log-book server.
     * <p>
     * That can be defined via the <code>igui.elog.url</code> property or the <code>TDAQ_ELOG_SERVER_URL</code> environment variable.
     * 
     * @return The URL of the log-book server, or an empty string if that option is not defined.
     */
    public String getElogURL() {
        return this.getValue(IguiConstants.ELOG_SERVER_URL_PROPERTY, IguiConstants.ELOG_SERVER_URL_VAR, "");
    }

    /**
     * It returns the URL of the OKS GIT repository web view.
     * <p>
     * That can be defined via the <code>igui.git.url</code> property or the <code>TDAQ_IGUI_DB_GIT_BASE</code> environment variable.
     * 
     * @return The URL of the OKS GIT repository web view, or an empty string if that option is not defined.
     */
    public String getGitURL() {
        return this.getValue(IguiConstants.DB_GIT_BASE_PROPERTY, IguiConstants.DB_GIT_BASE_ENV_VAR, "");
    }

    /**
     * It returns the name of the browser to be used to open web links.
     * <p>
     * That can be defined via the <code>igui.use.browser</code> property or the <code>TDAQ_IGUI_USE_BROWSER</code> environment variable.
     * 
     * @return The name of the browser to be used to open web links, or an empty string if that option is not defined.
     */
    public String getBrowser() {
        return this.getValue(IguiConstants.USE_BROWSER_PROPERTY, IguiConstants.USE_BROWSER_ENV_VAR, "");
    }

    /**
     * It returns the location of the <code>log4j</code> configuration file.
     * <p>
     * That can be defined via the <code>igui.log4j.conf.file</code> property or the <code>TDAQ_IGUI_LOG4J_CONF_FILE</code> environment
     * variable.
     * <p>
     * The default of the file is <code>${TDAQ_INST_PATH}/share/data/Igui/log4j_Igui_xmlConfig.xml</code>.
     * 
     * @return The location of the <code>log4j</code> configuration file, or its default location if that option is not defined.
     */
    public String getLog4jConfFile() {
        final String p = this.getValue(IguiConstants.TDAQ_LOG4J_CONF_PROPERTY, IguiConstants.TDAQ_LOG4J_CONF_ENV_VAR, "");
        return (p.isEmpty() == false) ? p : IguiConstants.INST_PATH + "/share/data/Igui/log4j_Igui_xmlConfig.xml";
    }

    /**
     * It returns whether a warning should be shown when the CONNETED FSM state is reached, reminding the operator to check the values of
     * the run parameters.
     * <p>
     * That can be defined via the <code>igui.hide.connect.warning</code> property or the <code>TDAQ_IGUI_NO_WARNING_AT_CONNECT</code>
     * environment variable.
     * 
     * @return Whether the warning should be shown or not, or <code>true</code> if that option is not defined.
     */
    public boolean hideConnectWarning() {
        final String p = this.getValue(IguiConstants.HIDE_WARNING_AT_CONNECT_PROPERTY, IguiConstants.HIDE_WARNING_AT_CONNECT_ENV_VAR, "");
        return (p.isEmpty() == false) ? true : false;
    }

    /**
     * It returns whether the Igui should ask the control resource to the ResourceManager when it starts.
     * <p>
     * That can be defined via the <code>igui.useRM</code> property or the <code>TDAQ_IGUI_USE_RM</code> environment variable.
     * 
     * @return Whether the Igui should ask the control resource to the ResourceManager when it starts, or <code>false</code> if that option
     *         is not defined.
     */
    public boolean askControl() {
        return Boolean.parseBoolean(this.getValue(IguiConstants.IGUI_USE_RM_PROPERTY, IguiConstants.IGUI_USE_RM_ENV_VAR, "false"));
    }

    /**
     * It returns the timeout (in minutes) after which an Igui in status display mode will show a dialog informing the operator that the Igui
     * instance is going to be terminated.
     * <p>
     * That can be defined via the <code>tdaq.igui.forcedstop.timeout</code> property or the <code>TDAQ_IGUI_FORCEDSTOP_TIMEOUT</code>
     * environment variable.
     * <p>
     * The default value is 300 minutes.
     * 
     * @return The timeout (in minutes) after which an Igui in status display mode will show a dialog informing the operator that the Igui
     *         instance is going to be terminated, or the default value of 300 minutes if that option is not defined.
     */
    public long getStopIguiTimeout() {
        long v = 300L;

        final String p = this.getValue(IguiConstants.FORCED_STOP_TIMEOUT_MINUTES_PROPERTY, IguiConstants.FORCED_STOP_TIMEOUT_MINUTES_ENV_VAR, "");
        if(p.isEmpty() == false) {
            try {
                v = Long.valueOf(p).longValue();
            }
            catch(final NumberFormatException ex) {
                ex.printStackTrace();
            }
        }

        return v;
    }

    /**
     * It returns a message to be shown in a confirmation dialog before the STOP command is sent.
     * <p>
     * That can be defined via the <code>igui.stop.warning</code> property or the <code>TDAQ_IGUI_STOP_WARNING</code> environment variable.
     * <p>
     * The property value must have the following format: <code>my message@myPartition</code>. Messages for several partitions can be
     * defined using the <code>:</code> separator.
     * 
     * @return The message to be shown in a confirmation dialog before the STOP command is sent, or an empty string if that option is not
     *         defined.
     */
    public String getStopWarning() {
        return this.getValue(IguiConstants.IGUI_STOP_MSG_PROPERTY, IguiConstants.IGUI_STOP_MSG_ENV_VAR, "");
    }

    /**
     * It gives the full name of the read-only configuration (e.g., <code>rdbconfig@RDB</code>).
     * 
     * @return The read-only configuration name
     */
    public String getConfigName() {
        return IguiConstants.DB_DEFAULT_IMPLEMENTATION + ":" + this.getDbServerName();
    }

    /**
     * It returns the name of the main RDB server.
     * <p>
     * That can be defined via the <code>igui.rdb.name</code> property.
     * <p>
     * The default value is "RDB".
     * 
     * @return The name of the main RDB server, or the "RDB" default value if that option is not defined.
     */
    public String getDbServerName() {
        return this.getValue(IguiConstants.RDB_NAME_PROPERTY, "", "RDB");
    }

    /**
     * It gives the full name of the read/write configuration (e.g., <code>rdbconfig@RDB_RW</code>)
     * 
     * @return The read/write configuration name
     */
    public String getRWConfigName() {
        return IguiConstants.DB_DEFAULT_IMPLEMENTATION + ":" + this.getRWDbServerName();
    }

    /**
     * It returns the name of the read/write RDB server.
     * <p>
     * That can be defined via the <code>igui.rdb_rw.name</code> property.
     * <p>
     * The default value is "RDB_RW".
     * 
     * @return The name of the read/write RDB server, or the "RDB_RW" default value if that option is not defined.
     */
    public String getRWDbServerName() {
        return this.getValue(IguiConstants.RDBRW_NAME_PROPERTY, "", "RDB_RW");
    }

    /**
     * It returns the number of rows to be shown by default in the ERS panel.
     * <p>
     * That can be defined via the <code>igui.ers.rows</code> property or the TDAQ_IGUI_ERS_ROWS environment variable.
     * <p>
     * The default value is 1000 rows.
     * 
     * @return The number of rows to be shown by default in the ERS panel.
     */
    public int getErsPanelRows() {
        int v = 1000;

        final String p = this.getValue(IguiConstants.ERS_PANEL_ROWS_PROPERTY, IguiConstants.ERS_PANEL_ROWS_ENV_VAR, "");
        if(p.isEmpty() == false) {
            try {
                v = Integer.valueOf(p).intValue();
            }
            catch(final NumberFormatException ex) {
                ex.printStackTrace();
            }
        }

        return v;
    }
    
    /**
     * It returns the value of a certain configuration item.
     * <p>
     * This method:
     * <ul>
     * <li>Looks for the defined property in the property file;
     * <li>If not found, looks for the same property if defined in the system properties;
     * <li>If no property has been defined, then it looks for the value of the value of the corresponding environment variable.
     * </ul>
     * 
     * @param propertyName The name of the corresponding property
     * @param envVar The name of the corresponding environment variable
     * @param defValue The default value to return when the option is not defined
     * @return The value of a certain configuration item.
     */
    private String getValue(final String propertyName, final String envVar, final String defValue) {
        String p = this.properties.getProperty(propertyName, System.getProperty(propertyName));

        if((p == null) && (envVar.isEmpty() == false)) {
            p = System.getenv(envVar);
        }

        if(p == null) {
            return defValue;
        }

        return IguiConfiguration.substituteEnvVars(p);
    }
    
    /**
     * It substitutes environment variables in the text with their actual values.
     * <p>
     * Environment variables must be formatted as <code>${VAR_NAME}</code>.
     * Default values can be expressed as <code>${VAR_NAME:-defValue}</code>
     * 
     * @param text The string to process
     * @return A string with environment variables replaced by their actual value.
     */
    private static String substituteEnvVars(final String text) {
        final StrSubstitutor sub = IguiConfiguration.ENV_RESOLVER.get();        
        return sub.replace(text);
    }
}
