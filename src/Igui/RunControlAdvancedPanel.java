package Igui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadPoolExecutor;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.BevelBorder;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import Igui.IguiConstants.AccessControlStatus;
import Igui.RunControlFSM.State;
import Igui.RunControlTreePanel.RCTreeNode;
import daq.rc.Command;
import daq.rc.Command.RCCommands;
import daq.rc.CommandSender;
import daq.rc.RCException;


/**
 * This panel exposes the user to an advanced interface to interact with the run control system. It contains three sub-panels:
 * <ul>
 * <li><b>FSM transition panel: </b> This panel allows to send FSM transition individually to the run control application selected in the
 * run control tree. Only the commands allowed in a determined state are shown;
 * <li><b>Advanced commands: </b> This panel allows to set the selected application debug level and ask it to publish its state or its
 * statistics information;
 * <li><b>Application information: </b> This panel shown some detailed information about the selected application.
 * </ul>
 * <p>
 * The {@link RunControlMainPanel} panel is the parent of this panel: this means that it is not the {@link Igui} to call methods on this
 * panel.
 */
class RunControlAdvancedPanel extends AbstractRunControlPanel {
    private static final long serialVersionUID = -4163730044779087140L;

    /**
     * Map associating a button to each FSM transition
     */
    private final Map<RunControlFSM.Transition, JButton> tbMap = new EnumMap<RunControlFSM.Transition, JButton>(RunControlFSM.Transition.class);

    /**
     * Map associating a {@link StateLabel} to each FSM state
     */
    private final Map<RunControlFSM.State, StateLabel> slMap = new EnumMap<RunControlFSM.State, StateLabel>(RunControlFSM.State.class);

    /**
     * Single executor used to send commands to the applications
     * <p>
     * This HAS TO BE USED to send remote commands to the applications
     */
    private final ThreadPoolExecutor cmdSender = ExecutorFactory.createSingleExecutor("RunControl-Adv-Cmd-Sender");

    /**
     * This panel name
     */
    private final static String pName = "Advanced";

    /**
     * A bold font
     */
    private final Font boldFont = new Font("Dialog", Font.BOLD, 10);

    /**
     * A plain font
     */
    private final Font plainFont = new Font("Dialog", Font.PLAIN, 10);

    /**
     * Vertical box used in the "FSM transition command" panel
     */
    private final Box vBox = Box.createVerticalBox();

    /**
     * Label used at the top of the panel to show the selected application name
     */
    private final JLabel cLabel = new JLabel() {
        private static final long serialVersionUID = 4136817004349916761L;

        @Override
        public void setText(final String text) {
            super.setText(text);
            this.setToolTipText(text);
        }
    };

    /**
     * Dimension to be used for the FSM transition button
     */
    private final Dimension buttonDim = new Dimension(90, 20);

    /**
     * Model for the spinner used to set the application debug level
     */
    private final SpinnerNumberModel dbgSpinnerModel = new SpinnerNumberModel(0, 0, 10, 1);

    /**
     * Model for the spinner used to set the probe interval time
     */
    private final SpinnerNumberModel probeSpinnerModel = new SpinnerNumberModel(0, 0, 900, 1);

    /**
     * Model for the spinner used to set the full stat interval time
     */
    private final SpinnerNumberModel statSpinnerModel = new SpinnerNumberModel(0, 0, 900, 1);

    /**
     * Debug level spinner selector
     */
    private final JSpinner dbgSpinner = new JSpinner(this.dbgSpinnerModel);

    /**
     * Probe interval spinner selector
     */
    private final JSpinner probeSpinner = new JSpinner(this.probeSpinnerModel);

    /**
     * Full stat interval spinner selector
     */
    private final JSpinner statSpinner = new JSpinner(this.statSpinnerModel);

    /**
     * The button to send the selected application the command to publish its state
     */
    private final JButton publishButton = new JButton("<html><CENTER>Publish<br />State</CENTER></html>");

    /**
     * The button used to send the selected application the command to publish statistics information
     */
    private final JButton publishStatisticsButton = new JButton("<html><CENTER>Publish<br />Statistics</CENTER></html>");

    /**
     * The button used to set the selected application debug level
     */
    private final JButton setDebugLevelButton = new JButton("Set");

    /**
     * The button used to set the selected application probe interval
     */
    private final JButton setProbeIntervalButton = new JButton("Set");

    /**
     * The button used to set the selected application full stat interval
     */
    private final JButton setStatIntervalButton = new JButton("Set");

    // Labels used in the "Application Information" panel
    private final JLabel hostLabel = new InfoLabel("");
    private final JLabel runningHostLabel = new InfoLabel("");
    private final JLabel paramsLabel = new InfoLabel("");
    private final JLabel exitUnexpectedLabel = new InfoLabel("");
    private final JLabel failsToStartLabel = new InfoLabel("");
    private final JLabel errorLabel = new InfoLabel("");
    private final JLabel startLabel = new InfoLabel("");
    private final JLabel stopLabel = new InfoLabel("");
    private final JLabel canExitLabel = new InfoLabel("");
    private final JLabel initLabel = new InfoLabel("");
    private final JLabel actionLabel = new InfoLabel("");

    /**
     * Object whose methods are associated to the button actions
     */
    private final ButtonActions bActions = new ButtonActions();

    /**
     * Simple label used for the "right side" of the "Application Information" panel.
     * <p>
     * It has been sub-classed to simply associate to it a tooltip text.
     */
    private final static class InfoLabel extends JXLabel {
        private static final long serialVersionUID = 8813939992432159943L;

        {
            this.setLineWrap(false);
            this.setFont(new Font("Dialog", Font.PLAIN, 12));
        }

        InfoLabel(final String text) {
            super(text);
            this.setToolTipText(this.getText());
        }

        @Override
        public void setText(final String text) {
            super.setText(text);
            this.setToolTipText(text);
        }
    }

    /**
     * Utility class to implement action associated to buttons.
     * <p>
     * For simplicity sake the FSM transition buttons in the "FSM Transition Commands" panel follow a different path (see
     * {@link TransitionButton}).
     */
    private final class ButtonActions {
        ButtonActions() {
        }

        /**
         * This method is executed when the {@link RunControlAdvancedPanel#setDebugLevelButton} button is pushed.
         * <p>
         * It executes the {@link DebugLevelTask} in the {@link RunControlAdvancedPanel#cmdSender} executor.
         * 
         * @param ctrl The application the command should be sent to
         * @param newValue The new debug value
         * @see Igui#sendControllerCommand(String, Igui.RunControlFSM.RCCommands, String)
         */
        void setDebugLevel(final String ctrl, final int newValue) {
            RunControlAdvancedPanel.this.cmdSender.execute(new DebugLevelTask(ctrl, Integer.valueOf(newValue)));
        }

        /**
         * This method is executed when the {@link RunControlAdvancedPanel#publishButton} button is pushed.
         * <p>
         * It sends the application the {@link RunControlFSM.RCCommands#PUBLISH} command in the {@link RunControlAdvancedPanel#cmdSender}
         * executor.
         * 
         * @param ctrl The application the command should be sent to
         * @see Igui#sendControllerCommand(String, Igui.RunControlFSM.RCCommands, String)
         */
        void publish(final String ctrl) {
            RunControlAdvancedPanel.this.cmdSender.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Igui.instance().sendControllerCommand(ctrl, new Command.PublishCmd());
                    }
                    catch(final RCException ex) {
                        final String errMsg = "Failed sending the \"" + RCCommands.PUBLISH.toString() + "\" command to \"" + ctrl + "\": "
                                              + ex;
                        ErrorFrame.showError("IGUI - Command Error", errMsg, ex);
                    }
                }
            });
        }

        /**
         * This method is executed when the {@link RunControlAdvancedPanel#publishStatisticsButton} button is pushed.
         * <p>
         * It sends the application the {@link RunControlFSM.RCCommands#PUBLISHSTATISTICS} command in the
         * {@link RunControlAdvancedPanel#cmdSender} executor.
         * 
         * @param ctrl The application the command should be sent to
         * @see Igui#sendControllerCommand(String, Igui.RunControlFSM.RCCommands, String)
         */
        void publishStats(final String ctrl) {
            RunControlAdvancedPanel.this.cmdSender.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Igui.instance().sendControllerCommand(ctrl, new Command.PublishFullStatsCmd());
                    }
                    catch(final RCException ex) {
                        final String errMsg = "Failed sending the \"" + RCCommands.PUBLISHSTATS.toString() + "\" command to \"" + ctrl
                                              + "\": " + ex;
                        ErrorFrame.showError("IGUI - Command Error", errMsg, ex);
                    }
                }
            });
        }

        /**
         * This method is executed when the {@link RunControlAdvancedPanel#setProbeIntervalButton} button is pushed.
         * <p>
         * It sends the application the {@link RunControlFSM.RCCommands#CHANGE_PROBE_INTERVAL} command in the
         * {@link RunControlAdvancedPanel#cmdSender} executor.
         * 
         * @param ctrl The application the command should be sent to
         * @param interval The time interval (in seconds) for the probe
         * @see Igui#sendControllerCommand(String, Igui.RunControlFSM.RCCommands, String)
         */
        void setProbeInterval(final String ctrl, final int interval) {
            RunControlAdvancedPanel.this.cmdSender.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Igui.instance().sendControllerCommand(ctrl, new Command.ChangePublishIntervalCmd(interval));
                    }
                    catch(final RCException ex) {
                        final String errMsg = "Failed sending the \"" + RCCommands.CHANGE_PROBE_INTERVAL + "\" command to \"" + ctrl
                                              + "\": " + ex;
                        ErrorFrame.showError("IGUI - Command Error", errMsg, ex);
                    }
                }
            });
        }

        /**
         * This method is executed when the {@link RunControlAdvancedPanel#setStatIntervalButton} button is pushed.
         * <p>
         * It sends the application the {@link RunControlFSM.RCCommands#CHANGE_FULLSTATISTICS_INTERVAL} command in the
         * {@link RunControlAdvancedPanel#cmdSender} executor.
         * 
         * @param ctrl The application the command should be sent to
         * @param interval The time interval (in seconds) for the full stat
         * @see Igui#sendControllerCommand(String, Igui.RunControlFSM.RCCommands, String)
         */
        void setStatInterval(final String ctrl, final int interval) {
            RunControlAdvancedPanel.this.cmdSender.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Igui.instance().sendControllerCommand(ctrl, new Command.ChangeFullStatsIntervalCmd(interval));
                    }
                    catch(final RCException ex) {
                        final String errMsg = "Failed sending the \"" + RCCommands.CHANGE_FULLSTATS_INTERVAL + "\" command to \"" + ctrl
                                              + "\": " + ex;
                        ErrorFrame.showError("IGUI - Command Error", errMsg, ex);
                    }
                }
            });
        }
    }

    /**
     * SwingWorker used to set the selected application debug level when the used pushes the
     * {@link RunControlAdvancedPanel#setDebugLevelButton} button.
     * <p>
     * This task is also used to retrieve the application current debug level (see {@link RunControlAdvancedPanel#panelSelected()}).
     */
    private final class DebugLevelTask extends SwingWorker<Integer, Void> {
        /**
         * The commander used to send the command
         */
        private final CommandSender commander = new CommandSender(Igui.instance().getPartition().getName(), "IGUI");

        /**
         * The name of the controller the command should be sent to
         */
        private final String controllerName;

        /**
         * The debug level value to set
         */
        private final Integer newValue;

        /**
         * Constructor.
         * 
         * @param controllerName The name of the controller the command should be sent to
         * @param newValue The debug level value to set (if <code>null</code> then this task will retrieve the current value of the
         *            application debug level)
         */
        DebugLevelTask(final String controllerName, final Integer newValue) {
            this.newValue = newValue;
            this.controllerName = controllerName;
        }

        /**
         * It returns the up to date value of the application debug level.
         * 
         * @see javax.swing.SwingWorker#doInBackground()
         */
        @Override
        protected Integer doInBackground() throws daq.rc.RCException {
            short valueToReturn;

            if(this.newValue != null) {
                // Not null -> set the desired debug level
                final short newDbgLevel = this.newValue.shortValue();
                this.commander.setDebugLevel(this.controllerName, newDbgLevel);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.INFORMATION,
                                                "Debug level set to " + newDbgLevel + " for application \"" + this.controllerName + "\"");
                valueToReturn = newDbgLevel;
            } else {
                // null -> get the current debug level
                final short currentDbgLevel = this.commander.getDebugLevel(this.controllerName);
                valueToReturn = currentDbgLevel;
            }

            return Integer.valueOf(valueToReturn);
        }

        /**
         * It gets the return value of the background task and shows the application debug level using the
         * {@link RunControlAdvancedPanel#dbgSpinner} spinner.
         * 
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            try {
                final Integer dbgLevel = this.get();
                // This check is really important to show consistent values: in fact the task is totally asynchronous and
                // it may be that in the while a different tree item has been selected. So set the spinner value
                // only if the currently selected application matches the name of the application this task has been started for.
                if(RunControlAdvancedPanel.this.cLabel.getText().equals(this.controllerName)) {
                    RunControlAdvancedPanel.this.dbgSpinnerModel.setValue(dbgLevel);
                }
            }
            catch(final InterruptedException ex) {
                final String errMsg = "Cannot get/set debug level because the working task has been interrupted";
                IguiLogger.error(errMsg);
                Thread.currentThread().interrupt();
            }
            catch(final Exception ex) {
                final String errMsg = "Unexpected error while getting/setting the debug level for \"" + this.controllerName + "\": "
                                      + ex.toString();
                IguiLogger.error(errMsg, ex);
                Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
            }
        }
    }

    /**
     * An instance of this class is a {@link JButton} which is associated to an FSM transition (i.e., it is used to ask an application to
     * move from a state towards another one in the FSM): it is used in the "FSM Transition Commands" panel.
     * <p>
     * An {@link ActionListener} is associated to each instance of this class: the listener task is to send the selected application the
     * right command associated to the FSM transition.
     * <p>
     * This button label is equal to the transition name.
     */
    private final class TransitionButton extends JButton {
        private static final long serialVersionUID = 6815607758604349076L;

        /**
         * Constructor.
         * 
         * @param tr The {@link RunControlFSM.Transition} transition associated to this button.
         */
        TransitionButton(final RunControlFSM.Transition tr) {
            // Call super setting the button label
            super(tr.toString());

            this.setFont(RunControlAdvancedPanel.this.plainFont);
            this.setHorizontalAlignment(SwingConstants.CENTER);

            // This alignment setting is really important for the correct layout of all the buttons in the panel
            this.setAlignmentY(0.5f);

            // The size is large enough to accommodate all the labels
            this.setPreferredSize(RunControlAdvancedPanel.this.buttonDim);
            this.setMaximumSize(RunControlAdvancedPanel.this.buttonDim);

            this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
            this.setBackground(tr.getDestinationState().getColor());

            this.setToolTipText(tr.toString());

            this.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    // Here we are in the EDT, so the panel top label reports the currently selected application.
                    // There is no possibility to be wrong!
                    final String selController = RunControlAdvancedPanel.this.cLabel.getText();

                    // Send the command using the command executor: remember that each command implies
                    // a remote command which can potentially take a large amount of time and lock
                    // the EDT.
                    RunControlAdvancedPanel.this.cmdSender.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Igui.instance().sendControllerTransitionCommand(selController, tr);
                            }
                            catch(final RCException ex) {
                                final String msg = "Failed sending the \"" + tr.toString() + "\" command to \"" + selController + "\": "
                                                   + ex;
                                IguiLogger.error(msg);
                                ErrorFrame.showError("IGUI - Transition Command Error", msg, ex);
                            }
                        }
                    });
                }
            });
        }
    }

    /**
     * An instance of this class is a {@link JLabel} which is associated to an FSM state: it is used in the "FSM Transition Commands" panel.
     */
    private final class StateLabel extends JLabel {
        private static final long serialVersionUID = -1092214042627715268L;

        /**
         * The state associated to this label
         */
        private final RunControlFSM.State state;

        /**
         * This label dimension
         */
        private final Dimension stateLabelDim = new Dimension(80, 20);

        /**
         * Constructor.
         * 
         * @param state The {@link RunControlFSM.State} state associated to this label.
         */
        StateLabel(final RunControlFSM.State state) {
            super(state.toString());

            this.state = state;

            // Size and alignment is very important to have a correct layout in the panel
            this.setPreferredSize(this.stateLabelDim);
            this.setMaximumSize(this.stateLabelDim);
            this.setHorizontalAlignment(SwingConstants.CENTER);
            this.setAlignmentY(0.5f);
            this.setOpaque(true);
        }

        /**
         * Whether to highlight or not this label.
         * <p>
         * Used to highlight the current state of the selected application.
         * 
         * @param highlight If <code>true</code> the label is highlighted.
         */
        void setHighlighted(final boolean highlight) {
            if(highlight == true) {
                this.setFont(RunControlAdvancedPanel.this.boldFont);
                this.setBackground(this.state.getColor());
                this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
                this.setForeground(Color.black);
            } else {
                this.setFont(RunControlAdvancedPanel.this.plainFont);
                this.setBackground(null);
                this.setBorder(null);
                this.setForeground(Color.gray);
            }
        }
    }

    /**
     * An empty label with the right dimension to fill the empty spaces in the "FSM Transion Commands" panel.
     */
    private final class EmptyLabel extends JLabel {
        private static final long serialVersionUID = 4047446865065968045L;

        EmptyLabel() {
            super("");
            this.setPreferredSize(RunControlAdvancedPanel.this.buttonDim);
            this.setMaximumSize(RunControlAdvancedPanel.this.buttonDim);
        }
    }

    /**
     * The constructor.
     * 
     * @param parent The parent panel
     * @see RunControlAdvancedPanel#createButtons()
     * @see RunControlAdvancedPanel#subscribeNodeSelection()
     * @see RunControlAdvancedPanel#subscribeNodeChanges()
     * @see RunControlAdvancedPanel#initGUI()
     */
    public RunControlAdvancedPanel(final RunControlMainPanel parent) {
        super(parent);
        this.createButtons();
        this.subscribeNodeSelection();
        this.subscribeNodeChanges();
        this.initGUI();
    }

    /**
     * If in status display all the buttons are disabled.
     * 
     * @see Igui.IguiInterface#accessControlChanged(Igui.IguiConstants.AccessControlStatus)
     * @see #setAdvancedButtonState(RunControlApplicationData)
     */
    @Override
    public void accessControlChanged(final AccessControlStatus newAccessStatus) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if(newAccessStatus.hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
                    RunControlAdvancedPanel.this.enableAllButtons(true);
                    final RCTreeNode selNode = RunControlAdvancedPanel.this.getMainPanel().getSelectedTreeNode();
                    if(selNode != null) {
                        RunControlAdvancedPanel.this.setAdvancedButtonState(selNode.getUserObject());
                    }
                } else {
                    RunControlAdvancedPanel.this.enableAllButtons(false);
                }
            }
        });
    }

    /**
     * Nothing to do when the database is reloaded: the information is updated every time a new node is selected or the node status changes;
     * moreover the tree selection changes when the DB is reloaded.
     * 
     * @see Igui.IguiInterface#dbReloaded()
     */
    @Override
    public void dbReloaded() {
    }

    /**
     * @see Igui.IguiInterface#getPanelName()
     */
    @Override
    public String getPanelName() {
        return RunControlAdvancedPanel.pName;
    }

    /**
     * When the panel is selected:
     * <ul>
     * <li>the "FSM Transition Commands" task pane is rebuilt taking into account the state of the selected application in the run control
     * tree;
     * <li>the state (enabled/disabled) of the buttons in the "Advanced Command" task pane is determined;
     * <li>the "Application Information" task pane is filled with the correct information;
     * <li>the selected application is asked about its debug level.
     * </ul>
     * <p>
     * Note that the {@link RunControlMainPanel} panel executes this method in the EDT.
     * 
     * @see Igui.IguiInterface#panelSelected()
     * @see RunControlMainPanel#panelSelected()
     * @see #buildFSM(RunControlApplicationData)
     * @see #setAdvancedButtonState(RunControlApplicationData)
     * @see #updateApplicationInfo(RunControlApplicationData)
     * @see DebugLevelTask
     */
    @Override
    public void panelSelected() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        final RCTreeNode selNode = this.getMainPanel().getSelectedTreeNode();
        final RunControlApplicationData data = ((selNode != null) ? selNode.getUserObject() : null);
        // Do something only if a tree node is selected
        final String nodeName;
        if(data != null) {
            nodeName = data.getName();
        } else {
            nodeName = "";
        }
        // Set FIRST the text of the label at the top of the panel: tasks and actions in this
        // panel use that label to know which application is selected in the RC tree. This procedure
        // may seem fragile but if correctly followed it make things much easier: it uses the EDT thread
        // to keep trace of the selected tree node and there is no need to ask avery time the tree.
        // Moreover it can be used in a thread safe way because it is accessed only from the EDT.
        // So pay attention to this simple rule!
        this.cLabel.setText(nodeName);
        this.buildFSM(data);
        this.setAdvancedButtonState(data);
        this.updateApplicationInfo(data);

        // Call revalidate and repaint to correctly layout
        this.revalidate();
        this.repaint();

        // Get the application debug level only if it is running, otherwise you cannot talk to it!
        if(data != null) {
            final RunControlApplicationData.ApplicationISStatus nodeISStatus = data.getISStatus();
            if(nodeISStatus.isUp() && data.isRunControlApplication()) {
                this.cmdSender.execute(new DebugLevelTask(nodeName, null));
            }
        }
    }

    /**
     * Nothing is done.
     * 
     * @see Igui.IguiInterface#panelDeselected()
     */
    @Override
    public void panelDeselected() {
    }

    /**
     * It just calls {@link #accessControlChanged(AccessControlStatus)}.
     * <p>
     * Not much more to do it: the panel is built in the constructor and is correctly filled with the right information every time it is
     * selected or the RC tree selection is changed.
     * 
     * @see IguiPanel#panelInit(State, AccessControlStatus)
     */
    @Override
    public void panelInit(final State rcState, final AccessControlStatus accessStatus) throws IguiException {
        this.accessControlChanged(accessStatus);
    }

    /**
     * It stops the executor used to send commands to the applications.
     * 
     * @see Igui.IguiInterface#panelTerminated()
     */
    @Override
    public void panelTerminated() {
        this.cmdSender.shutdown();
    }

    /**
     * The various task panes are rebuilt/redrawn taking into account the changes of the tree nodes.
     * <p>
     * Note that when this method is called the tree selection is NOT changed!
     * <p>
     * Note that the content of the "Application Information" task pane is not changed because it contains only static information. The
     * debug level is not changes as well since it is not part of the IS information causing the node changes.
     * 
     * @see Igui.AbstractRunControlPanel#treeNodesChanged(RCTreeNode[])
     * @see #buildFSM(RunControlApplicationData)
     * @see #setAdvancedButtonState(RunControlApplicationData)
     */
    @Override
    protected void treeNodesChanged(final RCTreeNode[] nodes) {
        // Do something only if this panel is currently selected (tree nodes are frequently updated)
        if(this.equals(this.getMainPanel().getSelectedSubPanel()) == true) {
            // Get the name of the selected application from the top label
            final String currentSelectedCtrl = this.cLabel.getText();
            // Loop over all the nodes whose state changed
            for(final RCTreeNode n : nodes) {
                final RunControlApplicationData data = n.getUserObject();
                if(data.getName().equals(currentSelectedCtrl)) {
                    // Take the right actions if the list of changed nodes includes the currently selected one
                    this.buildFSM(data);
                    this.setAdvancedButtonState(data);
                    // Revalidate and repaint to correctly layout all the panel elements
                    this.revalidate();
                    this.repaint();
                    break;
                }
            }
        }
    }

    /**
     * The various task panes are rebuilt/redrawn taking into account the new selected node.
     * 
     * @see Igui.AbstractRunControlPanel#treeNodeSelected(RCTreeNode)
     * @see #buildFSM(RunControlApplicationData)
     * @see #setAdvancedButtonState(RunControlApplicationData)
     * @see #updateApplicationInfo(RunControlApplicationData)
     * @see DebugLevelTask
     */
    @Override
    protected void treeNodeSelected(final RCTreeNode node) {
        // Do something only if the panel is selected
        if(this.equals(this.getMainPanel().getSelectedSubPanel()) == true) {
            final RunControlApplicationData data = ((node == null) ? null : node.getUserObject());
            final String nodeName;
            if(data != null) {
                nodeName = data.getName();
            } else {
                nodeName = "";
            }

            // See the comment in 'panelSelected': setting the label text is the very FIRST thing to do!
            this.cLabel.setText(nodeName);
            // Change properly the panel contents
            this.buildFSM(data);
            this.setAdvancedButtonState(data);
            this.updateApplicationInfo(data);
            // Revalidate and repaint the panel
            this.revalidate();
            this.repaint();

            // Ask the application about its debug level only if it is running
            if(data != null) {
                final RunControlApplicationData.ApplicationISStatus nodeISStatus = data.getISStatus();
                if(nodeISStatus.isUp() && data.isRunControlApplication()) {
                    this.cmdSender.execute(new DebugLevelTask(nodeName, null));
                }
            }
        }
    }

    /**
     * It updates the content of the "Application Information" task pane.
     * 
     * @param selectedNode The selected node whose state information is used to fill the table.
     */
    private void updateApplicationInfo(final RunControlApplicationData selectedNode) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        if(selectedNode != null) {
            this.hostLabel.setText(selectedNode.getHost());
            this.runningHostLabel.setText(selectedNode.getISStatus().getRunningHost());
            this.stopLabel.setText(selectedNode.getStopAt());
            this.paramsLabel.setText(selectedNode.getParameters());
            this.initLabel.setText(Integer.toString(selectedNode.getInitTimeout()));
            this.exitUnexpectedLabel.setText(selectedNode.getIfExitUnexpected());
            this.actionLabel.setText(selectedNode.getActionTimeout());
            this.failsToStartLabel.setText(selectedNode.getIfFailedToStart());
            this.errorLabel.setText(selectedNode.getIfError());
            this.startLabel.setText(selectedNode.getStartAt());
            this.canExitLabel.setText(Boolean.toString(selectedNode.canExitSpontaneously()));
        } else {
            this.hostLabel.setText("");
            this.runningHostLabel.setText("");
            this.stopLabel.setText("");
            this.paramsLabel.setText("");
            this.initLabel.setText("");
            this.exitUnexpectedLabel.setText("");
            this.actionLabel.setText("");
            this.failsToStartLabel.setText("");
            this.errorLabel.setText("");
            this.startLabel.setText("");
            this.canExitLabel.setText("");
        }
    }

    /**
     * It sets the status of the buttons in the "Advanced Commands" task pane taking into account the state of the selected node.
     * 
     * @param selectedNode The selected node whose state information is used to determine the button state.
     */
    private void setAdvancedButtonState(final RunControlApplicationData selectedNode) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        // Pay attention to the control level: if in status display then do not apply any change because the buttons
        // are already disabled in 'accessControlChanged'
        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
            final RunControlApplicationData.ApplicationISStatus selNodeISStatus;
            if((selectedNode != null) && selectedNode.isRunControlApplication() && ((selNodeISStatus = selectedNode.getISStatus()) != null)
               && selNodeISStatus.isUp())
            {
                this.publishButton.setEnabled(true);
                this.publishStatisticsButton.setEnabled(true);
                this.setDebugLevelButton.setEnabled(true);
                this.dbgSpinner.setEnabled(true);
                this.setProbeIntervalButton.setEnabled(true);
                this.probeSpinner.setEnabled(true);
                this.setStatIntervalButton.setEnabled(true);
                this.statSpinner.setEnabled(true);
            } else {
                this.publishButton.setEnabled(false);
                this.publishStatisticsButton.setEnabled(false);
                this.setDebugLevelButton.setEnabled(false);
                this.setProbeIntervalButton.setEnabled(false);
                this.setStatIntervalButton.setEnabled(false);
                this.dbgSpinner.setEnabled(false);
                this.probeSpinner.setEnabled(false);
                this.statSpinner.setEnabled(false);
            }
        }
    }

    /**
     * This methods takes care of rebuilding the "FSM Transition Commands" task pane every time a different node is selected or the already
     * selected node changes.
     * <p>
     * The panel is made up of multiple horizontal panels: each panel contains a transition button and a label showing the destination state
     * associated to the transition itself.
     * 
     * @param selectedNode The selected node whose state information is used to build the task pane.
     */
    private void buildFSM(final RunControlApplicationData selectedNode) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";
        // This is the conceptual schema to build the panel:
        // - given the node current state, all the possible transitions from that state are calculated;
        // - for each transition the destination state is calculated;
        // - a vertical box is filled containing horizontal boxes with transition buttons and associated destination states

        // Remove every thing in the panel
        this.vBox.removeAll();

        if(selectedNode != null) {
            // Show the FSM only if the selected application is a run control application
            if(selectedNode.isRunControlApplication()) {
                final RunControlApplicationData.ApplicationISStatus selNodeISStatus = selectedNode.getISStatus();
                // Check if the application is running
                if(selNodeISStatus.isUp()) {
                    final RunControlFSM.State selectedNodeState = selNodeISStatus.getState();
                    // Check if the application status is not ABSENT, transitions are allowed from the NONE state on
                    if(selectedNodeState.follows(RunControlFSM.State.ABSENT)) {
                        // Create a map associating a transition and its destination state
                        final Map<RunControlFSM.State, RunControlFSM.Transition> stateMap = new EnumMap<RunControlFSM.State, RunControlFSM.Transition>(RunControlFSM.State.class);
                        // The state of the selected node is associated to a null transition: the label showing the
                        // current state will have no buttons at its left and right sides
                        stateMap.put(selectedNodeState, null);

                        {
                            // Get all the possible transitions starting from the current state of the selected node
                            final Set<RunControlFSM.Transition> trs = RunControlFSM.instance().getTransitions(selectedNodeState);
                            final Iterator<RunControlFSM.Transition> it = trs.iterator();
                            while(it.hasNext()) {
                                // Fill the map associating a transition and its destination state
                                final RunControlFSM.Transition t = it.next();
                                stateMap.put(t.getDestinationState(), t);
                            }
                        }

                        // Loop over the map: the recursion order is given by the FSM definition in 'RunControlFSM'
                        final Set<Map.Entry<RunControlFSM.State, RunControlFSM.Transition>> me = stateMap.entrySet();
                        for(final Map.Entry<RunControlFSM.State, RunControlFSM.Transition> el : me) {
                            // Transition and its destination state
                            final RunControlFSM.State state = el.getKey();
                            final RunControlFSM.Transition transition = el.getValue();

                            // Basically we create an horizontal panel for each state in the map: the panel will contain a button
                            // and a label (the panel corresponding the application current state will have only the label):
                            // a transition button and its destination state (the label)
                            final JPanel p = new JPanel();
                            p.setAlignmentX(0.5f);
                            p.setLayout(new FlowLayout());
                            p.setOpaque(false);
                            final Box hBox = Box.createHorizontalBox();
                            p.add(hBox);

                            // The 'slMap' is built at construction time and associates state labels to all the
                            // possible states
                            final StateLabel sl = this.slMap.get(state);
                            if(transition != null) {
                                // If null then is is NOT the current node state
                                if(!transition.upTransition()) {
                                    // This is a transition going down into the FSM: layout is -> button - label -empty
                                    hBox.add(this.tbMap.get(transition));
                                    hBox.add(Box.createRigidArea(new Dimension(10, 0)));
                                    sl.setHighlighted(false);
                                    hBox.add(sl);
                                    hBox.add(Box.createRigidArea(new Dimension(10, 0)));
                                    hBox.add(new EmptyLabel());
                                } else {
                                    // This is a transition going up into the FSM: layout is -> empty - label - button
                                    hBox.add(new EmptyLabel());
                                    hBox.add(Box.createRigidArea(new Dimension(10, 0)));
                                    sl.setHighlighted(false);
                                    hBox.add(sl);
                                    hBox.add(Box.createRigidArea(new Dimension(10, 0)));
                                    hBox.add(this.tbMap.get(transition));
                                }
                            } else {
                                // The current application state: layout is empty - label - empty
                                hBox.add(new EmptyLabel());
                                hBox.add(Box.createRigidArea(new Dimension(10, 0)));
                                sl.setHighlighted(true);
                                hBox.add(sl);
                                hBox.add(Box.createRigidArea(new Dimension(10, 0)));
                                hBox.add(new EmptyLabel());
                            }

                            // Here is the end of the loop: add the horizontal panel to the vertical box
                            this.vBox.add(p);
                        }

                        // Take into account the access level and node status to enable/disable buttons
                        // Remember that you are asked to show the correct FSM status even in status display!
                        if(Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY)) {
                            if((selNodeISStatus.isBusy() && selNodeISStatus.isTransitioning()) || selNodeISStatus.isError()) {
                                // If the node is busy or in error then commands should not be allowed
                                for(final JButton b : this.tbMap.values()) {
                                    b.setEnabled(false);
                                }
                            } else {
                                for(final JButton b : this.tbMap.values()) {
                                    b.setEnabled(true);
                                }
                            }

                            // We want the SHUTDOWN always enabled for the Root
                            this.tbMap.get(RunControlFSM.Transition.SHUTDOWN).setEnabled(selectedNode.getName().equals(Igui.instance().getRootControllerName()));
                        }
                    } else {
                        this.vBox.add(this.createInfoPanel("\"" + this.cLabel.getText() + "\" has not reached the NONE state yet"));
                    }
                } else {
                    this.vBox.add(this.createInfoPanel("\"" + this.cLabel.getText() + "\" is not running"));
                }
            } else {
                this.vBox.add(this.createInfoPanel("\""
                                                   + this.cLabel.getText()
                                                   + "\" is not a Run Control application, it is not possible to send it state transition commands"));
            }
        }
    }

    /**
     * It enables/disables all the panel buttons.
     * 
     * @param enable If <code>true</code> all the buttons are enabled.
     */
    private void enableAllButtons(final boolean enable) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        for(final JButton b : this.tbMap.values()) {
            b.setEnabled(enable);
        }

        this.publishButton.setEnabled(enable);
        this.publishStatisticsButton.setEnabled(enable);
        this.setDebugLevelButton.setEnabled(enable);
        this.setProbeIntervalButton.setEnabled(enable);
        this.setStatIntervalButton.setEnabled(enable);
        this.dbgSpinner.setEnabled(enable);
        this.probeSpinner.setEnabled(enable);
        this.statSpinner.setEnabled(enable);
    }

    /**
     * It creates all the buttons used in this panel.
     */
    private final void createButtons() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        // Create buttons for all the FSM transitions and put them in the map
        for(final RunControlFSM.Transition t : RunControlFSM.Transition.values()) {
            this.tbMap.put(t, new TransitionButton(t));
        }

        // Create labels for all the RC states and put them in the map
        for(final RunControlFSM.State s : RunControlFSM.State.values()) {
            this.slMap.put(s, new StateLabel(s));
        }
    }

    /**
     * Simple panel showing a message.
     * <p>
     * It is used in {@link #buildFSM(RunControlApplicationData)} when the selected application is not running or has not reached the NONE
     * state yet.
     * 
     * @param msg The message to show
     * @return The built panel
     */
    private JPanel createInfoPanel(final String msg) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        final Box hBox = Box.createHorizontalBox();

        final JPanel p = new JPanel();
        p.setAlignmentX(0.5f);
        p.setLayout(new FlowLayout());
        p.add(hBox);

        final JTextArea ta = new JTextArea();
        ta.setAlignmentY(0.5f);
        ta.setOpaque(false);
        ta.setEditable(false);
        ta.setText(msg);
        ta.setLineWrap(true);
        ta.setWrapStyleWord(true);

        hBox.add(ta);

        return p;
    }

    /**
     * It creates and initializes the GUI.
     */
    private final void initGUI() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

        this.setLayout(new BorderLayout());

        // Add ActionListeners to all the buttons but the TransitionButtons (they have their listener at construction time)
        this.publishButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                RunControlAdvancedPanel.this.bActions.publish(RunControlAdvancedPanel.this.cLabel.getText());
            }
        });

        this.publishStatisticsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                RunControlAdvancedPanel.this.bActions.publishStats(RunControlAdvancedPanel.this.cLabel.getText());
            }
        });

        this.setDebugLevelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int value = RunControlAdvancedPanel.this.dbgSpinnerModel.getNumber().intValue();
                RunControlAdvancedPanel.this.bActions.setDebugLevel(RunControlAdvancedPanel.this.cLabel.getText(), value);
            }
        });

        this.setProbeIntervalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int value = RunControlAdvancedPanel.this.probeSpinnerModel.getNumber().intValue();
                RunControlAdvancedPanel.this.bActions.setProbeInterval(RunControlAdvancedPanel.this.cLabel.getText(), value);
            }
        });

        this.setStatIntervalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int value = RunControlAdvancedPanel.this.statSpinnerModel.getNumber().intValue();
                RunControlAdvancedPanel.this.bActions.setStatInterval(RunControlAdvancedPanel.this.cLabel.getText(), value);
            }
        });

        // The top label is contained in a 'top' panel
        this.cLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.cLabel.setBackground(Color.white);
        this.cLabel.setOpaque(true);
        this.cLabel.setMinimumSize(new Dimension(0, 20));

        final JPanel topPanel = new JPanel(new GridLayout(1, 0));
        topPanel.add(this.cLabel);
        topPanel.setBackground(Color.CYAN);
        topPanel.setOpaque(true);
        topPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 50, 5, 50),
                                                              BorderFactory.createLoweredBevelBorder()));

        // Container for the task panes
        final JXTaskPaneContainer taskContainer = new JXTaskPaneContainer();

        // FSM commands container
        final JXTaskPane fsmCmd = new JXTaskPane();
        fsmCmd.setLayout(new BorderLayout());
        fsmCmd.setTitle("FSM Transition Commands");
        this.vBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));
        final JScrollPane jscr = new JScrollPane(this.vBox);
        fsmCmd.add(jscr, BorderLayout.CENTER);
        fsmCmd.setCollapsed(true);

        // Advanced commands container
        final JXTaskPane advCmd = new JXTaskPane();
        advCmd.setLayout(new BorderLayout());
        advCmd.setTitle("Advanced Commands");
        final JPanel advCmdPanel = new JPanel(new BorderLayout(3, 0));
        advCmd.add(advCmdPanel);
        final JPanel bPanel = new JPanel(new GridLayout(2, 1, 0, 15));
        bPanel.add(this.publishButton);
        this.publishButton.setMargin(new Insets(0, 0, 0, 0));
        this.publishButton.setToolTipText("Send the \"" + RCCommands.PUBLISH.toString() + "\" command to the selected application");
        bPanel.add(this.publishStatisticsButton);
        this.publishStatisticsButton.setToolTipText("Send the \"" + RCCommands.PUBLISHSTATS + "\" command to the selected application");
        this.publishStatisticsButton.setMargin(new Insets(0, 0, 0, 0));
        advCmdPanel.add(bPanel, BorderLayout.CENTER);

        final JPanel spinnerPanel = new JPanel(new GridLayout(3, 1));

        final JPanel dbgPanel = new JPanel(new FlowLayout());
        dbgPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Debug Level"));
        dbgPanel.add(this.dbgSpinner);
        dbgPanel.add(this.setDebugLevelButton);

        final JPanel probePanel = new JPanel(new FlowLayout());
        probePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Probe Interval (s)"));
        probePanel.add(this.probeSpinner);
        probePanel.add(this.setProbeIntervalButton);

        final JPanel statPanel = new JPanel(new FlowLayout());
        statPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Full Stat Interval (s)"));
        statPanel.add(this.statSpinner);
        statPanel.add(this.setStatIntervalButton);

        spinnerPanel.add(dbgPanel);
        spinnerPanel.add(probePanel);
        spinnerPanel.add(statPanel);

        advCmdPanel.add(spinnerPanel, BorderLayout.EAST);
        advCmd.setCollapsed(true);

        // Application info container
        final JXTaskPane infoPane = new JXTaskPane();
        infoPane.setLayout(new BorderLayout());
        infoPane.setTitle("Application Information");
        final JPanel infoPanel = new JPanel(new GridLayout(0, 2));

        infoPanel.add(new JLabel("<html><b>Primary Host:</b></html>"));
        infoPanel.add(this.hostLabel);

        infoPanel.add(new JLabel("<html><b>Running Host:</b></html>"));
        infoPanel.add(this.runningHostLabel);        
        
        infoPanel.add(new JLabel("<html><b>Parameters:</b></html>"));
        infoPanel.add(this.paramsLabel);

        infoPanel.add(new JLabel("<html><b>If Exits Unexpectedly:</b></html>"));
        infoPanel.add(this.exitUnexpectedLabel);

        infoPanel.add(new JLabel("<html><b>If Fails To Start:</b></html>"));
        infoPanel.add(this.failsToStartLabel);

        infoPanel.add(new JLabel("<html><b>If Error:</b></html>"));
        infoPanel.add(this.errorLabel);

        infoPanel.add(new JLabel("<html><b>Start At:</b></html>"));
        infoPanel.add(this.startLabel);

        infoPanel.add(new JLabel("<html><b>Stop At:</b></html>"));
        infoPanel.add(this.stopLabel);

        infoPanel.add(new JLabel("<html><b>Can Exit Spontaneously:</b></html>"));
        infoPanel.add(this.canExitLabel);

        infoPanel.add(new JLabel("<html><b>Init Timeout:</b></html>"));
        infoPanel.add(this.initLabel);

        infoPanel.add(new JLabel("<html><b>Action Timeout:</b></html>"));
        infoPanel.add(this.actionLabel);

        infoPane.add(infoPanel, BorderLayout.CENTER);

        // Add task panes to container
        taskContainer.add(fsmCmd);
        taskContainer.add(advCmd);
        taskContainer.add(infoPane);

        // Scroll bar containing the task container
        final JScrollPane scr = new JScrollPane(taskContainer);
        scr.setBorder(new javax.swing.border.CompoundBorder(new javax.swing.border.CompoundBorder(new javax.swing.border.LineBorder(Color.gray),
                                                                                                  new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.RAISED,
                                                                                                                                      Color.white,
                                                                                                                                      Color.lightGray)),
                                                            new javax.swing.border.LineBorder(Color.darkGray)));

        // Add to this panel the top panel and the scroll pane
        this.add(topPanel, BorderLayout.NORTH);
        this.add(scr, BorderLayout.CENTER);
    }
}
