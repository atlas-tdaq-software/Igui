package Igui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXCollapsiblePane;

import rc.RCDataQualityNamed;
import Igui.IguiConstants.AccessControlStatus;
import Igui.IguiConstants.MessageSeverity;
import Igui.IguiException.ElogException;
import Igui.IguiException.UnknownTransition;
import Igui.RunControlFSM.State;
import daq.eformat.DetectorMask;
import daq.eformat.DetectorMask.DETECTOR;
import daq.eformat.DetectorMask.SUBDETECTOR;
import elisa_client.model.InputMessage;
import elisa_client.model.Option;
import elisa_client.model.OptionList;
import elisa_client.model.SystemAffectedList;


/**
 * This panel implements the functionalities allowing the Igui to send messages to the elog server.
 * <p>
 * This panel is a "non tabbed" panel which is not even put in the Igui main frame. The only reference to this panel is kept by the Igui in
 * its panel map. This panel is RC state aware, so that when the start or the stop of run is detected a dialog is shown showing the user the
 * form to send messages to the elog server. The dialog is created only once and uses the information stored in the elog server
 * configuration to build some parts of it. The elog server configuration is retrieved from the elog server only once, the first time a
 * start or stop of the run is detected. This is done to reduce the number of connections to the elog server and avoid possible problems.
 * The only side effect is that the Igui needs to be restarted when the elog server configuration is changed (but this is done really
 * rarely). Most of the work is done in {@link #rcStateChanged(State, State)}: it is here that all the important information about the run
 * is retrieved from IS or from the DB. That is the best place since it is executed in the panel own thread. Then all this information is
 * stored in an internal map afterwards used to fill the elog message with the right attributes. This panel also takes care of writing to IS
 * the RC DQ flag using the information provided by the user at the stop of run ({@linkplain #writeDQFlag(int, String)}).
 * <p>
 * The method {@link ElogDialog#showSpecificDialog(String, String, String)} can be used to show the dialog window on request whenever it is
 * needed.
 * 
 * @see #rcStateChanged(RunControlFSM.State, RunControlFSM.State)
 */
class ElogDialog extends IguiPanel {

    private static final long serialVersionUID = 4725691092176961812L;

    /**
     * Name of the IS server used to store DQ flags
     */
    private final static String isDQServerName = "RunParams.TDAQ_DQ_Flags";

    /**
     * IS information holding the trigger SMK
     */
    private final static String smKeyInfoName = "RunParams.TrigConfSmKey";

    /**
     * IS information holding the L1 pre-scale key
     */
    private final static String l1KeyInfoName = "RunParams.TrigConfL1PsKey";

    /**
     * IS information holding the HLT pre-scale key
     */
    private final static String hltKeyInfoName = "RunParams.TrigConfHltPsKey";

    /**
     * This panel name
     */
    private final static String cName = "Elog-Dialog";

    /**
     * The name of the user running the Igui
     */
    private final static String defaultUserName = System.getProperty("user.name");

    /**
     * The "unknown" string
     */
    private final static String unknown = "unknown";

    /**
     * The JDialog to show
     */
    private final JDialog dialog = new JDialog();

    // Swing components
    private final JTextField userName = new JTextField();
    private final JPasswordField psswd = new JPasswordField();
    private final JTextField authorName = new JTextField();
    private final JTextArea messageText = new JTextArea();
    private final JPanel checkBoxPanel = new JPanel();
    private final JComboBox<String> runOK = new JComboBox<String>();
    private final JButton okButton = new JButton("Send");
    private final JButton cancelButton = new JButton("Cancel");
    private final JProgressBar progBar = new JProgressBar();

    /**
     * List containing all the check boxes referring to different "systems affected" (Access it only from the EDT!)
     */
    private final List<JCheckBox> sysAff = new ArrayList<JCheckBox>(); // Access it only from the EDT!

    /**
     * Reference to the elog interface
     */
    private final AtomicReference<ElogInterface> elogInterface = new AtomicReference<ElogInterface>();

    /**
     * Boolean to know if the elog dialog is active (i.e., whether the dialog has to be shown or not)
     */
    private final AtomicBoolean isElogActive = new AtomicBoolean();

    /**
     * Reference to an object defining the actions associated to different buttons
     */
    private final ButtonActions bActions = new ButtonActions();

    /**
     * Single executor used to enqueue elog dialog requests and show them
     * <p>
     * For consistency reason this must be a single thread executor. In this way requests to show the modal dialog are enqueued and there is
     * no possibility to have dialog overlaps.
     * <p>
     * Every time a request to open an e-log dialog arrives (from the RC state or trigger key changes), the requestor should fill a new
     * {@link ElogMsgQualifiers} data structure and then use this executor to execute {@link #showDialog(ElogMsgQualifiers)}.
     */
    private final ThreadPoolExecutor elogMessageCreator = ExecutorFactory.createSingleExecutor("elog-message-creator");

    /**
     * Reference to the {@link ElogMsgQualifiers} object the currently shown elog dialog refers to.
     * <p>
     * This reference should only be updated in {@link #showDialog(ElogMsgQualifiers)}.
     */
    private final AtomicReference<ElogMsgQualifiers> elogMsgOptions = new AtomicReference<ElogMsgQualifiers>(new ElogMsgQualifiers());

    /**
     * Map used to keep trace the last run number the Elog dialog was shown for. The run number is saved separately for SOR and EOR.
     * <p>
     * The key represents the SOR (true) or the EOR (false). The value represents the last run number.
     */
    private final ConcurrentMap<Boolean, Long> rnMap = new ConcurrentHashMap<Boolean, Long>();

    /**
     * Data structure containing all the information needed to post an elog entry.
     */
    private static class ElogMsgQualifiers {
        /**
         * Synchronized map holding for each message attribute (the map key, {@link ElogMsgAttribute}) its value (the map value, String)
         * <p>
         * The map is used a central storage where the messages attributes are set and then retrieved when the message has to be sent to the
         * elog server.
         */
        // When iterating over the map synchronize on the map itself (java doc says this is imperative)
        private final Map<ElogMsgAttribute, String> msgAttrMap = Collections.synchronizedMap(new EnumMap<ElogMsgAttribute, String>(ElogMsgAttribute.class));

        /**
         * Long containing the end of run time (in ms)
         */
        private final AtomicLong eorTime = new AtomicLong(0);

        /**
         * Long containing the start of run time (in ms)
         */
        private final AtomicLong sorTime = new AtomicLong(0);

        /**
         * Long containing the run number
         */
        private final AtomicLong runNumber = new AtomicLong(-1);

        /**
         * String containing the detector mask
         */
        private volatile String detectorMask = "";

        /**
         * Boolean to keep trace if we are showing the dialog for the start or the end of run
         */
        private final AtomicBoolean isStartOfRun = new AtomicBoolean(false);

        /**
         * Boolean to keep trace if the dialog is shown on external request and not at the start/stop of the run
         */
        private final AtomicBoolean isPanelRequest = new AtomicBoolean(false);

        /**
         * StringBuffer holding the name of the Tier0 Project Tag (always access it through getters and setters)
         */
        private final StringBuffer t0ProjName = new StringBuffer(); // Always access via getters and setters

        /**
         * StringBuffer holding the name of the trigger keys (always access it through getters and setters)
         */
        private final StringBuffer triggerKeys = new StringBuffer(); // Always access via getters and setters

        /**
         * StringBuffer holding the message to add to the text area in the elog dialog
         */
        private final StringBuffer additionalTextMessage = new StringBuffer(); // Always access via getters and setters

        /**
         * StringBuffer holding the title of the dialog window
         */
        private final StringBuffer dialogTitle = new StringBuffer(); // Always access via getters and setters

        /**
         * StringBuffer holding the subject of the e-log message
         */
        private final StringBuffer messageSubject = new StringBuffer(); // Always access via getters and setters

        /**
         * The list of system affected
         */
        private final AtomicReference<List<String>> systemAffected = new AtomicReference<List<String>>(new ArrayList<String>());

        /**
         * Constructor.
         */
        ElogMsgQualifiers() {
            // Reset the attribute map to empty values
            for(final ElogMsgAttribute attr : ElogMsgAttribute.values()) {
                this.msgAttrMap.put(attr, "");
            }

            // Here are "fixed" attributes: do it only once
            this.msgAttrMap.put(ElogMsgAttribute.ONL_PARTITION, Igui.instance().getPartition().getName());
        }

        /**
         * It sets the value of the trigger keys.
         * 
         * @param trk Trigger keys value
         * @see #collectTriggerInfo()
         */
        private void setTriggerKeys(final String trk) {
            this.triggerKeys.replace(0, Integer.MAX_VALUE, trk);
        }

        /**
         * It returns the trigger keys current value.
         * 
         * @return The trigger keys current value
         */
        private String getTriggerKeys() {
            return this.triggerKeys.toString();
        }

        /**
         * It sets the name of the Tier0 Project.
         * 
         * @see #collectRunParamsInfo()
         * @param name The Tier0 Project name
         */
        private void setT0PrjName(final String name) {
            this.t0ProjName.replace(0, Integer.MAX_VALUE, name);
        }

        /**
         * It returns the current value of the Tier0 Project tag.
         * 
         * @return The current value of the Tier0 Project tag
         */
        private String getT0PrjName() {
            return this.t0ProjName.toString();
        }

        /**
         * It sets the text to add to the elog message (it appears in the dialog text area)
         * 
         * @param text The text to add to the elog message
         */
        private void setAdditionalText(final String text) {
            this.additionalTextMessage.replace(0, Integer.MAX_VALUE, text);
        }

        /**
         * It returns the text that has to be added to the elog message
         * 
         * @return The text that has to be added to the elog message
         */
        private String getAdditionalText() {
            return this.additionalTextMessage.toString();
        }

        /**
         * It sets the string used as a title for the dialog window
         * 
         * @param title The string used as a title for the dialog window
         */
        private void setDialogTitle(final String title) {
            this.dialogTitle.replace(0, Integer.MAX_VALUE, title);
        }

        /**
         * It returns the string used as a title for the dialog window
         * 
         * @return The string used as a title for the dialog window
         */
        private String getDialogTitle() {
            return this.dialogTitle.toString();
        }

        /**
         * It sets the string used as a subject for the e-log message
         * 
         * @param subject The string used as a subject for the e-log message
         */
        private void setMessageSubject(final String subject) {
            this.messageSubject.replace(0, Integer.MAX_VALUE, subject);
        }

        /**
         * It returns the string used as subject for the e-log entry
         * 
         * @return The string used as subject for the e-log entry
         */
        private String getMessageSubject() {
            return this.messageSubject.toString();
        }

        /**
         * It sets the list of systems that are affected by the e-log message
         * 
         * @param systemAffected The list of systems that are affected by the e-log message
         */
        private void setSystemAffected(final List<String> systemAffected) {
            this.systemAffected.set(systemAffected);
        }

        /**
         * It returns the list of systems that are affected by the e-log message
         * 
         * @return The list of systems that are affected by the e-log message
         */
        private List<String> getSystemAffected() {
            return Collections.unmodifiableList(this.systemAffected.get());
        }
    }

    /**
     * Enumeration listing the elog message attribute.
     * <p>
     * The list refers to the required attributes and may change when the elog configuration is changes.
     */
    enum ElogMsgAttribute {
        ONL_RUNOK("Onl_RunOK"),
        ONL_RUNNUMBER("Onl_RunNumber"),
        ONL_RUNTYPE("Onl_RunType"),
        ONL_NUMEVENTS("Onl_NumEvents"),
        ONL_RECORDING("Onl_Recording"),
        ONL_PARTITION("Onl_Partition"),
        ONL_DATABASE("Onl_Database");// ONL_TRIGGERKEYS("Onl_TriggerKeys");

        private final String attrName;

        ElogMsgAttribute(final String attrName) {
            this.attrName = attrName;
        }

        @Override
        public String toString() {
            return this.attrName;
        }
    }

    /**
     * SwingWorker used to send the message to the elog server.
     * 
     * @see ButtonActions#readInfo()
     */
    private class ElogMessageSender extends SwingWorker<Void, String> {
        /**
         * The user name provided for authentication to the elog
         */
        private final String uName;

        /**
         * The password provided for authentication to the elog
         */
        private final String userPswd;

        /**
         * The author of the e-log message
         */
        private final String author;

        /**
         * The actual message text
         */
        private final StringBuffer msgText = new StringBuffer();

        /**
         * The source of information to build the elog message.
         * <p>
         * It will always point to the current {@link ElogMsgQualifiers} set in {@link ElogDialog#showDialog(ElogMsgQualifiers)}.
         */
        private final ElogMsgQualifiers elogMsgQlf = ElogDialog.this.elogMsgOptions.get();

        /**
         * Constructor.
         * 
         * @param uName User name
         * @param uPasswd User password
         * @param msgText Message text
         */
        ElogMessageSender(final String uName, final String uPasswd, final String author, final String msgText) {
            this.uName = uName;
            this.userPswd = uPasswd;
            this.author = author;
            this.msgText.append(msgText);
        }

        /**
         * @see javax.swing.SwingWorker#doInBackground()
         */
        @Override
        protected Void doInBackground() throws IguiException.ElogException {
            // Disable the "OK" button, set the progress bar to 'indeterminate' and show a waiting cursor
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    ElogDialog.this.okButton.setEnabled(false);
                    ElogDialog.this.progBar.setIndeterminate(true);
                    ElogDialog.this.dialog.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                }
            });

            // Take care of collecting messages from all the panels
            // Messages from panels should be collected only for SOR/EOR messages
            if(this.elogMsgQlf.isPanelRequest.get() == false) {
                this.publish("Collecting messages from all the panels...");
                try {
                    // Cannot execute this in rcStateChanged, it will block
                    final String panelMessages = Igui.instance().getPanelLogMessages();
                    if(panelMessages.length() > 0) {
                        this.msgText.append("Messages from Igui panels:\n" + panelMessages);
                    }
                }
                catch(final InterruptedException ex) {
                    IguiLogger.warning("Thread interrupted!!!");
                    Thread.currentThread().interrupt();
                }
            }

            this.publish("Creating the e-log message...");

            final ElogInterface ei = ElogDialog.this.elogInterface.get();
            if(ei == null) {
                throw new IguiException.ElogException("Cannot access the e-log interface, got null reference");
            }
            
            final InputMessage elogMessage = new InputMessage();
            elogMessage.setAuthor(this.author);
            elogMessage.setBody(this.msgText.toString());
            elogMessage.setMessageType(ei.getMessageType());
            elogMessage.setSubject(this.elogMsgQlf.getMessageSubject());
            elogMessage.setSystemsAffected(new SystemAffectedList(this.elogMsgQlf.getSystemAffected()));

            final List<Option> messageOptions = new ArrayList<Option>();
            // Take the values for all the defined message attributes
            synchronized(this.elogMsgQlf.msgAttrMap) { // Synch collections doc says this is imperative
                for(final Map.Entry<ElogMsgAttribute, String> e : this.elogMsgQlf.msgAttrMap.entrySet()) {
                    final ElogMsgAttribute key = e.getKey();

                    // Interested to the "Run OK" and number of events only at the EOR
                    if(((this.elogMsgQlf.isStartOfRun.get() == true) || (this.elogMsgQlf.isPanelRequest.get() == true))
                       && (key.equals(ElogMsgAttribute.ONL_RUNOK) || (key.equals(ElogMsgAttribute.ONL_NUMEVENTS))))
                    {
                        continue;
                    }

                    final Option messageOption = new Option();
                    messageOption.setName(e.getKey().toString());
                    messageOption.setValue(e.getValue());

                    messageOptions.add(messageOption);
                }
            }

            elogMessage.setOptions(new OptionList(messageOptions));

            // This throws
            ei.sendMessage(elogMessage, this.uName, this.userPswd);

            return null;
        }

        /**
         * It writes message to the progress bar
         * 
         * @see javax.swing.SwingWorker#process(java.util.List)
         */
        @Override
        protected void process(final List<String> chunks) {
            for(final String s : chunks) {
                ElogDialog.this.progBar.setString(s);
            }
        }

        /**
         * It just gets the result of the computation and sends a warning if some problem occurred.
         * 
         * @see javax.swing.SwingWorker#done()
         */
        @Override
        protected void done() {
            final AtomicBoolean mayDispose = new AtomicBoolean(false);

            try {
                this.get();
                mayDispose.set(true);
            }
            catch(final InterruptedException ex) {
                Thread.currentThread().interrupt();

                final String errMsg = "EDT thread interrupted!!!";
                IguiLogger.warning(errMsg);
            }
            catch(final ExecutionException ex) {
                final String errMsg = "Problem sending the message to the e-log server: " + ex.getCause().getMessage();
                IguiLogger.error(errMsg, ex);
                ErrorFrame.showError("ATLOG Error", errMsg, ex);
            }
            finally {
                // At the end remember to enable again the "ok" button, reset the progress bar
                // and use the default cursor.
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ElogDialog.this.progBar.setString("");
                        ElogDialog.this.okButton.setEnabled(true);
                        ElogDialog.this.progBar.setIndeterminate(false);
                        ElogDialog.this.dialog.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        if(mayDispose.get() == true) {
                            // 'dispose' should be called after all the actions have been performed by the dialog
                            // As soon as the dialog is disposed the next request to show the dialog will be processed
                            // and if 'dispose' is not called the last then next operation may interfere with the new shown dialog
                            ElogDialog.this.dialog.dispose();
                        }
                    }
                });
            }
        }
    }

    /**
     * Simple class containing methods associated to button actions.
     */
    private class ButtonActions {
        private ButtonActions() {
        }

        /**
         * Method executed when the "OK" button is pressed.
         * <p>
         * <ul>
         * <li>The form in the shown dialog is read to get all the needed information;
         * <li>If we are at the end of run the DQ flag is written to IS;
         * <li>An {@link ElogMessageSender} is created and executed to actually send the message to the elog server.
         * </ul>
         * 
         * @see ElogDialog#writeDQFlag(int, String)
         */
        void readInfo() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            final ElogMsgQualifiers elogMsgQlf = ElogDialog.this.elogMsgOptions.get();

            // User name, password and author name from the form
            final String uName = ElogDialog.this.userName.getText().trim();
            final String pswd = new String(ElogDialog.this.psswd.getPassword());
            final String author = ElogDialog.this.authorName.getText().trim();

            if(uName.isEmpty() || pswd.isEmpty() || author.isEmpty()) {
                ErrorFrame.showError("Please, provide user name, author name and password");
            } else {
                // Get the systems affected checking the state of all the check boxes
                // add the system to the list only if the relative check box is selected
                final List<String> selSysAff = new ArrayList<String>();
                for(final JCheckBox cb : ElogDialog.this.sysAff) {
                    if(cb.isSelected() == true) {
                        selSysAff.add(cb.getText());
                    }
                }
                elogMsgQlf.setSystemAffected(selSysAff);

                // Run OK
                final String onlOK = ElogDialog.this.runOK.getSelectedItem().toString();
                elogMsgQlf.msgAttrMap.put(ElogMsgAttribute.ONL_RUNOK, onlOK);

                // Comments
                final StringBuilder comments = new StringBuilder();
                comments.append(ElogDialog.this.messageText.getText() + "\n\n\n");

                try {
                    // Get the sub-detectors using the detector mask
                    final Set<SUBDETECTOR> sd = DetectorMask.decode(elogMsgQlf.detectorMask).getSubdetectors();
                    comments.append("Sub-detectors: " + sd.toString() + "\n\n");
                }
                catch(final IllegalArgumentException ex) {
                    final String errMsg = "Cannot build the list of sub-detectors from the detector mask: " + ex;
                    IguiLogger.error(errMsg, ex);
                    Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                }

                // T0 Project Tag
                comments.append("T0 Project Tag: " + elogMsgQlf.getT0PrjName() + "\n");

                final String trk = elogMsgQlf.getTriggerKeys();
                comments.append(trk + "\n\n");
                // ElogDialog.this.msgAttrMap.put(ElogMsgAttribute.ONL_TRIGGERKEYS, trk);

                // Add time info and write DQ flag only only at the end of run message
                if((elogMsgQlf.isStartOfRun.get() == false) && (elogMsgQlf.isPanelRequest.get() == false)) {
                    // Add sor and eor information
                    comments.append("Run Start time: " + new Date(elogMsgQlf.sorTime.get()).toString() + "\n");
                    comments.append("Run Stop time: " + new Date(elogMsgQlf.eorTime.get()).toString() + "\n\n");

                    // Write the DQ flag
                    final int flag;
                    if(onlOK.equalsIgnoreCase("yes") == true) {
                        flag = 3;
                    } else {
                        flag = 1;
                    }
                    ElogDialog.this.writeDQFlag(flag, elogMsgQlf.msgAttrMap.get(ElogDialog.ElogMsgAttribute.ONL_RUNNUMBER));
                }

                // Start the SwingWorker taking care to send the message to the elog server
                new ElogMessageSender(uName, pswd, author, comments.toString()).execute();
            }
        }

        /**
         * Method executed when the "cancel" button is pressed.
         * <p>
         * The dialog is disposed and the right DQ flag is written to IS.
         * 
         * @see ElogDialog#writeDQFlag(int, String)
         */
        void cancel() {
            assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

            final ElogMsgQualifiers elogMsgQlf = ElogDialog.this.elogMsgOptions.get();

            // Do not write DQ flag for panel requests but only at EOR
            if((elogMsgQlf.isStartOfRun.get() == false) && (elogMsgQlf.isPanelRequest.get() == false)) {
                ElogDialog.this.writeDQFlag(0, elogMsgQlf.msgAttrMap.get(ElogMsgAttribute.ONL_RUNNUMBER));
            }

            // WARNING: call 'dispose' at the end. This will ensure that the task submitted to the elogMsgCreator
            // will return only when *really* everything has been done
            ElogDialog.this.dialog.dispose();
        }
    }

    /**
     * The constructor.
     * <p>
     * It initializes the gui and checks if the correct environment has been defined.
     * 
     * @param tabbed The constructor should always be called with <code>tabbed</code> equal to <code>false</code>
     * @see Igui#isElogEnvDefined()
     * @see #initGUI()
     */
    public ElogDialog(final boolean tabbed) {
        super(tabbed);

        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

        this.isElogActive.set(Igui.instance().isElogEnvDefined());
        this.initGUI();
    }

    /**
     * @see Igui.IguiPanel#getPanelName()
     */
    @Override
    public String getPanelName() {
        return ElogDialog.cName;
    }

    /**
     * @see Igui.IguiPanel#getTabName()
     */
    @Override
    public String getTabName() {
        return ElogDialog.cName;
    }

    /**
     * Empty.
     * 
     * @see Igui.IguiPanel#panelInit(Igui.RunControlFSM.State, Igui.IguiConstants.AccessControlStatus)
     */
    @Override
    public void panelInit(final State rcState, final AccessControlStatus controlStatus) throws IguiException {
    }

    /**
     * Empty
     * 
     * @see Igui.IguiPanel#panelTerminated()
     */
    @Override
    public void panelTerminated() {
    }

    /**
     * This panel is state aware.
     * <p>
     * The dialog is indeed shown at the start and stop of run.
     * 
     * @see Igui.IguiPanel#rcStateAware()
     */
    @Override
    public boolean rcStateAware() {
        return true;
    }

    /**
     * This method is the heart of the Elog dialog. Most of the work is done here since we can use the fact that this method is performed in
     * the panel own thread.
     * <p>
     * <ul>
     * <li>Nothing is done if the Igui is in status display or the elog configuration is not correctly set;
     * <li>Then a check is done on the RC states to be sure that we are at the start or end of run (otherwise nothing is done).
     * </ul>
     * 
     * @see #showSORDialog()
     * @see #showEORDialog()
     * @see Igui.IguiPanel#rcStateChanged(Igui.RunControlFSM.State, Igui.RunControlFSM.State)
     */

    @Override
    public void rcStateChanged(final State oldState, final State newState) {
        // Status display? Elog configuration correctly defined in the environment?
        if((Igui.instance().getAccessLevel().hasMorePrivilegesThan(IguiConstants.AccessControlStatus.DISPLAY) == true)
           && (this.isElogActive.get() == true))
        {
            try {
                // Make a check on the transition
                final Set<RunControlFSM.Transition> trs = RunControlFSM.instance().from(oldState, newState);
                if(trs.contains(RunControlFSM.Transition.START) == true) {
                    this.showSORDialog();
                } else if(trs.contains(RunControlFSM.Transition.STOPARCHIVING)) {
                    this.showEORDialog();
                }
            }
            catch(final ElogException ex) {
                final String errMsg = "Cannot start the e-log interface: " + ex.getMessage()
                                      + ".\nThe e-log interface can be disabled in the \"Settings\" menu.";
                IguiLogger.error(errMsg, ex);
                ErrorFrame.showError("ATLOG Error", errMsg, ex);
            }
            catch(final IllegalArgumentException ex) { // It may be thrown by "from()"
                IguiLogger.warning(ex.getMessage(), ex);
            }
            catch(final UnknownTransition ex) {
                IguiLogger.warning(ex.getMessage(), ex);
            }
        }
    }

    /**
     * It sets the elog interface as active or not.
     * 
     * @see Igui#enableElogInterface(boolean)
     * @param enable <code>true</code> if the elog interface has to be activated.
     */
    void activate(final boolean enable) {
        this.isElogActive.set(enable);
    }

    /**
     * It creates the interface to the e-log server.
     * 
     * @throws ElogException An error occurred creating the interface to the e-log server.
     * @see #rcStateChanged(State, State)
     */
    private void getElogInterface() throws ElogException {
        // Do we have the elog configuration? If we don't then try to get it!
        if(this.elogInterface.get() == null) {
            IguiLogger.info("Creating the interface to the e-log server");

            // Compare and set: allows this method to be safely called by multiple threads
            final ElogInterface eli = new ElogInterface();
            if(this.elogInterface.compareAndSet(null, eli) == true) {
                // Update the dialog form using the information coming from the elog configuration
                this.updateGUI();
            }
        }

        final ElogInterface eli = this.elogInterface.get();

        // Check if some attribute is not valid: this may help to discover that the
        // elog configuration has been modified in some way...
        for(final ElogMsgAttribute attr : ElogMsgAttribute.values()) {
            final String attrName = attr.toString();
            if(eli.isAttributeValid(attrName) == false) {
                IguiLogger.warning("The e-log attribute \"" + attrName + "\" is not valid!");
            }
        }
    }

    /**
     * It shows the elog dialog for the end of run entry submission
     * 
     * @see #prepareAndShowDialog(boolean, boolean, String, String, String)
     * @throws ElogException Some error occurred getting the elog configuration from the server or collecting the needed run information
     */
    private void showEORDialog() throws ElogException {
        // Note that the run number is added to the message text in collectRunParamsInfo(ElogMsgQualifiers)
        this.prepareAndShowDialog(false, false, "Stop of Run Entry", "Stop of Run", "Stop Reason:\n\n\nOther Comments:");
    }

    /**
     * It shows the elog dialog for the start of run entry submission
     * 
     * @see #prepareAndShowDialog(boolean, boolean, String, String, String)
     * @throws ElogException Some error occurred getting the elog configuration from the server or collecting the needed run information
     */
    private void showSORDialog() throws ElogException {
        // Note that the run number is added to the message text in collectRunParamsInfo(ElogMsgQualifiers)
        this.prepareAndShowDialog(true, false, "Start of Run Entry", "Start of Run", "Run Goal:\n\n\nOther Comments:");
    }

    /**
     * This method creates and show an elog dialog interface on request. The user has to define the name of the dialog window, the subject
     * of the elog entry and the message that will be added to it (appearing in the dialog text area).
     * <p>
     * This method performs remote calls (potentially time consuming) and should not be called in the EDT.
     * 
     * @param dialogTitle The dialog title
     * @param entrySubject The subject for the elog entry
     * @param customText The text to be added to the elog entry
     * @see #prepareAndShowDialog(boolean, boolean, String, String, String)
     * @return <code>false</code> if the elog dialog cannot be shown because it is disabled (it can be enabled in the IGUI "Settings" menu)
     * @throws ElogException Some error occurred getting the elog configuration or collecting the needed information about the run
     * @throws IllegalArgumentException The <code>entrySubject</code> string is null or empty
     */
    boolean showSpecificDialog(final String dialogTitle, final String entrySubject, final String customText)
        throws ElogException,
            IllegalArgumentException
    {
        final boolean toReturn;

        if(this.isElogActive.get() == true) {
            toReturn = true;
            this.prepareAndShowDialog(false, true, customText, entrySubject, customText);
        } else {
            toReturn = false;
        }

        return toReturn;
    }

    /**
     * It collects all the needed run information and then schedules the panel creation in the {@link #elogMessageCreator} executor.
     * <p>
     * This method performs potentially time consuming operations and should not be used in the EDT.
     * 
     * @param isStartOfRun <code>true</code> if we are at the start of run
     * @param requestByPanel <code>true</code> if the dialog is being created because on demand
     * @param dialogTitle The dialog title
     * @param entrySubject The elog entry subject (note that in case of start/stop of the run, the subject is modified adding the run
     *            number)
     * @param customText The message text to add to the elog entry
     * @see #getElogInterface()
     * @see #prepareMessage(ElogMsgQualifiers)
     * @see #showDialog(ElogMsgQualifiers)
     * @throws ElogException Some error occurred getting the elog configuration or collecting the run information
     * @throws IllegalArgumentException The provided elog entry subject is null or empty
     */
    private void prepareAndShowDialog(final boolean isStartOfRun,
                                      final boolean requestByPanel,
                                      final String dialogTitle,
                                      final String entrySubject,
                                      final String customText) throws ElogException, IllegalArgumentException
    {
        // Create a new data structure: fill with relevant information before passing it to other methods
        final ElogMsgQualifiers msgQlf = new ElogMsgQualifiers();

        // Enforce a subject
        if((entrySubject != null) && (entrySubject.isEmpty() == false)) {
            msgQlf.setMessageSubject(entrySubject);
        } else {
            throw new IllegalArgumentException("The elog entry subject cannot be null or empty");
        }

        msgQlf.isStartOfRun.set(isStartOfRun);
        msgQlf.isPanelRequest.set(requestByPanel);
        msgQlf.setAdditionalText(customText == null ? "" : customText);

        final StringBuilder dt = new StringBuilder("IGUI ELOG Interface");
        if((dialogTitle != null) && (dialogTitle.isEmpty() == false)) {
            dt.append(" - ");
            dt.append(dialogTitle);
        }
        msgQlf.setDialogTitle(dt.toString());

        // Get the elog configuration
        this.getElogInterface();

        // Prepare the elog message
        this.prepareMessage(msgQlf);

        // Enqueue the request to show the dialog
        this.elogMessageCreator.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ElogDialog.this.showDialog(msgQlf);
                }
                catch(final InterruptedException ex) {
                    IguiLogger.warning("The elog message creator thread has been interrupted!", ex);
                    Thread.currentThread().interrupt();
                }
                catch(final ExecutionException ex) {
                    final String errMsg = "Failed creating the e-log interface dialog: " + ex;
                    IguiLogger.error(errMsg, ex);
                    ErrorFrame.showError("IGUI Error", errMsg, ex);
                }
            }
        });
    }

    /**
     * It collects all the info needed to build the e-log message.
     * 
     * @param elogMsgQlf The data structure where information is stored
     * @throws ElogException Failed to collect run information (mandatory e-log attributes)
     * @see #collectRunParamsInfo()
     * @see #collectTriggerInfo()
     * @see #rcStateChanged(State, State)
     */
    private void prepareMessage(final ElogMsgQualifiers elogMsgQlf) throws ElogException {
        // Collect information about the run
        IguiLogger.info("Collecting run information...");
        this.collectRunParamsInfo(elogMsgQlf);

        // Collect information about the trigger configuration
        IguiLogger.info("Collecting trigger configuration information...");
        this.collectTriggerInfo(elogMsgQlf);
    }

    /**
     * It gets information about the run parameters.
     * 
     * @param msgQlf The data structure where information is stored
     * @see #prepareMessage(ElogMsgQualifiers)
     * @throws ElogException Some error occurred while getting run information
     */
    private void collectRunParamsInfo(final ElogMsgQualifiers msgQlf) throws ElogException {
        // Get info from RunParams IS server and insert relevant info
        // into the message attribute map
        try {
            final rc.RunParamsNamed rp = new rc.RunParamsNamed(Igui.instance().getPartition(), IguiConstants.RUNPARAMS_IS_INFO_NAME);
            rp.checkout();

            final long runN_l = Integer.toUnsignedLong(rp.run_number);
            msgQlf.runNumber.set(runN_l);
            final String runN = Long.toString(runN_l);
            msgQlf.msgAttrMap.put(ElogMsgAttribute.ONL_RUNNUMBER, runN);

            // If we are at the SOR/EOR, modify the subject with the run number
            if(msgQlf.isPanelRequest.get() == false) {
                final StringBuilder sbj = new StringBuilder(msgQlf.getMessageSubject());
                sbj.append(" ");
                sbj.append(runN);

                msgQlf.setMessageSubject(sbj.toString());
            }

            final String runType = rp.run_type;
            msgQlf.msgAttrMap.put(ElogMsgAttribute.ONL_RUNTYPE, runType);

            String recording;
            if(rp.recording_enabled == 1) {
                recording = "Enabled";
            } else {
                recording = "Disabled";
            }
            msgQlf.msgAttrMap.put(ElogMsgAttribute.ONL_RECORDING, recording);

            msgQlf.detectorMask = rp.det_mask;

            msgQlf.setT0PrjName(rp.T0_project_tag);

            msgQlf.sorTime.set(rp.timeSOR.getTime());
            msgQlf.eorTime.set(rp.timeEOR.getTime());
        }
        catch(final Exception ex) {
            throw new IguiException.ElogException("Cannot fill message attributes with the correct run information.\n"
                                                  + "Information cannot be retrieved from the \"" + IguiConstants.RUNPARAMS_IS_INFO_NAME
                                                  + "\" IS server: " + ex, ex);
        }

        // Get DB name from DAL and put it into the message attribute map
        String dbName = ElogDialog.unknown;
        try {
            dbName = Igui.instance().getDalPartition().get_DBName();
        }
        catch(final Exception ex) {
            IguiLogger.error("Error getting the database name from DAL: " + ex, ex);
        }
        finally {
            msgQlf.msgAttrMap.put(ElogMsgAttribute.ONL_DATABASE, dbName);
        }

        // Get recorded events only for the EOR message
        if((msgQlf.isPanelRequest.get() == false) && (msgQlf.isStartOfRun.get() == false)) {
            String recev = ElogDialog.unknown; // This string is used if some problem appears with the DB or IS
            try {
                final dal.IS_InformationSources isSources = Igui.instance().getDalPartition().get_IS_InformationSource();
                if(isSources != null) {
                    final dal.IS_EventsAndRates ear = isSources.get_Recording();
                    if(ear != null) {
                        final String evCounter = ear.get_EventCounter();
                        if(evCounter != null) {
                            final int index = evCounter.lastIndexOf('.');
                            if(index != -1) {
                                final String serverName = evCounter.substring(0, index);
                                final String attrName = evCounter.substring(index + 1);

                                final is.AnyInfo info = ISInfoReceiver.getInfo(serverName, is.AnyInfo.class);
                                final is.InfoDocument infoDoc = new is.InfoDocument(Igui.instance().getPartition(), info);

                                final int num = infoDoc.getAttributeCount();
                                for(int i = 0; i < num; ++i) {
                                    if(infoDoc.getAttribute(i).getName().equals(attrName)) {
                                        final Object value = info.getAttribute(i);
                                        final byte valueType = info.getAttributeType(i);
                                        switch(valueType) {
                                            case is.Type.U8:
                                                recev = Integer.toString(Byte.toUnsignedInt(Byte.parseByte(value.toString())));
                                                break;
                                            case is.Type.U16:
                                                recev = Integer.toString(Short.toUnsignedInt(Short.parseShort(value.toString())));
                                                break;
                                            case is.Type.U32:
                                                recev = Long.toString(Integer.toUnsignedLong(Integer.parseInt(value.toString())));
                                                break;
                                            default:
                                                recev = value.toString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(final Exception ex) {
                final String errMsg = "Cannot get the number of recorded events: " + ex;
                IguiLogger.error(errMsg, ex);
            }
            finally {
                // Put "unknown" or the real number of events
                msgQlf.msgAttrMap.put(ElogMsgAttribute.ONL_NUMEVENTS, recev);
            }
        }
    }

    /**
     * It collects information about the trigger keys in use.
     * 
     * @param msgQlf The data structure where information is stored
     * @see #getTriggerKey(String)
     * @see #prepareMessage(ElogMsgQualifiers)
     */
    private void collectTriggerInfo(final ElogMsgQualifiers msgQlf) {
        final StringBuilder k = new StringBuilder();

        final String sm = this.getTriggerKey(ElogDialog.smKeyInfoName);
        k.append("\nSuperMasterKey = " + sm);

        final String l1 = this.getTriggerKey(ElogDialog.l1KeyInfoName);
        k.append("\nL1PrescaleKey = " + l1);

        final String hlt = this.getTriggerKey(ElogDialog.hltKeyInfoName);
        k.append("\nHLTPrescaleKey = " + hlt);

        msgQlf.setTriggerKeys(k.toString());
    }

    /**
     * It retrieves the information contained into the IS server defined in <code>infoName</code>.
     * 
     * @param infoName The name of the IS information.
     * @return The information contained into the <code>infoName</code> IS server.
     */
    private String getTriggerKey(final String infoName) {
        final StringBuilder trg = new StringBuilder();

        try {
            final is.AnyInfo info = ISInfoReceiver.getInfo(infoName, is.AnyInfo.class);
            final int num = info.getAttributeCount();
            for(int i = 0; i < num; ++i) {
                if(i > 0) {
                    trg.append(" - ");
                }
                trg.append(info.getAttribute(i).toString());
            }
        }
        catch(final IguiException.ISException ex) {
            IguiLogger.warning("The trigger key \"" + infoName + "\" cannot be retrieved from IS: " + ex.getMessage(), ex);
        }

        return trg.toString();
    }

    /**
     * Update the dialog form taking into account information contained in the elog configuration.
     * <p>
     * This method will always execute its actions in the EDT and can be called from every thread.
     * 
     * @see @link #rcStateChanged(RunControlFSM.State, RunControlFSM.State)
     */
    private void updateGUI() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final ElogInterface eli = ElogDialog.this.elogInterface.get();
                if(eli != null) {
                    // Get all the "systems affected" from the elog configuration
                    final List<String> l = eli.getSystemAffected();

                    // Set up the sub-panel hosting the "systems affected" check boxes
                    ElogDialog.this.checkBoxPanel.setLayout(new GridLayout((l.size() / 3) + 1, 3, 5, 5));
                    for(final String s : l) {
                        final JCheckBox cb = new JCheckBox(s);
                        cb.setToolTipText(s);
                        ElogDialog.this.sysAff.add(cb);
                        ElogDialog.this.checkBoxPanel.add(cb);
                    }

                    // Get the "Onl_RunOK" option from the elog configuration and add the options to the "runOK"
                    // combo box
                    final String[] rok = eli.getAttributeValues(ElogMsgAttribute.ONL_RUNOK.toString());
                    if(rok != null) {
                        for(final String s : rok) {
                            ElogDialog.this.runOK.addItem(s);
                        }
                    } else {
                        final String errMsg = "The \"" + ElogMsgAttribute.ONL_RUNOK + "\" has not been found in the e-log configuration";
                        IguiLogger.warning(errMsg);
                    }
                } else {
                    final String errMsg = "The the e-log panel interface cannot be setup properly: the e-log configuration is not accessible";
                    IguiLogger.error(errMsg);
                    Igui.instance().internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
                }
            }
        });
    }

    /**
     * When called it shows the dialog; this method blocks until the dialog is disposed.
     * <p>
     * This is the only place where the {@link #elogMsgOptions} reference is updated.
     * <p>
     * This method will always execute its actions in the EDT and can be called from every thread.
     * 
     * @throws ExecutionException An error occurred trying to show the dialog
     * @throws InterruptedException The current thread has been interrupted waiting for the dialog to be disposed
     */
    private void showDialog(final ElogMsgQualifiers msgQlf) throws InterruptedException, ExecutionException {
        final FutureTask<Void> task = new FutureTask<Void>(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                // Do not show the dialog if already done for this run
                // This check does not apply if the request to show the dialog is coming from an user panel
                boolean showIt = true;
                if(msgQlf.isPanelRequest.get() == false) {
                    final Boolean isSOR = Boolean.valueOf(msgQlf.isStartOfRun.get());
                    final Long lastRN = ElogDialog.this.rnMap.get(isSOR);
                    final Long thisRN = Long.valueOf(msgQlf.runNumber.get());
                    if(thisRN.equals(lastRN) == true) {
                        showIt = false;
                    } else {
                        ElogDialog.this.rnMap.put(isSOR, thisRN);
                    }
                }

                if(showIt == true) {                    
                    ElogDialog.this.elogMsgOptions.set(msgQlf);

                    ElogDialog.this.dialog.setTitle(msgQlf.getDialogTitle());
                    ElogDialog.this.messageText.setText(msgQlf.getAdditionalText());

                    // Detect if we are at the start or end of run
                    if((msgQlf.isPanelRequest.get() == true) || (msgQlf.isStartOfRun.get() == true)) {
                        ElogDialog.this.runOK.setEnabled(false);
                    } else {
                        ElogDialog.this.runOK.setEnabled(true);
                    }

                    // Decode the detector mask and set selected the check box relative
                    // to the effected systems
                    Set<DETECTOR> dm = Collections.emptySet();
                    try {
                        dm = DetectorMask.decode(msgQlf.detectorMask).getDetectors();
                    }
                    catch(final IllegalArgumentException ex) {
                        final String errMsg = "Cannot get the list of detectors from the detector mask: " + ex.toString();
                        IguiLogger.error(errMsg, ex);
                        Igui.instance().internalMessage(MessageSeverity.ERROR, errMsg);
                    }

                    final ElogInterface ei = ElogDialog.this.elogInterface.get();
                    if(ei == null) {
                        throw new IguiException.ElogException("Cannot access the e-log interface, got null reference");
                    }

                    for(final JCheckBox cb : ElogDialog.this.sysAff) {
                        final String sysAffName = cb.getText();
                        final DETECTOR d = ei.getDetector(sysAffName);
                        if(((d != null) && (dm.contains(d) == true)) || 
                           (sysAffName.equals(ei.getCentralDAQSystemAffected()) == true)) // Always select TDAQ
                        { 
                            cb.setSelected(true);
                        } else {
                            cb.setSelected(false);
                        }
                    }

                    ElogDialog.this.dialog.pack();
                    ElogDialog.this.dialog.setLocationRelativeTo(Igui.instance().getMainFrame());

                    // This method blocks.
                    // 'showPanel' has always to be called in the single threaded executor so that
                    // requests to show the dialog will be processed serially: the thread will block
                    // when the dialog is visible and will return when the dialog is disposed.
                    // That's the reason why the panel should be disposed at the very end.
                    ElogDialog.this.dialog.setVisible(true);
                }

                return null;
            }
        });

        javax.swing.SwingUtilities.invokeLater(task);

        task.get();
    }

    /**
     * It starts a SwingWorker used to write in IS the RC DQ flag.
     * 
     * @param flag RC DQ flag
     * @param rn Run number
     * @see ButtonActions#cancel()
     * @see ButtonActions#readInfo()
     */
    private final void writeDQFlag(final int flag, final String rn) {
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                final rc.RCDataQualityNamed info = new RCDataQualityNamed(Igui.instance().getPartition(), ElogDialog.isDQServerName);
                info.Flag = flag;
                info.RunNumber = Integer.parseInt(rn);
                info.Owner = rc.RCDataQualityNamed.owner.shifter;

                info.checkin();

                return null;
            }

            @Override
            public void done() {
                try {
                    this.get();
                }
                catch(final InterruptedException ex) {
                    IguiLogger.warning("EDT interrupted!");
                    Thread.currentThread().interrupt();
                }
                catch(final ExecutionException ex) {
                    final Throwable cause = ex.getCause();
                    final String errMsg = "Failed writing the RC data quality information in IS: " + cause;
                    IguiLogger.error(errMsg, ex);
                    ErrorFrame.showError("Elog Dialog Error", errMsg, ex);
                }
            }
        }.execute();
    }

    /**
     * It builds the GUI.
     * <p>
     * Note that some parts of the GUI are finalized in {@link #updateGUI()} because some information contained in the elog configuration is
     * needed.
     */
    private final void initGUI() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation.";

        this.okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ElogDialog.this.bActions.readInfo();
            }
        });

        this.cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                ElogDialog.this.bActions.cancel();
            }
        });

        this.dialog.setLayout(new BorderLayout());
        this.dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        this.dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        final BoxLayout bl = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(bl);

        final Box usr = Box.createHorizontalBox();
        usr.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        {
            final JLabel l = new JLabel("User Name:");
            l.setPreferredSize(new Dimension(130, 10));
            l.setHorizontalAlignment(SwingConstants.RIGHT);
            usr.add(l);
            usr.add(this.userName);
            this.userName.setText(ElogDialog.defaultUserName);
        }

        final Box pswd = Box.createHorizontalBox();
        pswd.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        {
            final JLabel l = new JLabel("User Password:");
            l.setPreferredSize(new Dimension(130, 10));
            l.setHorizontalAlignment(SwingConstants.RIGHT);
            pswd.add(l);
            pswd.add(this.psswd);
        }

        final Box auth = Box.createHorizontalBox();
        auth.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        {
            final JLabel l = new JLabel("Author:");
            l.setPreferredSize(new Dimension(130, 10));
            l.setHorizontalAlignment(SwingConstants.RIGHT);
            auth.add(l);
            auth.add(this.authorName);
            this.authorName.setText(ElogDialog.defaultUserName);
        }

        final Box sys = Box.createHorizontalBox();
        final JXCollapsiblePane sysPane = new JXCollapsiblePane();
        {                    
            final JLabel l = new JLabel("System Affected:");
            l.setPreferredSize(new Dimension(130, 10));
            l.setHorizontalAlignment(SwingConstants.RIGHT);
            
            sysPane.setLayout(new BorderLayout());
            sysPane.setAnimated(false);
            sysPane.setCollapsed(true);
            sysPane.add(this.checkBoxPanel, BorderLayout.CENTER);
            sysPane.add(l, BorderLayout.WEST);

            sys.add(sysPane);
        }

                
        final Box rok = Box.createHorizontalBox();
        rok.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        {
            final JLabel l = new JLabel(ElogMsgAttribute.ONL_RUNOK.toString() + ":");
            l.setPreferredSize(new Dimension(130, 10));
            l.setHorizontalAlignment(SwingConstants.RIGHT);
            rok.add(l);
            rok.add(this.runOK);
        }

        final Box comm = Box.createHorizontalBox();
        {
            final JLabel l = new JLabel("Comments:");
            l.setHorizontalAlignment(SwingConstants.RIGHT);
            l.setPreferredSize(new Dimension(130, 10));
            comm.add(l);
            final JScrollPane sp = new JScrollPane(this.messageText);
            sp.setPreferredSize(new Dimension(130, 120));
            comm.add(sp);
        }

        final Box bb = Box.createHorizontalBox();
        bb.setAlignmentX(Component.CENTER_ALIGNMENT);
        bb.add(this.okButton);
        bb.add(Box.createHorizontalStrut(30));
        bb.add(this.cancelButton);
        
        final JCheckBox tb = new JCheckBox(sysPane.getActionMap().get(JXCollapsiblePane.TOGGLE_ACTION));
        tb.setSelected(false);        
        tb.setText("Show system affected");
        
        final JPanel bar = new JPanel(new BorderLayout());
        this.progBar.setStringPainted(true);
        this.progBar.setString("");
        bar.add(tb, BorderLayout.EAST);
        bar.add(this.progBar, BorderLayout.CENTER);
        bar.setMaximumSize(new Dimension(Integer.MAX_VALUE, 50));
        bar.setMinimumSize(new Dimension(130, 30));
        
        this.add(usr);
        this.add(Box.createVerticalStrut(10));
        this.add(Box.createVerticalGlue());
        this.add(pswd);
        this.add(Box.createVerticalStrut(10));
        this.add(Box.createVerticalGlue());
        this.add(auth);
        this.add(Box.createVerticalStrut(10));
        this.add(Box.createVerticalGlue());
        this.add(sys);
        this.add(Box.createVerticalStrut(10));
        this.add(Box.createVerticalGlue());
        this.add(rok);
        this.add(Box.createVerticalStrut(10));
        this.add(Box.createVerticalGlue());
        this.add(comm);
        this.add(Box.createVerticalStrut(10));
        this.add(Box.createVerticalGlue());
        this.add(bb);
        this.add(Box.createVerticalStrut(10));
        this.add(Box.createVerticalGlue());
        
        final Border titleBorder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10),
                                                                      BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                                                                                                       "Create your ATLOG entry",
                                                                                                       TitledBorder.LEFT,
                                                                                                       TitledBorder.TOP,
                                                                                                       new Font("Dialog", Font.ITALIC, 12)));

        this.setBorder(titleBorder);
        this.dialog.add(new JScrollPane(this), BorderLayout.CENTER);
        this.dialog.add(bar, BorderLayout.SOUTH);
        this.dialog.setMinimumSize(new Dimension(160, 400));
    }
}
