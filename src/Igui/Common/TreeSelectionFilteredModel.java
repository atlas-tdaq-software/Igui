package Igui.Common;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;


/**
 * Tree selection model allowing to apply a filter to the tree selection. Subclasses need to override -
 * {@link #areNodesEquivalent(Object, Object)} - only "equivalent" nodes will be added to the selection.
 * <p>
 * By default this selection model does not restrict in any way single node selections, unless {@link #canBeSelected(Object)} is overridden.
 * <p>
 * This model overrides only {@link DefaultTreeSelectionModel#addSelectionPaths(TreePath[])} and
 * {@link DefaultTreeSelectionModel#setSelectionPaths(TreePath[])} since the other methods adding or setting single tree paths do nothing
 * different than calling them.
 */
public abstract class TreeSelectionFilteredModel extends DefaultTreeSelectionModel {
    private static final long serialVersionUID = -2318033506857025228L;

    public TreeSelectionFilteredModel() {
        super();
    }

    /**
     * Add to the selection only the paths passing the filter.
     * <p>
     * This is usually called when adding nodes to the selection keeping the CTRL key pressed.
     * <p>
     * It calls the base class method {@link #addSelectionPaths(TreePath[])} passing it the list of "good" tree paths.
     * 
     * @see javax.swing.tree.DefaultTreeSelectionModel#addSelectionPaths(javax.swing.tree.TreePath[])
     */
    @Override
    public void addSelectionPaths(final TreePath[] paths) {
        super.addSelectionPaths(this.filterSelection(paths));
    }

    /**
     * If <code>path</code> is not <code>null</code> then is calls {@link #addSelectionPaths(TreePath[])}.
     * 
     * @see javax.swing.tree.DefaultTreeSelectionModel#addSelectionPath(javax.swing.tree.TreePath)
     */
    @Override
    public void addSelectionPath(final TreePath path) {
        if(path != null) {
            this.addSelectionPaths(new TreePath[] {path});
        }
    }

    /**
     * Do not do any filtering in case of single selection, otherwise select only the tree paths passing the filter.
     * <p>
     * It calls the base class method {@link #setSelectionPaths(TreePath[])} passing it the list of "good" tree paths.
     * <p>
     * This is usually called when multiple nodes are selected in a row keeping the SHIFT key pressed.
     * 
     * @see javax.swing.tree.DefaultTreeSelectionModel#setSelectionPaths(javax.swing.tree.TreePath[])
     */
    @Override
    public void setSelectionPaths(final TreePath[] paths) {
        final TreePath[] p;

        if(paths == null) {
            p = paths;
        } else if(paths.length == 1) {
            // Single selection
            if(this.canBeSelected(paths[0].getLastPathComponent()) == true) {
                p = paths;
            } else {
                p = null;
            }
        } else {
            p = this.filterSelection(paths);
        }

        super.setSelectionPaths(p);
    }

    /**
     * It calls {@link #setSelectionPaths(TreePath[])}.
     * 
     * @see javax.swing.tree.DefaultTreeSelectionModel#setSelectionPath(javax.swing.tree.TreePath)
     */
    @Override
    public void setSelectionPath(final TreePath path) {
        this.setSelectionPaths((path == null) ? null : new TreePath[] {path});
    }

    /**
     * It compares two tree nodes and returns <code>true</code> if they can be both selected at the same time (i.e., they are equivalent).
     * 
     * @param n A tree node
     * @param m A tree node
     * @return <code>true</code> if the two nodes are equivalent.
     */
    protected abstract boolean areNodesEquivalent(final Object n, final Object m);

    /**
     * A node can be selected only if this method returns <em>true</em>.
     * 
     * @param n The node that should be selected
     * @return <em>true</em> if the node can be selected, <em>false</em> otherwise.
     */
    protected boolean canBeSelected(final Object n) {
        return true;
    }

    /**
     * It filter <code>paths</code> returning an array containing only the tree paths passing the filter.
     * 
     * @param paths The list of tree paths to filter
     * @return An array containing only the tree paths passing the filter
     * @see #applyFilter(TreePath)
     */
    private TreePath[] filterSelection(final TreePath[] paths) {
        final TreePath[] filteredPath;

        if(paths == null) {
            filteredPath = paths;
        } else {
            final List<TreePath> l = new ArrayList<TreePath>();

            for(final TreePath p : paths) {
                if((this.canBeSelected(p.getLastPathComponent()) == true) && (this.applyFilter(p) == true)) {
                    l.add(p);
                }
            }

            filteredPath = l.toArray(new TreePath[l.size()]);
        }

        return filteredPath;
    }

    /**
     * It applies the filter to a tree path.
     * <p>
     * The filter is done via calls to {@link #areNodesEquivalent(Object, Object)} passing it the last <code>path</code> and
     * {@link DefaultTreeSelectionModel#leadPath} path components.
     * 
     * @param path The path to check
     * @return <code>true</code> if the path passes the filter
     */
    private boolean applyFilter(final TreePath path) {
        final boolean isFilterPassed;

        // The current path is checked wrt the last path added to the selection
        if(this.leadPath == null) {
            // If the lead path is null it means that currently nothing is selected
            // Then set the lead path to this path: if this is not done then
            // any path could be added because the lead path is set in the super class
            // in 'addSelectionPaths' or 'setSelectionPaths' here called AFTER
            // the tree path has been filtered
            isFilterPassed = true;
            this.leadPath = path; // !!! CRUCIAL !!!
        } else {
            // Add the path to the selection only if the last node in the path has the same state
            // of the last added one (the last node in the lead path)
            final Object lsn = this.leadPath.getLastPathComponent();
            final Object sn = path.getLastPathComponent();
            if(this.areNodesEquivalent(lsn, sn) == true) {
                isFilterPassed = true;
            } else {
                isFilterPassed = false;
            }
        }

        return isFilterPassed;
    }
}
