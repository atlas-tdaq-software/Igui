package Igui.Common;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.Timer;


public class BlinkingComponentTimer implements ActionListener {
    private final JComponent component;

    private final Color firstColor;
    private final Color secondColor;
    private final Color defaultBackgrnd;
    private final Color defaultForegrnd;

    private final boolean blinkWhenDisabled;
    private final boolean isOpaque;

    private final Timer timer = new Timer(500, this);

    public BlinkingComponentTimer(final JComponent button,
                                  final Color firstColor,
                                  final Color secondColor,
                                  final Color defaultBak,
                                  final Color defaultFore,
                                  final boolean blinkWhenDisabled)
    {
        this.component = button;
        this.firstColor = firstColor;
        this.secondColor = secondColor;
        this.defaultBackgrnd = defaultBak;
        this.defaultForegrnd = defaultFore;
        this.blinkWhenDisabled = blinkWhenDisabled;
        this.isOpaque = button.isOpaque();
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if((this.component.isEnabled() == true) || (this.blinkWhenDisabled == true)) {
            this.component.setOpaque(true);
            final Color c = this.component.getForeground();
            if(c.equals(this.firstColor) == true) {
                this.component.setForeground(this.secondColor);
                this.component.setBackground(this.firstColor);
            } else {
                this.component.setForeground(this.firstColor);
                this.component.setBackground(this.secondColor);
            }
        } else {
            this.component.setForeground(this.defaultForegrnd);
            this.component.setBackground(this.defaultBackgrnd);
        }
    }

    public void start() {
        this.timer.start();
    }

    public void stop() {
        this.timer.stop();

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                BlinkingComponentTimer.this.component.setOpaque(BlinkingComponentTimer.this.isOpaque);
                BlinkingComponentTimer.this.component.setForeground(BlinkingComponentTimer.this.defaultForegrnd);
                BlinkingComponentTimer.this.component.setBackground(BlinkingComponentTimer.this.defaultBackgrnd);
            }
        });
    }

    public boolean isRunning() {
        return this.timer.isRunning();
    }
}
