package Igui.Common;

import java.awt.Component;
import java.awt.Dimension;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;


/**
 * This class implements a "timeout" dialog which will appear after a user defined time period; once the dialog appears, the user has the
 * possibility to stop the "count-down". If the final "count-down" is not stopped, then an user defined action will be performed.
 * <p>
 * The user has the possibility to cancel the "count-down"; anyway the dialog will appear again after the defined timeout.
 * <p>
 * The dialog will show a user defined message and a progress bar reporting the remaining time.
 * <p>
 * Implementing classes have to override the {@link #doAction()} and {@link #message()} methods: the first one will executed when the dialog
 * timeout is expired, while the second one is used to report user information message on the dialog itself.
 */

public abstract class TimerDialog {
    /**
     * The parent component of the dialog
     */
    private final Component parentComponent;

    /**
     * Object holding the dialog timeouts
     */
    private final Timeout dialogTimeout;

    /**
     * The executor where tasks are submitted to build and show the dialog
     */
    private final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);

    /**
     * The task returned by the executor
     */
    private ScheduledFuture<?> task = null;

    /**
     * Simple information class holding the dialog timeout configuration
     */
    public static class Timeout {
        private final long timeout;
        private final long alertTimeout;
        private final TimeUnit timeUnit;

        /**
         * Constructor.
         * 
         * @param timeout The time period after which the dialog will be made visible
         * @param alertTimeout The "count-down" period shown in the dialog
         * @param timeUnit The timeout time unit
         */
        Timeout(final long timeout, final long alertTimeout, final TimeUnit timeUnit) {
            this.timeout = timeout;
            this.alertTimeout = alertTimeout;
            this.timeUnit = timeUnit;
        }

        /**
         * It returns the time period after which the dialog will be made visible
         * 
         * @return The time period after which the dialog will be made visible
         */
        public long getTimeout() {
            return this.timeout;
        }

        /**
         * It returns the "count-down" period shown in the dialog
         * 
         * @return The "count-down" period shown in the dialog
         */
        public long getAlertTimeout() {
            return this.alertTimeout;
        }

        /**
         * It returns the timeout time unit
         * 
         * @return The timeout time unit
         */
        public TimeUnit getTimeUnit() {
            return this.timeUnit;
        }
    }

    /**
     * Runnable used to create the dialog, wait for the "count-down" to expire and checking whether the user cancels the "count-down"
     */
    private static class DialogCreator implements Runnable {
        private final long timeoutValue;
        private final TimeUnit timeoutUnit;
        private final AtomicReference<JOptionPane> optionPane = new AtomicReference<JOptionPane>();
        private final AtomicReference<JDialog> optionDialog = new AtomicReference<JDialog>();
        private final AtomicReference<JProgressBar> progressBar = new AtomicReference<JProgressBar>();
        private final TimerDialog timerDialog;

        /**
         * Constructor
         * 
         * @param td Pointer to the {@link TimerDialog}
         * @param timeoutValue Value of the "count-down" timeout shown in the dialog
         * @param timeoutUnit The timeout time unit
         */
        DialogCreator(final TimerDialog td, final long timeoutValue, final TimeUnit timeoutUnit) {
            this.timeoutValue = timeoutValue;
            this.timeoutUnit = timeoutUnit;
            this.timerDialog = td;
        }

        @Override
        public void run() {
            // The timeout is converted into milliseconds
            final long timeoutMillis = this.timeoutUnit.toMillis(this.timeoutValue);

            // Task used to build the dialog in the EDT
            final FutureTask<Void> dialogTask = new FutureTask<Void>(new Callable<Void>() {
                @Override
                public Void call() {
                    assert (javax.swing.SwingUtilities.isEventDispatchThread() == true) : "EDT violation!";

                    // The progress bar
                    final JProgressBar pb = new JProgressBar();
                    pb.setIndeterminate(true);
                    pb.setStringPainted(true);
                    pb.setString(DialogCreator.this.buildTimeoutString(timeoutMillis));
                    DialogCreator.this.progressBar.set(pb);

                    // The option pane
                    final JOptionPane pane = new JOptionPane();
                    pane.setMessage(new Object[] {DialogCreator.this.timerDialog.message(), Box.createVerticalStrut(5), pb});
                    pane.setMessageType(JOptionPane.WARNING_MESSAGE);
                    pane.setOptions(new Object[] {"Cancel"});
                    DialogCreator.this.optionPane.set(pane);

                    // The dialog
                    final JDialog d = pane.createDialog("Timeout Dialog");
                    d.setSize(530, d.getPreferredSize().height);
                    d.setMinimumSize(new Dimension(d.getSize().width, d.getSize().height));
                    d.setResizable(true);
                    d.setLocationRelativeTo(DialogCreator.this.timerDialog.getParentComponent());
                    DialogCreator.this.optionDialog.set(d);

                    // This blocks until the dialog is closed
                    d.pack();
                    d.setVisible(true);

                    return null;
                }
            });

            // When the timeout will elapse
            final long timeoutElapsesAtMillis = System.currentTimeMillis() + timeoutMillis;

            // Create the dialog in the EDT and make it visible
            javax.swing.SwingUtilities.invokeLater(dialogTask);

            // Loop until the task is interrupted
            while((dialogTask.isCancelled() == false) && (Thread.interrupted() == false)) {
                try {
                    // Check every second whether the dialog is closed
                    dialogTask.get(1L, TimeUnit.SECONDS);

                    // Dialog closed, just stop the loop
                    break;
                }
                catch(final TimeoutException ex) {
                    // Timeout elapsed on 'get'; check whether the "count-down" dialog is elapsed
                    final long remainingTime = timeoutElapsesAtMillis - System.currentTimeMillis();
                    if(remainingTime <= 0) {
                        // "Count-down" timeout elapsed, take action
                        this.timerDialog.doAction();
                        break;
                    }

                    // "Count-down" timeout not elapsed yet, publish the remaining time
                    this.updateRemainingTime(remainingTime);
                }
                catch(final CancellationException ex) {
                    // Computation cancelled, do nothing
                    // Loop will stop at the next iteration
                }
                catch(final InterruptedException ex) {
                    // Thread interrupted while waiting, restore the interruption state
                    // The loop will stop at the next iteration
                    Thread.currentThread().interrupt();
                }
                catch(final ExecutionException ex) {
                    // This should never happen given the deterministic actions of the task
                    ex.printStackTrace();
                    break;
                }
            }

            // Dispose the dialog
            this.disposeDialog();
        }

        /**
         * It disposes the dialog
         */
        private void disposeDialog() {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    // Anyway dispose the dialog
                    final JDialog d = DialogCreator.this.optionDialog.get();
                    if(d != null) {
                        d.dispose();
                    }
                }
            });
        }

        /**
         * It updates the progress bar showing the remaining time before the timeout elapses
         * 
         * @param timeMillis The remaining time (in milliseconds) before the timeout elapses
         */
        private void updateRemainingTime(final long timeMillis) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    // Update the remaining time
                    final JProgressBar pb = DialogCreator.this.progressBar.get();
                    if(pb != null) {
                        pb.setString(DialogCreator.this.buildTimeoutString(timeMillis));
                    }
                }
            });
        }

        /**
         * It returns the remaining time in hours, minutes and seconds
         * 
         * @param remainingTimeMillis The remaining time in milliseconds
         * @return An array of size 3 whose first element represents the remaining hours, the second element represents the remaining
         *         minutes and the third element represents the remaining seconds
         */
        private long[] parseRemainingTime(final long remainingTimeMillis) {
            final long[] hms = new long[3];
            hms[0] = TimeUnit.MILLISECONDS.toHours(remainingTimeMillis);
            hms[1] = TimeUnit.MILLISECONDS.toMinutes(remainingTimeMillis - TimeUnit.HOURS.toMillis(hms[0]));
            hms[2] = TimeUnit.MILLISECONDS.toSeconds(remainingTimeMillis - TimeUnit.HOURS.toMillis(hms[0])
                                                     - TimeUnit.MINUTES.toMillis(hms[1]));

            return hms;
        }

        /**
         * It creates the string representing the remaining time
         * 
         * @param remainingTimeMillis The remaining time in milliseconds
         * @return A string representation of the remaining time
         */
        private String buildTimeoutString(final long remainingTimeMillis) {
            final long[] rt = this.parseRemainingTime(remainingTimeMillis);
            final long h = rt[0];
            final long m = rt[1];
            final long s = rt[2];

            final StringBuilder messageString = new StringBuilder();
            messageString.append("Timeout will elapse in ");
            messageString.append(h);
            messageString.append(" hours, ");
            messageString.append(m);
            messageString.append(" minutes and ");
            messageString.append(s);
            messageString.append(" seconds");

            return messageString.toString();
        }
    }

    /**
     * Constructor
     * <p>
     * Note that the total duration of the timeout is the sum of <code>timeout</code> (after which the dialog will appear) and
     * <code>alertTimeout</code> (the "count-down" reported in the dialog progress bar after whose expiration the user action defined in the
     * {@link TimerDialog#doAction()} method will be executed)
     * 
     * @param parentComponent The parent component of the dialog (the dialog will be placed on the screen in a position relative to its
     *            parent component)
     * @param timeout The timeout after which the dialog will appear
     * @param alertTimeout The "count-down" timeout that will be displayed in the dialog; after the expiration of this timeout the user
     *            defined action will be executed
     * @param timeUnit The time units of the defined timeouts
     */
    public TimerDialog(final Component parentComponent, final long timeout, final long alertTimeout, final TimeUnit timeUnit) {
        this.parentComponent = parentComponent;
        this.dialogTimeout = new Timeout(timeout, alertTimeout, timeUnit);
        this.executor.setKeepAliveTime(60, TimeUnit.SECONDS);
        this.executor.allowCoreThreadTimeOut(true);
    }

    /**
     * It starts the timeout counting.
     * 
     * @return <code>false</code> if the timeout counting has already been started
     */
    public synchronized boolean start() {
        boolean toReturn = true;

        // Schedule the task only if this is the first time (i.e., the task is null) or if a previous scheduled task
        // has already done its job
        if((this.task == null) || (this.task.isDone() == true)) {
            // Here is the trick: the DialogCreator Runnable is submitted to the scheduled thread pool
            // The initial and the subsequent delays are set equal to the delay defined by the user
            // The dialog will continue appearing on the screen with fixed delays until the task is cancelled
            // Note that the task returns only in two cases: the "count-down" expires or the user cancels it
            // This kind of executor continues to schedule the task until the task is cancelled, throws an exception or
            // the executor is stopped
            this.task = this.executor.scheduleWithFixedDelay((new DialogCreator(this,
                                                                                this.dialogTimeout.getAlertTimeout(),
                                                                                this.dialogTimeout.getTimeUnit())),
                                                             this.dialogTimeout.getTimeout(),
                                                             this.dialogTimeout.getTimeout(),
                                                             this.dialogTimeout.getTimeUnit());
        } else {
            toReturn = false;
        }

        return toReturn;
    }

    /**
     * It stops the timeout counting.
     */
    public synchronized void stop() {
        if(this.task != null) {
            // Cancelling the task is the way to tell the executor to not schedule it anymore
            // Force the thread interruption so that any blocking call is interrupted (otherwise
            // in-progress tasks are allowed to complete)
            this.task.cancel(true);
        }

        // This removes any cancelled task (no side effects, just to recall memory)
        this.executor.purge();
    }

    /**
     * It returns a pointer to the {@link Timeout} object holding the dialog timeouts
     * 
     * @return The dialog timeouts
     */
    public Timeout getTimeouts() {
        return this.dialogTimeout;
    }

    /**
     * Method to be overridden in implementing classes.
     * <p>
     * Here users should define the actions to be executed when the "count-down" reported by the panel expires
     */
    protected abstract void doAction();

    /**
     * Method to be overridden in implementing classes.
     * <p>
     * The string returned by this method will be shown in the dialog together with a progress bar showing the remaining time
     * 
     * @return The user message to be shown in the dialog. This could be a hint of the actions will be taken if the "count-down" timeout
     *         elapses
     */
    protected abstract String message();

    /**
     * It returns the parent component of the dialog
     * 
     * @return The parent component of the dialog
     */
    private Component getParentComponent() {
        return this.parentComponent;
    }
}
