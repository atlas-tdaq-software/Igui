package Igui.Common;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;


public class ProcessForker {
    private final ProcessBuilder procBuilder;
    private final File logFile;

    private class LogRedirector extends Thread {
        final Process proc;

        LogRedirector(final Process proc) {
            this.proc = proc;
            this.setPriority(Thread.MIN_PRIORITY);
            this.setName("Igui-" + ProcessForker.this.procBuilder.command().get(0) + "-LogRedirector");
        }

        @Override
        public void run() {
            try (final PrintWriter pw = new PrintWriter(ProcessForker.this.logFile);
                final BufferedReader is = new BufferedReader(new InputStreamReader(this.proc.getInputStream()))) {
                String ln = null;
                while((this.isInterrupted() == false) && ((ln = is.readLine()) != null)) {
                    pw.println(ln);
                }
            }
            catch(final Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public ProcessForker(final List<String> args) throws NullPointerException {
        this(args, null);
    }

    public ProcessForker(final List<String> args, final File logFile) throws NullPointerException {
        this.procBuilder = new ProcessBuilder(args);
        this.logFile = logFile;
        if(logFile != null) {
            this.procBuilder.redirectErrorStream(true);
        }
    }

    public ProcessForker(final List<String> args, final File logFile, final File workdir) throws NullPointerException {
        this(args, logFile);
        this.procBuilder.directory(workdir);
    }

    public ProcessForker(final List<String> args, final File logFile, final File workdir, final Map<? extends String, ? extends String> env)
        throws NullPointerException
    {
        this(args, logFile, workdir);
        this.procBuilder.environment().putAll(env);
    }

    public Process start() throws IOException, NullPointerException, IndexOutOfBoundsException {
        final Process p = this.procBuilder.start();

        if(this.logFile != null) {
            new LogRedirector(p).start();
        }

        return p;
    }
}
