package Igui.Common;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JComponent;
import javax.swing.JLabel;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.effect.BufferedImageOpEffect;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.icon.EmptyIcon;
import org.jdesktop.swingx.painter.BusyPainter;


/**
 * This class implements the UI used to put and IguiPanel in a busy state. It uses a {@link com.jhlabs.image.GammaFilter} as image effect
 * and a {@link org.jdesktop.swingx.JXBusyLabel} as 'waiting animation'. A {@link org.jdesktop.swingx.JXLabel} gives the possibility to show
 * the user some messages about the reason why the panel is busy (this can be done using the {@link BusyUI#setBusyMessage(String)} method).
 * 
 * @see BusyUI#setBusyMessage(String)
 */
public class BusyUI extends LockableUI {

    /**
     * {@link org.jdesktop.swingx.JXBusyLabel} used as 'waiting animation'.
     */
    private final JXBusyLabel busyLabel = new JXBusyLabel();

    /**
     * Empty {@link javax.swing.JLabel} used only to make layout easier.
     */
    private final JLabel emptyLabel = new JLabel();

    /**
     * {@link org.jdesktop.swingx.JXLabel} used to show user message.
     */
    private final JXLabel messageLabel = new JXLabel();

    {
        this.initBusyLabel();
        this.messageLabel.setLineWrap(true);
    }

    /**
     * Constructor setting a {@link com.jhlabs.image.GammaFilter} as image effect.
     */
    public BusyUI() {
        super(new BufferedImageOpEffect(new com.jhlabs.image.GammaFilter(2)));
    }

    /**
     * It installs this UI setting the glass panel layout and adding the needed components to it.
     * 
     * @see org.jdesktop.jxlayer.plaf.ext.LockableUI#installUI(javax.swing.JComponent)
     */
    @Override
    public void installUI(final JComponent c) {
        super.installUI(c);
        @SuppressWarnings("unchecked")
        final JXLayer<JComponent> l = (JXLayer<JComponent>) c;
        l.getGlassPane().setLayout(new GridLayout(1, 3, -300, 0));
        l.getGlassPane().add(this.emptyLabel);
        l.getGlassPane().add(this.busyLabel);
        l.getGlassPane().add(this.messageLabel);
        this.busyLabel.setCursor(Cursor.getDefaultCursor());
        this.setBusyMessage("");
    }

    /**
     * This method allows to show the user some messages explaining why this UI is used and it should be called after this UI has been
     * installed.
     * 
     * @param msg The user message.
     */
    public void setBusyMessage(final String msg) {
        this.messageLabel.setText(msg);
        this.setDirty(true);
    }

    /**
     * It set the status of this UI
     * 
     * @param isLocked Set the UI as locked or not. If true the glass pane is shown and the busy label is set to busy state.
     * @see org.jdesktop.jxlayer.plaf.ext.LockableUI#setLocked(boolean)
     */
    @Override
    public void setLocked(final boolean isLocked) {
        super.setLocked(isLocked);
        this.busyLabel.setBusy(isLocked);
        this.setBusyMessage("");
        this.getLayer().getGlassPane().setVisible(isLocked);
    }

    /**
     * It un-installs this UI and the glass pane is no more shown.
     * 
     * @see org.jdesktop.jxlayer.plaf.ext.LockableUI#uninstallUI(javax.swing.JComponent)
     */
    @Override
    public void uninstallUI(final JComponent c) {
        super.uninstallUI(c);
        this.setBusyMessage("");
        @SuppressWarnings("unchecked")
        final JXLayer<JComponent> l = (JXLayer<JComponent>) c;
        l.getGlassPane().setLayout(new FlowLayout());
        l.getGlassPane().remove(this.busyLabel);
        l.getGlassPane().remove(this.emptyLabel);
        l.getGlassPane().remove(this.messageLabel);
        l.getGlassPane().setVisible(false);
    }

    /**
     * Method to setup the visual representation of the {@link org.jdesktop.swingx.JXBusyLabel}.
     */
    private void initBusyLabel() {
        final BusyPainter painter = new BusyPainter(new RoundRectangle2D.Float(0, 0, 31.5f, 5.8f, 10.0f, 10.0f), new Ellipse2D.Float(15.0f,
                                                                                                                                     15.0f,
                                                                                                                                     70.0f,
                                                                                                                                     70.0f));
        painter.setTrailLength(8);
        painter.setPoints(13);
        painter.setFrame(8);

        this.busyLabel.setPreferredSize(new Dimension(100, 100));
        this.busyLabel.setIcon(new EmptyIcon(100, 100));
        this.busyLabel.setBusyPainter(painter);
        this.busyLabel.setBusy(false);
    }

}
