package Igui.Common;

/*
 * @(#)JTreeTable.java 1.2 98/10/27 Copyright 1997, 1998 Sun Microsystems, Inc. All Rights Reserved. Redistribution and use in source and
 * binary forms, with or without modification, are permitted provided that the following conditions are met: - Redistributions of source
 * code must retain the above copyright notice, this list of conditions and the following disclaimer. - Redistributions in binary form must
 * reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution. - Neither the name of Sun Microsystems nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
 * AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.LookAndFeel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.plaf.metal.MetalTreeUI;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;


/**
 * This example shows how to create a simple JTreeTable component, by using a JTree as a renderer (and editor) for the cells in a particular
 * column in the JTable.
 * <p>
 * Modified by G. Avolio: - Take into account variable row height and cell table spacing; - The tree is always and only rendered at column
 * '0'; - The tree model extends AbstractTreeTableModel; - Using some code from JXTreeTable (from swingx).
 * 
 * @version 1.2 10/27/98
 * @author Philip Milne
 * @author Scott Violet 17/12/2008 -
 */
public class JTreeTable extends MultiLineTable {
    private static final long serialVersionUID = -6603752802819496199L;

    /** A subclass of JTree. */
    private final TreeTableCellRenderer tree;

    /**
     * Tree model
     */
    public abstract static class AbstractTreeTableModel extends DefaultTreeModel {
        private static final long serialVersionUID = 7000783227620209024L;

        public AbstractTreeTableModel(final TreeNode root) {
            super(root);
        }

        /**
         * By default, make the column with the Tree in it the only editable one. Making this column editable causes the JTable to forward
         * mouse and keyboard events in the Tree column to the underlying JTree.
         */
        public boolean isCellEditable(final TreeNode node, final int column) {
            return column == 0;
        }

        /**
         * Returns the number of available column.
         */
        abstract public int getColumnCount();

        /**
         * Returns the name for column number <code>column</code>.
         */
        abstract public String getColumnName(int column);

        /**
         * Returns the type for column number <code>column</code>.
         */
        abstract public Class<?> getColumnClass(int column);

        /**
         * Returns the value to be displayed for node <code>node</code>, at column number <code>column</code>.
         */
        abstract public Object getValueAt(TreeNode node, int column);

        protected void fireTreeNodesChanged(final TreeModelEvent e) {
            for(final TreeModelListener ml : this.getTreeModelListeners()) {
                ml.treeNodesChanged(e);
            }
        }

        protected void fireTreeNodesInserted(final TreeModelEvent e) {
            for(final TreeModelListener ml : this.getTreeModelListeners()) {
                ml.treeNodesInserted(e);
            }
        }

        protected void fireTreeNodesRemoved(final TreeModelEvent e) {
            for(final TreeModelListener ml : this.getTreeModelListeners()) {
                ml.treeNodesRemoved(e);
            }
        }

        protected void fireTreeStructureChanged(final TreeModelEvent e) {
            for(final TreeModelListener ml : this.getTreeModelListeners()) {
                ml.treeStructureChanged(e);
            }
        }
    }

    /**
     * This is a wrapper class takes a TreeTableModel and implements the table model interface. The implementation is trivial, with all of
     * the event dispatching support provided by the superclass: the AbstractTableModel.
     * 
     * @version 1.2 10/27/98
     * @author Philip Milne
     * @author Scott Violet
     */
    private class TreeTableModelAdapter extends AbstractTableModel {
        private static final long serialVersionUID = 5060283169241120142L;

        private final JTree jtree;
        private final JTreeTable.AbstractTreeTableModel treeTableModel;

        TreeTableModelAdapter(final JTreeTable.AbstractTreeTableModel treeTableModel, final JTree jtree) {
            this.jtree = jtree;
            this.treeTableModel = treeTableModel;

            jtree.addTreeExpansionListener(new TreeExpansionListener() {
                // Don't use fireTableRowsInserted() here; the selection model
                // would get updated twice.
                @Override
                public void treeExpanded(final TreeExpansionEvent event) {
                    TreeTableModelAdapter.this.fireTableDataChanged();
                }

                @Override
                public void treeCollapsed(final TreeExpansionEvent event) {
                    TreeTableModelAdapter.this.fireTableDataChanged();
                }
            });

            // Install a TreeModelListener that can update the table when
            // tree changes. We use delayedFireTableDataChanged as we can
            // not be guaranteed the tree will have finished processing
            // the event before us.
            treeTableModel.addTreeModelListener(new TreeModelListener() {
                @Override
                public void treeNodesChanged(final TreeModelEvent e) {
                    TreeTableModelAdapter.this.delayedFireTableRowsChanged(e);
                }

                @Override
                public void treeNodesInserted(final TreeModelEvent e) {
                    TreeTableModelAdapter.this.delayedFireTableDataChanged(e, true);
                }

                @Override
                public void treeNodesRemoved(final TreeModelEvent e) {
                    TreeTableModelAdapter.this.delayedFireTableDataChanged(e, false);
                }

                @Override
                public void treeStructureChanged(final TreeModelEvent e) {
                    TreeTableModelAdapter.this.delayedFireTableStructureChanged();
                }
            });
        }

        // Wrappers, implementing TableModel interface.

        @Override
        public int getColumnCount() {
            return this.treeTableModel.getColumnCount();
        }

        @Override
        public String getColumnName(final int column) {
            return this.treeTableModel.getColumnName(column);
        }

        @Override
        public Class<?> getColumnClass(final int column) {
            return this.treeTableModel.getColumnClass(column);
        }

        @Override
        public int getRowCount() {
            return this.jtree.getRowCount();
        }

        @Override
        public Object getValueAt(final int row, final int column) {
            Object value = null;

            final TreeNode n = this.nodeForRow(row);
            if(n != null) {
                value = this.treeTableModel.getValueAt(n, column);
            }

            return value;
        }

        @Override
        public boolean isCellEditable(final int row, final int column) {
            return this.treeTableModel.isCellEditable(this.nodeForRow(row), column);
        }

        /**
         * Invokes fireTableDataChanged after all the pending events have been processed. SwingUtilities.invokeLater is used to handle this.
         */
        @SuppressWarnings("unused")
        private void delayedFireTableDataChanged() {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    TreeTableModelAdapter.this.fireTableDataChanged();
                }
            });
        }

        private void delayedFireTableStructureChanged() {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    TreeTableModelAdapter.this.fireTableDataChanged();
                }
            });
        }

        private void delayedFireTableRowsChanged(final TreeModelEvent e) {
            final boolean expanded = JTreeTable.this.tree.isExpanded(e.getTreePath());

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final int indices[] = e.getChildIndices();
                    final TreePath path = e.getTreePath();
                    if((indices != null) && (indices.length > 0)) {
                        if(expanded == true) {
                            final Object[] nodes = e.getChildren();

                            final List<Integer> tableRowList = new ArrayList<Integer>();
                            for(final Object n : nodes) {
                                final TreePath nPath = path.pathByAddingChild(n);
                                final int index = JTreeTable.this.tree.getRowForPath(nPath);
                                tableRowList.add(Integer.valueOf(index));
                            }

                            TreeTableModelAdapter.this.fireTableRowsUpdated(Collections.min(tableRowList).intValue(),
                                                                            Collections.max(tableRowList).intValue());
                        } else {
                            final int row = JTreeTable.this.tree.getRowForPath(path);
                            if(row >= 0) {
                                TreeTableModelAdapter.this.fireTableRowsUpdated(row, row);
                            } else if((path.getPathCount() == 1)
                                      && (path.getPathComponent(0) == TreeTableModelAdapter.this.treeTableModel.getRoot()))
                            {
                                // This fixes the problem observed when all the messages are removed from the table and
                                // new added messages are not visible. We reach this because when the root node has no children
                                // it is considered as collapsed. The check on the tree path are done to be sure that the event refers
                                // only to the case in which the root node has no children.
                                JTreeTable.this.tree.expandPath(path);
                                TreeTableModelAdapter.this.fireTableDataChanged();
                            }
                        }
                    } else {
                        TreeTableModelAdapter.this.fireTableDataChanged();
                    }
                }
            });
        }

        private void delayedFireTableDataChanged(final TreeModelEvent e, final boolean inserted) {
            final boolean expanded = JTreeTable.this.tree.isExpanded(e.getTreePath());

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        final int indices[] = e.getChildIndices();
                        if((indices != null) && (indices.length > 0)) {
                            final TreePath path = e.getTreePath();
                            if(expanded == true) {
                                final int startingRow = JTreeTable.this.tree.getRowForPath(path) + 1;
                                final int min = startingRow + indices[0];
                                final int max = startingRow + indices[indices.length - 1];
                                if(inserted == true) {
                                    TreeTableModelAdapter.this.fireTableRowsInserted(min, max);
                                } else {
                                    TreeTableModelAdapter.this.fireTableRowsDeleted(min, max);
                                }
                            } else {
                                final int row = JTreeTable.this.tree.getRowForPath(path);
                                if(row >= 0) {
                                    TreeTableModelAdapter.this.fireTableRowsUpdated(row, row);
                                } else if((path.getPathCount() == 1)
                                          && (path.getPathComponent(0) == TreeTableModelAdapter.this.treeTableModel.getRoot()))
                                {
                                    // This fixes the problem observed when all the messages are removed from the table and
                                    // new added messages are not visible. We reach this because when the root node has no children
                                    // it is considered as collapsed. The check on the tree path are done to be sure that the event refers
                                    // only to the case in which the root node has no children.
                                    JTreeTable.this.tree.expandPath(path);
                                    TreeTableModelAdapter.this.fireTableDataChanged();
                                }
                            }
                        } else {
                            TreeTableModelAdapter.this.fireTableDataChanged();
                        }
                    }
                    catch(final Exception ex) {
                        // TODO: this has to be understood even if it looks really puzzling!
                        // From P1 log files analysis it seems that 'indices[0]' sometimes
                        // throws an 'ArrayIndexOutOfBoundsException: 0' exception. And I cannot
                        // understand this because the array size is checked before accessing it...
                        // Catching the exception here and firing the event solves the problem
                        // without any side effect, but I am really curious to know the reason
                        // why that exception is raised!
                        TreeTableModelAdapter.this.fireTableDataChanged();

                        // Well, let's print the stack trace...
                        ex.printStackTrace();
                    }
                }
            });
        }

        private TreeNode nodeForRow(final int row) {
            TreeNode treeNode = null;

            try {
                final TreePath tp = this.jtree.getPathForRow(row);
                if(tp != null) {
                    treeNode = (TreeNode) tp.getLastPathComponent();
                }
            }
            catch(final NullPointerException ex) {
            }
            catch(final ClassCastException ex) {
            }

            return treeNode;
        }
    }

    /**
     * A TreeCellRenderer that displays a JTree.
     */
    private class TreeTableCellRenderer extends JTree implements TableCellRenderer {
        private static final long serialVersionUID = -9148148897550075622L;

        /** Last table/tree row asked to renderer. */
        private int visibleRow;

        private class VolatileUI extends MetalTreeUI {
            @Override
            public void paint(final Graphics g, final JComponent c) {
                // Need to invalidate cell sizes when using a multi-line cell renderer for the table
                this.treeState.invalidateSizes();
                super.paint(g, c);
            }
        }

        TreeTableCellRenderer(final AbstractTreeTableModel model) {
            super(model);

            this.setUI(new VolatileUI());

            this.setRowHeight(-1); // Needed to let the tree take the cell size from the renderer
            this.setRootVisible(false);
            this.setShowsRootHandles(true);

            this.setCellRenderer(new DefaultTreeCellRenderer() { // Use JLabel for rendering tree cells
                private static final long serialVersionUID = 6525906814797559086L;

                @Override
                public Component getTreeCellRendererComponent(final JTree jtree,
                                                              final Object value,
                                                              final boolean isSelected,
                                                              final boolean expanded,
                                                              final boolean leaf,
                                                              final int row,
                                                              final boolean focus)
                {
                    super.getTreeCellRendererComponent(jtree, value, isSelected, expanded, leaf, row, focus);

                    this.setIcon(null);

                    this.setHorizontalAlignment(SwingConstants.LEFT);
                    this.setHorizontalTextPosition(SwingConstants.LEFT);
                    this.setVerticalAlignment(SwingConstants.CENTER);
                    this.setVerticalTextPosition(SwingConstants.CENTER);

                    this.setText(value.toString());

                    // Set the tree cell size taking into account the table row height
                    final int rHeight = JTreeTable.this.getRowHeight(row);
                    try {
                        final TableColumn col = JTreeTable.this.getColumnModel().getColumn(0);
                        final int colWidth = col.getWidth();
                        final int actHeight = this.getPreferredSize().height;
                        if(actHeight != rHeight) {
                            this.setPreferredSize(new Dimension(colWidth, rHeight));
                        }
                    }
                    catch(final Exception ex) {
                    }

                    return this;
                }
            });
        }

        /**
         * updateUI is overridden to set the colors of the Tree's renderer to match that of the table.
         */
        @Override
        public void updateUI() {
            super.updateUI();
            // Make the tree's cell renderer use the table's cell selection
            // colors.
            final TreeCellRenderer tcr = this.getCellRenderer();
            if(tcr instanceof DefaultTreeCellRenderer) {
                final DefaultTreeCellRenderer dtcr = ((DefaultTreeCellRenderer) tcr);
                // For 1.1 uncomment this, 1.2 has a bug that will cause an
                // exception to be thrown if the border selection color is
                // null.
                dtcr.setBorderSelectionColor(null);
                dtcr.setTextSelectionColor(UIManager.getColor("Table.selectionForeground"));
                dtcr.setBackgroundSelectionColor(UIManager.getColor("Table.selectionBackground"));
            }
        }

        /**
         * This is overridden to set the height to match that of the JTable.
         */
        @Override
        public void setBounds(final int x, final int y, final int w, final int h) {
            super.setBounds(x, 0, w, JTreeTable.this.getHeight());
        }

        /**
         * Subclassed to translate the graphics such that the last visible row will be drawn at 0,0.
         */
        @Override
        public void paint(final Graphics g) {
            int tr = 0;
            for(int i = 0; i < this.visibleRow; ++i) {
                tr += this.getRowBounds(i).height;
            }

            g.translate(0, -tr);
            super.paint(g);
        }

        /**
         * TreeCellRenderer method. Overridden to update the visible row.
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {
            if(isSelected) {
                this.setBackground(table.getSelectionBackground());
            } else {
                this.setBackground(table.getBackground());
            }

            this.visibleRow = row;
            return this;
        }

    }

    /**
     * TreeTableCellEditor implementation. Component returned is the JTree.
     */
    private class TreeTableCellEditor extends AbstractCellEditor implements TableCellEditor {
        private static final long serialVersionUID = -3392426077929591311L;

        TreeTableCellEditor() {
            super();
        }

        @Override
        public Component getTableCellEditorComponent(final JTable table,
                                                     final Object value,
                                                     final boolean isSelected,
                                                     final int r,
                                                     final int c)
        {
            return JTreeTable.this.tree;
        }

        @Override
        public Object getCellEditorValue() {
            return null;
        }

        /**
         * Overridden to return false, and if the event is a mouse event it is forwarded to the tree.
         * <p>
         * The behavior for this is debatable, and should really be offered as a property. By returning false, all keyboard actions are
         * implemented in terms of the table. By returning true, the tree would get a chance to do something with the keyboard events. For
         * the most part this is ok. But for certain keys, such as left/right, the tree will expand/collapse where as the table focus should
         * really move to a different column. Page up/down should also be implemented in terms of the table. By returning false this also
         * has the added benefit that clicking outside of the bounds of the tree node, but still in the tree column will select the row,
         * whereas if this returned true that wouldn't be the case.
         * <p>
         * By returning false we are also enforcing the policy that the tree will never be editable (at least by a key sequence).
         */
        @Override
        public boolean isCellEditable(final EventObject e) {
            if(e instanceof MouseEvent) {
                final MouseEvent me = (MouseEvent) e;
                final Point mePoint = me.getPoint();

                final int selRow = JTreeTable.this.rowAtPoint(mePoint);
                final int cellYSpacing = JTreeTable.this.getIntercellSpacing().height;

                final MouseEvent newME = new MouseEvent(JTreeTable.this.tree,
                                                        me.getID(),
                                                        me.getWhen(),
                                                        me.getModifiersEx(),
                                                        me.getX(),
                                                        me.getY() - (cellYSpacing * selRow) - (cellYSpacing / 2), // Correct for cell
                                                        // spacing
                                                        me.getClickCount(),
                                                        me.isPopupTrigger());

                JTreeTable.this.tree.dispatchEvent(newME);
            }

            return false;
        }
    }

    /**
     * Constructor
     * 
     * @param treeTableModel
     */
    public JTreeTable(final AbstractTreeTableModel treeTableModel) {
        super();

        // Create the tree. It will be used as a renderer and editor.
        this.tree = new TreeTableCellRenderer(treeTableModel);
        this.tree.setScrollsOnExpand(true);

        // Install a tableModel representing the visible rows in the tree.
        super.setModel(new TreeTableModelAdapter(treeTableModel, this.tree));

        // Install the tree editor renderer and editor.
        this.getColumnModel().getColumn(0).setCellRenderer(this.tree);
        this.getColumnModel().getColumn(0).setCellEditor(new TreeTableCellEditor());

        // Add a selection model for the table; then add a listener to that
        // selection model so that the tree selection can be updated
        final ListSelectionModel tableSelModel = new DefaultListSelectionModel();
        tableSelModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                final int min = e.getFirstIndex();
                final int max = e.getLastIndex();

                if((min != -1) && (max != -1)) {
                    // Clear the tree selection
                    JTreeTable.this.tree.clearSelection();

                    // Set the tree selection
                    final int sel[] = JTreeTable.this.getSelectedRows();
                    JTreeTable.this.tree.addSelectionRows(sel);
                }
            }
        });
        this.setSelectionModel(tableSelModel);
    }

    /**
     * Overridden to message super and forward the method to the tree. Since the tree is not actually in the component hierarchy it will
     * never receive this unless we forward it in this manner.
     */
    @Override
    public void updateUI() {
        super.updateUI();
        if(this.tree != null) {
            this.tree.updateUI();
        }
        // Use the tree's default foreground and background colors in the
        // table.
        LookAndFeel.installColorsAndFont(this, "Tree.background", "Tree.foreground", "Tree.font");
    }

    /*
     * Workaround for BasicTableUI anomaly. Make sure the UI never tries to paint the editor. The UI currently uses different techniques to
     * paint the renderers and editors and overriding setBounds() below is not the right thing to do for an editor. Returning -1 for the
     * editing row in this case, ensures the editor is never painted.
     */
    @Override
    public int getEditingRow() {
        return (this.editingColumn == 0) ? -1 : this.editingRow;
    }

    /**
     * Returns the tree that is being shared between the model.
     */
    public JTree getTree() {
        return this.tree;
    }
}
