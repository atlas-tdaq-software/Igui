package Igui.Common;

/**
 * MultiLineTable.java Created: Tue May 18 13:15:59 1999
 * 
 * @author Thomas Wernitz, Da Vinci Communications Ltd <thomas_wernitz@clear.net.nz> credit to Zafir Anjum for JTableEx and thanks to SUN
 *         for their source code ;)
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.Segment;
import javax.swing.text.TabExpander;

import org.jdesktop.swingx.JXTable;


public class MultiLineTable extends JXTable {
    private static final long serialVersionUID = -6616239510091259491L;

    public static class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {
        private static final long serialVersionUID = -2018268222753658043L;

        protected final static Border noFocusBorder = new EmptyBorder(1, 2, 1, 2);

        private Color unselectedForeground;
        private Color unselectedBackground;

        public MultiLineCellRenderer() {
            super();
            this.setLineWrap(true);
            this.setWrapStyleWord(true);
            this.setOpaque(true);
            this.setBorder(MultiLineCellRenderer.noFocusBorder);
        }

        @Override
        public void setForeground(final Color c) {
            super.setForeground(c);
            this.unselectedForeground = c;
        }

        @Override
        public void setBackground(final Color c) {
            super.setBackground(c);
            this.unselectedBackground = c;
        }

        @Override
        public void updateUI() {
            super.updateUI();
            this.setForeground(null);
            this.setBackground(null);
        }

        @Override
        public Component getTableCellRendererComponent(final JTable table,
                                                       final Object value,
                                                       final boolean isSelected,
                                                       final boolean hasFocus,
                                                       final int row,
                                                       final int column)
        {

            if(isSelected) {
                super.setForeground(table.getSelectionForeground());
                super.setBackground(table.getSelectionBackground());
            } else {
                super.setForeground((this.unselectedForeground != null) ? this.unselectedForeground : table.getForeground());
                super.setBackground((this.unselectedBackground != null) ? this.unselectedBackground : table.getBackground());
            }

            this.setFont(table.getFont());

            if(hasFocus) {
                this.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
                if(table.isCellEditable(row, column)) {
                    super.setForeground(UIManager.getColor("Table.focusCellForeground"));
                    super.setBackground(UIManager.getColor("Table.focusCellBackground"));
                }
            } else {
                this.setBorder(MultiLineCellRenderer.noFocusBorder);
            }

            this.setValue(value);

            return this;
        }

        protected void setValue(final Object value) {
            this.setText((value == null) ? "" : value.toString());
        }
    }

    private static class MultiLineBasicTableUI extends javax.swing.plaf.basic.BasicTableUI {
        MultiLineBasicTableUI() {
            super();
        }

        @Override
        public void paint(final Graphics g, final JComponent c) {
            super.paint(g, c);

            // Paint the grid
            this.paintGrid(g);
        }

        private void paintGrid(final Graphics g) {
            g.setColor(this.table.getGridColor());

            if(this.table.getShowHorizontalLines()) {
                this.paintHorizontalLines(g);
            }
            if(this.table.getShowVerticalLines()) {
                this.paintVerticalLines(g);
            }
        }

        /*
         * This method paints horizontal lines regardless of whether the table is set to paint one automatically.
         */
        private void paintHorizontalLines(final Graphics g) {
            final Rectangle r = g.getClipBounds();

            final int firstIndex = this.table.rowAtPoint(new Point(0, r.y));
            final int lastIndex = this.lastVisibleRow(r);
            final int rMargin = this.table.getRowMargin();

            int y = -rMargin;
            for(int i = 0; i < firstIndex; i++) {
                y += ((MultiLineTable) this.table).getRowHeight(i) + rMargin;
            }

            for(int index = firstIndex; index <= lastIndex; index++) {
                y += ((MultiLineTable) this.table).getRowHeight(index) + rMargin;
                if((y >= r.y) && (y <= (r.y + r.height))) {
                    g.drawLine(r.x, y, (r.x + r.width) - 1, y);
                }
            }
        }

        /*
         * This method paints vertical lines regardless of whether the table is set to paint one automatically.
         */
        private void paintVerticalLines(final Graphics g) {
            final Rectangle rect = g.getClipBounds();
            int x = 0;
            final int count = this.table.getColumnCount();
            final int horizontalSpacing = this.table.getIntercellSpacing().width;
            for(int index = 0; index <= count; index++) {
                if((x > 0) && (((x - 1) >= rect.x) && ((x - 1) <= (rect.x + rect.width)))) {
                    g.drawLine(x - 1, rect.y, x - 1, (rect.y + rect.height) - 1);
                }

                if(index < count) {
                    x += (this.table.getColumnModel().getColumn(index)).getWidth() + horizontalSpacing;
                }
            }
        }

        private int lastVisibleRow(final Rectangle clip) {
            int lastIndex = this.table.rowAtPoint(new Point(0, (clip.y + clip.height) - 1));
            // If the table does not have enough rows to fill the view we'll get -1.
            // Replace this with the index of the last row.
            if(lastIndex == -1) {
                lastIndex = this.table.getRowCount() - 1;
            }
            return lastIndex;
        }

    } // MultiLineBasicTableUI

    {
        this.setRolloverEnabled(false);

        // Since the last version of SwingX, the sorting has to
        // be disabled, otherwise exceptions are raised when the
        // messages are removed from the table. And it looks like
        // that setting 'setSortsable' to false is not enough
        this.setSortable(false);
        this.setRowSorter(null);
        this.setAutoCreateRowSorter(false);
    }

    public MultiLineTable() {
        this(null, null, null);
    }

    public MultiLineTable(final TableModel dm) {
        this(dm, null, null);
    }

    public MultiLineTable(final TableModel dm, final TableColumnModel cm) {
        this(dm, cm, null);
    }

    public MultiLineTable(final TableModel dm, final TableColumnModel cm, final ListSelectionModel sm) {
        super(dm, cm, sm);
        this.setUI(new MultiLineBasicTableUI());
    }

    public MultiLineTable(final int numRows, final int numColumns) {
        this(new DefaultTableModel(numRows, numColumns));
    }

    public MultiLineTable(final Vector<?> rowData, final Vector<?> columnNames) {
        super(rowData, columnNames);
        this.setUI(new MultiLineBasicTableUI());
    }

    public MultiLineTable(final Object[][] rowData, final Object[] columnNames) {
        super(rowData, columnNames);
        this.setUI(new MultiLineBasicTableUI());
    }

    @Override
    public int rowAtPoint(final Point point) {
        final int y = point.y;
        final int rowSpacing = this.getIntercellSpacing().height;
        final int rowCount = this.getRowCount();
        int rHeight = 0;
        for(int row = 0; row < rowCount; row++) {
            rHeight += this.getRowHeight(row) + rowSpacing;
            if(y < rHeight) {
                return row;
            }
        }
        return -1;
    }

    public int getHeight(final String text, final int width) {
        final FontMetrics fm = this.getFontMetrics(this.getFont());
        int numLines = 1;
        final Segment s = new Segment(text.toCharArray(), 0, 0);
        s.count = s.array.length;
        final TabExpander te = new MyTabExpander(fm);
        int breaks = this.getBreakLocation(s, fm, 0, width, te, 0);
        while((breaks + s.offset) < s.array.length) {
            s.offset += breaks;
            s.count = s.array.length - s.offset;
            numLines++;
            breaks = this.getBreakLocation(s, fm, 0, width, te, 0);
        }

        return numLines * fm.getHeight();
    }

    protected int getTabbedTextOffset(final Segment s,
                                      final FontMetrics metrics,
                                      final int x0,
                                      final int x,
                                      final TabExpander e,
                                      final int startOffset,
                                      final boolean round)
    {
        int currX = x0;
        int nextX = currX;
        final char[] txt = s.array;
        final int n = s.offset + s.count;
        for(int i = s.offset; i < n; i++) {
            if(txt[i] == '\t') {
                if(e != null) {
                    nextX = (int) e.nextTabStop(nextX, (startOffset + i) - s.offset);
                } else {
                    nextX += metrics.charWidth(' ');
                }
            } else if(txt[i] == '\n') {
                return i - s.offset;
            } else if(txt[i] == '\r') {
                return (i + 1) - s.offset; // kill the newline as well
            } else {
                nextX += metrics.charWidth(txt[i]);
            }
            if((x >= currX) && (x < nextX)) {
                // found the hit position... return the appropriate side
                if((round == false) || ((x - currX) < (nextX - x))) {
                    return i - s.offset;
                }

                return (i + 1) - s.offset;
            }
            currX = nextX;
        }

        return s.count;
    }

    protected int getBreakLocation(final Segment s,
                                   final FontMetrics metrics,
                                   final int x0,
                                   final int x,
                                   final TabExpander e,
                                   final int startOffset)
    {

        int index = this.getTabbedTextOffset(s, metrics, x0, x, e, startOffset, false);

        if((s.offset + index) < s.array.length) {
            for(int i = s.offset + Math.min(index, s.count - 1); i >= s.offset; i--) {

                final char ch = s.array[i];
                if(Character.isWhitespace(ch)) {
                    // found whitespace, break here
                    index = (i - s.offset) + 1;
                    break;
                }
            }
        }
        return index;
    }

    private static class MyTabExpander implements TabExpander {
        int tabSize;

        public MyTabExpander(final FontMetrics metrics) {
            this.tabSize = 5 * metrics.charWidth('m');
        }

        @Override
        public float nextTabStop(final float x, final int offset) {
            final int ntabs = (int) x / this.tabSize;
            return (ntabs + 1) * this.tabSize;
        }
    }

    @Override
    public int getRowHeight() {
        return -1;
    }

    @Override
    public int getRowHeight(final int row) {
        final TableModel tm = this.getModel();
        final int fontHeight = this.getFontMetrics(this.getFont()).getHeight();
        int height = fontHeight;
        final Enumeration<TableColumn> cols = this.getColumnModel().getColumns();
        int i = 0;
        while(cols.hasMoreElements()) {
            final TableColumn col = cols.nextElement();
            final TableCellRenderer tcr = col.getCellRenderer();
            if(tcr instanceof MultiLineCellRenderer) {
                final Object value = tm.getValueAt(row, i);
                if(value != null) {
                    height = Math.max(height, this.getHeight(value.toString(), col.getWidth()));
                }
            }
            i++;
        }
        return height;
    }

    @Override
    public Rectangle getCellRect(final int row, final int column, final boolean includeSpacing) {
        Rectangle cellFrame;
        TableColumn aColumn;

        cellFrame = new Rectangle();
        // cellFrame.height = getRowHeight() + rowMargin;
        // cellFrame.y = row * cellFrame.height;
        cellFrame.height = this.getRowHeight(row) + this.rowMargin;
        cellFrame.y = 0;
        for(int i = 0; i < row; i++) {
            cellFrame.y += this.getRowHeight(i) + this.rowMargin;
        }

        int index = 0;
        final int columnMargin = this.getColumnModel().getColumnMargin();
        final Enumeration<TableColumn> enumeration = this.getColumnModel().getColumns();
        while(enumeration.hasMoreElements()) {
            aColumn = enumeration.nextElement();
            cellFrame.width = aColumn.getWidth() + columnMargin;

            if(index == column) {
                break;
            }

            cellFrame.x += cellFrame.width;
            index++;
        }

        if(!includeSpacing) {
            final Dimension spacing = this.getIntercellSpacing();
            // This is not the same as grow(), it rounds differently.
            cellFrame.setBounds(cellFrame.x + (spacing.width / 2),
                                cellFrame.y + (spacing.height / 2),
                                cellFrame.width - spacing.width,
                                cellFrame.height - spacing.height);
        }
        return cellFrame;
    }

    @Override
    public void columnSelectionChanged(final ListSelectionEvent e) {
        this.repaint();
    }

} // MultiLineTable
