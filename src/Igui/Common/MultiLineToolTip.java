package Igui.Common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.JTextPane;
import javax.swing.JToolTip;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.ToolTipUI;
import javax.swing.plaf.basic.BasicToolTipUI;


public class MultiLineToolTip extends JToolTip {

    private final static long serialVersionUID = -1888599343896708261L;

    private final static MultiLineToolTipUI tUI = new MultiLineToolTipUI();

    private final static class MultiLineToolTipUI extends BasicToolTipUI {
        private final static MultiLineToolTipUI sharedInstance = new MultiLineToolTipUI();
        private final JTextPane textPane = new JTextPane();
        private final static int defaultWidth = 600;

        MultiLineToolTipUI() {
            super();
            this.textPane.setOpaque(false);
            this.textPane.setMaximumSize(new Dimension(MultiLineToolTipUI.defaultWidth, 9999999));
            this.textPane.setEditable(false);
            this.textPane.setForeground(Color.BLACK);
            this.textPane.setFont(new Font("Dialog", Font.PLAIN, 13));
        }

        public static ComponentUI createUI(final JComponent c) {
            return MultiLineToolTipUI.sharedInstance;
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.plaf.basic.BasicToolTipUI#installUI(javax.swing.JComponent)
         */
        @Override
        public void installUI(final JComponent c) {
            super.installUI(c);
            c.setLayout(new BorderLayout());
            c.setBackground(new Color(255, 255, 224));
            c.add(this.textPane, BorderLayout.CENTER);
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.plaf.basic.BasicToolTipUI#uninstallUI(javax.swing.JComponent)
         */
        @Override
        public void uninstallUI(final JComponent c) {
            super.uninstallUI(c);
            c.remove(this.textPane);
        }

        public void setToolTipText(final String text) {
            this.textPane.setText(text);
        }

        public String getToolTipText() {
            return this.textPane.getText();
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.plaf.basic.BasicToolTipUI#paint(java.awt.Graphics, javax.swing.JComponent)
         */
        @Override
        public void paint(final Graphics g, final JComponent c) {
            // super.paint(g, c);
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.plaf.basic.BasicToolTipUI#getMaximumSize(javax.swing.JComponent)
         */
        @Override
        public Dimension getMaximumSize(final JComponent c) {
            return this.getPreferredSize(c);
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.plaf.basic.BasicToolTipUI#getMinimumSize(javax.swing.JComponent)
         */
        @Override
        public Dimension getMinimumSize(final JComponent c) {
            return this.getPreferredSize(c);
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.plaf.basic.BasicToolTipUI#getPreferredSize(javax.swing.JComponent)
         */
        @Override
        public Dimension getPreferredSize(final JComponent c) {
            final Dimension maxSize = this.textPane.getMaximumSize();
            final Dimension prefSize = this.textPane.getPreferredSize();

            final Dimension dim = new Dimension(Math.min(prefSize.width, maxSize.width), Math.min(prefSize.height, maxSize.height));

            dim.width += 3;
            dim.height += 3;

            // Add safety space
            this.textPane.setSize(dim);

            return dim;
        }

        public void setHTML(final boolean isHTML) {
            if(isHTML == true) {
                this.textPane.setContentType("text/html");
            } else {
                this.textPane.setContentType("text/plain");
            }
        }
    }

    public MultiLineToolTip(final boolean isHTML) {
        super();
        this.setUI(MultiLineToolTip.tUI);
        MultiLineToolTip.tUI.setHTML(isHTML);
    }

    public MultiLineToolTip() {
        this(false);
    }

    @Override
    public ToolTipUI getUI() {
        return MultiLineToolTip.tUI;
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.JToolTip#getTipText()
     */
    @Override
    public String getTipText() {
        return MultiLineToolTip.tUI.getToolTipText();
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.JToolTip#setTipText(java.lang.String)
     */
    @Override
    public void setTipText(final String tipText) {
        MultiLineToolTip.tUI.setToolTipText(tipText);
    }

}
