package Igui.Common;

import java.awt.Cursor;

import javax.swing.JComponent;

import org.jdesktop.jxlayer.plaf.effect.BufferedImageOpEffect;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;


/**
 * This class implements the UI used to put and IguiPanel in a "disabled" state. It uses a {@link com.jhlabs.image.EmbossFilter} as image
 * effect.
 */
public class LockedUI extends LockableUI {

    /**
     * Constructor setting a {@link com.jhlabs.image.EmbossFilter} as image effect.
     */
    public LockedUI() {
        super(new BufferedImageOpEffect(new com.jhlabs.image.EmbossFilter()));
    }

    /**
     * It installs this UI.
     * 
     * @see org.jdesktop.jxlayer.plaf.ext.LockableUI#installUI(javax.swing.JComponent)
     */
    @Override
    public void installUI(final JComponent c) {
        super.installUI(c);
        c.setCursor(Cursor.getDefaultCursor());
    }

    /**
     * It set the status of this UI.
     * 
     * @param isLocked Set the UI as locked or not.
     * @see org.jdesktop.jxlayer.plaf.ext.LockableUI#setLocked(boolean)
     */
    @Override
    public void setLocked(final boolean isLocked) {
        super.setLocked(isLocked);
    }

    /**
     * It un-installs this UI.
     * 
     * @see org.jdesktop.jxlayer.plaf.ext.LockableUI#uninstallUI(javax.swing.JComponent)
     */
    @Override
    public void uninstallUI(final JComponent c) {
        super.uninstallUI(c);
    }

}
