#!/bin/sh

declare partition;

declare javaBin;

declare classpath;

declare properties;
declare iguiProperties;
declare iguiCmdlnProperties;

declare ersDefaultSubscription;

declare jacorbProperties="-Djacorb.poa.thread_pool_min=25 -Djacorb.poa.thread_pool_max=50 -Djacorb.poa.queue_wait=on -Djacorb.poa.queue_max=50000 -Djacorb.poa.queue_min=49500";
declare assertion="-da";
declare vmProperties="-Dsun.java2d.pmoffscreen=false";

# The help
help() {
    echo "Usage: start_ers_monitor -p <partition> -s <ERS default subscription> <vm properties>"
}

while test $# != 0; do
    case "$1" in
	-[h?x]*) help ; exit ;;
	-D*)    iguiCmdlnProperties="$iguiCmdlnProperties $1" ;;
	-p)     shift; partition="$1" ;;
	-s)     shift; ersDefaultSubscription="$1" ;;
    esac
    shift;
done

# ERS subscription
if [ -n "$ersDefaultSubscription" ]; then
    ersProperties="-Digui.ers.subscription=${ersDefaultSubscription}" ;
fi

# Partition is mandatory
if [ -z "$partition" ]; then
    partition="$TDAQ_PARTITION"
fi

# Check the java bin
javaBin="$TDAQ_JAVA_HOME"/bin/java
if [ -z "$TDAQ_JAVA_HOME" ]; then
    echo "Could not find java binary. Please, check the TDAQ_JAVA_HOME value."
    exit
fi

# Check classpath
classpath="$TDAQ_CLASSPATH"
if [ -z "$TDAQ_CLASSPATH" ]; then
    echo "Could not find java classpath! Exiting..."
    exit
fi

# If not in dbg disable assertions
echo $CMTCONFIG | grep dbg
if [ $? == 0 ]; then
    assertion="-ea";
    iguiProperties="-Dtdaq.igui.debug=true $iguiProperties";
fi 

# Set property for ERS filter file
if [ -n "$TDAQ_IGUI_ERS_FILTER_FILE" ]; then
	iguiProperties="-Digui.ers.filter.file=$TDAQ_IGUI_ERS_FILTER_FILE $iguiProperties";
fi

# Set property for the log4j configuration
if [ -n "$TDAQ_IGUI_LOG4J_CONF_FILE" ]; then
    iguiProperties="-Digui.log4j.conf.file=$TDAQ_IGUI_LOG4J_CONF_FILE $iguiProperties";
else
    iguiProperties="-Digui.log4j.conf.file=$TDAQ_INST_PATH/share/data/Igui/log4j_Igui_xmlConfig.xml $iguiProperties";
fi

# If in P1 then set the property defining the user profile
if [ "$TDAQ_SETUP_POINT1" = "1" ]; then
    userRoles=`amUserRoles -S | tr -s "\n" | tr -s " " | tr "\n" "," | tr " " ","`;
    iguiProperties="-Digui.user.profile=$userRoles $iguiProperties";
fi

# Collect properties
properties="$assertion $vmProperties $jacorbProperties $iguiProperties $iguiCmdlnProperties"

# Disable the ERS signal handler (the VM may crash)
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1

# Set the application name
export TDAQ_APPLICATION_NAME="ERS_MONITOR"

# Start the application
if [ -n "$ersProperties" ]; then
    "$javaBin" "$ersProperties" $properties -cp "$classpath" Igui.ErsMonitor "$partition" &
else
    "$javaBin" $properties -cp "$classpath" Igui.ErsMonitor "$partition" &
fi
